@include('header')
<tr>
    <td style="background: url(https://www.nguyenkim.com/images/companies/_1/html/2018/T5/email_preorder/banner-nameproduct.jpg) no-repeat left top; padding: 20px 0;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td height="27" style="height: 35px;"></td>
            </tr>
            <tr>
                <td style="color: #212121; font-size: 20px; text-align: center;">
                    <a href="http://nguyenkimonline.com/link.php?M=8220104&amp;N=2403&amp;L=60765&amp;F=H" target="_blank" style="color: #212121; text-decoration: none; ">
                        <strong>{{ $title }}</strong>
                    </a>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td height="3" style="height: 3px;"></td>
</tr>
<tr>
    <td style="padding:32px 0 27px; background-color:#000000;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background:#000">
            <tbody>
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background:#000">
                        <tbody>
                        <tr>
                            <td rowspan="3" style="width:145px;"></td>
                            <td rowspan="3" style="width:47px;">
                                <img src="https://www.nguyenkim.com/images/companies/_1/html/2018/T3/big4u/bg-left-voucher.png"
                                     style="border:none;" class="CToWUd">
                            </td>
                            <td style="font-size:14px;color:#fff;text-align:center;text-transform:uppercase;line-height:24px;">
                                Thanh toán cọc
                            </td>
                            <td rowspan="3" style="width:47px;">
                                <img src="https://www.nguyenkim.com/images/companies/_1/html/2018/T3/big4u/bg-right-voucher.png"
                                     style="border:none;" class="CToWUd">
                            </td>
                            <td rowspan="3" style="width:145px;"></td>
                        </tr>
                        <tr>
                            <td style="font-size:60px;color:#fff;text-align:center;text-transform:uppercase;line-height:24px;padding:20px 0 14px;">
                                {{ $total  }}đ
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding: 22px 0 30px;">
                    <a href="{{ $urlRecall }}" style="text-decoration:none" target="_blank">
                        <img src="https://www.nguyenkim.com/images/companies/_1/html/2018/T5/email_preorder/btn-coc.png"
                             style="border:none;" class="CToWUd">
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-size:14px;color:#fff;text-align:center; line-height: 24px;">
                    Quý khách vui lòng thanh toán cọc trong thời gian chương trình để nhận được ưu đãi. <br/>
                    Liên hệ 1900 1267 khi cần hỗ trợ. Xin cảm ơn.
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td style="padding:7px 16px 0; margin-bottom:7px; background-color:#fff;border-bottom:1px solid #ebebec;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td style="color: #102296; font-size: 22px; text-align: center;padding: 18px 0 26px; border-top: 1px solid #0e2093;">
                    Ưu đãi hấp dẫn khi đặt trước {{ $title }}
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        @foreach ($option_images as $image)
                            @if ($loop->index%2 == 0)
                                <tr>
                                    @endif
                                    <td width="100px" align="center" style="padding-bottom: 20px;text-align: center;">
                                        <img style="margin:0;padding:0;border:0;"
                                             @if ($image->image_path == '')
                                             src="https://www.nguyenkim.com/images/companies/_1/gifts.jpg"
                                             @else
                                             src="{{{ $image->image_path }}}"
                                             @endif >
                                    </td>
                                    <td width="234px" style="color: #212121; font-size: 16px;padding-bottom: 20px;">
                                        {{ $image->alt }}
                                    </td>
                                    @if($loop->index%2 != 0)
                                </tr>
                                @endif
                                @if ($loop->last && $loop->count % 2 == 0)
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
@include('footer')