<!DOCTYPE html>
<html>
<head>
    <title>Đấu trường ngược giá</title>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        #product_image img{
            width: 100%;
        }
        #product_descriptions p{
            display: list-item;
            list-style-type: disc;
            list-style-position: inside;
            margin: 0 0 5px;
        }
    </style>
</head>
<body style="padding: 0; margin: 0; font-family: Arial,sans-serif; color: #333333">
<table style="width: 650px; margin: 0 auto" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td><img src="https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T01/dautruongnguocgia/header.jpg" alt="Đấu trường ngược giá"></td>
    </tr>
    <tr>
        <td style="padding: 0 0 20px 0">
            <table style="width: 580px; margin: 0 auto" border="0" cellspacing="0" cellpadding="0">
                <tr style="text-align: center; font-size: 14px; line-height: 20px">
                    <td>
                        <p style="padding: 0; margin: 0 0 20px 0; font-size: 16px">Chào đấu sĩ <strong>{{ $UserName }},</strong></p>
                        <p style="padding: 0; margin: 0 0 20px 0; font-size: 16px">Nguyễn Kim xin chúc mừng bạn đã chính thức trở thành nhà vô địch của <br>“<strong>ĐẤU TRƯỜNG NGƯỢC GIÁ</strong>” trong phiên đấu ngày <strong>{{ $WinnerDate }}.</strong></p>
                        <p style="padding: 0; margin: 0 0 20px 0; font-size: 16px">Với giá đấu chiến thắng chung cuộc là <strong style="color: #ff0000">{{ $Value }}đ</strong>, bạn chính là chủ nhân <br>của siêu phẩm <strong>{{ $ProductName }}.</strong></p>
                    </td>
                </tr>
                <tr style="text-align: center">
                    <td style="border: 1px dashed #c1c1c1; border-radius: 10px; background: #f9f9f9; padding: 20px 0;">
                        <table style="width: 100%" align="center" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="line-height: 20px;; font-size: 16px; padding-bottom: 20px;">Lưu ngay MÃ CODE để mua siêu phẩm <br/>dành riêng cho nhà vô địch</td>
                            </tr>
                            <tr>
                                <td style="color: #000; font-size: 32px; padding: 0; padding-bottom: 20px;">{{ $CouponCode  }}</td>
                            </tr>
                            <tr>
                                <td><a style="background: #ff0000; text-decoration: none; color: #fff; font-size: 14px; width: 156px; height: 44px;display: block; line-height: 44px; text-transform: uppercase; margin: 0 auto 20px auto; border-radius: 50px" href="{{ $url }}">Mua ngay</a></td>
                            </tr>
                            <tr>
                                <td style=" font-size: 14px; color: #666;">Nhấn vào nút “Mua ngay” và nhập mã code để “rinh” chiến lợi phẩm nào!</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <p style="padding: 0; margin: 20px 0; color: #333; font-size: 16px">CHIẾN LỢI PHẨM CỦA BẠN CÓ GÌ ĐẶC BIỆT?</p>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table style="width: 200px;" align="left" border="0" cellspacing="0" cellpadding="0">
                            <tr style="vertical-align: top">
                                <td id="product_image" style="padding: 30px; text-align: center; border: 1px solid #dadada; border-radius: 10px"><img src="{{ $image_url }}" alt="{{ $ProductName }}"></td>
                            </tr>
                        </table>
                        <table style="width: 326px;" align="right" border="0" cellspacing="0" cellpadding="0">
                            <tr style="vertical-align: top">
                                <td style="padding: 0 0 0 20px">
                                    <p style="padding: 0; margin: 0 0 20px 0; color: #000; font-size: 18px"><strong>{{ $ProductName }}</strong></p>
                                    <p style="padding: 0; margin: 0 0 15px 0; color: #000; font-size: 14px">Thông số kỹ thuật</p>
                                    <div id="product_descriptions" style="padding: 0; margin: 0 0 15px 0; color: #333333; font-size: 14px; list-style: none">
                                        {!! $product_descriptions !!}
                                    </div>
                                    <p style="padding: 0; margin: 0"><a href="{{ $url }}" style="font-style: italic; color: #ff0000; font-size: 14px; text-decoration: none">Xem thông tin chi tiết</a></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>