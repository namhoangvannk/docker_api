<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\Models\CscartNkUserGateway;
use App\Models\Mongo\NkUserGateways;
use App\Models\Mongo\NkUsergatewayPermisson;

class PublicAuthenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $header = $request->header();
        $route = $request->route();
        list($path_controller, $actionName) = explode('@', $route[1]['uses']);
        $arrController = explode('\\', $path_controller);
        $controllerName = str_replace("Controller", "", $arrController[count($arrController) - 1]);
        $actionPath = $controllerName . '/' . $actionName;

        if (!isset($header['authorization'][0])) {
            return response('unauthorized to system', 401);
        }

        if (!isset($header['password'][0])) {
            return response('unauthorized to system', 401);
        }

        $authorization = $header['authorization'][0];
        $password = $header['password'][0];
        // $ip_address = $header['ip-address'][0];

        $user_gateway = NkUserGateways::raw()->findOne(
            [
                'user_gateway' => $authorization,
                'token' => $password,
                'is_actived' => 1,
                // 'ip_address' => $ip_address
            ]
        );

        if ($user_gateway == null) {
            return response('unauthorized to system', 401);
        }

        $user_gateway_name = $user_gateway->user_gateway;

        $user_permission = NkUsergatewayPermisson::raw()->findOne(
            [
                'user_gateway' => $user_gateway_name,
                'action_path' => $actionPath,
            ]
        );

        if ($user_permission == null) {
            return response('unauthorized to system', 401);
        }
        
        return $next($request);
    }
}
