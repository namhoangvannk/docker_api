<?php
namespace App\Http\Controllers\Shipping;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Shipping\Repositories\NkShippingVrpPriceConfigsRepository;

class NkShippingVrpPriceConfigsController extends Controller
{
    protected $repo;

    public function __construct(NkShippingVrpPriceConfigsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function import(Request $request)
    {
        return $this->respond(
            $this->repo->import($request)
        );
    }

}
