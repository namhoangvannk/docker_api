<?php
namespace App\Http\Controllers\Shipping;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Shipping\Repositories\NkShippingPricePackagesRepository;

class NkShippingPricePackagesController extends Controller
{
    protected $repo;

    public function __construct(NkShippingPricePackagesRepository $repo)
    {
        $this->repo = $repo;
    }

    public function import(Request $request)
    {
        return $this->respond(
            $this->repo->import($request)
        );
    }

}
