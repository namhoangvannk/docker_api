<?php

namespace App\Http\Controllers\Shipping;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Shipping\Repositories\NkShippingVendorsRepository;

class NkShippingVendorsController extends Controller
{
    protected $repo;
	
	public function __construct(NkShippingVendorsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function import(Request $request)
    {
        return $this->respond(
            $this->repo->import($request)
        );
    }

}
