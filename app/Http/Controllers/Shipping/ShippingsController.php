<?php
namespace App\Http\Controllers\Shipping;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Shipping\Repositories\ShippingsRepository;

class ShippingsController extends Controller
{
    protected $repo;

    public function __construct(ShippingsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function checkout(Request $request)
    {
        return $this->respond(
            $this->repo->checkout($request)
        );
    }

}
