<?php

namespace App\Http\Controllers\Shipping;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Shipping\Repositories\RoutesRepository;

class RoutesController extends Controller
{
    protected $repo;
	
	public function __construct(RoutesRepository $repo)
    {
        $this->repo = $repo;
    }

    public function import(Request $request)
    {
        return $this->respond(
            $this->repo->import($request)
        );
    }

}
