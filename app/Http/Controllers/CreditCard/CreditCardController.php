<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 28/12/2017
 * Time: 10:28 AM
 */

namespace App\Http\Controllers\CreditCard;


use App\Http\Controllers\Controller;
use App\Src\CreditCard\Repositories\CreditCardRepository;
use Illuminate\Http\Request;

class CreditCardController extends Controller
{
    protected $repo;

    public function __construct(CreditCardRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function update(Request $request, $register_id)
    {
        return $this->respond(
            $this->repo->update($request, $register_id)
        );
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }
}