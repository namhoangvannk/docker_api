<?php
namespace App\Http\Controllers\Payoo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Payoo\Repositories\PayooRepository;

class PayooController extends Controller
{
    protected $repo;

    public function __construct(PayooRepository $repo)
    {
        $this->repo = $repo;
    }

    public function execute2(Request $request)
    {
        return $this->respond(
            $this->repo->execute2($request)
        );
    }
}
