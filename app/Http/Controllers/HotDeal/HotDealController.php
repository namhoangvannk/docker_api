<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 23/05/2018
 * Time: 3:29 PM
 */

namespace App\Http\Controllers\HotDeal;

use App\Http\Controllers\Controller;
use App\Src\HotDeal\Repositories\HotDealRepository;
use Illuminate\Http\Request;

class HotDealController extends Controller
{
    protected $repo;
    public function __construct(HotDealRepository $repo){
        $this->repo = $repo;
    }
    // TAO SP GROL
    public function create(Request $request){
        return $this->respond(
            $this->repo->create($request)
        );
    }
    // LAY DANH SACH CAMPAIGN GROL
    public function index(Request $request) {
        return $this->respond(
            $this->repo->all($request)
        );
    }
    // UPDATE GROL
    public function update(Request $request, $product_id){
        return $this->respond(
            $this->repo->update($request, $product_id)
        );
    }

    // UPDATE GROL V2
    public function updateV2(Request $request){
        return $this->respond(
            $this->repo->updateV2($request)
        );
    }
    // LAY DS SP THEO CampaignID
    public function getById(Request $request,$id){
        return $this->respond(
            $this->repo->getHotDealById($request,$id)
        );
    }
    // CRON CHAY GIA RE
    public function cronHotDeal(Request $request){
        return $this->respond(
            $this->repo->cronShockPriceOnline($request)
        );
    }
    // IMPORT SP GIA RE
    public function import(Request $request){
        return $this->respond(
            $this->repo->import($request)
        );
    }
    // IMPORT TRANG THAI
    public function importStatus(Request $request){
        return $this->respond(
            $this->repo->importStatus($request)
        );
    }
    // EXPORT GROL RUNNING
    public function exportHotdealRunning(Request $request){
        return $this->respond(
            $this->repo->exportHotdealRunning($request)
        );
    }
    // UPDATE TRANG THAI GROL THEO CAMPAIGN
    public function updateStatusEndCampaign(Request $request){
        return $this->respond(
            $this->repo->updateStatusEndCampaign($request)
        );
    }
    // UPDATE TRANG THAI GROL (END)
    public function updateStatusEnd(Request $request){
        return $this->respond(
            $this->repo->updateStatusEnd($request)
        );
    }
    // LAY DS GROL NHIEU CAMPAIGN
    public function getHotdealByMids(Request $request){
        return $this->respond(
            $this->repo->getHotdealByMids($request)
        );
    }

}