<?php
namespace App\Http\Controllers\Promotion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Promotion\Repositories\PromotionsUseTodayRepository;

class PromotionsUseTodayController extends Controller
{
    protected $repo;

    public function __construct(PromotionsUseTodayRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function update(Request $request, $id)
    {
        return $this->respond(
            $this->repo->update($request, $id)
        );
    }
}
