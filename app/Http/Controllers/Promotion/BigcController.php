<?php
namespace App\Http\Controllers\Promotion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Promotion\Repositories\BigcRepository;

class BigcController extends Controller
{
    protected $repo;

    public function __construct(BigcRepository $repo)
    {
        $this->repo = $repo;
    }


    public function sendCouponSms(Request $request){
        $result = $this->repo->sendCouponSms($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond($result);
    }
}