<?php
namespace App\Http\Controllers\Promotion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Promotion\Repositories\CouponsAutomaticRepository;

class CouponsAutomaticController extends Controller
{
    protected $repo;

    public function __construct(CouponsAutomaticRepository $repo)
    {
        $this->repo = $repo;
    }

    public function check(Request $request)
    {
        return $this->respond(
            $this->repo->check($request)
        );
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }
}
