<?php
namespace App\Http\Controllers\Promotion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Promotion\Repositories\PromotionsRepository;

class PromotionsController extends Controller
{
    protected $repo;

    public function __construct(PromotionsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function check(Request $request)
    {
        return $this->respond(
            $this->repo->check($request)
        );
    }

    public function update(Request $request, $id)
    {
        return $this->respond(
            $this->repo->update($request)
        );
    }

}
