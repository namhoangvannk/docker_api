<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Product\Repositories\ProductsRepository;

class ProductsController extends Controller
{
    protected $repo;
	
	public function __construct(ProductsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function getProductFeaturePdw(Request $request)
    {
        return $this->respond(
            $this->repo->getProductFeaturePdw($request)
        );
    }

    public function show(Request $request, $product_code)
    {
        return $this->respond(
            $this->repo->show($request, $product_code)
        );
    }

    public function partnerDetail(Request $request, $product_code)
    {
        return $this->respond(
            $this->repo->partnerDetail($request, $product_code)
        );
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function update(Request $request, $product_id)
    {
        return $this->respond(
            $this->repo->update($request,  $product_id)
        );
    }

    public function updatePrice(Request $request)
    {
        return $this->respond(
            $this->repo->updatePrice($request)
        );
    }

    public function updateFromExcel(Request $request)
    {
        return $this->respond(
            $this->repo->updateFromExcel($request)
        );
    }

    public function getRegionsByProduct(Request $request, $product_id) {
        return $this->respond(
            $this->repo->getRegionsByProduct($request, $product_id)
        );
    }

    public function getRelationProducts(Request $request, $product_id) {
        return $this->respond(
            $this->repo->getRelationProducts($request, $product_id)
        );
    }
	
	 public function setRedisValue(Request $request) {
        return $this->respond(
            $this->repo->setRedisValue($request)
        );
    }
}
