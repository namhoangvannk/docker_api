<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 08/06/2018
 * Time: 10:36 AM
 */

namespace App\Http\Controllers\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Product\Repositories\ProductCateFullModeRepository;

class ProductCateFullModeController extends Controller {
    protected $repo;

    public function __construct(ProductCateFullModeRepository $repo){
        $this->repo = $repo;
    }

    public function storeProductFullMode(Request $request){
        return $this->respondCreated(
            $this->repo->storeProductToMongo($request)
        );
    }

    public function storeCateFullMode(Request $request){
        return $this->respondCreated(
            $this->repo->storeCateToMongo($request)
        );
    }

}