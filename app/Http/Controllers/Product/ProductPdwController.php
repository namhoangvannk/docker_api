<?php
namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Product\Repositories\ProductPdwRepository;

class ProductPdwController extends Controller
{
    protected $repo;

    public function __construct(ProductPdwRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function show(Request $request, $product_code)
    {
        return $this->respond(
            $this->repo->show($request, $product_code)
        );
    }
}
