<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Auth\Repositories\AuthRepository;

class AuthController extends Controller
{
    protected $repo;

    public function __construct(AuthRepository $repo)
    {
        $this->repo = $repo;
    }

    public function sigin(Request $request)
    {
        return $this->respondCreated(
            $this->repo->sigin($request)
        );
    }

    public function issigin(Request $request)
    {
        return $this->respondCreated(
            $this->repo->issigin($request)
        );
    }
    public function check_email(Request $request)
    {
        return $this->respondCreated(
            $this->repo->check_email($request)
        );
    }
    public function register_social(Request $request)
    {
        return $this->respondCreated(
            $this->repo->register_social($request)
        );
    }
    public function get_info_social(Request $request)
    {
        return $this->respondCreated(
            $this->repo->get_info_social($request)
        );
    }

}
