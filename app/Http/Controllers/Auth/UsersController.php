<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Auth\Repositories\UsersRepository;

class UsersController extends Controller
{
    protected $repo;

    public function __construct(UsersRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }
    public function register_social(Request $request)
    {
        return $this->respondCreated(
            $this->repo->register_social($request)
        );
    }
    public function get_info_social(Request $request)
    {
        return $this->respondCreated(
            $this->repo->get_info_social($request)
        );
    }

}
