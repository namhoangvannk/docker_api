<?php

namespace App\Http\Controllers\Stock;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Stock\Repositories\StockRepository;

class StockController extends Controller
{
    protected $repo;

    public function __construct(StockRepository $repo)
    {
        $this->repo = $repo;
    }
    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function create(Request $request)
    {
        return $this->respond(
            $this->repo->create($request)
        );
    }

    public function syncProduct(Request $request)
    {
        return $this->respond(
            $this->repo->syncProduct($request)
        );
    }

    public function syncWarehouse(Request $request)
    {
        return $this->respond(
            $this->repo->syncWarehouse($request)
        );
    }

    public function syncProductStatus2Redis(Request $request){
        return $this->respond(
            $this->repo->syncProductStatus2Redis($request)
        );
    }

}