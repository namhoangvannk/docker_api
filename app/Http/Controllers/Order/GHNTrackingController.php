<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 31/05/2018
 * Time: 8:32 AM
 */

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Order\Repositories\GHNTrackingRepository;

class GHNTrackingController extends Controller {

    protected $repo;

    public function __construct(GHNTrackingRepository $repo){
        $this->repo = $repo;
    }
    public function getTrackingData(Request $request,$id){
        return $this->respondCreated(
            $this->repo->allById($request,$id)
        );
    }

    public function createTracking(Request $request){
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function receiveCallbackDataGHN(Request $request){
        return $this->respondCreated(
            $this->repo->tracking($request)
        );
    }

}