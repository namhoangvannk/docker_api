<?php
namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Order\Repositories\CybersourceRepository;

class CybersourceController extends Controller
{
    protected $repo;
	
	public function __construct(CybersourceRepository $repo)
    {
        $this->repo = $repo;
    }

    public function callSilentWithCardInfo(Request $request)
    {
        return $this->respondCreated(
            $this->repo->callSilentWithCardInfo($request)
        );
    }

    public function updateWithCardInfo(Request $request)
    {
        return $this->respondCreated(
            $this->repo->updateWithCardInfo($request)
        );
    }

}
