<?php
namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Order\Repositories\OrderCouponsRepository;

class OrderCouponsController extends Controller
{
    protected $repo;

    public function __construct(OrderCouponsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function show(Request $request, $coupon_code)
    {
        return $this->respond(
            $this->repo->show($request, $coupon_code)
        );
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }
}
