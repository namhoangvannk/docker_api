<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Order\Repositories\OrderRepository;

class OrderController extends Controller
{
    protected $repo;
	
	public function __construct(OrderRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function update(Request $request, $order_id)
    {
        return $this->respond(
            $this->repo->update($request, $order_id)
        );
    }

    public function trackingStatus(Request $request)
    {
        return $this->respond(
            $this->repo->trackingStatus($request)
        );
    }

    public function trackingProductSale(Request $request) {
        return $this->respond(
            $this->repo->trackingProductSale($request)
        );
    }

    public function index(Request $request) {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function show(Request $request, $id)
    {
        return $this->respond(
            $this->repo->show($request, $id)
        );
    }

    public function updateMall(Request $request)
    {
        return $this->respond(
            $this->repo->updateMall($request)
        );
    }
    
    public function updateStatusAutoOrderConfirm(Request $request, $id)
    {
        return $this->respond(
            $this->repo->updateStatusAutoOrderConfirm($request, $id)
        );
    }
    
    public function updateCardName(Request $request)
    {
        return $this->respond(
            $this->repo->updateCardName($request)
        );
    }

}
