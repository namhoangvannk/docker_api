<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Src\Order\Repositories\ExtShipmentsRepository;
use Illuminate\Http\Request;

class ExtShipmentsController extends Controller
{
    protected $repo;
	
	public function __construct(ExtShipmentsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function tracking(Request $request)
    {
        return $this->respondCreated(
            $this->repo->tracking($request)
        );
    }

    public function log(Request $request)
    {
        return $this->respondCreated(
            $this->repo->log($request)
        );
    }

}
