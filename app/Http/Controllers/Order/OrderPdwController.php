<?php
namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Order\Repositories\OrderPdwRepository;

class OrderPdwController extends Controller
{
    protected $repo;

    public function __construct(OrderPdwRepository $repo)
    {
        $this->repo = $repo;
    }

    public function trackingStatus(Request $request)
    {
        return $this->respond(
            $this->repo->trackingStatus($request)
        );
    }

    public function update(Request $request, $order_id)
    {
        return $this->respond(
            $this->repo->update($request, $order_id)
        );
    }

    public function get(Request $request, $order_id)
    {
        return $this->respond(
            $this->repo->get($request,$order_id)
        );
    }



}
