<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Src\Order\Repositories\OrderZaloPayRepository;
use Illuminate\Http\Request;

class OrderZaloPayController extends Controller
{
    protected $repo;

    public function __construct(OrderZaloPayRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function notify(Request $request) {
        return $this->respond(
            $this->repo->notify($request)
        );
    }

    public function trackingStatus(Request $request, $id) {
        return $this->respond(
            $this->repo->trackingStatus($request, $id)
        );
    }
    public function tracking(Request $request, $id) {
        return $this->respond(
            $this->repo->tracking($request, $id)
        );
    }
}