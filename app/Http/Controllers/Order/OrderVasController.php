<?php
namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Order\Repositories\OrderVasRepository;

class OrderVasController extends Controller
{
    protected $repo;

    public function __construct(OrderVasRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function update(Request $request, $order_code)
    {
        return $this->respond(
            $this->repo->update($request, $order_code)
        );
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

}
