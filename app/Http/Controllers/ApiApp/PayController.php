<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 10/15/2018
 * Time: 5:00 PM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\PayRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;


class PayController extends Controller
{
    protected $repo;

    public function __construct(PayRepository $repo)
    {
        $this->repo = $repo;
    }
    public function packageOptimize(Request $request)
    {
        return $this->respond(
            $this->repo->packageOptimize($request)
        );
    }
    public function getInfoPackageByPriceMonth(Request $request)
    {
        return $this->respond(
            $this->repo->getInfoPackageByPriceMonth($request)
        );
    }

}