<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/12/2018
 * Time: 3:23 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\BlockRepository;

class BlockController extends Controller
{
    protected $repo;
    public function __construct(BlockRepository $repo)
    {
        $this->repo = $repo;
    }
    public function index(Request $request ,$id)
    {
        $data = array (
            0 =>
                array (
                    'product_id' => '21091',
                    'image_path' => '10008086-TỦ-LẠNH-HITACHI-584-LÍT-R-M700GPGV2X-_MIR_-01.jpg',
                    'list_price' => '72000000.00',
                    'price' => '70450000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            1 =>
                array (
                    'product_id' => '39944',
                    'image_path' => '10025732-TỦ-LẠNH-SHARP-INVERTER-470-LÍT-SJ-GF60A-01.jpg',
                    'list_price' => '74000000.00',
                    'price' => '51900000.00',
                    'nk_shortname' => 'Inverter - 470 lít - Ngăn đá dưới',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            2 =>
                array (
                    'product_id' => '39954',
                    'image_path' => '10025733-TỦ-LẠNH-SHARP-INVERTER-470-LÍT-SJ-GF60A-01.jpg',
                    'list_price' => '74000000.00',
                    'price' => '51900000.00',
                    'nk_shortname' => 'Inverter - 470 lít - Ngăn đá dưới',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
        );
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => [
                    'header' =>[],
                    'banner' =>[],
                    'product' => $data,
                    'footer' =>[]
                ],
            )
        );
    }
    // dan - lay danh sach tang danh muc
    public function getAll(Request $request){
        $data = config('constants.category_floors');
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => [
                    'category_floors' => $data
                ],
            )
        );
    }
    public function getById(Request $request,$id){
        $data = config('constants.category_floors');
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => [
                    'category_floor' => $data[$id]
                ],
            )
        );
    }
    public function home(Request $request){
        $data_block = config('constants.category_floors');
        return $this->respond(
            $this->repo->home($request,$data_block)
        );
    }
    public function home_v2(Request $request){
        $data_block = config('constants.category_floors_v2');
        return $this->respond(
            $this->repo->home_v2($request,$data_block)
        );
    }
    public function home_v3(Request $request){
        $data_block = config('constants.category_floors_v2');
        return $this->respond(
            $this->repo->home_v3($request,$data_block)
        );
    }
    public function cat_home(Request $request){
        
        return $this->respond(
            $this->repo->cat_home($request)
        );
    }
    public function getHeaderApp(Request $request)
    {
        $data = config('constants.header_app');
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    public function banner_by_category_id(Request $request,$id){
        return $this->respond(
            $this->repo->banner_by_category_id($request,$id)
        );
    }
    public function banner_by_cate_home(Request $request,$id){
        return $this->respond(
            $this->repo->banner_by_cate_home($request,$id)
        );
    }
    public function category_search(Request $request,$id){
        return $this->respond(
            $this->repo->category_search($request,$id)
        );
    }
    public function categories_pretties(Request $request){
        return $this->respond(
            $this->repo->categories_pretties($request)
        );
    }

}