<?php
/**
 * Created by PhpStorm.
 * User: nguyenkim
 * Date: 1/3/19
 * Time: 1:46 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\TestDataRepository;

class TestDataController extends Controller
{
    protected $repo;
    public function __construct(TestDataRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request){
        return $this->respond(
            $this->repo->create($request)
        );
    }
    public function update(Request $request,$id){
        return $this->respond(
            $this->repo->update($request,$id)
        );
    }
}