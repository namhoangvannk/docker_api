<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 5/8/2019
 * Time: 3:43 PM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\InstallmentCreditRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;

class InstallmentCreditController extends Controller
{
    protected $repo;
    public function __construct(InstallmentCreditRepository $repo)
    {
        $this->repo = $repo;
    }
    public function validateCard(Request $request ,$bank_id)
    {
        return $this->respond(
            $this->repo->validateCard($request ,$bank_id)
        );
    }
    public function cardType(Request $request)
    {
        return $this->respond(
            $this->repo->cardType($request)
        );
    }
    public function listBank(Request $request)
    {
        return $this->respond(
            $this->repo->listBank($request)
        );
    }
    public function detailsBank(Request $request , $bank_id)
    {
        return $this->respond(
            $this->repo->detailsBank($request,$bank_id)
        );
    }
    public function conversionRate(Request $request)
    {
        return $this->respond(
            $this->repo->conversionRate($request)
        );
    }
}