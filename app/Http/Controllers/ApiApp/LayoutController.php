<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 4/25/2019
 * Time: 9:18 AM
 */
namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\LayoutRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;
class LayoutController extends Controller
{
    protected $repo;
    public function __construct(LayoutRepository $repo)
    {
        $this->repo = $repo;
    }
    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->index($request)
        );
    }
    public function giadung(Request $request)
    {
        return $this->respond(
            $this->repo->giadung($request)
        );
    }
    public function blog(Request $request)
    {
        return $this->respond(
            $this->repo->blog($request)
        );
    }
}