<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 4/24/2019
 * Time: 9:53 AM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\CrmRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;


class CrmController extends Controller
{
    protected $repo;

    public function __construct(CrmRepository $repo)
    {
        $this->repo = $repo;
    }
    public function createTicket(Request $request)
    {
        $input = $request->all() ;
        return $this->respond(
            $this->repo->createTicket($request)
        );
    }
    public function resolveTicket(Request $request, $ticket_id)
    {
        return $this->respond(
            $this->repo->resolveTicket($request,$ticket_id)
        );
    }

}