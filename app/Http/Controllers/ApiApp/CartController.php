<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:19 PM
 */
namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\CartRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;

class CartController extends Controller
{
    protected $repo;

    public function __construct(CartRepository $repo)
    {
        $this->repo = $repo;
    }
    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->show($request)
        );
    }
}