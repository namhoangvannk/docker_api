<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 11/23/2018
 * Time: 3:38 PM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\LandingpageRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\Banners;
use Mockery\Exception;

class LandingpageController extends Controller
{
    protected $repo;
    public function __construct(LandingpageRepository $repo)
    {
        $this->repo = $repo;
    }
    public function cloneLDP(Request $request)
    {
        return $this->respond(
            $this->repo->cloneLDP($request)
        );
    }
    public function save(Request $request)
    {
        return $this->respond(
            $this->repo->save($request)
        );
    }
    public function updateStatus(Request $request)
    {
        return $this->respond(
            $this->repo->updateStatus($request)
        );
    }
    public function delete(Request $request)
    {
        return $this->respond(
            $this->repo->delete($request)
        );
    }
    public function checkSeoLandingpage(Request $request , $seo_name)
    {
        return $this->respond(
            $this->repo->checkSeoLandingpage($request,$seo_name)
        );
    }
    public function getDetailLandingpage(Request $request , $landing_page_id)
    {
        return $this->respond(
            $this->repo->getDetailLandingpage($request,$landing_page_id)
        );
    }
    public function getListLandingpage(Request $request)
    {
        return $this->respond(
            $this->repo->getListLandingpage($request)
        );
    }
}