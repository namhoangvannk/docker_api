<?php
/**
 * Created by PhpStorm.
 * User: nguyenkim
 * Date: 1/3/19
 * Time: 1:46 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\MomoRepository;

class MomoController extends Controller
{
    protected $repo;
    public function __construct(MomoRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request){
        return $this->respond(
            $this->repo->create($request)
        );
    }
    public function update(Request $request,$id){
        return $this->respond(
            $this->repo->update($request,$id)
        );
    }
}