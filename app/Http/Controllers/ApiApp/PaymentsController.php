<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:23 PM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\PaymentsRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;

class PaymentsController extends Controller
{
    protected $repo;

    public function __construct(PaymentsRepository $repo)
    {
        $this->repo = $repo;
    }
    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->listPayments($request)
        );
    }

    public function info(Request $request, $payment_id)
    {
        return $this->respond(
            $this->repo->info($request, $payment_id)
        );
    }

    public function imageCreditCart(Request $request)
    {
        return $this->respond(
            $this->repo->imageCreditCart($request)
        );
    }
    // api danh sách cty thanh toán bằng thẻ tín dụng
    public function getListCreditCart(Request $request)
    {
        return $this->respond(
            $this->repo->getListCreditCart($request)
        );
    }
    // api kieu tra gop
    public function partPay(Request $request)
    {
        return $this->respond(
            $this->repo->partPay($request)
        );
    }

    // api ti le chuyen doi
    public function conversionRate(Request $request)
    {
        return $this->respond(
            $this->repo->conversionRate($request)
        );
    }

    // api list credit
    public function listCredit(Request $request)
    {
        return $this->respond(
            $this->repo->listCredit($request)
        );
    }

    public function infoCreditcart(Request $request)
    {
        return $this->respond(
            $this->repo->infoCreditcart($request)
        );
    }

    public function listCardnumber(Request $request, $user_id)
    {
        return $this->respond(
            $this->repo->listCardnumber($request, $user_id)
        );
    }
}