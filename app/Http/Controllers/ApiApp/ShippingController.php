<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 7/13/2018
 * Time: 10:51 AM
 */
namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ShippingController extends Controller
{
    public function __construct(){
    }
    public function index(Request $request)
    {
        $valid = Validator::make($request->all(),
            [
                'product_id' => 'required',
                'qty' => 'required',
                'address' => 'required',
                'type' => 'required',
            ],
            [
                'product_id.required' =>'ProductID không được rỗng',
                'qty.required' =>'Số lượng không được rỗng ',
                'address.required' =>'Địa chỉ không được rỗng ',
                'type.required' =>'Hình thức giao hàng không được rỗng ',
            ]
        );
        if($valid->passes()) {
            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => 0,
                )
            );
        }else{
            $errors = $valid->errors();
            $data_error = [];
            !$errors->has('product_id') ?: $data_error['product_id'] = $errors->first('product_id');
            !$errors->has('qty') ?: $data_error['qty'] = $errors->first('qty');
            !$errors->has('address') ?: $data_error['address'] = $errors->first('address');
            !$errors->has('type') ?: $data_error['type'] = $errors->first('type');
            return response()->json([
                'status' => 400,
                'message' => $data_error
            ]);
        }
    }
}