<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/12/2018
 * Time: 3:22 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Likes;


class LikesController extends Controller
{
    public function __construct(){
    }
    public function storeLike(Request $request)
    {
        $valid = Validator::make($request->all(),
            [
                'user_id' => 'required',
                'product_id' => 'required',
                'status' => 'required'
            ],
            [
                'user_id.required' =>'UserID không được rỗng',
                'product_id.required' =>'ProductID không được rỗng',
                'status.required' =>'Trạng thái không được rỗng ',
            ]
        );
        if($valid->passes()) {
            $user_id = $request->input('user_id');
            $product_id = $request->input('product_id');
            $status = $request->input('status');
            $likes = DB::connection('mongodb')->collection('nk_likes')->where('user_id', $user_id)->where('product_id',$product_id)
                ->get(['id','status']);
            if(!empty($likes[0])){ // update
                DB::connection('mongodb')->collection('nk_likes')->where('user_id', $user_id)->where('product_id',$product_id)
                    ->update(['status' => $status]);
                Likes::where('product_id', $product_id)->where('user_id',$user_id)
                    ->update(['status' => $status]);
            }else{ // thêm mới
                $data = array(
                    'user_id' => $user_id,
                    'product_id' => $product_id,
                    'status' => $status,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $rs = DB::connection('mongodb')->collection('nk_likes')->insert($data);
                $flight = new Likes;
                $flight->user_id = $user_id;
                $flight->product_id = $product_id;
                $flight->status = $status;
                $flight->created_at = date('Y-m-d H:i:s');
                $flight->updated_at = date('Y-m-d H:i:s');
                $flight->save();
            }
            $total_like = DB::connection('mongodb')->collection('nk_likes')->where('status', 1)->where('product_id',$product_id)
                ->count();
            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'total_like' => $total_like,
                    'data' => $status,
                )
            );
        }else{
            $errors = $valid->errors();
            $data_error = [];
            !$errors->has('user_id') ?: $data_error['user_id'] = $errors->first('user_id');
            !$errors->has('product_id') ?: $data_error['product_id'] = $errors->first('product_id');
            !$errors->has('status') ?: $data_error['status'] = $errors->first('status');
            return response()->json([
                'status' => 400,
                'message' => $data_error
            ]);
        }
    }
    public function index(Request $request){

        $user_id = (int)$request->input('user_id', 0);
        $limit = $request->input('limit', 10);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $start = ($offset - 1) * $limit;
        $params = $request->all();
        $product_ids=[];
        $data = [];
        $total = DB::connection('mongodb')->collection('nk_likes')->where('user_id', $user_id)->where('status',1)->count();
        $likes = DB::connection('mongodb')->collection('nk_likes')->where('user_id', $user_id)->where('status',1)->get(['product_id']);
        if(!empty($likes)){
            foreach ($likes as $item){
                array_push($product_ids,$item['product_id']);
            }
            if(!empty($product_ids)){
                $select = [
                    'product_id'
                    ,'mobile_short_description'
                    ,'product_code'
                    ,'amount'
                    ,'weight'
                    ,'length'
                    ,'width'
                    ,'height'
                    ,'list_price'
                    ,'price'
                    ,'product'
                    ,'nk_shortname'
                    ,'main_pair'
                    ,'category_ids'
                    ,'nk_tragop_0'
                    ,'product_options'
                    ,'tag_image'
                    ,'nk_is_shock_price'
                    ,'product_features'
                    ,'nk_product_tags'
                    ,'text_shock'
                    ,'shock_online_exp'
                    ,'nk_special_tag'
                    ,'offline_price'
                    ,'model'
                    ,'nk_is_tra_gop'
                    ,'nk_is_add_cart'
                    ,'seo_name'
                    ,'hidden_option'
                    ,'nk_check_total_amount'
                    ,'short_description'
                ];
                $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->whereIn('product_id', $product_ids)->get($select);
                if(!empty($data_products1s)){
                    $datas1 = [];
                    foreach ($data_products1s as $data_products1){
                        $data_products['text_promotion']=[];
                        $data1['images']='';
                        $data1['brand'] = '';
                        $data1['tag_image_top_left'] = '';
                        $data1['tag_image_top_right'] = '';
                        $data1['tag_image_bottom_left'] = '';
                        $data1['tag_image_bottom_right'] = '';
                        $data1['model'] ='';// $data_products1['model'];
                        if($data_products1){
                            if(!empty($data_products1['product_options'])){
                                foreach ($data_products1['product_options'] as $key => $val){
                                    $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                    break;
                                }
                            }
                            if(!empty($data_products1['nk_product_tags'])){
                                foreach ($data_products1['nk_product_tags'] as $key => $val){
                                    if($val['tag_position'] == 'bottom-right'){
                                        $data1['tag_image_bottom_right'] = $val['tag_image'];
                                    }elseif($val['tag_position'] == 'bottom-left'){
                                        $data1['tag_image_bottom_left'] = $val['tag_image'];
                                    }elseif($val['tag_position'] == 'top-right'){
                                        $data1['tag_image_top_right'] = $val['tag_image'];
                                    }elseif($val['tag_position'] == 'top-left'){
                                        $data1['tag_image_top_left'] = $val['tag_image'];
                                    }
                                }
                            }

                            $thumb = $data_products1['main_pair'];
                            $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
                            $data1['images'] = 'https://www.nguyenkim.com/images/thumbnails/250/250/'. $image['relative_path'];

                            if(!empty($data_products1['product_features'])){
                                if(!empty($data_products1['product_features']['591'])){
                                    if(!empty($data_products1['product_features']['591']['subfeatures'])){
                                        if(!empty($data_products1['product_features']['591']['subfeatures']['599'])){
                                            $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                            if(!empty($data_variant['variants'])){
                                                foreach ($data_variant['variants'] as $item){
                                                    $data1['brand'] = $item['variant'];
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if(!empty($data_products1['product_features'])){
                                if(!empty($data_products1['product_features']['591'])){
                                    if(!empty($data_products1['product_features']['591']['subfeatures'])){
                                        if(!empty($data_products1['product_features']['591']['subfeatures']['596'])){
                                            $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                            if(!empty($data_variant['variants'])){
                                                foreach ($data_variant['variants'] as $item){
                                                    $data1['model'] = $item['variant'];
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $data1['product_id'] = $data_products1['product_id'];
                        $data1['description'] = $data_products1['mobile_short_description'];
                        $data1['product_code'] = $data_products1['product_code'];
                        $data1['amount'] = $data_products1['amount'];
                        $data1['weight'] = $data_products1['weight'];
                        $data1['list_price'] = (int)$data_products1['list_price'];
                        $data1['price'] = (int)$data_products1['price'];
                        $data1['offline_price'] = (int)$data_products1['offline_price'];
                        $data1['product_name'] = $data_products1['product'];
                        $data1['shortname'] = $data_products1['nk_shortname'];
                        $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                        $data1['categories'] = $data_products1['category_ids'];
                        $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                        $data1['length'] = $data_products1['length'];
                        $data1['width'] = $data_products1['width'];
                        $data1['height'] = $data_products1['height'];
                        $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                        $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%402x.png';
                        $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                        $data1['text_shock'] = $data_products1['text_shock'];
                        $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                        $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                        $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                        // get like product
                        $data1['is_like'] = 0;
                        if($user_id > 0){
                            $like = DB::connection('mongodb')->collection('nk_likes')->where('product_id', (int)$data_products1['product_id'])->where('user_id',$user_id)->get(['status']);
                            if(!empty($like[0])){
                                $data1['is_like'] = $like[0]['status'];
                            }
                        }
                        $data1['nk_is_tra_gop'] = $data_products1['nk_is_tra_gop'];
                        $data1['nk_is_add_cart'] = $data_products1['nk_is_add_cart'];
                        $data1['seo_name'] = $data_products1['seo_name'];
                        $data1['hidden_option'] = $data_products1['hidden_option'];
                        $data1['nk_check_total_amount'] = $data_products1['nk_check_total_amount'];
                        $data1['short_description'] = $data_products1['short_description'];
                        $data[] =  $data1;
                    }

                }

            }
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data,
                'total' => $total
            )
        );
    }

}