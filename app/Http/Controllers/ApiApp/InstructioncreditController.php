<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:20 PM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;

class InstructioncreditController extends Controller
{
    public function credit(Request $request)
    {
        $site_root = config('constants.site_root');
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'link' => $site_root . 'khuyen-mai-tra-gop-tai-nguyen-kim.html?webview=nkapp'
          //  'link' => $site_root .'index.php?dispatch=nk_tragop_hd.instruction_credit'
        );
    }
    public function atm(Request $request)
    {
        $site_root = config('constants.site_root');
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'link' => $site_root . 'khuyen-mai-tra-gop-tai-nguyen-kim.html?webview=nkapp'
        );
    }
}