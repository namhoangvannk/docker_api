<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/12/2018
 * Time: 3:22 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\DiscussionPosts;
use App\Models\Mongo\ProductsFullModeNew as MProducts;



class CommentsController extends Controller
{

    public function __construct()
    {
    }
    public function storeLike(Request $request)
    {
        $valid = Validator::make($request->all(),
            [
                'user_id' => 'required',
                'product_id' => 'required',
                'status' => 'required'
            ],
            [
                'user_id.required' =>'UserID không được rỗng',
                'product_id.required' =>'ProductID không được rỗng',
                'status.required' =>'Trạng thái không được rỗng ',
            ]
        );
        if($valid->passes()) {
            $user_id = $request->input('user_id');
            $product_id = $request->input('product_id');
            $status = $request->input('status');
            $query = DB::table('nk_likes');
            $query->where('user_id',$user_id);
            $query->where('product_id', $product_id);
            $likes = $query->orderBy('id', 'DESC');
            $likes = $likes->select('id','status')->first();
            if(!empty($likes)){ // update
                DB::table('nk_likes')
                    ->where('user_id', $user_id)
                    ->where('product_id', $product_id)
                    ->update(['status' => $status]);
            }else{ // thêm mới
                $data = array(
                    'user_id' => $user_id,
                    'product_id' => $product_id,
                    'status' => $status,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                DB::table('nk_likes')->insert($data);
            }
            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => [],
                )
            );
        }else{
            $errors = $valid->errors();
            $data_error = [];
            !$errors->has('user_id') ?: $data_error['user_id'] = $errors->first('user_id');
            !$errors->has('product_id') ?: $data_error['product_id'] = $errors->first('product_id');
            !$errors->has('status') ?: $data_error['status'] = $errors->first('status');
            return response()->json([
                'status' => 400,
                'message' => $data_error
            ]);
        }

    }
    public function index(Request $request){
        $product_id = (int)$request->input('product_id');
        $limit = $request->input('limit', 3);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $show = $request->input('show', '');
        $start = ($offset - 1) * $limit;
        $thread_id = 0;
        if($product_id > 0){
            $query = DB::table('cscart_discussion');
            $query->where('object_id', $product_id);
            $query->where('object_type', 'P');
            $thread_id = $query->select('thread_id')->first();
            if(!empty($thread_id)){
                $thread_id = $thread_id->thread_id;
            }
        }
        $thread_data = [];
        $data = [];
        if($thread_id > 0){
            $query = DB::table('cscart_discussion_posts AS p');
            $query->leftJoin ('cscart_discussion_messages AS m', 'p.post_id', '=','m.post_id');
            $query->leftJoin ('cscart_discussion_rating AS r', 'p.post_id', '=','r.post_id');
            $query->where('p.thread_id',$thread_id);
            $query->where('p.status','A');
            $query->where('r.rating_value','>',0);
            //if($show == '')
                $query->where('p.parent_id',0);
            $query = $query->orderBy('p.like_counter', 'DESC');
            $query = $query->orderBy('p.timestamp', 'DESC');
            $query = $query->orderBy('p.parent_id', 'DESC');
            $thread_data =$query->select('p.*','m.message','r.rating_value');
            $total = $query->select('p.*','m.message','r.rating_value')->count();
            if($show  == 'all')
                $thread_data = $thread_data->get();
            else
                $thread_data = $thread_data->skip($start)->take($limit)->get();
            if(!empty($thread_data)){
                if($show == ''){
                    foreach ($thread_data as $value){
                        $datas[$value->post_id]['post_id'] = $value->post_id;
                        $datas[$value->post_id]['thread_id'] = $value->thread_id;
                        $datas[$value->post_id]['name'] = $value->name;
                        $datas[$value->post_id]['timestamp'] = time_ago($value->timestamp);
                        $datas[$value->post_id]['user_id'] = $value->user_id;
                        $datas[$value->post_id]['ip_address'] = $value->ip_address;
                        $datas[$value->post_id]['status'] = $value->status;
                        $datas[$value->post_id]['parent_id'] = $value->parent_id;
                        $datas[$value->post_id]['tree'] = $value->tree;
                        $datas[$value->post_id]['email'] = $value->email;
                        $datas[$value->post_id]['phone'] = $value->phone;
                        $datas[$value->post_id]['name_clone'] = $value->name_clone;
                        $datas[$value->post_id]['like_counter'] = $value->like_counter;
                        $datas[$value->post_id]['user_role'] = $value->user_role;
                        if($value->user_role == 1)
                            $datas[$value->post_id]['post_admin'] = 'Quản trị viên';
                        else
                            $datas[$value->post_id]['post_admin'] = '';
                        $datas[$value->post_id]['count_answer'] = $value->count_answer;
                        $datas[$value->post_id]['message'] = strip_tags($value->message);
                        $datas[$value->post_id]['rating_value'] = $value->rating_value;
                        $datas[$value->post_id]['rating_color'] = show_color_code($value->rating_value);
                        $datas[$value->post_id]['is_payment_order'] = 0;
                        $datas[$value->post_id]['sub_thread']= [];


                        $data = $datas;
                    }
                }else{
                    foreach ($thread_data as $value){
                        if($value->parent_id ==  0){
                            $datas[$value->post_id]['post_id'] = $value->post_id;
                            $datas[$value->post_id]['thread_id'] = $value->thread_id;
                            $datas[$value->post_id]['name'] = $value->name;
                            $datas[$value->post_id]['timestamp'] = time_ago($value->timestamp);
                            $datas[$value->post_id]['user_id'] = $value->user_id;
                            $datas[$value->post_id]['ip_address'] = $value->ip_address;
                            $datas[$value->post_id]['status'] = $value->status;
                            $datas[$value->post_id]['parent_id'] = $value->parent_id;
                            $datas[$value->post_id]['tree'] = $value->tree;
                            $datas[$value->post_id]['email'] = $value->email;
                            $datas[$value->post_id]['phone'] = $value->phone;
                            $datas[$value->post_id]['name_clone'] = $value->name_clone;
                            $datas[$value->post_id]['like_counter'] = $value->like_counter;
                            $datas[$value->post_id]['user_role'] = $value->user_role;
                            if($value->user_role == 1)
                                $datas[$value->post_id]['post_admin'] = 'Quản trị viên';
                            else
                                $datas[$value->post_id]['post_admin'] = '';
                            $datas[$value->post_id]['count_answer'] = $value->count_answer;
                            $datas[$value->post_id]['message'] = strip_tags($value->message);
                            $datas[$value->post_id]['rating_value'] = $value->rating_value;
                            $datas[$value->post_id]['rating_color'] = show_color_code($value->rating_value);
                            $datas[$value->post_id]['is_payment_order'] = 0;
                            $datas[$value->post_id]['sub_thread']= [];

                            $query2 = DB::table('cscart_discussion_posts AS p');
                            $query2->leftJoin ('cscart_discussion_messages AS m', 'p.post_id', '=','m.post_id');
                            $query2->leftJoin ('cscart_discussion_rating AS r', 'p.post_id', '=','r.post_id');
                            $query2->where('p.status','A');
                            $query2->where('r.rating_value','>',0);
                            $query2->where('p.parent_id',$value->post_id);
                            $query2 = $query2->orderBy('p.like_counter', 'DESC');
                            $query2 = $query2->orderBy('p.timestamp', 'DESC');
                            $query2 = $query2->orderBy('p.parent_id', 'DESC');
                            $thread_data2 =$query2->select('p.*','m.message','r.rating_value')->get();
                            if(!empty($thread_data2)){
                                foreach ($thread_data2 as $value2){
                                    $datas2[$value2->post_id]['post_id'] = $value2->post_id;
                                    $datas2[$value2->post_id]['thread_id'] = $value2->thread_id;
                                    $datas2[$value2->post_id]['name'] = $value2->name;
                                    $datas2[$value2->post_id]['timestamp'] = time_ago($value2->timestamp);
                                    $datas2[$value2->post_id]['user_id'] = $value2->user_id;
                                    $datas2[$value2->post_id]['ip_address'] = $value2->ip_address;
                                    $datas2[$value2->post_id]['status'] = $value2->status;
                                    $datas2[$value2->post_id]['parent_id'] = $value2->parent_id;
                                    $datas2[$value2->post_id]['tree'] = $value2->tree;
                                    $datas2[$value2->post_id]['email'] = $value2->email;
                                    $datas2[$value2->post_id]['phone'] = $value2->phone;
                                    $datas2[$value2->post_id]['name_clone'] = $value2->name_clone;
                                    $datas2[$value2->post_id]['like_counter'] = $value2->like_counter;
                                    $datas2[$value2->post_id]['user_role'] = $value2->user_role;
                                    if($value2->user_role == 1)
                                        $datas2[$value2->post_id]['post_admin'] = 'Quản trị viên';
                                    else
                                        $datas2[$value2->post_id]['post_admin'] = '';
                                    $datas2[$value2->post_id]['count_answer'] = $value2->count_answer;
                                    $datas2[$value2->post_id]['message'] = strip_tags($value2->message);
                                    $datas2[$value2->post_id]['rating_value'] = $value2->rating_value;
                                    $datas2[$value2->post_id]['rating_color'] = show_color_code($value2->rating_value);
                                    $datas2[$value2->post_id]['is_payment_order'] = 0;
                                    $datas[$value->post_id]['sub_thread'] = $datas2;
                                }

                            }
                            $data = [$datas];
                        }
                        //$datas[$value->post_id]['sub_thread']= [];

                    }
                    //var_dump($data);die;

                    /*foreach ($thread_data as $value){
                        if($value->parent_id > 0){
                            $parent_id = $value->parent_id;
                            if(!empty($data)){
                                $data1=[];
                                foreach($data as $key => $items){

                                    foreach ($items as $item ) {
                                        if ($item['post_id'] == $parent_id) {
                                            $datas1[$item['post_id']]['sub_thread']['post_id'] = $item['post_id'];
                                            $datas1[$item['post_id']]['sub_thread']['thread_id'] = $item['thread_id'];
                                            $datas1[$item['post_id']]['sub_thread']['name'] = $item['name'];
                                            $datas1[$item['post_id']]['sub_thread']['timestamp'] = time_ago($item['timestamp']);
                                            $datas1[$item['post_id']]['sub_thread']['user_id'] = $item['user_id'];
                                            $datas1[$item['post_id']]['sub_thread']['ip_address'] = $item['ip_address'];
                                            $datas1[$item['post_id']]['sub_thread']['status'] = $item['status'];
                                            $datas1[$item['post_id']]['sub_thread']['parent_id'] = $item['parent_id'];
                                            $datas1[$item['post_id']]['sub_thread']['tree'] = $item['tree'];
                                            $datas1[$item['post_id']]['sub_thread']['email'] = $item['email'];
                                            $datas1[$item['post_id']]['sub_thread']['phone'] = $item['phone'];
                                            $datas1[$item['post_id']]['sub_thread']['name_clone'] = $item['name_clone'];
                                            $datas1[$item['post_id']]['sub_thread']['like_counter'] = $item['like_counter'];
                                            $datas1[$item['post_id']]['sub_thread']['user_role'] = $item['user_role'];
                                            if ($item['user_role'] == 1)
                                                $datas1[$item['post_id']]['sub_thread']['post_admin'] = 'Quản trị viên';
                                            else
                                                $datas1[$item['post_id']]['sub_thread']['post_admin'] = '';
                                            $datas1[$item['post_id']]['sub_thread']['count_answer'] = $item['count_answer'];
                                            $datas1[$item['post_id']]['sub_thread']['message'] = $item['message'];
                                            $datas1[$item['post_id']]['sub_thread']['rating_value'] = $item['rating_value'];
                                            $datas1[$item['post_id']]['sub_thread']['rating_color'] = show_color_code($item['rating_value']);
                                            $datas1[$item['post_id']]['sub_thread']['is_payment_order'] = 0;
                                            //$data1[] = $datas1;
                                        }
                                    }
                                }
                            }
                        }
                    }*/
                    //var_dump($data1);die;

                    // $data = $datas;
                }

            }
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data,
                'total' => $total
            )
        );
    }
    public function rating(Request $request){
        $product_id = (int)$request->input('product_id');
        $thread_id = 0;
        if($product_id > 0){
            $query = DB::table('cscart_discussion');
            $query->where('object_id', $product_id);
            $query->where('object_type', 'P');
            $thread_id = $query->select('thread_id')->first();
            if(!empty($thread_id)){
                $thread_id = $thread_id->thread_id;
            }
        }
        $thread_data = 0;
        if($thread_id > 0){
            $query = DB::table('cscart_discussion_rating AS r');
            $query->leftJoin ('cscart_discussion_posts AS p', 'r.post_id', '=','p.post_id');
            $query->where('r.thread_id',$thread_id);
            $query->where('p.status','A');
            $query->where('r.rating_value','>',0);
            $thread_data =$query->select(DB::raw('ROUND(AVG(r.rating_value),1) AS val'))->first();
            $thread_data = $thread_data->val;

        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $thread_data,
            )
        );

    }
    public function review(Request $request){
        $product_id = (int)$request->input('product_id');
        $thread_id = 0;
        if($product_id > 0){
            $query = DB::table('cscart_discussion');
            $query->where('object_id', $product_id);
            $query->where('object_type', 'P');
            $thread_id = $query->select('thread_id')->first();
            if(!empty($thread_id)){
                $thread_id = $thread_id->thread_id;
            }

        }
        $thread_data = 0;
        if($thread_id > 0){
            $query = DB::table('cscart_discussion_rating AS r');
            $query->leftJoin ('cscart_discussion_posts AS p', 'r.post_id', '=','p.post_id');
            $query->where('r.thread_id',$thread_id);
            $query->where('p.status','A');
            $query->where('r.rating_value','>',0);
            $thread_data =$query->select(DB::raw('COUNT(r.post_id)AS val'))->first();
            $thread_data = $thread_data->val;

        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $thread_data,
            )
        );

    }
    public function storeReview(Request $request)
    {
        $type = $request->input('type') ? $request->input('type') : 0; // 0 là review, 1 là comment
        if($type == 0){
            $valid = Validator::make($request->all(),
                [
                    'product_id' => 'required',
                    'name' => 'required',
                    'message' => 'required',
                    'phone' => 'required',
                    'type' => 'required',
                ],
                [
                    'product_id.required' =>'ProductID không được rỗng',
                    'name.required' =>'Họ tên không được rỗng ',
                    'message.required' =>'Nội dung không được rỗng ',
                    'phone.required' =>'Số điện thoại không được rỗng ',
                    'type.required' =>'Type không được rỗng ',
                ]
            );
        }else{
            $valid = Validator::make($request->all(),
                [
                    'product_id' => 'required',
                    'name' => 'required',
                    'message' => 'required',
                    'type' => 'required',
                ],
                [
                    'product_id.required' =>'ProductID không được rỗng',
                    'name.required' =>'Họ tên không được rỗng ',
                    'message.required' =>'Nội dung không được rỗng ',
                    'type.required' =>'Type không được rỗng ',
                ]
            );
        }
        if($valid->passes()) {
            $user_id = (int)$request->input('user_id');
            $product_id = (int)$request->input('product_id');
            $name = $request->input('name');
            $message = $request->input('message');
            $phone = $request->input('phone');
            $rating = $request->input('rating') ? $request->input('rating'): 0;
            $parent_id = $request->input('parent_id') ? $request->input('parent_id') : 0;

            $query = DB::table('cscart_discussion');
            $query->where('object_id',$product_id);
            if($type == 0)
                $query->where('object_type', 'P');
            else
                $query->where('object_type', 'T');
            $count = $query->select('thread_id')->first();
            $thread_id = 0;
            if(count($count) > 0)
                $thread_id = $count->thread_id;
            $data_discussion['object_id'] = $product_id;
            if($type == 0)
                $data_discussion['object_type'] = 'P';
            else
                $data_discussion['object_type'] = 'T';
            $data_discussion['type'] = 'B';
            $data_discussion['company_id'] = 1;

            if($thread_id == 0){
                // insert cscart_discussion
                $thread_id = DB::table('cscart_discussion')->insertGetId($data_discussion);
            }
            // insert cscart_discussion_posts
            $data_posts['thread_id'] = $thread_id;
            $data_posts['name'] = $name;
            $data_posts['user_id'] = $user_id;
            $data_posts['ip_address'] = $rating;
            $data_posts['status'] = "D";
            $data_posts['timestamp'] = time();
            $data_posts['parent_id'] = $parent_id;
            $data_posts['tree'] = "Y";
            $post_id =DB::table('cscart_discussion_posts')->insertGetId($data_posts);
            // insert cscart_discussion_messages
            $data_messages1['thread_id'] = $thread_id;
            $data_messages1['post_id'] = $post_id;
            $data_messages1['message'] = $message;
            DB::table('cscart_discussion_messages')->insert($data_messages1);
            // insdert cscart_discussion_rating
            $data_rating['thread_id'] = $thread_id;
            $data_rating['post_id'] = $post_id;
            $data_rating['rating_value'] = $rating;
            DB::table('cscart_discussion_rating')->insert($data_rating);

            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => [],
                )
            );
        }else{
            $errors = $valid->errors();
            $data_error = [];
            if($type == 0)
                !$errors->has('phone') ?: $data_error['phone'] = $errors->first('phone');
            !$errors->has('product_id') ?: $data_error['product_id'] = $errors->first('product_id');
            !$errors->has('name') ?: $data_error['name'] = $errors->first('name');
            !$errors->has('message') ?: $data_error['message'] = $errors->first('message');
            return response()->json([
                'status' => 400,
                'message' => $data_error
            ]);
        }
    }
    public function storeReply(Request $request)
    {
        $valid = Validator::make($request->all(),
            [
                'product_id' => 'required',
                'name' => 'required',
                'message' => 'required',
                'parent_id' => 'required',
                'thread_id' => 'required',
            ],
            [
                'product_id.required' =>'ProductID không được rỗng',
                'name.required' =>'Họ tên không được rỗng ',
                'message.required' =>'Nội dung không được rỗng',
                'thread_id.required' =>'Thread ID không được rỗng',
            ]
        );
        if($valid->passes()) {
            $user_id = $request->input('user_id') ? $request->input('user_id') : 0;
            $product_id = (int)$request->input('product_id');
            $name = $request->input('name');
            $message = $request->input('message');
            $phone = '';
            $rating = $request->input('rating') ? $request->input('rating') : 0;
            $parent_id = $request->input('parent_id') ? $request->input('parent_id') : 0;
            $thread_id = (int)$request->input('thread_id');

            // insert cscart_discussion_posts
            $data_posts['thread_id'] = $thread_id;
            $data_posts['name'] = $name;
            $data_posts['user_id'] = $user_id;
            $data_posts['ip_address'] = $rating;
            $data_posts['status'] = "D";
            $data_posts['parent_id'] = $parent_id;
            $data_posts['timestamp'] = time();
            $data_posts['tree'] = "Y";
            $post_id =DB::table('cscart_discussion_posts')->insertGetId($data_posts);

            // insert cscart_discussion_messages
            $data_messages1['thread_id'] = $thread_id;
            $data_messages1['post_id'] = $post_id;
            $data_messages1['message'] = $message;
            DB::table('cscart_discussion_messages')->insert($data_messages1);

            // insdert cscart_discussion_rating
            $data_rating['thread_id'] = $thread_id;
            $data_rating['post_id'] = $post_id;
            $data_rating['rating_value'] = $rating;
            DB::table('cscart_discussion_rating')->insert($data_rating);

            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => [],
                )
            );

        }else{
            $errors = $valid->errors();
            $data_error = [];
            !$errors->has('thread_id') ?: $data_error['thread_id'] = $errors->first('thread_id');
            !$errors->has('product_id') ?: $data_error['product_id'] = $errors->first('product_id');
            !$errors->has('name') ?: $data_error['name'] = $errors->first('name');
            !$errors->has('message') ?: $data_error['message'] = $errors->first('message');
            return response()->json([
                'status' => 400,
                'message' => $data_error
            ]);
        }
    }
    public function likePost(Request $request){
        $valid = Validator::make($request->all(),
            [
                'post_id' => 'required',
                'type' => 'required'
            ],
            [
                'post_id.required' =>'Post ID không được rỗng',
                'type.required' =>'Type không được rỗng',
            ]
        );
        if($valid->passes()) {

            $post_id = $request->input('post_id');
            $type = $request->input('type');
            $query = DB::table('cscart_discussion_posts');
            $query->where('post_id', $post_id);
            $like_counter = $query->select('like_counter')->first();
            $like_counter = (int)$like_counter->like_counter;
            if ($type == 0) {
                $like_counter = $like_counter + 1;
            } else {
                if($like_counter > 0){
                    $like_counter = $like_counter - 1;
                }
            }

            DB::table('cscart_discussion_posts')
                ->where('post_id', $post_id)
                ->update(['like_counter' => $like_counter]);

            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => $like_counter ,
                )
            );
        }else{
            $errors = $valid->errors();
            $data_error = [];
            !$errors->has('post_id') ?: $data_error['post_id'] = $errors->first('post_id');
            !$errors->has('type') ?: $data_error['status'] = $errors->first('type');
            return response()->json([
                'status' => 400,
                'message' => $data_error
            ]);
        }
    }
    public function list_comment(Request $request){
        $product_id = (int)$request->input('product_id');
        $limit = $request->input('limit', 3);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $show = $request->input('show', '');
        $start = ($offset - 1) * $limit;
        $thread_id = 0;
        if($product_id > 0){
            $query = DB::table('cscart_discussion');
            $query->where('object_id', $product_id);
            $query->where('object_type', 'T');
            $thread_id = $query->select('thread_id')->first();
            if(!empty($thread_id)){
                $thread_id = $thread_id->thread_id;
            }
        }
        $thread_data = [];
        $data = [];
        $total = 0;
        if($thread_id > 0){
            $query = DB::table('cscart_discussion_posts AS p');
            $query->leftJoin ('cscart_discussion_messages AS m', 'p.post_id', '=','m.post_id');
            $query->leftJoin ('cscart_discussion_rating AS r', 'p.post_id', '=','r.post_id');
            $query->where('p.thread_id',$thread_id);
            $query->where('p.status','A');
            $query->where('r.rating_value','>',0);
            if($show == '')
                $query->where('p.parent_id',0);
            else
                $query = $query->orderBy('p.like_counter', 'DESC');

            $query = $query->orderBy('p.timestamp', 'DESC');
            $query = $query->orderBy('p.parent_id', 'DESC');
            $thread_data =$query->select('p.*','m.message','r.rating_value');
            $total = $query->select('p.*','m.message','r.rating_value')->count();
            if($show  == 'all')
                $thread_data = $thread_data->get();
            else
                $thread_data = $thread_data->skip($start)->take($limit)->get();
            if(!empty($thread_data)){
                if($show == ''){

                    foreach ($thread_data as $value){
                        $datas[$value->post_id]['post_id'] = $value->post_id;
                        $datas[$value->post_id]['thread_id'] = $value->thread_id;
                        $datas[$value->post_id]['name'] = $value->name;
                        $datas[$value->post_id]['timestamp'] = time_ago($value->timestamp);
                        $datas[$value->post_id]['user_id'] = $value->user_id;
                        $datas[$value->post_id]['ip_address'] = $value->ip_address;
                        $datas[$value->post_id]['status'] = $value->status;
                        $datas[$value->post_id]['parent_id'] = $value->parent_id;
                        $datas[$value->post_id]['tree'] = $value->tree;
                        $datas[$value->post_id]['email'] = $value->email;
                        $datas[$value->post_id]['phone'] = $value->phone;
                        $datas[$value->post_id]['name_clone'] = $value->name_clone;
                        $datas[$value->post_id]['like_counter'] = $value->like_counter;
                        $datas[$value->post_id]['user_role'] = $value->user_role;
                        if($value->user_role == 1)
                            $datas[$value->post_id]['post_admin'] = 'Quản trị viên';
                        else
                            $datas[$value->post_id]['post_admin'] = '';
                        $datas[$value->post_id]['count_answer'] = $value->count_answer;
                        $datas[$value->post_id]['message'] = strip_tags($value->message);
                        $datas[$value->post_id]['rating_value'] = $value->rating_value;
                        $datas[$value->post_id]['rating_color'] = show_color_code($value->rating_value);
                        $datas[$value->post_id]['is_payment_order'] = 0;
                        $datas[$value->post_id]['sub_thread']= [];
                        $data = $datas;
                    }
                }else{
                    foreach ($thread_data as $value){
                        if($value->parent_id ==  0){
                            $datas[$value->post_id]['post_id'] = $value->post_id;
                            $datas[$value->post_id]['thread_id'] = $value->thread_id;
                            $datas[$value->post_id]['name'] = $value->name;
                            $datas[$value->post_id]['timestamp'] = time_ago($value->timestamp);
                            $datas[$value->post_id]['user_id'] = $value->user_id;
                            $datas[$value->post_id]['ip_address'] = $value->ip_address;
                            $datas[$value->post_id]['status'] = $value->status;
                            $datas[$value->post_id]['parent_id'] = $value->parent_id;
                            $datas[$value->post_id]['tree'] = $value->tree;
                            $datas[$value->post_id]['email'] = $value->email;
                            $datas[$value->post_id]['phone'] = $value->phone;
                            $datas[$value->post_id]['name_clone'] = $value->name_clone;
                            $datas[$value->post_id]['like_counter'] = $value->like_counter;
                            $datas[$value->post_id]['user_role'] = $value->user_role;
                            if($value->user_role == 1)
                                $datas[$value->post_id]['post_admin'] = 'Quản trị viên';
                            else
                                $datas[$value->post_id]['post_admin'] = '';
                            $datas[$value->post_id]['count_answer'] = $value->count_answer;
                            $datas[$value->post_id]['message'] = strip_tags($value->message);
                            $datas[$value->post_id]['rating_value'] = $value->rating_value;
                            $datas[$value->post_id]['rating_color'] = show_color_code($value->rating_value);
                            $datas[$value->post_id]['is_payment_order'] = 0;
                            $datas[$value->post_id]['sub_thread']= [];

                            $query2 = DB::table('cscart_discussion_posts AS p');
                            $query2->leftJoin ('cscart_discussion_messages AS m', 'p.post_id', '=','m.post_id');
                            $query2->leftJoin ('cscart_discussion_rating AS r', 'p.post_id', '=','r.post_id');
                            $query2->where('p.status','A');
                            $query2->where('p.parent_id',$value->post_id);
                            $query2 = $query2->orderBy('p.like_counter', 'DESC');
                            $query2 = $query2->orderBy('p.timestamp', 'DESC');
                            $query2 = $query2->orderBy('p.parent_id', 'DESC');
                            $thread_data2 =$query2->select('p.*','m.message','r.rating_value')->get();
                            if(!empty($thread_data2)){
                                foreach ($thread_data2 as $value2){
                                    $datas2[$value->post_id]['post_id'] = $value2->post_id;
                                    $datas2[$value->post_id]['thread_id'] = $value2->thread_id;
                                    $datas2[$value->post_id]['name'] = $value2->name;
                                    $datas2[$value->post_id]['timestamp'] = time_ago($value2->timestamp);
                                    $datas2[$value->post_id]['user_id'] = $value2->user_id;
                                    $datas2[$value->post_id]['ip_address'] = $value2->ip_address;
                                    $datas2[$value->post_id]['status'] = $value2->status;
                                    $datas2[$value->post_id]['parent_id'] = $value2->parent_id;
                                    $datas2[$value->post_id]['tree'] = $value2->tree;
                                    $datas2[$value->post_id]['email'] = $value2->email;
                                    $datas2[$value->post_id]['phone'] = $value2->phone;
                                    $datas2[$value->post_id]['name_clone'] = $value2->name_clone;
                                    $datas2[$value->post_id]['like_counter'] = $value2->like_counter;
                                    $datas2[$value->post_id]['user_role'] = $value2->user_role;
                                    if($value2->user_role == 1)
                                        $datas2[$value->post_id]['post_admin'] = 'Quản trị viên';
                                    else
                                        $datas2[$value->post_id]['post_admin'] = '';
                                    $datas2[$value->post_id]['count_answer'] = $value2->count_answer;
                                    $datas2[$value->post_id]['message'] = strip_tags($value2->message);
                                    $datas2[$value->post_id]['rating_value'] = $value2->rating_value;
                                    $datas2[$value->post_id]['rating_color'] = show_color_code($value2->rating_value);
                                    $datas2[$value->post_id]['is_payment_order'] = 0;
                                    $datas[$value->post_id]['sub_thread'] = $datas2;
                                }

                            }
                            $data[]=$datas;
                        }
                    }
                }

            }
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data,
                'total' => $total
            )
        );
    }
    public function list_review_by_user_id(Request $request){
        $user_id = (int)$request->input('user_id');
        $limit = $request->input('limit', 3);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $start = ($offset - 1) * $limit;
        $data = [];
        $query = DB::table('cscart_discussion_posts AS p');
        $query->leftJoin ('cscart_discussion_messages AS m', 'p.post_id', '=','m.post_id');
        $query->leftJoin ('cscart_discussion_rating AS r', 'p.post_id', '=','r.post_id');
        $query->leftJoin ('cscart_discussion AS d', 'p.thread_id', '=','d.thread_id');
        $query->where('p.user_id',$user_id);
        $query->where('d.object_type','P');
        $query->where('r.rating_value','>',0);
        $query = $query->orderBy('p.like_counter', 'DESC');
        $query = $query->orderBy('p.timestamp', 'DESC');
        $query = $query->orderBy('p.parent_id', 'DESC');
        $thread_data =$query->select('p.*','m.message','r.rating_value','d.object_id');
        $total = $query->select('p.*','m.message','r.rating_value','d.object_id')->count();
        $thread_data = $thread_data->get();

        if(!empty($thread_data)){
            foreach ($thread_data as $value) {
                $datas['post_id'] = $value->post_id;
                $datas['thread_id'] = $value->thread_id;
                $datas['name'] = $value->name;
                $datas['timestamp'] = date('d/m/Y H:i:s',$value->timestamp);
                $datas['user_id'] = $value->user_id;
                $datas['ip_address'] = $value->ip_address;
                if($value->status == 'A')
                    $datas['status'] = 'Đã duyệt';
                else
                    $datas['status'] = 'Đang chờ duyệt';
                $datas['parent_id'] = $value->parent_id;
                $datas['tree'] = $value->tree;
                $datas['email'] = $value->email;
                $datas['phone'] = $value->phone;
                $datas['name_clone'] = $value->name_clone;
                $datas['like_counter'] = $value->like_counter;
                $datas['user_role'] = $value->user_role;
                if ($value->user_role == 1)
                    $datas['post_admin'] = 'Quản trị viên';
                else
                    $datas['post_admin'] = '';
                $datas['count_answer'] = $value->count_answer;
                $datas['message'] = $value->message;
                $datas['rating_value'] = $value->rating_value;
                $datas['rating_color'] = show_color_code($value->rating_value);
                $datas['is_payment_order'] = 0;
                $datas['object_id'] = $value->object_id;
                $product = MProducts::getDetail($value->object_id);
                $datas['image'] = $product['image_thumb'];
                $datas['nk_shortname'] = $product['nk_shortname'];
                $datas['seo_name'] = $product['seo_name'];
                $datas['product_name'] = $product['product_name'];
                $data[] = $datas;
            }
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data,
                'total' => $total
            )
        );
    }
    public function list_comment_by_user_id(Request $request){
        $user_id = (int)$request->input('user_id');
        $limit = $request->input('limit', 3);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $show = $request->input('show', '');
        $start = ($offset - 1) * $limit;
        $data = [];
        $query = DB::table('cscart_discussion_posts AS p');
        $query->leftJoin ('cscart_discussion_messages AS m', 'p.post_id', '=','m.post_id');
        $query->leftJoin ('cscart_discussion_rating AS r', 'p.post_id', '=','r.post_id');
        $query->leftJoin ('cscart_discussion AS d', 'p.thread_id', '=','d.thread_id');
        $query->where('p.user_id',$user_id);
        //$query->where('p.status','A');
        $query->where('d.object_type','T');
        $query->where('r.rating_value','>',0);
        $query = $query->orderBy('p.like_counter', 'DESC');
        $query = $query->orderBy('p.timestamp', 'DESC');
        $query = $query->orderBy('p.parent_id', 'DESC');
        $thread_data =$query->select('p.*','m.message','r.rating_value','d.object_id');
        $total = $query->select('p.*','m.message','r.rating_value','d.object_id')->count();
        $thread_data = $thread_data->get();
        if(!empty($thread_data)){
            foreach ($thread_data as $value) {
                $datas['post_id'] = $value->post_id;
                $datas['thread_id'] = $value->thread_id;
                $datas['name'] = $value->name;
                $datas['timestamp'] = time_ago($value->timestamp);
                $datas['user_id'] = $value->user_id;
                $datas['ip_address'] = $value->ip_address;
                if($value->status == 'A')
                    $datas['status'] = 'Đã duyệt';
                else
                    $datas['status'] = 'Đang chờ duyệt';
                $datas['parent_id'] = $value->parent_id;
                $datas['tree'] = $value->tree;
                $datas['email'] = $value->email;
                $datas['phone'] = $value->phone;
                $datas['name_clone'] = $value->name_clone;
                $datas['like_counter'] = $value->like_counter;
                $datas['user_role'] = $value->user_role;
                if ($value->user_role == 1)
                    $datas['post_admin'] = 'Quản trị viên';
                else
                    $datas['post_admin'] = '';
                $datas['count_answer'] = $value->count_answer;
                $datas['message'] = $value->message;
                $datas['rating_value'] = $value->rating_value;
                $datas['rating_color'] = show_color_code($value->rating_value);
                $datas['is_payment_order'] = 0;
                $datas['object_id'] = $value->object_id;
                $product = MProducts::getDetail($value->object_id);
                $datas['image'] = $product['image_thumb'];
                $datas['nk_shortname'] = $product['nk_shortname'];
                $datas['seo_name'] = $product['seo_name'];
                $datas['product_name'] = $product['product_name'];
                $data[] = $datas;

            }

        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data,
                'total' => $total
            )
        );
    }
}