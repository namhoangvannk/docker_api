<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/14/2018
 * Time: 9:45 AM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use App\Models\Mongo\ProductsFullModeNew;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\ProductRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\Products;
use Mockery\Exception;
use MongoDB\Driver\Manager;
use MongoDB\Driver\BulkWrite;
use Illuminate\Support\Facades\Config;

class ProductController extends Controller
{
    protected $repo;
    protected $configMongodb;

    public function __construct(ProductRepository $repo)
    {
        $this->repo = $repo;
        $config = Config::get('database.connections.mongodb');
        $this->configMongodbURI = "mongodb://".$config['username'].":".$config['password']."@".$config['host'].":".$config['port']."/".$config['database'];
    }
    public function index(Request $request ,$id)
    {
        $data = array (
            0 =>
                array (
                    'product_id' => '3106',
                    'image_path' => 'floor-jujifilm_5htz-wg.jpg',
                    'list_price' => '18190000.00',
                    'price' => '18190000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            1 =>
                array (
                    'product_id' => '14284',
                    'image_path' => '10008092-TỦ-LẠNH-HITACHI-INVERTER-584-LÍT-R-M700GPGV2-01.jpg',
                    'list_price' => '58500000.00',
                    'price' => '57450000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            2 =>
                array (
                    'product_id' => '15160',
                    'image_path' => '10008089-TỦ-LẠNH-HITACHI-INVERTER-589-LÍT-R-S700GPGV2-01.jpg',
                    'list_price' => '52500000.00',
                    'price' => '51450000.00',
                    'nk_shortname' => 'Inverter - 589 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            3 =>
                array (
                    'product_id' => '15325',
                    'image_path' => '10008093-TỦ-LẠNH-HITACHI-INVERTER-584-LÍT-R-M700GPGV2-_GS_-01.jpg',
                    'list_price' => '58500000.00',
                    'price' => '57450000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            4 =>
                array (
                    'product_id' => '15326',
                    'image_path' => '10008088-TỦ-LẠNH-HITACHI-INVERTER-589-LÍT-R-S700GPGV2-01.jpg',
                    'list_price' => '52500000.00',
                    'price' => '51450000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            5 =>
                array (
                    'product_id' => '21091',
                    'image_path' => '10008086-TỦ-LẠNH-HITACHI-584-LÍT-R-M700GPGV2X-_MIR_-01.jpg',
                    'list_price' => '72000000.00',
                    'price' => '70450000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            6 =>
                array (
                    'product_id' => '31053',
                    'image_path' => '10020786-TỦ-LẠNH-HITACHI-INVERTER-584-LÍT-R-M700AGPGV4X-_DIA_-01_dac2-nx.jpg',
                    'list_price' => '80000000.00',
                    'price' => '70000000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            7 =>
                array (
                    'product_id' => '39372',
                    'image_path' => '10024625-TỦ-LẠNH-HITACHI-R-E5000V-_XK_-01.jpg',
                    'list_price' => '63000000.00',
                    'price' => '60500000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            8 =>
                array (
                    'product_id' => '39374',
                    'image_path' => '10024626-TỦ-LẠNH-HITACHI-R-E5000V-_XT_-01.jpg',
                    'list_price' => '63000000.00',
                    'price' => '63000000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            9 =>
                array (
                    'product_id' => '39374',
                    'image_path' => 'Khuyenmai-page-web-uudaitphcm.jpg',
                    'list_price' => '63000000.00',
                    'price' => '63000000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            10 =>
                array (
                    'product_id' => '39374',
                    'image_path' => 'Khuyenmai-page-phone-uudaitphcm.jpg',
                    'list_price' => '63000000.00',
                    'price' => '63000000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            11 =>
                array (
                    'product_id' => '39874',
                    'image_path' => '10020328-TỦ-LẠNH-MITSUBISHI-ELECTRIC-694-LÍT-MR-WX71Y-02.jpg',
                    'list_price' => '119000000.00',
                    'price' => '119000000.00',
                    'nk_shortname' => 'Inverter - 694 lít - Ngăn đá dưới',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
        );
        $data_premium = array (
            0 =>
                array (
                    'product_id' => '31053',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap1_%402x-16032018.png',
                    'list_price' => '80000000.00',
                    'price' => '70000000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            1 =>
                array (
                    'product_id' => '39944',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap3_%402x.png',
                    'list_price' => '74000000.00',
                    'price' => '51900000.00',
                    'nk_shortname' => 'Inverter - 470 lít - Ngăn đá dưới',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            2 =>
                array (
                    'product_id' => '47064',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap4_%402x.png',
                    'list_price' => '19900000.00',
                    'price' => '19400000.00',
                    'nk_shortname' => 'Inverter - 600 lít - Ngăn đá trên',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            4 =>
                array (
                    'product_id' => '49738',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap2_%402x.png',
                    'list_price' => '48900000.00',
                    'price' => '37400000.00',
                    'nk_shortname' => 'Inverter - 601 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            5 =>
                array (
                    'product_id' => '54096',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap5_%402x-17h-16032018.png',
                    'list_price' => '21000000.00',
                    'price' => '17900000.00',
                    'nk_shortname' => 'Inverter - 456 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                )

        );
        return $this->respond(
            $this->repo->all($request,$id)
        );
    }
    public function premium(Request $request ,$id)
    {
        $data = array (
            0 =>
                array (
                    'product_id' => '3106',
                    'image_path' => 'floor-jujifilm_5htz-wg.jpg',
                    'list_price' => '18190000.00',
                    'price' => '18190000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            1 =>
                array (
                    'product_id' => '14284',
                    'image_path' => '10008092-TỦ-LẠNH-HITACHI-INVERTER-584-LÍT-R-M700GPGV2-01.jpg',
                    'list_price' => '58500000.00',
                    'price' => '57450000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            2 =>
                array (
                    'product_id' => '15160',
                    'image_path' => '10008089-TỦ-LẠNH-HITACHI-INVERTER-589-LÍT-R-S700GPGV2-01.jpg',
                    'list_price' => '52500000.00',
                    'price' => '51450000.00',
                    'nk_shortname' => 'Inverter - 589 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            3 =>
                array (
                    'product_id' => '15325',
                    'image_path' => '10008093-TỦ-LẠNH-HITACHI-INVERTER-584-LÍT-R-M700GPGV2-_GS_-01.jpg',
                    'list_price' => '58500000.00',
                    'price' => '57450000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            4 =>
                array (
                    'product_id' => '15326',
                    'image_path' => '10008088-TỦ-LẠNH-HITACHI-INVERTER-589-LÍT-R-S700GPGV2-01.jpg',
                    'list_price' => '52500000.00',
                    'price' => '51450000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            5 =>
                array (
                    'product_id' => '21091',
                    'image_path' => '10008086-TỦ-LẠNH-HITACHI-584-LÍT-R-M700GPGV2X-_MIR_-01.jpg',
                    'list_price' => '72000000.00',
                    'price' => '70450000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            6 =>
                array (
                    'product_id' => '31053',
                    'image_path' => '10020786-TỦ-LẠNH-HITACHI-INVERTER-584-LÍT-R-M700AGPGV4X-_DIA_-01_dac2-nx.jpg',
                    'list_price' => '80000000.00',
                    'price' => '70000000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            7 =>
                array (
                    'product_id' => '39372',
                    'image_path' => '10024625-TỦ-LẠNH-HITACHI-R-E5000V-_XK_-01.jpg',
                    'list_price' => '63000000.00',
                    'price' => '60500000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            8 =>
                array (
                    'product_id' => '39374',
                    'image_path' => '10024626-TỦ-LẠNH-HITACHI-R-E5000V-_XT_-01.jpg',
                    'list_price' => '63000000.00',
                    'price' => '63000000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            9 =>
                array (
                    'product_id' => '39374',
                    'image_path' => 'Khuyenmai-page-web-uudaitphcm.jpg',
                    'list_price' => '63000000.00',
                    'price' => '63000000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            10 =>
                array (
                    'product_id' => '39374',
                    'image_path' => 'Khuyenmai-page-phone-uudaitphcm.jpg',
                    'list_price' => '63000000.00',
                    'price' => '63000000.00',
                    'nk_shortname' => 'Tủ lạnh',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            11 =>
                array (
                    'product_id' => '39874',
                    'image_path' => '10020328-TỦ-LẠNH-MITSUBISHI-ELECTRIC-694-LÍT-MR-WX71Y-02.jpg',
                    'list_price' => '119000000.00',
                    'price' => '119000000.00',
                    'nk_shortname' => 'Inverter - 694 lít - Ngăn đá dưới',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
        );
        $data_premium = array (
            0 =>
                array (
                    'product_id' => '31053',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap1_%402x-16032018.png',
                    'list_price' => '80000000.00',
                    'price' => '70000000.00',
                    'nk_shortname' => 'Inverter - 584 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            1 =>
                array (
                    'product_id' => '39944',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap3_%402x.png',
                    'list_price' => '74000000.00',
                    'price' => '51900000.00',
                    'nk_shortname' => 'Inverter - 470 lít - Ngăn đá dưới',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            2 =>
                array (
                    'product_id' => '47064',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap4_%402x.png',
                    'list_price' => '19900000.00',
                    'price' => '19400000.00',
                    'nk_shortname' => 'Inverter - 600 lít - Ngăn đá trên',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            4 =>
                array (
                    'product_id' => '49738',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap2_%402x.png',
                    'list_price' => '48900000.00',
                    'price' => '37400000.00',
                    'nk_shortname' => 'Inverter - 601 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                ),
            5 =>
                array (
                    'product_id' => '54096',
                    'image_path' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap5_%402x-17h-16032018.png',
                    'list_price' => '21000000.00',
                    'price' => '17900000.00',
                    'nk_shortname' => 'Inverter - 456 lít - Side by side',
                    'nk_is_tra_gop' => '1',
                    'status' => 'A',
                    'brand' => 'brand',
                )

        );
        return $this->respond(
            $this->repo->premium($request,$id)
        );
    }
    public function show(Request $request ,$id,$user_id)
    {
        return $this->respond(
            $this->repo->show($request,$id,$user_id)
        );
    }
    public function GetListProduct(Request $request){
        return $this->respond(
            $this->repo->GetListProduct($request)
        );
    }
    public function storeProductFullMode(Request $request)
    {
        $params_list = $request->all();
        $data = $params_list['data'];
        $product_id = $params_list['product_id'];
        $product_ids = array_values($product_id);
        try {
            //file_put_contents('log.log', var_export($product_ids, truel), FILE_APPEND);
            $rep = DB::connection('mongodb')->collection('products_full_mode_new')->whereIn('product_id' , $product_ids)->delete();
            //if($rep){
                $result = DB::connection('mongodb')->collection('products_full_mode_new')->insert($data);
                Products::whereIn('product_id', $product_ids)->update(['sync_mongo' => 1]);
            //}
            return [
                'code'=>200,
                'status'=>true,
                'msg'=>'store product to mongo success'
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }


    public function additionFieldsInsertProduct(&$product,$is_create=true){
        if($is_create){

            $product['text_promotion']      = '';
            $product['text_installment']    = '';
            $product['image_urls']          = '';
            $product['categories']          = [];
            $product['discount_stickers']   = [];
            $product['warehouse_stock']     = [];
            $product['warehouse_sale']      = 0;
            $data_brand = $this->getListProductBrand(implode(',',[$product['product_id']]));
            $product['brand']               = '';
            if(!empty($data_brand)){
                $product['brand']           = isset($data_brand[0]['brand'])? $data_brand[0]['brand'] : '';
            }

        } else {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://dev.api.nguyenkimonline.com/v1/SyncAdditionDataFeed?chunk=1&pids=".$product['product_id'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
        }

    }
    // Sync Brand from cscart
    public function syncBrandProductFullMode(Request $request){
        $input = $request->all();
        try {
            if(!empty($input['product_id'])){
                $ids = array();
                $product_ids = !empty($input['product_id']) ? $input['product_id'] : '';
                if (!empty($product_ids)) {
                    $pos = strpos($product_ids, ',');
                    if ($pos === false) {
                        $ids[] = (int)$product_ids;
                    } else {
                        $p_ids = explode(',', $product_ids);
                        if ($p_ids) {
                            foreach ($p_ids as $id) {
                                $ids[] = (int)$id;
                            }
                        }
                    }
                }
                if(!empty($ids)){
                    $data_brand = $this->getListProductBrand(implode(',',$ids));
                    $updated = array();
                    if(!empty($data_brand)){
                        foreach (array_chunk($data_brand,100) as $chunk_brand){
                            foreach ($chunk_brand as $product){
                                $result = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id',$product['product_id'])->update(array('brand'=>$product['brand']));
                                if($result){
                                    array_push($updated,$product['product_id']);
                                }
                            }
                            sleep(0.1);
                        }
                        return [
                            'code'      => 200,
                            'status'    => true,
                            'msg'       => 'synced product brand success',
                            'pids'      => implode(',',$updated)
                        ];
                    }
                }
            }
            return [
                'code'      => 200,
                'status'    => true,
                'msg'       => 'Product not exits!'
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }

    public function syncAdditionDataFeed(Request $request){
        $input          = $request->all();
        $limit          = isset($input['chunk'])? intval($input['chunk']): 10;
        $pids           = isset($input['pids'])? $input['pids']: '';
        $exist_fields   = isset($input['exist_fields'])? $input['exist_fields']: '';
        $select_fields  = array(
            'product_id',
            'product_options',
            'category_ids',
            'main_pair',
            'details_layout',
            'price',
            'nk_product_tags',
            'warehouse_stock',
            'warehouse_sale'
        );
        // params pids=1,2,3
        if (!empty($pids)) {
            $pos = strpos($pids, ',');
            if ($pos === false) {
                $ids[] = (int)$pids;
            } else {
                $p_ids = explode(',', $pids);
                if ($p_ids) {
                    foreach ($p_ids as $id) {
                        $ids[] = (int)$id;
                    }
                }
            }
            $pids = $ids;
        }

        // params exist_fields=a,b,c
        if (!empty($exist_fields)) {
            $pos = strpos($exist_fields, ',');
            if ($pos === false) {
                $fields[] = $exist_fields;
            } else {
                $field_ds = explode(',', $exist_fields);
                if ($field_ds) {
                    foreach ($field_ds as $field) {
                        $fields[] = $field;
                    }
                }
            }
            $exist_fields = $fields;
        }

        // params reset=1
        if(isset($input['reset'])){
            $result = DB::connection('mongodb')->collection('products_full_mode_new')->update(array('sync_mongo'=> 0));
            if(!empty($result)){
                return array(
                    'status'=> true,
                    'msg'   => 'products_full_mode_new has been reset sync_mongo = 0'
                );
            }
        }

        if(!empty($exist_fields)){
            $or_conditions = array();
            foreach ($exist_fields as $field){
                array_push($or_conditions,array("$field"
                    => array('$exists' => false))
                );
            }
        }

        if(!empty($pids)){
            $or_conditions = array();
            array_push($or_conditions,array(
                'product_id'=> array('$in'=>$pids)
            ));
        }

        if(isset($input['last_sync_time'])){
            $or_conditions = array();
            $last_sync_time_con = time()-((isset($input['last_sync_time'])? intval($input['last_sync_time']): 10)*60);
            array_push($or_conditions,array('last_sync_time'
                => array('$exists' => false))
            );
            array_push($or_conditions,array(
                'last_sync_time' => array('$lte'    => $last_sync_time_con))
            );
        }

        $rawQuery = array();
        if(isset($or_conditions)){
            $rawQuery['$or']= $or_conditions;
        } else {
            $rawQuery['sync_mongo'] = array('$eq'=>0);
        }

        $products_sync = ProductsFullModeNew::whereRaw($rawQuery)->take($limit)->get($select_fields);
        $total         = ProductsFullModeNew::whereRaw($rawQuery)->count();

        $array_synced = array();
        if(!empty($products_sync)){
            $products_sync = $products_sync->toArray();
            foreach (array_chunk($products_sync,10) as $products){
                foreach ($products as $product){
                    $array_update = array();
                    $result = $this->updateAdditionDataFeed($array_update,$product);
                    if(!empty($result)){
                        array_push($array_synced,$array_update);
                    }
                }
            }
        }

        return array(
            'status'        => 200,
            'total_count'   => $total,
            'total_synced'  => count($array_synced),
            'synced'        => $array_synced
        );
    }

    public function extractProductImageLink($product){
        if (empty($product['main_pair']['detailed']['image_path'])
            && empty($product['main_pair']['icon']['image_path'])) return "";
        $image_path = !empty($product['main_pair']['icon']['image_path']) ? $product['main_pair']['icon']['image_path'] : $product['main_pair']['detailed']['image_path'];

        if(isset($product['details_layout'])
            && $product['details_layout'] == 'nk_template_2016_premium'
            && !empty(!empty($product['main_pair']['icon']['image_path']))
        ){
            return $image_path;
        }

        if(empty($image_path)){
            return "";
        }
        $image_path = str_replace('https://www.nguyenkim.com','https://cdn.nguyenkimmall.com',$image_path);
        return $image_path;
    }

    public function extractProductCategories($category_ids){
        $data = DB::select("SELECT t1.category_id, t2.category, t3.`name` AS 'seo_name' FROM cscart_categories t1 LEFT JOIN cscart_category_descriptions t2 ON t1.category_id = t2.category_id AND t2.lang_code = 'vi' LEFT JOIN cscart_seo_names t3 ON t3.object_id = t1.category_id AND t3.type = 'c' AND t3.lang_code = 'vi' WHERE t1.category_id IN({$category_ids})");
        return json_decode(json_encode($data),true);
    }

    public function extractProductTextPromotion($product){
        $text_promotion = '';
        if(!empty($product['product_options'])){
            foreach($product['product_options'] as $option){
                if(isset($option['nk_uu_dai_len_den']) && !empty($option['nk_uu_dai_len_den'])){
                    $text_promotion .= $option['nk_uu_dai_len_den'].',';
                }
            }
        }
        return $text_promotion;
    }


    public function extractProductTextInstallment($product){
        $TextInstallment = '';
        if(isset($product['price']) && $product['price'] >= 3000000){
            $price_installment = ($product['price']/6);
            $TextInstallment = 'Trả góp chỉ '.number_format($price_installment, 0, ',', '.').' đ /tháng (6 tháng)';
        } else {
            $TextInstallment = 'Trả góp thủ tục đơn giản, lãi suất thấp';
        }
        return $TextInstallment;
    }

    public function extractProductDiscountStickers($productTags){
        $data = [];
        if ( !empty($productTags)) {
            foreach ($productTags as $value) {
                if(isset($value['tag_image']) && !empty($value['tag_image'])){
                    $data[] = [
                        "position"  => $value['tag_position'],
                        "image"     => $value['tag_image'],
                        "starttime" => date("Y-m-d H:i:s", $value['tag_start_time']),
                        "endtime"   => date("Y-m-d H:i:s", $value['tag_end_time']),
                    ];
                }
                continue;
            }
        }

        return $data;
    }

    public function updateAdditionDataFeed(&$array_update,&$product){
        $array_update['product_id']         = $product['product_id'];
        $array_update['sync_mongo']         = 1;
        $array_update['last_sync_time']     = time();
        if(isset($_REQUEST['only_stock'])) {
            if(isset($product['warehouse_stock'])){
                $warehouse_stock = $this->getStockByProduct($product['product_id']);
                $array_update['warehouse_stock'] = $warehouse_stock;
                $count_warehouse_sale = 0;
                if(!empty($warehouse_stock)){
                    foreach ($warehouse_stock as $sitecodes){
                        if(!empty($sitecodes)){
                            foreach ($sitecodes as $siteloc){
                                if(isset($siteloc['type'],$siteloc['qty']) && $siteloc['type'] == 1 && $siteloc['qty'] > 0){
                                    $count_warehouse_sale += 1;
                                }
                            }
                        }
                    }
                }
                $array_update['warehouse_sale']  = $count_warehouse_sale;
            } else {
                $array_update['warehouse_stock'] = [];
                $array_update['warehouse_sale']  = 0;
            }
            $result = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id',$product['product_id'])->update($array_update);
            $array_update['msg'] = 'synced';
            return $result;
        } else {
            $image_urls = $this->extractProductImageLink($product);
            if (!empty($image_urls)) {
                $array_update['image_urls'] = $image_urls;
            } else {
                $array_update['image_urls'] = '';
            }
            if (isset($product['category_ids'])) {
                $categories = $this->extractProductCategories(implode(',', $product['category_ids']));
                if (!empty($categories)) {
                    $array_update['categories'] = $categories;
                } else {
                    $array_update['categories'] = [];
                }
            } else {
                $array_update['categories'] = [];
                $array_update['category_ids'] = [];
            }
            $text_promotion = $this->extractProductTextPromotion($product);
            if (!empty($text_promotion)) {
                $array_update['text_promotion'] = $text_promotion;
            } else {
                $array_update['text_promotion'] = '';
            }
            $text_installment = $this->extractProductTextInstallment($product);
            if (!empty($text_installment)) {
                $array_update['text_installment'] = $text_installment;
            } else {
                $array_update['text_installment'] = '';
            }
            if (isset($product['nk_product_tags'])) {
                $discount_stickers = $this->extractProductDiscountStickers($product['nk_product_tags']);
                $array_update['discount_stickers'] = $discount_stickers;
            } else {
                $array_update['discount_stickers'] = [];
                $array_update['nk_product_tags'] = [];
            }
            $data_brand = $this->getListProductBrand(implode(',', [$product['product_id']]));
            if (!empty($data_brand)) {
                $array_update['brand'] = isset($data_brand[0]['brand']) ? $data_brand[0]['brand'] : '';
            } else {
                $array_update['brand'] = '';
            }

            if (isset($product['warehouse_stock'])) {
                $warehouse_stock = $this->getStockByProduct($product['product_id']);
                $array_update['warehouse_stock'] = $warehouse_stock;
                $count_warehouse_sale = 0;
                if (!empty($warehouse_stock)) {
                    foreach ($warehouse_stock as $sitecodes) {
                        if (!empty($sitecodes)) {
                            foreach ($sitecodes as $siteloc) {
                                if (isset($siteloc['type'], $siteloc['qty']) && $siteloc['type'] == 1 && $siteloc['qty'] > 0) {
                                    $count_warehouse_sale += 1;
                                }
                            }
                        }
                    }
                }
                $array_update['warehouse_sale'] = $count_warehouse_sale;
            } else {
                $array_update['warehouse_stock'] = [];
                $array_update['warehouse_sale'] = 0;
            }

            $result = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', $product['product_id'])->update($array_update);
            $array_update['msg'] = 'synced';
            return $result;
        }
    }

    public function cronSyncStocks2MongoAll(Request $request){
        set_time_limit(1800);
        $pids = array();
        $input = $request->all();
        $product_ids = !empty($input['pids']) ? $input['pids'] : '';
        $chunk       = !empty($input['chunk']) ? intval($input['chunk']) : 10;
        if (!empty($product_ids)) {
            $pos = strpos($product_ids, ',');
            if ($pos === false) {
                $pids[] = (int)$product_ids;
            } else {
                $prod_ids = explode(',', $product_ids);
                if ($prod_ids) {
                    foreach ($prod_ids as $id) {
                        $pids[] = (int)$id;
                    }
                }
            }
        } else {
            $data     = DB::select("SELECT DISTINCT product_id FROM cscart_nk_product_stocks_loc WHERE amount > 0");
            if(!empty($data)){
                $products = json_decode(json_encode($data),true);
                $pids     = array_column($products,'product_id');
            }

        }
        $bulk = new BulkWrite(['ordered' => true]);
        foreach (array_chunk($pids,$chunk) as $chunk_pid) {
            foreach ($chunk_pid as $product_id) {
                $array_update = array();
                $array_update['product_id'] = intval($product_id);
                $array_update['sync_mongo'] = 1;
                $array_update['last_sync_time'] = time();
                $warehouse_stock = $this->getStockByProduct(intval($product_id));
                if (!empty($warehouse_stock)) {
                    $array_update['warehouse_stock'] = $warehouse_stock;
                    $count_warehouse_sale = 0;
                    if (!empty($warehouse_stock)) {
                        foreach ($warehouse_stock as $sitecodes) {
                            if (!empty($sitecodes)) {
                                foreach ($sitecodes as $siteloc) {
                                    if (isset($siteloc['type'], $siteloc['qty']) && $siteloc['type'] == 1 && $siteloc['qty'] > 0) {
                                        $count_warehouse_sale += 1;
                                    }
                                }
                            }
                        }
                    }
                    $array_update['warehouse_sale'] = $count_warehouse_sale;
                } else {
                    $array_update['warehouse_stock'] = [];
                    $array_update['warehouse_sale'] = 1;
                }
                // Bulk Update
                $bulk->update(
                    ['product_id' => $array_update['product_id']],
                    ['$set' => $array_update],
                    ['multi' => false, 'upsert' => true]
                );

            }
        }
        $manager = new Manager($this->configMongodbURI);
        $result = $manager->executeBulkWrite('db.products_full_mode_new',$bulk,null);
        return array(
            'status'        => 200,
            'total_count'   => count($pids),
            'total_synced'  => $result->getModifiedCount()
        );
    }
    public function getStockByProduct($product_id){
        $output = array();
        $datas = DB::table('cscart_nk_product_stocks_loc AS t1')
            ->join('cscart_nk_site_loc AS t2', function ($join) {
                $join->on('t1.loc_code', '=', 't2.siteLocId');
                $join->on('t1.warehouse_code', '=', 't2.siteCode');
            })
            ->where('t1.product_id', $product_id)->select('t1.*', 't2.status', 't2.siteLocDesc')->get();
        if ($datas) {
            foreach ($datas as $data) {
                $output[$data->warehouse_code][$data->loc_code] = array(
                    'type' => $data->status,
                    'qty' => $data->amount
                );
            }
        }
        return $output;
    }


    public function storeProductFullMode3(Request $request){
        $params = $request->all();
        try {
            if(!empty($params)){
                foreach($params as $key => $val){
                    $check = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id',$val['product_id'])->get(['product_id'])->first();
                    if($check){
                        unset($val['product_features']);
                        $result = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id',$val['product_id'])
                            ->update($val);
                        Products::where('product_id', $val['product_id'])
                            ->update(['sync_mongo' => 1]);
                    }else{
                        $result = DB::connection('mongodb')->collection('products_full_mode_new')->insert($val);
                        Products::where('product_id', $val['product_id'])
                            ->update(['sync_mongo' => 1]);
                    }
                }
            }
            return [
                'code'=>200,
                'status'=>true,
                'msg'=>'store product to mongo success'
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }
    public function storeProductFullMode3Feature(Request $request){
        $params = $request->all();
        try {
            if(!empty($params)){
                foreach($params as $key => $val){

                    $check = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id',(int)$val['product_id'])->first();
                    if($check){
                        $result = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id',(int)$val['product_id'])->update(['product_features' => $val['product_features']]);
                        Products::where('product_id', $val['product_id'])
                            ->update(['sync_mongo_feature' => 1]);
                    }
                }
            }
            return [
                'code'=>200,
                'status'=>true,
                'msg'=>'store product feature to mongo success'
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }
    public function storeCateFullMode(Request $request){
        $params = $request->all();
        $validator = Validator::make($params,[
            'category_id'=>['required'],
            'parent_id'=>['required']
        ]);
        $validator->after(function ($validator) {
            if ($validator->errors()->isEmpty()) {
                $data = $validator->getData();
                $check = DB::connection('mongodb')->collection('category_full_mode')->where('category_id',$data['category_id'])->get(['category_id'])->first();
                if($check){
                    $validator->errors()->add('CategoryExits', 'Category '.$check['category_id'].' has been created in Mongo');
                }
            }
        });
        if($validator->fails()){
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }
        try {
            $result = DB::connection('mongodb')->collection('category_full_mode')->insert($params);
            return [
                'code'=>200,
                'status'=>true,
                'msg'=>'store cate to mongo success'
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }
    public function getProductFullMode(Request $request, $product_id){
        try {
            $result = DB::connection('mongodb')->collection('products_full_mode')->where('product_id', (int)$product_id)->get()->toArray();
            return [
                'code'=>200,
                'status'=>true,
                'data'=>$result
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }
    public function barcode(Request $request ,$slug)
    {
        return $this->respond(
            $this->repo->barcode($request,$slug)
        );
    }
    public function checkqrcode(Request $request)
    {
        return $this->respond(
            $this->repo->checkqrcode($request)
        );
    }
    public function search(Request $request)
    {
        return $this->respond(
            $this->repo->search($request)
        );
    }
    public function count_duplication(Request $request)
    {
        $arr =[];
        $data = DB::connection('mongodb')->collection('products_full_mode_new')->distinct('product_id')->get();
        foreach ($data as $key => $item){
            array_push($arr,$item);
            Products::where('product_id', $item)
                ->update(['duplication' => 1]);
        }
    }
    public function compare(Request $request){
        return $this->respond(
            $this->repo->compare($request)
        );
    }
    public function compare1(Request $request){
        return $this->respond(
            $this->repo->compare1($request)
        );
    }
    public function getbrand(Request $request)
    {
        return $this->respond(
            $this->repo->getbrand($request)
        );
    }

    public function getListProductBrand($product_ids){
        $data = DB::select("SELECT v.product_id, vd.variant AS 'brand' FROM cscart_product_features AS f LEFT JOIN cscart_product_features_values AS v ON v.feature_id = f.feature_id LEFT JOIN cscart_product_features_descriptions AS fd ON fd.feature_id = v.feature_id AND fd.lang_code = 'vi' JOIN cscart_product_feature_variants fv ON fv.variant_id = v.variant_id JOIN cscart_product_feature_variant_descriptions AS vd ON vd.variant_id = fv.variant_id AND vd.lang_code = 'vi' LEFT JOIN cscart_product_features AS gf ON gf.feature_id = f.parent_id AND gf.feature_type = 'G' JOIN cscart_product_descriptions pd ON pd.product_id = v.product_id AND pd.lang_code = 'vi' WHERE f.`status` IN( 'A') AND v.product_id IN ({$product_ids}) AND f.feature_id = 599 AND IF ( f.parent_id, ( SELECT `status` FROM cscart_product_features AS df WHERE df.feature_id = f.parent_id ), 'A' ) IN ( 'A' ) AND ( v.variant_id != 0 OR ( f.feature_type != 'C' AND v.`value` != '' ) OR ( f.feature_type = 'C' ) OR v.value_int != '' ) AND v.lang_code = 'vi' ORDER BY fd.description, fv.position");
        return json_decode(json_encode($data),true);
    }
    public function storeProductFullModeNew(Request $request){
        $params_list = $request->all();
        try {
            foreach($params_list as $key => $params){
                $check = DB::connection('mongodb')->collection('products_full_mode_new3')->where('product_id',$params['product_id'])->get(['product_id'])->first();
                if($check){
                    $result = DB::connection('mongodb')->collection('products_full_mode_new3')->where('product_id',$params['product_id'])
                        ->update($params);
                    $this->additionFieldsInsertProduct($params,false);
                    Products::where('product_id', $params['product_id'])
                        ->update(['sync_mongo' => 1]);
                }else{
                    $this->additionFieldsInsertProduct($params,true);
                    $result = DB::connection('mongodb')->collection('products_full_mode_new3')->insert($params);
                    $this->additionFieldsInsertProduct($params,false);
                    Products::where('product_id', $params['product_id'])
                        ->update(['sync_mongo' => 1]);
                }
            }
            return [
                'code'=>200,
                'status'=>true,
                'msg'=>'store product to mongo success'
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }
    public function giadung(Request $request)
    {
        return $this->respond(
            $this->repo->giadung($request)
        );
    }
}