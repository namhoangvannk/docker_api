<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 7/13/2018
 * Time: 10:51 AM
 */
namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;

class DeeplinkController extends Controller
{
    public function __construct(){
    }
    // Danh sách
    public function index(Request $request)
    {
        //$str = 'https://beta.nguyenkim.com/tim-kiem.html?cid=&q=tivi&subcats=Y&pcode_from_q=Y&pshort=Y&pfull=Y&pname=Y&pkeywords=Y&search_performed=Y';
        //$str = "https://beta.nguyenkim.com/samsung-galaxy-j7-pro-vi.html?pid=55160#op=30916&var=39092";
        //$str ="https://www.nguyenkim.com/dien-thoai-di-dong-nokia/";
        //$str ="https://beta.nguyenkim.com/iphone-8-plus-256gb.html?pid=59275#op=35077&var=48147";
        $str = $request->input('url');
        $arr = explode("/",$str);
        $slug = $arr[3];
        $pid = 0;
        if(strlen(strstr($slug, 'pid='))){
            $arr_pid = explode("pid=",$str);
            $pid = (int)$arr_pid[1];
        }

        if (strlen(strstr($slug, '.html')) > 0){ // check string .html
            $arr1 = explode(".html",$slug);
            $slug = $arr1[0];
        }elseif(strlen(strstr($slug, '?')) > 0){ // check string ?
            $arr2 = explode("?",$slug);
            $slug = $arr2[0];
        }
        $data = [];
        $status = 404;
        $message = 'Dữ liêụ không tồn tại';
        if(!empty($slug)){
            $query = DB::table('cscart_seo_names');
            if($pid == 0){
                $query->where('name',$slug);
                $query->orWhere('name_master',$slug);
            }else{
                $query->Where('object_id',$pid);
                $query->Where('master_url','Y');
                $query->where('path','<>','');
            }

            //$query->Where('is_default','Y');
            $query->where('lang_code','vi');
            $res = $query->select('type', 'object_id' , 'dispatch' , 'master_url')->get();
            if(!$res->isEmpty()){
                //$arr_browser = ['a','l','m','u','b','e'];
                $data['type'] = $res[0]->type;
                $data['object_id'] = (int)$res[0]->object_id;
                $data['dispatch'] = $res[0]->dispatch;
                $data['master_url'] = $res[0]->master_url;
                $status = 200;
                $message = "Thành công";
            }
        }
        return response()->json(
            array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            )
        );
    }

}