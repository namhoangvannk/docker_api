<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/12/2018
 * Time: 3:22 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class MenuController extends Controller
{
    public function __construct(){
    }
    public function index(Request $request)
    {
        $data = config('constants.menu_config');
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data,
            )
        );
    }
}