<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:19 PM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\AddressRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;

class AddressController extends Controller
{
    protected $repo;

    public function __construct(AddressRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getStates(Request $request)
    {
        return $this->respond(
            $this->repo->getStates($request)
        );
    }
    public function getWards(Request $request)
    {
        return $this->respond(
            $this->repo->getWards($request)
        );
    }

    public function getDistricts(Request $request)
    {
        return $this->respond(
            $this->repo->getDistricts($request)
        );
    }

    public function getDistrictsByState(Request $request, $state_code)
    {
        return $this->respond(
            $this->repo->getDistrictsByState($request, $state_code)
        );
    }
    public function getWardsByDistrict(Request $request, $district_id, $state_id)
    {
        return $this->respond(
            $this->repo->getWardsByDistrict($request, $district_id, $state_id)
        );
    }
    public function get_area_id(Request $request)
    {
        try {
            $params = $request->all();
            $province_name = trim($params['province_name']);
            if(!empty($province_name)){
                $query = DB::table('cscart_areas');
                $query->where('name', 'LIKE', '%'.$province_name.'%');
                $res = $query->select('area_id')->first();

                return [
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => $res
                ];
            } else {

                return array(
                    'status' => 400,
                    'message' => 'Tên tỉnh thành không được rỗng',
                );
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}