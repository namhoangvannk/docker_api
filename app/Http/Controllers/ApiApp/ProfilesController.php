<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/14/2018
 * Time: 9:45 AM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\ProfilesRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;
use App\Models\Users;

class ProfilesController extends Controller
{
    protected $repo;

    public function __construct(ProfilesRepository $repo)
    {
        $this->repo = $repo;
    }
    // add address
    public function addAddress(Request $request)
    {
        return $this->respond(
            $this->repo->addAddress($request)
        );
    }
    public function getAddressdefault(Request $request , $user_id)
    {
        return $this->respond(
            $this->repo->getAddressdefault($request ,$user_id)
        );
    }
    // get list address
    public function getListAddress(Request $request , $user_id)
    {
        return $this->respond(
            $this->repo->getListAddress($request ,$user_id)
        );
    }

    // delete address
    public function deleteAddress(Request $request)
    {
        return $this->respond(
            $this->repo->deleteAddress($request)
        );
    }
    // update address
    public function updateAddress(Request $request)
    {
        return $this->respond(
            $this->repo->updateAddress($request)
        );
    }

    public function getUser(Request $request, $user_id)
    {
        return $this->respond(
            $this->repo->getUser($request, $user_id)
        );
    }

    public function updateUser(Request $request, $user_id)
    {
        return $this->respond(
            $this->repo->updateUser($request, $user_id)
        );
    }
    public function updateUserSocical(Request $request, $user_id)
    {
        return $this->respond(
            $this->repo->updateUserSocical($request, $user_id)
        );
    }
    public function changePassword(Request $request, $user_id)
    {
        return $this->respond(
            $this->repo->changePassword($request, $user_id)
        );
    }
    public function uploadAvatar(Request $request)
    {
        $user_id = $request->input('user_id');
        $base64_image = $request->input('base64_image');
        $png_url = "avatar-" . time() . ".png";
        $base_to_php = explode(',', $base64_image);
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base_to_php[1]));
        $filepath = './media/images/' . $png_url;
        file_put_contents($filepath, $data);
        $url = "https://www.nguyenkim.com/upload.php";
        $uploadDir = './public/media/images/';
        $uploadFile = $uploadDir . basename($png_url);
        $uploadRequest = array(
            'fileName' => basename($filepath),
            'fileData' => base64_encode(file_get_contents($filepath)),
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $uploadRequest);
        $response = curl_exec($curl);
        if (trim($response) == 'Success') {
            Users::where('user_id', $user_id)
                ->update(['avatar' => $png_url]);
            unlink($filepath);
            curl_close($curl);
            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => 'https://www.nguyenkim.com/images/companies/_1/avatar_app/'.$png_url,
                )
            );
        } else {
            curl_close($curl);
            return response()->json(
                array(
                    'status' => 400,
                    'message' => 'Lỗi upload, vui lòng thử lại',
                )
            );
        }
    }
    public function listHobby(Request $request){
        return $this->respond(
            $this->repo->listHobby($request)
        );

    }

}