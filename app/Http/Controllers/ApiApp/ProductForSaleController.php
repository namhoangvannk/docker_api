<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 3/22/2019
 * Time: 9:46 AM
 */
namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\ProductForSaleRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;

class ProductForSaleController extends Controller
{
    protected $repo;
    public function __construct(ProductForSaleRepository $repo)
    {
        $this->repo = $repo;
    }
    public function exportCSV(Request $request)
    {
        return $this->respond(
            $this->repo->exportCSV($request)
        );
    }
    public function listProduct(Request $request)
    {
        return $this->respond(
            $this->repo->listProduct($request)
        );
    }
}