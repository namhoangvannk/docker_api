<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 7/13/2018
 * Time: 10:51 AM
 */
namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Illuminate\Support\Facades\Validator;

class PartialController extends Controller
{
    public function __construct(){
    }
    // Danh sách
    public function get_option(Request $request,$pid)
    {
        $arr = array();
        $arr_product = array();
        $query = DB::table('cscart_product_options_inventory AS poi');
        $query->join ('cscart_product_options AS po', 'poi.product_id', '=','po.product_id');
        $query->join ('cscart_product_options_descriptions AS pod', 'pod.option_id', '=','po.option_id');
        $query->Where(function($query) use ($pid){
            $query->where('poi.product_id',$pid);
            $query->orWhere('poi.product_code',$pid);
        });
        $product_data = $query->select('poi.product_id')->get();
        $arr = [];
        if(!empty($product_data)){
            foreach ($product_data as $item){
                array_push($arr, $item->product_id);
            }
        }
        $query = DB::table('cscart_product_options_inventory AS poi');
        $query->join ('cscart_product_options AS po', 'poi.product_id', '=','po.product_id');
        $query->join ('cscart_product_options_descriptions AS pod', 'pod.option_id', '=','po.option_id');
        $query->join ('cscart_products AS p', 'poi.product_code', '=','p.product_id');
        if(!empty($arr)){
            $query->whereIn('poi.product_id',$arr);
        }
        $query->where('p.status','<>','D');
        $query->orderBy('po.position', 'DESC');
        $query->orderBy('poi.position', 'DESC');
        $res = $query->select('p.product_id', 'poi.combination' , 'poi.combination_hash' , 'pod.option_id', 'pod.option_name')->get();

        $sum_cid = count($res);
        for ($i = 0; $i < $sum_cid; $i++) {
            $option_id = $res[$i]->option_id;
            $combination = $res[$i]->combination;
            $product_id = $res[$i]->product_id;
            $variant_id = $this->fn_nk_get_variant_id_from_combination($combination, $option_id);
            if($variant_id != 0 && $product_id == $pid) {
                $arr_product[$option_id]['option_name'] = $res[$i]->option_name;
                if (!isset($arr_product[$option_id]['variant_ids'][$variant_id]['pid'])) {
                    $arr_product[$option_id]['variant_ids'][$variant_id]['pid'] = $product_id;
                    $arr_product[$option_id]['variant_ids'][$variant_id]['combination'] = $combination;
                    $arr_product[$option_id]['variant_ids'][$variant_id]['combination_hash'] = $res[$i]->combination_hash;
                }
            }
        }
        for ($i = 0; $i < $sum_cid; $i++) {
            $option_id = $res[$i]->option_id;
            $combination = $res[$i]->combination;
            $product_id = $res[$i]->product_id;
            $variant_id = $this->fn_nk_get_variant_id_from_combination($combination, $option_id);

            if($variant_id != 0) {
                $arr[$option_id]['option_name'] = $res[$i]->option_name;
                if (!isset($arr[$option_id]['variant_ids'][$variant_id]['pid']) ) {
                    $arr[$option_id]['variant_ids'][$variant_id]['pid'] = $product_id;
                    $arr[$option_id]['variant_ids'][$variant_id]['combination'] = $combination;
                    $arr[$option_id]['variant_ids'][$variant_id]['combination_hash'] = $res[$i]->combination_hash;
                } else {
                    if ( $product_id == $pid ) {
                        $arr[$option_id]['variant_ids'][$variant_id]['pid'] = $product_id;
                        $arr[$option_id]['variant_ids'][$variant_id]['combination'] = $combination;
                        $arr[$option_id]['variant_ids'][$variant_id]['combination_hash'] = $res[$i]->combination_hash;
                    } else {
                        $isGuess = false;
                        foreach($arr_product as $pro_option_id => $obj){
                            $pro_variant_id = key($obj['variant_ids']);
                            if($pro_option_id <>$option_id ){
                                $other_variant_id = $this->fn_nk_get_variant_id_from_combination($combination, $pro_option_id);
                                if($other_variant_id == $pro_variant_id){
                                    $isGuess = true;
                                }
                            }
                        }
                        if($isGuess) {
                            $arr[$option_id]['variant_ids'][$variant_id]['pid'] = $product_id;
                            $arr[$option_id]['variant_ids'][$variant_id]['combination'] = $combination;
                            $arr[$option_id]['variant_ids'][$variant_id]['combination_hash'] = $res[$i]->combination_hash;
                        }
                    }
                }
            }
        }

        $data = [];
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $arr,
            )
        );
    }
    function fn_nk_get_variant_id_from_combination($combination, $option_id)
    {
        $tmp_arr = explode('_', $combination);
        $inext = false;
        $val = 0;
        foreach ($tmp_arr as $item) {
            if ($inext) {
                $val = $item;
                break;
            }
            if ($item == $option_id)
                $inext = true;

        }
        return $val;
    }
    public function fn_nk_get_variant_name($var_id){
        if (empty($var_id)) {
            return;
        }
        $query = DB::table('cscart_product_option_variants_descriptions');
        $query->where('lang_code','vi');
        $query->where('variant_id',$var_id);
        $data = $query->select('variant_name')->first();
        return $data;

    }
    public function get_video(Request $request,$pid){
        $query = DB::table('cscart_product_videos');
        $query->where('product_id',$pid);
        $query->where('video_status','A');
        $query->orderBy('video_id', 'ASC');
        $datas =[];
        $res = $query->select('product_id', 'video_id' , 'video_title' , 'youtube_code')->get();
        if(!empty($res)){
            foreach ($res as $item){
                $data['product_id'] = $item->product_id;
                $data['video_id'] = $item->video_id;
                $data['video_title'] = $item->video_title;
                $data['video_url'] = 'https://www.youtube.com/watch?v='.$item->youtube_code;
                $data['video_image_thumbs'] = 'https://i1.ytimg.com/vi/'.$item->youtube_code.'/2.jpg';
                $data['video_image'] = 'https://i1.ytimg.com/vi/'.$item->youtube_code.'/0.jpg';
                $datas[] = $data;
            }
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $datas,
            )
        );
    }
    public function get_image(Request $request,$pid){
        $query = DB::table('cscart_product_image_360');
        $query->where('product_id',$pid);
        $query->where('status','Y');
        $datas =[];
        $res = $query->select('product_id', 'path','status','type')->get();
        if(!empty($res)){
            foreach ($res as $item){
                $data['product_id'] = $item->product_id;
                for($i=1;$i<=35;$i++){
                    $data['data_images'][] =[
                        'url' => $item->path.$i.'.'.$item->type,
                    ];
                }
                $datas[] = $data;
            }
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $datas,
            )
        );
    }
    public function get_mall(Request $request){
        $state_id = $request->input('state_id') ? $request->input('state_id') : '001';
        $district_id = $request->input('district_id') ? $request->input('district_id') : '';
        $query = DB::table('cscart_nk_mall');
        $query->where('state_code','=',$state_id);
        if($district_id !='')
            $query->where('district_code',$district_id);
        $query->where('status','A');
        $query->where('type','T');
        $query->orderBy('mall_ids', 'ASC');
        $data = $query->select('id', 'state_code' , 'district_code' , 'address')->get();
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    public function get_total_profile(Request $request, $user_id){
        $data['total_orders'] = 10;
        $data['total_reviews'] = 5;
        $data['total_product_likes'] = 15;
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    public function subscribe(Request $request){

        $userId = $request->input('user_id');
        $phone = $request->input('phone');
        $prodId = $request->input('product_id');
        $valid = Validator::make($request->all(),
            [
                'phone' => 'required',
                'product_id' => 'required',
                'user_id' => 'required'
            ],
            [
                'phone.required' =>'Số điện thoại không được rỗng',
                'product_id.required' =>'Product ID không được rỗng',
                'user_id.required' =>'ID user không được rỗng',
            ]
        );
        if($valid->passes()){
            $query = DB::table('cscart_product_subscriptions');
            $query->where('Phone', $phone);
            $data = $query->select('subscription_id')->get();
            if(!$data->isEmpty()){
                $sub_id = $data[0]->subscription_id;
                if(!empty($sub_id)){
                    $_data['subscription_id'] = $sub_id;
                }
                $_data['product_id'] = $prodId;
                $_data['user_id'] = $userId;
                $_data['email'] = 'nk@nk.com';
                $_data['Phone'] = $phone;
                if(intval($sub_id) > 0){
                    DB::table('cscart_product_subscriptions')
                        ->where('subscription_id', $sub_id)
                        ->update($_data);
                }
                return response()->json(
                    array(
                        'status' => 200,
                        'message' => 'Thành công',
                        'data' => $_data
                    )
                );
            }else{
                $_data['product_id'] = $prodId;
                $_data['user_id'] = $userId;
                $_data['email'] = 'nk@nk.com';
                $_data['Phone'] = $phone;
                DB::table('cscart_product_subscriptions')->insert($_data);
                return response()->json(
                    array(
                        'status' => 200,
                        'message' => 'Thành công',
                        'data' => $_data
                    )
                );
            }

        }else{
            $errors = $valid->errors();
            $data_error = [];
            !$errors->has('phone') ?: $data_error['phone'] = $errors->first('phone');
            !$errors->has('product_id') ?: $data_error['product_id'] = $errors->first('product_id');
            !$errors->has('user_id') ?: $data_error['user_id'] = $errors->first('user_id');
            return [
                'success' => 400,
                'message' => $data_error,
                'data' => []
            ];
        }


    }
}