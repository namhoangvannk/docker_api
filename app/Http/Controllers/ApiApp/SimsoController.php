<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 7/13/2018
 * Time: 10:51 AM
 */
namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use SoapClient;

class SimsoController extends Controller
{
    public function __construct(){
    }
    // menu
    public function menu_simso(){
        $data = array(
            0 => array(
                'name' => 'Sim số đẹp',
                'icon' => "sim-o",
                'object_id' => 1,
                'color' => '#0b284a'
            ),
            1 => array(
                'name' => 'Mua thẻ cào',
                'icon' => "phone-card-o",
                'object_id' => 2,
                'color' => '#f86666'
            ),
            2 => array(
                'name' => 'Nạp tiền điện thoại',
                'icon' => "phone-money-o",
                'object_id' => 3,
                'color' => '#f86666'

            ),
            3 => array(
                'name' => 'Mua thẻ game',
                'icon' => "game-o",
                'object_id' => 4,
                'color' => '#0b284a'
            )
        );
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    // danh sách simso
    // danh sách gói cươc theo nhà mạng
    // danh sách nhà cung cấp thẻ cào
    public function fn_vas_get_providers($provider_code = NULL){
        $data = [];
        $query = DB::table('cscart_nk_vas_provider');
        if(empty($provider_code) && $provider_code != NULL){
            $query->where('ProviderCode',mb_strtoupper($provider_code));
        }
        $data = $query->select('*')->get();
        return $data;
    }
    function fn_vas_get_providers_game($type='')
    {
        $type="%".$type."%";
        $data = [];
        $query = DB::table('cscart_nk_vas_provider');
        $query->where('ProviderService','like',$type);
        $data = $query->select('*')->get();
        return $data;
    }

    function fn_vas_get_all_provider($type = 'MCARD', $active = 1)
    {
        $query = DB::table('cscart_nk_vas_provider_value');
        $query->leftJoin ('cscart_nk_vas_provider', 'cscart_nk_vas_provider_value.ProviderID', '=','cscart_nk_vas_provider.ProviderID');
        $query->where('ValueStatus',$active);
        $query->where('cscart_nk_vas_provider_value.Type', mb_strtoupper($type));
        $query = $query->orderBy('ListPrice', 'ASC');
        $data =$query->select('cscart_nk_vas_provider_value.*','cscart_nk_vas_provider.ProviderName AS ProviderName','cscart_nk_vas_provider.ProviderCode AS ProviderCode')->get();
        return $data;

    }
    // danh sách mệnh giá thẻ cào theo nhà cung cấp và giá bán
    // Mệnh giá thẻ theo nhà mạng
    public function get_price_by_provider(Request $request){
        $provider_code = $request->input('provider_code');
        $data = [];
        $data = $this->fn_vas_get_providers($provider_code);
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    // Danh sách
    function fn_get_list_simso($params){

        //require_once ('app/Libraries/class.soapclient.php');
        $pdw_url = 'https://wslocal.trade.nguyenkim.com/WS_ECOM/ECOM_Services.svc';
        $total = 0;
        $sims = [];
        $client = new SoapClient($pdw_url . '?wsdl&1', array("connection_timeout" => 90, "trace" => 1, "location" => "..", 'exceptions' => 1, "stream_context" => stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true)))));
        $client->__setLocation($pdw_url);
        $result = $client->ECOM_GetListSimMobile($params);
        $result = (array)$result;
        $response = json_decode(json_encode($result['ECOM_GetListSimMobileResult']), true);
        $response = json_decode($response, true);
        if($response["Header"][0]["Msg_Code"] == "1"){
            $total = $response["RowCount"][0]["RowCount"];//tong so row
            $sims = $response["Data"];
        }

        return ['sims' => $sims, 'total' => $total];
    }
    // sim số
    public function simso(Request $request){
        $params = array(
            'pSimCode' => !empty($_REQUEST["simso"])?$_REQUEST["simso"]:"",
            'pIsPage' => "Y",
            'pPageNumb' => isset($_REQUEST["page"])?$_REQUEST["page"]:1,
            'pPageSize' => 20,
            'pSmGrName' => !empty($_REQUEST["group_sim"])?$_REQUEST["group_sim"]:"",
            'pSmHdName' => !empty($_REQUEST["ncc_sim"])?$_REQUEST["ncc_sim"]:"mobifone|vinaphone", //"mobifone|vinaphone|viettel"
            'pSimSold' => $this->fn_get_list_sim_sold(), //sim1|sim2|sim3
            'pintSimLen' => 0 //0 or 10 or 11 số theo so dt
        );

        //list($sims, $total) =
        $res = $this->fn_get_list_simso($params);
        $total = $res['total'];
        $sims = $res['sims'];
        $providers = $this->fn_vas_get_providers();
        $data['title'] = 'Mua sim nghe gọi, 3G, 4G';
        $data['warning_choose_package'] = 'Bạn vui lòng chọn gói cước trước khi mua sim';
        $data['icon'] ='"\E90E"';
        $data['thu_tuc_dang_ky_sim'] =[
            'title' => 'Xem thủ tục đăng ký sim',
            'content' => [
                "title" => "Thủ tục đăng ký SIM bao gồm:",
                'note' => "(Theo quy định của nhà mạng)",
                "data" => array(
                    array('name' => "Bản gốc CMND (cấp dưới 15 năm) hoặc Căn cước công dân (còn thời hạn) hoặc Hộ chiếu (còn thời hạn trên 6 tháng) của chủ thuê bao, nếu là bản sao phải có công chứng dưới 6 tháng."),
                    array('name' => "Ảnh chân dung của chủ thuê bao tại thời điểm giao dịch.")
                )

            ]
        ];
        $data['thong_tin_nha_mang'] =[
            'title' => 'Thông tin nhà mạng',
            'content' => [
                0 => [
                    'name' => 'Mobifone',
                    'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
                    'info' => [
                        array( 'name' => 'Tổng đài 24/24' , 'des' => '1800.1090 - 9090'),
                        array( 'name' => 'Đường dây nóng:' , 'des' => '0908.144.144'),
                        array('name' => 'Các đầu số nhận biết' , 'des' => '090 - 093 - 0120 - 0121 - 0122 - 0126 - 0128 - 089')
                    ]
                ],
                1 => [
                    'name' => 'Vinaphone',
                    'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_vinaphone%402x.png',
                    'info' => [
                        array('name' => 'Tổng đài 24/24' , 'des' => '1800.1091 - 9191'),
                        array('name' => 'Đường dây nóng:' , 'des' => '0912.48.1111 - 0918.68.1111'),
                        array('name' => 'Các đầu số nhận biết' , 'des' => '091 - 094 - 0123 - 0124 - 0125 - 0127 - 0129 - 088')
                    ]
                ],

            ]
        ];
        $data['text_search'] = config('constants.text_search');
        $data['nhamang'] = config('constants.nhamang');
        $data['loaisim'] = config('constants.category_simso');
        $data['title_checkbox'] = config('constants.dauso');
        $data['package_data_simso'] = [];
        $packe_data_simso = config('constants.packe_data_simso');
        if(!empty($packe_data_simso)){
            foreach($packe_data_simso as $key => $value){
                if($value['show'] == 0){
                    unset($packe_data_simso[$key]);
                }
            }
        }

        $data['package_data_simso'] = array_values($packe_data_simso);
        $data['vas_providers'] = $providers;
        $data_sims = [];
        if(!empty($sims)){
            foreach($sims as $key => $value){
                $data_sim = $value;
                $data_sim['image'] = '';
                if(strtolower($value['SmHdName']) == 'mobifone'){
                    $data_sim['image'] = 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png';
                    $data_sim['product_id'] = 60667;
                }elseif(strtolower($value['SmHdName']) == 'vinaphone'){
                    $data_sim['image'] = 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_vinaphone%402x.png';
                    $data_sim['product_id'] = 60693;
                }
                $data_sims[] = $data_sim;
            }
        }
        $data['sim_data'] = $data_sims;

        $data['total'] = (int)$total;
        $data['current_page'] = 0;
        $data['total_page'] = 0;
        $page = isset($_REQUEST["page"])?$_REQUEST["page"]:1;
        if($total > 0){
            $data['total_page'] = ceil($total/20);
            $data['current_page'] = (int)$page;
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }

    // nạp thẻ cào
    public function recharge_card(Request $request){
        $data['title'] = 'Mua thẻ cào';
        $providers = $this->fn_vas_get_providers();
        $list= $this->fn_vas_get_all_provider();
        $arr=array();
        foreach ($list as $key => $item) {
            $arr[$item->ProviderCode][]=$item;
        }
        $vas_providers = [];
        if(!empty($providers)){
            foreach($providers as $key => $value){
                if(!empty($value->ProviderService)){
                    $arr1 = explode("," ,$value->ProviderService);
                    if(in_array('MCARD',$arr1)){
                        $vas_providers[] = $value;
                    }
                }

            }
        }
        $data['vas_providers'] = $vas_providers;
        $data['list_type_price'] = $arr;
        $data['price_table'] = $this->get_vas_provider_value($providers[0]->ProviderID,'MCARD');
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    // nạp tiền điện thoại
    public function recharge_phone(Request $request){
        $data['title'] = 'Nạp tiền điện thoại';
        $arr = [];
        $type =  "MTOPUP";
        $providers = $this->fn_vas_get_providers();
        $list= $this->fn_vas_get_all_provider($type);
        $arr=array();
        foreach ($list as $key => $item) {
            $arr[$item->ProviderCode][]=$item;
        }
        $vas_providers = [];
        if(!empty($providers)){
            foreach($providers as $key => $value){
                if(!empty($value->ProviderService)){
                    $arr1 = explode("," ,$value->ProviderService);
                    if(in_array('MTOPUP',$arr1)){
                        $vas_providers[] = $value;
                    }
                }
            }
        }
        $data['vas_providers'] = $vas_providers;
        $data['list_type_price'] = $arr;
        $data['price_table'] = $this->get_vas_provider_value($providers[0]->ProviderID,'MCARD');
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    // nạp thẻ game
    public function recharge_game(Request $request){
        $data['title'] = 'Mua thẻ game';
        $arr = [];
        $type =  "GCARD";
        $providers = $this->fn_vas_get_providers_game($type);
        $list= $this->fn_vas_get_all_provider($type);
        $arr=array();
        foreach ($list as $key => $item) {
            $arr[$item->ProviderCode][]=$item;
        }
        $data['vas_providers'] = $providers;
        $data['list_type_price'] = $arr;
        $data['price_table'] = $this->get_vas_provider_value($providers[0]->ProviderID,'GCARD');
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    public function fn_get_list_sim_sold(){
        $time = time() - 86400; // 12h
        /*$rs = db_get_array("SELECT sim FROM ?:order_simso os JOIN ?:orders o WHERE os.timestamp>=?i AND o.status NOT IN(?a)", $time, STATUS_CANCELED_ORDER_ALL);
        if(empty($rs)){
            return "";
        }
        return implode("|", fn_array_column($rs, 'sim'));*/

        $query = DB::table('cscart_order_simso');
        $query->leftJoin ('cscart_orders', 'cscart_order_simso.order_id', '=','cscart_orders.order_id');
        $query->where('cscart_order_simso.timestamp','>',$time);
        $query->whereIn('cscart_orders.status',['I']);
        //$query = $query->orderBy('ListPrice', 'DESC');
        $data =$query->select('cscart_order_simso.sim')->get();
        //var_dump((array) $data);die;
        return $data;
    }
    public function search_simso(Request $request){
        $params = array(
            'pSimCode' => !empty($_REQUEST["simso"])?$_REQUEST["simso"]:"",
            'pIsPage' => "Y",
            'pPageNumb' => isset($_REQUEST["page"])?$_REQUEST["page"]:1,
            'pPageSize' => 20,
            'pSmGrName' => !empty($_REQUEST["group_sim"])?$_REQUEST["group_sim"]:"",
            'pSmHdName' => !empty($_REQUEST["ncc_sim"])?$_REQUEST["ncc_sim"]:"mobifone|vinaphone", //"mobifone|vinaphone|viettel"
            'pSimSold' => $this->fn_get_list_sim_sold(), //sim1|sim2|sim3
            'pintSimLen' => 0 //0 or 10 or 11 số theo so dt
        );
        $res = $this->fn_get_list_simso($params);
        $total = $res['total'];
        $sims = $res['sims'];
        $ncc_sim = $_REQUEST["ncc_sim"];
        $data['nhamang'] = config('constants.nhamang');
        $nhamang = config('constants.nhamang');
        if(!empty($nhamang) && $_REQUEST["ncc_sim"] !=''){
            if($_REQUEST["ncc_sim"] == 'Mobifone' || $_REQUEST["ncc_sim"] == 'Vinaphone'){
                foreach ($nhamang as $key => $value) {
                    if(strtolower($value['value']) == strtolower($_REQUEST["ncc_sim"])){
                        $data['nhamang'] = array($value);
                        break;
                    }
                }
            }
        }
        $data['nhamangselected'] ='';
        if(!empty($nhamang) && $_REQUEST["ncc_sim"] !=''){
            if(strtolower($ncc_sim) == 'mobifone'){
                $data['nhamangselected'] = "mobifone";
            }
            if(strtolower($ncc_sim) == 'vinaphone'){
                $data['nhamangselected'] = "vinaphone";
            }
        }
        $data['loaisimselected'] = '';
        $data['loaisim'] = config('constants.category_simso');
        if(!empty($data['loaisim']) && $_REQUEST["group_sim"] !=''){
            foreach ($nhamang as $key => $value) {
                if(strtolower($value['value']) == strtolower($_REQUEST["group_sim"])){
                    $data['loaisimselected'] = $value['value'];
                    break;
                }
            }
        }
        $data_sims = [];
        if(!empty($sims)){
            foreach($sims as $key => $value){
                $data_sim = $value;
                $data_sim['image'] = '';
                if(strtolower($value['SmHdName']) == 'mobifone'){
                    $data_sim['image'] = 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png';
                    $data_sim['product_id'] = 60667;
                }elseif(strtolower($value['SmHdName']) == 'vinaphone'){
                    $data_sim['image'] = 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_vinaphone%402x.png';
                    $data_sim['product_id'] = 60693;
                }
                $data_sims[] = $data_sim;
            }
        }
        $data['sim_data'] = $data_sims;
        $data['total'] = (int)$total;
        $data['current_page'] = 0;
        $data['total_page'] = 0;
        $page = isset($_REQUEST["page"])?$_REQUEST["page"]:1;
        if($total > 0){
            $data['total_page'] = ceil($total/20);
            $data['current_page'] = (int)$page;
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    public function banner(Request $request){
        $data = array (
            0 =>
                array (
                    '_id' =>
                        array (
                            '$oid' => '5b6d57fdc7365ab02d22a821',
                        ),
                    'banner_id' => 4787,
                    'banner' => 0,
                    'url' => 'https://www.nguyenkim.com/cuong-nhiet-cung-worldcup-2018.html',
                    'nk_banner_ext' => 'https://beta.nguyenkim.com/images/promo/526/home-scroll-mobile-wc2018-new.jpg',
                ),
            1 =>
                array (
                    '_id' =>
                        array (
                            '$oid' => '5b6d57fec7365ab2c4335c86',
                        ),
                    'banner_id' => 4640,
                    'banner' => 0,
                    'url' => 'http://nguyenkim.com/gia-soc-cuoi-tuan.html',
                    'nk_banner_ext' => 'https://beta.nguyenkim.com/images/promo/521/HW23_768x298_nkx0-ar.jpg',
                ),
            2 =>
                array (
                    '_id' =>
                        array (
                            '$oid' => '5b6d57fec7365abecd184cb7',
                        ),
                    'banner_id' => 4593,
                    'banner' => 0,
                    'url' => 'https://www.nguyenkim.com/tivi-tcl-world-cup.html',
                    'nk_banner_ext' => 'https://beta.nguyenkim.com/images/promo/519/Tivi-TCL-World-Cup-768x298.jpg',
                )
        );
        $data = [];
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );
    }
    function vas_provider_value(Request $request)
    {
        $data = [];
        $provider_id = $request->input('provider_id');
        $type = $request->input('type');
        $query = DB::table('cscart_nk_vas_provider_value');
        $query->leftJoin ('cscart_nk_vas_provider', 'cscart_nk_vas_provider_value.ProviderID', '=','cscart_nk_vas_provider.ProviderID');
        $query->where('ValueStatus',1);
        $query->where('cscart_nk_vas_provider.ProviderID', $provider_id);
        $query->where('cscart_nk_vas_provider_value.Type', mb_strtoupper($type));
        $query = $query->orderBy('ListPrice', 'ASC');
        $data_price =$query->select('cscart_nk_vas_provider_value.*','cscart_nk_vas_provider.ProviderName AS ProviderName','cscart_nk_vas_provider.ProviderCode AS ProviderCode')->get();
        if(!empty($data_price)){
            foreach($data_price as $item){
                $datas['menh_gia'] = $item->ListPrice;
                $datas['gia_nk'] = $item->SalePrice;
                $data[] = $datas;

            }
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            )
        );

    }
    function get_vas_provider_value($provider_id, $type)
    {
        $data = [];
        $query = DB::table('cscart_nk_vas_provider_value');
        $query->leftJoin ('cscart_nk_vas_provider', 'cscart_nk_vas_provider_value.ProviderID', '=','cscart_nk_vas_provider.ProviderID');
        $query->where('ValueStatus',1);
        $query->where('cscart_nk_vas_provider.ProviderID', $provider_id);
        $query->where('cscart_nk_vas_provider_value.Type', mb_strtoupper($type));
        $query = $query->orderBy('ListPrice', 'ASC');
        $data_price =$query->select('cscart_nk_vas_provider_value.*','cscart_nk_vas_provider.ProviderName AS ProviderName','cscart_nk_vas_provider.ProviderCode AS ProviderCode')->get();
        if(!empty($data_price)){
            foreach($data_price as $item){
                $datas['menh_gia'] = $item->ListPrice;
                $datas['gia_nk'] = $item->SalePrice;
                $data[] = $datas;

            }
        }
        return $data;


    }

}