<?php
/**
 * Created by PhpStorm.
 * User: nguyenkim
 * Date: 1/3/19
 * Time: 1:46 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use App\Src\ApiApp\Repositories\TestRepository;
use Illuminate\Http\Request;

class TestController extends Controller {
	protected $repo;
	public function __construct(TestRepository $repo) {
		$this->repo = $repo;
	}

	public function index(Request $request) {
		return $this->respond(
			$this->repo->index($request)
		);
	}
}