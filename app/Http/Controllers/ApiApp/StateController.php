<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:24 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StateController extends Controller
{
    public function __construct(){
    }
    public function index(Request $request)
    {
        $data = array (
            0 =>
                array (
                    'state_id' => '692',
                    'state' => 'Tp. Hồ Chí Minh',
                ),
            1 =>
                array (
                    'state_id' => '717',
                    'state' => 'Hà Nội',
                ),
            2 =>
                array (
                    'state_id' => '711',
                    'state' => 'Đà Nẵng',
                ),
            3 =>
                array (
                    'state_id' => '713',
                    'state' => 'Đồng Tháp',
                ),
            4 =>
                array (
                    'state_id' => '757',
                    'state' => 'Đồng Nai',
                ),
            5 =>
                array (
                    'state_id' => '693',
                    'state' => 'Bình Dương',
                ),
            6 =>
                array (
                    'state_id' => '694',
                    'state' => 'Vĩnh Long',
                ),
            7 =>
                array (
                    'state_id' => '696',
                    'state' => 'Long An',
                ),
            8 =>
                array (
                    'state_id' => '697',
                    'state' => 'Bến Tre',
                ),
            9 =>
                array (
                    'state_id' => '698',
                    'state' => 'Tiền Giang',
                ),
            10 =>
                array (
                    'state_id' => '699',
                    'state' => 'Cần Thơ',
                ),
            11 =>
                array (
                    'state_id' => '700',
                    'state' => 'Quảng Trị',
                ),
            12 =>
                array (
                    'state_id' => '701',
                    'state' => 'An Giang',
                ),
            13 =>
                array (
                    'state_id' => '702',
                    'state' => 'Bà Rịa-Vũng Tàu',
                ),
            14 =>
                array (
                    'state_id' => '703',
                    'state' => 'Bắc Cạn',
                ),
            15 =>
                array (
                    'state_id' => '704',
                    'state' => 'Bắc Giang',
                ),
            16 =>
                array (
                    'state_id' => '705',
                    'state' => 'Bắc Ninh',
                ),
            17 =>
                array (
                    'state_id' => '706',
                    'state' => 'Bình Định',
                ),
            18 =>
                array (
                    'state_id' => '707',
                    'state' => 'Bình Phước',
                ),
            19 =>
                array (
                    'state_id' => '708',
                    'state' => 'Bình Thuận',
                ),
            20 =>
                array (
                    'state_id' => '709',
                    'state' => 'Cao Bằng',
                ),
            21 =>
                array (
                    'state_id' => '710',
                    'state' => 'Cà Mau',
                ),
            22 =>
                array (
                    'state_id' => '712',
                    'state' => 'Đắk Lắk',
                ),
            23 =>
                array (
                    'state_id' => '714',
                    'state' => 'Gia Lai',
                ),
            24 =>
                array (
                    'state_id' => '715',
                    'state' => 'Hà Giang',
                ),
            25 =>
                array (
                    'state_id' => '716',
                    'state' => 'Hà Nam',
                ),
            26 =>
                array (
                    'state_id' => '718',
                    'state' => 'Hà Tây',
                ),
            27 =>
                array (
                    'state_id' => '719',
                    'state' => 'Hà Tĩnh',
                ),
            28 =>
                array (
                    'state_id' => '720',
                    'state' => 'Hải Dương',
                ),
            29 =>
                array (
                    'state_id' => '721',
                    'state' => 'Hải Phòng',
                ),
            30 =>
                array (
                    'state_id' => '722',
                    'state' => 'Hòa Bình',
                ),
            31 =>
                array (
                    'state_id' => '723',
                    'state' => 'Hưng Yên',
                ),
            32 =>
                array (
                    'state_id' => '724',
                    'state' => 'Khánh Hòa',
                ),
            33 =>
                array (
                    'state_id' => '725',
                    'state' => 'Kiên Giang',
                ),
            34 =>
                array (
                    'state_id' => '726',
                    'state' => 'Kon Tum',
                ),
            35 =>
                array (
                    'state_id' => '727',
                    'state' => 'Lai Châu',
                ),
            36 =>
                array (
                    'state_id' => '728',
                    'state' => 'Lạng Sơn',
                ),
            37 =>
                array (
                    'state_id' => '729',
                    'state' => 'Lào Cai',
                ),
            38 =>
                array (
                    'state_id' => '730',
                    'state' => 'Lâm Đồng',
                ),
            39 =>
                array (
                    'state_id' => '731',
                    'state' => 'Nam Định',
                ),
            40 =>
                array (
                    'state_id' => '732',
                    'state' => 'Nghệ An',
                ),
            41 =>
                array (
                    'state_id' => '733',
                    'state' => 'Ninh Bình',
                ),
            42 =>
                array (
                    'state_id' => '734',
                    'state' => 'Ninh Thuận',
                ),
            43 =>
                array (
                    'state_id' => '753',
                    'state' => 'Phú Thọ',
                ),
            44 =>
                array (
                    'state_id' => '736',
                    'state' => 'Phú Yên',
                ),
            45 =>
                array (
                    'state_id' => '737',
                    'state' => 'Quảng Bình',
                ),
            46 =>
                array (
                    'state_id' => '738',
                    'state' => 'Quảng Nam',
                ),
            47 =>
                array (
                    'state_id' => '739',
                    'state' => 'Quảng Ngãi',
                ),
            48 =>
                array (
                    'state_id' => '740',
                    'state' => 'Sơn La',
                ),
            49 =>
                array (
                    'state_id' => '741',
                    'state' => 'Sóc Trăng',
                ),
            50 =>
                array (
                    'state_id' => '744',
                    'state' => 'Tây Ninh',
                ),
            51 =>
                array (
                    'state_id' => '743',
                    'state' => 'Thái Bình',
                ),
            52 =>
                array (
                    'state_id' => '745',
                    'state' => 'Thanh Hóa',
                ),
            53 =>
                array (
                    'state_id' => '746',
                    'state' => 'Thừa Thiên Huế',
                ),
            54 =>
                array (
                    'state_id' => '747',
                    'state' => 'Trà Vinh',
                ),
            55 =>
                array (
                    'state_id' => '748',
                    'state' => 'Tuyên Quang',
                ),
            56 =>
                array (
                    'state_id' => '749',
                    'state' => 'Vĩnh Phúc',
                ),
            57 =>
                array (
                    'state_id' => '750',
                    'state' => 'Yên Bái',
                ),
            58 =>
                array (
                    'state_id' => '751',
                    'state' => 'Quảng Ninh',
                ),
            59 =>
                array (
                    'state_id' => '752',
                    'state' => 'Mỹ Tho',
                ),
            60 =>
                array (
                    'state_id' => '754',
                    'state' => 'Hậu Giang',
                ),
            61 =>
                array (
                    'state_id' => '755',
                    'state' => 'Điện Biên Phủ',
                ),
            62 =>
                array (
                    'state_id' => '756',
                    'state' => 'Đắk Nông',
                ),
            63 =>
                array (
                    'state_id' => '758',
                    'state' => 'Bạc Liêu',
                ),
            64 =>
                array (
                    'state_id' => '759',
                    'state' => 'Thái Nguyên',
                ),
            65 =>
                array (
                    'state_id' => '760',
                    'state' => 'Phan Thiết',
                ),
        );
        return response()->json(
            array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data,
            )
        );
    }
}