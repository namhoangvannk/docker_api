<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:19 PM
 */

namespace App\Http\Controllers\ApiApp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use App\Models\Mongo\Address;
use App\Models\Mongo\ProductsFullModeNew;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
        $user_id = $request->input('user_id',0);
        $limit = $request->input('limit', 10);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $start = ($offset - 1) * $limit;
        $order_id = $request->input('order_id', 0);
        $data = [];
        if($user_id > 0){
            $str_sql = "SELECT distinct cscart_orders.order_id, cscart_orders.issuer_id, cscart_orders.user_id, cscart_orders.is_parent_order, cscart_orders.parent_order_id, cscart_orders.company_id, cscart_orders.company, cscart_orders.timestamp, cscart_orders.firstname, cscart_orders.lastname, cscart_orders.email, cscart_orders.company, cscart_orders.phone, cscart_orders.status, cscart_orders.total, invoice_docs.doc_id as invoice_id, memo_docs.doc_id as credit_memo_id, CONCAT(issuers.firstname, ' ', issuers.lastname) as issuer_name, cscart_orders.shipping_status, cscart_orders.payment_status, cscart_orders.delivery_time, cscart_order_data.data as points, cscart_payment_descriptions.payment FROM cscart_orders  
LEFT JOIN cscart_users as issuers ON issuers.user_id = cscart_orders.issuer_id 
LEFT JOIN cscart_order_docs as invoice_docs ON invoice_docs.order_id = cscart_orders.order_id AND invoice_docs.type = 'I' 
LEFT JOIN cscart_order_docs as memo_docs ON memo_docs.order_id = cscart_orders.order_id AND memo_docs.type = 'C' 
LEFT JOIN cscart_order_data ON cscart_order_data.order_id = cscart_orders.order_id AND cscart_order_data.type = 'W' 
JOIN cscart_payments on cscart_payments.payment_id = cscart_orders.payment_id  
JOIN cscart_payment_descriptions on cscart_payments.payment_id = cscart_payment_descriptions.payment_id and cscart_payment_descriptions.lang_code='vi' 
WHERE 1  AND cscart_orders.is_parent_order != 'Y' ";
//AND  cscart_orders.cancel_reason != 7";
            if($order_id > 0){
                $str_sql.=" and  cscart_orders.order_id =".$order_id;
            }
            if($user_id > 0){
                $str_sql.=" AND cscart_orders.user_id IN (".$user_id.")";
            }
            $total = DB::select($str_sql);
            $total = count($total);
            $str_sql.=" ORDER BY cscart_orders.timestamp desc, cscart_orders.order_id desc  LIMIT ".$start.", ".$limit." ";

            $order = DB::select($str_sql);
            if(!empty($order)){
                foreach($order as $key => $item){
                    $datas['order_id'] = (int)$item->order_id;
                    $datas['timestamp'] = date('d/m/Y', (int)$item->timestamp);
                    $fn_get_status_name = $this->fn_get_status_name($item->status);
                    $datas['status'] = $fn_get_status_name[0]->ord_profile_decription;
                    $datas['total'] = (int)$item->total;
                    $data_product_detail = $this->get_product_order($item->order_id);
                    $datas['product_list'] = [];
                    if(!empty($data_product_detail)){
                        foreach ($data_product_detail as $key1 => $item1){
                            $datas1['product_id'] = (int)$item1->product_id;
                            $datas1['product_name'] = "";
                            $datas1['price'] = (int)$item1->price;
                            $datas1['amount'] = (int)$item1->amount;
                            $datas1['promotion_text'] = '';
                            $url = "https://api.nguyenkim.com/v1/product-details/".(int)$item1->product_id."/0";
                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "GET",
                                CURLOPT_HTTPHEADER => array(
                                    "authorization: nko_internal",
                                    "cache-control: no-cache",
                                    "content-type: application/json",
                                    "password: eJwrtjIytVJyyMvOj8/MK0ktykvMSTUyMDR3yPPWS87PVbIGALc2Cqc="
                                ),
                            ));
                            $response = curl_exec($curl);
                            $data_p = json_decode($response,true);
                            if(!empty($data_p['data'])){
                                $datas1['images'] = $data_p['data']['image_thumb'][0];
                                $datas1['product_name'] = $data_p['data']['nk_shortname'];
                            }
                            $datas['product_list'][] = $datas1;
                        }
                        //}
                        // if(!empty($products_list['product_groups'])){
                        //     foreach ($products_list['product_groups']['products'] as $key => $item){
                        //         $datas1['images'] = (int)$item['main_pair']['detailed']['image_path'];
                        //         $datas['product_list'][] = $datas1;
                        //     }
                        // }
                    }
                    $data[] = $datas;
                }

            }
        }else{
            if($order_id > 0){
                $str_sql = "SELECT distinct cscart_orders.s_state,cscart_orders.s_city,cscart_orders.s_county,cscart_orders.order_id,cscart_orders.subtotal_discount, cscart_orders.subtotal, cscart_orders.notes, cscart_orders.s_firstname, cscart_orders.s_address, cscart_orders.s_city,cscart_orders.s_phone ,cscart_orders.issuer_id,cscart_orders.user_id,cscart_orders.is_parent_order,cscart_orders.parent_order_id, cscart_orders.company_id, cscart_orders.company, cscart_orders.timestamp, cscart_orders.firstname, cscart_orders.lastname, cscart_orders.email, cscart_orders.company,cscart_orders.phone,cscart_orders.status, cscart_orders.total, invoice_docs.doc_id as invoice_id, memo_docs.doc_id as credit_memo_id, CONCAT(issuers.firstname, ' ', issuers.lastname) as issuer_name, cscart_orders.shipping_status, cscart_orders.payment_status,cscart_orders.delivery_time,cscart_order_data.data as points, cscart_payment_descriptions.payment FROM cscart_orders  
LEFT JOIN cscart_users as issuers ON issuers.user_id = cscart_orders.issuer_id 
LEFT JOIN cscart_order_docs as invoice_docs ON invoice_docs.order_id = cscart_orders.order_id AND invoice_docs.type = 'I' 
LEFT JOIN cscart_order_docs as memo_docs ON memo_docs.order_id = cscart_orders.order_id AND memo_docs.type = 'C' 
LEFT JOIN cscart_order_data ON cscart_order_data.order_id = cscart_orders.order_id AND cscart_order_data.type = 'W' 
JOIN cscart_payments on cscart_payments.payment_id = cscart_orders.payment_id  
JOIN cscart_payment_descriptions on cscart_payments.payment_id = cscart_payment_descriptions.payment_id and cscart_payment_descriptions.lang_code='vi' 
WHERE 1  AND cscart_orders.is_parent_order != 'Y' "; //  AND  cscart_orders.cancel_reason != 7 ";
                if($order_id > 0){
                    $str_sql.=" and  cscart_orders.order_id =".$order_id;
                }
                $str_sql.=" ORDER BY cscart_orders.timestamp desc, cscart_orders.order_id desc  LIMIT 1 ";

                $order = DB::select($str_sql);
                $data_orders = $order[0];
                //echo $data_orders->s_county;die;
                $data_address = $this->get_address($data_orders->s_county,$data_orders->s_city,$data_orders->s_state);
                //echo $data_address;die;
                $list_status = $this->fn_get_list_status();
                // danh sách trạng thái
                $datas['list_status'] = $list_status;
                // coupons
                $data_coupons = $this->get_coupons($data_orders->order_id);
                // fields
                $data_fields = $this->get_fields($data_orders->order_id);
                // thông tin xuất hóa đơn
                $datas['commapry'] = $data_orders->company;
                $datas['address'] = "";
                $datas['taxcode'] = "";
                $datas['representative'] = "";
                if(!empty($data_fields)){
                    foreach($data_fields as $key => $value){
                        if(isset($value->field_id) && $value->field_id == 36){
                            $datas['address'] = $value->value;
                        }
                        if(isset($value->field_id) && $value->field_id == 37){
                            $datas['taxcode'] = $value->value;
                        }
                        if(isset($value->field_id) && $value->field_id == 42){
                            $datas['representative'] = $value->value;
                        }

                    }
                }
                // thông tin đơn hàng
                $datas['order_id'] = (int)$data_orders->order_id;
                $datas['timestamp'] = date('d/m/Y',(int)$data_orders->timestamp);
                $datas['delivery_time'] = '';
                if((int)$data_orders->delivery_time > 0){
                    $datas['delivery_time'] = date('d/m/Y',(int)$data_orders->delivery_time);
                }

                $datas['total'] = (int)$data_orders->total;
                $datas['subtotal'] = (int)$data_orders->subtotal;
                $datas['subtotal_discount'] = (int)$data_orders->subtotal_discount;
                $datas['shipping'] = 0;
                $datas['coupons'] = '';
                if(!empty($data_coupons)){
                    foreach ($data_coupons as $key => $value){
                        $datas['coupons'] = (string)$key;
                        break;
                    }
                }
                $fn_get_status_name = $this->fn_get_status_name($data_orders->status);
                $datas['status_code'] =(int) $fn_get_status_name[0]->ord_profile_id;
                //if ($data_orders['status'] == 'JA' || $data_orders['status'] == 'L') {
                //  $datas['status_name'] = 'Đặt hàng thành công';
                //} else {
                $datas['status_name'] = $fn_get_status_name[0]->ord_profile_decription;
                //}

                $datas['notes'] = $data_orders->notes;
                // địa chỉ giao hàng
                $datas['s_firstname'] = $data_orders->s_firstname;
                $datas['s_address'] = $data_orders->s_address;
                if(!empty($data_address)){
                    $datas['s_address'] = $data_orders->s_address.', '.$data_address;
                }
                $datas['s_city'] = $data_orders->s_city;
                $datas['s_phone'] = $data_orders->s_phone;
                // thông tin sản phẩm
                $data_product_detail = $this->get_product_order($data_orders->order_id);
                $datas['product_list'] = [];
                $check_product_simso = 0;
                if(!empty($data_product_detail)){
                    foreach ($data_product_detail as $key1 => $item1){
                        $product_id = (int)$item1->product_id;
                        if($product_id == 60667 || $product_id == 60693 || $product_id == 60695){
                            $check_product_simso = 1;
                        }
                        $datas1['product_id'] = (int)$item1->product_id;
                        $datas1['product_name'] = "";
                        $datas1['price'] = (int)$item1->price;
                        $datas1['amount'] = (int)$item1->amount;
                        $datas1['promotion_text'] = '';
                        $url = "https://api.nguyenkim.com/v1/product-details/".(int)$item1->product_id."/0";
                        //echo $url;die;
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "GET",
                            CURLOPT_HTTPHEADER => array(
                                "authorization: nko_internal",
                                "cache-control: no-cache",
                                "content-type: application/json",
                                "password: eJwrtjIytVJyyMvOj8/MK0ktykvMSTUyMDR3yPPWS87PVbIGALc2Cqc="
                            ),
                        ));
                        $response = curl_exec($curl);
                        $data_p = json_decode($response,true);
                        if(!empty($data_p['data'])){
                            $datas1['images'] = $data_p['data']['image_thumb'][0];
                            $datas1['product_name'] = $data_p['data']['nk_shortname'];
                        }
                        $datas['product_list'][] = $datas1;
                    }
                    //}
                    // if(!empty($products_list['product_groups'])){
                    //     foreach ($products_list['product_groups']['products'] as $key => $item){
                    //         $datas1['images'] = (int)$item['main_pair']['detailed']['image_path'];
                    //         $datas['product_list'][] = $datas1;
                    //     }
                }
                if($check_product_simso == 1){
                    $datas['commapry'] = "";
                    $datas['address'] = "";
                    $datas['taxcode'] = "";
                    $datas['representative'] = "";
                }
                $data = $datas;
                if(!empty($data)){
                    $total = 1;
                }
            }
        }
        return response()->json(
            array(
                'status' => 200,
                'message' => 'successful',
                'data' => $data,
                'total' => $total
            )
        );
    }

    public function get_product_order($order_id){
        $data = [];
        $query = DB::table('cscart_order_details');
        $query->where('order_id', $order_id);
        $data = $query->select('product_id','product_code','price','amount')->get();
        return $data;
    }
    public function fn_get_status_name($status){
        $list_data =[];
        $query = DB::table('cscart_statuses as s');
        $query->leftJoin('cscart_profile_status as d', 's.status_id', '=', 'd.status_id');
        $query->where('s.type','=', 'O');
        $query->where('s.status','=', $status);
        $list_data = $query->select('d.ord_profile_decription','d.ord_profile_id')->get();
        return $list_data;
    }

    public function fn_get_list_status(){
        $data = array(
            0 => array(
                'status_code' => 1,
                'status_name' =>'Đặt hàng thành công'
            ),
            1 => array(
                'status_code' => 2,
                'status_name' =>'Đã xác nhận'
            ),
            2 => array(
                'status_code' => 3,
                'status_name' =>'Đang giao hàng'
            ),
            3 => array(
                'status_code' => 4,
                'status_name' =>'Giao hàng thành công'
            ),
            4 => array(
                'status_code' => 5,
                'status_name' =>'Đã hủy đơn hàng'
            ),
        );
        return $data;
    }
    public function get_fields($order_id){
        $list_data =[];
        $query = DB::table('cscart_profile_fields_data');
        $query->where('object_type','=', 'O');
        $query->where('object_id','=', $order_id);
        $list_data = $query->select('field_id', 'value')->get();
        return $list_data;
    }
    public function get_coupons($order_id){
        $list_data =[];
        $query = DB::table('cscart_order_data');
        $query->where('order_id','=', $order_id);
        $list_datas = $query->select('type', 'data')->get();
        if(!empty($list_datas)){
            foreach($list_datas as $key => $value){
                if($value->type == 'C'){
                    $list_data = unserialize($value->data);
                    break;
                }
            }
        }
        return $list_data;
    }
    public function get_address($ward_id, $district_id, $citi_id){
        $list_data = '';
        $address = new Address();
        $list_data =  $address->get_address($ward_id, $district_id, $citi_id);
        return $list_data;

    }
    public function get_products_by_code(Request $request){
        $product_code = $request->input('product_code');
        $product_id = 0;
        $curl = curl_init();
        $product_id = ProductsFullModeNew::findOne(trim(strtoupper($product_code)));
        if($product_id > 0){
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://www.nguyenkim.com/rest-api/stock/state/VN/".$product_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: nko_internal",
                    "Content-Type: application/json",
                    "password: eJwrtjIytVJyyMvOj8/MK0ktykvMSTUyMDR3yPPWS87PVbIGALc2Cqc="
                ),
            ));
            $response = curl_exec($curl);
            $response = json_decode($response,true);
            $err = curl_error($curl);
            curl_close($curl);
            $data_node = $response['result'];
            $str_sql ='SELECT a.*, b.name, c.amount, s.code as state_code,s.state_id
                        FROM cscart_nk_regions a 
                        LEFT JOIN cscart_nk_region_descriptions b ON a.region_id = b.region_id AND b.lang_code = "vi"
                        JOIN cscart_nk_region_amounts c ON c.region_id = a.region_id 
                        JOIN cscart_products p on p.product_id = c.product_id
                        LEFT JOIN cscart_states s on s.region_id = a.region_id 
                        WHERE  p.product_id = '.$product_id.' and c.amount >= 1 ORDER BY position ASC, region_id ASC;';
            $data = DB::select($str_sql);
            if(!empty($data)){
                $arr_hcm = [];
                $arr_hn = [];
                $arr = [];
                $arr2=[];
                foreach($data as $key => $value){
                    if($value->position > 0){
                        $arr[] = $value;

                    }else{
                        if (strlen(strstr($value->name, 'KV.HCM')) > 0) {
                            array_push($arr_hcm,$value);
                        }
                        if (strlen(strstr($value->name, 'KV.Hà Nội')) > 0) {
                            array_push($arr_hn,$value);
                        }
                    }

                }
                $arr_hcm_f = [];
                if(!empty($arr_hcm)){
                    $count = 0;
                    foreach($arr_hcm as $k => $v){
                        $count = $count + $v->amount;

                    }
                    $arr_hcm_f= array('code' =>'001', 'state' => 'TP. HCM','amount' => $count);
                    array_push($arr2,$arr_hcm_f);
                }
                $arr_hn_f = [];
                if(!empty($arr_hn)){
                    $count1 = 0;
                    foreach($arr_hn as $k1 => $v1){
                        $count1 = $count1 + $v1->amount;

                    }
                    $arr_hn_f= array('code' =>'027', 'state' => 'Hà Nội','amount' => $count1);
                    array_push($arr2,$arr_hn_f);
                }
            }
            $d3=[];
            if(!empty($data_node)){
                foreach($data_node as $k3 => $v3){
                    foreach($arr as $k4 => $v4){
                        if($v4->name == $v3['state']){
                            $d31['code'] = $v3['code'];
                            $d31['state'] = $v3['state'];
                            $d31['amount'] = $v4->amount;
                            $d3[]= $d31;

                        }

                    }

                }

            }
            if(!empty($arr2)){
                $d3 = array_merge($d3,$arr2);
            }
            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => $d3
                )
            );
        }
    }
    public function update_status_card(Request $request){
        $order_code = $request->input('order_code');
        $query = DB::table('cscart_nk_vas_order');
        $query->where('OrderCode','=', $order_code);
        $query->where('Status','=', 'New');
        $data = $query->select('OrderID')->get();
        $order_id = 0;
        if(!empty($data[0])){
            $order_id = (int)$data[0]->OrderID;
            if($order_id > 0){
                DB::table('cscart_nk_vas_order')
                    ->where('OrderID', $order_id)
                    ->update(['Status' => 'Cancelled']);
                DB::table('cscart_nk_vas_order_payment')
                    ->where('OrderID', $order_id)
                    ->update(['PaymentStatus' => 'F']);
            }
            return response()->json(
                array(
                    'status' => 200,
                    'message' => 'Thành công'
                )
            );
        }else{
            return response()->json(
                array(
                    'status' => 400,
                    'message' => 'Có lỗi xảy ra'
                )
            );
        }
    }

}