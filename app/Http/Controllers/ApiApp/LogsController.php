<?php
/**
 * Created by PhpStorm.
 * User: nguyenkim
 * Date: 1/3/19
 * Time: 2:49 PM
 */

namespace App\Http\Controllers\ApiApp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\ApiApp\Repositories\LogsRepository;

class LogsController extends Controller
{
    protected $repo;
    public function __construct(LogsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request){
        return $this->respond(
            $this->repo->create($request)
        );
    }
    public function update(Request $request,$id){
        return $this->respond(
            $this->repo->update($request,$id)
        );
    }
}