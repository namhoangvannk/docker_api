<?php
/**
 * NGUYEN KIM
 * Created by NGUYEN KIM
 * User: Dan.SonThanh
 * Date: 18/05/2018
 * Time: 2:31 PM
 */

namespace App\Http\Controllers\APIQuery;
use App\Http\Controllers\Controller;
use App\Src\APIQuery\Repositories\APIQueryRepository;
use Illuminate\Http\Request;

class APIQueryController extends Controller
{
    protected $repo;

    public function __construct(APIQueryRepository $repo){
        $this->repo = $repo;
    }

    public function APIquery(Request $request){
        return $this->respond(
            $this->repo->db_query($request)
        );
    }

}