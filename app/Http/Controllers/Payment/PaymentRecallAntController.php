<?php
namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Payment\Repositories\PaymentRecallAntRepository;

class PaymentRecallAntController extends Controller
{
    protected $repo;

    public function __construct(PaymentRecallAntRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        return $this->respond(
            $this->repo->create($request)
        );
    }
}