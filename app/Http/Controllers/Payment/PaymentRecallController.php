<?php
namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Payment\Repositories\PaymentRecallRepository;

class PaymentRecallController extends Controller
{
    protected $repo;

    public function __construct(PaymentRecallRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond( $result);
    }

    public function show(Request $request, $token)
    {
        $result = $this->repo->show($request, $token);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond($result);
    }
}