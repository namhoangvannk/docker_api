<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/21/2018
 * Time: 9:42 AM
 */
namespace App\Http\Controllers\SyncMongo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\SyncMongo\Repositories\ProductCategoriesRepository;

class ProductCategoriesController extends Controller
{
    protected $repo;

    public function __construct(ProductCategoriesRepository $repo)
    {
        $this->repo = $repo;
    }
    public function sync(Request $request)
    {
        file_put_contents('data_log.log', var_export($request,true),FILE_APPEND);
        return $this->respond(
            $this->repo->sync($request)
        );
    }

}