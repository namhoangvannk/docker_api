<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 08/05/2018
 * Time: 5:20 PM
 */

namespace App\Http\Controllers\WorldCup;

use App\Http\Controllers\Controller;
use App\Src\WorldCup\Repositories\WorldCupPlayerMatchesRepositories;
use Illuminate\Http\Request;

class WorldCupPlayerMatchesController extends Controller
{
    protected $repo;

    public function __construct(WorldCupPlayerMatchesRepositories $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request) {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respondCreated($result);
    }
}