<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 9:56 AM
 */

namespace App\Http\Controllers\WorldCup;

use App\Http\Controllers\Controller;
use App\Src\WorldCup\Repositories\WorldCupRepositories;
use Illuminate\Http\Request;

class WorldCupController extends Controller
{
    protected $repo;

    public function __construct(WorldCupRepositories $repo)
    {
        $this->repo = $repo;
    }

    public function ranks(Request $request) {
        return $this->respond(
            $this->repo->ranks($request)
        );
    }
    public function guesses(Request $request, $id) {
        $result = $this->repo->guesses($request, $id);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond($result);
    }
}