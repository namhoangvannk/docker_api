<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 10:55 AM
 */

namespace App\Http\Controllers\WorldCup;

use App\Http\Controllers\Controller;
use App\Src\WorldCup\Repositories\WorldCupPlayersRepositories;
use Illuminate\Http\Request;

class WorldCupPlayersController extends Controller
{
    protected $repo;

    public function __construct(WorldCupPlayersRepositories $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respondCreated($result);
    }

    public function index(Request $request) {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function show(Request $request, $player_id) {
        return $this->respond(
            $this->repo->show($request, $player_id)
        );
    }

    public function update(Request $request, $player_id)
    {
        $result = $this->repo->update($request, $player_id);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond( $result);
    }
    public function updateuser()
    {
        $result = $this->repo->updateuser();
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond( $result);
    }
    public function scoring(Request $request)
    {
        $result = $this->repo->scoring($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond( $result);
    }
}