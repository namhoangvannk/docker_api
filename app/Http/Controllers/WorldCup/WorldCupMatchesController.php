<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 9:56 AM
 */

namespace App\Http\Controllers\WorldCup;

use App\Http\Controllers\Controller;
use App\Src\WorldCup\Repositories\WorldCupMatchesRepositories;
use Illuminate\Http\Request;

class WorldCupMatchesController extends Controller
{
    protected $repo;

    public function __construct(WorldCupMatchesRepositories $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respondCreated($result);
    }

    public function index(Request $request) {
        return $this->respond(
            $this->repo->all($request)
        );
    }
    public function updatefinal()
    {
        $result = $this->repo->updatefinal();
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond( $result);
    }
    public function update(Request $request, $code)
    {
        $result = $this->repo->update($request, $code);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond( $result );
    }
    public function show(Request $request, $id) {
        return $this->respond(
            $this->repo->show($request, $id)
        );
    }

}