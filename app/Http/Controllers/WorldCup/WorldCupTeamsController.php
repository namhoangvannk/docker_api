<?php

namespace App\Http\Controllers\WorldCup;

use App\Http\Controllers\Controller;
use App\Src\WorldCup\Repositories\WorldCupTeamsRepositories;
use Illuminate\Http\Request;

class WorldCupTeamsController extends Controller
{
    protected $repo;

    public function __construct(WorldCupTeamsRepositories $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respondCreated($result);
    }

    public function index(Request $request) {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function show(Request $request, $team_id) {
        return $this->respond(
            $this->repo->show($request, $team_id)
        );
    }

    public function update(Request $request, $team_id)
    {
        $result = $this->repo->update($request, $team_id);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond( $result);
    }

    public function scoring(Request $request, $team_id)
    {
        $result = $this->repo->scoring($request, $team_id);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond( $result);
    }
}