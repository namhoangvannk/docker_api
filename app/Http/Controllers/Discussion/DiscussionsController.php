<?php
namespace App\Http\Controllers\Discussion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Discussions\Repositories\DiscussionsRepository;

class DiscussionsController extends Controller
{
    protected $repo;

    public function __construct(DiscussionsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        return $this->respondCreated(
            $this->repo->create($request)
        );
    }

    public function update(Request $request, $order_id)
    {
        return $this->respond(
            $this->repo->update($request, $order_id)
        );
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }


}
