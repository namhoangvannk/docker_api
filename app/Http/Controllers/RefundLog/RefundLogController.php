<?php
namespace App\Http\Controllers\RefundLog;

use App\Http\Controllers\Controller;
use App\Src\RefundLog\Repositories\RefundLogRepository;
use Illuminate\Http\Request;

class RefundLogController extends Controller
{
    protected $repo;

    public function __construct(RefundLogRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request,$id)
    {
        return $this->respond(
            $this->repo->index($request,$id)
        );
    }

    public function create(Request $request)
    {
        return $this->respond(
            $this->repo->create($request)
        );
    }
}
