<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 14/06/2019
 * Time: 9:15 AM
 */

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Common\Repositories\RedisRepository;

class RedisController extends Controller {

    protected $repo;

    public function __construct(RedisRepository $repo){
        $this->repo     = $repo;
    }

    public function getRedisByKey(Request $request,$key){
        return $this->respond(
            $this->repo->getRedisByKey($request, $key)
        );
    }

    public function setRedisByKey(Request $request){
        $result = $this->repo->setRedisByKey($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respondCreated($result);
    }

    public function setRedisTime2LiveByKey(Request $request){
        $result = $this->repo->setRedisTime2LiveByKey($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respondCreated($result);
    }

}