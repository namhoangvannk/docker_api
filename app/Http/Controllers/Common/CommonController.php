<?php
namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Common\Repositories\CommonRepository;

class CommonController extends Controller
{
    protected $repo;

    public function __construct(CommonRepository $repo)
    {
        $this->repo = $repo;
    }

    public function generateQRCode(Request $request)
    {
        return $this->respond(
            $this->repo->generateQRCode($request)
        );
    }
	
	public function createApiLog(Request $request)
    {
        return $this->respond(
            $this->repo->createApiLog($request)
        );
    }



    public function generateQRCodePartner(Request $request){
        return $this->respond(
            $this->repo->generateQRCodePartner($request)
        );
    }

}
