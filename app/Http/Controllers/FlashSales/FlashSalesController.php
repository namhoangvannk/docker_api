<?php
namespace App\Http\Controllers\FlashSales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\FlashSales\Repositories\FlashSalesRepository;

class FlashSalesController extends Controller
{
    protected $repo;

    public function __construct(FlashSalesRepository $repo)
    {
        $this->repo = $repo;
    }
    public function sync(Request $request)
    {
        return $this->respond(
            $this->repo->setRedisValue($request)
        );
    }
    public function all(Request $request)
    {
        return $this->respond(
            $this->repo->allRedisValue($request)
        );
    }
}
