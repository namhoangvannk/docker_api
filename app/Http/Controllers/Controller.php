<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected $statusCode = 200;

    /**
     * Status code getter.
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Status code setter.
     *
     * @param  $code int
     * @return $this
     */
    public function setStatusCode($code)
    {
        $this->statusCode = $code;

        return $this;
    }

    /**
     * Response to client.
     *
     * @param array $content Response data
     * @param array $headers Response headers
     * @return mixed
     */
    public function respond($content = null, $headers = [])
    {
        return response($content, $this->getStatusCode())->withHeaders($headers);
    }

    /**
     * 400 - The request was invalid.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function respondBadRequest($message = 'The request was invalid.')
    {
        return $this
            ->setStatusCode(400)
            ->respondWithError($message);
    }

    /**
     * Response 404.
     *
     * @param string $message
     * @return mixed
     */
    public function respondNotFound($message = 'Not Found')
    {
        return $this
            ->setStatusCode(404)
            ->respondWithError($message);
    }

    /**
     * Response 204.
     *
     * @return mixed
     */
    public function respondNoContent()
    {
        return $this
            ->setStatusCode(204)
            ->respond();
    }

    /**
     * Response 201.
     *
     * @param array $data
     * @return mixed
     */
    public function respondCreated($data = null)
    {
        return $this
            ->setStatusCode(201)
            ->respond($data);
    }

    /**
     * Response 500.
     *
     * @param string $message
     * @return mixed
     */
    public function respondInternalError($message = 'Internal Server Error')
    {
        return $this
            ->setStatusCode(500)
            ->respondWithError($message);
    }

    /**
     * Response with error.
     *
     * @param string $message
     * @param int $code
     * @return mixed
     */
    public function respondWithError($message, $code = null)
    {
        $data = [
            'error' => [
                'message' => $message,
            ],
        ];

        if ($code) {
            $data['error']['code'] = $code;
        }

        return $this->respond($data);
    }

    /**
     * Convert format error.
     * @param object $data
     * @return mixed
     */
    public function convertFormatError($data)
    {
        return $data->toArray();
    }
}
