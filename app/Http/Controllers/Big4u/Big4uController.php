<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 20/03/2018
 * Time: 4:17 PM
 */
namespace App\Http\Controllers\Big4u;

use App\Http\Controllers\Controller;
use App\Src\Big4u\Repositories\Big4uRepository;
use Illuminate\Http\Request;


class Big4uController extends Controller
{
    protected $repo;

    public function __construct(Big4uRepository $repo)
    {
        $this->repo = $repo;
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respondCreated($result);
    }

    public function index(Request $request) {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function update(Request $request, $code)
    {
        $result = $this->repo->update($request, $code);
        if (isset($result['errors']) && $result['errors'])
            $this->setStatusCode(500);
        return $this->respond(
            $this->repo->update($request, $code)
        );
    }

    public function trackingStatus(Request $request)
    {
        return $this->respond(
            $this->repo->trackingStatus($request)
        );
    }

    public function searchProduct(Request $request, $id){
        return $this->respond(
            $this->repo->searchProduct($request, $id)
        );
    }

    public function report(Request $request){
        return $this->respond(
            $this->repo->report($request)
        );
    }

    public function cronUnsetCoupons(Request $request){
        return $this->respond(
            $this->repo->cronUnsetCoupons($request)
        );
    }
    // dan moi them
    public function createBannerPDP(Request $request){
        return $this->respond(
            $this->repo->createBannerPDP($request)
        );
    }
    public function getAllBannerPDP(Request $request){
        return $this->respond(
            $this->repo->getAllBannerPDP($request)
        );
    }

    public function getBannerByPromotionID(Request $request,$id){
        return $this->respond(
            $this->repo->getBannerByPromotionID($request,$id)
        );
    }

    public function updateBannerByPromotionID(Request $request,$promotion_id){
        return $this->respond(
            $this->repo->updateBannerByPromotionID($request,$promotion_id)
        );
    }

    public function checkIsShowBannerPDP($product_id){
        return $this->respond(
            $this->repo->checkIsShowBannerPDP($product_id)
        );
    }

    public function updateSingleBannerPDP(Request $request,$m_id){
        return $this->respond(
            $this->repo->updateSingleBannerPDP($request,$m_id)
        );
    }
}