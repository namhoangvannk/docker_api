<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:37 AM
 */

namespace App\Http\Controllers\Sync;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Sync\Repositories\SyncRepository;

class SyncController extends Controller
{
    protected $repo;

    public function __construct(SyncRepository $repo)
    {
        $this->repo = $repo;
    }

    public function blog(Request $request)
    {
        return $this->respond(
            $this->repo->blog($request)
        );
    }
    public function page(Request $request)
    {
        return $this->respond(
            $this->repo->page($request)
        );
    }

}