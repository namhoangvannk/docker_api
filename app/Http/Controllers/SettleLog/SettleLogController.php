<?php
namespace App\Http\Controllers\SettleLog;

use App\Http\Controllers\Controller;
use App\Src\SettleLog\Repositories\SettleLogRepository;
use Illuminate\Http\Request;

class SettleLogController extends Controller
{
    protected $repo;

    public function __construct(SettleLogRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request,$id)
    {
        return $this->respond(
            $this->repo->index($request,$id)
        );
    }

    public function create(Request $request)
    {
        return $this->respond(
            $this->repo->create($request)
        );
    }
}
