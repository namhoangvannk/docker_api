<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 02/02/2018
 * Time: 11:27 AM
 */

namespace App\Http\Controllers\System;


use App\Http\Controllers\Controller;
use App\Src\System\Repositories\NkActionsRepository;
use Illuminate\Http\Request;

class NkActionsController extends Controller
{
    protected $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NkActionsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']))
            $this->setStatusCode(500);

        return $this->respondCreated($result);
    }


    public function show(Request $request, $id)
    {
        return $this->respond(
            $this->repo->show($request, $id)
        );
    }

    public function update(Request $request, $id)
    {
        $update = $this->repo->update($request, $id);
        if (isset($update['errors']))
            $this->setStatusCode(404);

        return $this->respond($update);
    }
}