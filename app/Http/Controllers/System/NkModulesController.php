<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Src\System\Repositories\NkModulesRepository;

class NkModulesController extends Controller
{
    protected $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NkModulesRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']))
            $this->setStatusCode(500);

        return $this->respondCreated($result);
    }


    public function show(Request $request, $id)
    {
        return $this->respond(
            $this->repo->show($request, $id)
        );
    }

    public function update(Request $request, $id)
    {
        $update = $this->repo->update($request, $id);
        if (isset($update['errors']))
            $this->setStatusCode(404);

        return $this->respond($update);
    }
}
