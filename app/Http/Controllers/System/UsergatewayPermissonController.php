<?php
namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Src\System\Repositories\UsergatewayPermissonRepository;

class UsergatewayPermissonController extends Controller
{
    protected $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UsergatewayPermissonRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }

    public function create(Request $request)
    {
        $result = $this->repo->create($request);
        if (isset($result['errors']))
            $this->setStatusCode(500);

        return $this->respondCreated($result);
    }

    public function update(Request $request, $id)
    {
        $update = $this->repo->update($request, $id);
        if (isset($update['errors']))
            $this->setStatusCode(404);

        return $this->respond($update);
    }
}
