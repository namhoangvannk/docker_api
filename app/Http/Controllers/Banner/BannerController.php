<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:37 AM
 */

namespace App\Http\Controllers\Banner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Src\Banner\Repositories\BannerRepository;

class BannerController extends Controller
{
    protected $repo;

    public function __construct(BannerRepository $repo)
    {
        $this->repo = $repo;
    }
    public function index(Request $request)
    {
        return $this->respond(
            $this->repo->all($request)
        );
    }
    public function create(Request $request)
    {
        return $this->respond(
            $this->repo->create($request)
        );
    }
    public function delete(Request $request, $id)
    {
        return $this->respond(
            $this->repo->delete($request,$id)
        );
    }
    public function show(Request $request, $id)
    {
        return $this->respond(
            $this->repo->show($request,$id)
        );
    }
    public function update(Request $request, $id)
    {
        return $this->respond(
            $this->repo->update($request,$id)
        );
    }
    public function sync(Request $request)
    {
        return $this->respond(
            $this->repo->sync($request)
        );
    }
    public function sync2(Request $request)
    {
        return $this->respond(
            $this->repo->sync2($request)
        );
    }

}