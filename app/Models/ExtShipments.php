<?php
namespace App\Models;

class ExtShipments extends Model
{
    protected $primaryKey = 'shipment_id';
    protected $table = 'nk_ext_shipments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array a
     */
    protected $fillable = [
        'id',
        'shipment_id',
        'vendor_id',
        'main_code',
        'tracking_id',
        'evt_status',
        'evt_description',
        'evt_datetime',
        'evt_city',
        'evt_postcode',
        'evt_state',
        'evt_country',
        'content'

    ];
}