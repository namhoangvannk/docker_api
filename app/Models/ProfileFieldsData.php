<?php
namespace App\Models;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class ProfileFieldsData extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'object_id';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_profile_fields_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'object_id',
        'object_type',
        'field_id',
        'value'
    ];
}
