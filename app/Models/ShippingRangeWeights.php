<?php

namespace App\Models;

class ShippingRangeWeights extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_range_weights';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_weight',
        'to_weight',
        'description'
    ];
}
