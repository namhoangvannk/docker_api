<?php

namespace App\Models;

class CscartNkUserGateway extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_user_gateway';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_name',
        'password',
        'is_actived'
    ];

    protected $attributes = [
        'client_name' => null,
        'password' => null,
        'is_actived' => null
    ];

    public static function checkValidUser($client_name, $password)
    {
        return CscartNkUserGateway::where('is_actived', 1)
            ->where('client_name', (string)$client_name)
            ->where('password', (string)$password)
            ->first();
    }
}
