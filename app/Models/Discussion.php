<?php
namespace App\Models;

class Discussion extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'thread_id';
    protected $table = 'cscart_discussion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array a
     */
    protected $fillable = [
        'thread_id',
        'object_id',
        'object_type',
        'type',
        'company_id'
    ];
}