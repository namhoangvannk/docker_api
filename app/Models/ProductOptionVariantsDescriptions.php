<?php

namespace App\Models;

class ProductOptionVariantsDescriptions extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'variant_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_option_variants_descriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'variant_id',
        'lang_code',
        'variant_name',
        'variant_name_rich'
    ];

    
}
