<?php
namespace App\Models;

class ProductPopularity extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'product_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_popularity';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'viewed',
        'added',
        'deleted',
        'bought',
        'total'
    ];

}
