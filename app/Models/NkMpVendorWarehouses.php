<?php
namespace App\Models;

class NkMpVendorWarehouses extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_mp_vendor_warehouses';
}
