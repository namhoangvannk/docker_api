<?php
namespace App\Models;

class NkTragop extends Model
{
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_tragop';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'hoten',
        'cmnd_so',
        'cmnd_noicap',
        'cmnd_ngay',
        'cmnd_thang',
        'cmnd_nam',
        'diachi',
        'dienthoai',
        'email',
        'sothe',
        'nganhang',
        'hieuluc_thang',
        'hieuluc_nam',
        'hethan_thang',
        'hethan_nam',
        'created',
        'status',
        'thangtra',
        'chuyendoi'
    ];
}
