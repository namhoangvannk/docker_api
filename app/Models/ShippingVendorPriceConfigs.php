<?php
namespace App\Models;

class ShippingVendorPriceConfigs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_vendor_price_configs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_route_config_id',
        'range_weight_id',
        'price'
    ];
}
