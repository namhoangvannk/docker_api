<?php
namespace App\Models;

class NkOrderPromotionDetails extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    /**
     * The table associated with the model.
     *
     * @var string     */
    protected $table = 'cscart_nk_order_promotion_details';

    protected $fillable = [
        'order_id',
        'promotion_id',
        'priority',
        'discount_percent',
        'base_amount',
        'discount_amount'
    ];
}

