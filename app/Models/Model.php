<?php

namespace App\Models;

use Illuminate\Support\Str;

class Model extends \Illuminate\Database\Eloquent\Model
{
    public static function getColumns()
    {
        $tableName = str_replace('\\', '', Str::snake(class_basename(get_called_class())));

        return \Schema::getColumnListing($tableName);
    }

    /**
     * Retrieve transformer class.
     *
     * @return string
     */
    public static function getTransformerClass()
    {
        return str_replace('Models', 'Transformers', get_called_class());
    }
}
