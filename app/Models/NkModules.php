<?php
namespace App\Models;

use Validator;

class NkModules extends Model
{
    private $errors;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_modules';

    protected $fillable = [
        'module_code',
        'module_name'
    ];

    protected $rules = [
        'module_code' => 'required',
        'module_name' => 'required'
    ];

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function actions()
    {
        return $this->hasMany('App\Models\NkActions', 'module_id');
    }

}
