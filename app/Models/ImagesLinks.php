<?php

namespace App\Models;

class ImagesLinks extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'pair_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_images_links';

    protected $fillable = [
        'object_id',
        'object_type',
        'image_id',
        'detailed_id',
        'type',
        'position'
    ];

}
