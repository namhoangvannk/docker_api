<?php

namespace App\Models;

class ProductOptions extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'option_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'company_id',
        'option_type',
        'inventory',
        'regexp',
        'required',
        'multiupload',
        'allowed_extensions',
        'max_file_size',
        'missing_variants_handling',
        'status',
        'position',
        'value',
        'expired',
        'old_expired',
        'nk_option_type',
        'nk_option_image',
        'nk_option_show',
        'expired1',
        'available_time',
        'nk_uu_dai_len_den',
        'date_start',
        'date_end',
        'type',
        'nk_option_key',
        'nk_prolink',
        'nk_sap_sync_option_type'
    ];

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id');
    }

    public function option_descriptions()
    {
        return $this->hasMany(ProductOptionsDescriptions::class, 'option_id');
    }
}
