<?php

namespace App\Models;

class NkShippingRangeWeights extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_shipping_range_weights';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_weight',
        'to_weight',
        'description'
    ];
}
