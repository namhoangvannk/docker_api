<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 03/06/2018
 * Time: 7:39 PM
 */

namespace App\Models;


class ExtShipmentsGHN extends Model {

    protected $primaryKey = 'shipment_id';
    protected $table = 'cscart_nk_ext_shipments_ghn_tracking';

    /**
     * The attributes that are mass assignable.
     *
     * @var array a
     */
    protected $fillable = [
        'id',
        'shipment_id',
        'main_code',
        'vendor_id',
        'tracking_id',
        'status_code',
        'description',
        'tracking_time',
        'current_warehouse',
        'created_at',
        'updated_at'
    ];
}