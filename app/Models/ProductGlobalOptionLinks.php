<?php

namespace App\Models;

class ProductGlobalOptionLinks extends Model
{
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_global_option_links';

    protected $fillable = [
        'option_id',
        'product_id'
    ];

    public function option()
    {
        return $this->belongsTo(ProductOptions::class, 'option_id');
    }

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id');
    }
}
