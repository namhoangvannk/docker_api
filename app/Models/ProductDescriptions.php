<?php

namespace App\Models;

class ProductDescriptions extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'product_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_descriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'lang_code',
        'product',
        'shortname',
        'short_description',
        'full_description',
        'meta_keywords',
        'meta_description',
        'search_words',
        'page_title',
        'mobile_short_description',
        'mobile_full_description',
        'display_name'
    ];

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id');
    }
}
