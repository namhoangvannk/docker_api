<?php
namespace App\Models;

class OrderCoupons extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_order_coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'coupon_code',
        'coupon_value'
    ];
}
