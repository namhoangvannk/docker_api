<?php

namespace App\Models;

class ShippingRoutes extends Model
{    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_routes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'org_province_id',
        'org_district_id',
        'des_province_id',
        'des_district_id'
    ];
}
