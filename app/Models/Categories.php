<?php

namespace App\Models;

class Categories extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_categories';

    protected $fillable = [
        'module_id',
        'action_code',
        'display_name',
        'mothod',
        'path'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Models\Products', 'cscart_products_categories', 'product_id', 'category_id');
    }

}
