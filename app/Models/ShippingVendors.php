<?php

namespace App\Models;

class ShippingVendors extends Model
{    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_vendors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_code',
        'vendor_name'
    ];
}
