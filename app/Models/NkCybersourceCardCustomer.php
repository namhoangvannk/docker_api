<?php

namespace App\Models;

class NkCybersourceCardCustomer extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $table = 'nk_cybersource_card_customer';

    protected $fillable = [
        'id',
        'user_id',
        'card_name',
        'payment_token',
        'card_number',
        'card_expiry_date',
        'payment_method',
        'card_type',
        'created_at'
    ];
}