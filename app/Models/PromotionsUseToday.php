<?php
namespace App\Models;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class PromotionsUseToday extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'promotion_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_promotions_use_today';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'promotion_id',
        'today',
        'bonunumber_of_usages_todayses'
    ];
}
