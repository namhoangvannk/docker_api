<?php
namespace App\Models;

class ShippingPricePackages extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_price_packages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'package_name',
        'min_hour_delivery',
        'max_hour_delivery',
        'min_day_delivery',
        'max_day_delivery',
        'is_active',
        'description'
    ];
}
