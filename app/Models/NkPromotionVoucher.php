<?php
namespace App\Models;

class NkPromotionVoucher extends Model
{
    public $timestamps = false;

   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nk_promotion_voucher';

    protected $fillable = [
        'promotion_id',
        'voucher_code',
        'order_id',
        'company_code',
        'status',
        'updated_user',
        'created_date'
    ];
}
