<?php
namespace App\Models;

class Likes extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nk_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'product_id',
        'user_id',
        'status',
        'created_at',
        'updated_at'
    ];
}
