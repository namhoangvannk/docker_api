<?php

namespace App\Models;

class SeoNames extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'object_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_seo_names';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'object_id',
        'company_id',
        'type',
        'dispatch',
        'path',
        'lang_code'
    ];

    public function products()
    {
        return $this->hasMany(Products::class, 'object_id');
    }
}
