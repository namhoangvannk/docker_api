<?php

namespace App\Models;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class ProductFeatures extends Model
{
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_features';

    public function feature_values()
    {
        return $this->hasMany(ProductFeaturesValues::class, 'feature_id');
    }

    public function feature_product()
    {
        return $this->hasMany(Products::class, 'product_id');
    }

}
