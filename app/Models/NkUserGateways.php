<?php
namespace App\Models;

use Validator;

class NkUserGateways extends Model
{
    private $errors;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_user_gateways';

    protected $fillable = [
        'user_gateway',
        'token',
        'is_actived',
        'description'
    ];

    protected $rules = [
        'user_gateway' => 'required|string',
        'token' => 'required|string'
    ];

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function actions()
    {
        return $this->belongsToMany('App\Models\NkActions', 'cscart_nk_usergateway_permisson', 'user_gateway_id', 'action_id');
    }

    public function user_gateway_ips()
    {
        return $this->hasMany('App\Models\NkUserGatewayIps');
    }

}
