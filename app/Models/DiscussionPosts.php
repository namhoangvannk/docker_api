<?php
namespace App\Models;

class DiscussionPosts extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'post_id';
    protected $table = 'cscart_discussion_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array a
     */
    protected $fillable = [
        'thread_id',
        'name',
        'status',
        'timestamp',
        'user_id',
        'ip_address',
        'parent_id',
        'tree',
        'emai',
        'phone',
        'name_clone',
        'like_counter',
        'user_role',
        'count_answer'
    ];
//    public function parent()
//    {
//        return $this->belongsTo('App\Models\DiscussionPosts','parent_id')->where('parent_id',0)->with('parent');
//    }
//
//    public function children()
//    {
//        return $this->hasMany('App\Models\DiscussionPosts','parent_id')->with('children');
//    }
//    public function parent()
//    {
//        return $this->belongsTo('DiscussionPosts', 'parent_id');
//    }
//
//    public function children()
//    {
//        return $this->hasMany('DiscussionPosts', 'parent_id');
//    }
    public function child()
    {
        return $this->hasMany('App\Models\CourseModule', 'parent');
    }
    public function children_rec()
    {
        return $this->child()->with('children_rec');
        // which is equivalent to:
        // return $this->hasMany('App\CourseModule', 'parent')->with('children_rec);
    }
// parent
    public function parent()
    {
        return $this->belongsTo('App\Models\CourseModule','parent');
    }

// all ascendants
    public function parent_rec()
    {
        return $this->parent()->with('parent_rec');
    }
    public static function getTree(string $id, array $tree = [])
    {
        $lowestLevel = DiscussionPosts::where('post_id', $id)->first();

        if (!$lowestLevel) {
            return $tree;
        }

        $tree[] = $lowestLevel->toArray();

        if ($lowestLevel->parent_id !== 0) {
            $tree = DiscussionPosts::getTree($lowestLevel->parent_id, $tree);
        }

        return $tree;

    }
}