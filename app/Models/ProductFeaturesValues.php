<?php

namespace App\Models;

use DB;

class ProductFeaturesValues extends Model
{
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_features_values';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'feature_id',
        'product_id',
        'variant_id',
        'value',
        'value_int',
        'lang_code'
    ];

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id');
    }

    public function feature()
    {
        return $this->belongsTo(ProductFeatures::class, 'feature_id');
    }
    public static function getBrand($product_id){
        $str_sql = "select D.variant from cscart_product_features_values as F 
                    LEFT JOIN cscart_product_feature_variants as V On F.variant_id = V.variant_id
                    LEFT JOIN cscart_product_feature_variant_descriptions as D On D.variant_id = V.variant_id
                    where F.product_id = '".$product_id."' 
                    and F.lang_code ='vi' 
                    and F.feature_id = 599";

        $data = DB::select($str_sql);
        if(!empty($data)){
            $data = $data[0]->variant;
        }
        return $data;
    }
}
