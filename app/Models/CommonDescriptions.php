<?php

namespace App\Models;

class CommonDescriptions extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'object_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_common_descriptions';

    protected $fillable = [
        'object_id',
        'object_type',
        'description',
        'lang_code',
        'object',
        'object_holder'
    ];

}
