<?php
namespace App\Models;

use Validator;

class NkActions extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_actions';

    protected $fillable = [
        'module_id',
        'action',
        'display'
    ];

    protected $rules = [
        'module_id' => 'required',
        'action' => 'required',
        'display' => 'required'
    ];

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function module()
    {
        return $this->belongsTo('App\Models\NkModules', 'module_id');
    }

    public function user_gateways()
    {
        return $this > belongsToMany('App\Models\UserGateways', 'cscart_nk_usergateway_permisson', 'action_id', 'user_gateway_id');
    }

}
