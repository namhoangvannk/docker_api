<?php

namespace App\Models;

class CategoryDescriptions extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_category_descriptions';

    public function category()
    {
        return $this->belongsTo(Categories::class, 'category_id');
    }
}
