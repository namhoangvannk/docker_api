<?php
namespace App\Models;

class NkShippingRegionsCities extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_shipping_regions_cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'region_id',
        'district_id',
    ];
}
