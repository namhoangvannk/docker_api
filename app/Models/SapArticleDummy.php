<?php

namespace App\Models;

class SapArticleDummy extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'SAP_ArticleDummy';

    protected $fillable = [
        'SiteCode',
        'Art_Code',
        'QUOMCode',
        'WrhsCode',
        'Quantity',
        'InptDate'
    ];

}
