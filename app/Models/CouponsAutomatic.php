<?php
namespace App\Models;

class CouponsAutomatic extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_coupons_automatic';

    protected $fillable = [
        'coupon_code',
        'coupon_name',
        'product_id',
        'amount',
        'user_id',
        'is_used',
        'email',
        'decrease_value',
        'expire_time',
        'created_at',
        'updated_at'
    ];

}
