<?php
namespace App\Models;

class OrderDetails extends Model
{
    public $timestamps = false;

	/*protected $primaryKey = ['item_id', 'order_id'];*/
	public $incrementing = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_order_details';

	protected $fillable = [
        'item_id',
		'order_id',
		'product_id',
		'product_code',
		'price',
		'amount',
		'extra',
		'modify_time_erp',
		'nk_mp_detail_error_erp',
		'sitecode',
		'warehouse',
		'reserve_code',
		'wrhsrsrv',
		'store_loc',
		'reserve_cancel'
    ];

}
