<?php
namespace App\Models;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class ProfileFields extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'field_id';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_profile_fields';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'field_name',
        'profile_show',
        'profile_required',
        'checkout_show',
        'checkout_required',
        'partner_show',
        'partner_required',
        'field_type',
        'position',
        'is_default',
        'section',
        'matching_id',
        'section',
        'matching_id',
        'class',
        'autocomplete_type',
        'checkout_position',
        'position_checkout',
        'group_checkout',
        'position_register',
        'register_show',
        'register_required',
        'position_call_request',
        'call_request_show',
        'call_request_required'
    ];
}
