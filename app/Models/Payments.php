<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 20/03/2018
 * Time: 9:16 AM
 */

namespace App\Models;


class Payments extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'payment_id';

    protected $table = 'cscart_payments';

    protected $fillable = [
        'payment_id',
        'company_id',
        'usergroup_ids',
        'position',
        'status',
        'template',
        'processor_id',
        'processor_params',
        'a_surcharge',
        'p_surcharge',
        'tax_ids',
        'localization',
        'payment_category',
        'is_mastercard',
        'nk_from_date',
        'nk_to_date',
        'nk_icon'
    ];

    protected $status = [
        'active' => 'A',
        'deactive' => 'D',
    ];

    public function descriptions()
    {
        return $this->hasMany(PaymentDescriptions::class, 'payment_id');
    }

    /**
     * Scope a query to only include active payment.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', $this->status['active']);
    }

    /**
     * Scope a query to only include category payment.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCategory($query, $category)
    {
        if(is_array($category)){
            return $query->whereIn('payment_category', $category);
        }
        return $query->where('payment_category', $category);
    }

    public function getPaymentByCategory($query,$category = '')
    {
        if(is_array($category)){
            return $this->where('payment_category', 'in', $category);
        }
        return $this->where('payment_category', $category);
    }
}