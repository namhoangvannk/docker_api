<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 20/03/2018
 * Time: 9:26 AM
 */

namespace App\Models;


class PaymentDescriptions extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'payment_id';

    protected $table = 'cscart_payment_descriptions';

    protected $fillable = [
        'payment_id',
        'payment',
        'description',
        'instructions',
        'surcharge_title',
        'lang_code'
    ];

    public function payment()
    {
        return $this->belongsTo(Payments::class, 'payment_id');
    }
}