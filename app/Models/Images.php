<?php

namespace App\Models;

class Images extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'image_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_images';

    protected $fillable = [
        'image_path',
        'image_x',
        'image_y'
    ];

}
