<?php
namespace App\Models;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class PromotionDescriptions extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'promotion_id';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_promotion_descriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'short_description',
        'detailed_description',
        'lang_code'
    ];
}
