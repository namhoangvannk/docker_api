<?php

namespace App\Models;

class ProductFeatureVariantDescriptions extends Model
{
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_feature_variant_descriptions';

    public function variant()
    {
        return $this->belongsTo(ProductOptions::class, 'variant_id');
    }
}
