<?php
namespace App\Models;

class OrderData extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'order_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_order_data';

    protected $fillable = [
        'order_id',
        'type',
        'data'
    ];
}
