<?php

namespace App\Models;

class ShippingUnits extends Model
{    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_units';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shipping_code',
        'shipping_name'
    ];
}
