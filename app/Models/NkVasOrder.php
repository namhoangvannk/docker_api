<?php
namespace App\Models;

class NkVasOrder extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'OrderID';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_vas_order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'OrderCode',
        'ProviderID',
        'ProviderValueID',
        'OrderType',
        'OrderDate',
        'CompletedDate',
        'Status',
        'PaymentDate',
        'ConfirmDate',
        'CancelledDate',
        'SentDate',
        'ServiceProviderStatus',
        'ServiceProviderMsg',
        'NextAction',
        'Quantity',
        'RefNo',
        'RefPrice',
        'TotalAmount',
        'TotalDiscount',
        'FinalAmount',
        'CustomerPhone',
        'CustomerMail',
        'CustomerID',
        'Platform',
        'PayCodes',
        'Source',
        'Medium',
        'Retry',
        'CreatedDate',
        'UpdatedDate',
        'CreatedBy',
        'UpdatedBy'
    ];
}
