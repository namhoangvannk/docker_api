<?php
namespace App\Models;

class DiscussionMessages extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'post_id';
    protected $table = 'cscart_discussion_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array a
     */
    protected $fillable = [
        'message',
        'message_title',
        'thread_id',
        'post_id'
    ];
}