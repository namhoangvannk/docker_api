<?php
namespace App\Models;

class NkTragopOrders extends Model
{
    public $timestamps = false;
    
	protected $primaryKey = 'id';
	
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_tragop_orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'prepay',
        'term',
        'interest',
        'mPayEachMonth',
        'birthday',
        'note',
        'profile_id',
        'credit_inst',
        'reject_note',
        'accept_time',
        'cmnd',
        'mprepay',
        'user_id',
        'tragop_user_id',
        'sitecode',
        'nk_user_id',
        'tragop_last_update_time',
        'begin_update_time',
        'nk_change_to_inst_time',
        'nk_last_update_time',
        'mthuho',
        'mthue',
        'mtong',
        'mchenhlech',
        'giayto',//vuong bs
        'order_verification_code',
        'crmid',
        'extid'
    ];
}
