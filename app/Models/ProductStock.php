<?php

namespace App\Models;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class ProductStock extends Model
{
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_product_stocks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'warehouse_code',
        'company_code',
        'product_code',
        'amount',
        'updated_time',
        'updated_id',
        'sap_amount'
    ];

}
