<?php
namespace App\Models;

use Validator;

class NkUsergatewayPermisson extends Model
{
    private $errors;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_usergateway_permisson';

    protected $fillable = [
        'user_gateway_id',
        'action_id'
    ];

    protected $rules = [
        'user_gateway_id' => 'required|integer'
    ];

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function action()
    {
        return $this->belongsTo('App\Models\NkActions');
    }

    public function user_gateway()
    {
        return $this->belongsTo('App\Models\NkUserGateways');
    }

}
