<?php

namespace App\Models;

class ProductOptionVariants extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'variant_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_option_variants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'option_id',
        'position',
        'modifier',
        'modifier_special',
        'modifier_type',
        'weight_modifier',
        'weight_modifier_type',
        'point_modifier',
        'point_modifier_type',
        'status',
        'nk_type',
        'nk_code',
        'nk_mp_start_date',
        'nk_mp_end_date',
        'is_show_banner',
        'url'
    ];

    
}
