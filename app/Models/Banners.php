<?php
namespace App\Models;

class Banners extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'banner_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_banners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
}
