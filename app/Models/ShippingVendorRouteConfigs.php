<?php
namespace App\Models;

class ShippingVendorRouteConfigs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_vendor_route_configs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_id',
        'route_id',
        'price_package_id',
        'status'
    ];
}
