<?php
namespace App\Models;

class NkCybersourceOrder extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $table = 'nk_cybersource_order';

    protected $fillable = [
        'id',
        'req_reference_number',
        'req_card_number',
        'req_card_expiry_date',
        'reason_code',
        'decision',
        'auth_amount',
        'req_payment_method',
        'req_card_type',
        'message',
        'req_transaction_type',
        'signed_date_time',
        'response_data',
        'created_at',
        'required_fields',
        'invalid_fields'
    ];
}