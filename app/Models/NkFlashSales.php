<?php
namespace App\Models;

class NkFlashSales extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    /**
     * The table associated with the model.
     *
     * @var string     */
    protected $table = 'cscart_nk_flash_sales';

    protected $fillable = [
        'promotion_id',
        'order_id',
        'product_id',
        'amount',
        'discount',
        'bonuses_data',
        'status', //A,D
    ];
}

