<?php
namespace App\Models;

class NkShippingVrpPriceConfigs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_shipping_vrp_price_configs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_route_config_id',
        'range_weight_id',
        'price'
    ];
}
