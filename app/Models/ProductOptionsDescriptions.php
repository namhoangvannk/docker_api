<?php

namespace App\Models;

class ProductOptionsDescriptions extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'option_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'option_id',
        'lang_code',
        'option_name',
        'option_text',
        'description',
        'comment',
        'inner_hint',
        'incorrect_message',
        'description_backup',
        'nk_popup_title',
        'nk_popup_content'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_options_descriptions';

    public function option()
    {
        return $this->belongsTo(ProductOptions::class, 'option_id');
    }
}
