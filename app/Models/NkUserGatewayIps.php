<?php
namespace App\Models;

use Validator;

class NkUserGatewayIps extends Model
{
    private $errors;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_user_gateway_ips';

    protected $fillable = [
        'user_gateway_id',
        'ip_address'
    ];

    protected $rules = [
        'user_gateway_id' => 'required|integer',
        'ip_address' => 'required|string'
    ];

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function actions()
    {
        return $this->belongsToMany('App\Models\NkActions', 'cscart_nk_usergateway_permisson', 'user_gateway_id', 'action_id');
    }

    public function user_gateway()
    {
        return $this->belongsTo('App\Models\NKUserGateways', 'user_gateway_id');
    }

}
