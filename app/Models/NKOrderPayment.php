<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 31/01/2018
 * Time: 11:01 AM
 */

namespace App\Models;


class NKOrderPayment extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'PaymentID';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_order_payment';

    protected $fillable = [
        'OrderID',
        'PaymentType',
        'PaymentGateway',
        'PaymentCategoryID',
        'PaymentCategoryName',
        'PaymentAmount',
        'PaymentStatus',
        'TransactionID',
        'ApproveCode',
        'AdditionalData',
        'PaymentReconcile',
        'CardNumber',
        'CommissionReconcile',
        'CardName',
        'CreatedDate',
        'UpdatedDate'
    ];
}