<?php

namespace App\Models;

class ShippingUnitConfigs extends Model
{    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_unit_configs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shipping_unit_id',
        'org_province_id',
        'des_province_id',
        'delivery_time',
        'status',
        'priority',
        'unit_price'
    ];
}
