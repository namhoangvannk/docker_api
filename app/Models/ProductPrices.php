<?php

namespace App\Models;

class ProductPrices extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'product_id';
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'price',
        'percentage_discount',
        'lower_limit',
        'usergroup_id'
    ];

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id');
    }
}
