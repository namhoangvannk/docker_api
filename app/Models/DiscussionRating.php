<?php
namespace App\Models;

class DiscussionRating extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'post_id';
    protected $table = 'cscart_discussion_rating';

    /**
     * The attributes that are mass assignable.
     *
     * @var array a
     */
    protected $fillable = [
        'post_id',
        'rating_value',
        'thread_id'
    ];
}