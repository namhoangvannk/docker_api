<?php

namespace App\Models;

class ProductsCategories extends Model
{
    public $timestamps = false;
    
    // protected $primaryKey = array('product_id', 'category_id');
    // protected $primaryKey = 'product_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_products_categories';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'category_id',
        'link_type',
        'position',
        'pos_path'
    ];

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id');
    }

    public function category()
    {
        return $this->belongsTo(Categories::class, 'category_id');
    }
}
