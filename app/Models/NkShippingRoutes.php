<?php

namespace App\Models;

class NkShippingRoutes extends Model
{    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_shipping_routes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_region_id',
        'to_region_id'
    ];
}
