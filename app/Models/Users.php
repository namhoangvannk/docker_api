<?php
namespace App\Models;

class Users extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'user_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'user_type',
        'user_login',
        'referer',
        'is_root',
        'company_id',
        'last_login',
        'timestamp',
        'password',
        'salt',
        'firstname',
        'lastname',
        'company',
        'email',
        'phone',
        'fax',
        'url',
        'tax_exempt',
        'lang_code',
        'birthday',
        'purchase_timestamp_from',
        'purchase_timestamp_to',
        'responsible_email',
        'last_passwords',
        'password_change_timestamp',
        'api_key',
        'janrain_identifier',
        'confirmed',
        'identifier',
        'social_id',
        'is_update_socical',
        'avatar_socical'
    ];


}
