<?php
namespace App\Models;

class NkVasOrderPayment extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'PaymentID';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_nk_vas_order_payment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'OrderID',
        'PaymentType',
        'PaymentGateway',
        'PaymentAmount',
        'PaymentStatus',
        'TransactionID',
        'RefID',
        'Bank',
        'CardNo',
        'PaymentReconcile',
        'CommissionReconcile',
        'CreatedDate',
        'UpdatedDate',
        'CreatedBy',
        'UpdatedBy'
    ];
}
