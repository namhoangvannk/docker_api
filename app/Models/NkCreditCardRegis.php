<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 28/12/2017
 * Time: 10:01 AM
 */

namespace App\Models;


class NkCreditCardRegis extends Model
{
    protected $table = 'cscart_nk_creditcard_regis';
    protected $primaryKey = 'register_id';
    const CREATED_AT = 'createdTime';
    const UPDATED_AT = 'updatedTime';

    protected $fillable = [
        'register_id',
        'CustomerName',
        'Email',
        'Phone',
        'UserId',
        'PersonalId',
        'LocationId',
        'IncomeLevel',
        'ReceiveIncomeMethod',
        'Partner',
        'status',
        'CreditCardType',
        'NKcardNumber',
        'NKcustomerType',
        'CouponRewardType',
        'CouponRewardStatus',
        'CouponCode',
        'ApprovedTime',
        'ApprovedBy',
        'RejectedTime',
        'RejectedBy',
        'ActivatedTime',
        'ActivatedBy',
        'createdBy',
        'updateBy'
    ];

}