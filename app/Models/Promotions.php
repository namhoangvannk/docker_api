<?php
namespace App\Models;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class Promotions extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'promotion_id';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_promotions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'conditions',
        'bonuses',
        'to_date',
        'from_date',
        'priority',
        'stop',
        'zone',
        'conditions_hash',
        'number_of_usages',
        'users_conditions_hash'
    ];
}
