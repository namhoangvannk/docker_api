<?php

namespace App\Models;

class ProductFeatureVariants extends Model
{
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_product_feature_variants';

    public function feature()
    {
        return $this->belongsTo(ProductFeatures::class, 'product_id');
    }

}
