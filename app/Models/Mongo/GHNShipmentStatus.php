<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 03/06/2018
 * Time: 7:29 PM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GHNShipmentStatus extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'ghn_shipment_status';
    protected $primaryKey = 'ghn_status_code';

}