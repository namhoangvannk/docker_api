<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class ExtShipmentRequestLogs extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'ext_shipment_request_logs';
    //protected $primaryKey = 'shipment_id';
}
