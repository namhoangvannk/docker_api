<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class CommonApiLogs extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'common_api_logs';

    /*
     * request
     * response
     * created_time
     * timestamp
     * ip
     * source
     */

}
