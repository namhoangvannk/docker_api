<?php

namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Address extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = '';
    protected $primaryKey = '';


    public static function get_address($ward_id, $district_id, $citi_id)
    {
        $address = '';
        $data_citis = [];
        $data_districts = [];
        $data_wards = [];
        if(!empty($citi_id)){
            $data_citis = DB::connection('mongodb')->collection('nk_states')->where('code', $citi_id)->limit(1)->get(['state']);
        }
        if(!empty($ward_id)){
            $data_districts = DB::connection('mongodb')->collection('nk_districts')->where('code', $ward_id)->where('state_code', $citi_id)->limit(1)->get(['name']);
        }
        if(!empty($district_id)){
            $data_wards = DB::connection('mongodb')->collection('nk_wards')->where('code', $district_id)->where('district_code',$ward_id)->where('state_code',$citi_id)->limit(1)->get(['name']);
        }
        if(!empty($data_wards)){
            $address.= $data_wards[0]['name'];
        }
        if(!empty($data_districts)){
            $address.= ', '.$data_districts[0]['name'];
        }
        if(!empty($data_citis)){
            $address.= ', '.$data_citis[0]['state'];
        }
        return $address;
    }
}
