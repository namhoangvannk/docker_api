<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 04/06/2018
 * Time: 9:42 AM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class HotDealCampaign extends Eloquent {
    protected $connection = 'mongodb';
    protected $collection = 'hot_deal_campaign';
    protected $primaryKey = '_id';
//    public    $timestamps = false;
    protected $fillable = [
        'CampaignID',
        'CampaignName'
    ];
    public static function boot(){
        parent::boot();
    }

    public function product_campaign(){
        return $this->hasMany('App\Models\Mongo\HotDeal',
            'CampaignID',
            'CampaignID');
    }
}