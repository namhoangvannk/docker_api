<?php

namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class NkUsergatewayPermisson extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'sys_usergateway_permisson';
    // protected $primaryKey = 'product_id';
}
