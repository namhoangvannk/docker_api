<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class RefundLog extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'cscart_order_refund_action_logs';
    protected $primaryKey = 'action_id';
}
