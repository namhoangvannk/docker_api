<?php
/**
 * Created by PhpStorm.
 * User: nguyenkim
 * Date: 1/3/19
 * Time: 2:51 PM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class LogComments extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'log_commments';
    protected $primaryKey = '_id';
    public    $timestamps = true;

    protected $fillable = [
        'request_id',
        'title',
        'input',
        'output',
        'object_id',
        'thread_id',
        'created_at',
        'updated_at'
    ];

    protected $attributes = [
    ];
    protected $casts = [
    ];
    public static function boot(){
        parent::boot();
    }
}