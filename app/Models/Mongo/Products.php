<?php

namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class Products extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'products';
    protected $primaryKey = 'product_id';


    public static function findAll()
    {
        $products = Products::all();
        return $products;
    }



}
