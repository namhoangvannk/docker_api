<?php
namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class NkShippingRegionsCities extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'shipping_regions_cities';
    protected $primaryKey = '_id';

}
