<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 21/03/2018
 * Time: 10:55 AM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FlagCoupon extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'nk_flag_coupon';
    protected $primaryKey = '_id';
    public $timestamps = false;
    protected $fillable = [
        "coupon_3",
        "coupon_6",
        "coupon_10",
        "CreateAt",
    ];
    public static function boot()
    {
        parent::boot();

        self::updating(function($model){
            $model->CreateAt = new \DateTime("now");
        });
    }
}