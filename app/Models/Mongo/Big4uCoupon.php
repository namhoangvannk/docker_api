<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 21/03/2018
 * Time: 10:53 AM
 */

namespace App\Models\Mongo;

use App\Helpers\SMS\Sms;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Big4uCoupon extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'big4u_coupon';
    protected $primaryKey = '_id';
    public $timestamps = false;

    protected $fillable = [
        'CouponCode',
        'ProductID',
        'PromotionID',
        'CustomerName',
        'CustomerPhone',
        'CustomerMail',
        'Type',
        'IssuedDate',//ngày cấp mã giảm giá
        'OrderID',//đơn hàng được áp dụng mã giảm giá
        'Status',// 0:inactive - 1:active
        'ExpireDate'
    ];

    protected $attributes = [
        'CouponCode' => '',
        'ProductID' => '',
        'PromotionID' => '',
        'CustomerName' => '',
        'CustomerPhone' => '',
        'CustomerMail' => '',
        'IssuedDate' => '',
        'Type'=> null,
        'OrderID' => null,
        'Status' => 1,
        'ExpireDate' => ''
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $duration = strtotime("+".Config::get('campaign.sancp.duration')." days");
            $end_date = strtotime(Config::get('campaign.sancp.end_date'));
            if($duration > $end_date){
                $duration = $end_date;
            }
            $model->IssuedDate = date('Y-m-d H:i:s');
            $model->ExpireDate = date('Y-m-d 23:59:59',$duration);
        });

        self::created(function($model){
//            SMS
//            if(Config::get('campaign.big4u.notify.sms')){
//                $code = strtoupper($model->CouponCode);
//                $expire_date = date('d/m/Y', strtotime($model->ExpireDate));
//                $text = "NguyenKim gui quy khach ma $code tri gia den 6 trieu khi mua TV Samsung, chuong trinh Big4U, han dung den ngay $expire_date. Hotline 19001267. Xin cam on";
//                $sms = new Sms();
//                $sms->fill([
//                    'phone' => $model->CustomerPhone,
//                    'text' => $text,
//                ]);
//                $sms->fn_send_sms_async();
//            }
            if(Config::get('campaign.sancp.notify.email')){
                Mail::to($model->CustomerMail)->send(new \App\Mail\Big4uCoupon($model));
            }
        });
    }

    public function order()
    {
        return new \Jenssegers\Mongodb\Relations\HasOne(\App\Models\Orders::query(), $this, 'OrderID', 'order_id', 'cscart_orders');
    }

}