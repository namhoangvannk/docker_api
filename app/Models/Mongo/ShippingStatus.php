<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class ShippingStatus extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'shipping_status';
    protected $primaryKey = 'status_id';
}
