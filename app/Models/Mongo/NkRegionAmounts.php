<?php
namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class NkRegionAmounts extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'regions_products_amount';
    protected $primaryKey = 'product_id';
}
