<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 08/05/2018
 * Time: 4:01 PM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WorldCupPlayerMatches extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'worldcup_player_matches';
    protected $primaryKey = '_id';
    protected $dates = ['PredictTime'];

    protected $fillable = [
        'PlayerID',             //ID người chơi
        'MatchID',              //ID trận đấu
        'FstTeamScoreNinety',   //Số bàn thắng trong 90p của đội 1 do người chơi bình chọn
        'SecTeamScoreNinety',   //Số bàn thắng trong 90p của đội 2 do người chơi bình chọn
        'MatchResFinal',        //Kết quả trận đấu do người chơi bình chọn - FstWin: đội 1 thắng, SecWin: đội 2 thắng, Draw: hòa
        'PlayerPredictRes',     //True: bình chọn chính xác - False: bình chọn sai
        'PlayerScoreReward',    //Điểm thưởng từ việc bình chọn đúng
        'PredictTime'           //Thời gian thực hiện dự đoán
    ];

    protected $attributes = [
        'FstTeamScoreNinety'  => 0,
        'SecTeamScoreNinety'  => 0,
        'PlayerPredictRes'    => null,
        'PlayerScoreReward'   => 0,
        'MatchResFinal'       => null
    ];

    protected $casts = [
        'PlayerPredictRes' => 'boolean',
        'PlayerScoreReward' => 'integer',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->PredictTime = new \DateTime("now");
        });
    }

    /**
     * Xuất danh thông tin người chơi
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player(){
        return $this->belongsTo(WorldCupPlayers::class, 'PlayerID','PlayerID');
    }

    /**
     * Trận đấu theo dự đoán
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match(){
        return $this->belongsTo(WorldCupMatches::class, 'MatchID','MatchID');
    }
}