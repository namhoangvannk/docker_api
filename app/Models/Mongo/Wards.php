<?php
namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class Wards extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'wards';
    protected $primaryKey = 'ward_id';

}
