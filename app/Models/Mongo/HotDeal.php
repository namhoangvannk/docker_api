<?php
/**
 * NGUYEN KIM
 * Created Dan.SonThanh
 * Date: 17/04/2018
 * Time: 12:02 PM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class HotDeal extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'hot_deal';
    protected $primaryKey = '_id';
    public    $timestamps = false;

    public static $Waitting = 'waitting';
    public static $Running  = 'running';
    public static $End      = 'end';
    public static $Cancel   = 'cancel';

    protected $fillable = [
        'CampaignID',
        'ProductID',
        'ProductCode',
        'CampaignName',
        'Product',
        'StartTime',
        'EndTime',
        'Status',
        'PromoPrice',
        'StockLimit',
        'ListPriceBefore',
        'SalePriceBefore',
        'OfflinePriceBefore',
        'SpecialPriceStatusBefore',
        // 'TextShockBefore',
        'CreatedTime',
        'CreatedBy',
        'UpdatedTime',
        'UpdatedBy'
    ];

    protected $attributes = [
        'CampaignID'                => 0,
        'ProductID'                 => 0,
        'StartTime'                 => 0,
        'EndTime'                   => 0,
        'Status'                    => '',
        'StockLimit'                => 0,
        'OfflinePriceBefore'        => 0,
        'SpecialPriceStatusBefore'  => ''
    ];
    protected $casts = [

    ];

    public static function boot(){
        parent::boot();
        self::updated(function($model){
            $model->UpdatedTime = date('Y-m-d H:i:s');
        });
    }
}