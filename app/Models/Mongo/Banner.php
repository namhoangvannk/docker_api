<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:39 AM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Banner extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'banners';
    protected $primaryKey = '_id';
    public    $timestamps = false;

    protected $fillable = [
        'banner_id',
        'banner',
        'url',
        'status',
        'type',
        'target',
        'localization',
        'timestamp',
        'position',
        'position_floor_banner',
        'company_id',
        'time_start',
        'time_finish',
        'type_display',
        'category_ids',
        'subcat_ids',
        'is_recursive',
        'is_check',
        'flexible_banner_position',
        'display_type',
        'floor_position',
        'start_time',
        'end_time',
        'background_color',
        'category_menu_position',
        'category_menu_position_inner',
        'display_on',
        'nk_mp_vendor',
        'carousel_grid_location',
        'carousel_grid_position',
        'brand_ids',
        'area_ids',
        'banner_descriptions',
        'banner_images',
        'bg_image',
        'nk_banner_ext',
        'nk_mall_banners',
        'nk_mall_floor_banners',
        'banner_areas',
        'grid_position'
    ];

    protected $attributes = [
        /*'CampaignID'=> 0,
        'ProductID' => 0,
        // 'CampaignName'=> '',
        // 'Product'=>'',
        'StartTime'=>0,
        'EndTime'=>0,
        'Status' => '',
        'PromoPrice'=>0,
        'StockLimit'=>0,
        'ListPriceBefore'=>0,
        'SalePriceBefore'=>0,
        'OfflinePriceBefore'=>0,
        'SpecialPriceStatusBefore'=>'',
        // 'CreatedTime'=>'',
        // 'CreatedBy'=>'',
        // 'UpdatedTime'=>'',
        // 'UpdatedBy'=>''*/
    ];
    protected $casts = [
        'banner_id' => 'integer',
        /*'timestamp' => 'integer',
        'position' => 'integer',
        'position_floor_banner' => 'integer',
        'time_start' => 'integer',
        'time_finish' => 'integer',
        'type_display' => 'integer',
        'display_type' => 'integer',
        'floor_position' => 'integer',
        'banner_areas' => 'integer'*/
    ];
    public static function boot(){
        parent::boot();
    }
}