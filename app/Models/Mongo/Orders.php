<?php
namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class Orders extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'orders';
    protected $primaryKey = 'order_id';

}
