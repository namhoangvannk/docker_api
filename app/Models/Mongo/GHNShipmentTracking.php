<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 31/05/2018
 * Time: 9:22 AM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GHNShipmentTracking extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'ghn_shipment_tracking';
    protected $primaryKey = 'shipment_id';
    public    $timestamps = false;

    protected $fillable = [
        "shipment_id",
        "tracking_id",
        "status_code",
        "main_code",
        "vendor_id",
        "tracking_time",
        "current_warehouse",
        "description"
    ];
//    protected $attributes = [];
    protected $casts = [];

    public static function boot(){
        parent::boot();
    }

    public function status() {
        return $this->hasOne('App\Models\Mongo\GHNShipmentStatus',
            'ghn_status_code',
            'status_code');
    }
}