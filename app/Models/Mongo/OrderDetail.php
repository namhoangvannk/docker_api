<?php
namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class OrderDetail extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'order_details';
    protected $primaryKey = 'item_id';

}
