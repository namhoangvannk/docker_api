<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 8:55 AM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WorldCupMatches extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'worldcup_matches';
    protected $primaryKey = '_id';
    protected $dates = ['MatchStartTime'];

    protected $casts = [
        'PlayerID' => 'integer',
        'FstTeamScoreNinety' => 'integer',
        'FstTeamScoreTotal' => 'integer',
        'FstTeamPoint' => 'integer',
        'SecTeamScoreNinety' => 'integer',
        'SecTeamScoreTotal' => 'integer',
        'SecTeamPoint' => 'integer',
        'MatchResFinal' => 'string'
    ];
    protected $fillable = [
        'MatchID',
        'RoundCode',
        'RoundName',
        'FstTeam',
        'FstTeamScoreNinety',
        'FstTeamScoreTotal',
        'FstTeamPoint',
        'SecTeam',
        'SecTeamScoreNinety',
        'SecTeamScoreTotal',
        'SecTeamPoint',
        'MatchResFinal',
        'MatchStartTime',
    ];

    protected $attributes = [
        'FstTeamScoreNinety' => 0,
        'FstTeamScoreTotal' => 0,
        'FstTeamPoint' => 0,
        'SecTeamScoreNinety' => 0,
        'SecTeamScoreTotal' => 0,
        'SecTeamPoint' => 0,
        'MatchResFinal' => ''
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->MatchID = strtoupper(implode("_",[$model->FstTeam,$model->SecTeam,$model->RoundCode]));
            $model->MatchStartTime = new \DateTime($model->MatchStartTime);
        });
        /*self::updating(function($model){
            if((int)$model->FstTeamScoreTotal > (int)$model->SecTeamScoreTotal){
                $model->MatchResFinal = 'FstWin';
            }else if((int)$model->FstTeamScoreTotal < (int)$model->SecTeamScoreTotal){
                $model->MatchResFinal = 'SecWin';
            }else{
                $model->MatchResFinal = 'Draw';
            }
        });*/
    }

    /**
     * Đính kèm thông tin đội thứ nhất
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fst_team(){
        return $this->belongsTo(WorldCupTeams::class, 'FstTeam','TeamID');
    }

    /**
     * Đính kèm thông tin đội thứ 2
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sec_team(){
        return $this->belongsTo(WorldCupTeams::class, 'SecTeam','TeamID');
    }

    /**
     * Xuất danh sách trận đấu đính kèm dự đoán theo người chơi
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function guess(){
        return $this->hasOne(WorldCupPlayerMatches::class, 'MatchID','MatchID');
    }

    /**
     * Xuất danh sách trận đấu đính kèm các dự đoán
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function guesses(){
        return $this->hasMany(WorldCupPlayerMatches::class, 'MatchID','MatchID');
    }

    /**
     * Lấy danh sách trận đấu theo ID người chơi
     * @param $query
     * @param $player_id
     * @return mixed
     */
    public function scopePlayer($query,$player_id)
    {
        $matches = WorldCupPlayerMatches::where('PlayerID', (int)$player_id)->get(['MatchID'])->toArray();
        $matches = array_column($matches,'MatchID');
        return $query->whereIn('MatchID',$matches );
    }

}