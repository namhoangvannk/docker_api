<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:39 AM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class  Momo extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'momo';
    protected $primaryKey = '_id';
    public    $timestamps = true;


    protected $fillable = [
        'title',
        'data'
    ];

    protected $attributes = [
    ];
    protected $casts = [
    ];
    public static function boot(){
        parent::boot();
    }
}