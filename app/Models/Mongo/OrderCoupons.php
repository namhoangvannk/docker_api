<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class OrderCoupons extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'order_coupons';
    protected $primaryKey = 'order_id';

}
