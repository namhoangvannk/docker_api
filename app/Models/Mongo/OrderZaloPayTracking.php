<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class OrderZaloPayTracking extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'order_zalopay_tracking';
    protected $primaryKey = '_id';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'zptransid' => 'string',
    ];
}