<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 07/05/2018
 * Time: 11:30 AM
 */

namespace App\Models\Mongo;

use App\Helpers\Helpers;
use App\Helpers\SMS\Sms;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PaymentRecallAnt extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'payment_recall';
    protected $primaryKey = '_id';
    protected $dates = ['created_at', 'updated_at', 'ExpireDate'];

    protected $fillable = [
        'OrderID',
        'Product',
        'ProductID',
        'Total',
        'Status',
        'PaymentStatus',
        'PaymentID',
        'PaymentMethod',
        'PaymentCategory',
        'CustomerEmail',
        'CustomerPhone',
        'ExpireDate'
    ];
    protected $attributes = [];

     public static function boot()
     {
         parent::boot();

         self::creating(function($model){
             $model->ExpireDate = new DateTime("+1 days");
         });

//         self::created(function($model){
//
//             //$url = 'https://www.nguyenkim.com/index.php?dispatch=nk_mp_order.payment_recall_order&token='.$model->_id;
//             $url = 'http://test.nguyenkimonline.com/index.php?dispatch=nk_mp_order.payment_recall_order&token='.$model->_id;
//             $url = curl_escape(curl_init(),$url);
//             $client = new \GuzzleHttp\Client();
//             $response = $client->request('GET', "https://api-ssl.bitly.com/v3/shorten?access_token=be4a12111eea480b2bc39d1c643ffb56c05a0e3a&longUrl=$url");
//             $body = $response->getBody();
//             $data = $body->getContents();
//             $data = json_decode($data,true);
//             $link = $data['data']['url'];
//             file_put_contents('url.log', var_export($link,true));
//             //TODO: sent email
//             Mail::to($model->CustomerEmail)->send(new \App\Mail\PaymentRecallMail($model,$link));
//             //TODO: sent SMS
//             $code = strtoupper($model->OrderID);
//             $text = "Qui khach vui long thanh toan dat coc cho don hang $code tai day: $link  de nhan uu dai hap dan. Xin lien he 19001267 de duoc ho tro";
//             $sms = new Sms();
//             $sms->fill([
//                 'phone' => $model->CustomerPhone,
//                 'text' => $text,
//             ]);
//             $sms->fn_send_sms_async();
//         });
     }

}