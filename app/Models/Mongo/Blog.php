<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:39 AM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Blog extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'blog';
    protected $primaryKey = '_id';
    public    $timestamps = false;

    protected $fillable = [];

    protected $attributes = [];
    protected $casts = [
        'post_id' => 'integer',
        'timestamp' => 'integer',
    ];
    public static function boot(){
        parent::boot();
    }
}