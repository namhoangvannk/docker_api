<?php
namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class NkUserGateways extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'sys_user_gateways';
    protected $primaryKey = 'user_gateway';

}
