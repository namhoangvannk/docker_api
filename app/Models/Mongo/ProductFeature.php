<?php
namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class ProductFeature extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'products_features_pdw';
    protected $primaryKey = 'product_id';


}
