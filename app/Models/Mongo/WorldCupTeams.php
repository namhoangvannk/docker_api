<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WorldCupTeams extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'worldcup_teams';
    protected $primaryKey = '_id';
    public $timestamps = false;

    protected $fillable = [
        'TeamID',       //Mã đội bóng (Ví dụ: VN)
        'Name',         //Tên đội (Ví dụ: vietnam)
        'Description',  //Tên đội (Ví dụ: Việt Nam)
        'Group',        //Tên bảng đấu (A,B,C,D,E)
        'Class',        //Class CSS bổ sung cờ hiển thị trên giao diện
        'Matches',      //Số trận đã đấu
        'Win',          //Thắng
        'Draw',         //Hòa
        'Loss',         //Thua
        'Points'        //Tổng điểm theo bảng đấu
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'TeamID' => 'string',
        'Name' => 'string',
        'Description' => 'string',
        'Group' => 'string',
        'Matches' => 0,
        'Win' => 0,
        'Draw' => 0,
        'Loss' => 0,
        'Points' => 0,
    ];

    protected $attributes = [
        'Class' => '',
        'Matches' => 0,
        'Win' => 0,
        'Draw' => 0,
        'Loss' => 0,
        'Points' => 0
    ];
}