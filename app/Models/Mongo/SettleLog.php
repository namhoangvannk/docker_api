<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class SettleLog extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'cscart_order_settle_action_logs';
    protected $primaryKey = 'action_id';
}
