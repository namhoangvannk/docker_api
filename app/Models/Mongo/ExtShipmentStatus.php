<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class ExtShipmentStatus extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'ext_shipment_status';
    protected $primaryKey = 'portal_id';
}
