<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BigcCouponSms extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'bigc_coupon_sms';
//    protected $primaryKey = '_id';
}