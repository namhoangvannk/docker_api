<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:39 AM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class ProductCategories extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'products_categories';
    protected $primaryKey = '_id';
    public    $timestamps = false;

    protected $fillable = [
        'product_id',
        'category_id',
        'link_type',
        'position',
        'pos_path',
        'sync_mongo'
    ];

    protected $attributes = [
    ];
    protected $casts = [

    ];
    public static function boot(){
        parent::boot();
    }
}