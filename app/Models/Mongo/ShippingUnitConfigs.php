<?php

namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class ShippingUnitConfigs extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'shipping_unit_confgis';
    // protected $primaryKey = 'product_id';
}
