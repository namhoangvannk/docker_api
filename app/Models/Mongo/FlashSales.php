<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:39 AM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class FlashSales extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'flash_sales';
    protected $primaryKey = '_id';
    public    $timestamps = false;

    protected $fillable = [
        'promotion_id',
        'product_id',
        'price',
        'fs_price',
        'timestamp',
    ];
    public static function boot(){
        parent::boot();
    }
}