<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 29/01/2018
 * Time: 12:56 AM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class OrderZaloPayCallback extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'order_zalopay_callback';
    protected $primaryKey = '_id';
}