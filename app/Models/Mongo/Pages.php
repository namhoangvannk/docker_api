<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:39 AM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Pages extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'pages';
    protected $primaryKey = '_id';
    public    $timestamps = false;

    protected $fillable = [];

    protected $attributes = [];
    protected $casts = [
        'page_id' => 'integer',
        'avail_from_timestamp' => 'integer',
        'avail_till_timestamp' => 'integer',
    ];
    public static function boot(){
        parent::boot();
    }
}