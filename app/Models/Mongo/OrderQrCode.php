<?php
namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class OrderQrCode extends Eloquent
{
    const PRIVATE_KEY_SACOMBANK = '-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAhYUwl/KgehLrfQBPbLwjfg4znBhtfj5fd7I4fGk7w/1VIIr8
ynLBESQFh5+jOlSUNR2g7wYBFua+wzx0ti7lmNQ6Fzamuky1vdE2CuyhjyLkkp9K
6ncGFZIUgxtMm8EglMv5gbHwMW6CvKcdY6M6n8fDCwFJdS+L3toRQ/6sDFQiBFqv
CZZz0lC7bLGpSavJ/dXkaBzE0Kc0lyKQ6CuqfJZPNr1GxyCZzW6aGOR2nP7noUdG
LdVk6TAmkdzYklxsH+A0iYIVVRjsvLDcOJtCJo3PD6DJcddh7dnvaYy9rnrL2mlA
FE5OL97OQdF0Fk5msqWMP1tJ+y0LifoLeEBbcwIDAQABAoIBAAS6Ak5euG+Z29xA
9o7S2i7MMeeEtduSOa7zECzXTNZTUYAla21/QnZo4Ak9DXozJEv9EIVloQQ+huJC
cZ9Bab9PQ/8uYBiYPpbJoTAh0FZQ1eaa3PwP2pdI5JD/lmatH4ok7mA4/FfDob0r
7U5pqNmfKLhrAz6ywDDTZ2kQyuZkxkp4NE6g3zv2JY9rGgS17L+BC8Vu2UUeDqjE
30NO2ptM7PKK+aBZ/QMVv2+KXCbs1R9rKnVJwm9y7sNgfsdVRsnOZeeqYqfR07g9
812WK6MhOhU7FL62Gw1xadfkN65kh+yLKliaBhLw/fAQz+ZJp3TBGEnPUg0176yJ
irNrPAECgYEA/tfDyAO1EmT2aQe88+MKqnbsgvBK2DZwLE3daaTsgTbdkVbVnQTF
3XB/ODiiW9bJCaYVgBaC1fY85OZNrKvcQUm0j4Cg8qOeEwLl815vAoIYefBfjSFU
P49TN3UWPgcuhbRh1+MSy40JNcYUYcm/bymEjJRxL8zvmW0/+xiKOtMCgYEAhiBl
mloNwR1Pj5LJS32WXnyP3FbebQwqQcuHW1NYG8sR33plcANHmyQiTd8tZMjAA3iD
qcrwmkqiPXFhB7nqTj+He8I6CbuUD1iBPhbp6BsYEVHij/qR8k8GzB1IpHD8IPRA
6KjOMcHVf6Y/uT1hT/y1oORWQWee5JkPxo65uOECgYA7MvQP6oyNkVaMX3fNT9iS
cL3QTPGV+E9me1AL0nv+KDigIoPMzecI4bEr5jwRCXv5+dLUxmXuPh03LyisC+BG
alC/ZSn+M3PtBqdUOYOgGYiGCwinSC7B9RFnuJNKJXDxLk3sUJt1HXYeP71MWglW
UVaxMWp5qCmggnhyY+j4cQKBgH1Q550/Znp4GE3cp5aQ0F2XpBIKV+cq5pByyRTa
2gedxCU5Bm2RouWg7Sw0yQTRTBXyE5IguoxuDwe5Vx8Q5YoWcU+AORm+2Kc92gNZ
BVKHfgUwrK45PwNWMtOiyKxbpDZl//9hOMXkhMWvXvqEDtfdFRYuQSj618pd+A9c
asoBAoGBAJdBEn1ozxZpCHojEuWGWdx4x94xzA8OZAocXNaewo9y15cPqtjRb3HR
wGVMpaDyjhxVtIce2ZbfRbiyyHlgunsLtvPs5bnYbXMEAnAdr7NOflL5e2RbOUdb
IHQyMhnbUhLULRCPwolIW9moi5AHCGdx6+5olKZPVA3vmX3iPP1j
-----END RSA PRIVATE KEY-----';

    protected $connection = 'mongodb';
    protected $collection = 'order_qrcode';
    protected $primaryKey = 'order_id';

}
