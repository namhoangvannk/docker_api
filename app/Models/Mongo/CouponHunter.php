<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:39 AM
 */

namespace App\Models\Mongo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class CouponHunter extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'nk_coupon_hunter';
    protected $primaryKey = '_id';
    public    $timestamps = false;

    protected $fillable = [
        'coupon',
        'type',
        'status'
    ];

    protected $attributes = [
        'status' => 1,
    ];
    protected $casts = [

    ];
    public static function boot(){
        parent::boot();
    }
}