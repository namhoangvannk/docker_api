<?php
namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class ProductsWarehouses extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'products_warehouses';
    protected $primaryKey = 'product_id';
}
