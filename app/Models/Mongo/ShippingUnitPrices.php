<?php

namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class ShippingUnitPrices extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'shipping_unit_prices';
    protected $primaryKey = 'id';
}
