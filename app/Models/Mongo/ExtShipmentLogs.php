<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class ExtShipmentLogs extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'ext_shipment_logs';
    protected $primaryKey = 'shipment_id';

    public function status() {
        return $this->hasOne('App\Models\Mongo\ExtShipmentStatus',
            'portal_id',
            'evt_status');
    }
}
