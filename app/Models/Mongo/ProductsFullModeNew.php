<?php

namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class ProductsFullModeNew extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'products_full_mode_new';
    protected $primaryKey = 'product_id';


    public static function findAll()
    {
        $products = ProductsFullModeNew::all();
        return $products;
    }
    public static function getDetail($product_id)
    {
        $datas1 = [];
        $datas =  DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', $product_id)->limit(1)
            ->get([
                'product_id'
                ,'description'
                ,'product_code'
                ,'amount'
                ,'weight'
                ,'length'
                ,'width'
                ,'height'
                ,'list_price'
                ,'price'
                ,'nk_shortname'
                ,'product'
                ,'display_name'
                ,'brand'
                ,'variants'
                ,'product_options'
                ,'product_descriptions'
                ,'tag_image'
                ,'mobile_short_description'
                ,'mobile_full_description'
                ,'main_pair'
                ,'image_pairs'
                ,'product_features'
                ,'url_digital_images'
                ,'nk_is_shock_price'
                ,'nk_tragop_0'
                ,'tag_image'
                ,'status'
                ,'hidden_option'
                ,'text_shock'
                ,'shock_online_exp'
                ,'nk_is_shock_price'
                ,'nk_product_tags'
                ,'nk_special_tag'
                ,'offline_price'
                ,'model'
                ,'barcode'
                ,'seo_name'
                ,'meta_description'
                ,'page_title'
            ]);
        $datas1['nk_shortname'] = $datas[0]['nk_shortname'];
        $datas1['seo_name'] = $datas[0]['seo_name'];
        $datas1['product_name'] = $datas[0]['product'];
        if(!empty($datas[0]['main_pair'])){
            if(!empty($datas[0]['main_pair']['icon'])) {
                $datas1['image_thumb'] = str_replace('images/product','images/thumbnails/290/235/product',$datas[0]['main_pair']['icon']['image_path']);
            }else{
                if(!empty($datas[0]['main_pair']['detailed'])) {
                    $datas1['image_thumb'] = str_replace('images/detailed','images/thumbnails/290/235/detailed',$datas[0]['main_pair']['detailed']['image_path']);
                }
            }
        }
        return $datas1;
    }
    public static function findOne($product_code)
    {
        $product_id = 0;
        $products =  DB::connection('mongodb')->collection('products_full_mode_new')->where('product_code', $product_code)->first();
        if(!empty($products)){
            $product_id = (int)$products['product_id'];

        }
        return $product_id;
    }
    protected $attributes = [
        'comment'=> 0,
        'rating' => 0,

    ];
}
