<?php

namespace App\Models\Mongo;

use DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product Model
 * @author khoa.nguyenthanh
 * @package App\Models\Mongo
 */
class NkShippingVrpPriceConfigs extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'vendor_price_configs';
    // protected $primaryKey = 'product_id';
}
