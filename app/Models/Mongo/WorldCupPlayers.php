<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 8:55 AM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WorldCupPlayers extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'worldcup_players';
    protected $primaryKey = '_id';
    protected $dates = ['updated_at','created_at','RegisteredDate','TimeGroup','TimeSixteen','TimeQuarter','TimeSemi','TimePlayoff','TimeFinal'];

    protected $fillable = [
        'PlayerID',           // ID tham chiếu đến user NK
        'PersonalID',       // Số CMND
        'RegisteredDate',   // Ngày đăng ký tham gia
        'Status',           // Active/Inactive
        'TotalScore',       // Tổng số điểm đạt được
        'Group',            // Y: được tham gia / N: không được tham gia
        'GroupScore',       // Tổng điểm đạt được ở vòng loại
        'RankGroup',
        'TimeGroup',        // Thời Gian Dự đoán trận đầu tiên vòng bảng
        'Sixteen',          // Y: được tham gia / N: không được tham gia
        'SixteenScore',     // Tổng điểm đạt được ở vòng 1 / 16
        'RankSixteen',
        'TimeSixteen',
        'Quarter',          // Y: được tham gia / N: không được tham gia
        'QuarterScore',     // Tổng điểm đạt được ở vòng tứ kết
        'TimeQuarter',
        'RankQuarter',
        'Semi',             // Y: được tham gia / N: không được tham gia
        'SemiScore',        // Tổng điểm đạt được ở vòng bán kết
        'TimeSemi',
        'RankSemi',
        'Playoff',          // Y: được tham gia / N: không được tham gia
        'PlayoffScore',     // Tổng điểm đạt được ở vòng play off
        'TimePlayoff',
        'RankPlayoff',
        'Final',            // Y: được tham gia / N: không được tham gia
        'FinalScore',       // Tổng điểm đạt được ở vòng chung kết
        'TimeFinal',
        'RankFinal',
        'FullName',         // Họ và tên
        'Gender',           // Giới tính W or M
        'Phone',            // Sđt
        'Email'             // Email
    ];

    protected $attributes = [
        'Status'        => 'Y',
        'TotalScore'    => 0,
        'Group'         => 'Y',
        'GroupScore'    => 0,
        'TimeGroup'     => null,
        'RankGroup'     => 0,
        'Sixteen'       => 'N',
        'SixteenScore'  => 0,
        'TimeSixteen'     => null,
        'RankSixteen'     => 0,
        'Quarter'       => 'N',
        'QuarterScore'  => 0,
        'TimeQuarter'     => null,
        'RankQuarter'     => 0,
        'Semi'          => 'N',
        'SemiScore'     => 0,
        'TimeSemi'     => null,
        'RankSemi'     => 0,
        'Playoff'       => 'N',
        'PlayoffScore'  => 0,
        'TimePlayoff'     => null,
        'RankPlayoff'     => 0,
        'Final'         => 'N',
        'FinalScore'    => 0,
        'TimeFinal'     => null,
        'RankFinal'     => 0,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'PlayerID' => 'integer',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->RegisteredDate = new \DateTime($model->RegisteredDate);
        });

        self::created(function($model){

        });
    }

    public function matches()
    {
        return $this->hasMany('\App\Models\Mongo\WorldCupPlayerMatches', 'PlayerID', 'PlayerID');
    }

}