<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 21/03/2018
 * Time: 10:55 AM
 */

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Big4uProduct extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'big4u_product';
    protected $primaryKey = '_id';
    public $timestamps = false;
    protected $fillable = [
        "Campaign",
        "ProductID",
        "PromotionID",
        "CreateAt",
        "BannerType",
        "StartTime",
        "EndTime",
        "Status",
        "BannerLink",
        "LinkToPage"
    ];
}