<?php

namespace App\Models\Mongo;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class OrderZaloPay extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'order_zalopay';
    protected $primaryKey = '_id';

    const STATUS_NEW = 'new';
    const STATUS_PAID = 'paid';
    const STATUS_REFUND = 'refund';
    const STATUS_CANCELED = 'cancelled';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'apptime' => 'string',
    ];
}