<?php

namespace App\Models;

class ShippingUnitPrices extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_unit_prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_kg',
        'to_kg',
        'price'
    ];
}
