<?php
namespace App\Models;

class EmailSending extends Model
{
    protected $primaryKey = 'email_id';
    protected $table = 'nk_email_sending';

    /**
     * The attributes that are mass assignable.
     *
     * @var array a
     */
    protected $fillable = [
        'email_id',
        'receiver',
        'object_id',
        'object_type',
        'content',
        'status',
        'created_date',
        'send_date',
        'retry',
        'is_delete'
    ];
}