<?php

namespace App\Models;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class Products extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'product_id';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cscart_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'product_code',
        'product_type',
        'status',
        'company_id',
        'approved',
        'sub_approved',
        'check_product_info',
        'list_price',
        'installment_price',
        'installment_price_type',
        'amount',
        'weight',
        'length',
        'width',
        'height',
        'shipping_params',
        'template_cate',
        'text_shock',
        'shock_online_exp',
        'nk_last_sync_time',
        'offline_price'
    ];


    public function categories()
    {
        return $this->belongsToMany('App\Models\Categories', 'cscart_products_categories', 'category_id', 'product_id');
    }

    public function prices()
    {
        return $this->hasMany(ProductsPrices::class, 'product_id');
    }

    public function options()
    {
        return $this->hasMany(ProductOptions::class, 'product_id');
    }


    

}
