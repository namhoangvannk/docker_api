<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 7/25/2018
 * Time: 5:04 PM
 */
function time_ago($timestamp){
    date_default_timezone_set("Asia/Kolkata");
    //$time_ago        = strtotime($timestamp);
    $time_ago        = $timestamp;
    $current_time    = time();
    $time_difference = $current_time - $time_ago;
    $seconds         = $time_difference;
    $minutes = round($seconds / 60); // value 60 is seconds
    $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec
    $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;
    $weeks   = round($seconds / 604800); // 7*24*60*60;
    $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60
    $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
    if ($seconds <= 60){
        return "vừa mới";
    } else if ($minutes <= 60){
        if ($minutes == 1){
            return "1 phút trước";
        } else {
            return "$minutes phút trước";
        }
    } else if ($hours <= 24){
        if ($hours == 1){
            return "1 giờ trước";
        } else {
            return "$hours giờ trước";
        }
    } else if ($days <= 7){
        if ($days == 1){
            return "hôm qua";
        } else {
            return "$days ngày trước";
        }
    } else if ($weeks <= 4.3){
        if ($weeks == 1){
            return "1 tuần trước";
        } else {
            return "$weeks tuần trước";
        }
    } else if ($months <= 12){
        if ($months == 1){
            return "1 tháng trước";
        } else {
            return "$months tháng trước";
        }
    } else {
        if ($years == 1){
            return "1 năm trước";
        } else {
            return "$years năm trước";
        }
    }
}
function show_color_code($type){
    $color ='';
    if($type == 3 || $type == 4 || $type == 5){
        $color = '#458e35';
    }elseif($type == 1){
        $color = '#f5605d';
    }elseif($type == 2){
        $color = '#f79f00';
    }else{
        $color = '#adb3c0';
    }
    return $color;
}
function get_value_href($str){
    $url = preg_match('/href=["\']?([^"\'>]+)["\']?/', $str, $match);
    $info = parse_url($match[1]);
    if(isset($info['scheme']) && $info['host'] && $info['path']){
        return $info['scheme'].'://'.$info['host'].$info['path'];
    }
    return '';
}
function replace_content($str){
    if(!empty($str)){
        $str = strip_tags($str);
        $str = trim(preg_replace('/\s\s+/', ' ', $str));
    }
    return $str;
}
function replace_data_src($str){
    if(!empty($str)){

        $str = str_replace('data-src','src',$str);
        $str = str_replace('Xem thêm các tính năng','',$str);
        $str = trim(preg_replace('/\s\s+/', ' ', $str));
    }
    return $str;

}
function get_text_tra_gop($price){
    $text = '';
    if($price < 3000000){
        $text = '<div>Trả góp thủ tục đơn giản, lãi suất thấp</div>';
    }else{
        $text_price = round($price/6);
        $text_price = number_format($text_price,0,",",".");
        $text = '<div >Trả góp chỉ <b style="color:#000">'.$text_price.'đ</b>/tháng (6 tháng)</div>';
    }
    return $text;
}