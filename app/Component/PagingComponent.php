<?php

namespace App\Component;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PagingComponent
{
    private $total;
    private $offset;
    private $limit;

    public function __construct($total, $offset, $limit)
    {
        $this->total = $total;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    public function render()
    {
        return [
            'total'        => $this->total,
            'current_page' => floor($this->offset / $this->limit) + 1,
            'from'         => $this->offset > 0 ? $this->offset : 1,
            'to'           => $this->offset + $this->limit,
            'per_page'     => $this->limit,
            'last_page'    => ceil($this->total / $this->limit), ];
    }
}
