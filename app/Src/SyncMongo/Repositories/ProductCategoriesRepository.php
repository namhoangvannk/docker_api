<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/21/2018
 * Time: 9:45 AM
 */
namespace App\Src\SyncMongo\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Mongo\ProductCategories;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class ProductCategoriesRepository extends RepositoryBase
{
    protected $model;
    protected $helper;
    protected $config;

    public function __construct(Helpers $helper)
    {
        $this->helper = $helper;
        $this->model = new ProductCategories();
    }
    public function sync($request)
    {
        $datas = $request->all();
        file_put_contents('data_log.log', var_export($request,true),FILE_APPEND);
        try {
            if(!empty($datas)){
                foreach ($datas as $key => $data){
                    $data_main['product_id'] = $data['product_id'];
                    $data_main['category_id'] = $data['category_id'];
                    $data_main['link_type'] = $data['link_type'];
                    $data_main['position'] = $data['position'];
                    $data_main['pos_path'] = $data['pos_path'];
                    $rs = DB::connection('mongodb')->collection('products_categories')->insert($data_main);
                }
                return [
                    'status' => 200,
                    'message' => 'Sync thành công'
                ];
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}