<?php

namespace App\Src\Payment\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Fractal\ArraySerializer;
use App\Helpers\Helpers;
use App\Models\Mongo\PaymentRecallAnt;
use App\Models\OrderDetails;
use App\Models\Orders;
use App\Models\ProductDescriptions;
use App\Src\Payment\Transformers\PaymentRecallTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;

class PaymentRecallAntRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;

    public function __construct(
        Manager $fractal,
        PaymentRecallTransformer $transformer
    ) {
        $this->model       = new PaymentRecallAnt();
        $this->transformer = $transformer;
        $this->fractal     = $fractal;
    }

    public function create($request)
    {
        $params = $request->all();
        if (empty($params['OrderID'])) {
            return ['errors' => 'Mã đơn hàng không được rỗng'];
        }
        $order = Orders::where('order_id', intval($params['OrderID']))->first();
        if ( !$order) {
            return ['errors' => 'Đơn hàng không tồn tại'];
        }
        if ( !in_array($order->status, ['N', 'F', 'L', 'J'])) {
            return ['errors' => 'Đơn hàng không đủ điều kiện thanh toán lại'];
        }
        if ($order->payment_id == 6) {
            return ['errors' => 'Phương thức thanh toán không phù hợp'];
        }
        if (!in_array($order->src_type,[13,14])) {
            return ['errors' => 'Chỉ cho phép thanh toán lại đối với đơn hàng PreOrder'];
        }

        /* Chỉ cho phép thanh toán lại đối với phương thức thanh toán online */
        $payment = $order->payment()->first();
        $des          = $payment->descriptions()->where('lang_code', 'vi')->first();
        $order_detail = OrderDetails::where('order_id', $params['OrderID'])->first();
        $product      = ProductDescriptions::where('product_id', intval($order_detail['product_id']))->where(
            'lang_code',
            'vi'
        )->first();
        try {
            $params = [
                'OrderID'         => (string)$params['OrderID'],
                'Product'         => $product->product,
                'ProductID'       => $product->product_id,
                'Total'           => $order->total,
                'Status'          => $order->status,
                'PaymentStatus'   => $order->payment_status,
                'PaymentID'       => $order->payment_id,
                'PaymentMethod'   => $params['BankID'],
                'PaymentCategory' => $des->payment,
                'CustomerEmail'   => $order->email,
                'CustomerPhone'   => $order->b_phone,
            ];
            $model  = new PaymentRecallAnt();
            $model->fill($params);
            $model->save();
            $res = $this->fractal->setSerializer(new ArraySerializer())->createData(
                new Item($model, $this->transformer)
            )->toArray();
            //$url = 'http://test.nguyenkimonline.com/index.php?dispatch=nk_mp_order.payment_recall_order&token='.$res['Token'];

            $url = 'https://www.nguyenkim.com/index.php?dispatch=nk_mp_order.payment_recall_order2&token='.$res["Token"];
            $url = curl_escape(curl_init(),$url);
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', "https://api-ssl.bitly.com/v3/shorten?access_token=be4a12111eea480b2bc39d1c643ffb56c05a0e3a&longUrl=$url");
            $body = $response->getBody();
            $data = $body->getContents();
            $data = json_decode($data,true);
            $link = $data['data']['url'];
            return array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => $link,
            );
        } catch (\Exception $e) {
            return ['errors' => $e->getMessage()];
        }
    }

}