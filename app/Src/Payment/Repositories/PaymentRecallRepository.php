<?php
namespace App\Src\Payment\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Fractal\ArraySerializer;
use App\Helpers\Helpers;
use App\Models\Mongo\PaymentRecall;
use App\Models\OrderDetails;
use App\Models\Orders;
use App\Models\ProductDescriptions;
use App\Src\Payment\Transformers\PaymentRecallTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;

class PaymentRecallRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;

    public function __construct(
        Manager $fractal,
        PaymentRecallTransformer $transformer
    ) {
        $this->model       = new PaymentRecall();
        $this->transformer = $transformer;
        $this->fractal     = $fractal;
    }

    public function create($request)
    {
        $params = $request->all();
        if (empty($params['OrderID'])) {
            return ['errors' => 'Mã đơn hàng không được rỗng'];
        }
        $order = Orders::where('order_id', intval($params['OrderID']))->first();
        if ( !$order) {
            return ['errors' => 'Đơn hàng không tồn tại'];
        }
        if ( !in_array($order->status, ['N', 'F', 'L', 'J'])) {
            return ['errors' => 'Đơn hàng không đủ điều kiện thanh toán lại'];
        }
        if ($order->payment_id == 6) {
            return ['errors' => 'Phương thức thanh toán không phù hợp'];
        }
        if ($order->src_type !== 6) {
            return ['errors' => 'Chỉ cho phép thanh toán lại đối với đơn hàng PreOrder'];
        }
        /* Chỉ cho phép thanh toán lại đối với phương thức thanh toán online */
        $payment = $order->payment()->first();
        if ($payment->payment_category != 2) {
            return ['errors' => 'Chỉ cho phép thanh toán lại đối với phương thức thanh toán online'];
        }
        $des          = $payment->descriptions()->where('lang_code', 'vi')->first();
        $order_detail = OrderDetails::where('order_id', $params['OrderID'])->first();
        $product      = ProductDescriptions::where('product_id', intval($order_detail['product_id']))->where(
            'lang_code',
            'vi'
        )->first();
        try {
            $params = [
                'OrderID'         => (string)$params['OrderID'],
                'Product'         => $product->product,
                'ProductID'       => $product->product_id,
                'Total'           => $order->total,
                'Status'          => $order->status,
                'PaymentStatus'   => $order->payment_status,
                'PaymentID'       => $order->payment_id,
                'PaymentMethod'   => $params['BankID'],
                'PaymentCategory' => $des->payment,
                'CustomerEmail'   => $order->email,
                'CustomerPhone'   => $order->b_phone,
            ];
            $model  = new PaymentRecall();
            $model->fill($params);
            $model->save();

            return $this->fractal->setSerializer(new ArraySerializer())->createData(
                new Item($model, $this->transformer)
            )->toArray();

        } catch (\Exception $e) {
            return ['errors' => $e->getMessage()];
        }
    }

    public function show($request, $token)
    {
        $found = $this->model->find($token);
        if ( !$found) {
            return ['errors' => 'The token is invalid'];
        }
        $order_id = $found->OrderID;
        $order    = Orders::find(intval($order_id));
        if ( !$order) {
            return ['errors' => 'The order is not found'];
        }

        return $this->fractal->setSerializer(new ArraySerializer())->createData(
            new Item($found, $this->transformer)
        )->toArray();
    }
}