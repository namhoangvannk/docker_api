<?php
namespace App\Src\Payment\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class PaymentRecallTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        $expire_date = '';
        $bankId      = '';
        if ( !empty($obj->ExpireDate)) {
            /** @var Carbon $date */
            $date        = $obj->ExpireDate;
            $bankId      = $obj->PaymentMethod;
            $expire_date = $date->toDateTimeString();
        }

        return [
            'OrderID'    => $obj->OrderID,
            'Token'      => $obj->_id,
            'ExpireDate' => $expire_date,
            'BankId'     => $bankId,
            'URL'        => 'https://www.nguyenkim.com/index.php?dispatch=nk_mp_order.payment_recall_order&token=' . $obj->_id,
        ];
    }
}