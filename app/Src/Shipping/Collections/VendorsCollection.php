<?php

namespace App\Src\Shipping\Collections;

use App\Helpers\Database\Collection;

class VendorsCollection extends Collection
{
    /**
     * The table associated with the collection.
     *
     * @var string
     */
    protected $table = 'cscart_shipping_units';

}
