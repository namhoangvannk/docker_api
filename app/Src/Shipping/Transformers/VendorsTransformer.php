<?php

namespace App\Src\Shipping\Transformers;

use League\Fractal\TransformerAbstract;

class VendorsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj['id']
        ];
    }
}
