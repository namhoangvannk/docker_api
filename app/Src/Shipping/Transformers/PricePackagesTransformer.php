<?php
namespace App\Src\Shipping\Transformers;

use League\Fractal\TransformerAbstract;

class PricePackagesTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj['id']
        ];
    }
}
