<?php

namespace App\Src\Shipping\Transformers;

use League\Fractal\TransformerAbstract;

class ShippingUnitPricesTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj['id'],
            'from_kg' => (float)$obj['from_kg'],
            'to_kg' => (float)$obj['to_kg'],
            'price' => (float)$obj['price'],
        ];
    }
}
