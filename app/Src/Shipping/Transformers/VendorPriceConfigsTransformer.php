<?php
namespace App\Src\Shipping\Transformers;

use League\Fractal\TransformerAbstract;

class VendorPriceConfigsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'vendor_id' => $obj['vendor_id'],
            'org_province_id' => $obj['org_province_id'],
            'org_district_id' => $obj['org_district_id'],
            'des_province_id' => $obj['des_province_id'],
            'des_district_id' => $obj['des_district_id'],
            'package_name' => $obj['package_name'],
            'min_hour_delivery' => $obj['min_hour_delivery'],
            'max_hour_delivery' => $obj['max_hour_delivery'],
            'min_day_delivery' => $obj['min_day_delivery'],
            'max_day_delivery' => $obj['max_day_delivery'],
            'json_price' => json_decode($obj['json_price'],true)
        ];
    }
}
