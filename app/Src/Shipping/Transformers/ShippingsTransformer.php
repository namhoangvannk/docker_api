<?php
namespace App\Src\Shipping\Transformers;

use League\Fractal\TransformerAbstract;

class ShippingsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'vendor_id' => (int)$obj['vendor_id'],
            'vendor_name' => $obj['vendor_name'],
            'package_name' => $obj['package_name'],
            'min_hour_delivery' => (float)$obj['min_hour_delivery'],
            'max_hour_delivery' => (float)$obj['max_hour_delivery'],
            'min_day_delivery' => (float)$obj['min_day_delivery'],
            'max_day_delivery' => (float)$obj['max_day_delivery'],
            'price' => json_decode($obj['json_price'], true)
        ];
    }
}