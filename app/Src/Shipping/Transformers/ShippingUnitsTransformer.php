<?php

namespace App\Src\Shipping\Transformers;

use League\Fractal\TransformerAbstract;

class ShippingUnitsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj['id']
        ];
    }
}
