<?php
namespace App\Src\Shipping\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use App\Helpers\Cache\RedisManager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Src\Shipping\Transformers\ShippingsTransformer;

use App\Models\Mongo\ProductsWarehouses;
use App\Models\Mongo\NkShippingRegionsCities;
use App\Models\Mongo\NkShippingVrpPriceConfigs;


class ShippingsRepository extends RepositoryBase
{
    protected $fractal;
    protected $redis;
    protected $transform;

    public function __construct(Manager $fractal, 
                                RedisManager $redis,
                                ShippingsTransformer $transform)
    {
        $this->fractal = $fractal;
        $this->redis = $redis;
        $this->transform = $transform;
    }

    public function all($request)
    {
        $result = $this->allByMongo($request);
        return $result;
    }

    private function allByMongo($request)
    {
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 20);

        $model = ShippingVendorPriceConfigs::select(
            'vendor_id',
            'vendor_name',
            'package_name',
            'min_hour_delivery',
            'max_hour_delivery',
            'min_day_delivery',
            'max_day_delivery',
            'json_price'
        );

        if ($request->input('des_province_id') != null) {
            $model = $model->where('des_province_id', '=', intval($request->input('des_province_id')));
        }

        if ($request->input('des_district_id') != null) {
            $model = $model->where('des_district_id', '=', intval($request->input('des_district_id')));
        }

        $list = $model->skip($offset)->take($limit)->get();
        $resource = new Collection($list, $this->transform);
        $data = $this->fractal->createData($resource)->toArray();

        $paging = new PagingComponent($model->count(), $offset, $limit);
        $data['paging'] = $paging->render();
        return $data;
    }

    public function checkout($request)
    {
        $params = $request->all();
        $products = $params['products'];

        $region_cities = NkShippingRegionsCities::raw()->find(
            ['district_id' => intval($params['district_id'])]
        );

        $result = [];
        foreach($region_cities as $value) {
            $to_region_id = intval($value->region_id);
            $price_configs = NkShippingVrpPriceConfigs::raw()->find(
                ['to_region_id' => $to_region_id]
            );

            if($price_configs == null) {
                return [
                    'message' => 'NK Không hổ trợ vận chuyển đến vùng này'
                ];
                break;
            }
                
            $tmp = [];
            foreach($price_configs as $subvalue) {
                $json_price = json_decode($subvalue->json_price, true);
                $fee_ship = $this->calculateFeeShip($products, $json_price, $subvalue->vendor_code);
                $tmp = [
                    'vendor' => $subvalue->vendor_name,
                    'fee_ship' => $fee_ship,
                    'package_name' => $subvalue->package_name,
                ];

                $result[] = $tmp;
            }

        }

        return ['data' => $result];

        
        die;

        $vendor_route_price_config = ShippingVendorPriceConfigs::where('des_province_id', intval($params['province_id']))
            ->where('des_district_id', intval($params['district_id']))->get();

        $vendor_count = count($vendor_route_price_config);
        // echo $vendor_count;die;

        if($vendor_count == 0)
            return ['message'=>'Không hổ trợ tuyến vận chuyển'];

        if($vendor_count == 1) {
            $vendor_route_price_config = $vendor_route_price_config[0];
            $total_fee_ship = $this->calculateFeeShip($vendor_route_price_config, $params['products']);
            // Check kho
            $product_warehouse = $this->calculateWarehouse($vendor_route_price_config->org_district_id, $params['products']);

            $result = [
                'fee_shipping' => isset($total_fee_ship['fee_ship']) ? $total_fee_ship['fee_ship'] : 0,
                'product_warehouse' => $product_warehouse
            ];

            return $result;
        }

        // over more vendor
        // case nhieu tuyen thuoc vendor
        $arr_total_shipping = [];
        foreach($vendor_route_price_config as $item => $object) {
            $fee_ship = $this->calculateFeeShip($object, $params['products']);
            $arr_total_shipping[] = $fee_ship;
        }

        $price = array();
        foreach ($arr_total_shipping as $key => $row) {
            $price[$key] = $row;
        }
        array_multisort($price, SORT_ASC, $arr_total_shipping);

        $arr_total_shipping =  $arr_total_shipping[0];
        $product_warehosue  = $this->calculateWarehouse($arr_total_shipping['district_id'], $params['products']);

        $result = [
            'fee_shipping' => isset($arr_total_shipping['fee_ship']) ? $arr_total_shipping['fee_ship'] : 0,
            'product_warehosue' => $product_warehosue
        ];
        return $result;


        // case nhieu tuyen khac vendor






    }

    private function calculateFeeShip($products, $prices, $vendor)
    {
        $total_fee_ship = 0;
        $maxPrice = $this->maxPrice($prices);

        foreach($products as $product) {
            $weight = $product['weight'];

            foreach($prices as $price) {

                if($price['from_weight'] < $weight && $weight <= $price['to_weight']) {
                    if($vendor == 'DHL') {                       
                        if($weight > 10) {
                            $sub_weight = $weight - 10;
                            $total_fee_ship += ($maxPrice + (floatval($sub_weight / 0.5) * $price['price']));
                            break;
                        }
                    }

                    if ($vendor == 'GHN') {
                        if ($weight > 15) {
                            $sub_weight = $weight - 10;
                            $total_fee_ship += ($maxPrice + (floatval($sub_weight / 0.5) * $price['price']));
                            break;
                        }
                    }

                    $total_fee_ship += $price['price'];
                    break;
                }
            }
        }

        return floatval($total_fee_ship);
    }

    private function maxPrice($prices)
    {
        $max = 0;
        foreach($prices as $value) {
            if($max < $value['price']){
                $max = floatval($value['price']);
            }
        }
        return $max;
    }


    private function calculateWarehouse($district_id, $products)
    {
        // Lay thong tin region dua vao district
        $region_city = RegionsCities::where('district_id', $district_id)->first();
        if($region_city == null)
            return [];

        $region_id = intval($region_city->region_id);

        $warehouse_code = '';
        $result = [];
        // Lay thong tin ware house cua product
        $vendor_warehouse = NkMpVendorWarehouses::where(
            'region_id',
            $region_id
        )->orderBy('position', 'ASC')->first();

        if($vendor_warehouse != null)
            $warehouse_code = $vendor_warehouse->code;

        foreach ($products as $product) {
            $product_id = intval($product['id']);
            $product_warehouse = ProductsWarehouses::find($product_id);
            $warehouses = json_decode($product_warehouse->warehouses, true);

            $arr_tmp = [];
            foreach($warehouses as $warehouse) {
                if(intval($warehouse['amount']) > 0) {
                    $arr_tmp[] = $warehouse;
                }
            }

            $flag = false;
            foreach($arr_tmp as $value) {
                if($value['warehouse_code'] == $warehouse_code) {
                    $value['product_id'] = $product_id;
                    $result[] = $value;
                    $flag = true;
                    break;
                }
            }

            if(!$flag) {
                $value = $arr_tmp[0];
                $value['product_id'] = $product_id;
                $result[] = $value;
            }
        }

        return $result;

    }
}
