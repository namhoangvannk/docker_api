<?php
namespace App\Src\Shipping\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use App\Helpers\Cache\RedisManager;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Maatwebsite\Excel\Excel;
use DB;
use App\Models\NkShippingVrpConfigs;

class NkShippingVrpConfigsRepository extends RepositoryBase
{
    protected $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function import($request)
    {
        try {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $data = \Excel::load($path)->get();

            if ($data->count()) {
                $arr_temp = [];

                foreach ($data as $key => $value) {
                    $arr_temp[] = [
                        'vendor_id' => intval($value->vendor_id),
                        'route_id' => intval($value->route_id),
                        'price_package_id' => intval($value->price_package_id),
                        'status' => intval($value->status),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ];
                }

                if (!empty($arr_temp)) {
                    NkShippingVrpConfigs::insert($arr_temp);
                }
            }
            $result['message'] = 'Import successfull';
            return $result;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }
}
