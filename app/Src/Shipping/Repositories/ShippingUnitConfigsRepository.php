<?php

namespace App\Src\Shipping\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use App\Helpers\Cache\RedisManager;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Src\Shipping\Collections\ShippingUnitsCollection;
use App\Src\Shipping\Transformers\ShippingUnitsTransformer;
use App\Src\Shipping\Transformers\ShippingUnitPricesTransformer;
use Maatwebsite\Excel\Excel;
use DB;
use App\Models\ShippingUnitConfigs;
use App\Models\Mongo\ShippingUnitConfigs as MShippingUnitConfigs;
use App\Models\Mongo\ShippingUnitPrices as MShippingUnitPrices;

class ShippingUnitConfigsRepository extends RepositoryBase
{
    protected $manager;
    protected $collection;
    protected $Transformer;
    protected $model;
    Protected $redis;

    public function __construct(
        Manager $manager,       
        ShippingUnitsCollection $collection,
        ShippingUnitsTransformer $Transformer,
        RedisManager $redis)
    {
        $this->manager = $manager;
        $this->collection = $collection;
        $this->transformer = $Transformer;
        $this->model = new ShippingUnitConfigs();
        $this->redis = $redis;
    }

    public function import($request)
    {
        try {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $data = \Excel::load($path)->get();

            if ($data->count()) {
                $arr_temp = [];
                
                foreach ($data as $key => $value) {
                    $redis_key = 'shipping_config_'. $value->shipping_unit_id.'_'.
                        $value->org_province_id .'_'. $value->des_province_id;
                    $unit_price_col = DB::connection('mongodb')->collection('shipping_unit_prices');
                    $integerIDs = [];
                    $integerIDs = array_map('intval', explode(',', $value->unit_price));
                    $unit_price = $unit_price_col->where(['id' => ['$in' => $integerIDs]])->get();

                    $resource = new Collection($unit_price, new ShippingUnitPricesTransformer());
                    $unit_price_data = $this->manager->createData($resource)->toArray();

                    $arr_temp[] = [
                        'shipping_unit_id' => intval($value->shipping_unit_id),
                        'org_province_id' => intval($value->org_province_id),
                        'des_province_id' => intval ($value->des_province_id),
                        'delivery_time' => $value->delivery_time,
                        'status' => intval($value->status),
                        'priority' => intval($value->priority),
                        'unit_price' => json_encode($unit_price_data['data']),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ];

                    $this->redis->setValue($redis_key, $arr_temp);
                }
                if (!empty($arr_temp)) {
                    ShippingUnitConfigs::insert($arr_temp);
                    MShippingUnitConfigs::insert($arr_temp);
                }
                
            }
            $result['message'] = 'Import successfull';
            return $result;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }
}
