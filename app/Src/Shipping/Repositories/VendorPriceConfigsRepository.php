<?php

namespace App\Src\Shipping\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use App\Src\Shipping\Transformers\VendorPriceConfigsTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Maatwebsite\Excel\Excel;
use DB;

use App\Models\ShippingVendorPriceConfigs;
use App\Models\Mongo\ShippingVendorPriceConfigs as MShippingVendorPriceConfigs;
use App\Models\ShippingVendorRouteConfigs;
use App\Models\ShippingRoutes;
use App\Models\ShippingPricePackages;
use App\Models\ShippingRangeWeights;
use App\Models\ShippingVendors;

class VendorPriceConfigsRepository extends RepositoryBase
{
    protected $fractal;

    public function __construct(Manager $fractal)
    {
        $this->fractal = $fractal;
    }

    public function import($request)
    {
        try {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $data = \Excel::load($path)->get();

            if ($data->count()) {
                $arr_temp = [];
                $mongo_data = [];

                foreach ($data as $key => $value) {
                    $arr_tmp = [];
                    $vendor_route_config_id = intval($value->vendor_route_config_id);
                    $ship_vendor_route_configs = ShippingVendorRouteConfigs::find($vendor_route_config_id);

                    if($ship_vendor_route_configs == null)
                        continue;

                    $arr_tmp['key'] = $vendor_route_config_id;
                    $arr_tmp['vendor_id'] = $ship_vendor_route_configs->vendor_id;

                    $shipping_vendor = ShippingVendors::find(intval($ship_vendor_route_configs->vendor_id));
                    if($shipping_vendor != null)
                        $arr_tmp['vendor_name'] = $shipping_vendor->vendor_name;

                    $arr_tmp['price'] = $value->price;

                    $shipping_routes = ShippingRoutes::find(intval($ship_vendor_route_configs->route_id));
                    if ($shipping_routes != null) {
                        $arr_tmp['org_province_id'] = $shipping_routes->org_province_id;
                        $arr_tmp['org_district_id'] = $shipping_routes->org_district_id;
                        $arr_tmp['des_province_id'] = $shipping_routes->des_province_id;
                        $arr_tmp['des_district_id'] = $shipping_routes->des_district_id;
                    }

                    $price_packages = ShippingPricePackages::find(intval($ship_vendor_route_configs->price_package_id));
                    if ($price_packages != null) {
                        $arr_tmp['package_name'] = $price_packages->package_name;
                        $arr_tmp['min_hour_delivery'] = $price_packages->min_hour_delivery;
                        $arr_tmp['max_hour_delivery'] = $price_packages->max_hour_delivery;
                        $arr_tmp['min_day_delivery'] = $price_packages->min_day_delivery;
                        $arr_tmp['max_day_delivery'] = $price_packages->max_day_delivery;
                    }

                    // Range weight range_weight_id
                    $range_weight = ShippingRangeWeights::find(intval($value->range_weight_id));
                    if($range_weight != null) {
                        $arr_tmp['from_weight'] = (float)$range_weight->from_weight;
                        $arr_tmp['to_weight'] = (float)$range_weight->to_weight;
                    }

                    $arr_temp[] = [
                        'vendor_route_config_id' => intval($value->vendor_route_config_id),
                        'range_weight_id' => intval($value->range_weight_id),
                        'price' => floatval($value->price),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ];
                    
                    $mongo_data[] = $arr_tmp;
                }
                
                $grouped_data = [];

                foreach ($mongo_data as $type) {
                    $grouped_data[$type['key']][] = $type;
                }

                $mongo_data = [];
                foreach($grouped_data as $key => $arr_value) {
                    $mongo_data_tmp = [];
                    $check = 0;
                    $price = [];
                    
                    foreach($arr_value as $skey => $svalue) {
                        $price_tmp = [];
                        if($check == 0) {
                            $mongo_data_tmp['vendor_id'] = $svalue['vendor_id'];
                            $mongo_data_tmp['vendor_name'] = $svalue['vendor_name'];
                            $mongo_data_tmp['org_province_id'] = $svalue['org_province_id'];
                            $mongo_data_tmp['org_district_id'] = $svalue['org_district_id'];
                            $mongo_data_tmp['des_province_id'] = $svalue['des_province_id'];
                            $mongo_data_tmp['des_district_id'] = $svalue['des_district_id'];
                            $mongo_data_tmp['package_name'] = $svalue['package_name'];
                            $mongo_data_tmp['min_hour_delivery'] = $svalue['min_hour_delivery'];
                            $mongo_data_tmp['max_hour_delivery'] = $svalue['max_hour_delivery'];
                            $mongo_data_tmp['min_day_delivery'] = $svalue['min_day_delivery'];
                            $mongo_data_tmp['max_day_delivery'] = $svalue['max_day_delivery'];
                        }

                        $price_tmp['from_weight'] = $svalue['from_weight'];
                        $price_tmp['to_weight'] = $svalue['to_weight'];
                        $price_tmp['price'] = $svalue['price'];
                        $price[] = $price_tmp;
                        $check += 1;
                    }
                    $mongo_data_tmp['json_price'] = json_encode($price);
                    $mongo_data[] = $mongo_data_tmp;
                    
                }

                if (!empty($arr_temp)) {
                    ShippingVendorPriceConfigs::insert($arr_temp);
                }

                if (!empty($mongo_data)) {
                    MShippingVendorPriceConfigs::insert($mongo_data);
                }
            }
            $result['message'] = 'Import successfull';
            return $result;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }
}
