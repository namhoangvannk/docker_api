<?php
namespace App\Src\Shipping\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Src\Shipping\Collections\PricePackagesCollection;
use App\Src\Shipping\Transformers\PricePackagesTransformer;
use Maatwebsite\Excel\Excel;

use App\Models\NKShippingPricePackages;

class NkShippingPricePackagesRepository extends RepositoryBase
{
    protected $manager;
    protected $collection;
    protected $Transformer;

    public function __construct(
        Manager $manager,
        PricePackagesCollection $collection,
        PricePackagesTransformer $Transformer
    )
    {
        $this->manager = $manager;
        $this->collection = $collection;
        $this->transformer = $Transformer;
    }

    public function import($request)
    {
        try {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $data = \Excel::load($path)->get();

            if ($data->count()) {
                $arr_temp = [];
                foreach ($data as $key => $value) {
                    $arr_temp[] = [
                        'package_name' => $value->package_name,
                        'min_hour_delivery' => (float)$value->min_hour_delivery,
                        'max_hour_delivery' => (float)$value->max_hour_delivery,
                        'min_day_delivery' => (float)$value->min_day_delivery,
                        'max_day_delivery' => (float)$value->max_day_delivery,
                        'status' => (int)$value->status,
                        'description' => $value->description,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ];
                }

                if (!empty($arr_temp)) {
                    NKShippingPricePackages::insert($arr_temp);
                }
            }
            $result['message'] = 'Import successfull';
            return $result;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }
}
