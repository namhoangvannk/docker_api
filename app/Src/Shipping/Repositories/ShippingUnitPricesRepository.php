<?php

namespace App\Src\Shipping\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Src\Shipping\Collections\ShippingUnitsCollection;
use App\Src\Shipping\Transformers\ShippingUnitsTransformer;
use Maatwebsite\Excel\Excel;

use App\Models\ShippingUnitPrices;
use App\Models\Mongo\ShippingUnitPrices as MShippingUnitPrices;

class ShippingUnitPricesRepository extends RepositoryBase
{
    protected $manager;
    protected $collection;
    protected $Transformer;
    protected $model;

    public function __construct(
        Manager $manager,
        ShippingUnitsCollection $collection,
        ShippingUnitsTransformer $Transformer)
    {
        $this->manager = $manager;
        $this->collection = $collection;
        $this->transformer = $Transformer;
        $this->model = new ShippingUnitPrices();
    }

    public function import($request)
    {
        try {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $data = \Excel::load($path)->get();

            if ($data->count()) {
                $arr_temp = [];
                foreach ($data as $key => $value) {
                    $arr_temp = [
                        'from_kg' => $value->from_kg,
                        'to_kg' => $value->to_kg,
                        'price' => $value->price,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ];

                    $model = new ShippingUnitPrices();
                    $model->fill($arr_temp);
                    $model->save();
                    $arr_temp['id'] = $model->id;
                    MShippingUnitPrices::insert($arr_temp);
                }
            }
            $result['message'] = 'Import successfull';
            return $result;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }
}
