<?php

namespace App\Src\Discussions\Transformers;

use League\Fractal\TransformerAbstract;

class DiscussionsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'post_id' => (int)$obj->post_id
        ];
    }
}
