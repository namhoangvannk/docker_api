<?php

namespace App\Src\Discussions\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;

use App\Src\Discussions\Transformers\DiscussionsTransformer;
use App\Models\DiscussionPosts;
use App\Models\DiscussionMessages;
use App\Models\DiscussionRating;
use App\Models\Discussion;
use App\Models\SeoNames;
use App\Models\Mongo\LogComments as LogComments;

use DB;
use Log;

class DiscussionsRepository extends RepositoryBase
{
    protected $fractal;
    protected $transformer;
    protected $model;
    protected $helper;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        DiscussionsTransformer $transformer
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new DiscussionPosts;
        $this->transformer = $transformer;

    }
    public function all($request)
    {

    }

    public function create($request)
    {
        $params = $request->all();
        $model = $this->model;
		
		Log::info('Discussion Params: ' . var_export($params, true));
        try {
            if (empty($params['thread_id'])) {
               LogComments::insert(['request_id' => $params['object_id'],'title' => 'params','object_id' => $params['object_id'],'input' => json_encode($params)] );
               $d = Discussion::where('object_id', intval($params['object_id']))
                        ->where('object_type', $params['object_type'])
						->where('type', $params['type'])
						->first();
                $curl = $this->process_purgecacheall(0,$params['object_id']);
                LogComments::insert(['request_id' => $params['object_id'],'title' => '$curl','object_id' => $params['object_id'],'input' => $curl] );
				Log::info('DIS:' . intval($params['object_id']) . '|'.$params['object_type']. '|'. $params['type'] . '#' . $d->thread_id . '|');
				if(!empty($d) && count($d)>0) {
					return ['thread_id' => $d->thread_id];
				}					
                $discussion_post = [
                    'object_id' => intval($params['object_id']),
                    'object_type' => $params['object_type'],
                    'type' => $params['type'],
                    'company_id' => isset($params['company_id']) ? $params['company_id'] : 0
                ];
                $discuss = new Discussion();
                $discuss->fill($discussion_post);
                $discuss->save();
                LogComments::insert(['request_id' => $params['object_id'],'title' => 'ket thuc empty($params[thread_id])','object_id' => $params['object_id'],'input' => json_encode($discussion_post)] );
                $curl = $this->process_purgecacheall(0,$params['object_id']);
                return ['thread_id' => (int)$discuss->thread_id];
            } else {
                $is_admin = (isset($params['user_role']) && '1' == $params['user_role']) ? true : false;
                //chong ddos comment sau 1'
                if(!$is_admin) {
                    $n_posts = DiscussionPosts::select('thread_id')
                        ->whereRaw('thread_id = ? AND ip_address = ? AND timestamp > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 MINUTE)) ',
                            [intval($params['thread_id']), $params['ip_address']])
                        ->get()->count();

                    if ($n_posts > 1) {
                        return array(
                            "code" => 'DUP'
                        );
                    }
                }
                $phone = isset($params['phone']) ? $params['phone'] : '';
                $discussion_post = [
                    'thread_id' => intval($params['thread_id']),
                    'name' => $params['name'],
                    'status' => $params['status'],
                    'ip_address' => $params['ip_address'],
                    'timestamp' => $params['timestamp'],
                    'tree' => $params['tree'],
                    'user_id' => $params['user_id'],
                    'parent_id' => $params['parent_id'],
                    'phone' => $phone,
                    'user_role' => isset($params['user_role']) ? $params['user_role'] : '0'
                ];
                //hunglm bo sung count answer khi admin comment
                if (!empty($params['parent_id']) && $is_admin) {
                    $discuss_post_main = DiscussionPosts::find($params['parent_id']);
                    $p_update = array(
                        'count_answer' => isset($discuss_post_main['count_answer']) ? $discuss_post_main['count_answer'] + 1 : 1
                    );
                    $discuss_post_main->fill($p_update);
                    $discuss_post_main->update();
                }
                //End
                $model->fill($discussion_post);
                $model->save();
                $post_id = $model->post_id;
                LogComments::insert([
                    'request_id' => $params['thread_id'],
                    'title' => 'ket thuc insert $discussion_post','object_id' => $params['thread_id'],
                    'input' => json_encode($discussion_post)] );
                // Insert message
                $discussion_message = [
                    'post_id' => $post_id,
                    'thread_id' => intval($params['thread_id']),
                    'message' => $params['message']
                ];

                $message_model = new DiscussionMessages();
                $message_model->fill($discussion_message);
                $message_model->save();

                // Insert rating
                $discussion_rating = [
                    'post_id' => $post_id,
                    'thread_id' => intval($params['thread_id']),
                    'rating_value' => isset($params['rating_value']) ? intval($params['rating_value']) : 5
                ];
                $rating_model = new DiscussionRating();
                $rating_model->fill($discussion_rating);
                $rating_model->save();
                LogComments::insert([
                    'request_id' => $params['thread_id'],
                    'title' => 'ket thuc insert $discussion_rating',
                    'object_id' => $params['thread_id'],
                    'input' => json_encode($discussion_rating)] );
                $curl = $this->process_purgecacheall($params['thread_id'],0);
                return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

   public function update($request, $post_id)
    {
        $params = $request->all();
        try {
            $post = DiscussionPosts::find($post_id);
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Post ID: ' . $post_id);
            Log::info('Params: ' . var_export($params, true));
            if ($post == null)
                //abort(400, 'Comment không tồn tại');
                return ['success' => false, 'message' => 'Comment không tồn tại'];
            // xu ly xoa cache PDP
            $thread_id = $post->thread_id;
            // end xu ly cache PDP
            $post->fill($params);
            $post->update();
            $curl = $this->process_purgecacheall($thread_id,0);
            return ['success' => true, 'message' => 'Cập nhật thành công'];
        } catch (\Exception $e) {
            abort(400, $e->getMessage());
        }
        abort(500, 'Có lỗi xảy ra!');
        return [];
    }
    public function process_purgecacheall($thread_id,$object_id){
        if($object_id == 0 && $thread_id > 0){
            $d = Discussion::where('thread_id',$thread_id)
            ->first();
            if(!empty($d)){
                $object_id = $d->object_id;
            }
        }
        if($object_id > 0){
            $p = SeoNames::where('object_id',$object_id)
                ->where('type','p')
                ->first();
            $url = '';
            $name = '';
            if(!empty($p)){
                $name = $p->name;
            }
            if($name !=''){
                $url = 'https://www.nguyenkim.com/'.$name.'.html';
            }
            if(strlen(strstr($url, 'adm')) > 0){
                $url = str_replace("adm","www",$url);
            }
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://171.244.45.67:8088/purgecacheall.php",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => array(
                    'link' => $url,
                    'device' => 'ALL',
                    'xoacache' => 'xoacache'
                ),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Basic bmthZG1pbjpOa0FkbTFuITIwMk9pTA==",
                    "Content-Type: multipart/form-data"
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);

            Log::info(date('Y-m-d H:i:s'));
            Log::info('DEL CACHE Post ID: ' . $object_id);
            Log::info('Params: ' . var_export($response, true));
            return $response;
        }
    }
}
