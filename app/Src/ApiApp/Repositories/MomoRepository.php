<?php
/**
 * Created by PhpStorm.
 * User: nguyenkim
 * Date: 1/3/19
 * Time: 1:47 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Mongo\Momo;
use Illuminate\Support\Facades\Validator;
use DB;

class MomoRepository extends RepositoryBase
{
    protected $model;
    protected $helper;
    protected $config;
    public function __construct(Helpers $helper)
    {
        $this->helper = $helper;
    }
    public function create($request)
    {
        $params = $request->all();
        $title = $params['title'];
        $data = $params['data'];

        try {
            Momo::insert(['title' => $title,'data' => json_encode($data)]);

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}