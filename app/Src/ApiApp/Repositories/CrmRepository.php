<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 4/24/2019
 * Time: 9:54 AM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use Carbon\Carbon;
use DB;
use Log;

class CrmRepository extends RepositoryBase
{
    public function createTicket($request)
    {
        $input = $request->all();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://crm.nguyenkim.com/ants/ticket",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($input),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 5320b014-5329-f28d-573d-84d00c12dc8c",
                "x-ants-secret: vIO2t2giSvAdUFi2HOSsihK"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $data_res = json_decode($response, true);
        if (!empty($data_res['ticket_id'])) {
            if (!empty($input['order_id'])) {
                DB::table('cscart_orders')->where('order_id', (int)$input['order_id'])
                    ->update([
                        'id_code' => $data_res['ticket_id'],
                        'update_timestamp' => Carbon::now()->timestamp
                    ]);
                return array(
                    'status' => 200,
                    'message' => 'Thành công'
                );
            } else {
                return array(
                    'status' => 400,
                    'message' => 'Không thành công'
                );
            }
        } else {
            return array(
                'status' => 400,
                'message' => 'Tạo ticket không thành công'
            );
        }
    }
    public function resolveTicket($request, $ticket_id)
    {
        if ($ticket_id) {
            $input = $request->all();
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://crm.nguyenkim.com/ants/ticket/" . $ticket_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($input),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "postman-token: e1aa8908-4acc-bd28-4204-de456101051c",
                    "x-ants-secret: vIO2t2giSvAdUFi2HOSsihK"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $data_res = json_decode($response, true);
            if ($data_res['success']) {
                return array(
                    'status' => 200,
                    'message' => 'Thành công'
                );
            } else {
                return array(
                    'status' => 400,
                    'message' => 'Không thành công'
                );
            }
        }else {
            return array(
                'status' => 400,
                'message' => 'Không tồn tại ticket ID'
            );
        }
    }
}