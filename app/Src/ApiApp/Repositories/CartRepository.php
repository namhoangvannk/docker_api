<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:18 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class CartRepository extends RepositoryBase
{
    public function product_info($product_id, $product_options = array())
    {
        $product_info = array();
        $options = array();
        $products = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', $product_id)->first();
        $product_info = array(
            'product_id' => $products['product_id'],
            'price' => $products['price'],
            'product' => $products['product'],
            'images' => isset($products['main_pair']['detailed']['image_path']) ? $products['main_pair']['detailed']['image_path'] : '',
        );
        if (count($product_options) > 0) {
            foreach ($product_options as $option_id => $variant_id) {
                $option_name = isset($products['product_options'][$option_id]['option_name']) ? $products['product_options'][$option_id]['option_name'] : '';
                $variant_name = isset($products['product_options'][$option_id]['variants'][$variant_id]['variant_name']) ? $products['product_options'][$option_id]['variants'][$variant_id]['variant_name'] : '';
                if (!empty($option_name) && !empty($variant_name)) {
                    $options[$option_name] = $variant_name;
                }
            }
        }

        $product_info['product_options'] = $options;
        return $product_info ;

    }

    public function show($request)
    {
        $params = $request->all();
        $cart_info = $params['cart_info'];
        $datas = array();

        foreach ($cart_info as $product_id => $product) {
            $product_info = $this->product_info($product_id, $product['product_options']);
            $datas[] = $product_info;
        }
        return [
            'code'=>200,
            'status'=>true,
            'datas'=>$datas
        ];
    }
}