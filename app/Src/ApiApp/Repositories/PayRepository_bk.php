<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/20/2018
 * Time: 3:13 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class PayRepository extends RepositoryBase
{
    protected $table_name = 'cscart_nk_tragop_configs';

    public function getlistConfig($product_id, $quantity)
    {
        $data_product = array();
        $list_tragop = array();
        $outputs = array();
        $category = array();
        $quantity = (!isset($quantity) || !is_int($quantity)) ? 1 : (int)$quantity;
        $data_product['product_id'] = (int)$product_id;

        $products = DB::table('cscart_products')->where('product_id', (int)$product_id)->get(['product_id'])->first();

        if (!empty($products->product_id)) {
            // lấy giá trả góp của sàn phẩm với product_id
            $data_installment_price_type = DB::table('cscart_products')->where('product_id', $product_id)->get(['installment_price_type'])->first();
            $installment_price_type = $data_installment_price_type->installment_price_type;
            if ($installment_price_type == 'B') {
                $d_price = DB::table('cscart_product_prices')->where('product_id', $product_id)->get(['price'])->first();
                $data_product['price'] = (float)$d_price->price * $quantity;
            } elseif ($installment_price_type == 'Y') {
                $d_price = DB::table('cscart_products')->where('product_id', $product_id)->get(['list_price'])->first();
                $data_product['price'] = (float)$d_price->list_price * $quantity;
            } else {
                $d_price = DB::table('cscart_products')->where('product_id', $product_id)->get(['installment_price'])->first();
                $data_product['price'] = (float)$d_price->installment_price * $quantity;
            }

            $d_product_code = DB::table('cscart_products')->where('product_id', $product_id)->get(['product_code'])->first();
            $data_product['product_code'] = $d_product_code->product_code;
            $d_category = DB::table('cscart_products_categories as cpc')
                ->leftJoin('cscart_categories as c', 'c.category_id', '=', 'cpc.category_id')
                ->where(
                    [
                        ['cpc.product_id', '=', $product_id],
                        ['c.level', '=', 2],
                    ]
                )
                ->get(['cpc.category_id']);
            if ($d_category) {
                foreach ($d_category as $cat) {
                    $category[] = $cat->category_id;
                }
            }
            $outputs['price'] = (float)$data_product['price'];
            $data_product['category'] = $category;
            $max_loan = (float)$data_product['price'] * 0.3;
            $d_list_tragop = DB::table($this->table_name)
                ->where(
                    [
                        ['status', '=', 1],
                        ['from_time', '<=', date('Y-m-d H:i:s')],
                        ['to_time', '>=', date('Y-m-d H:i:s')],
                        ['to_loan', '>=', $max_loan],
                    ]
                )
                ->whereIn('approval', ['Onl', 'All', 'Off'])
                ->get(['id', 'inst', 'type', 'product_code', 'from_price', 'to_price', 'approval', 'category', 'except_prod', 'except_cate', 'kyhanvay', 'laisuat', 'tratruoc', 'phithuho_month', 'paper_code', 'insurance_rate']);
            if ($d_list_tragop) {
                foreach ($d_list_tragop as $tragop) {
                    $list_tragop[$tragop->id] = array(
                        'id' => $tragop->id,
                        'inst' => $tragop->inst,
                        'type' => $tragop->type,
                        'from_price' => $tragop->from_price,
                        'to_price' => $tragop->to_price,
                        'product_code' => (!empty($tragop->product_code) && trim($tragop->product_code) <> 'all') ? explode(',', trim($tragop->product_code)) : trim($tragop->product_code),
                        'category' => (!empty($tragop->category) && $tragop->category != 'all') ? explode(',', trim($tragop->category)) : trim($tragop->category),
                        'except_prod' => (!empty($tragop->except_prod) && $tragop->except_prod != 'all') ? explode(',', trim($tragop->except_prod)) : trim($tragop->except_prod),
                        'except_cate' => (!empty($tragop->except_cate) && $tragop->except_cate != 'all') ? explode(',', trim($tragop->except_cate)) : trim($tragop->except_cate),
                        'kyhanvay' => (!empty($tragop->kyhanvay)) ? explode(',', trim($tragop->kyhanvay)) : array(),
                        'tratruoc' => (!empty($tragop->tratruoc)) ? explode(',', trim($tragop->tratruoc)) : array(),
                        'laisuat' => $tragop->laisuat,
                        'phithuho_month' => $tragop->phithuho_month,
                        'paper_code' => $tragop->paper_code,
                        'insurance_rate' => $tragop->insurance_rate,
                        'approval' => $tragop->approval
                    );
                }
            }

            if ($list_tragop) {
                foreach ($list_tragop as $id => $list_tragop_info) {
                    if ((float)$list_tragop_info['from_price'] > (float)$data_product['price'] || (float)$list_tragop_info['to_price'] < (float)$data_product['price']) {
                        unset($list_tragop[$id]);
                    }

                    // RULE FOR TYPE =  PRODUCT
                    if ($list_tragop_info['type'] == 'p') {
                        if (!is_array($list_tragop_info['product_code']) && trim($list_tragop_info['product_code']) <> 'all') {
                            unset($list_tragop[$id]);
                        } elseif (is_array($list_tragop_info['product_code']) && !in_array($data_product['product_code'], $list_tragop_info['product_code'])) {
                            unset($list_tragop[$id]);
                        }
                        // CHECK PRODUCT_CODE NOT IN FEILD except_prod REMOVE RULE IF TRUE
                        if (is_array($list_tragop_info['except_prod']) && in_array($data_product['product_code'], $list_tragop_info['except_prod'])) {
                            unset($list_tragop[$id]);
                        }
                        //CHECK CATEGORY NOT IN FEILD except_cate REMOVE RULE IF TRUE
                        if (is_array($data_product['category']) && is_array($list_tragop_info['except_cate'])) {
                            $intersect = array_intersect($data_product['category'], $list_tragop_info['except_cate']);
                            if (count($intersect) > 0) {
                                unset($list_tragop[$id]);
                            }
                        }

                    } elseif ($list_tragop_info['type'] == 'c') {
                        // RULE FOR TYPE =  CATEGORY
                        if (!is_array($list_tragop_info['category']) && trim($list_tragop_info['category']) <> 'all') {
                            unset($list_tragop[$id]);
                        } elseif (is_array($list_tragop_info['category']) && is_array($data_product['category'])) {
                            if (count($data_product['category']) == count($list_tragop_info['category'])) {
                                $diff = array_diff($data_product['category'], $list_tragop_info['category']);
                                if ($diff > 0) {
                                    unset($list_tragop[$id]);
                                }
                            } elseif (count($data_product['category']) > count($list_tragop_info['category'])) {
                                $diff = array_diff($list_tragop_info['category'], $data_product['category']);
                                if ($diff > 0) {
                                    unset($list_tragop[$id]);
                                }
                            } else {
                                $diff = array_diff($data_product['category'], $list_tragop_info['category']);
                                if ($diff > 0) {
                                    unset($list_tragop[$id]);
                                }
                            }
                        }
                        //CHECK CATEGORY NOT IN FEILD except_cate REMOVE RULE IF TRUE
                        if (is_array($data_product['category']) && is_array($list_tragop_info['except_cate'])) {
                            $intersect = array_intersect($data_product['category'], $list_tragop_info['except_cate']);
                            if (count($intersect) > 0) {
                                unset($list_tragop[$id]);
                            }
                        }
                        // CHECK PRODUCT_CODE NOT IN FEILD except_prod REMOVE RULE IF TRUE
                        if (is_array($list_tragop_info['except_prod']) && in_array($data_product['product_code'], $list_tragop_info['except_prod'])) {
                            unset($list_tragop[$id]);
                        }
                    }
                }
            }


            if ($list_tragop) {
                foreach ($list_tragop as $tragop) {
                    //  $outputs['ids'][] = $tragop['id'];
                    $outputs['rule'][] = array(
                        'ids' => $tragop['id'],
                        'tratruoc' => $tragop['tratruoc'],
                        'kyhanvay' => $tragop['kyhanvay'],
                        'laisuat' => $tragop['laisuat'],
                        'inst' => $tragop['inst'],
                        'phithuho_month' => $tragop['phithuho_month'],
                        'paper_code' => $tragop['paper_code'],
                        'insurance_rate' => $tragop['insurance_rate'],
                        'price' => $outputs['price'],
                        'approval' => $tragop['approval']
                    );
                }
            }
            return $outputs;
        } else {
            return array();

        }
    }
    public function packageOptimize($request)
    {
        $optimizes = array();
        $data_optimizes = array();
        $input = $request->all();
        $tratruoc = array();
        $laixuat = array();
        $goi_tra_gop = array();
        $sort_by_percent = array();
        $packages = $this->getlistConfig($input['product_id'], $input['quantity']);

        if (isset($packages['rule']) && count($packages['rule']) > 0) {
            $price = (float)$packages['price'];
            foreach ($packages['rule'] as $package_id => $package_info) {
                $laixuat[] = $package_info['laisuat'];
                if (count($package_info['tratruoc']) > 0) {
                    foreach ($package_info['tratruoc'] as $value) {
                        $sort_by_percent[] = $value;
                        $tratruoc[$value] = array(
                            'percent' => $value,
                            'price' => (float)($price * $value) / 100,
                            'label' => $value . '% - ' . number_format((($price * $value) / 100), 0, ",", ".") . 'đ'
                        );

                    }
                }
            }
            array_multisort($tratruoc, SORT_ASC, array_unique($sort_by_percent));
            print_r($tratruoc);
            die ;
            // selected % tra truoc đầu tiên của gói tối ưu nhất
            $optimizes['tratruoc'] = $tratruoc;

            array_multisort($laixuat, SORT_ASC, $packages['rule']);
            $first_item = array_shift($packages['rule']);
            if ($first_item['kyhanvay']) {
                foreach ($first_item['kyhanvay'] as $kyhanvay) {
                    $data_optimizes[$kyhanvay] = isset($first_item['laisuat']) ? $first_item['laisuat'] : '';
                }
            }

            // add package missing to array $optimizes
            foreach ($packages['rule'] as $value) {
                if ($value['kyhanvay']) {
                    foreach ($value['kyhanvay'] as $v_kyhanvay) {
                        // if not exit --> add to array $optimizes
                        if (!in_array($v_kyhanvay, array_keys($data_optimizes))) {
                            $data_optimizes[$v_kyhanvay] = isset($value['laisuat']) ? $value['laisuat'] : '';
                        } else {
                            if ((float)$value['laisuat'] < (float)$data_optimizes[$v_kyhanvay]) {
                                $data_optimizes[$v_kyhanvay] = $value['laisuat'];
                            } else {
                                continue;
                            }
                        }
                    }
                }
            }

            ksort($data_optimizes);
            $default = array();
            if ($data_optimizes) {
                foreach ($data_optimizes as $key => $optimize) {
                    if ($key == 6) {
                        $default[$key] = ($optimize == '0') ? 'Trả góp 0%' : '';
                        unset($data_optimizes[6]);
                    } else {
                        $data_optimizes[$key] = ($optimize == '0') ? 'Trả góp 0%' : '';
                    }
                }
            }
            if (count($default) == 0) {
                $max_key = max(array_keys($data_optimizes));
                $default[$max_key] = ($data_optimizes[$max_key] == '0') ? 'Trả góp 0%' : '';
                unset($data_optimizes[$max_key]);
            }

            $optimizes['kyhanvay'] = $default + $data_optimizes;
            // lấy thông tin mặc định của các gói
            $optimizes['list_data'] = [];
            if (!empty($optimizes)) {
                $percent = 0;
                $month = 0;
                if (!empty($optimizes['tratruoc'])) {
                    //$percent = (float)$optimizes['tratruoc'][0]['percent'];
                    foreach ($optimizes['tratruoc'] as $key => $val) {
                        $percent = (float)$val['percent'];
                        break;
                    }

                }
                if (!empty($optimizes['kyhanvay'])) {
                    foreach ($optimizes['kyhanvay'] as $key => $val) {
                        $month = (float)$key;
                        break;
                    }


                }
                $params = array(
                    "percent" => $percent,
                    "month" => $month,
                    "product_id" => $input['product_id'],
                    "quantity" => $input['quantity'],
                    "insurance" => 1
                );
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://dev.api.nguyenkimonline.com/v1/get_info_package",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($params),
                    CURLOPT_HTTPHEADER => array(
                        "authorization: nko_internal",
                        "cache-control: no-cache",
                        "content-type: application/json",
                        "password: eJwrtjIytVJyyMvOj8/MK0ktykvMSTUyMDR3yPPWS87PVbIGALc2Cqc=",
                    ),
                ));
                $response = curl_exec($curl);
                $datas = json_decode($response, true);
                $err = curl_error($curl);
                curl_close($curl);
                if (!empty($datas)) {
                    $optimizes['list_data'] = $datas['data'];
                }
            }
            return (
            array(
                'status' => 200,
                'message' => 'Thành công',
                'datas' => $optimizes
            )
            );
        } else {
            return (
            array(
                'status' => 400,
                'message' => 'Không có gói trả góp nào phù hợp',
            )
            );
        }

    }

    public function getInfoPackageByPriceMonth($request)
    {
        $optimizes = array();
        $input = $request->all();
        $tratruoc = array();
        $goi_tra_gop = array();
        $packages = $this->getlistConfig($input['product_id'], $input['quantity']);
        if (isset($packages['rule']) && count($packages['rule']) > 0) {
            $data = [];
            $i = 0;
            foreach ($packages['rule'] as $package_id => $package_info) {
                $i++;
                $check_per[$i] = 0;
                $check_month[$i] = 0;
                if (!empty($package_info['tratruoc'])) {
                    foreach ($package_info['tratruoc'] as $key => $val) {
                        if ($val == intval($input['percent']))
                            $check_per[$i] = 1;
                    }
                }
                if (!empty($package_info['kyhanvay'])) {
                    foreach ($package_info['kyhanvay'] as $key1 => $val1) {
                        if ($val1 == intval($input['month']))
                            $check_month[$i] = 1;
                    }
                }
                if ($check_per[$i] == 1 && $check_month[$i] == 1)
                    $data[] = $package_info;
            }
            // lấy lãi suất thấp nhất theo từng ngân hàng
            $temp = [];
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    if (empty($temp)) {
                        $arr = array($value['inst'] => $value);
                        array_push($temp, $value);
                    } else {
                        $lai_suat = $value['laisuat'];
                        $inst = $value['inst'];
                        foreach ($temp as $k => $v) {
                            if ($inst == $v['inst']) {
                                if ($lai_suat < $v['laisuat']) {
                                    unset($temp[$k]);
                                    array_push($temp, $value);
                                }
                            } else {
                                array_push($temp, $value);
                                break;
                            }
                        }
                    }
                }
            }
            $data_hd = [];
            if (!empty($temp)) {
                foreach ($temp as $item) {
                    // E10
                    $so_tien_vay_chua_bao_hiem = $item['price'] * (1 - ($input['percent'] / 100));
                    // E8
                    $so_tien_bao_hiem = $so_tien_vay_chua_bao_hiem * ($item['insurance_rate'] / 100);
                    // E9 phí bảo hiểm (tháng)
                    $phi_bao_hiem = $so_tien_bao_hiem / $input['month'];
                    // E15 chênh lệch với mua thẳng, chênh lệch so với giá bán
                    $chenh_lech_so_voi_gia_ban = ($so_tien_bao_hiem + $item['phithuho_month']) * $input['month'];
                    // E16 khoản tiền trả trước
                    $khoan_tien_tra_trươc = $item['price'] * ($input['percent'] / 100);
                    // E4 Khoản vay (bao gồm bảo hiểm nếu có)
                    $khoan_vay = ($item['price'] * (1 - ($input['percent'] / 100))) + $so_tien_bao_hiem;
                    // E17 gốc
                    $goc = $khoan_vay / $input['month'];
                    // E18 Lãi
                    $lai = 0;
                    // E19 tổng số tiền phải trả cả gốc và lãi
                    $tong_tien_phai_tra = ($goc + $lai) * $input['month'];
                    // start PMT
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://newapi.nguyenkimonline.com/finance/v1/pmt",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "nper: " . $input['month'],
                            "pv: " . $khoan_vay,
                            "rate: " . $item['laisuat']
                        ),
                    ));
                    $response = curl_exec($curl);
                    $arr = json_decode($response, true);
                    $err = curl_error($curl);

                    curl_close($curl);
                    $so_tien_thanh_toan_hang_thang = 0;
                    if ($arr['status'] == 200) {
                        $so_tien_thanh_toan_hang_thang = $arr['PMT'];
                    }

                    // end PMT
                    $datas['so_tien_thanh_toan_hang_thang'] = $so_tien_thanh_toan_hang_thang;
                    $datas['lai_suat_thuc_moi_thang'] = (float)$item['laisuat']; // E11
                    $datas['goc'] = (float)$goc;
                    $datas['lai'] = (float)$item['laisuat'];
                    $datas['phi_thu_ho'] = (float)$item['phithuho_month']; // E13 Phí đóng tiền hàng tháng
                    $datas['phi_bao_hiem'] = (float)$phi_bao_hiem;
                    $datas['tong_tien'] = (float)$tong_tien_phai_tra;
                    $datas['chenh_lech_so_voi_gia_ban'] = (float)$chenh_lech_so_voi_gia_ban;
                    $datas['giay_to_yeu_cau'] = $item['paper_code'];
                    $datas['inst'] = $item['inst'];
                    $datas['ty_le_bao_hiem'] = $item['insurance_rate'];
                    $datas['so_tien_vay_chua_bao_hiem'] = $so_tien_vay_chua_bao_hiem;
                    $datas['so_tien_bao_hiem'] = $so_tien_bao_hiem;
                    $datas['khoan_vay'] = $khoan_vay;
                    $data_hd[$item['inst']][] = $datas;
                }
            }
            return (
            array(
                'status' => 200,
                'data' => $data_hd
            )
            );

        } else {
            return (
            array(
                'status' => 400,
                'message' => 'Không có gói trả góp nào phù hợp',
            )
            );
        }
    }

}