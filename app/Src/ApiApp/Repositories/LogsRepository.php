<?php
/**
 * Created by PhpStorm.
 * User: nguyenkim
 * Date: 1/3/19
 * Time: 2:50 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Mongo\Logs as MLogs;
use Illuminate\Support\Facades\Validator;
use DB;

class LogsRepository extends RepositoryBase
{
    protected $model;
    protected $helper;
    protected $config;
    public function __construct(Helpers $helper)
    {
        $this->helper = $helper;
    }
    public function create($request)
    {
        $params = $request->all();
        $request_id = $params['request_id'];
        $type = $params['type'];
        $data = $params['data'];
        $title = $params['title'];

        try {
            if($type == 0){
                MLogs::insert(['request_id' =>  $request_id,'title' => $title,'input' => json_encode($data)] );
            }else if($type == 1){
                $order_id = $params['order_id'];
                MLogs::where('request_id', $request_id)
                ->update(['output' => json_encode($data),'order_id' => $order_id ]);
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function update($request,$id)
    {
        $params = $request->all();
        try {
            MLogs::where('request_id', $id)
                ->update(['output' => $request]);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}