<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:16 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class Profiles extends RepositoryBase
{

    public function get($request, $user_id)
    {
        $user_info = array() ;
        if (is_numeric($user_id)){
            $user_info = DB::table('cscart_users')
                ->leftJoin('cscart_user_profiles', 'cscart_users.user_id', '=', 'cscart_user_profiles.user_id')
                ->select('cscart_users.firstname', 'cscart_users.lastname', 'cscart_users.phone', 'cscart_users.birthday', 'cscart_users.email', 'cscart_user_profiles.gender')
                ->where('cscart_users.user_id', $user_id)
                ->get();
        }
        return [
            'code' => 200,
            'status' => true,
            'datas' => $user_info
        ];
    }
    public function show($request)
    {
        $params = $request->all();
        $email = isset($params['email']) ? $params['email'] : '';
        if ($email) {
            $users_info = DB::table('cscart_users')->where(
                [
                    ['email', '=', $email],
                    ['status', '=', 'A'],
                ]

            )->first();
            if (!empty($users_info['email'])) {
            } else {
                $datas = 'Email không phù hợp với bất kỳ tài khoản nào hiện đang được lưu trữ. Hãy chắc chắn bạn nhập đúng email và thử lại.';
            }
        }


//        if (fn_allowed_for('ULTIMATE')) {
//            if (Registry::get('settings.Stores.share_users') == 'N' && AREA != 'A') {
//                $condition = fn_get_company_condition('?:users.company_id');
//            }
//        }
//        $uid = db_get_field("SELECT user_id FROM ?:users WHERE email = ?s" . $condition, $params['email']);
//        $users_info = fn_get_user_info($uid, false);
        return [
            'code' => 200,
            'status' => true
        ];
    }
}