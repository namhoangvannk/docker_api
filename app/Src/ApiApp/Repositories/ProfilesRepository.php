<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/20/2018
 * Time: 3:13 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use DB;
use Log;

class ProfilesRepository extends RepositoryBase
{
    public function listHobby($request)
    {
        $hobbys = array();
        $hobbys = DB::table('cscart_hobby_description')->where('lang_code', 'vi')->get(['hobby_id', 'description']);
        return [
            'status' => 200,
            'message' => 'Success',
            'datas' => $hobbys
        ];
    }
    public function changePassword($request, $user_id)
    {
        // kiem tra ton tai user ID
        $users = DB::table('cscart_users')->where('user_id', $user_id)->get(['user_id', 'salt', 'password'])->first();
        if (!isset($users->user_id)) {
            return (
            array(
                'status' => 400,
                'message' => 'User ID không tồn tại',
            )
            );
        } else {
            $input = $request->all();
            if ($input['new_password'] == $input['old_password']) {
                $data_error['re_new_password'] = 'Mật khẩu mới và cũ giống nhau';
                return (
                array(
                    'status' => 400,
                    'message' => 'Error',
                    'errors' => $data_error,
                )
                );
            }
            $data_error = [];
            if (!empty($input['old_password'])) {
                $input['old_password'] = $this->fn_generate_salted_password($input['old_password'], $users->salt);
                if ($input['old_password'] != $users->password) {
                    $data_error['old_password'] = 'Mật khẩu hiện tại không đúng';
                }
            } else {
                $data_error['old_password'] = 'Mật khẩu hiện tại không được trống';
            }

            if ($input && count($data_error) == 0) {
                $min_length = !empty($input['min_length_require']) ? $input['min_length_require'] : 6;
                $valid = Validator::make($input,
                    [
                        'new_password' => 'required|min:' . $min_length,
                        're_new_password' => 'required|min:' . $min_length . '|same:new_password'
                    ],
                    [
                        'new_password.required' => 'Mật khẩu mới không được trống',
                        'new_password.min' => 'Mật khẩu mới tối thiểu ' . $min_length . ' kí tự',

                        're_new_password.required' => 'Mật khẩu xác nhận không được trống',
                        're_new_password.min' => 'Mật khẩu xác nhận tối thiểu ' . $min_length . ' kí tự',
                        're_new_password.same' => 'Mật khẩu xác nhận không đúng',
                    ]
                );
                if ($valid->passes()) {
                    // update password and send email to customer if send mail is ok
                    $salt = $this->fn_generate_salt();
                    $password = $this->fn_generate_salted_password($input['new_password'], $salt);
                    $timestamp = time();
                    $password_change_timestamp = time();
                    DB::table('cscart_users')
                        ->where('user_id', $user_id)
                        ->update([
                            'salt' => $salt,
                            'password' => $password,
                            'timestamp' => $timestamp,
                            'password_change_timestamp' => $password_change_timestamp
                        ]);

                    // notify to customer
                    if (isset($input['notify_customer']) && $input['notify_customer'] == '1') {
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://www.nguyenkim.com/api/SendEmail",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_SSL_VERIFYHOST => FALSE,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "{  \r\n   \"user_id\":\"" . $user_id . "\",\r\n   \"new_password\":\"" . $input['new_password'] . "\"\r\n}",
                            CURLOPT_HTTPHEADER => array(
                                "authorization: Basic YmluaC5waGFudGhpQHRyYWRlLm5ndXllbmtpbS5jb206MXAxN0IwMUIzNlQ1TzYxcDhKNHhtNjQ3Y1hpQWU4Nng=",
                                "cache-control: no-cache",
                                "content-type: application/json",
                                "postman-token: 17605414-b2f3-bbae-84ef-85dac3fa7e83"
                            ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);

                    }
                    return [
                        'status' => 200,
                        'message' => 'Success'
                    ];
                } else {
                    $errors = $valid->errors();
                    !$errors->has('new_password') ?: $data_error['new_password'] = $errors->first('new_password');
                    !$errors->has('re_new_password') ?: $data_error['re_new_password'] = $errors->first('re_new_password');
                    return (
                    array(
                        'status' => 400,
                        'message' => 'Error',
                        'errors' => $data_error,
                    )
                    );
                }

            } else {
                return (
                array(
                    'status' => 400,
                    'message' => 'Error',
                    'errors' => $data_error,
                )
                );
            }
        }
    }

    public function addAddress($request)
    {
        $input = $request->all();
        if (!empty($input['user_id'])) {
            $total_address = DB::table('cscart_user_profiles')->where([
                ['user_id', $input['user_id']],
                ['s_address', '<>', ''],
            ])->count();
            if ($total_address < 3) {
                if ($total_address >= 1) {
                    $default = !empty($input['user_address']['default']) ? 1 : 0;
                } else {
                    $default = 1;
                }
                if ($input['user_address']) {
                    $valid = Validator::make($input['user_address'],
                        [
                            'full_name' => 'required',
                            'phone' => 'required',
                            'address' => 'required'
                        ],
                        [
                            'full_name.required' => 'Họ và tên không được rỗng',
                            'phone.required' => 'Số điện thoại không được rỗng',
                            'address.required' => 'Địa chỉ nhận hàng không được rỗng ',
                        ]
                    );
                    if ($valid->passes()) {
                        //validate ok => insert to database
                        if ($input['user_address']['default'] == 1) {
                            // update address before is default to none ;
                            DB::table('cscart_user_profiles')
                                ->where('user_id', $input['user_id'])
                                ->update(['default' => 0]);
                        }
                        $profile_id = DB::table('cscart_user_profiles')->insertGetId(
                            [
                                'user_id' => $input['user_id'],
                                'b_firstname' => $input['user_address']['full_name'],
                                'b_address' => $input['user_address']['address'],
                                'b_ward' => isset($input['user_address']['ward']) ? $input['user_address']['ward'] : '',
                                'b_state' => isset($input['user_address']['state']) ? $input['user_address']['state'] : '',
                                'b_county_district' => isset($input['user_address']['district']) ? $input['user_address']['district'] : '',
                                'b_country' => isset($input['user_address']['country']) ? $input['user_address']['country'] : 'VN',
                                'b_phone' => $input['user_address']['phone'],
                                'fullname' => $input['user_address']['full_name'],
                                's_firstname' => $input['user_address']['full_name'],
                                's_address' => $input['user_address']['address'],
                                's_ward' => isset($input['user_address']['ward']) ? $input['user_address']['ward'] : '',
                                's_state' => isset($input['user_address']['state']) ? $input['user_address']['state'] : '',
                                's_county_district' => isset($input['user_address']['district']) ? $input['user_address']['district'] : '',
                                's_country' => isset($input['user_address']['country']) ? $input['user_address']['country'] : 'VN',
                                's_phone' => $input['user_address']['phone'],
                                'default' => $default
                            ]
                        );
                        return (
                        array(
                            'status' => 200,
                            'message' => 'Thêm Thành công',
                            'profile_id' => $profile_id,
                        )
                        );
                    } else {
                        $errors = $valid->errors();
                        $data_error = [];
                        !$errors->has('full_name') ?: $data_error['full_name'] = $errors->first('full_name');
                        !$errors->has('phone') ?: $data_error['phone'] = $errors->first('phone');
                        !$errors->has('address') ?: $data_error['address'] = $errors->first('address');

                        return (
                        array(
                            'status' => 400,
                            'message' => 'Validate không thành công',
                            'errors' => $data_error,
                        )
                        );
                    }
                }
            }
        }
    }

    public function deleteAddress($request)
    {
        if (!empty($_GET['profile_id']) && !empty($_GET['user_id'])) {
            $total_address = DB::table('cscart_user_profiles')->where([
                ['user_id', '=', $_GET['user_id']],
                ['s_address', '<>', ''],
            ])->count();
            if ($total_address > 1) {
                // delete if total address > 1
                $datas = DB::table('cscart_user_profiles')
                    ->where([
                        ['profile_id', '=', (int)$_GET['profile_id']],
                        ['user_id', '=', (int)$_GET['user_id']],
                    ])->select('user_id', 'default')->first();

                if (!empty($datas->user_id)) {
                    DB::table('cscart_user_profiles')
                        ->where([
                            ['user_id', '=', $_GET['user_id']],
                            ['profile_id', '=', $_GET['profile_id']]
                        ])->delete();
                    // if is default address => check default other address
                    if ($datas->default) {
                        DB::table('cscart_user_profiles')->where([
                            ['user_id', $datas->user_id],
                            ['s_address', '<>', ''],
                        ])->limit(1)->update(['default' => 1]);
                    }
                    return [
                        'status' => 200,
                        'message' => 'Thành công'
                    ];
                } else {
                    return [
                        'status' => 400,
                        'message' => 'User ID không tồn tại'
                    ];
                }
            }
        }
    }
    public function updateAddress($request)
    {
        if (!empty($_GET['profile_id']) && !empty($_GET['user_id'])) {
            $input = $request->all();
            $datas = DB::table('cscart_user_profiles')
                ->where([
                    ['profile_id', '=', $_GET['profile_id']],
                    ['user_id', '=', $_GET['user_id']],
                ])
                ->select('user_id', 'default')
                ->first();
            if (!empty($datas->user_id)) {
                $total_address = DB::table('cscart_user_profiles')->where('user_id', $datas->user_id)->count();
                if ($total_address > 2) {
                    $default = !empty($input['user_address']['default']) ? 1 : 0;
                } else {
                    $default = 1;
                }
                if ($input['user_address']) {
                    $valid = Validator::make($input['user_address'],
                        [
                            'full_name' => 'required',
                            'phone' => 'required',
                            'address' => 'required'
                        ],
                        [
                            'full_name.required' => 'Họ và tên không được rỗng',
                            'phone.required' => 'Số điện thoại không được rỗng',
                            'address.required' => 'Địa chỉ nhận hàng không được rỗng ',
                        ]
                    );
                    if ($valid->passes()) {
                        if ($total_address > 1 && $default == 1) {
                            // update address before is default to none ;
                            DB::table('cscart_user_profiles')
                                ->where('user_id', $_GET['user_id'])
                                ->update(['default' => 0]);
                        }
                        // if before 1 and then 0 , update default 1 to other address diff address_id input
                        if ($input['user_address']['default'] == '0' && $datas->default == '1') {
                            $user_other = DB::table('cscart_user_profiles')
                                ->where([
                                    ['user_id', '=', $_GET['user_id']],
                                    ['profile_id', '<>', $_GET['profile_id']],
                                    ['s_address', '<>', '']
                                ])
                                ->get(['profile_id'])->first();

                            if (!empty($user_other->profile_id)) {
                                DB::table('cscart_user_profiles')
                                    ->where('profile_id', $user_other->profile_id)
                                    ->update([
                                        'default' => 1
                                    ]);
                            }
                        }
                        DB::table('cscart_user_profiles')
                            ->where([
                                ['user_id', '=', $_GET['user_id']],
                                ['profile_id', '=', $_GET['profile_id']]
                            ])
                            ->update([
                                'b_firstname' => $input['user_address']['full_name'],
                                'b_address' => $input['user_address']['address'],
                                'b_ward' => isset($input['user_address']['ward']) ? $input['user_address']['ward'] : '',
                                'b_state' => isset($input['user_address']['state']) ? $input['user_address']['state'] : '',
                                'b_county_district' => isset($input['user_address']['district']) ? $input['user_address']['district'] : '',
                                'b_country' => isset($input['user_address']['country']) ? $input['user_address']['country'] : 'VN',
                                'b_phone' => $input['user_address']['phone'],
                                'fullname' => $input['user_address']['full_name'],
                                's_firstname' => $input['user_address']['full_name'],
                                's_address' => $input['user_address']['address'],
                                's_ward' => isset($input['user_address']['ward']) ? $input['user_address']['ward'] : '',
                                's_state' => isset($input['user_address']['state']) ? $input['user_address']['state'] : '',
                                's_county_district' => isset($input['user_address']['district']) ? $input['user_address']['district'] : '',
                                's_country' => isset($input['user_address']['country']) ? $input['user_address']['country'] : 'VN',
                                's_phone' => $input['user_address']['phone'],
                                'default' => $default
                            ]);
                        return [
                            'status' => 200,
                            'message' => 'thành công',
                            'profile_id' => $_GET['profile_id']
                        ];
                    } else {
                        $errors = $valid->errors();
                        $data_error = [];
                        !$errors->has('full_name') ?: $data_error['full_name'] = $errors->first('full_name');
                        !$errors->has('phone') ?: $data_error['phone'] = $errors->first('phone');
                        !$errors->has('address') ?: $data_error['address'] = $errors->first('address');
                        return (
                        array(
                            'status' => 400,
                            'message' => 'Validate không thành công',
                            'errors' => $data_error,
                        )
                        );
                    }
                }
            } else {
                return [
                    'status' => 400,
                    'message' => 'User ID không tồn tại'
                ];
            }
        }
    }

    public function getState($state_id)
    {
        $state_name = DB::table('cscart_states AS s')
            ->leftJoin('cscart_state_descriptions AS sd', 'sd.state_id', '=', 's.state_id')
            ->select('sd.state')
            ->where(
                [
                    ['s.code', '=', $state_id],
                    ['sd.lang_code', '=', 'vi'],
                ]
            )->first();
        return isset($state_name->state) ? $state_name->state : '';
    }

    public function getDistrict($state, $distinct)
    {
        $distinct_name = DB::table('cscart_nk_districts AS d')
            ->leftJoin('cscart_nk_district_descriptions AS dd', 'dd.district_id', '=', 'd.district_id')
            ->select('dd.name')
            ->where(
                [
                    ['d.state_code', '=', $state],
                    ['d.code', '=', $distinct],
                    ['dd.lang_code', '=', 'vi'],
                ]
            )->first();
        return isset($distinct_name->name) ? $distinct_name->name : '';
    }

    public function getWard($state_id, $district_id, $ward_id)
    {
        $ward_name = DB::table('cscart_nk_wards AS w')
            ->leftJoin('cscart_nk_ward_descriptions AS wd', 'w.ward_id', '=', 'wd.ward_id')
            ->select('wd.name')
            ->where(
                [
                    ['w.code', '=', $ward_id],
                    ['w.district_code', '=', $district_id],
                    ['w.state_code', '=', $state_id],
                    ['wd.lang_code', '=', 'vi'],
                ]
            )->first();
        return isset($ward_name->name) ? $ward_name->name : '';

    }

    public function getAddressdefault($request, $user_id)
    {
        $outputs = array();
        $users = DB::table('cscart_users')->where('user_id', $user_id)->get(['user_id'])->first();

        if (!empty($users->user_id)) {
            $address = DB::table('cscart_user_profiles AS up')
                ->leftJoin('cscart_users AS u', 'u.user_id', '=', 'up.user_id')
                ->select(
                    'up.profile_id',
                    'up.fullname',
                    'up.s_firstname',
                    'up.s_address',
                    'up.s_country',
                    'up.s_state',
                    'up.s_county_district as s_district',
                    'up.s_ward',
                    'up.s_phone',
                    'up.default',
                    'up.gender',
                    'u.email'
                )
                ->where([
                    ['up.user_id', '=', $user_id],
                    ['up.default', '=', 1],
                ])
                ->first();

            $district_name = (isset($address->s_state) && isset($address->s_district)) ? $this->getDistrict($address->s_state, $address->s_district) : '';
            $state_name = isset($address->s_state) ? $this->getState($address->s_state) : '';
            $ward_name = (isset($address->s_district) && isset($address->s_ward)) ? $this->getWard($address->s_state, $address->s_district, $address->s_ward) : '';
            $outputs['profile_id'] = isset($address->profile_id) ? $address->profile_id : '';
            $outputs['fullname'] = isset($address->fullname) ? $address->fullname : '';
            $outputs['s_firstname'] = isset($address->s_firstname) ? $address->s_firstname : '';
            $outputs['s_address'] = isset($address->s_address) ? $address->s_address : '';
            $outputs['s_country'] = isset($address->s_country) ? $address->s_country : '';
            $outputs['s_state'] = isset($address->s_state) ? $address->s_state : '';
            $outputs['s_district'] = isset($address->s_district) ? $address->s_district : '';
            $outputs['s_ward'] = isset($address->s_ward) ? $address->s_ward : '';
            $outputs['state_name'] = $state_name;
            $outputs['district_name'] = $district_name;
            $outputs['ward_name'] = $ward_name;
            $outputs['s_phone'] = isset($address->s_phone) ? $address->s_phone : '';
            $outputs['email'] = isset($address->email) ? $address->email : '';
            $outputs['gender'] = isset($address->gender) ? $address->gender : '';
            return [
                'status' => 200,
                'meassage' => 'Thành công',
                'datas' => $outputs
            ];


        } else {
            return [
                'status' => 400,
                'meassage' => 'User ID không tồn tại',
                'datas' => $outputs
            ];
        }
    }

    public function getListAddress($request, $user_id)
    {
        $outputs = array();
        $list_address = array();
        if (!empty($user_id)) {
            $list_address = DB::table('cscart_user_profiles AS up')
                ->leftJoin('cscart_users AS u', 'u.user_id', '=', 'up.user_id')
                ->select(
                    'up.profile_id',
                    'up.fullname',
                    'up.s_firstname',
                    'up.s_address',
                    'up.s_country',
                    'up.s_state',
                    'up.s_county_district as s_district',
                    'up.s_ward',
                    'up.s_phone',
                    'up.default',
                    'u.email'
                )
                ->where('up.user_id', $user_id)
                ->orderBy('up.profile_id', 'desc')
                ->limit(3)
                ->get();
        }
        if ($list_address) {
            foreach ($list_address as $address) {
                $district_name = $this->getDistrict($address->s_state, $address->s_district);
                $state_name = $this->getState($address->s_state);
                $ward_name = $this->getWard($address->s_state, $address->s_district, $address->s_ward);
                if (!empty($address->s_address)) {
                    $outputs[] = array(
                        'profile_id' => $address->profile_id,
                        'fullname' => $address->fullname,
                        's_firstname' => $address->s_firstname,
                        's_address' => $address->s_address,
                        's_country' => $address->s_country,
                        's_state' => $address->s_state,
                        's_district' => $address->s_district,
                        's_ward' => $address->s_ward,
                        'state_name' => $state_name,
                        'district_name' => $district_name,
                        'ward_name' => $ward_name,
                        's_phone' => $address->s_phone,
                        'default' => $address->default,
                        'email' => $address->email
                    );
                }
            }
        }
        return [
            'code' => 200,
            'status' => true,
            'datas' => $outputs
        ];
    }

    public function getUser($request, $user_id)
    {
        $users = DB::table('cscart_users')->where('user_id', $user_id)->get(['user_id'])->first();
        if (!empty($users->user_id)) {
            $user_info = array();
            $hobbys = array();
            $user_data = DB::table('cscart_users AS u')
                ->leftJoin('cscart_user_profiles AS up', 'up.user_id', '=', 'u.user_id')
                ->select('u.firstname', 'u.lastname', 'u.phone', 'u.birthday', 'u.email', 'u.hobby_id', 'up.gender')
                ->where('u.user_id', $user_id)
                ->first();
            if ($user_data) {
                $user_info['firstname'] = $user_data->firstname;
                $user_info['lastname'] = $user_data->lastname;
                $user_info['birthday'] = !empty($user_data->birthday) ? date("Y-m-d", (int)$user_data->birthday) : '';
                $user_info['email'] = $user_data->email;
                $user_info['gender'] = $user_data->gender;
                $user_info['phone'] = $user_data->phone;
            }
            $hobbys = DB::table('cscart_hobby_description')->where('lang_code', 'vi')->get(['hobby_id', 'description']);
            if (isset($user_data->hobby_id)) {
                $hobby_user = explode(',', $user_data->hobby_id);
                foreach ($hobbys as $key => $hobby) {
                    if (in_array($hobby->hobby_id, $hobby_user)) {
                        $hobbys[$key]->checked = 1;
                    } else {
                        $hobbys[$key]->checked = 0;
                    }
                }
                unset($user_info->hobby_id);
                $user_info['hobbys'] = $hobbys;
            } else {
                foreach ($hobbys as $key => $hobby) {
                    $hobbys[$key]->checked = 0;
                }
                $user_info['hobbys'] = $hobbys;
            }
            return [
                'status' => 200,
                'message' => 'Thành công',
                'datas' => $user_info
            ];
        } else {
            return [
                'status' => 400,
                'message' => 'User ID không tồn tại',
            ];
        }
    }
    public function updateUser($request, $user_id)
    {
        $users = DB::table('cscart_users')->where('user_id', $user_id)->get(['user_id', 'email'])->first();
        if (!isset($users->user_id)) {
            return (
            array(
                'status' => 400,
                'message' => 'User ID không tồn tại',
            )
            );
        } else {
            $input = $request->all();
            $valid = Validator::make($input['user_info'],
                [
                    'full_name' => 'required',
                    'phone' => 'required',
                    //'birthday' => 'date|date_format:"Y-m-d"'
                ],
                [
                    'full_name.required' => 'Họ và tên không được rỗng',
                    'phone.required' => 'Số điện thoại không được rỗng',
                    //'birthday.date_format' => 'birthday không đúng định dạng ',
                    //'birthday.date' => 'birthday không hợp lệ'
                ]
            );
            if ($valid->passes()) {
                if (!empty($input['user_info']['birthday'])) {
                    if (isset($input['user_info']['mobile']) && $input['user_info']['mobile'] == 1) {
                        DB::table('cscart_users')
                            ->where('user_id', $user_id)
                            ->update([
                                'firstname' => $input['user_info']['full_name'],
                                'phone' => $input['user_info']['phone'],
                                'birthday' => date_timestamp_get(date_create($input['user_info']['birthday'])),
                                'hobby_id' => $input['user_info']['hobby'],
                            ]);
                    } else {
                        DB::table('cscart_users')
                            ->where('user_id', $user_id)
                            ->update([
                                'firstname' => $input['user_info']['full_name'],
                                'phone' => $input['user_info']['phone'],
                                'birthday' => date_timestamp_get(date_create($input['user_info']['birthday'])),
                                'hobby_id' => $input['user_info']['hobby'],
                                'state_id' => isset($input['user_info']['state_id']) ? $input['user_info']['state_id'] : '',
                                'district_id' => isset($input['user_info']['district_id']) ? $input['user_info']['district_id'] : '',
                                'ward_id' => isset($input['user_info']['ward_id']) ? $input['user_info']['ward_id'] : '',
                                'address' => isset($input['user_info']['address']) ? $input['user_info']['address'] : '',
                            ]);
                    }
                } else {
                    if (isset($input['user_info']['mobile']) && $input['user_info']['mobile'] == 1) {
                        DB::table('cscart_users')
                            ->where('user_id', $user_id)
                            ->update([
                                'firstname' => $input['user_info']['full_name'],
                                'phone' => $input['user_info']['phone'],
                                'hobby_id' => $input['user_info']['hobby'],
                            ]);
                    } else {
                        DB::table('cscart_users')
                            ->where('user_id', $user_id)
                            ->update([
                                'firstname' => $input['user_info']['full_name'],
                                'phone' => $input['user_info']['phone'],
                                'hobby_id' => $input['user_info']['hobby'],
                                'state_id' => isset($input['user_info']['state_id']) ? $input['user_info']['state_id'] : '',
                                'district_id' => isset($input['user_info']['district_id']) ? $input['user_info']['district_id'] : '',
                                'ward_id' => isset($input['user_info']['ward_id']) ? $input['user_info']['ward_id'] : '',
                                'address' => isset($input['user_info']['address']) ? $input['user_info']['address'] : '',
                            ]);
                    }
                }
                $input['user_info']['gender'] = (!isset($input['user_info']['gender']) || (isset($input['user_info']['gender']) && $input['user_info']['gender'] != 1)) ? 2 : 1;
                DB::table('cscart_user_profiles')
                    ->where('user_id', $user_id)
                    ->update([
                        'gender' => $input['user_info']['gender']
                    ]);

                if (isset($input['user_info']['mobile']) && $input['user_info']['mobile'] == 1) {
                } else {
                    $district_name = (!empty($input['user_info']['state_id']) && !empty($input['user_info']['district_id'])) ? $this->getDistrict($input['user_info']['state_id'], $input['user_info']['district_id']) : '';
                    $state_name = !empty($input['user_info']['state_id']) ? $this->getState($input['user_info']['state_id']) : '';
                    $ward_name = (!empty($input['user_info']['district_id']) && !empty($input['user_info']['ward_id'])) ? $this->getWard($input['user_info']['state_id'],$input['user_info']['district_id'], $input['user_info']['ward_id']) : '';
                }

                if (!empty($state_name)) {
                    $input['user_info']['state'] = $state_name;
                }
                if (!empty($district_name)) {
                    $input['user_info']['district'] = $district_name;
                }
                if (!empty($ward_name)) {
                    $input['user_info']['ward'] = $ward_name;
                }
                return [
                    'code' => 200,
                    'status' => true,
                    'user_info' => $input['user_info']
                ];
            } else {
                $errors = $valid->errors();
                $data_error = [];
                !$errors->has('full_name') ?: $data_error['full_name'] = $errors->first('full_name');
                !$errors->has('phone') ?: $data_error['phone'] = $errors->first('phone');
                //!$errors->has('birthday') ?: $data_error['birthday'] = $errors->first('birthday');
                return (
                array(
                    'status' => 400,
                    'message' => 'Validate không thành công',
                    'errors' => $data_error,
                )
                );
            }
        }
    }

    function fn_generate_salt($length = 10)
    {
        $length = $length > 10 ? 10 : $length;
        $salt = '';
        for ($i = 0; $i < $length; $i++) {
            $salt .= chr(rand(33, 126));
        }
        return $salt;
    }

    function fn_generate_salted_password($password, $salt)
    {
        $_pass = '';
        if (empty($salt)) {
            $_pass = md5($password);
        } else {
            $_pass = md5(md5($password) . md5($salt));
        }
        return $_pass;
    }

    public function show($request)
    {
        $params = $request->all();
        $email = isset($params['email']) ? $params['email'] : '';
        if ($email) {
            $users_info = DB::table('cscart_users')->where(
                [
                    ['email', ' = ', $email],
                    ['status', ' = ', 'A'],
                ]

            )->first();
            if (!empty($users_info['email'])) {
            } else {
                $datas = 'Email không phù hợp với bất kỳ tài khoản nào hiện đang được lưu trữ . Hãy chắc chắn bạn nhập đúng email và thử lại . ';
            }
        }
        return [
            'code' => 200,
            'status' => true
        ];
    }

    public function updateUserSocical($request, $user_id)
    {
        $users = DB::table('cscart_users')->where('user_id', $user_id)->get(['user_id', 'email'])->first();
        if (!isset($users->user_id)) {
            return (
            array(
                'status' => 400,
                'message' => 'User ID không tồn tại',
            )
            );
        } else {
            $input = $request->all();
            $valid = Validator::make($input['user_info'],
                [
                    'full_name' => 'required',
                    'phone' => 'required',
                    'birthday' => 'date|date_format:"Y-m-d"'

                ],
                [
                    'full_name.required' => 'Họ và tên không được rỗng',
                    'phone.required' => 'Số điện thoại không được rỗng',
                    'birthday.date_format' => 'birthday không đúng định dạng ',
                    'birthday.date' => 'birthday không hợp lệ'

                ]
            );
            if ($valid->passes()) {
                if (!empty($input['user_info']['birthday'])) {
                    DB::table('cscart_users')
                        ->where('user_id', $user_id)
                        ->update([
                            'firstname' => $input['user_info']['full_name'],
                            'phone' => $input['user_info']['phone'],
                            'birthday' => date_timestamp_get(date_create($input['user_info']['birthday'])),
                            'hobby_id' => $input['user_info']['hobby'],
                            'is_update_socical' => 1
                        ]);
                } else {
                    DB::table('cscart_users')
                        ->where('user_id', $user_id)
                        ->update([
                            'firstname' => $input['user_info']['full_name'],
                            'phone' => $input['user_info']['phone'],
                            'hobby_id' => $input['user_info']['hobby'],
                            'is_update_socical' => 1
                        ]);
                }
                $avatar = '';
                $input['user_info']['is_update_socical'] = 0;
                $users = DB::table('cscart_users')->where('user_id', $user_id)->get(['user_id', 'email','is_update_socical','avatar','avatar_socical','social_id'])->first();
                if (!empty($users)) {
                    $input['user_info']['avatar'] = 'https://www.nguyenkim.com/images/companies/_1/avatar_app/' . $users->avatar;
                    if ($users->social_id > 1) {
                        if ($users->avatar == '') {
                            $input['user_info']['avatar'] = $users->avatar_socical;
                        }
                    }
                    $input['user_info']['is_update_socical'] = $users->is_update_socical;

                }
                $input['user_info']['birthday'] = $input['user_info']['birthday'];
                $input['user_info']['user_id'] = $user_id;
                $input['user_info']['gender'] = (!isset($input['user_info']['gender']) || (isset($input['user_info']['gender']) && $input['user_info']['gender'] != 1)) ? 2 : 1;
                DB::table('cscart_user_profiles')
                    ->where('user_id', $user_id)
                    ->update([
                        'gender' => $input['user_info']['gender']
                    ]);
                return [
                    'status' => 200,
                    'user_info' => $input['user_info']
                ];
            } else {
                $errors = $valid->errors();
                $data_error = [];
                !$errors->has('full_name') ?: $data_error['full_name'] = $errors->first('full_name');
                !$errors->has('phone') ?: $data_error['phone'] = $errors->first('phone');
                !$errors->has('birthday') ?: $data_error['birthday'] = $errors->first('birthday');
                return (
                array(
                    'status' => 400,
                    'message' => 'Cập nhật không thành công',
                    'errors' => $data_error,
                )
                );
            }
        }
    }
}