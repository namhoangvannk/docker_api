<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/20/2018
 * Time: 3:13 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class LandingpageRepository extends RepositoryBase
{
    public function cloneLandingPages($clone_ldp_id, $new_ldp_id)
    {
        $landing_pages = DB::connection('mongodb')->collection('nk_landing_pages')->where('landing_page_id', (int)$clone_ldp_id)->first();
        if (isset($landing_pages['_id'])) {
            unset($landing_pages['_id']);
        }
        $landing_pages['landing_page_id'] = (int)$new_ldp_id;
        $landing_pages['name'] = '[CLONE]' . $landing_pages['name'];

        DB::connection('mongodb')->collection('nk_landing_pages')->insert($landing_pages);
    }

    public function cloneSeoLinks($clone_ldp_id, $new_ldp_id)
    {
        $landing_pages = DB::connection('mongodb')->collection('nk_seo_links')->where('object_id', (int)$clone_ldp_id)->where('type', '=', 'LDP')->first();
        if (isset($landing_pages['_id'])) {
            unset($landing_pages['_id']);
        }
        $landing_pages['object_id'] = (int)$new_ldp_id;
        $landing_pages['url'] = 'seoname-clone' . '-' . $landing_pages['url'];
        DB::connection('mongodb')->collection('nk_seo_links')->insert($landing_pages);
    }

    public function cloneLandingPagesLayouts($clone_ldp_id, $new_ldp_id)
    {
        $landing_pages = DB::connection('mongodb')->collection('nk_landing_page_layouts')->where('landing_page_id', (int)$clone_ldp_id)->first();
        if (isset($landing_pages['_id'])) {
            unset($landing_pages['_id']);
        }
        $landing_pages['landing_page_id'] = (int)$new_ldp_id;
        DB::connection('mongodb')->collection('nk_landing_page_layouts')->insert($landing_pages);
    }

    public function cloneLandingPagesAdminLayouts($clone_ldp_id, $new_ldp_id)
    {
        $landing_pages = DB::connection('mongodb')->collection('nk_landing_page_admin_layouts')->where('landing_page_id', (int)$clone_ldp_id)->first();
        if (isset($landing_pages['_id'])) {
            unset($landing_pages['_id']);
        }
        $landing_pages['landing_page_id'] = (int)$new_ldp_id;
        DB::connection('mongodb')->collection('nk_landing_page_admin_layouts')->insert($landing_pages);
    }

    public function cloneLDP($request)
    {
        try {
            $this->cloneLandingPages($_GET['clone_ldp_id'], $_GET['new_ldp_id']);
            $this->cloneSeoLinks($_GET['clone_ldp_id'], $_GET['new_ldp_id']);
            $this->cloneLandingPagesLayouts($_GET['clone_ldp_id'], $_GET['new_ldp_id']);
            $this->cloneLandingPagesAdminLayouts($_GET['clone_ldp_id'], $_GET['new_ldp_id']);
            return array(
                'status' => 200,
                'message' => 'Thành công',
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function details($landing_page_id)
    {
        $outputs = array();
        $landing_pages = array();
        $landing_pages = DB::connection('mongodb')->collection('nk_landing_pages')->where('landing_page_id', (int)$landing_page_id)->first();
        if (isset($landing_pages['_id'])) {
            unset($landing_pages['_id']);
        }
        $landing_pages = (count($landing_pages)) > 0 ? $landing_pages : array();
        $layouts = DB::connection('mongodb')->collection('nk_landing_page_layouts')->where('landing_page_id', (int)$landing_page_id)->select('layouts')->first();
        if (!empty($layouts['layouts'])) {
            $outputs['layouts'] = $layouts['layouts'];
        }
        $seo_url = DB::connection('mongodb')->collection('nk_seo_links')->where('object_id', (int)$landing_page_id)->select('url')->first();
        if (!empty($seo_url['seo_url'])) {
            $outputs['seo_url'] = $seo_url['url'];
        }

        if (!empty($_GET['is_admin'])) {
            $outputs['admin'] = DB::connection('mongodb')->collection('nk_landing_page_admin_layouts')->where('landing_page_id', (int)$landing_page_id)->first();
            if (isset($outputs['admin']['_id'])) {
                unset($outputs['admin']['_id']);
            }
        }
        return array_merge($landing_pages, $outputs);
    }

    public function getDetailLandingpage($request, $landing_page_id)
    {
        try {
            $details = $this->details($landing_page_id);
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $details
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function updateStatus()
    {
        try {
            if (!empty($_GET['landing_page_id']) && !empty($_GET['new_status'])) {
                if (in_array($_GET['new_status'], array('A', 'D'))) {
                    DB::connection('mongodb')->collection('nk_landing_pages')->where('landing_page_id', '=', (int)$_GET['landing_page_id'])->update(['status' => $_GET['new_status']]);
                    return array(
                        'status' => 200,
                        'message' => 'Thành công',
                    );
                } else {
                    return array(
                        'status' => 400,
                        'message' => 'Không thành công',
                    );
                }
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function delete($request)
    {
        try {
            if (!empty($_GET['landing_page_id'])) {
                DB::connection('mongodb')->collection('nk_landing_pages')->where('landing_page_id', '=', (int)$_GET['landing_page_id'])->delete();
                DB::connection('mongodb')->collection('nk_landing_page_layouts')->where('landing_page_id', '=', (int)$_GET['landing_page_id'])->delete();
                DB::connection('mongodb')->collection('nk_landing_page_admin_layouts')->where('landing_page_id', '=', (int)$_GET['landing_page_id'])->delete();
                DB::connection('mongodb')->collection('nk_seo_links')->where('type', 'LDP')->where('object_id', '=', (int)$_GET['landing_page_id'])->delete();
                return array(
                    'status' => 200,
                    'message' => 'Thành công',
                );
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function save($request)
    {
        try {
            $input = $request->all();
            if (!empty($input['nk_seo_links']['url'])) {
                if (!empty($_GET['landing_page_id'])) {
                    $object_id = DB::connection('mongodb')->collection('nk_seo_links')->where('type', 'LDP')->where('object_id', '=', (int)$_GET['landing_page_id'])->select('object_id')->first();
                    if (!empty($object_id)) {
                        $seo_name = DB::connection('mongodb')->collection('nk_seo_links')->where('url', $input['nk_seo_links']['url'])->where('object_id', '<>', (int)$_GET['landing_page_id'])->select('url')->first();
                    } else {
                        $seo_name = DB::connection('mongodb')->collection('nk_seo_links')->where('url', $input['nk_seo_links']['url'])->select('url')->first();
                    }
                } else {
                    $seo_name = DB::connection('mongodb')->collection('nk_seo_links')->where('url', $input['nk_seo_links']['url'])->select('url')->first();
                }
                if (!empty($seo_name['url'])) {
                    return (
                    array(
                        'status' => 400,
                        'message' => 'SEO name đã tồn tại'
                    )
                    );
                } else {
                    foreach ($input as $table_name => $value) {
                        if ($table_name) {
                            if (!empty($_GET['landing_page_id'])) {
                                if ($table_name == 'nk_seo_links') {
                                    $type_seo_name = DB::connection('mongodb')->collection('nk_seo_links')->where('object_id', '<>', (int)$_GET['landing_page_id'])->select('type')->first();
                                    if ($type_seo_name['type'] == 'LDP') {
                                        DB::connection('mongodb')->collection($table_name)->where('object_id', '=', (int)$_GET['landing_page_id'])->delete();
                                    }
                                } else {
                                    DB::connection('mongodb')->collection($table_name)->where('landing_page_id', '=', (int)$_GET['landing_page_id'])->delete();
                                }
                            }
                            if ($table_name != 'landing_page_id') {
                                DB::connection('mongodb')->collection($table_name)->insert($input[$table_name]);
                            }
                        }
                    }
                    return array(
                        'status' => 200,
                        'message' => 'Thành công',
                    );
                }
            } else {
                return (
                array(
                    'status' => 400,
                    'message' => 'SEO name không được trống'
                )
                );
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkSeoLandingpage($request, $seo_name)
    {
        try {
            $seo_links = DB::connection('mongodb')->collection('nk_seo_links')->where('url', $seo_name)->select('object_id')->first();
            if (isset($seo_links['object_id'])) {
                $object_id = $seo_links['object_id'];
            } else {
                $object_id = 0;
            }
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'object_id' => $object_id
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getListLandingpage($request)
    {
        $outputs = array();
        try {
            $limit = $request->input('limit', 10);
            $offset = ($request->input('page')) ? $request->input('page') : 1;
            $start = ($offset - 1) * $limit;
            $where = [];
            if (!empty($_GET['name'])) {
                array_push($where, ['name', 'like', '%' . $_GET['name'].'%']);
            }
            if (!empty($_GET['start_time'])) {
                array_push($where, ['start_time', '=', (int) $_GET['start_time']]);
            }
            if (!empty($_GET['end_time'])) {
                array_push($where, ['end_time', '=', (int) $_GET['end_time']]);
            }
            if (!empty($_GET['status'])) {
                array_push($where, ['status', '=', $_GET['status']]);
            }
            $outputs['details'] = DB::connection('mongodb')->collection('nk_landing_pages')->where($where)->skip((int)$start)->take((int)$limit)->get();
            $outputs['totals'] = DB::connection('mongodb')->collection('nk_landing_pages')->where($where)->count();
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $outputs,
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}