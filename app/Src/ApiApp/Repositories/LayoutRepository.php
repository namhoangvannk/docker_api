<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 3/7/2019
 * Time: 9:50 AM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Cache\RedisClient;
use Carbon\Carbon;
use DB;
use Log;
use function FastRoute\TestFixtures\empty_options_cached;


class LayoutRepository extends RepositoryBase
{
    public function getBlockdetails($block_id)
    {
        $blocks = DB::table('cscart_bm_blocks_content')
            ->where([
                ['lang_code', '=', 'vi'],
                ['block_id', '=', (int)$block_id],
            ])->select('content')->first();
        $block = DB::table('cscart_bm_blocks')
            ->where([
                ['block_id', '=', (int)$block_id],
            ])->select('properties')->first();
        if (isset($blocks->content) && !empty($blocks->content)) {
            $config = unserialize($blocks->content);
            $qty = unserialize($block->properties);
            if(isset($config['items']['filling']) == 'best_promotion'){
                $check_param  = isset($config['items']['quantity_product']) && isset($config['items']['cid']);
                if($check_param){
                    $total_products = (int)$config['items']['quantity_product'];
                    $category_ids = $config['items']['cid'];
                    $category_ids = explode(',',$category_ids);
                    $result = [];
                    $max_item = 0;
                    $limit = ceil($total_products/count($category_ids));
                    if(count($category_ids)>0){
                        foreach ($category_ids as $cid){
                            $result[$cid] = DB::table('cscart_products as t1')
                                ->join('cscart_product_prices as price', function ($join) {
                                    $join->on('t1.product_id', '=', 'price.product_id')
                                        ->where('price.lower_limit', '=', 1);
                                })
                                ->join('cscart_products_categories as t3', function ($join) {
                                    $join->on('t1.product_id', '=', 't3.product_id');
                                })
                                ->join('cscart_categories as t4', function ($join) {
                                    $join->on('t3.category_id', '=', 't4.category_id');
                                })
                                ->where('t1.status','A')
                                ->where('t1.hidden_option','N')
                                ->where('t4.category_id',(int)$cid)
                                ->where(DB::raw("((t1.list_price - price.price)/t1.list_price*100)"),'>',0)
                                ->orderBy(DB::raw("((t1.list_price - price.price)/t1.list_price*100)"),'DESC')
                                ->limit($limit)
                                ->get(['t1.product_id']);
                            $max_item = $max_item<count( $result[$cid])?count( $result[$cid]):$max_item;
                        }
                        $final_product_ids = [];
                        for($i=0;$i<$max_item;$i++){
                            foreach ($result as $cid => $dssp){
                                if(count($dssp)>$i){
                                    $final_product_ids[] = $dssp[$i];
                                }
                            }
                        }

                        if(!empty($final_product_ids)){
                            $arr = '';
                            foreach ($final_product_ids as $key => $value){
                                foreach ($value as $k => $v){
                                    $arr.= $v.",";
                                }
                            }
                            return explode(',', $arr);
                        }
                    }
                }
            }
            if (isset($config['items']['item_ids'])) {
                $final_product_ids = explode(',', $config['items']['item_ids']);
                if(!empty($final_product_ids)){
                    $arr = '';
                    if(isset($qty['limit_quantity']) && (int)$qty['limit_quantity'] > 0){
                        $i= 0;
                        foreach ($final_product_ids as $key => $value){
                            $i++;
                            if($i<= (int)$qty['limit_quantity']){
                                $arr.= (int)$value.",";
                            }
                        }
                        return explode(',', $arr);
                    }else{
                        foreach ($final_product_ids as $key => $value){
                            $arr.= (int)$value.",";
                        }
                        return explode(',', $arr);
                    }
                }
            }
        }
        return array();
    }
    public function getBlockID($gird_id)
    {
        $data_block = array();
        $blocks = DB::table('cscart_bm_snapping')->where([
            ['status', '=', 'A'],
            ['grid_id', '=', (int)$gird_id],
        ])->select('block_id')->get();

        if ($blocks) {
            foreach ($blocks as $block_id) {
                if ($block_id->block_id) {
                    $data_block[] = $block_id->block_id;
                }
            }
        }
        return $data_block;
    }
    public function process($id)
    {
        list($first, $host) = explode(".", $_SERVER["SERVER_NAME"]);
        if ($first == 'dev' || $first == 'dev1' || $first == 'dev2' || $first == 'test') {
            $url= 'https://test.nguyenkimonline.com/';
        } elseif ($first == 'beta') {
            $url = 'https://test.nguyenkimonline.com/';
        } else {
            $url = 'https://www.nguyenkim.com/';
        }
        $home_page = config('constants.home_page');
        $data_container = array();
        $containers = DB::table('cscart_bm_containers')->where([
            ['status', '=', 'A'],
            ['location_id', '=', (int)$id],
        ])->select('container_id')->get();
        if ($containers) {
            foreach ($containers as $container) {
                $data_container[] = $container->container_id;
            }
            if (count($data_container) > 0) {
                $data_grid = array();
                foreach ($data_container as $container_id) {
                    //1575
                    $grids = DB::table('cscart_bm_grids')->where([
                        ['status', '=', 'A'],
                        ['container_id', '=', (int)$container_id],
                    ])->select('grid_id')->get();
                    if ($grids) {
                        foreach ($grids as $grid_id) {
                            $data_grid[$container_id][] = $grid_id->grid_id;
                        }
                    }
                }
                if ($data_grid) {
                    $data_block = array();
                    foreach ($data_grid as $grids) {
                        foreach ($grids as $grid_id) {
                            if (count($this->getBlockID($grid_id)) > 0) {
                                $data_block[$grid_id] = $this->getBlockID($grid_id);
                            }
                        }
                    }
                }
                $outputs = array();
                if ($data_block) {
                    foreach ($data_block as $block) {
                        foreach ($block as $block_id) {
                            $datas = $this->getBlockdetails($block_id);
                            // if (count($datas) > 0 && isset($home_page[$block_id])) {
                            $key_block = 'BLOCK_'.$block_id;
                            if (count($datas) > 0) {
                                $datas2 = [];
                                foreach($datas as $key => $value){
                                        $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', (int)$value)->where('status','A')->get([
                                            'product_id'
                                            , 'mobile_short_description'
                                            , 'product_code'
                                            , 'amount'
                                            , 'weight'
                                            , 'length'
                                            , 'width'
                                            , 'height'
                                            , 'list_price'
                                            , 'price'
                                            , 'product'
                                            , 'nk_shortname'
                                            , 'main_pair'
                                            , 'category_ids'
                                            , 'nk_tragop_0'
                                            , 'product_options'
                                            , 'tag_image'
                                            , 'nk_is_shock_price'
                                            , 'product_features'
                                            , 'nk_product_tags'
                                            , 'text_shock'
                                            , 'shock_online_exp'
                                            , 'nk_special_tag'
                                            , 'offline_price'
                                            , 'model'
                                            , 'status'
                                            , 'hidden_option'
                                            , 'nk_show_two_price'
                                            , 'nk_discount_label'
                                            , 'tag_image_special'
                                            , 'tag_start_time_special'
                                            , 'tag_end_time_special'
                                            ,'nk_check_amount'
                                            ,'nk_check_total_amount'
                                            ,'rating'
                                            ,'comment'
                                        ])->unique('product_id');
                                        if(!empty($data_products1s)){
                                            foreach ($data_products1s as $data_products1) {
                                                $data1['product_id'] = $data_products1['product_id'];
                                                $data1['rating'] = isset($data_products1['comment']) ? (float)$data_products1['comment'] : 0;
                                                $data1['comment'] = isset($data_products1['rating']) ? (float)$data_products1['rating'] : 0;
                                                $data1['status'] = isset($data_products1['status']) ? $data_products1['status'] : '';
                                                $data1['hidden_option'] = $data_products1['hidden_option'];
                                                $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                                                $data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                                                $data1['discount_percent'] = 0;
                                                if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                                    $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                                                }
                                                $data1['check_stock'] = 0; // chay hang
                                                if($data_products1['amount'] > $data_products1['nk_check_amount'] && $data_products1['amount'] > $data_products1['nk_check_total_amount']){
                                                    $data1['check_stock'] = 1;
                                                }
                                                $data_products['text_promotion'] = [];
                                                $data1['images'] = '';
                                                $data1['tag_image_top_left'] = '';
                                                $data1['tag_image_top_right'] = '';
                                                $data1['tag_image_bottom_left'] = '';
                                                $data1['tag_image_bottom_right'] = '';
                                                $data1['model'] = '';
                                                $data1['brand'] = '';
                                                if (!empty($data_products1['nk_product_tags'])) {
                                                    foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                                        if ($val['tag_position'] == 'bottom-right') {
                                                            $data1['tag_image_bottom_right'] = $val['tag_image'];
                                                        } elseif ($val['tag_position'] == 'bottom-left') {
                                                            $data1['tag_image_bottom_left'] = $val['tag_image'];
                                                        } elseif ($val['tag_position'] == 'top-right') {
                                                            $data1['tag_image_top_right'] = $val['tag_image'];
                                                        } elseif ($val['tag_position'] == 'top-left') {
                                                            $data1['tag_image_top_left'] = $val['tag_image'];
                                                        }
                                                    }
                                                }
                                                if ($data_products1) {
                                                    if (!empty($data_products1['product_options'])) {
                                                        foreach ($data_products1['product_options'] as $key => $val) {
                                                            if (trim($val['nk_uu_dai_len_den']) != '') {
                                                                $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    $image = '';
                                                    $thumb = $data_products1['main_pair'];
                                                    if(!empty($thumb['icon'])){
                                                        $image = $thumb['icon'];
                                                    }else{
                                                        if(!empty($thumb['detailed']))
                                                            $image = $thumb['detailed'];
                                                    }
                                                    if($image !=''){
                                                        $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                                    }

                                                    if (trim($data_products1['tag_image_special']) != '') {
                                                        $time = time();
                                                        if ($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']) {
                                                            $data1['images'] = $data_products1['tag_image_special'];
                                                        }
                                                    }


                                                    if (!empty($data_products1['product_features'])) {
                                                        if (!empty($data_products1['product_features']['591'])) {
                                                            if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                                                if (!empty($data_products1['product_features']['591']['subfeatures']['599'])) {
                                                                    $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                                    if (!empty($data_variant['variants'])) {
                                                                        foreach ($data_variant['variants'] as $item) {
                                                                            $data1['brand'] = $item['variant'];
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (!empty($data_products1['product_features'])) {
                                                        if (!empty($data_products1['product_features']['591'])) {
                                                            if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                                                if (!empty($data_products1['product_features']['591']['subfeatures']['596'])) {
                                                                    $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                                                    if (!empty($data_variant['variants'])) {
                                                                        foreach ($data_variant['variants'] as $item) {
                                                                            $data1['model'] = $item['variant'];
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                $data1['product_id'] = $data_products1['product_id'];
                                                $data1['description'] = $data_products1['mobile_short_description'];
                                                $data1['product_code'] = $data_products1['product_code'];
                                                $data1['amount'] = $data_products1['amount'];
                                                $data1['weight'] = $data_products1['weight'];
                                                $data1['list_price'] = (int)$data_products1['list_price'];
                                                if ($data_products1['nk_show_two_price'] == 'N' && ($data_products1['nk_is_shock_price'] != 'Y' && $data_products1['nk_is_shock_price'] != 'O')) {
                                                    $data1['list_price'] = (int)$data_products1['price'];
                                                }
                                                $data1['price'] = (int)$data_products1['price'];
                                                $data1['offline_price'] = (int)$data_products1['offline_price'];
                                                $data1['product_name'] = $data_products1['product'];
                                                $data1['shortname'] = $data_products1['nk_shortname'];
                                                $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                                                $data1['categories'] = $data_products1['category_ids'];
                                                $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                                                $data1['length'] = $data_products1['length'];
                                                $data1['width'] = $data_products1['width'];
                                                $data1['height'] = $data_products1['height'];
                                                $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                                                $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                                                $data1['text_shock'] = $data_products1['text_shock'];
                                                $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                                                $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                                                // fix hiện giá rẻ online khỏi sửa bên app
                                                if (isset($data_products1['text_shock']) && !empty($data_products1['text_shock'])) {
                                                    $data1['nk_special_tag'] = 1;
                                                }
                                                $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                                                $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                                                $data1['text_tragop'] = '';
                                                //if(($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] =='O'){
                                                if($data_products1['nk_tragop_0'] == 1)
                                                    $data1['text_tragop'] = get_text_tra_gop($data_products1['price']);
                                                //}
                                                $datas2[] = $data1;
                                            }
                                        }
                                }
                                RedisClient::setValue($key_block, $datas2, 72000);
                                //   $outputs[$block_id] = $home_page[$block_id] ;
                                $outputs[$block_id] = $datas;
                            }
                        }
                    }
                }

                if (count($outputs) > 0) {
                    DB::connection('mongodb')->collection('home_page')->where('home_page', '=', $id)->delete();
                    DB::connection('mongodb')->collection('home_page')->insert([
                        ['home_page' => $id, 'datas' => json_encode($outputs)],
                    ]);
                }
            }
        }
    }
    public function index($request)
    {
        if (!empty($_GET['ids'])) {
            $pos = strpos($_GET['ids'], '-');
            if ($pos === false) {
                $this->process((int)$_GET['ids']);
            } else {
                $ids = explode('-', $_GET['ids']);
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $this->process((int)$id);
                    }
                }
            }
            return array(
                'status' => 200,
                'message' => 'Thành công'
            );

        } else {
            return array(
                'status' => 400,
                'message' => 'Không thành công'
            );
        }
    }
    public function giadung($request)
    {
        
        $category_menu_giadung = config('constants.category_menu_giadung');
        $catid = $request->input('catid');
        $data = $request->input('data');
        if($catid == 1){
            foreach ($data as $k => $v){
                file_put_contents('data_log1.log', 'catid:'.$catid. '---data----'.var_export($v,true),FILE_APPEND);
                foreach($category_menu_giadung as $key => $val){
                    if($k == $val['id']){
                        $key_block = 'CATES_'.$val['id'];
                        RedisClient::setValue($key_block, $v, 720000);
                    }
                }
            }
        }else{
            file_put_contents('data_log.log', 'catid:'.$catid. '---data----'.var_export($data,true),FILE_APPEND);
            foreach($category_menu_giadung as $key => $val){
                if($request->input('catid') == $val['id']){
                    $key_block = 'CATE_'.$val['id'];
                    RedisClient::setValue($key_block, $request->input('data'), 720000);
                }
            }
        }

        return array(
            'status' => 200,
            'message' => 'Thành công'
        );
    }
    public function blog($request)
    {
        
        $category_menu_giadung = config('constants.category_menu_giadung');
        $catid = $request->input('catid');
        $data = $request->input('data');
        $key_block = 'BLOG';
        RedisClient::setValue($key_block, $request->input('data'), 72000000);
        return array(
            'status' => 200,
            'message' => 'Thành công'
        );
    }
}
