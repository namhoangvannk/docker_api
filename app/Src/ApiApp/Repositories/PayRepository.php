<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 9/11/2018
 * Time: 8:51 AM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class PayRepository extends RepositoryBase
{
    protected $table_name = 'cscart_nk_tragop_configs';

    public function getlistConfig($product_id, $quantity)
    {
        $data_product = array();
        $list_tragop = array();
        $outputs = array();
        $category = array();

        $data_product['product_id'] = (int)$product_id;
        // gia ban
        $price = 0;
        $product_price = DB::table('cscart_product_prices')->where('product_id', (int)$product_id)->get(['price'])->first();
        if (!empty($product_price->price)) {
            $price = $product_price->price;
        }

        $products = DB::table('cscart_products')->where('product_id', (int)$product_id)->get(['product_id'])->first();
        if (!empty($products->product_id)) {
            // lấy giá trả góp của sàn phẩm với product_id
            $data_installment_price_type = DB::table('cscart_products')->where('product_id', $product_id)->get(['installment_price_type'])->first();
            $installment_price_type = $data_installment_price_type->installment_price_type;
            if ($installment_price_type == 'B') {
                $d_price = DB::table('cscart_product_prices')->where('product_id', $product_id)->get(['price'])->first();
                $data_product['price'] = (float)$d_price->price;
            } elseif ($installment_price_type == 'Y') {
                $d_price = DB::table('cscart_products')->where('product_id', $product_id)->get(['list_price'])->first();
                $data_product['price'] = (float)$d_price->list_price;
            } else {
                $d_price = DB::table('cscart_products')->where('product_id', $product_id)->get(['installment_price'])->first();
                $data_product['price'] = (float)$d_price->installment_price;
            }
            $d_product_code = DB::table('cscart_products')->where('product_id', $product_id)->get(['product_code'])->first();
            $data_product['product_code'] = $d_product_code->product_code;
            $d_category = DB::table('cscart_products_categories as cpc')
                ->leftJoin('cscart_categories as c', 'c.category_id', '=', 'cpc.category_id')
                ->where(
                    [
                        ['cpc.product_id', '=', $product_id],
                        ['c.level', '=', 2],
                    ]
                )
                ->get(['cpc.category_id']);
            if ($d_category) {
                foreach ($d_category as $cat) {
                    $category[] = $cat->category_id;
                }
            }
            $data_product['category'] = $category;
            $max_loan = (float)$data_product['price'] * 0.3;
            $d_list_tragop = DB::table($this->table_name)
                ->where(
                    [
                        ['status', '=', 1],
                        ['from_time', '<=', date('Y-m-d H:i:s')],
                        ['to_time', '>=', date('Y-m-d H:i:s')],
                        //   ['to_loan', '>=', $max_loan],
                    ]
                )
                ->whereIn('approval', ['Onl', 'All', 'Off'])
                ->get(['id', 'inst', 'type', 'product_code', 'from_loan', 'to_loan', 'from_price', 'to_price', 'approval', 'category', 'except_prod', 'except_cate', 'kyhanvay', 'laisuat', 'tratruoc', 'phithuho_month', 'paper_code', 'insurance_rate','initial_fee']);
            if ($d_list_tragop) {
                foreach ($d_list_tragop as $tragop) {
                    $list_tragop[$tragop->id] = array(
                        'id' => $tragop->id,
                        'inst' => $tragop->inst,
                        'type' => $tragop->type,
                        'from_price' => $tragop->from_price,
                        'to_price' => $tragop->to_price,
                        'from_loan' => $tragop->from_loan,
                        'to_loan' => $tragop->to_loan,
                        'product_code' => (!empty($tragop->product_code) && trim($tragop->product_code) <> 'all') ? explode(',', trim($tragop->product_code)) : trim($tragop->product_code),
                        'category' => (!empty($tragop->category) && $tragop->category != 'all') ? explode(',', trim($tragop->category)) : trim($tragop->category),
                        'except_prod' => (!empty($tragop->except_prod) && $tragop->except_prod != 'all') ? explode(',', trim($tragop->except_prod)) : trim($tragop->except_prod),
                        'except_cate' => (!empty($tragop->except_cate) && $tragop->except_cate != 'all') ? explode(',', trim($tragop->except_cate)) : trim($tragop->except_cate),
                        'kyhanvay' => (isset($tragop->kyhanvay)) ? explode(',', trim($tragop->kyhanvay)) : array(),
                        'tratruoc' => (isset($tragop->tratruoc)) ? explode(',', trim($tragop->tratruoc)) : array(),
                        'laisuat' => $tragop->laisuat,
                        'phithuho_month' => $tragop->phithuho_month,
                        'paper_code' => $tragop->paper_code,
                        'insurance_rate' => $tragop->insurance_rate,
                        'approval' => $tragop->approval,
                        'initial_fee' => $tragop->initial_fee,
                        'price' => $data_product['price'],
                        'giaban' => $price
                    );
                }
            }

            if ($list_tragop) {
                foreach ($list_tragop as $id => $list_tragop_info) {
                    if ((float)$list_tragop_info['from_price'] > (float)$data_product['price'] || (float)$list_tragop_info['to_price'] < (float)$data_product['price']) {
                        unset($list_tragop[$id]);
                    }
                    if (!empty($list_tragop_info['to_loan']) && $max_loan > (float)$list_tragop_info['to_loan']) {
                        unset($list_tragop[$id]);
                    }
                    // RULE FOR TYPE =  PRODUCT
                        if ($list_tragop_info['type'] == 'p') {
                        if (!is_array($list_tragop_info['product_code']) && trim($list_tragop_info['product_code']) <> 'all') {
                            unset($list_tragop[$id]);
                        } elseif (is_array($list_tragop_info['product_code']) && !in_array($data_product['product_code'], $list_tragop_info['product_code'])) {
                            unset($list_tragop[$id]);
                        }
                        // CHECK PRODUCT_CODE NOT IN FEILD except_prod REMOVE RULE IF TRUE
                        if (is_array($list_tragop_info['except_prod']) && in_array($data_product['product_code'], $list_tragop_info['except_prod'])) {
                            unset($list_tragop[$id]);
                        }
                        //CHECK CATEGORY NOT IN FEILD except_cate REMOVE RULE IF TRUE
                        if (is_array($data_product['category']) && is_array($list_tragop_info['except_cate'])) {
                            $intersect = array_intersect($data_product['category'], $list_tragop_info['except_cate']);
                            if (count($intersect) > 0) {
                                unset($list_tragop[$id]);
                            }
                        }

                    } elseif ($list_tragop_info['type'] == 'c') {
                        // RULE FOR TYPE =  CATEGORY
                        if (!is_array($list_tragop_info['category']) && trim($list_tragop_info['category']) <> 'all') {
                            unset($list_tragop[$id]);
                        } elseif (is_array($list_tragop_info['category']) && is_array($data_product['category'])) {
                            if (count($data_product['category']) == count($list_tragop_info['category'])) {
                                $diff = array_diff($data_product['category'], $list_tragop_info['category']);
                                if (count($diff) > 0) {
                                    unset($list_tragop[$id]);
                                }
                            } elseif (count($data_product['category']) > count($list_tragop_info['category'])) {
                                $diff = array_diff($list_tragop_info['category'], $data_product['category']);
                                if (count($diff) > 0) {
                                    unset($list_tragop[$id]);
                                }
                            } else {
                                $diff = array_diff($data_product['category'], $list_tragop_info['category']);
                                if (count($diff) > 0) {
                                    unset($list_tragop[$id]);
                                }
                            }
                        }
                        //CHECK CATEGORY NOT IN FEILD except_cate REMOVE RULE IF TRUE
                        if (is_array($data_product['category']) && is_array($list_tragop_info['except_cate'])) {
                            $intersect = array_intersect($data_product['category'], $list_tragop_info['except_cate']);
                            if (count($intersect) > 0) {
                                unset($list_tragop[$id]);
                            }
                        }
                        // CHECK PRODUCT_CODE NOT IN FEILD except_prod REMOVE RULE IF TRUE
                        if (is_array($list_tragop_info['except_prod']) && in_array($data_product['product_code'], $list_tragop_info['except_prod'])) {
                            unset($list_tragop[$id]);
                        }
                    }
                }
            }
            if ($list_tragop) {
                $outputs['price'] = (float)$data_product['price'];
                foreach ($list_tragop as $tragop) {
                    $outputs['rule'][] = array(
                        'ids' => $tragop['id'],
                        'tratruoc' => $tragop['tratruoc'],
                        'kyhanvay' => $tragop['kyhanvay'],
                        'laisuat' => $tragop['laisuat'],
                        'from_loan' => $tragop['from_loan'],
                        'to_loan' => $tragop['to_loan'],
                        'inst' => $tragop['inst'],
                        'phithuho_month' => $tragop['phithuho_month'],
                        'paper_code' => $tragop['paper_code'],
                        'insurance_rate' => $tragop['insurance_rate'],
                        'price' => $outputs['price'], // gia tra gop
                        'approval' => $tragop['approval'],
                        'initial_fee' => $tragop['initial_fee'],
                        'giaban' => $price,
                    );
                }
            }
            return $outputs;
        } else {
            return array();

        }
    }
    public function packageOptimize($request)
    {
        $optimizes = array();
        $input = $request->all();
        $tratruoc = $kyhanvay = array();
        $laixuat = array();
        $sort_by_percent = $sort_by_kyhan = array();
        $quantity = (!isset($input['quantity']) || !is_int($input['quantity']))? 1 : (int) $input['quantity'] ;
        $packages = $this->getlistConfig($input['product_id'], $quantity);
        if (isset($packages['rule']) && count($packages['rule']) > 0) {
            $price = (float)$packages['price'] * $quantity;
            $optimize_kyhanvay = array();
            foreach ($packages['rule'] as $package_id => $package_info) {
                $laixuat[] = $package_info['laisuat'];
                $optimize_kyhanvay[$package_info['ids']] = array(
                    'ids' => $package_info['ids'],
                    'kyhanvay' => $package_info['kyhanvay'],
                    'laisuat' => $package_info['laisuat']
                );
                if (count($package_info['tratruoc']) > 0) {
                    foreach ($package_info['tratruoc'] as $value) {
                        $tratruoc[$value] = array(
                            'percent' => $value,
                            'price' => (float)($price * $value) / 100,
                            'label' => $value . '% - ' . number_format((($price * $value) / 100), 0, ",", ".") . 'đ'
                        );
                    }
                }
            }
            ksort($tratruoc);
            $optimizes['tratruoc'] = array_values($tratruoc);
            array_multisort($optimize_kyhanvay, SORT_NUMERIC, $laixuat);
            //lấy rule đầu tiên có lãi xuất thấp nhất
            $best_rule = array_shift($optimize_kyhanvay);
            if ($best_rule && count($best_rule['kyhanvay']) > 0) {
                foreach ($best_rule['kyhanvay'] as $month) {
                    $kyhanvay[$month] = $best_rule['laisuat'];
                }
            }
            if ($optimize_kyhanvay) {
                foreach ($optimize_kyhanvay as $key => $value) {
                    if ($value['kyhanvay']) {
                        foreach ($value['kyhanvay'] as $month) {
                            // neu kho ton tai trong $kyhanvay truoc do them vao , nguoc lai remove
                            if (!in_array($month, array_keys($kyhanvay))) {
                                $kyhanvay[$month] = $value['laisuat'];
                            } else {
                                unset($month);
                            }
                        }
                    }
                }
            }
            $default_checked = array();
            ksort($kyhanvay);
            if (count($kyhanvay) > 0) {
                if (in_array(6, array_keys($kyhanvay))) {
                    $best_month = 6;
                    $best_interest_rate = (float)$kyhanvay[6];
                } else {
                    $best_month = max(array_keys($kyhanvay));
                    $best_interest_rate = (float)$kyhanvay[$best_month];
                }
                // lay % tra truoc theo $best_month && $best_interest_rate
                $temp_pay_before = array();
                $best_pay_before = '';
                if ($packages['rule']) {
                    foreach ($packages['rule'] as $rule) {
                        if ($rule['laisuat'] == $best_interest_rate && in_array($best_month, $rule['kyhanvay'])) {
                            foreach ($rule['tratruoc'] as $tratruoc) {
                                $temp_pay_before[$tratruoc] = $tratruoc;
                            }
                        }
                    }
                    if(count($temp_pay_before)>0){
                        $best_pay_before = min($temp_pay_before);
                    }

                }
                if (isset($best_pay_before) && isset($best_month)) {
                    $default_checked['month'] = $best_month;
                    $default_checked['pay_before'] = $best_pay_before;
                    //   $default_checked['best_interest_rate'] = $best_interest_rate;
                }

            }
            // selected % tra truoc đầu tiên của gói tối ưu nhất
            $data_kyhanvay = array();
            if ($kyhanvay) {
                foreach ($kyhanvay as $month => $rate) {
                    $data_kyhanvay[$month] =  (isset ($rate) && $rate == '0') ? 'Trả góp 0%' : '';
                }
            }

            $is_free  = (in_array(0 ,(array_values($kyhanvay))))? 1 : 0  ;

            $optimizes['kyhanvay'] = $data_kyhanvay;
            $optimizes['default_checked'] = $default_checked;
            //end process nvanh
            // lấy thông tin mặc định của các gói
            $optimizes['list_data'] = [];
            if (!empty($optimizes)) {
                $params = array(
                    "percent" => $default_checked['pay_before'],
                    "month" => $default_checked['month'],
                    "product_id" => $input['product_id'],
                    "quantity" => $input['quantity'],
                    "insurance" => 1
                );
                $response = $this->getInfoPackageByPriceMonth($request, $params);
                if (!empty($response)) {
                    $optimizes['list_data'] = $response['data'];
                }
            }
            $optimizes['free_rate'] = (count($optimizes['list_data']) > 0 && $is_free)? 1 :  0 ;
            return (
            array(
                'status' => 200,
                'message' => 'Thành công',
                'datas' => $optimizes
            )
            );
        } else {
            return (
            array(
                'status' => 400,
                'message' => 'Không có gói trả góp nào phù hợp',
            )
            );
        }

    }

    public function getInfoPackageByPriceMonth($request, $params = null)
    {
        $optimizes = array();
        $input = isset($params)? $params : $request->all();
        $tratruoc = array();
        $goi_tra_gop = array();
        $packages = $this->getlistConfig($input['product_id'], $input['quantity']);
        $tra_truoc = 0;
        if (isset($packages['rule']) && count($packages['rule']) > 0) {
            $optimizes['list_data'] = [];
            //if (!empty($optimizes)) {
            $params = array(
                "product_id" => $input['product_id'],
                "quantity" => $input['quantity'],
                "insurance" => 1
            );
            $price = (float)$packages['price'] * $input['quantity'];
            $optimize_kyhanvay = array();
            foreach ($packages['rule'] as $package_id => $package_info) {
                if (count($package_info['tratruoc']) > 0) {
                    foreach ($package_info['tratruoc'] as $value) {
                        if($value == $input['percent']){
                            $tra_truoc = (float)($price * $value) / 100;
                        }
                    }
                }
            }
            $data = [];
            $i = 0;
            foreach ($packages['rule'] as $package_id => $package_info) {
                $i++;
                $check_per[$i] = 0;
                $check_month[$i] = 0;
                if (!empty($package_info['tratruoc'])) {
                    foreach ($package_info['tratruoc'] as $key => $val) {
                        if ($val == intval($input['percent'])){
                            //$tra_truoc = $val['price'];
                            $check_per[$i] = 1;
                        }


                    }
                }
                if (!empty($package_info['kyhanvay'])) {
                    foreach ($package_info['kyhanvay'] as $key1 => $val1) {
                        if ($val1 == intval($input['month']))
                            $check_month[$i] = 1;
                    }
                }
                if ($check_per[$i] == 1 && $check_month[$i] == 1)
                    $data[] = $package_info;
            }

            // lấy lãi suất thấp nhất theo từng ngân hàng
            // lấy lãi suất thấp nhất theo từng ngân hàng
            $temp = [];
            $datas['homecredit'] = $datas['fecredit'] = $datas['hdsaison'] = $datas['acs'] = [];
            $temp_homecredit = 0;
            $arr_bank = ['homecredit','fecredit','hdsaison','acs'];
            $arr_bank1 = ['fecredit'];
            $arr_bank2 = ['hdsaison'];

            if(!empty($data)) {
                foreach($arr_bank as $k => $val){
                    foreach ($data as $key => $value) {
                        $lai_suat = (float)$value['laisuat'];
                        $inst = $value['inst'];
                        if(in_array($inst,$arr_bank)){

                            if(empty($datas[$inst])){
                                array_push($datas[$inst], $value);
                            }else{
                                $d = (array)$datas[$inst][0];
                                $v = (float)$d['laisuat'];
                                if($lai_suat < $v){
                                    $datas[$inst][0] = $value;
                                    continue;
                                }
                            }
                        }
                    }
                }
                $arrr = array_values($datas);
                $temp = [];
                if(!empty($arrr)){
                    foreach($arrr as $key2 => $val2){
                        if(!empty($val2)){
                            $temp[] = $val2[0];
                        }
                    }
                }

            }
            $data_hd = [];
            if(!empty($temp)){
                foreach ($temp as $item){
                    $so_tien_vay_chua_bao_hiem = ($item['price']*$input['quantity']) * (1-($input['percent']/100));
                    // E9 phí bảo hiểm (tháng)
                    if($input['insurance'] == 1) {
                        // E8
                        $so_tien_bao_hiem = $so_tien_vay_chua_bao_hiem * ($item['insurance_rate'] / 100);
                        $phi_bao_hiem = $so_tien_bao_hiem / $input['month'];
                    }else{
                        // E8
                        $so_tien_bao_hiem = 0;
                        $phi_bao_hiem = 0;
                    }
                    // E15 chênh lệch với mua thẳng, chênh lệch so với giá bán

                    // E16 khoản tiền trả trước
                    $khoan_tien_tra_trươc = ($item['price']*$input['quantity'])*($input['percent']/100);
                    // E4 Khoản vay (bao gồm bảo hiểm nếu có)
                    $khoan_vay = (($item['price']*$input['quantity'])*(1-($input['percent']/100))) + $so_tien_bao_hiem;
                    // khoản vay chưa bảo hiểm
                    //$khoan_vay_chua_bao_hiem = (($item['price']*$input['quantity'])*(1-($input['percent']/100)));

                    // start PMT
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api.nguyenkim.com/node/finance/v1/pmt",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "nper: ".$input['month'],
                            "pv: ".$so_tien_vay_chua_bao_hiem,
                            "rate: ".$item['laisuat']/100
                        ),
                    ));
                    $response = curl_exec($curl);
                    $arr = json_decode($response,true);
                    $err = curl_error($curl);
                    curl_close($curl);
                    $org = 0;
                    $irs = 0;
                    // E17 gốc
                    $goc = $so_tien_vay_chua_bao_hiem / $input['month'];
                    $lai = 0;
                    // E18 Lãi
                    $so_tien_thanh_toan_hang_thang = 0;
                    if($arr['status'] == 200 ){
                        $org = isset($arr['Org']) ? $arr['Org'] : 0 ;
                        $irs = isset($arr['Irs']) ? $arr['Irs'] : 0;
                        $so_tien_thanh_toan_hang_thang = $arr['PMT'] + $phi_bao_hiem +$item['phithuho_month'];
                    }
                    if($org == 0)
                        $org = $so_tien_thanh_toan_hang_thang;
                    $lai = $so_tien_thanh_toan_hang_thang - $goc;

                    $chenh_lech_so_voi_gia_ban = $so_tien_bao_hiem + (($item['phithuho_month'] + $irs) * $input['month']) + $item['initial_fee'];

                    // E19 tổng số tiền phải trả cả gốc và lãi
                    if($item['initial_fee']>0){ // phi ho so
                        $tong_tien_phai_tra = (($irs + $phi_bao_hiem + $item['phithuho_month'])* $input['month']) + ($item['price']*$input['quantity']) + $item['initial_fee'] ;
                        $so_tien_tra_truoc = $tra_truoc+$item['initial_fee'];
                    }else{
                        $tong_tien_phai_tra = (($irs + $phi_bao_hiem + $item['phithuho_month'])* $input['month']) + ($item['price']*$input['quantity']);
                        $so_tien_tra_truoc = $tra_truoc;
                    }

                    $datas = [];
                    // end PMT
                    if((float)$item['from_loan'] > 0 && (float)$item['to_loan'] > 0 ){
                        if((float)$item['from_loan'] < $khoan_vay && (float)$item['to_loan'] > $khoan_vay){
                            $datas['ids'] = $item['ids'] ? $item['ids'] : '';
                            $datas['so_tien_thanh_toan_hang_thang'] = round($so_tien_thanh_toan_hang_thang, -3,PHP_ROUND_HALF_UP);
                            $datas['lai_suat_thuc_moi_thang'] = (float)$item['laisuat']; // E11
                            $datas['goc'] = round($goc, -3,PHP_ROUND_HALF_UP);
                            $datas['lai'] = (float)$item['laisuat'];
                            $datas['phi_thu_ho'] = round($item['phithuho_month'], -3,PHP_ROUND_HALF_UP); // E13 Phí đóng tiền hàng tháng
                            $datas['phi_bao_hiem'] = round($phi_bao_hiem, -3,PHP_ROUND_HALF_UP);
                            $datas['tong_tien'] = round($tong_tien_phai_tra, -3,PHP_ROUND_HALF_UP);
                            $datas['chenh_lech_so_voi_gia_ban'] = round($chenh_lech_so_voi_gia_ban, -3,PHP_ROUND_HALF_UP);
                            $datas['giay_to_yeu_cau'] = $item['paper_code'];
                            $datas['inst'] = $item['inst'];
                            $datas['ty_le_bao_hiem'] = $item['insurance_rate'];
                            $datas['so_tien_vay_chua_bao_hiem'] = round($so_tien_vay_chua_bao_hiem, -3,PHP_ROUND_HALF_UP);
                            $datas['so_tien_bao_hiem'] = round($so_tien_bao_hiem, -3,PHP_ROUND_HALF_UP);
                            $datas['khoan_vay'] = round($khoan_vay, -3,PHP_ROUND_HALF_UP);
                            $datas['approval'] = $item['approval'];
                            $datas['Org'] = $org;
                            $datas['Irs'] = round($irs, -3,PHP_ROUND_HALF_UP);
                            $datas['notes'] ='';
                            if($item['approval'] == 'Off'){
                                $datas['notes'] ='Chỉ áp dụng mua hàng trực tiếp tại TTMS Nguyễn Kim';
                            }
                            $datas['initial_fee'] = round($item['initial_fee'], -3,PHP_ROUND_HALF_UP);
                            $datas['price'] = round($item['price'], -3,PHP_ROUND_HALF_UP);
                            $datas['giaban'] = round($item['giaban'], -3,PHP_ROUND_HALF_UP);
                            $datas['goc_va_lai'] = round($goc+$irs, -3,PHP_ROUND_HALF_UP);
                            $datas['tra_truoc'] = round($so_tien_tra_truoc, -3,PHP_ROUND_HALF_UP);
                            $datas['initial_fee'] = $item['initial_fee'];
                            $datas['percent'] = $input['percent'];
                        }

                    }else{
                        $datas['ids'] = $item['ids'] ? $item['ids'] : '';
                        $datas['so_tien_thanh_toan_hang_thang'] = round($so_tien_thanh_toan_hang_thang, -3,PHP_ROUND_HALF_UP);
                        $datas['lai_suat_thuc_moi_thang'] = round($item['laisuat'], -3,PHP_ROUND_HALF_UP); // E11
                        $datas['goc'] = round($goc, -3,PHP_ROUND_HALF_UP);
                        $datas['lai'] = (float)$item['laisuat'];
                        $datas['phi_thu_ho'] = round($item['phithuho_month'], -3,PHP_ROUND_HALF_UP); // E13 Phí đóng tiền hàng tháng
                        $datas['phi_bao_hiem'] = round($phi_bao_hiem, -3,PHP_ROUND_HALF_UP);
                        $datas['tong_tien'] = round($tong_tien_phai_tra, -3,PHP_ROUND_HALF_UP);
                        $datas['chenh_lech_so_voi_gia_ban'] = round($chenh_lech_so_voi_gia_ban, -3,PHP_ROUND_HALF_UP);
                        $datas['giay_to_yeu_cau'] = $item['paper_code'];
                        $datas['inst'] = $item['inst'];
                        $datas['ty_le_bao_hiem'] = $item['insurance_rate'];
                        $datas['so_tien_vay_chua_bao_hiem'] = round($so_tien_vay_chua_bao_hiem, -3,PHP_ROUND_HALF_UP);
                        $datas['so_tien_bao_hiem'] = round($so_tien_bao_hiem, -3,PHP_ROUND_HALF_UP);
                        $datas['khoan_vay'] = round($khoan_vay, -3,PHP_ROUND_HALF_UP);
                        $datas['approval'] = $item['approval'];
                        $datas['Org'] = $org;
                        $datas['Irs'] = round($irs, -3,PHP_ROUND_HALF_UP);
                        $datas['notes'] ='';
                        if($item['approval'] == 'Off'){
                            $datas['notes'] ='Chỉ áp dụng mua hàng trực tiếp tại TTMS Nguyễn Kim';
                        }
                        $datas['initial_fee'] = round($item['initial_fee'], -3,PHP_ROUND_HALF_UP);
                        $datas['price'] = round($item['price'], -3,PHP_ROUND_HALF_UP);
                        $datas['giaban'] = round($item['giaban'], -3,PHP_ROUND_HALF_UP);
                        $datas['goc_va_lai'] = round($goc+$irs, -3,PHP_ROUND_HALF_UP);
                        $datas['tra_truoc'] = round($so_tien_tra_truoc, -3,PHP_ROUND_HALF_UP);
                        $datas['initial_fee'] = $item['initial_fee'];
                        $datas['percent'] = $input['percent'];
                    }

                    $data_hd[$item['inst']][] = $datas;
                }
            }
            if(!empty($data_hd['homecredit'])){
                foreach ($data_hd['homecredit'] as $key => $value) {
                    if (empty($value)) {
                        unset($data_hd['homecredit'][$key]);
                    }
                }
            }
            if(!empty($data_hd['fecredit'])){
                foreach ($data_hd['fecredit'] as $key => $value) {
                    if (empty($value)) {
                        unset($data_hd['fecredit'][$key]);
                    }
                }
            }
            if(!empty($data_hd['hdsaison'])){
                foreach ($data_hd['hdsaison'] as $key => $value) {
                    if (empty($value)) {
                        unset($data_hd['hdsaison'][$key]);
                    }
                }
            }
            if(!empty($data_hd['acs'])){
                foreach ($data_hd['acs'] as $key => $value) {
                    if (empty($value)) {
                        unset($data_hd['acs'][$key]);
                    }
                }
            }

            return (
            array(
                'status' => 200,
                'data' => $data_hd
            )
            );

        } else {
            return (
            array(
                'status' => 400,
                'message' => 'Không có gói trả góp nào phù hợp',
            )
            );
        }
    }
}
