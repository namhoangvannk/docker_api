<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 5/8/2019
 * Time: 3:44 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class InstallmentCreditRepository extends RepositoryBase
{
    public function cardType($request){
        $valid_card =  0 ;
        if(!empty($request->input('card_number'))) {
            $datas = DB::table('cscart_nk_creditcard_type')->where([
                ['status', '=', '1'],
                ['start_time', '<=', date('Y-m-d H:i:s')],
                ['end_time', '>=', date('Y-m-d H:i:s')],
            ])->get(['id_credit', 'name', 'card', 'logo','card_start','card_end']);
          if($datas){
              foreach ($datas as $data){
                  $card_number = $request->input('card_number') ;
                  $cards = explode(',' , $data->card);
                  $card_start = (int)$data->card_start;
                  $card_end = (int)$data->card_end;
                  if(is_array($cards) && in_array($card_number,$cards) || (!empty($card_start) && !empty($card_end) && $card_start <= (int)$card_number &&  (int)$card_number <= $card_end)){
                      return [
                          'status' => 200,
                          'message' => 'Success',
                          'logo' => $data->logo
                      ];
                  }
              }
              if(!$valid_card){
                  return [
                      'status' => 400,
                      'message' => 'Định dạng thẻ không đúng',
                  ];
              }
          }else{
              return [
                  'status' => 400,
                  'datas' => [],
              ];
          }
        }else{
            return [
                'status' => 400,
                'datas' => [],
            ];
        }
    }

    public function validateCard($request, $bank_id)
    {
        $output = array();
        $valid_card = 0;
        if (!empty($request->input('card_number'))) {
            $data = DB::table('cscart_nk_creditcard_bank')->where('id_bank', (int)$bank_id)->first(['card']);
            if (!empty($data->card)) {
                $cards = explode(',', $data->card);
                foreach ($cards as $card) {
                    if (substr($request->input('card_number'), 0, strlen($card)) == $card) {
                        $valid_card = 1;
                        break;
                    }
                }
                if ($valid_card) {
                    return [
                        'status' => 200,
                        'message' => 'Success'
                    ];
                } else {
                    $output['valid_card'] = $data->card;
                    return [
                        'status' => 400,
                        'message' => 'Số thẻ không hợp lệ',
                        'datas' => $output
                    ];
                }
            } else {
                return [
                    'status' => 400,
                    'datas' => [],
                ];
            }

        } else {
            return [
                'status' => 400,
                'datas' => [],
            ];
        }

    }

    public function packageInstallment($bank_id, $total, $price)
    {
        $packages = array();
        $sort_terms = array();
        $datas = DB::table('cscart_nk_creditcard_description')->where('id_bank', (int)$bank_id)
            ->where([
                ['status', '=', '1'],
                ['start_time', '<=', date('Y-m-d H:i:s')],
                ['end_time', '>=', date('Y-m-d H:i:s')],
            ])
            ->get(['*']);
        if ($datas) {
            foreach ($datas as $data) {
                if (!empty($data->conversion_amount)) {
                    $fee = (empty($data->conversion_amount)) ? $data->conversion_amount : round((float)$data->conversion_amount, -3);
                } else {
                    $fee = (float)$data->conversion_percent / 100 * $total;
                    //$amount = DB::table('cscart_nk_creditcard_description')->where('id', (int)$term_id)->select('amount_max', 'amount_min')->first();
                    if ((float)$data->amount_max > 0 && (float)$data->amount_min > 0) {
                        if ($fee < (float)$data->amount_min) {
                            $fee = (float)$data->amount_min;
                        } elseif ($fee < (float)$data->amount_max) {
                            $fee = (float)$data->amount_max;
                        }
                    } elseif ((float)($data->amount_max) > 0 && (float)($data->amount_min) == 0) {
                        if ($fee > (float)$data->amount_max) {
                            $fee = (float)$data->amount_max;
                        }
                    } else {
                        if ($fee < (float)$data->amount_min) {
                            $fee = (float)$data->amount_min;
                        }
                    }
                }
                if (strlen(trim($data->term)) > 1) {
                    $term = explode(',', trim($data->term));
                    if ($term) {
                        foreach ($term as $value) {
                            $sort_terms[] = $value;
                            $packages[] = array(
                                'term_id' => $data->id,
                                'term' => $value,
                                'value' => round((float)$total / $value, -3),
                                'money' => number_format(round((float)$total / $value, -3), 0, '.', '.') . 'đ / tháng',
                                'fee' => $fee,
                                'price' =>$price,
                                'total' => round(($value * ($total / $value)) + $fee, -3)
                            );
                        }
                    }
                } else {
                    $sort_terms[] = $data->term;
                    $packages[] = array(
                        'term_id' => $data->id,
                        'term' => $data->term,
                        'value' => round((float)$total / ($data->term), -3),
                        'money' => number_format(round((float)$total / ($data->term), -3), 0, '.', '.') . 'đ / tháng',
                        'fee' => $fee,
                        'price' =>$price,
                        'total' => round(($data->term * ($total / $data->term)) + $fee, -3)
                    );
                }
            }
        }
        array_multisort($sort_terms, SORT_ASC, $packages);
        return $packages;
    }

    public function Conversion($term_id, $total)
    {
        //
        $fee = '';
        $data = DB::table('cscart_nk_creditcard_description')->where('id', (int)$term_id)->select('conversion_amount', 'conversion_percent')->first();
        if (!empty($data->conversion_amount)) {
            $value = (empty($data->conversion_amount)) ? $data->conversion_amount : round((float)$data->conversion_amount, -3);
            $label = number_format(round($value, -3), 0, '.', '.') . 'đ';
            return array('label' => $label , 'value' => $value) ;
        } else {
            $fee = (float)$data->conversion_percent / 100 * $total;
            $amount = DB::table('cscart_nk_creditcard_description')->where('id', (int)$term_id)->select('amount_max', 'amount_min')->first();
            if ((float)$amount->amount_max > 0 && (float)$amount->amount_min > 0) {
                if ($fee < (float)$amount->amount_min) {
                    $fee = (float)$amount->amount_min;
                } elseif ($fee < (float)$amount->amount_max) {
                    $fee = (float)$amount->amount_max;
                }
            } elseif ((float)($amount->amount_max) > 0 && (float)($amount->amount_min) == 0) {
                if ($fee > (float)$amount->amount_max) {
                    $fee = (float)$amount->amount_max;
                }
            } else {
                if ($fee < (float)$amount->amount_min) {
                    $fee = (float)$amount->amount_min;
                }
            }
        }
        $value =  (empty($fee)) ? $fee : round($fee, -3) ;
        $label = number_format(round($value, -3), 0, '.', '.') . 'đ';
        return array('label' => $label , 'value' => $value) ;
    }

    public function conversionRate($request)
    {
        $convertion_rate = $this->Conversion($_GET['term_id'], (int)$_GET['total']);
        return [
            'status' => 200,
            'message' => 'Success',
            'datas' => $convertion_rate
        ];
    }

    public function detailsBank($request, $bank_id)
    {
        $outputs = array();
        $data = DB::table('cscart_nk_creditcard_bank')->where('id_bank', (int)$bank_id)->first();
        if (empty($data->id_bank)) {
            return [
                'status' => 400,
                'datas' => $outputs
            ];
        } else {
            // lấy giá trả góp của sàn phẩm với product_id
            $product_id = $request->product_id;
            $data_installment_price_type = DB::table('cscart_products')->where('product_id', $product_id)->get(['installment_price_type'])->first();
            $installment_price_type = $data_installment_price_type->installment_price_type;
            if ($installment_price_type == 'B') {
                $d_price = DB::table('cscart_product_prices')->where('product_id', $product_id)->get(['price'])->first();
                $price = (float)$d_price->price;
            } elseif ($installment_price_type == 'Y') {
                $d_price = DB::table('cscart_products')->where('product_id', $product_id)->get(['list_price'])->first();
                $price = (float)$d_price->list_price;
            } else {
                $d_price = DB::table('cscart_products')->where('product_id', $product_id)->get(['installment_price'])->first();
                $price = (float)$d_price->installment_price;
            }
            $outputs['box1'] = '<span style="color:red">*</span> Chương trình áp dụng trả góp 0% ngân hàng <span style="color:red">' . strtoupper($data->bank) . '</span> với các đầu thẻ sau:
            <span style="color:red">' . strtoupper(trim($data->card)) . '</span>';
            $outputs['box2'] = 'Trả góp 0% với thẻ tín dụng <span style="color:red">' .  ucfirst(strtolower($data->bank)) . '</span>';
            $outputs['box3'] = 'Nhân viên <span style="color:red">'. ucfirst(strtolower($data->bank)).'</span> sẽ liên hệ với khách hàng để xác nhận thông tin chuyển đổi trả góp';
            $outputs['box4'] = '<span style="color:white">Thông tin thẻ tín dụng</span><span style="color:white">' .  ucfirst(strtolower($data->bank)) . '</span>';
            $outputs['id_bank'] = $data->id_bank;
            $outputs['bank'] = $data->bank;
            $outputs['logo'] = !empty($data->logo)? $data->logo: '';
            $outputs['bank_name'] = $data->bank_name;
            $outputs['card'] = trim($data->card);
            $outputs['description'] = $data->description;
            $outputs['id_bank'] = $data->id_bank;
            if (!empty($_GET['total'])) {
                $packages = $this->packageInstallment($bank_id, (int)$_GET['total'],(int)$price);
                if ($packages) {
                    $outputs['packages'] = $packages;
                    $outputs['selected_package'] = array_shift($packages);
                    //$outputs['conversion_fee'] = $this->Conversion(4,(int) $_GET['total']) ;
                    $outputs['conversion_fee'] = $this->Conversion($outputs['selected_package']['term_id'], (int)$_GET['total']);
                }
            }
            return [
                'status' => 200,
                'message' => 'Success',
                'datas' => $outputs
            ];
        }
    }

    public function listBank($request)
    {
        $banks = array();
        if (isset($_GET['total'])) {
            $datas = DB::table('cscart_nk_creditcard_bank')->where([
                ['status', '=', '1'],
                ['start_time', '<=', date('Y-m-d H:i:s')],
                ['end_time', '>=', date('Y-m-d H:i:s')],
                ['min_price', '<=', (int)$_GET['total']],
            ])
                ->where(function ($query) {
                    $query->where('max_price', '>=', (int)$_GET['total'])
                        ->orWhere('max_price', '=', NULL);
                })->get(['id_bank', 'bank', 'bank_name', 'card', 'description', 'logo']);
            if ($datas) {
                foreach ($datas as $data) {
                    $bank = trim($data->bank);
                    $banks[] = array(
                        'id_bank' => $data->id_bank,
                        'value' => strtoupper(str_replace(' ', '', $bank)),
                        'label' => $data->bank_name,
                        'card' => $data->card,
                        'description' => $data->description,
                        'image' => $data->logo
                    );
                }
            }
            return [
                'status' => 200,
                'message' => 'Success',
                'datas' => $banks
            ];
        } else {
            return [
                'status' => 400,
                'datas' => $banks
            ];
        }
    }
}