<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/20/2018
 * Time: 3:13 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Cache\RedisClient;
use App\Models\ProductFeaturesValues as Features;
use DB;
use Log;
use GuzzleHttp\Client;

class ProductRepository extends RepositoryBase
{
    protected $model;
    protected $helper;
    protected $config;

    public function __construct(Helpers $helper)
    {
        $this->helper = $helper;
        $this->model = new Products();
    }
    public function all($request)
    {
        $limit = $request->input('limit', 1000);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $start = ($offset - 1) * $limit;
        $data = DB::connection('mongodb')->collection('products')->project(['categories' => ['$slice' => 421]])->skip($start)->take($limit)
            ->get([
                'product_id'
                ,'description'
                ,'product_code'
                ,'amount'
                ,'weight'
                ,'length'
                ,'width'
                ,'height'
                ,'list_price'
                ,'price'
                ,'product_name'
                ,'display_name'
                ,'model'
                ,'brand'
                ,'images'
                ,'categories'
            ]);
        $data_premium = [];
        $total = count($data);
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => [
                'premium ' =>$data_premium,
                'brand' =>[],
                'header' =>[
                    'id' => 423,
                    'alias' =>'may-giat',
                    'name' => 'Tủ lạnh',
                    'description' => 'Danh sách Máy Giặt chính hãng giá tốt'
                ],
                'filter' => [],
                'sort_by' => [
                    '1' => 'Bán chạy nhất',
                    '2' => 'Giá thấp đến cao',
                    '3' => 'Giá cao xuống thấp'
                ],
                'banner' =>[],
                'product' => $data,
                'page_info' =>[
                    'total'=> $total,
                    'page_curent' => 1
                ]
            ],
        );
    }
    public function show($request,$id,$user_id)
    {
        $product_id = (int)$id;
        $params = $request->all();

        try {
            $datas =  DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', $product_id)->limit(1)
                ->get([
                    'product_id'
                    ,'description'
                    ,'product_code'
                    ,'amount'
                    ,'weight'
                    ,'length'
                    ,'width'
                    ,'height'
                    ,'list_price'
                    ,'price'
                    ,'nk_shortname'
                    ,'product'
                    ,'display_name'
                    ,'brand'
                    ,'variants'
                    ,'product_options'
                    ,'product_descriptions'
                    ,'tag_image'
                    ,'mobile_short_description'
                    ,'mobile_full_description'
                    ,'main_pair'
                    ,'image_pairs'
                    ,'product_features'
                    ,'url_digital_images'
                    ,'nk_is_shock_price'
                    ,'nk_tragop_0'
                    ,'tag_image'
                    ,'status'
                    ,'hidden_option'
                    ,'text_shock'
                    ,'shock_online_exp'
                    ,'nk_is_shock_price'
                    ,'nk_product_tags'
                    ,'nk_special_tag'
                    ,'offline_price'
                    ,'model'
                    ,'barcode'
                    ,'seo_name'
                    ,'meta_description'
                    ,'page_title'
                    ,'category_ids'
                    ,'nk_is_add_cart'
                    ,'nk_check_total_amount'
                    ,'nk_is_tra_gop'
                    ,'nk_show_two_price'
                    ,'nk_discount_label'
                    ,'status'
                    ,'tag_image_special'
                    ,'tag_start_time_special'
                    ,'tag_end_time_special'
                    ,'short_description'
                ]);
            $total = count($datas);
            // thông tin sản phẩm
            $cf_is_one_click = 0;
            if ('1' != $datas[0]['nk_is_add_cart']) {
                $cf_is_one_click = 0;
            }
            if ($datas[0]['status'] != "D" && ($datas[0]['status'] != "H" || $datas[0]['hidden_option'] == "G" || $datas[0]['hidden_option'] == "N" || empty($datas[0]['hidden_option']))) {
                if ($datas[0]['amount'] > $datas[0]['nk_check_total_amount']) {
                    $cf_is_one_click = 1;
                }
            }
            $cf_is_tra_gop = 0;
            if ('1' != $datas[0]['nk_is_tra_gop'] || ( '0' == $datas[0]['nk_is_add_cart'] ) ) {
                $cf_is_tra_gop = 0;
            }
            if (!isset($datas[0]['price']) || $datas[0]['price'] < 2000000 || $datas[0]['price'] > 60000000) {
                $cf_is_tra_gop = 0;
            }
            if ($datas[0]['status'] != "D" && ($datas[0]['status'] != "H" || $datas[0]['hidden_option'] == "G" || $datas[0]['hidden_option'] == "N" || empty($datas[0]['hidden_option']))) {
                if ($datas[0]['amount'] > $datas[0]['nk_check_total_amount']) {
                    $cf_is_tra_gop = 1;
                }
            }
            // status_button = 3 hàng sắp về , status_button = 2 ngừng kinh doanh, status_button = 1 tạm hết hàng
            $status_button = '';

            if(($datas[0]['status'] == 'H' || $datas[0]['status'] == 'D' ) && $datas[0]['hidden_option'] == "D"){
                $discontinued_for_sale = 1;
            }else{
                $discontinued_for_sale = 0;
            }
            if($datas[0]['hidden_option'] == 'P' && $discontinued_for_sale == 0){
                $price_confirm = 1;
            }else{
                $price_confirm = 0;
            }

            if($datas[0]['category_ids'] && in_array(2042, $datas[0]['category_ids'])){
                $status_button = 3;
            }
            if($discontinued_for_sale == 1){
                $status_button = 2;
            }//else{
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://test.nguyenkimonline.com/rest-api/stock/check/".$datas[0]['product_id'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                    ),
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);
                if(!$err) {
                    $data_stock = json_decode($response,true);
                    if(!empty($data_stock['result'])){
                        if($data_stock['result']['TAM_HET_HANG'] == 1 && $data_stock['result']['CHO_VAO_GIO'] == 0){
                            $status_button = 1;
                        }
                    }
                }
            //}
            $datas1['status'] = $datas[0]['status'];
            $datas1['hidden_option'] = $datas[0]['hidden_option'];
            $datas1['nk_show_two_price'] = $datas[0]['nk_show_two_price'];
            $datas1['nk_discount_label'] = $datas[0]['nk_discount_label'];

            $datas1['discount_percent'] = 0;
            if ( ($datas[0]['nk_is_shock_price'] == 'O' && $datas[0]['nk_discount_label'] == 'Y') || ($datas[0]['nk_show_two_price'] == 'Y' && $datas[0]['nk_discount_label'] == 'Y')){
                if((int)$datas[0]['list_price'] > 0){
                    if((int)$datas[0]['list_price'] > 0){
                        $datas1['discount_percent'] = round((($datas[0]['list_price'] - $datas[0]['price'])/($datas[0]['list_price'])*100));
                    }
                }
            }

            $datas1['status_button'] = $status_button;
            $datas1['product_id'] = $datas[0]['product_id'];
            $datas1['product_code'] = $datas[0]['product_code'];
            $datas1['barcode'] = $datas[0]['barcode'];
            $datas1['amount'] = $datas[0]['amount'];
            $datas1['list_price'] = (int)$datas[0]['list_price'];
            $datas1['price'] = (int)$datas[0]['price'];
            $datas1['offline_price'] = (int)$datas[0]['offline_price'];
            $datas1['nk_shortname'] = $datas[0]['nk_shortname'];
            $datas1['product_name'] = $datas[0]['product'];
            $datas1['category_ids'] = $datas[0]['category_ids'];

            if(!empty($datas[0]['short_description'])){
                $short_des = explode("</p>",$datas[0]['short_description']);
                $data2 =[];
                foreach($short_des as $key2 => $value2){
                    $str = replace_content($value2);
                    $data2[] = iconv(mb_detect_encoding($str, mb_detect_order(), true), "UTF-8", $str);
                }
                $data2 = array_filter($data2);
                $i=0;
                $data=[];
                foreach($data2 as $key => $value){
                    $str1= strip_tags($value);
                    $str1 = iconv(mb_detect_encoding($str1, mb_detect_order(), true), "UTF-8", $str1);
                    $data1 = ['id' => $i,'value' => replace_content($str1) ];
                    $i++;
                    $data[] = $data1;
                }
                $datas1['mobile_short_description'] = $data;
            }else{
                $datas1['mobile_short_description'] = [];
            }
            $full ='<span class="m">'.$datas[0]['mobile_full_description'].'</span>';
            //$full = str_replace('data-src','src',$full);
            $datas1['mobile_full_description'] = replace_data_src($full);
            if($datas1['mobile_full_description'] == '<span class="m"><!--elm_product_full_descr--></span>'
                || $datas1['mobile_full_description'] == '<span class="m"> <!--elm_product_full_descr--> <!--elm_product_full_descr--></span>')
                $datas1['mobile_full_description'] = '';
            $datas1['image_thumb']= [];
            $datas1['image_detailed']= [];
            $datas1['brand'] = '';
            $datas1['tag_image_top_left'] = '';
            $datas1['tag_image_top_right'] = '';
            $datas1['tag_image_bottom_left'] = '';
            $datas1['tag_image_bottom_right'] = '';
            $datas1['url_digital_images'] = $datas[0]['url_digital_images'];
            $datas1['nk_is_shock_price'] = $datas[0]['nk_is_shock_price'];
            $datas1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
            $datas1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
            $datas1['nk_tragop_0'] = $datas[0]['nk_tragop_0'];
            $datas1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
            $datas1['discontinued_for_sale'] = $discontinued_for_sale; // 1: ngừng kinh doanh
            $datas1['text_shock'] = $datas[0]['text_shock'];
            $datas1['shock_online_exp'] = $datas[0]['shock_online_exp'];
            $datas1['nk_special_tag'] = $datas[0]['nk_special_tag'];
            // fix hiện giá rẻ online khỏi sửa bên app
            if(isset($datas[0]['text_shock']) && !empty($datas[0]['text_shock'])){
                $datas1['nk_special_tag'] = 1;
            }
            $datas1['nk_product_tags'] = $datas[0]['nk_product_tags'];
            $datas1['model'] = '';
            $product_features = $datas[0]['product_features'];
            if(!empty($datas[0]['nk_product_tags'])){
                foreach ($datas[0]['nk_product_tags'] as $key => $val){
                    if($val['tag_position'] == 'bottom-right'){
                        $datas1['tag_image_bottom_right'] = $val['tag_image'];
                    }elseif($val['tag_position'] == 'bottom-left'){
                        $datas1['tag_image_bottom_left'] = $val['tag_image'];
                    }elseif($val['tag_position'] == 'top-right'){
                        $datas1['tag_image_top_right'] = $val['tag_image'];
                    }elseif($val['tag_position'] == 'top-left'){
                        $datas1['tag_image_top_left'] = $val['tag_image'];
                    }
                }
            }
            //$thumb = $datas[0]['main_pair'];
            //$image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
            $thumb = $datas[0]['main_pair'];
            if(!empty($thumb['icon'])) {
                $image = $thumb['icon'];
            }else{
                if(!empty($thumb['detailed'])){
                    $image = $thumb['detailed'];
                }
            }
            if(trim($datas[0]['tag_image_special']) != ''){
                $time = time();
                if($time > $datas[0]['tag_start_time_special'] && $time < $datas[0]['tag_end_time_special']){
                    $datas1['image_thumb'][] = $datas[0]['tag_image_special'];
                }
            }
            else if(!empty($image)){
                if($product_id == 60667 || $product_id == 60693 || $product_id == 60695){ // mobi - vina - viettel
                    $datas1['image_thumb'][] = 'https://www.nguyenkim.com/images/thumbnails/75/92/'. $image['relative_path'];
                }else{
                    $datas1['image_thumb'][] = 'https://www.nguyenkim.com/images/thumbnails/290/235/'. $image['relative_path'];
                }
                $datas1['image_detailed'][] = 'https://www.nguyenkim.com/images/'.$image['relative_path'];
            }

            if(!empty($datas[0]['image_pairs'])){
                foreach ($datas[0]['image_pairs'] as $value){
                    if(isset($value['icon'])){
                        $datas1['image_thumb'][] = 'https://www.nguyenkim.com/images/thumbnails/290/235/'.$value['icon']['relative_path'];
                    }else{
                        if(!empty($value['detailed'])){
                            $datas1['image_thumb'][] = 'https://www.nguyenkim.com/images/thumbnails/290/235/'.$value['detailed']['relative_path'];
                        }else{
                            $datas1['image_thumb'][] = '';
                        }
                    }
                    if(!empty($value['detailed'])){
                        $datas1['image_detailed'][] = 'https://www.nguyenkim.com/images/'.$value['detailed']['relative_path'];
                    }else{
                        $datas1['image_detailed'][] = '';
                    }
                }
            }
            if(!empty($datas[0]['product_features'])){
                if(!empty($datas[0]['product_features']['591'])){
                    if(!empty($datas[0]['product_features']['591']['subfeatures'])){
                        if(!empty($datas[0]['product_features']['591']['subfeatures']['599'])){
                            $data_variant = $datas[0]['product_features']['591']['subfeatures']['599'];
                            if(!empty($data_variant['variants'])){
                                foreach ($data_variant['variants'] as $item){
                                    $datas1['brand'] = $item['variant'];
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if(!empty($datas[0]['product_features'])){
                if(!empty($datas[0]['product_features']['591'])){
                    if(!empty($datas[0]['product_features']['591']['subfeatures'])){
                        if(!empty($datas[0]['product_features']['591']['subfeatures']['596'])){
                            $data_variant = $datas[0]['product_features']['591']['subfeatures']['596'];
                            if(!empty($data_variant['variants'])){
                                foreach ($data_variant['variants'] as $item){
                                    $datas1['model'] = $item['variant'];
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            $datas1['promotion'] =[];
            $datas1['promotion_option'] =[];
            if(!empty($datas[0]['product_options'])){
                $datas_ops=[];
                foreach($datas[0]['product_options'] as $key => $value){
                    $datas_op['option_id']= $value['option_id'];
                    $datas_op['product_id']= $value['product_id'];
                    $datas_op['option_type']= $value['option_type'];
                    $datas_op['option_name']= $value['option_name'];
                    $datas_op['status']= $value['status'];
                    $datas_op['missing_variants_handling']= $value['missing_variants_handling'];
                    $datas_op['nk_option_type']= $value['nk_option_type'];
                    $datas_op['nk_option_image']= $value['nk_option_image'];
                    $datas_op['nk_option_show']= $value['nk_option_show'];
                    $datas_op['description']= $value['description'];
                    $datas_op['variants'] = [];
                    $data_v=[];
                    if(!empty($value['variants'])){
                        foreach ($value['variants'] as $key2 => $value2){
                            $data_v['variant_id'] = $value2['variant_id'];
                            $data_v['option_id'] = $value2['option_id'];
                            $data_v['variant_name'] = $value2['variant_name'];
                            $data_v['is_show_banner'] = $value2['is_show_banner'];
                            $data_v['image_pair'] = $value2['image_pair'];
                            $data_v['image_desktop_pair'] = $value2['image_desktop_pair'];
                            $data_v['image_mobile_pair'] = $value2['image_mobile_pair'];
                            $datas_op['variants'][]=$data_v;
                        }
                    }
                    $datas_ops[] = $datas_op;
                }
            }
            $datas1['promotion_option_new'] = [];
            if(!empty($datas[0]['product_options'])){
                foreach ($datas[0]['product_options'] as $key => $value){
                    if($value['nk_option_type'] == 'P' || $value['nk_option_type'] == 'N'){
                        if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                            if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                if ($value['option_type'] == "R"){
                                    if (isset($value['variants']) && !empty($value['variants']) && count($value['variants']) == 0 ){
                                        $pro_des = isset($value['description']) ? $value['description'] : '' ;
                                        $datas3['url']='';
                                        $datas3['show']= 0;
                                        if(strstr($pro_des,"Xem chi tiết")){
                                            $pro_des = str_replace('Xem chi tiết','',$pro_des);
                                            if(strlen(trim($pro_des)) > 0){
                                                $datas3['url'] = get_value_href($pro_des);
                                            }else{
                                                $datas3['url'] = '';
                                            }
                                            $datas3['show']= 1;
                                        }
                                        $datas3['des'] = replace_content($pro_des);
                                        $datas3['option_type'] = $value['option_type'];
                                        $datas1['promotion'][] = $datas3;
                                    }else{
                                        if(!empty($value['variants'])){
                                            $datas_ops_new[$key]['des'] = replace_content($value['description']);
                                            foreach ($value['variants'] as $key1 => $value1){
                                                $datas_ops1['variant_id'] = $value1['variant_id'];
                                                $datas_ops1['option_id'] = $value1['option_id'];
                                                $datas_ops1['variant_name'] = $value1['variant_name'];
                                                $datas_ops_new[$key]['data'][] = $datas_ops1;
                                            }
                                            $datas1['promotion_option'] = array_values($datas_ops_new);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // start option moi
                $k=0;
                foreach ($datas[0]['product_options'] as $key => $value) {
                    $data_option_new[$key]['show'] = 0;
                    if(isset($value['description']) && $value['description'] !='' ){
                        $pro_des_option_new = $value['description'];
                    }else{
                        $pro_des_option_new = isset($value['option_name']) ? $value['option_name'] : '' ;
                    }

                    if(strstr($pro_des_option_new,"Xem chi tiết")){
                        $pro_des_option_new = str_replace('Xem chi tiết','',$pro_des_option_new);
                        if(strlen(trim($pro_des_option_new)) > 0){
                            $data_option_new[$key]['url'] = get_value_href($pro_des_option_new);
                            $data_option_new[$key]['show'] = 1;
                        }else{
                            $data_option_new[$key]['url'] = '';
                        }
                    }
                    $data_option_new[$key]['des'] = replace_content($pro_des_option_new);
                    $data_option_new[$key]['option_id'] = $value['option_id'];
                    $data_option_new[$key]['font_icon_app'] = isset($value['font_icon_app']) ? str_replace('nki-','',$value['font_icon_app']) : '';
                    //$data_option_new[$key]['des'] = replace_content($value['description']);
                    $data_option_new[$key]['option_type'] = $value['option_type'];
                    $data_option_new[$key]['type'] = 1;
                    //$data_option_new[$key]['url'] = '';
                    $data_option_new[$key]['promotion_option'] = [];
                    if($value['nk_option_type'] == 'P' || $value['nk_option_type'] == 'N'){
                        if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                            if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                if ($value['option_type'] == "R"){
                                    if (isset($value['variants']) && !empty($value['variants']) && count($value['variants']) == 0 ){
                                        $data_option_new[$key]['type']= 1;
                                        $pro_des_option_new = isset($value['description']) ? $value['description'] : '' ;
                                        $data_option_new[$key]['url']='';
                                        $data_option_new[$key]['show']= 0;
                                        if(strstr($pro_des_option_new,"Xem chi tiết")){
                                            $pro_des_option_new = str_replace('Xem chi tiết','',$pro_des_option_new);
                                            if(strlen(trim($pro_des_option_new)) > 0){
                                                $data_option_new[$key]['url'] = get_value_href($pro_des_option_new);
                                            }else{
                                                $data_option_new[$key]['url'] = '';
                                            }
                                            $data_option_new[$key]['show']= 1;
                                        }
                                        $data_option_new[$key]['des'] = replace_content($pro_des_option_new);
                                        $data_option_new[$key]['option_type'] = $value['option_type'];
                                    }else{
                                        $k++;
                                        $data_option_new[$key]['type']= 2;
                                        if(!empty($value['variants'])){
                                            $data_option_new[$key]['promotion_option']['row'] = 'row_'.$k;
                                            $data_option_new[$key]['promotion_option']['des'] = replace_content($value['description']);
                                            foreach ($value['variants'] as $key1 => $value1){
                                                $datas_ops1_option_new['row'] = 'row_'.$k;
                                                $datas_ops1_option_new['variant_id'] = $value1['variant_id'];
                                                $datas_ops1_option_new['option_id'] = $value1['option_id'];
                                                $datas_ops1_option_new['variant_name'] = $value1['variant_name'];
                                                $data_option_new[$key]['promotion_option']['data'][] = $datas_ops1_option_new;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $datas1['promotion_option_new'] = array_values($data_option_new);
                // end
            }

            $datas1['promotion_img'] = [];
            $datas33 = [];
            if(!empty($datas[0]['product_options'])){
                foreach ($datas[0]['product_options'] as $key => $value){
                    if($value['nk_option_type'] == 'P'){
                        if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                            if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                if ($value['option_type'] == "R"){
                                    if(!empty($value['variants']) && count($value['variants']) > 0){
                                        foreach ($value['variants'] as $key1 => $value1){
                                            if($value1['is_show_banner'] != 'Y'){
                                                if($value['nk_option_show'] == "A"){
                                                    if(
                                                        $value['nk_option_image'] != null || $value['nk_option_image'] != ""
                                                        || !empty($value1['image_desktop_pair']) ){
                                                        if(isset($value1['image_desktop_pair']['icon']) && !empty($value1['image_desktop_pair']['icon'])){

                                                            $image = $value1['image_desktop_pair']['icon']['image_path'];
                                                            $datas33['image'] = $image;
                                                            $datas33['name'] = $value1['image_desktop_pair']['icon']['alt'];
                                                            $datas1['promotion_img'][] =$datas33;
                                                        }
                                                        // else{
                                                        //     $datas33['image'] = $value['nk_option_image'];
                                                        //     $datas33['name'] = "";
                                                        // }
                                                    }
                                                    // else{
                                                    //     $image = $value['nk_option_image'];
                                                    //     $datas33['image'] = $image;
                                                    //     $datas33['name'] = "";
                                                    // }
                                                }
                                            }
                                        }
                                    }
                                    // else{
                                    //     foreach ($value['variants'] as $vr){
                                    //         if($vr['is_show_banner'] != 'Y'){
                                    //             if($value['nk_option_image'] != null && $value['nk_option_image'] != "" && $value['nk_option_show'] == "A"){
                                    //                 $image = $value['nk_option_image'];
                                    //                 $datas33['image'] = $image;
                                    //                 $datas33['name'] = "";
                                    //             }
                                    //         }
                                    //     }
                                    // }

                                }
                            }
                        }
                    }
                }
            }
            /*if(!empty($datas1['promotion_img'])){
                $datas1['promotion_img'] = array_filter($datas1['promotion_img']);
            }*/
            if(!empty($datas1['promotion_img'])){
                foreach ($datas1['promotion_img'] as $key => $value) {
                    if (empty($value)) {
                        unset($datas1['promotion_img'][$key]);
                    }
                }
            }

            $datas4=[];
            $datas2=[];
            if(!empty($product_features)){
                $i=1;
                foreach ($product_features as $feature){
                    if($i > 8)
                        break;
                    if($feature['feature_type'] == 'G' && isset($feature['subfeatures'])){
                        if(!empty($feature['subfeatures'])){
                            foreach ($feature['subfeatures'] as $subfeature){
                                if($i > 10)
                                    break;
                                $data4['title'] = $subfeature['description'];
                                $suffix = $subfeature['suffix'] ? $subfeature['suffix'] : '';
                                foreach ($subfeature['variants'] as $var){

                                    $data4['des'] = $var['variant'] .' '.$suffix;
                                }
                                $datas4[] = $data4;
                                $i++;
                            }
                        }
                    }
                }
                foreach ($product_features as $feature){
                    if($feature['feature_type'] == 'G' && isset($feature['subfeatures'])){
                        if(!empty($feature['subfeatures'])){
                            foreach ($feature['subfeatures'] as $subfeature){
                                $data22['title'] = $subfeature['description'];
                                $suffix = $subfeature['suffix'] ? $subfeature['suffix'] : '';
                                foreach ($subfeature['variants'] as $var){

                                    $data22['des'] = $var['variant'] .' '.$suffix;
                                }
                                $datas2[] = $data22;
                            }
                        }
                    }
                }
            }

            $datas1['product_characteristics']= $datas4;
            $datas1['specifications']= $datas2;
            $datas1['banner'] = [];
            /* start banner PDP */
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.nguyenkim.com/v1/banner-pdp/search/".(int)$product_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: nko_internal",
                    "content-type: application/json",
                    "password: eJwrtjIytVJyyMvOj8/MK0ktykvMSTUyMDR3yPPWS87PVbIGALc2Cqc=",
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            if(!$err) {
                $data_banner = json_decode($response,true);
                if(!empty($data_banner['data'])){
                    $datas1['banner']=[
                        'image' => $data_banner['data']['BannerLink'],
                        'url' => ''
                    ];
                }
            }
            /* end banner PDP */
            $datas1['installment_finance'] = array(
                'status' => 0,
                'payment_id' => 55
            );
            $data_post = array(
                'product_id' => $datas[0]['product_id'],
                'quantity' => 1
            );
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://dev.api.nguyenkimonline.com/v1/package-optimize",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data_post),
                CURLOPT_HTTPHEADER => array(
                        "authorization: nko_internal",
                        "content-type: application/json",
                        "password: eJwrtjIytVJyyMvOj8/MK0ktykvMSTUyMDR3yPPWS87PVbIGALc2Cqc=",
                    ),
            ));
            $response1 = curl_exec($curl);
            $err = curl_error($curl);
            $res_api = json_decode($response1, true);
            if(!$err) {
                $datas1['installment_finance'] = array(
                    'status' => (isset($res_api['datas']['list_data']) && count($res_api['datas']['list_data']) > 0) ? 1 : 0,
                    'payment_id' => 55
                );
            }


            $datas1['style_css_pdp'] = 'https://beta.api.nguyenkimonline.com/css/app_product.css';
            $total_like = DB::connection('mongodb')->collection('nk_likes')
                ->where('product_id', (int)$product_id)
                ->where('status',1)->get();
            $datas1['total_like'] = count($total_like);
            $datas1['is_like'] = 0;

            if((int)$user_id > 0){
                $like = DB::connection('mongodb')->collection('nk_likes')->where('product_id', (int)$product_id)->where('user_id',(int)$user_id)->get(['status']);
                if(!empty($like[0]['status'])){
                    $datas1['is_like'] = $like[0]['status'];
                }
            }

            $datas1['meta_url'] = 'https://www.nguyenkim.com/'.$datas[0]['seo_name'].'.html';
            $datas1['meta_description'] = $datas[0]['meta_description'];
            $datas1['meta_title'] = $datas[0]['page_title'];

            if($total == 0){
                return array(
                    'status' => 400,
                    'message' => 'Sản phẩm không tồn tại',
                    'data' => []
                );
            }
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $datas1
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function categories($request,$id)
    {
        $limit = $request->input('limit', 12);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $start = ($offset - 1) * $limit;
        $data_menu_config = config('constants.menu_config');
        $data_info_cate = [];
        foreach ($data_menu_config as $key => $value){
            if($value['id'] == $id){
                $data_info_cate = $value;
                break;
            }
        }
        if(empty($data_info_cate)){
            return array(
                'status' => 500,
                'message' => 'Không có dữ liệu phù hợp',
                'data' => []
            );
        }
        $data = DB::connection('mongodb')->collection('products_full_mode')->where('product_id',58263)->skip($start)->take($limit)
            ->get([
                'product_id'
                ,'description'
                ,'product_code'
                ,'amount'
                ,'weight'
                ,'length'
                ,'width'
                ,'height'
                ,'list_price'
                ,'price'
                ,'product_name'
                ,'display_name'
                ,'model'
                ,'brand'
                ,'images'
                ,'category_ids'
            ]);

        $total = count($data);
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => [

                'brand' =>[],
                'filter' => [],
                'sort_by' => [
                    '1' => 'Bán chạy nhất',
                    '2' => 'Giá thấp đến cao',
                    '3' => 'Giá cao xuống thấp'
                ],
                'header' =>[
                    'id' => $data_info_cate['id'],
                    'alias' =>$data_info_cate['alias'],
                    'name' => $data_info_cate['name'],
                    'description' => $data_info_cate['name']
                ],
                'banner' =>[],
                'product' => $data,
                'page_info' =>[
                    'total'=> $total,
                    'page_curent' => 1
                ]
            ],
        );

    }
    public function premium($request,$id){
        $data_info = config('constants.category_info');
        $time = time();
        $data = [];
        $key_home = "REDIS_PREMIUM_".$id;
        if(RedisClient::existsKey($key_home)){
            $data = RedisClient::getValue($key_home);
        }else {
            foreach ($data_info as $key => $value) {
                if ($value['id'] == $id) {
                    if (!empty($value['premium'])) {
                        if ($value['premium']['status'] == 1) {
                            $title = $value['premium']['title'];
                            $background = $value['premium']['background'];
                            if (!empty($value['premium']['data'])) {
                                foreach ($value['premium']['data'] as $item) {
                                    $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', intval($item['id']))->limit(1)
                                        ->get([
                                            'product_id'
                                            , 'mobile_short_description'
                                            , 'product_code'
                                            , 'amount'
                                            , 'weight'
                                            , 'length'
                                            , 'width'
                                            , 'height'
                                            , 'list_price'
                                            , 'price'
                                            , 'product'
                                            , 'nk_shortname'
                                            , 'main_pair'
                                            , 'category_ids'
                                            , 'nk_tragop_0'
                                            , 'product_options'
                                            , 'tag_image'
                                            , 'nk_is_shock_price'
                                            , 'product_features'
                                            , 'nk_product_tags'

                                        ]);
                                    $datas1 = [];
                                    foreach ($data_products1s as $data_products1) {
                                        $data1['images'] = $item['images'];
                                        $data1['tag_image_top_left'] = '';
                                        $data1['tag_image_top_right'] = '';
                                        $data1['tag_image_bottom_left'] = '';
                                        $data1['tag_image_bottom_right'] = '';
                                        if (!empty($data_products1['nk_product_tags'])) {
                                            foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                                if ($val['tag_position'] == 'bottom-right') {
                                                    $data1['tag_image_bottom_right'] = $val['tag_image'];
                                                } elseif ($val['tag_position'] == 'bottom-left') {
                                                    $data1['tag_image_bottom_left'] = $val['tag_image'];
                                                } elseif ($val['tag_position'] == 'top-right') {
                                                    $data1['tag_image_top_right'] = $val['tag_image'];
                                                } elseif ($val['tag_position'] == 'top-left') {
                                                    $data1['tag_image_top_left'] = $val['tag_image'];
                                                }
                                            }
                                        }
                                        $data1['list_price'] = (int)$data_products1['list_price'];
                                        $data1['price'] = (int)$data_products1['price'];
                                        $data1['product_name'] = $data_products1['product'];
                                        $data1['shortname'] = $data_products1['nk_shortname'];
                                        $data1['product_id'] = $data_products1['product_id'];
                                    }
                                    $data[] = $data1;
                                }
                            }
                        }
                    }
                    $data_res = [
                        'title' => $title,
                        'background' => $background,
                        'products' => $data,

                    ];
                    RedisClient::setValue($key_home, $data_res, 86400);
                    break;
                }
            }
        }
        if(empty($data)){
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => []
            );
        }else{
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            );
        }

    }
    public function GetListProduct($request){
        $params = $request->all();
        $time = time();
        $data=[];
        $product_ids = (array)$params['products_list'];
        if(isset($params['user_id']))
            $user_id = $params['user_id'];
        else
            $user_id = 0;
        $datas1 = [];
        $key_home = "REDIS_LIST_PRODUCT";
//        if(RedisClient::existsKey($key_home)){
//            $data = RedisClient::getValue($key_home);
//        }else {
            if(!empty($product_ids)){
                foreach($product_ids as $key => $value){
                    $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', (int)$value)->get([
                        'product_id'
                        ,'mobile_short_description'
                        ,'product_code'
                        ,'amount'
                        ,'weight'
                        ,'length'
                        ,'width'
                        ,'height'
                        ,'list_price'
                        ,'price'
                        ,'product'
                        ,'nk_shortname'
                        ,'main_pair'
                        ,'category_ids'
                        ,'nk_tragop_0'
                        ,'product_options'
                        ,'tag_image'
                        ,'nk_is_shock_price'
                        ,'product_features'
                        ,'nk_product_tags'
                        ,'text_shock'
                        ,'shock_online_exp'
                        ,'nk_special_tag'
                        ,'offline_price'
                        ,'model'
                        ,'nk_is_tra_gop'
                        ,'nk_is_add_cart'
                        ,'seo_name'
                        ,'hidden_option'
                        ,'nk_check_total_amount'
                        ,'short_description'
                        ,'tag_image_special'
                        ,'tag_start_time_special'
                        ,'tag_end_time_special'
                        ,'status'
                        ,'hidden_option'
                        ,'nk_show_two_price'
                        ,'nk_discount_label'
                        ,'rating'
                        ,'comment'
                        ,'product_options'
                        ,'nk_new_model'
                        ,'nk_is_bestseller'
                    ])->unique('product_id');

                    foreach ($data_products1s as $data_products1){
                        $discontinued_for_sale = 0;
                        if(($data_products1['status'] == 'H' || $data_products1['status'] == 'D' ) && $data_products1['hidden_option'] == "D"){
                            $discontinued_for_sale = 1;
                        }
                        if($discontinued_for_sale == 0){
                            $data1['rating'] = isset($data_products1['rating'])?$data_products1['rating']:0;
                            $data1['comment'] = isset($data_products1['comment'])?$data_products1['comment']:'';
                            $data1['status'] = $data_products1['status'];
                            $data1['hidden_option'] = $data_products1['hidden_option'];
                            $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                            $data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                            $data1['discount_percent'] = 0;
                            $data1['discount_percent'] = 0;
                            if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                            }
                            $data_products['text_promotion']=[];
                            $data1['tag_image_special'] = $data_products1['tag_image_special'];
                            $data1['tag_start_time_special'] = $data_products1['tag_start_time_special'];
                            $data1['tag_end_time_special'] = $data_products1['tag_end_time_special'];
                            $data1['images']='';
                            $data1['brand'] = '';
                            $data1['tag_image_top_left'] = '';
                            $data1['tag_image_top_right'] = '';
                            $data1['tag_image_bottom_left'] = '';
                            $data1['tag_image_bottom_right'] = '';
                            $data1['model'] ='';// $data_products1['model'];
                            if($data_products1){
                                if(!empty($data_products1['product_options'])){
                                    foreach ($data_products1['product_options'] as $key => $val){
                                        if(trim($val['nk_uu_dai_len_den']) !=''){
                                            $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                            break;
                                        }
                                    }
                                }
                                if(!empty($data_products1['nk_product_tags'])){
                                    foreach ($data_products1['nk_product_tags'] as $key => $val){
                                        if($val['tag_position'] == 'bottom-right'){
                                            $data1['tag_image_bottom_right'] = $val['tag_image'];
                                        }elseif($val['tag_position'] == 'bottom-left'){
                                            $data1['tag_image_bottom_left'] = $val['tag_image'];
                                        }elseif($val['tag_position'] == 'top-right'){
                                            $data1['tag_image_top_right'] = $val['tag_image'];
                                        }elseif($val['tag_position'] == 'top-left'){
                                            $data1['tag_image_top_left'] = $val['tag_image'];
                                        }
                                    }
                                }
                                //$thumb = $data_products1['main_pair'];
                                //$image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
                                //$data1['images'] = 'https://www.nguyenkim.com/images/thumbnails/250/250/'. $image['relative_path'];

                                $thumb = $data_products1['main_pair'];
                                if(!empty($thumb['icon'])) {
                                    $image = $thumb['icon'];
                                }else{
                                    if(!empty($thumb['detailed'])){
                                        $image = $thumb['detailed'];
                                    }
                                }
                                $data1['images'] = 'https://www.nguyenkim.com/images/thumbnails/250/250/'. $image['relative_path'];


                                if($data_products1['tag_image_special'] != ''){
                                    $time = time();
                                    if($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']){
                                        $data1['images'] = str_replace("/images/","/images/thumbnails/250/250/",$data_products1['tag_image_special']);
                                        //$data1['images'] = $data_products1['tag_image_special'];
                                    }
                                }

                                if(!empty($data_products1['product_features'])){
                                    if(!empty($data_products1['product_features']['591'])){
                                        if(!empty($data_products1['product_features']['591']['subfeatures'])){
                                            if(!empty($data_products1['product_features']['591']['subfeatures']['599'])){
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                if(!empty($data_variant['variants'])){
                                                    foreach ($data_variant['variants'] as $item){
                                                        $data1['brand'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if(empty($data1['brand'])){
                                    $data1['brand'] = $this->getbrand((int)$data_products1['product_id']);
                                }
                                if(!empty($data_products1['product_features'])){
                                    if(!empty($data_products1['product_features']['591'])){
                                        if(!empty($data_products1['product_features']['591']['subfeatures'])){
                                            if(!empty($data_products1['product_features']['591']['subfeatures']['596'])){
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                                if(!empty($data_variant['variants'])){
                                                    foreach ($data_variant['variants'] as $item){
                                                        $data1['model'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $data1['product_id'] = $data_products1['product_id'];
                            $data1['description'] = $data_products1['mobile_short_description'];
                            $data1['product_code'] = $data_products1['product_code'];
                            $data1['amount'] = $data_products1['amount'];
                            $data1['weight'] = $data_products1['weight'];
                            $data1['list_price'] = (int)$data_products1['list_price'];
                            if($data_products1['nk_show_two_price'] == 'N' && ($data_products1['nk_is_shock_price'] != 'Y' && $data_products1['nk_is_shock_price'] != 'O' )){
                                $data1['list_price'] = (int)$data_products1['price'];
                            }
                            $data1['price'] = (int)$data_products1['price'];
                            $data1['offline_price'] = (int)$data_products1['offline_price'];
                            $data1['product_name'] = $data_products1['product'];
                            $data1['shortname'] = $data_products1['nk_shortname'];
                            $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                            $data1['categories'] = $data_products1['category_ids'];
                            $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                            $data1['length'] = $data_products1['length'];
                            $data1['width'] = $data_products1['width'];
                            $data1['height'] = $data_products1['height'];
                            $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                            $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                            $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                            $data1['text_shock'] = $data_products1['text_shock'];
                            $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                            $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                            $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                            // get like product
                            $data1['is_like'] = 0;
                            /*if($user_id > 0){
                                $like = DB::connection('mongodb')->collection('nk_likes')->where('product_id', (int)$data_products1['product_id'])->where('user_id',$user_id)->get(['status']);
                                if(!empty($like[0])){
                                    $data1['is_like'] = $like[0]['status'];
                                }
                            }*/
                            $data1['nk_is_tra_gop'] = $data_products1['nk_is_tra_gop'];
                            $data1['nk_is_add_cart'] = $data_products1['nk_is_add_cart'];
                            $data1['seo_name'] = $data_products1['seo_name'];
                            $data1['hidden_option'] = $data_products1['hidden_option'];
                            $data1['nk_check_total_amount'] = $data_products1['nk_check_total_amount'];
                            $data1['short_description'] = $data_products1['short_description'];
                            $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];//default
                            $data1['nk_discount_label'] = isset($data_products1['nk_discount_label'])?$data_products1['nk_discount_label']:'Y';//default
                            $data1['product_options'] = isset($data_products1['product_options'])?$data_products1['product_options']:[];
                            $data1['nk_new_model'] = isset($data_products1['nk_new_model'])?$data_products1['nk_new_model']:0;
                            $data1['nk_is_bestseller'] = isset($data_products1['nk_is_bestseller'])?$data_products1['nk_is_bestseller']:0;
                            $data1['text_tragop'] = '';
                            //if(($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] =='O'){
                                if($data_products1['nk_tragop_0'] == 1) {
                                    $data1['text_tragop'] = $this->get_text_tra_gop($data_products1['price']);
                                }

                            //}
                            $data[] =  $data1;

                        }

                    }
                }
            }
            //RedisClient::setValue($key_home, $data, 86400);
        //}
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data
        );
    }
    public function barcode($request,$id)
    {
        $product_id = intval($id);
        try {
            $datas =  DB::connection('mongodb')->collection('products_full_mode_new')
                ->where('product_id', $product_id)
                ->orWhere('barcode', $product_id)
                ->limit(1)
                ->get(['product_id']);
            $total = count($datas);

            if($total == 0){
                return array(
                    'status' => 400,
                    'message' => 'Sản phẩm không tồn tại',
                    'data' => []
                );
            }
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => [
                    'product_id' =>$datas[0]['product_id'],
                    'url' => 'https://www.nguyenkim.com/index.php?dispatch=products.view?product_id=' . $product_id
                ]
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function checkqrcode($request)
    {

        $params = $request->all();
        $str = isset($params['str']) ? $params['str'] : '' ;
        if(isset($params['pcode'])){
            $pcode = isset($params['pcode']) ? $params['pcode'] : '';
        }
        if(isset($params['pid'])){
            $pcode = isset($params['pid']) ? $params['pid'] : '';
        }

        try {

            //$str ="https://test.nguyenkimonline.com/index.php?dispatch=partner.demo_apply_coupon&pcode=000000000010035884-BO&coupon_code=BIG4U";
            if($pcode !='' && (isset($params['ckey']) && $params['ckey'] !='') && (isset($params['src_type']) && $params['src_type'] == 15)){

                $coupon_code = $params['ckey'];
                $product_id = intval($str);
                if(isset($params['pcode']) && $params['pcode'] !=''){
                    $datas =  DB::connection('mongodb')->collection('products_full_mode_new')
                        ->where('product_code','like', "%$pcode%")
                        ->orWhere('product_short_code','like', "%$pcode%")
                        ->limit(1)
                        ->get(['product_id']);
                }else{
                    $datas =  DB::connection('mongodb')->collection('products_full_mode_new')
                        ->where('product_id', (int)$pcode)
                        ->limit(1)
                        ->get(['product_id']);
                }


                $total = count($datas);
                if($total == 0){
                    return array(
                        'status' => 400,
                        'message' => 'Sản phẩm không tồn tại',
                        'data' => []
                    );
                }
                return array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => [
                        'product_id' =>$datas[0]['product_id'],
                        'coupon_code' => $coupon_code,
                        'check_cart' => 1,
                        'url' => 'https://www.nguyenkim.com/index.php?dispatch=products.view?product_id=' . $product_id
                    ]
                );
            }else{
                return array(
                    'status' => 400,
                    'message' => 'Sản phẩm không tồn tại',
                    'data' => [
                    ]
                );
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function search($request){
        $params = $request->all();
        $limit = $request->input('limit', 40);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $start = ($offset - 1) * $limit;
        $keyword = $request->input('keyword') ? $request->input('keyword') : '';
        if(!empty($keyword))
            $keyword =urldecode($keyword);
        $time = time();
        $data=[];
        $total = DB::connection('mongodb')->collection('products_full_mode_new')
            ->where('nk_shortname','like', '%'.$keyword.'%')
            ->where('status', 'A')
            ->count();
        if(isset($params['user_id']))
            $user_id = $params['user_id'];
        else
            $user_id = 0;
        $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')
            ->where('nk_shortname','like', '%'.$keyword.'%')
            ->where('status', 'A')
            ->skip($start)
            ->take($limit)
            ->get([
                'product_id'
                ,'mobile_short_description'
                ,'product_code'
                ,'amount'
                ,'weight'
                ,'length'
                ,'width'
                ,'height'
                ,'list_price'
                ,'price'
                ,'product'
                ,'nk_shortname'
                ,'main_pair'
                ,'category_ids'
                ,'nk_tragop_0'
                ,'product_options'
                ,'tag_image'
                ,'nk_is_shock_price'
                ,'product_features'
                ,'nk_product_tags'
                ,'text_shock'
                ,'shock_online_exp'
                ,'nk_special_tag'
                ,'offline_price'
                ,'model'
            ]);
        $datas1 = [];
        foreach ($data_products1s as $data_products1){
            $data_products['text_promotion']=[];
            $data1['images']='';
            $data1['brand'] = '';
            $data1['tag_image_top_left'] = '';
            $data1['tag_image_top_right'] = '';
            $data1['tag_image_bottom_left'] = '';
            $data1['tag_image_bottom_right'] = '';
            $data1['model'] ='';// $data_products1['model'];
            if($data_products1){
                if(!empty($data_products1['product_options'])){
                    foreach ($data_products1['product_options'] as $key => $val){
                        $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                    }
                }
                if(!empty($data_products1['nk_product_tags'])){
                    foreach ($data_products1['nk_product_tags'] as $key => $val){
                        if($val['tag_position'] == 'bottom-right'){
                            $data1['tag_image_bottom_right'] = $val['tag_image'];
                        }elseif($val['tag_position'] == 'bottom-left'){
                            $data1['tag_image_bottom_left'] = $val['tag_image'];
                        }elseif($val['tag_position'] == 'top-right'){
                            $data1['tag_image_top_right'] = $val['tag_image'];
                        }elseif($val['tag_position'] == 'top-left'){
                            $data1['tag_image_top_left'] = $val['tag_image'];
                        }
                    }
                }
                if(!empty($data_products1['main_pair'])){
                    if(!empty($data_products1['main_pair']['icon'])) {
                        $data1['images'] = str_replace('images/product','images/thumbnails/250/250/product',$data_products1['main_pair']['icon']['image_path']);
                    }else{
                        if(!empty($data_products1['main_pair']['detailed'])) {
                            $data1['images'] = str_replace('images/detailed', 'images/thumbnails/250/250/detailed', $data_products1['main_pair']['detailed']['image_path']);
                        }
                    }
                }
                if(!empty($data_products1['product_features'])){
                    if(!empty($data_products1['product_features']['591'])){
                        if(!empty($data_products1['product_features']['591']['subfeatures'])){
                            if(!empty($data_products1['product_features']['591']['subfeatures']['599'])){
                                $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                if(!empty($data_variant['variants'])){
                                    foreach ($data_variant['variants'] as $item){
                                        $data1['brand'] = $item['variant'];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if(!empty($data_products1['product_features'])){
                    if(!empty($data_products1['product_features']['591'])){
                        if(!empty($data_products1['product_features']['591']['subfeatures'])){
                            if(!empty($data_products1['product_features']['591']['subfeatures']['596'])){
                                $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                if(!empty($data_variant['variants'])){
                                    foreach ($data_variant['variants'] as $item){
                                        $data1['model'] = $item['variant'];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $data1['product_id'] = $data_products1['product_id'];
            $data1['description'] = $data_products1['mobile_short_description'];
            $data1['product_code'] = $data_products1['product_code'];
            $data1['amount'] = $data_products1['amount'];
            $data1['weight'] = $data_products1['weight'];
            $data1['list_price'] = (int)$data_products1['list_price'];
            $data1['price'] = (int)$data_products1['price'];
            $data1['offline_price'] = (int)$data_products1['offline_price'];
            $data1['product_name'] = $data_products1['product'];
            $data1['shortname'] = $data_products1['nk_shortname'];
            $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
            $data1['categories'] = $data_products1['category_ids'];
            $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
            $data1['length'] = $data_products1['length'];
            $data1['width'] = $data_products1['width'];
            $data1['height'] = $data_products1['height'];
            $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
            $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
            $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
            $data1['text_shock'] = $data_products1['text_shock'];
            $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
            $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
            $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
            // get like product
            $data1['is_like'] = 0;
            if($user_id > 0){
                $like = DB::connection('mongodb')->collection('nk_likes')->where('product_id', (int)$data_products1['product_id'])->where('user_id',$user_id)->get(['status']);
                if(!empty($like[0])){
                    $data1['is_like'] = $like[0]['status'];
                }
            }
            $data[] =  $data1;
        }
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data,
            'pageinfo'=>[
                'total' => $total,
                'current_page' => $offset
            ]
        );
    }
    public function compare($request){
        $params = $request->all();
        $time = time();
        $data=[];
        $product_ids = (array)$params['products_list'];
        if(isset($params['user_id']))
            $user_id = $params['user_id'];
        else
            $user_id = 0;
        $datas1 = [];
        if(!empty($product_ids)){
            foreach($product_ids as $key => $value){
                $url = "https://dev.api.nguyenkimonline.com/v1/product-details/".(int)$value."/0";
                //echo $url;die;
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: nko_internal",
                        "cache-control: no-cache",
                        "content-type: application/json",
                        "password: eJwrtjIytVJyyMvOj8/MK0ktykvMSTUyMDR3yPPWS87PVbIGALc2Cqc="
                    ),
                ));
                $response = curl_exec($curl);
                $response = json_decode($response,true);
                //var_dump($response);die;
                $data['promotion'][] = $response['data']['promotion'];
                $data['promotion_option'][] = $response['data']['promotion_option'];
                $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', (int)$value)->get([
                    'product_id'
                    ,'mobile_short_description'
                    ,'product_code'
                    ,'amount'
                    ,'weight'
                    ,'length'
                    ,'width'
                    ,'height'
                    ,'list_price'
                    ,'price'
                    ,'product'
                    ,'nk_shortname'
                    ,'main_pair'
                    ,'category_ids'
                    ,'nk_tragop_0'
                    ,'product_options'
                    ,'tag_image'
                    ,'nk_is_shock_price'
                    ,'product_features'
                    ,'nk_product_tags'
                    ,'text_shock'
                    ,'shock_online_exp'
                    ,'nk_special_tag'
                    ,'offline_price'
                    ,'model'
                    ,'nk_is_tra_gop'
                    ,'nk_is_add_cart'
                    ,'seo_name'
                    ,'hidden_option'
                    ,'nk_check_total_amount'
                    ,'short_description'
                    ,'nk_show_two_price'
                ]);

                foreach ($data_products1s as $data_products1){
                    $data_products['text_promotion']=[];
                    $data1['images']='';
                    $data1['brand'] = '';
                    $data1['tag_image_top_left'] = '';
                    $data1['tag_image_top_right'] = '';
                    $data1['tag_image_bottom_left'] = '';
                    $data1['tag_image_bottom_right'] = '';
                    $data1['model'] ='';// $data_products1['model'];
                    if($data_products1){
                        if(!empty($data_products1['product_options'])){
                            foreach ($data_products1['product_options'] as $key => $val){
                                $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                break;
                            }
                        }
                        if(!empty($data_products1['nk_product_tags'])){
                            foreach ($data_products1['nk_product_tags'] as $key => $val){
                                if($val['tag_position'] == 'bottom-right'){
                                    $data1['tag_image_bottom_right'] = $val['tag_image'];
                                }elseif($val['tag_position'] == 'bottom-left'){
                                    $data1['tag_image_bottom_left'] = $val['tag_image'];
                                }elseif($val['tag_position'] == 'top-right'){
                                    $data1['tag_image_top_right'] = $val['tag_image'];
                                }elseif($val['tag_position'] == 'top-left'){
                                    $data1['tag_image_top_left'] = $val['tag_image'];
                                }
                            }
                        }
                        $thumb = $data_products1['main_pair'];
                        $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
                        $data1['images'] = 'https://www.nguyenkim.com/images/thumbnails/250/250/'. $image['relative_path'];
                        if(!empty($data_products1['product_features'])){
                            if(!empty($data_products1['product_features']['591'])){
                                if(!empty($data_products1['product_features']['591']['subfeatures'])){
                                    if(!empty($data_products1['product_features']['591']['subfeatures']['599'])){
                                        $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                        if(!empty($data_variant['variants'])){
                                            foreach ($data_variant['variants'] as $item){
                                                $data1['brand'] = $item['variant'];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(!empty($data_products1['product_features'])){
                            if(!empty($data_products1['product_features']['591'])){
                                if(!empty($data_products1['product_features']['591']['subfeatures'])){
                                    if(!empty($data_products1['product_features']['591']['subfeatures']['596'])){
                                        $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                        if(!empty($data_variant['variants'])){
                                            foreach ($data_variant['variants'] as $item){
                                                $data1['model'] = $item['variant'];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $data1['product_id'] = $data_products1['product_id'];
                    $data1['description'] = $data_products1['mobile_short_description'];
                    $data1['product_code'] = $data_products1['product_code'];
                    $data1['amount'] = $data_products1['amount'];
                    $data1['weight'] = $data_products1['weight'];
                    $data1['list_price'] = (int)$data_products1['list_price'];
                    $data1['price'] = (int)$data_products1['price'];
                    $data1['offline_price'] = (int)$data_products1['offline_price'];
                    $data1['product_name'] = $data_products1['product'];
                    $data1['shortname'] = $data_products1['nk_shortname'];
                    $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                    $data1['categories'] = $data_products1['category_ids'];
                    $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                    $data1['length'] = $data_products1['length'];
                    $data1['width'] = $data_products1['width'];
                    $data1['height'] = $data_products1['height'];
                    $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                    $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                    $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                    $data1['text_shock'] = $data_products1['text_shock'];
                    $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                    $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                    $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                    $data1['nk_is_tra_gop'] = $data_products1['nk_is_tra_gop'];
                    $data1['nk_is_add_cart'] = $data_products1['nk_is_add_cart'];
                    $data1['seo_name'] = $data_products1['seo_name'];
                    $data1['hidden_option'] = $data_products1['hidden_option'];
                    $data1['nk_check_total_amount'] = $data_products1['nk_check_total_amount'];
                    $data1['short_description'] = $data_products1['short_description'];
                    $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                    $data['DetailProduct'][] =  $data1;
                    $datas2=[];
                    $product_features = $data_products1['product_features'];
                    $datas4 =[];
                    $data['promotion'] = [];
                    $promotion_option = [];
                    $datas_ops222 = [];
                    if(!empty($data_products1['product_options'])){
                        foreach ($data_products1['product_options'] as $key => $value){
                            if($value['nk_option_type'] == 'P' || $value['nk_option_type'] == 'N'){
                                if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                                    if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                        if ($value['option_type'] == "R"){
                                            if (!empty($value['variants']) && count($value['variants']) <= 1 ){
                                                $pro_des = $value['description'];
                                                $datas3['url']='';
                                                $datas3['show']= 0;
                                                if(strstr($pro_des,"Xem chi tiết")){
                                                    $pro_des = str_replace('Xem chi tiết','',$pro_des);
                                                    $datas3['url'] = get_value_href($pro_des);
                                                    $datas3['show']= 1;
                                                }
                                                $datas3['des'] = strip_tags($pro_des);
                                                $datas3['option_type'] = $value['option_type'];
                                                $data['promotion']['data'][] =$datas3;
                                            }else{
                                                if(!empty($value['variants'])){
                                                    //$data['promotion_option']['des'] = $value['description'];
                                                    foreach ($value['variants'] as $key1 => $value1){
                                                        $datas_ops111['variant_id'] = $value1['variant_id'];
                                                        $datas_ops111['option_id'] = $value1['option_id'];
                                                        $datas_ops111['variant_name'] = $value1['variant_name'];
                                                        $datas_ops222[] = $datas_ops111;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $promotion_option[] = $datas_ops222;
                    if(!empty($product_features)){
                        foreach ($product_features as $feature){
                            if($feature['feature_type'] == 'G' && isset($feature['subfeatures'])){
                                if(!empty($feature['subfeatures'])){
                                    foreach ($feature['subfeatures'] as $keys => $subfeature){
                                        $data22['title'] = $subfeature['description'];
                                        foreach ($subfeature['variants'] as  $var){
                                            $data22['key'] = $keys;
                                            $data22['variant_id'] = $var['variant_id'];
                                            $data22['des'] = $var['variant'];
                                            $datas4[] = $data22;
                                        }

                                    }
                                }
                            }
                        }
                    }

                    if(!empty($data_products1['product_options'])){
                        foreach ($data_products1['product_options'] as $key => $value){
                            if($value['nk_option_type'] == 'P' || $value['nk_option_type'] == 'N'){
                                if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                                    if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                        if ($value['option_type'] == "R"){
                                            if (!empty($value['variants']) && count($value['variants']) <= 1 ){
                                                $pro_des = $value['description'];
                                                //$datas3['url']='';
                                                //$datas3['show']= 0;

                                                //$datas3 = [];

                                                if(strstr($pro_des,"Xem chi tiết")){
                                                    $pro_des = str_replace('Xem chi tiết','',$pro_des);
                                                    //$datas3['url'] = get_value_href($pro_des);
                                                    //$datas3['show']= 1;
                                                }
                                                $datas2['info'] = (array)strip_tags($pro_des);
                                            }else{
                                                if(!empty($value['variants'])){
                                                    //$datas1['promotion_option']['des'] = [];
                                                    $datas_ops1 = [];
                                                    foreach ($value['variants'] as $key1 => $value1){
                                                        array_push($datas_ops1,$value1['variant_name']);
                                                        $datas2['info'] = $datas_ops1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $datas33 = [];
                    if(!empty($data_products1['product_options'])){
                        foreach ($data_products1['product_options'] as $key => $value){
                            if($value['nk_option_type'] == 'P'){
                                if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                                    if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                        if ($value['option_type'] == "R"){
                                            if(!empty($value['variants']) && count($value['variants']) > 0){
                                                foreach ($value['variants'] as $key1 => $value1){
                                                    if($value1['is_show_banner'] != 'Y'){
                                                        if($value['nk_option_show'] == "A"){
                                                            if($value['nk_option_image'] != null || $value['nk_option_image'] != "" || $value1['image_desktop_pair']!= null || $value1['image_desktop_pair'] != ""){
                                                                if(isset($value1['image_desktop_pair']['icon'])){
                                                                    $image = $value1['image_desktop_pair']['icon']['image_path'];
                                                                    $datas33['image'] = $image;
                                                                    $datas33['name'] = $value1['image_desktop_pair']['icon']['alt'];
                                                                    $datas2['image'][] = $datas33;
                                                                }
//                                                                else{
//                                                                    $datas33['image'] = $value['nk_option_image'];
//                                                                    $datas33['name'] = "";
//                                                                }
                                                            }
//                                                            else{
//                                                                $image = $value['nk_option_image'];
//                                                                $datas33['image'] = $image;
//                                                                $datas33['name'] = "";
//                                                            }
                                                        }
                                                    }
                                                }
                                            }
//                                            else{
//                                                foreach ($value['variants'] as $vr){
//                                                    if($vr['is_show_banner'] != 'Y'){
//                                                        if($value['nk_option_image'] != null && $value['nk_option_image'] != "" && $value['nk_option_show'] == "A"){
//                                                            $image = $value['nk_option_image'];
//                                                            $datas33['image'] = $image;
//                                                            $datas33['name'] = "";
//                                                        }
//                                                    }
//                                                }
//                                            }
                                            //$data['promotion_img'][] =$datas33;

                                        }
                                    }
                                }
                            }
                        }
                    }

                    $data['KhuyenMai']['title'] =  'Khuyến mãi';
                    $data['KhuyenMai']['data'][] =  $datas2;
                    $de[] = $datas4;


                }
            }

            $d2 = [];
            $arr = [];
            foreach ($de as $key => $value) {
                foreach ($value as $k => $v) {
                    array_push($arr, $v['key']);
                }
            }
            $arr2 = [];
            if(!empty($arr)){
                foreach ($arr as $key3 => $value3) {
                    $arr2[$value3]=[];
                    foreach ($de as $key4 => $value4) {
                        foreach ($value4 as $k4 => $v4) {
                            if($v4['key'] == $value3){
                                array_push($arr2[$value3], $v4);
                            }

                        }
                    }
                }
            }
            // loại bỏ những key nào chỉ có 1 dong
            if(!empty($arr2)){
                foreach ($arr2 as $key5 => $value5) {
                    if(count($value5) == 1){
                        unset($arr2[$key5]);
                    }
                }

            }
            if(!empty($arr2)){
                foreach ($arr2 as $key6 => $value6) {
                    $d =[];
                    $d['title'] = $value6[0]['title'];
                    foreach ($value6 as $k7 => $v7) {
                        $ds['name'] = $v7['des'];
                        $d['data'][] = $ds;
                    }
                    $d2[] = $d;

                }
            }
            $d2 = array_filter($d2);
            $data['Description'] = [
                0 => array(
                    'title' => 'Thông số kỹ thuật',
                    'data' => $d2,
                    'show' => true
                )
            ];
        }

        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data
        );
    }
    public function compare1($request){
        $params = $request->all();
        $time = time();
        $data=[];
        $product_ids = (array)$params['products_list'];
        if(isset($params['user_id']))
            $user_id = $params['user_id'];
        else
            $user_id = 0;
        $datas1 = [];
        if(!empty($product_ids)){
            foreach($product_ids as $key => $value){
                $url = "https://api.nguyenkim.com/v1/product-details/".(int)$value."/0";
                //echo $url;die;
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: nko_internal",
                        "cache-control: no-cache",
                        "content-type: application/json",
                        "password: eJwrtjIytVJyyMvOj8/MK0ktykvMSTUyMDR3yPPWS87PVbIGALc2Cqc="
                    ),
                ));
                $response = curl_exec($curl);
                $response = json_decode($response,true);
                //var_dump($response);die;
                $data['promotion'][] = $response['data']['promotion'];
                $data['promotion_option'][] = $response['data']['promotion_option'];

                //var_dump($data['promotion_option']);die;
                $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', (int)$value)->get([
                    'product_id'
                    ,'mobile_short_description'
                    ,'product_code'
                    ,'amount'
                    ,'weight'
                    ,'length'
                    ,'width'
                    ,'height'
                    ,'list_price'
                    ,'price'
                    ,'product'
                    ,'nk_shortname'
                    ,'main_pair'
                    ,'category_ids'
                    ,'nk_tragop_0'
                    ,'product_options'
                    ,'tag_image'
                    ,'nk_is_shock_price'
                    ,'product_features'
                    ,'nk_product_tags'
                    ,'text_shock'
                    ,'shock_online_exp'
                    ,'nk_special_tag'
                    ,'offline_price'
                    ,'model'
                    ,'nk_is_tra_gop'
                    ,'nk_is_add_cart'
                    ,'seo_name'
                    ,'hidden_option'
                    ,'nk_check_total_amount'
                    ,'short_description'
                ]);

                foreach ($data_products1s as $data_products1){
                    $data_products['text_promotion']=[];
                    $data1['images']='';
                    $data1['brand'] = '';
                    $data1['tag_image_top_left'] = '';
                    $data1['tag_image_top_right'] = '';
                    $data1['tag_image_bottom_left'] = '';
                    $data1['tag_image_bottom_right'] = '';
                    $data1['model'] ='';// $data_products1['model'];
                    if($data_products1){
                        if(!empty($data_products1['product_options'])){
                            foreach ($data_products1['product_options'] as $key => $val){
                                $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                break;
                            }
                        }
                        if(!empty($data_products1['nk_product_tags'])){
                            foreach ($data_products1['nk_product_tags'] as $key => $val){
                                if($val['tag_position'] == 'bottom-right'){
                                    $data1['tag_image_bottom_right'] = $val['tag_image'];
                                }elseif($val['tag_position'] == 'bottom-left'){
                                    $data1['tag_image_bottom_left'] = $val['tag_image'];
                                }elseif($val['tag_position'] == 'top-right'){
                                    $data1['tag_image_top_right'] = $val['tag_image'];
                                }elseif($val['tag_position'] == 'top-left'){
                                    $data1['tag_image_top_left'] = $val['tag_image'];
                                }
                            }
                        }
                        $thumb = $data_products1['main_pair'];
                        if(!empty($thumb['icon'])) {
                            $image = $thumb['icon'];
                        }else{
                            if(!empty($thumb['detailed'])){
                                $image = $thumb['detailed'];
                            }
                        }
                        if(!empty($image)){
                            $data1['images'] = 'https://www.nguyenkim.com/images/thumbnails/250/250/'. $image['relative_path'];
                        }

                        if(!empty($data_products1['product_features'])){
                            if(!empty($data_products1['product_features']['591'])){
                                if(!empty($data_products1['product_features']['591']['subfeatures'])){
                                    if(!empty($data_products1['product_features']['591']['subfeatures']['599'])){
                                        $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                        if(!empty($data_variant['variants'])){
                                            foreach ($data_variant['variants'] as $item){
                                                $data1['brand'] = $item['variant'];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(!empty($data_products1['product_features'])){
                            if(!empty($data_products1['product_features']['591'])){
                                if(!empty($data_products1['product_features']['591']['subfeatures'])){
                                    if(!empty($data_products1['product_features']['591']['subfeatures']['596'])){
                                        $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                        if(!empty($data_variant['variants'])){
                                            foreach ($data_variant['variants'] as $item){
                                                $data1['model'] = $item['variant'];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $data1['product_id'] = $data_products1['product_id'];
                    $data1['description'] = $data_products1['mobile_short_description'];
                    $data1['product_code'] = $data_products1['product_code'];
                    $data1['amount'] = $data_products1['amount'];
                    $data1['weight'] = $data_products1['weight'];
                    $data1['list_price'] = (int)$data_products1['list_price'];
                    $data1['price'] = (int)$data_products1['price'];
                    $data1['offline_price'] = (int)$data_products1['offline_price'];
                    $data1['product_name'] = $data_products1['product'];
                    $data1['shortname'] = $data_products1['nk_shortname'];
                    $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                    $data1['categories'] = $data_products1['category_ids'];
                    $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                    $data1['length'] = $data_products1['length'];
                    $data1['width'] = $data_products1['width'];
                    $data1['height'] = $data_products1['height'];
                    $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                    $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                    $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                    $data1['text_shock'] = $data_products1['text_shock'];
                    $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                    $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                    $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                    $data1['nk_is_tra_gop'] = $data_products1['nk_is_tra_gop'];
                    $data1['nk_is_add_cart'] = $data_products1['nk_is_add_cart'];
                    $data1['seo_name'] = $data_products1['seo_name'];
                    $data1['hidden_option'] = $data_products1['hidden_option'];
                    $data1['nk_check_total_amount'] = $data_products1['nk_check_total_amount'];
                    $data1['short_description'] = $data_products1['short_description'];
                    $data['DetailProduct'][] =  $data1;
                    $datas2=[];
                    $product_features = $data_products1['product_features'];
                    $datas4 =[];
                    $data['promotion'] = [];
                    $promotion_option = [];
                    $datas_ops222 = [];
                    if(!empty($data_products1['product_options'])){
                        foreach ($data_products1['product_options'] as $key => $value){
                            if($value['nk_option_type'] == 'P' || $value['nk_option_type'] == 'N'){
                                if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                                    if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                        if ($value['option_type'] == "R"){
                                            if (!empty($value['variants']) && count($value['variants']) <= 1 ){
                                                $pro_des = $value['description'];
                                                $datas3['url']='';
                                                $datas3['show']= 0;
                                                if(strstr($pro_des,"Xem chi tiết")){
                                                    $pro_des = str_replace('Xem chi tiết','',$pro_des);
                                                    $datas3['url'] = get_value_href($pro_des);
                                                    $datas3['show']= 1;
                                                }
                                                $datas3['des'] = strip_tags($pro_des);
                                                $datas3['option_type'] = $value['option_type'];
                                                $data['promotion']['data'][] =$datas3;
                                            }else{
                                                if(!empty($value['variants'])){
                                                    //$data['promotion_option']['des'] = $value['description'];
                                                    foreach ($value['variants'] as $key1 => $value1){
                                                        $datas_ops111['variant_id'] = $value1['variant_id'];
                                                        $datas_ops111['option_id'] = $value1['option_id'];
                                                        $datas_ops111['variant_name'] = $value1['variant_name'];
                                                        $datas_ops222[] = $datas_ops111;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $promotion_option[] = $datas_ops222;
                    if(!empty($product_features)){
                        foreach ($product_features as $feature){
                            if($feature['feature_type'] == 'G' && isset($feature['subfeatures'])){
                                if(!empty($feature['subfeatures'])){
                                    foreach ($feature['subfeatures'] as $keys => $subfeature){
                                        $data22['title'] = $subfeature['description'];
                                        foreach ($subfeature['variants'] as  $var){
                                            $data22['key'] = $keys;
                                            $data22['variant_id'] = $var['variant_id'];
                                            $data22['des'] = $var['variant'];
                                            $datas4[] = $data22;
                                        }

                                    }
                                }
                            }
                        }
                    }
                    if(!empty($data_products1['product_options'])){
                        foreach ($data_products1['product_options'] as $key => $value){
                            if($value['nk_option_type'] == 'P' || $value['nk_option_type'] == 'N'){
                                if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                                    if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                        if ($value['option_type'] == "R"){
                                            if (!empty($value['variants']) && count($value['variants']) <= 1 ){
                                                $pro_des = $value['description'];
                                                //$datas3['url']='';
                                                //$datas3['show']= 0;

                                                //$datas3 = [];

                                                if(strstr($pro_des,"Xem chi tiết")){
                                                    $pro_des = str_replace('Xem chi tiết','',$pro_des);
                                                    //$datas3['url'] = get_value_href($pro_des);
                                                    //$datas3['show']= 1;
                                                }
                                                $datas2['info'] = (array)strip_tags($pro_des);
                                            }else{
                                                if(!empty($value['variants'])){
                                                    //$datas1['promotion_option']['des'] = [];
                                                    $datas_ops1 = [];
                                                    foreach ($value['variants'] as $key1 => $value1){
                                                        array_push($datas_ops1,$value1['variant_name']);
                                                        $datas2['info'] = $datas_ops1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if(!empty($data_products1['product_options'])){
                        foreach ($data_products1['product_options'] as $key => $value){
                            if($value['nk_option_type'] == 'P'){
                                if (strtolower($value['option_name']) != 'color' && strtolower($value['option_name']) != 'mau'){
                                    if (!(strpos($value['option_type'],"SRC") !== false && !$value['variants'] && $value['missing_variants_handling'] == "H")){
                                        if ($value['option_type'] == "R"){
                                            if(!empty($value['variants']) && count($value['variants']) > 0){
                                                foreach ($value['variants'] as $key1 => $value1){
                                                    if($value1['is_show_banner'] != 'Y'){
                                                        if($value['nk_option_show'] == "A"){
                                                            if($value['nk_option_image'] != null || $value['nk_option_image'] != "" || $value1['image_desktop_pair']!= null || $value1['image_desktop_pair'] != ""){
                                                                if(isset($value1['image_desktop_pair']['icon'])){
                                                                    $image = $value1['image_desktop_pair']['icon']['image_path'];
                                                                    $datas33['image'] = $image;
                                                                    $datas33['name'] = $value1['image_desktop_pair']['icon']['alt'];
                                                                }else{
                                                                    $datas33['image'] = $value['nk_option_image'];
                                                                    $datas33['name'] = "";
                                                                }
                                                            }else{
                                                                $image = $value['nk_option_image'];
                                                                $datas33['image'] = $image;
                                                                $datas33['name'] = "";
                                                            }
                                                        }
                                                    }
                                                }
                                            }else{
                                                foreach ($value['variants'] as $vr){
                                                    if($vr['is_show_banner'] != 'Y'){
                                                        if($value['nk_option_image'] != null && $value['nk_option_image'] != "" && $value['nk_option_show'] == "A"){
                                                            $image = $value['nk_option_image'];
                                                            $datas33['image'] = $image;
                                                            $datas33['name'] = "";
                                                        }
                                                    }
                                                }
                                            }
                                            //$data['promotion_img'][] =$datas33;
                                            $datas2['image'][] = $datas33;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $data['KhuyenMai']['title'] =  'Khuyến mãi';
                    $data['KhuyenMai']['data'][] =  $datas2;
                    $de[] = $datas4;


                }

            }
            //$data['promotion_option'] = (array)$promotion_option;
            $d2 = [];
            $arr = [];
            foreach ($de as $key => $value) {
                foreach ($value as $k => $v) {
                    array_push($arr, $v['key']);
                }
            }
            $arr2 = [];
            if(!empty($arr)){
                foreach ($arr as $key3 => $value3) {
                    $arr2[$value3]=[];
                    foreach ($de as $key4 => $value4) {
                        foreach ($value4 as $k4 => $v4) {
                            if($v4['key'] == $value3){
                                array_push($arr2[$value3], $v4);
                            }

                        }
                    }
                }
            }
            // loại bỏ những key nào chỉ có 1 dong
            if(!empty($arr2)){
                foreach ($arr2 as $key5 => $value5) {
                    if(count($value5) == 1){
                        unset($arr2[$key5]);
                    }
                }

            }
            if(!empty($arr2)){
                foreach ($arr2 as $key6 => $value6) {
                    $d =[];
                    $d['title'] = $value6[0]['title'];
                    foreach ($value6 as $k7 => $v7) {
                        $ds['name'] = $v7['des'];
                        $d['data'][] = $ds;
                    }
                    $d2[] = $d;

                }
            }
            $d2 = array_filter($d2);
            $data['Description'] = [
                0 => array(
                    'title' => 'Thông số kỹ thuật',
                    'data' => $d2,
                    'show' => true
                )
            ];
        }
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data
        );
    }
    public function getbrand($product_id){
        $brand = Features::getBrand($product_id);
        return $brand;
    }
    public function get_text_tra_gop($price){
        $text = '';
        if($price < 3000000){
            $text = '<div style="text-align: center">Trả góp thủ tục đơn giản, lãi suất thấp</div>';
        }else{
            $text_price = round($price/6);
            $text_price = number_format($text_price,0,",",".");
            $text = '<div style="text-align: center">Trả góp chỉ <b style="color:#000">'.$text_price.'đ</b>/tháng (6 tháng)</div>';
        }
        return $text;
    }
    public function giadung($request){
        try {
            $time = time();
            $area_id = $request->input('area_id', 18001);
            $key_home = "REDIS_GIADUNG_V1";
            if(RedisClient::existsKey($key_home)){
                $data = RedisClient::getValue($key_home);
            }else {
                $data['blog'] = RedisClient::getValue('BLOG');
                $data_cate = config('constants.category_menu_giadung');
                foreach ($data_cate as $key => $value) {
                    $cate_id = $value['id'];
                    $key_cate = "CATES_".$cate_id;
                    $datas1 = $value;
                    if(RedisClient::existsKey($key_cate)){
                        $datas1['data'] = RedisClient::getValue($key_cate);
                    }else{
                        $datas1['data'] = [];
                    }
                    $data['menu'][] = $datas1;
                }
                $banners = DB::connection('mongodb')->collection('banners')
                    ->where('type_display', 3)
                    ->where('position_floor_banner', 3)
                    ->where('time_start', '<', $time)
                    ->where('time_finish', '>', $time)
                    ->where('status', 'A')
                    ->whereIn('banner_areas', [(int)$area_id])
                    ->orderBy('position', 'ASC')
                    ->orderBy('timestamp', 'DESC')
                    ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
                $banner = [];
                $data['banner'] = [];
                if (!empty($banners)) {
                    foreach ($banners as $key => $value) {
                        $banner[$value['banner_id']]['banner_id'] = $value['banner_id'];
                        $banner[$value['banner_id']]['banner'] = $value['banner'];
                        $banner[$value['banner_id']]['url'] = trim($value['url']);
                        $banner[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];

                    }
                    $data['banner'] = array_values($banner);
                }
                $data['data_blocks'] = [];
                $data_block = config('constants.category_giadung');
                foreach ($data_block as $key => $value) {
                    $product_id = $value['id'];
                    $key_cate = "BLOCK_".$product_id;
                    $datas = $value;
                    if(RedisClient::existsKey($key_cate)){
                        $datas['data'] = RedisClient::getValue($key_cate);
                    }else{
                        $datas['data'] = [];
                    }
                    $data['data_blocks'][] = $datas;
                }
                RedisClient::setValue($key_home, $data, 720000000);
            }

            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }


}
