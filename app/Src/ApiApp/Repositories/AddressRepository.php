<?php
/**
 * Created by PhpStorm.
 * User: Anh.NguyenVan
 * Date: 7/12/2018
 * Time: 5:12 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class AddressRepository extends RepositoryBase
{

    public function getStates($request)
    {
        $outputs = array();
        try {
            $limit = $request->input('limit', 10);
            $offset = ($request->input('page')) ? $request->input('page') : 1;
            $start = ($offset - 1) * $limit;
            $total_row = DB::connection('mongodb')->collection('nk_states')->get();
            $states = DB::connection('mongodb')->collection('nk_states')->orderBy('ordering', 'ASC')->skip((int)$start)->take((int)$limit)->get();
            if(!empty($states)){
                foreach ($states as $state) {
                    $outputs[] = array(
                        'state_id' =>  $state['code'] ,
                        'code' => $state['code'],
                        'state' => $state['state']

                    );
                }
            }
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $outputs,
                'page_info' => array(
                    'total_row' => count($total_row),
                    'current_page' => $offset
                ),
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getWards($request)
    {
        $outputs = array();
        try {
            $limit = $request->input('limit', 10);
            $offset = ($request->input('page')) ? $request->input('page') : 1;
            $start = ($offset - 1) * $limit;
            $outputs = [];
            $total_wards = DB::connection('mongodb')->collection('nk_wards')->get();
            $wards = DB::connection('mongodb')->collection('nk_wards')->skip((int)$start)->take((int)$limit)->get();
            if (!empty($wards)) {
                foreach ($wards as $ward) {
                    $outputs[] = array(
                        'ward_id' => $ward['code'],
                        'district_id' => $ward['district_code'],
                        'state_code' => $ward['state_code'],
                        'district_code' => $ward['district_code'],
                        'code' => $ward['code'],
                        'name' => $ward['name']
                    );
                }
            }
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $outputs,
                'page_info' => array(
                    'total_row' => count($total_wards),
                    'current_page' => $offset
                ),
            );


        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getDistricts($request)
    {
        $outputs = array();
        try {
            $limit = $request->input('limit', 10);
            $offset = ($request->input('page')) ? $request->input('page') : 1;
            $start = ($offset - 1) * $limit;

            $districts = DB::connection('mongodb')->collection('nk_districts')->skip((int)$start)->take((int)$limit)->get();
            $total_districts = DB::connection('mongodb')->collection('nk_districts')->get();

            if(!empty($districts)){
                foreach ($districts as $district) {
                    $outputs[] = array(
                        'district_id' => $district['code'],
                        'state_code' => $district['state_code'],
                        'code' => $district['code'],
                        'name' => $district['name']

                    );
                }
            }

            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $outputs,
                'page_info' => array(
                    'total_row' => count($total_districts),
                    'current_page' => $offset
                ),
            );

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getDistrictsByState($request, $state_code)
    {
        $outputs = array();
        try {
            if (!empty($state_code)) {
                $districts = DB::connection('mongodb')->collection('nk_districts')->where('state_code', $state_code)->orderBy('position','ASC')->get();
                if (!empty($districts)) {
                    foreach ($districts as $district) {
                        $outputs[] = array(
                            'district_id' => $district['code'],
                            'state_code' => $district['state_code'],
                            'code' => $district['code'],
                            'name' => $district['name']
                        );
                    }
                    if (count($outputs) > 0) {
                        return array(
                            'status' => 200,
                            'message' => 'Thành công',
                            'data' => $outputs
                        );
                    } else {
                        echo 'Empty';
                    }

                } else {
                    echo 'Empty';
                }
            } else {
                echo 'State code not correct!';
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getWardsByDistrict($request, $district_id, $state_id)
    {
        $outputs = array();
        try {
            if (!empty($district_id)) {
                $wards = DB::connection('mongodb')->collection('nk_wards')->where('district_code', $district_id)->where('state_code', $state_id)->orderBy('position','ASC')->get();
                if (!empty($wards)) {
                    foreach ($wards as $ward) {
                        $outputs[] = array(
                            'ward_id' => $ward['state_code'],
                            'district_id' => $ward['district_code'],
                            'state_code' => $ward['state_code'],
                            'district_code' => $ward['district_code'],
                            'code' => $ward['code'],
                            'name' => $ward['name']
                        );
                    }
                    if (count($outputs) > 0) {
                        return array(
                            'status' => 200,
                            'message' => 'Thành công',
                            'data' => $outputs
                        );
                    } else {
                        echo 'Empty';
                    }

                } else {
                    echo 'Empty';
                }
            } else {
                echo 'District id not correct!';
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    
}