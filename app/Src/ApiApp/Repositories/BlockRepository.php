<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/20/2018
 * Time: 3:13 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Cache\RedisClient;
use DB;
use Log;

class BlockRepository extends RepositoryBase
{
    protected $model;
    protected $helper;
    protected $config;
    public function __construct(Helpers $helper)
    {
        $this->helper = $helper;
        $this->model = new Products();
    }
    public function all($request)
    {
        $limit = $request->input('limit', 1000);
        $offset     = ($request->input('page')) ? $request->input('page') : 1;
        $start = ($offset - 1) * $limit;
        $data = DB::connection('mongodb')->collection('products')->project(['categories' => ['$slice' => 421]])->skip($start)->take($limit)
            ->get([
                'product_id'
                ,'description'
                ,'product_code'
                ,'amount'
                ,'weight'
                ,'length'
                ,'width'
                ,'height'
                ,'list_price'
                ,'price'
                ,'product_name'
                ,'display_name'
                ,'model'
                ,'brand'
                ,'images'
                ,'categories'
            ]);
        $data_premium = [];
        $total = count($data);
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => [
                'premium ' =>$data_premium,
                'brand' =>[],
                'header' =>[
                    'id' => 423,
                    'alias' =>'may-giat',
                    'name' => 'Tủ lạnh',
                    'description' => 'Danh sách Máy Giặt chính hãng giá tốt'
                ],
                'filter' => [],
                'sort_by' => [
                    '1' => 'Bán chạy nhất',
                    '2' => 'Giá thấp đến cao',
                    '3' => 'Giá cao xuống thấp'
                ],
                'banner' =>[],
                'product' => $data,
                'page_info' =>[
                    'total'=> $total,
                    'page_curent' => 1
                ]
            ],
        );

    }
    public function show($request,$id)
    {
        $product_id = intval($id);
        try {
            $datas =  DB::connection('mongodb')->collection('products')->where('product_id', $product_id)//->first();
            ->get([
                'product_id'
                ,'description'
                ,'product_code'
                ,'amount'
                ,'weight'
                ,'length'
                ,'width'
                ,'height'
                ,'list_price'
                ,'price'
                ,'product_name'
                ,'display_name'
                ,'model'
                ,'brand'
                ,'images'
                ,'variants'
                ,'product_options'
                ,'product_descriptions'
            ]);
            $total = count($datas);
            if($total == 0){
                return array(
                    'status' => 400,
                    'message' => 'Sản phẩm không tồn tại',
                    'data' => []
                );
            }
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $datas
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function home($request,$data_block){
        try {
            $key_home = "REDIS_HOME";
            if(RedisClient::existsKey($key_home)){
                $data = RedisClient::getValue($key_home);
            }else {
                $time = time();
                $area_id = $request->input('area_id', 18001);
                $data['tab_bigbang'] = array(
                    '0' => array(
                        'name' => 'BigBang 2018',
                        'url' => 'https://www.nguyenkim.com/big-bang.html',
                        'default' => 1,
                        'active' => 'https://www.nguyenkim.com/images/companies/_1/bigbang/BigBang.png',
                        'deactive' => 'https://www.nguyenkim.com/images/companies/_1/bigbang/BigBang-1.png',
                    ),
                    '1' => array(
                        'name' => 'Trang chủ',
                        'url' => 'https://www.nguyenkim.com/khuyen-mai.html',
                        'default' => 0,
                        'active' => 'https://www.nguyenkim.com/images/companies/_1/bigbang/BigBang-3.png',
                        'deactive' => 'https://www.nguyenkim.com/images/companies/_1/bigbang/BigBang-2.png',
                    )
                );
                $data['tab_bigbang'] = [];
                $data['text_price_online'] = array(
                    '0' => array(
                        'icon' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T07/icon/hopqua_33x33.gif',
                        'name' => 'Mua online ưu đãi hơn',
                        'alias' => ''
                    ),
                    '1' => array(
                        'name' => 'Xem ngay',
                        //'alias' => 'https://www.nguyenkim.com/mua-online-uu-dai-hon.html'
                        'alias' => 'https://www.nguyenkim.com/mua-online-uu-dai-hon.html'
                    )
                );
                $banner_price_online = [];
                $banner_price_online = DB::connection('mongodb')->collection('banners')
                    ->where('type_display', 1)
                    ->where('time_start', '<', $time)
                    ->where('time_finish', '>', $time)
                    ->where('status', 'A')
                    ->whereIn('banner_areas', [(int)$area_id])
                    ->orderBy('position', 'ASC')
                    ->orderBy('timestamp', 'DESC')
                    ->limit(7)
                    ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);

                $banner_online = [];
                $data['banner_price_online'] = [];
                //        if(!empty($banner_price_online)){
                //            foreach($banner_price_online as $key => $value){
                //                $banner_online[$value['banner_id']]['_id'] = $value['_id'];
                //                $banner_online[$value['banner_id']]['banner_id'] = $value['banner_id'];
                //                $banner_online[$value['banner_id']]['banner'] = $value['banner'];
                //                $banner_online[$value['banner_id']]['url'] = trim($value['url']);
                //                $banner_online[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
                //                $banner_online[$value['banner_id']]['time_start'] = $value['time_start'];
                //                $banner_online[$value['banner_id']]['time_finish'] = $value['time_finish'];
                //                $banner_online[$value['banner_id']]['position'] = $value['position'];
                //                $banner_online[$value['banner_id']]['grid_position'] = $value['grid_position'];
                //                $banner_online[$value['banner_id']]['timestamp'] = $value['timestamp'];
                //            }
                //            if(!empty($banner_online)){
                //                $data['banner_price_online'] = $this->process_data_banner(array_values($banner_online));
                //            }
                //        }
                $data['banner_price_online'] = array(
                    0 =>
                        array(
                            'banner_id' => 4967,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/happy-weekend-gia-soc-cuoi-tuan.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/thumbnails/519/promo/542/768X298-H-WEEKEND16.jpg',
                        ),
                    1 =>
                        array(
                            'banner_id' => 1,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/galaxy-a20a30a50-sieu-pham-cuc-hot.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/thumbnails/519/promo/510/768x298-Samsung.jpg',
                        ),
                    2 =>
                        array(
                            'banner_id' => 2,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/flash-sale-may-giat-samsung.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/thumbnails/519/promo/540/home-mobile-FS-MG-SS.jpg',
                        ),
                    3 =>
                        array(
                            'banner_id' => 3,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/dien-thoai-oppo-f11.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/thumbnails/519/promo/558/768x298-OppF11_6wzk-pw.jpg',
                        ),
                    4 =>
                        array(
                            'banner_id' => 4,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/dien-thoai-samsung-galaxy-s10-plus.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/thumbnails/519/promo/553/768x298-Samsung-thang4-S10-Plus.jpg',
                        ),
                    5 =>
                        array(
                            'banner_id' => 5,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/lg-dual-cool-inverter.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/thumbnails/519/promo/522/maylanh-lg-thang03-768x298_4wr8-kx.jpg',
                        ),
                    6 =>
                        array(
                            'banner_id' => 6,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/dat-som-tivi-samsung-uhd-rinh-qua-den-5-trieu.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/thumbnails/519/promo/563/Samsung---UHD-RU7400---Banner--768x298_dr2w-lo.jpg',
                        )
                );
                $data['memu_icon_home'] = config('constants.menu_icon_home');
                $data['promotion_hot'] = array(
                    '0' => array(
                        'icon' => '\E92C',
                        'name' => 'Khuyến mãi HOT',
                        'alias' => 'https://www.nguyenkim.com/khuyen-mai.html'
                    )
                    /*,
                    '1' => array(
                        'name' => 'Hãy để Nguyễn Kim tư vấn sản phẩm cho bạn',
                        'alias' => 'mua-online-uu-dai-hon.html'
                    )*/
                );
                $data['promotion_hot'] = [];
                $banner_promotion_hots = DB::connection('mongodb')->collection('banners')
                    ->where('type_display', 2)
                    ->where('position_floor_banner', 19)
                    ->where('time_start', '<', $time)
                    ->where('time_finish', '>', $time)
                    ->where('status', 'A')
                    ->whereIn('banner_areas', [(int)$area_id])
                    ->orderBy('position', 'ASC')
                    ->orderBy('timestamp', 'DESC')
                    ->limit(3)
                    ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
                $data['banner_promotion_hot'] = [];
                $banner_promotion_hot = [];
                if (!empty($banner_promotion_hots)) {
                    foreach ($banner_promotion_hots as $key => $value) {
                        $banner_promotion_hot[$value['banner_id']]['_id'] = $value['_id'];
                        $banner_promotion_hot[$value['banner_id']]['banner_id'] = $value['banner_id'];
                        $banner_promotion_hot[$value['banner_id']]['banner'] = $value['banner'];
                        $banner_promotion_hot[$value['banner_id']]['url'] = trim($value['url']);
                        $banner_promotion_hot[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
                        $banner_promotion_hot[$value['banner_id']]['time_start'] = $value['time_start'];
                        $banner_promotion_hot[$value['banner_id']]['time_finish'] = $value['time_finish'];
                        $banner_promotion_hot[$value['banner_id']]['position'] = $value['position'];
                        $banner_promotion_hot[$value['banner_id']]['grid_position'] = $value['grid_position'];
                        $banner_promotion_hot[$value['banner_id']]['timestamp'] = $value['timestamp'];
                    }
                    $data['banner_promotion_hot'] = array_values($banner_promotion_hot);
                }

                /*$data['banner_promotion_hot'] = array (
                    0 =>
                        array (
                            'banner_id' => 4967,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/zalopay.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/promo/540/Hot-Promotion528x205-zalopay_y6v2-nf.jpg',
                        ),
                    1 =>
                        array (
                            'banner_id' => 4835,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/giam-den-1-trieu-dong-cung-napas-tai-nguyen-kim-bo-lo-tiec-hui-hui.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/promo/529/Napas_528x205.jpg',
                        ),
                    2 =>
                        array (
                            'banner_id' => 4724,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/giao-hang-mien-phi-toan-quoc.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/promo/523/GiaoHangTanTay_KhongLoVePhi_528x205.jpg',
                        ),
                    3 =>
                        array (
                            'banner_id' => 4597,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/dien-thoai-iphone-6.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/promo/519/Homepage-Hot-Promotion-ip6_5epz-oz.jpg',
                        ),
                    4 =>
                        array (
                            'banner_id' => 4583,
                            'banner' => 0,
                            'url' => 'https://www.nguyenkim.com/iphone-6s-plus-32gb-gold.html',
                            'nk_banner_ext' => 'https://cdn.nguyenkimmall.com/images/promo/519/Homepage-Hot-Promotion-iPhone6Splus-a.jpg',
                        )
                );*/

                // tầng wc
                /*$data['wc']['banner'] =[
                    'image' => 'https://www.nguyenkim.com/images/companies/_1/img/mobile_banner-WC-2018.png',
                    'url' => 'https://www.nguyenkim.com/mua-tivi-online-uu-dai-hon-vi.html'
                ];
                $data['wc']['background'] = "https://beta.nguyenkim.com/images/companies/_1/html/2018/T07/congchao/TLV-WelcomeGate-Tablet%2BMobile-Bg.jpg";

                $data['wc']['product_list'] = [];
                $list_pwc = [51366,54166,62801,53272,60819];
                $data_products11s = DB::connection('mongodb')->collection('products_full_mode_new')->whereIn('product_id', $list_pwc)->get([
                    'product_id'
                    ,'mobile_short_description'
                    ,'product_code'
                    ,'amount'
                    ,'weight'
                    ,'length'
                    ,'width'
                    ,'height'
                    ,'list_price'
                    ,'price'
                    ,'product'
                    ,'nk_shortname'
                    ,'main_pair'
                    ,'category_ids'
                    ,'nk_tragop_0'
                    ,'product_options'
                    ,'tag_image'
                    ,'nk_is_shock_price'
                    ,'product_features'
                    ,'nk_product_tags'
                    ,'text_shock'
                    ,'shock_online_exp'
                    ,'nk_special_tag'
                    ,'offline_price'
                    ,'model'
                ]);
                $datas11 = [];
                foreach ($data_products11s as $data_products11){
                    $data11['bg_header'] = 'https://beta.nguyenkim.com/images/companies/_1/html/2018/T07/congchao/box1-2.png';
                    $data11['bg_footer'] = 'https://beta.nguyenkim.com/images/companies/_1/html/2018/T07/congchao/box2_1_2.png';
                    $data11['line_color'] = ['#ff0', '#ff6e02'];
                    $data11['percent'] = 14;
                    $data11['title_deal'] = 'Deal còn lại 87%';
                    $data_products['text_promotion']=[];
                    $data11['images'] = '';
                    $data11['tag_image_top_left'] = '';
                    $data11['tag_image_top_right'] = '';
                    $data11['tag_image_bottom_left'] = '';
                    $data11['tag_image_bottom_right'] = '';
                    $data11['model'] = '';
                    if(!empty($data_products11['nk_product_tags'])){
                        foreach ($data_products11['nk_product_tags'] as $key => $val){
                            if($val['tag_position'] == 'bottom-right'){
                                $data11['tag_image_bottom_right'] = $val['tag_image'];
                            }elseif($val['tag_position'] == 'bottom-left'){
                                $data11['tag_image_bottom_left'] = $val['tag_image'];
                            }elseif($val['tag_position'] == 'top-right'){
                                $data11['tag_image_top_right'] = $val['tag_image'];
                            }elseif($val['tag_position'] == 'top-left'){
                                $data11['tag_image_top_left'] = $val['tag_image'];
                            }
                        }
                    }
                    if($data_products11){
                        if(!empty($data_products11['product_options'])){
                            foreach ($data_products11['product_options'] as $key => $val){
                                $data11['text_promotion'] = $val['nk_uu_dai_len_den'];
                                break;
                            }
                        }
                        $thumb = $data_products11['main_pair'];
                        $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
                        $data11['images'] = 'https://www.nguyenkim.com/images/thumbnails/290/235/'. $image['relative_path'];


                        if(!empty($data_products11['product_features'])){
                            if(!empty($data_products11['product_features']['591'])){
                                if(!empty($data_products11['product_features']['591']['subfeatures'])){
                                    if(!empty($data_products11['product_features']['591']['subfeatures']['599'])){
                                        $data_variant = $data_products11['product_features']['591']['subfeatures']['599'];
                                        if(!empty($data_variant['variants'])){
                                            foreach ($data_variant['variants'] as $item){
                                                $data11['brand'] = $item['variant'];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(!empty($data_products11['product_features'])){
                            if(!empty($data_products11['product_features']['591'])){
                                if(!empty($data_products11['product_features']['591']['subfeatures'])){
                                    if(!empty($data_products11['product_features']['591']['subfeatures']['596'])){
                                        $data_variant = $data_products11['product_features']['591']['subfeatures']['596'];
                                        if(!empty($data_variant['variants'])){
                                            foreach ($data_variant['variants'] as $item){
                                                $data11['model'] = $item['variant'];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $data11['product_id'] = $data_products11['product_id'];
                    $data11['description'] = $data_products11['mobile_short_description'];
                    $data11['product_code'] = $data_products11['product_code'];
                    $data11['amount'] = $data_products11['amount'];
                    $data11['weight'] = $data_products11['weight'];
                    $data11['list_price'] = (int)$data_products11['list_price'];
                    $data11['price'] = (int)$data_products11['price'];
                    $data11['offline_price'] = (int)$data_products11['offline_price'];
                    $data11['product_name'] = $data_products11['product'];
                    $data11['shortname'] = $data_products11['nk_shortname'];
                    $data11['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                    $data11['categories'] = $data_products11['category_ids'];
                    $data11['nk_tragop_0'] = $data_products11['nk_tragop_0'];
                    $data11['length'] = $data_products11['length'];
                    $data11['width'] = $data_products11['width'];
                    $data11['height'] = $data_products11['height'];
                    $data11['nk_is_shock_price'] = $data_products11['nk_is_shock_price'];
                    $data11['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%402x.png';
                    $data11['text_shock'] = $data_products11['text_shock'];
                    $data11['shock_online_exp'] = $data_products11['shock_online_exp'];
                    $data11['nk_special_tag'] = $data_products11['nk_special_tag'];
                    $data11['nk_product_tags'] = $data_products11['nk_product_tags'];
                    $data11['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                    $datas11[] =  $data11;
                }
                $data['wc']['product_list'] = $datas11;
                */
                $data['wc'] = [];
                $data['data_blocks'] = [];
                $datas['banner_block_top'] = [];
                foreach ($data_block as $key => $value) {
                    $product_ids = $value['products'];
                    $position_display = $value['position_display'];
                    if (!empty($product_ids)) {
                        $datas1 = [];
                        foreach ($product_ids as $key1 => $value1) {
                            $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', (int)$value1)->get([
                                'product_id'
                                , 'mobile_short_description'
                                , 'product_code'
                                , 'amount'
                                , 'weight'
                                , 'length'
                                , 'width'
                                , 'height'
                                , 'list_price'
                                , 'price'
                                , 'product'
                                , 'nk_shortname'
                                , 'main_pair'
                                , 'category_ids'
                                , 'nk_tragop_0'
                                , 'product_options'
                                , 'tag_image'
                                , 'nk_is_shock_price'
                                , 'product_features'
                                , 'nk_product_tags'
                                , 'text_shock'
                                , 'shock_online_exp'
                                , 'nk_special_tag'
                                , 'offline_price'
                                , 'model'
                                , 'status'
                                , 'hidden_option'
                                , 'nk_show_two_price'
                                , 'nk_discount_label'
                                , 'tag_image_special'
                                , 'tag_start_time_special'
                                , 'tag_end_time_special'
                            ]);

                            foreach ($data_products1s as $data_products1) {
                                $data1['status'] = $data_products1['status'];
                                $data1['hidden_option'] = $data_products1['hidden_option'];
                                $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                                $data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                                $data1['discount_percent'] = 0;
                                if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                    $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                                }

                                $data_products['text_promotion'] = [];
                                $data1['images'] = '';
                                $data1['tag_image_top_left'] = '';
                                $data1['tag_image_top_right'] = '';
                                $data1['tag_image_bottom_left'] = '';
                                $data1['tag_image_bottom_right'] = '';
                                $data1['model'] = '';
                                $data1['brand'] = '';
                                if (!empty($data_products1['nk_product_tags'])) {
                                    foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                        if ($val['tag_position'] == 'bottom-right') {
                                            $data1['tag_image_bottom_right'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'bottom-left') {
                                            $data1['tag_image_bottom_left'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'top-right') {
                                            $data1['tag_image_top_right'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'top-left') {
                                            $data1['tag_image_top_left'] = $val['tag_image'];
                                        }
                                    }
                                }
                                if ($data_products1) {
                                    if (!empty($data_products1['product_options'])) {
                                        foreach ($data_products1['product_options'] as $key => $val) {
                                            if (trim($val['nk_uu_dai_len_den']) != '') {
                                                $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                                break;
                                            }
                                        }
                                    }

                                    $thumb = $data_products1['main_pair'];
                                    $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
                                    $data1['images'] = 'https://www.nguyenkim.com/images/thumbnails/190/190/' . $image['relative_path'];
                                    if (trim($data_products1['tag_image_special']) != '') {
                                        $time = time();
                                        if ($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']) {
                                            $data1['images'] = $data_products1['tag_image_special'];
                                        }
                                    }


                                    if (!empty($data_products1['product_features'])) {
                                        if (!empty($data_products1['product_features']['591'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                                if (!empty($data_products1['product_features']['591']['subfeatures']['599'])) {
                                                    $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                    if (!empty($data_variant['variants'])) {
                                                        foreach ($data_variant['variants'] as $item) {
                                                            $data1['brand'] = $item['variant'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!empty($data_products1['product_features'])) {
                                        if (!empty($data_products1['product_features']['591'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                                if (!empty($data_products1['product_features']['591']['subfeatures']['596'])) {
                                                    $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                                    if (!empty($data_variant['variants'])) {
                                                        foreach ($data_variant['variants'] as $item) {
                                                            $data1['model'] = $item['variant'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                $data1['product_id'] = $data_products1['product_id'];
                                $data1['description'] = $data_products1['mobile_short_description'];
                                $data1['product_code'] = $data_products1['product_code'];
                                $data1['amount'] = $data_products1['amount'];
                                $data1['weight'] = $data_products1['weight'];
                                $data1['list_price'] = (int)$data_products1['list_price'];
                                if ($data_products1['nk_show_two_price'] == 'N' && ($data_products1['nk_is_shock_price'] != 'Y' && $data_products1['nk_is_shock_price'] != 'O')) {
                                    $data1['list_price'] = (int)$data_products1['price'];
                                }
                                $data1['price'] = (int)$data_products1['price'];
                                $data1['offline_price'] = (int)$data_products1['offline_price'];
                                $data1['product_name'] = $data_products1['product'];
                                $data1['shortname'] = $data_products1['nk_shortname'];
                                $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                                $data1['categories'] = $data_products1['category_ids'];
                                $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                                $data1['length'] = $data_products1['length'];
                                $data1['width'] = $data_products1['width'];
                                $data1['height'] = $data_products1['height'];
                                $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                                $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%402x.png';
                                $data1['text_shock'] = $data_products1['text_shock'];
                                $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                                $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                                // fix hiện giá rẻ online khỏi sửa bên app
                                if (isset($data_products1['text_shock']) && !empty($data_products1['text_shock'])) {
                                    $data1['nk_special_tag'] = 1;
                                }
                                $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                                $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                                $data1['text_tragop'] = '';
                                //if(($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] =='O'){
                                if($data_products1['nk_tragop_0'] == 1)
                                    $data1['text_tragop'] = get_text_tra_gop($data_products1['price']);
                                //}
                                $datas1[] = $data1;
                            }

                        }
                    }

                    $datas = $value;
                    $datas['product_list_details'] = $datas1;
                    // banner
                    $banners = DB::connection('mongodb')->collection('banners')
                        ->where('type_display', 2)
                        ->where('position_floor_banner', $position_display)
                        ->where('time_start', '<', $time)
                        ->where('time_finish', '>', $time)
                        ->where('status', 'A')
                        ->whereIn('banner_areas', [(int)$area_id])
                        ->orderBy('position', 'ASC')
                        ->orderBy('timestamp', 'DESC')
                        ->limit(3)
                        ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
                    $banner = [];
                    $datas['banner_block'] = [];
                    if (!empty($banners)) {
                        foreach ($banners as $key => $value) {
                            $banner[$value['banner_id']]['_id'] = $value['_id'];
                            $banner[$value['banner_id']]['banner_id'] = $value['banner_id'];
                            $banner[$value['banner_id']]['banner'] = $value['banner'];
                            $banner[$value['banner_id']]['url'] = trim($value['url']);
                            $banner[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
                            $banner[$value['banner_id']]['time_start'] = $value['time_start'];
                            $banner[$value['banner_id']]['time_finish'] = $value['time_finish'];
                            $banner[$value['banner_id']]['position'] = $value['position'];
                            $banner[$value['banner_id']]['grid_position'] = $value['grid_position'];
                            $banner[$value['banner_id']]['timestamp'] = $value['timestamp'];
                        }
                        $datas['banner_block'] = array_values($banner);
                    }
                    //$banner_block_id = $value['banner_block_id'];
                    $datas['banner_block_top'] = [];
                    if (!empty($banner_block_id)) {
                        $banner_block_tops = DB::connection('mongodb')->collection('banners')
                            ->where('position_floor', $banner_block_id)
                            ->where('type_display', 2)
                            ->where('time_start', '<', $time)
                            ->where('time_finish', '>', $time)
                            ->where('status', 'A')
                            ->whereIn('banner_areas', [(int)$area_id])
                            ->orderBy('position', 'ASC')
                            ->orderBy('timestamp', 'DESC')
                            ->limit(3)
                            ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
                        $banners_block_top = [];
                        if (!empty($banner_block_tops)) {
                            foreach ($banner_block_tops as $key => $value) {
                                $banners_block_top[$value['banner_id']]['_id'] = $value['_id'];
                                $banners_block_top[$value['banner_id']]['banner_id'] = $value['banner_id'];
                                $banners_block_top[$value['banner_id']]['banner'] = $value['banner'];
                                $banners_block_top[$value['banner_id']]['url'] = trim($value['url']);
                                $banners_block_top[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
                                $banners_block_top[$value['banner_id']]['time_start'] = $value['time_start'];
                                $banners_block_top[$value['banner_id']]['time_finish'] = $value['time_finish'];
                                $banners_block_top[$value['banner_id']]['position'] = $value['position'];
                                $banners_block_top[$value['banner_id']]['grid_position'] = $value['grid_position'];
                                $banners_block_top[$value['banner_id']]['timestamp'] = $value['timestamp'];
                            }
                            $datas['banner_block_top'] = array_values($banners_block_top);
                        }
                    }
                    $data['data_blocks'][] = $datas;

                }
                RedisClient::setValue($key_home, $data);
            }
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function home_v2($request,$data_block){
        try {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            list($first, $host) = explode(".", $_SERVER["SERVER_NAME"]);
            if ($first == 'dev' || $first == 'dev1' || $first == 'dev2' || $first == 'test') {
                $url= 'https://test.nguyenkimonline.com/';
            } elseif ($first == 'beta') {
                $url = 'https://test.nguyenkimonline.com/';
            } else {
                $url = 'https://www.nguyenkim.com/';
            }
            $time = time();
            $key_home = "REDIS_HOME";
           if(RedisClient::existsKey($key_home)){
               $data = RedisClient::getValue($key_home);
           }else {
                $area_id = $request->input('area_id', 18001);
                $data['tab_bigbang'] = array(
                    '0' => array(
                        'name' => 'BigBang 2018',
                        'url' => 'https://www.nguyenkim.com/nguyen-kim-khuay-dao-cuoi-nam.html',
                        'default' => 1,
                        'active' => 'https://www.nguyenkim.com/images/companies/_1/img/home2.png',
                        'deactive' => 'https://www.nguyenkim.com/images/companies/_1/img/home1.png',
                    ),
                    '1' => array(
                        'name' => 'Trang chủ',
                        'url' => 'https://www.nguyenkim.com/khuyen-mai.html',
                        'default' => 0,
                        'active' => 'https://www.nguyenkim.com/images/companies/_1/img/BigBang2.png',
                        'deactive' => 'https://www.nguyenkim.com/images/companies/_1/img/BigBang1.png',
                    )
                );
                //$data['tab_bigbang'] = [];
                // check text_price_online
                if(config('constants.check_text_price_online') == 1){
                    $data['check_text_price_online'] = 1;
                    $data['text_price_online'] = array(
                        '0' => array(
                            'icon' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T07/icon/hopqua_33x33.gif',
                            'name' => 'Mua online ưu đãi hơn',
                            'alias' => ''
                        ),
                        '1' => array(
                            'name' => 'Xem ngay',
                            //'alias' => 'https://www.nguyenkim.com/mua-online-uu-dai-hon.html'
                            'alias' => 'https://www.nguyenkim.com/mua-online-uu-dai-hon.html'
                        )
                    );
                }else{
                    $data['check_text_price_online'] = 0;
                    $data['text_price_online'] = [];
                }
                if(config('constants.check_text_price_online') == 1){
                    $data['check_slide_menu'] = 1;
                    $data['slide_menu'] = config('constants.slide_menu');
                }else{
                    $data['check_slide_menu'] = 0;
                    $data['slide_menu'] = [];
                }

                $banner_price_online = [];
                $banner_price_online = DB::connection('mongodb')->collection('banners')
                    ->where('type_display', 1)
                    ->where('time_start', '<', (int)$time)
                    ->where('time_finish', '>', (int)$time)
                    ->where('status', 'A')
                    ->whereIn('banner_areas', [(int)$area_id])
                    ->orderBy('position', 'ASC')
                    ->orderBy('timestamp', 'DESC')
                    ->limit(7)
                    ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp','bg_image']);

                $banner_online = [];
                $data['banner_price_online'] = [];
                if(!empty($banner_price_online)){
                    foreach($banner_price_online as $key => $value){
                        $banner_online['_id'] = $value['_id'];
                        $banner_online['banner_id'] = $value['banner_id'];
                        $banner_online['banner'] = $value['banner'];
                        $banner_online['url'] = trim($value['url']);
                        $banner_online['nk_banner_ext'] = $value['bg_image'];
                        $banner_online['time_start'] = $value['time_start'];
                        $banner_online['time_finish'] = $value['time_finish'];
                        $banner_online['position'] = $value['position'];
                        $banner_online['grid_position'] = $value['grid_position'];
                        $banner_online['timestamp'] = $value['timestamp'];
                        $data['banner_price_online'][] = $banner_online;
                    }
                }
                if(config('constants.check_news') == 1){
                    $data['news'] = [];
                    $data['check_news'] = 0;
                    $data_news = DB::connection('mongodb')->collection('blog')->limit(4)->get(['post_id', 'name', 'seo_name','images','author','date']);
                    if(!empty($data_news)){
                        $datas_new = [];
                        foreach($data_news as $key => $value){
                            $datas_news['id'] = $value['post_id'];
                            $datas_news['name'] = $value['name'];
                            $datas_news['alias'] = 'https://www.nguyenkim.com/'.$value['seo_name'].'.html';
                            $datas_news['images'] = $value['images'];
                            $datas_news['author'] = $value['author'];
                            $datas_news['date'] = $value['date'];
                            $datas_news['category'] = 'Kinh nghiệm hay';
                            $data['news'][] = $datas_news;
                        }
                    }
                }else{
                    $data['check_news'] = 0;
                    $data['news'] = [];
                }
                if(config('constants.check_menu_icon_home_v2') == 1){
                    $data['check_memu_icon_home'] = 1;
                    $data['memu_icon_home'] = config('constants.menu_icon_home_v2');
                }else{
                    $data['check_memu_icon_home'] = 0;
                    $data['memu_icon_home'] = [];
                }
                if(config('constants.check_menu_icon_home_top') == 1){
                    $data['check_menu_icon_home_top'] = 1;
                    $data['memu_icon_home_top'] = config('constants.menu_icon_home_top');
                }else{
                    $data['check_menu_icon_home_top'] = 0;
                    $data['memu_icon_home_top'] = [];
                }
                $banner_promotion_hots = DB::connection('mongodb')->collection('banners')
                    ->where('type_display', 2)
                    ->where('position_floor_banner', 19)
                    ->where('time_start', '<', $time)
                    ->where('time_finish', '>', $time)
                    ->where('status', 'A')
                    ->whereIn('banner_areas', [(int)$area_id])
                    ->orderBy('position', 'ASC')
                    ->orderBy('timestamp', 'DESC')
                    ->limit(3)
                    ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
                $data['banner_promotion_hot'] = [];
                $banner_promotion_hot = [];
                if (!empty($banner_promotion_hots)) {
                    foreach ($banner_promotion_hots as $key => $value) {
                        $banner_promotion_hot[$value['banner_id']]['_id'] = $value['_id'];
                        $banner_promotion_hot[$value['banner_id']]['banner_id'] = $value['banner_id'];
                        $banner_promotion_hot[$value['banner_id']]['banner'] = $value['banner'];
                        $banner_promotion_hot[$value['banner_id']]['url'] = trim($value['url']);
                        $banner_promotion_hot[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
                        $banner_promotion_hot[$value['banner_id']]['time_start'] = $value['time_start'];
                        $banner_promotion_hot[$value['banner_id']]['time_finish'] = $value['time_finish'];
                        $banner_promotion_hot[$value['banner_id']]['position'] = $value['position'];
                        $banner_promotion_hot[$value['banner_id']]['grid_position'] = $value['grid_position'];
                        $banner_promotion_hot[$value['banner_id']]['timestamp'] = $value['timestamp'];
                    }
                    $data['banner_promotion_hot'] = array_values($banner_promotion_hot);
                }
                $banner_price_online = [];
                $banner_promotion_hot_bottom = DB::connection('mongodb')->collection('pages')->get(['page_id', 'redirect_url', 'list_img_url']);
                $data['banner_promotion_hot_bottom'] = [];
                if(!empty($banner_promotion_hot_bottom)){
                    foreach($banner_promotion_hot_bottom as $key => $value){
                        $banner_promotion_hot_bottoms['banner_id'] = $value['page_id'];
                        $banner_promotion_hot_bottoms['banner'] = 0;
                        $banner_promotion_hot_bottoms['url'] = $value['redirect_url'];
                        $banner_promotion_hot_bottoms['nk_banner_ext'] = $value['list_img_url'];
                        $data['banner_promotion_hot_bottom'][] = $banner_promotion_hot_bottoms;
                    }
                }
                $data['data_blocks'] = [];
                $datas['banner_block_top'] = [];
                $data_home_v2 = config('constants.config_block_home_v2');
                $data['name_top'] = $data_home_v2['name'];
                $data['name_center'] = $data_home_v2['name1'];
                $data['name_bottom'] = $data_home_v2['name2'];
                $data['wc'] = [];
                $data['data_blocks'] = [];
                $datas['banner_block_top'] = [];
                $home_page = DB::connection('mongodb')->collection('home_page')->where('home_page',(int)$data_home_v2['location'])->get(['datas']);
                if(!empty($home_page)){
                    $product_data = json_decode($home_page[0]['datas'],true);

                }
                foreach ($data_block as $key => $value) {
                    if(!empty($product_data)){
                        $product_ids1 = isset($product_data[$value['id']]) ? $product_data[$value['id']] : [];
                        $arr_id = [];
                        if(count($product_ids1) > 0){
                            foreach($product_ids1 as $k => $v){
                                array_push($arr_id,(int)$v);
                            }
                        }
                        $product_ids = [];
                        if(!empty($arr_id)){
                            $product_ids = array_values($arr_id);
                        }
                    }
                    $product_ids_more = $value['products_view_more'];
                    $position_display = $value['position_floor'];
                    if (!empty($product_ids1)) {
                        $key_block = 'BLOCK_'.$value['id'];
                        if(RedisClient::existsKey($key_block)){
                            $data_products1s = RedisClient::getValue($key_block);
                        }else{
                            $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->whereIn('product_id', $product_ids)->where('status','A')->get([
                                'product_id'
                                , 'mobile_short_description'
                                , 'product_code'
                                , 'amount'
                                , 'weight'
                                , 'length'
                                , 'width'
                                , 'height'
                                , 'list_price'
                                , 'price'
                                , 'product'
                                , 'nk_shortname'
                                , 'main_pair'
                                , 'category_ids'
                                , 'nk_tragop_0'
                                , 'product_options'
                                , 'tag_image'
                                , 'nk_is_shock_price'
                                , 'product_features'
                                , 'nk_product_tags'
                                , 'text_shock'
                                , 'shock_online_exp'
                                , 'nk_special_tag'
                                , 'offline_price'
                                , 'status'
                                , 'hidden_option'
                                , 'nk_show_two_price'
                                , 'nk_discount_label'
                                , 'tag_image_special'
                                , 'tag_start_time_special'
                                , 'tag_end_time_special'
                                ,'rating'
                                ,'comment'

                            ])->unique('product_id');
                        }
                        $total = 0;
                        $datas1 = [];
                        $datas2 = [];
                        $i = 0;
                        $data1 = [];
                        if(!empty($data_products1s)){
                            foreach ($data_products1s as $data_products1) {
                                $i++;
                                $total++;
                                $data1['rating'] = isset($data_products1['comment']) ? (float)$data_products1['comment'] : 0;
                                $data1['comment'] = isset($data_products1['rating']) ? (float)$data_products1['rating'] : 0;
                                $data1['status'] = isset($data_products1['status']) ? $data_products1['status'] : '';
                                $data1['hidden_option'] = isset($data_products1['hidden_option']) ? $data_products1['hidden_option'] : '';
                                $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                                $data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                                $data1['discount_percent'] = 0;
                                if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                    $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                                }

                                $data_products['text_promotion'] = [];
                                $data1['images'] = '';
                                $data1['tag_image_top_left'] = '';
                                $data1['tag_image_top_right'] = '';
                                $data1['tag_image_bottom_left'] = '';
                                $data1['tag_image_bottom_right'] = '';
                                $data1['model'] = '';
                                $data1['brand'] = '';
                                if (!empty($data_products1['nk_product_tags'])) {
                                    foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                        if ($val['tag_position'] == 'bottom-right') {
                                            $data1['tag_image_bottom_right'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'bottom-left') {
                                            $data1['tag_image_bottom_left'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'top-right') {
                                            $data1['tag_image_top_right'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'top-left') {
                                            $data1['tag_image_top_left'] = $val['tag_image'];
                                        }
                                    }
                                }
                                if ($data_products1) {
                                    if (!empty($data_products1['product_options'])) {
                                        foreach ($data_products1['product_options'] as $key => $val) {
                                            if (trim($val['nk_uu_dai_len_den']) != '') {
                                                $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                                break;
                                            }
                                        }
                                    }

//                                    $thumb = $data_products1['main_pair'];
//                                    $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
//                                    $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                    $image = '';
                                    $thumb = isset($data_products1['main_pair']) ? $data_products1['main_pair'] : [];
                                    if(!empty($thumb['icon'])){
                                        $image = $thumb['icon'];
                                    }else{
                                        if(!empty($thumb['detailed']))
                                            $image = $thumb['detailed'];
                                    }
                                    if($image !=''){
                                        $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                    }
                                    if (isset($data_products1['tag_image_special']) && $data_products1['tag_image_special'] != '') {
                                        $time = time();
                                        if ($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']) {
                                            $data1['images'] = $data_products1['tag_image_special'];
                                        }
                                    }


                                    if (!empty($data_products1['product_features'])) {
                                        if (!empty($data_products1['product_features']['591'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                                if (!empty($data_products1['product_features']['591']['subfeatures']['599'])) {
                                                    $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                    if (!empty($data_variant['variants'])) {
                                                        foreach ($data_variant['variants'] as $item) {
                                                            $data1['brand'] = $item['variant'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!empty($data_products1['product_features'])) {
                                        if (!empty($data_products1['product_features']['591'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                                if (!empty($data_products1['product_features']['591']['subfeatures']['596'])) {
                                                    $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                                    if (!empty($data_variant['variants'])) {
                                                        foreach ($data_variant['variants'] as $item) {
                                                            $data1['model'] = $item['variant'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                $data1['product_id'] = $data_products1['product_id'];
                                $data1['description'] = isset($data_products1['mobile_short_description']) ? $data_products1['mobile_short_description'] : "";
                                $data1['product_code'] = $data_products1['product_code'];
                                $data1['amount'] = $data_products1['amount'];
                                $data1['weight'] = $data_products1['weight'];
                                $data1['list_price'] = (int)$data_products1['list_price'];
                                if ($data_products1['nk_show_two_price'] == 'N' && ($data_products1['nk_is_shock_price'] != 'Y' && $data_products1['nk_is_shock_price'] != 'O')) {
                                    $data1['list_price'] = (int)$data_products1['price'];
                                }
                                $data1['price'] = (int)$data_products1['price'];
                                $data1['offline_price'] = (int)$data_products1['offline_price'];
                                $data1['product_name'] = $data_products1['product'];
                                $data1['shortname'] = $data_products1['nk_shortname'];
                                $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                                $data1['categories'] = $data_products1['category_ids'];
                                $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                                $data1['length'] = $data_products1['length'];
                                $data1['width'] = $data_products1['width'];
                                $data1['height'] = $data_products1['height'];
                                $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                                $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                                $data1['text_shock'] = $data_products1['text_shock'];
                                $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                                $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                                // fix hiện giá rẻ online khỏi sửa bên app
                                if (isset($data_products1['text_shock']) && !empty($data_products1['text_shock'])) {
                                    $data1['nk_special_tag'] = 1;
                                }
                                $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                                $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                                $data1['text_tragop'] = '';
                                //if(($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] =='O'){
                                if($data_products1['nk_tragop_0'] == 1)
                                    $data1['text_tragop'] = get_text_tra_gop($data_products1['price']);
                                //}
                                if($i < 5 ){
                                    $datas1[] = $data1;
                                }
                                $datas2[] = $data1;
                            }
                        }
                    }
                    $datas = $value;
                    $datas['product_list_details'] = $datas1;
                    $datas['total'] = $total;
                    $datas['product_list_details_more'] = $datas2;
                    // banner
                    $banners = DB::connection('mongodb')->collection('banners')
                        //->where('type_display', 2)
                        ->whereIn('position_floor_banner', $position_display)
                        ->where('time_start', '<', $time)
                        ->where('time_finish', '>', $time)
                        ->where('status', 'A')
                        ->whereIn('banner_areas', [(int)$area_id])
                        ->orderBy('position', 'ASC')
                        ->orderBy('timestamp', 'DESC')
                        //->limit(3)
                        ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
                    $banner = [];
                    $datas['banner_block'] = [];
                    if (!empty($banners)) {
                        foreach ($banners as $key => $value) {
                            $banner[$value['banner_id']]['_id'] = $value['_id'];
                            $banner[$value['banner_id']]['banner_id'] = $value['banner_id'];
                            $banner[$value['banner_id']]['banner'] = $value['banner'];
                            $banner[$value['banner_id']]['url'] = trim($value['url']);
                            $banner[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
                            $banner[$value['banner_id']]['time_start'] = $value['time_start'];
                            $banner[$value['banner_id']]['time_finish'] = $value['time_finish'];
                            $banner[$value['banner_id']]['position'] = $value['position'];
                            $banner[$value['banner_id']]['grid_position'] = $value['grid_position'];
                            $banner[$value['banner_id']]['timestamp'] = $value['timestamp'];
                        }
                        $datas['banner_block'] = array_values($banner);
                    }
//                    $banner_block_id = [26,27]; //$value['banner_block_id'];
//                    $datas['banner_block_top'] = [];
//                    if (!empty($banner_block_id)) {
//                        $banner_block_tops = DB::connection('mongodb')->collection('banners')
//                            //->where('position_floor', $banner_block_id)
//                            ->whereIn('position_floor_banner', [99999])
//                            //->where('type_display', 2)
//                            ->where('time_start', '<', $time)
//                            ->where('time_finish', '>', $time)
//                            ->where('status', 'A')
//                            ->whereIn('banner_areas', [(int)$area_id])
//                            ->orderBy('position', 'ASC')
//                            ->orderBy('timestamp', 'DESC')
//                            ->limit(3)
//                            ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
//                        $banners_block_top = [];
//                        if (!empty($banner_block_tops)) {
//                            foreach ($banner_block_tops as $key => $value) {
//                                $banners_block_top[$value['banner_id']]['_id'] = $value['_id'];
//                                $banners_block_top[$value['banner_id']]['banner_id'] = $value['banner_id'];
//                                $banners_block_top[$value['banner_id']]['banner'] = $value['banner'];
//                                $banners_block_top[$value['banner_id']]['url'] = trim($value['url']);
//                                $banners_block_top[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
//                                $banners_block_top[$value['banner_id']]['time_start'] = $value['time_start'];
//                                $banners_block_top[$value['banner_id']]['time_finish'] = $value['time_finish'];
//                                $banners_block_top[$value['banner_id']]['position'] = $value['position'];
//                                $banners_block_top[$value['banner_id']]['grid_position'] = $value['grid_position'];
//                                $banners_block_top[$value['banner_id']]['timestamp'] = $value['timestamp'];
//                            }
//                            $datas['banner_block_top'] = array_values($banners_block_top);
//                        }
//                    }
                    $datas['banner_block_top'] = [];
                    $data['data_blocks'][] = $datas;
                }
               RedisClient::setValue($key_home, $data, 720);
           }
            $data_home_v2 = config('constants.config_block_home_v2');
            $home_page = DB::connection('mongodb')->collection('home_page')->where('home_page',(int)$data_home_v2['location'])->get(['datas']);
            if(!empty($home_page)){
                $product_data = json_decode($home_page[0]['datas'],true);
            }
            $data['check_flash_sale'] = 1;
            $data['date_flash_sale'] = config('constants.date_flash_sale');
            $data['check_endtime_flash_sale'] = config('constants.check_endtime_flash_sale');
            $time = time();
            $time_end = strtotime($data['date_flash_sale']);
            if($time > $time_end){
                $data['check_endtime_flash_sale'] = 0;
            }
            if(config('constants.check_flash_sale') == 1){
                $date_flash_sale = $data['date_flash_sale'];
                $data['time_end_flash_sale'] = strtotime($date_flash_sale) - $time;
                $data['data_flash_sale'] = [];
                if(!empty($product_data)){
                    $flash_sale_id =  config('constants.flash_sale_id');

                    $product_ids = isset($product_data[$flash_sale_id]) ? $product_data[$flash_sale_id] : [];
                    $arr_id = [];
                    if(count($product_ids) > 0){
                        foreach($product_ids as $k => $v){
                            array_push($arr_id,(int)$v);
                        }
                    }
                    $product_ids = [];
                    if(!empty($arr_id)){
                        $product_ids = array_values($arr_id);
                    }
                }
                if(!empty($product_ids)){
                    $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->whereIn('product_id',$product_ids)->where('status','A')->get([
                        'product_id'
                        , 'mobile_short_description'
                        , 'product_code'
                        , 'amount'
                        , 'weight'
                        , 'length'
                        , 'width'
                        , 'height'
                        , 'list_price'
                        , 'price'
                        , 'product'
                        , 'nk_shortname'
                        , 'main_pair'
                        , 'category_ids'
                        , 'nk_tragop_0'
                        , 'product_options'
                        , 'tag_image'
                        , 'nk_is_shock_price'
                        , 'product_features'
                        , 'nk_product_tags'
                        , 'text_shock'
                        , 'shock_online_exp'
                        , 'nk_special_tag'
                        , 'offline_price'
                        , 'model'
                        , 'status'
                        , 'hidden_option'
                        , 'nk_show_two_price'
                        , 'nk_discount_label'
                        , 'tag_image_special'
                        , 'tag_start_time_special'
                        , 'tag_end_time_special'
                        ,'nk_check_amount'
                        ,'nk_check_total_amount'
                        ,'rating'
                        ,'comment'
                    ])->unique('product_id');
                    $data1 = [];
                    if(!empty($data_products1s)){
                        foreach ($data_products1s as $data_products1) {
                            $data1['check_end_time_flash_sale'] = config('constants.check_endtime_flash_sale');
                            if($time > $time_end){
                                $data1['check_end_time_flash_sale'] = 0;
                            }
                            $data1['rating'] = isset($data_products1['comment']) ? (float)$data_products1['comment'] : 0;
                            $data1['comment'] = isset($data_products1['rating']) ? (float)$data_products1['rating'] : 0;
                            $data1['status'] = isset($data_products1['status']) ? $data_products1['status'] : '';
                            $data1['hidden_option'] = $data_products1['hidden_option'];
                            $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                            $data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                            $data1['discount_percent'] = 0;
                            if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                            }
                            $data1['check_stock'] = 0; // chay hang
                            if($data_products1['amount'] > $data_products1['nk_check_amount'] && $data_products1['amount'] > $data_products1['nk_check_total_amount']){
                                $data1['check_stock'] = 1;
                            }
                            $data_products['text_promotion'] = [];
                            $data1['images'] = '';
                            $data1['tag_image_top_left'] = '';
                            $data1['tag_image_top_right'] = '';
                            $data1['tag_image_bottom_left'] = '';
                            $data1['tag_image_bottom_right'] = '';
                            $data1['model'] = '';
                            $data1['brand'] = '';
                            if (!empty($data_products1['nk_product_tags'])) {
                                foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                    if ($val['tag_position'] == 'bottom-right') {
                                        $data1['tag_image_bottom_right'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'bottom-left') {
                                        $data1['tag_image_bottom_left'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'top-right') {
                                        $data1['tag_image_top_right'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'top-left') {
                                        $data1['tag_image_top_left'] = $val['tag_image'];
                                    }
                                }
                            }
                            if ($data_products1) {
                                if (!empty($data_products1['product_options'])) {
                                    foreach ($data_products1['product_options'] as $key => $val) {
                                        if (trim($val['nk_uu_dai_len_den']) != '') {
                                            $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                            break;
                                        }
                                    }
                                }

//                                $thumb = $data_products1['main_pair'];
//                                $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
//                                $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                $image = '';
                                $thumb = isset($data_products1['main_pair']) ? $data_products1['main_pair'] : [];
                                if(!empty($thumb['icon'])){
                                    $image = $thumb['icon'];
                                }else{
                                    if(!empty($thumb['detailed']))
                                        $image = $thumb['detailed'];
                                }
                                if($image !=''){
                                    $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                }

                                if (isset($data_products1['tag_image_special']) && $data_products1['tag_image_special'] != '') {
                                    $time = time();
                                    if ($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']) {
                                        $data1['images'] = $data_products1['tag_image_special'];
                                    }
                                }


                                if (!empty($data_products1['product_features'])) {
                                    if (!empty($data_products1['product_features']['591'])) {
                                        if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures']['599'])) {
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                if (!empty($data_variant['variants'])) {
                                                    foreach ($data_variant['variants'] as $item) {
                                                        $data1['brand'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!empty($data_products1['product_features'])) {
                                    if (!empty($data_products1['product_features']['591'])) {
                                        if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures']['596'])) {
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                                if (!empty($data_variant['variants'])) {
                                                    foreach ($data_variant['variants'] as $item) {
                                                        $data1['model'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $data1['product_id'] = $data_products1['product_id'];
                            $data1['description'] = isset($data_products1['mobile_short_description']) ? $data_products1['mobile_short_description'] : "";
                            $data1['product_code'] = $data_products1['product_code'];
                            $data1['amount'] = $data_products1['amount'];
                            $data1['weight'] = $data_products1['weight'];
                            $data1['list_price'] = (int)$data_products1['list_price'];
                            if ($data_products1['nk_show_two_price'] == 'N' && ($data_products1['nk_is_shock_price'] != 'Y' && $data_products1['nk_is_shock_price'] != 'O')) {
                                $data1['list_price'] = (int)$data_products1['price'];
                            }
                            $data1['price'] = (int)$data_products1['price'];
                            $data1['offline_price'] = (int)$data_products1['offline_price'];
                            $data1['product_name'] = $data_products1['product'];
                            $data1['shortname'] = $data_products1['nk_shortname'];
                            $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                            $data1['categories'] = $data_products1['category_ids'];
                            $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                            $data1['length'] = $data_products1['length'];
                            $data1['width'] = $data_products1['width'];
                            $data1['height'] = $data_products1['height'];
                            $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                            $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                            $data1['text_shock'] = $data_products1['text_shock'];
                            $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                            $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                            // fix hiện giá rẻ online khỏi sửa bên app
                            if (isset($data_products1['text_shock']) && !empty($data_products1['text_shock'])) {
                                $data1['nk_special_tag'] = 1;
                            }
                            $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                            $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                            $data1['text_tragop'] = '';
                            //if(($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] =='O'){
                            if($data_products1['nk_tragop_0'] == 1)
                                $data1['text_tragop'] = get_text_tra_gop($data_products1['price']);
                            //}
                            $data['data_flash_sale'][] = $data1;
                        }
                    }
                }
            }else{
                $data['check_flash_sale'] = 0;
                $data['data_flash_sale'] = [];
            }
            $data['check_promotion'] = 1;
            $data['date_promotion'] = config('constants.date_promotion');
            $data['check_endtime_promotion'] = config('constants.check_endtime_promotion');
            $time = time();
            $time_end = strtotime($data['date_promotion']);
            if($time > $time_end){
                $data['check_endtime_promotion'] = 0;
            }
            if(config('constants.check_off_promotion') == 1){
                $date_promotion = $data['date_promotion'];
                $data['time_end_promotion'] = strtotime($date_promotion) - $time;
                $data['data_promotion'] = [];
                if(!empty($product_data)){
                    $promotion_id =  config('constants.promotion_id');

                    $product_ids = isset($product_data[$promotion_id]) ? $product_data[$promotion_id] : [];
                    $arr_id = [];
                    if(count($product_ids) > 0){
                        foreach($product_ids as $k => $v){
                            array_push($arr_id,(int)$v);
                        }
                    }
                    $product_ids = [];
                    if(!empty($arr_id)){
                        $product_ids = array_values($arr_id);
                    }
                }
                if(!empty($product_ids)){
                    $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->whereIn('product_id',$product_ids)->where('status','A')->orderBy('list_discount_prc', 'desc')->get([
                        'product_id'
                        , 'mobile_short_description'
                        , 'product_code'
                        , 'amount'
                        , 'weight'
                        , 'length'
                        , 'width'
                        , 'height'
                        , 'list_price'
                        , 'price'
                        , 'product'
                        , 'nk_shortname'
                        , 'main_pair'
                        , 'category_ids'
                        , 'nk_tragop_0'
                        , 'product_options'
                        , 'tag_image'
                        , 'nk_is_shock_price'
                        , 'product_features'
                        , 'nk_product_tags'
                        , 'text_shock'
                        , 'shock_online_exp'
                        , 'nk_special_tag'
                        , 'offline_price'
                        , 'model'
                        , 'status'
                        , 'hidden_option'
                        , 'nk_show_two_price'
                        , 'nk_discount_label'
                        , 'tag_image_special'
                        , 'tag_start_time_special'
                        , 'tag_end_time_special'
                        ,'nk_check_amount'
                        ,'nk_check_total_amount'
                        ,'rating'
                        ,'comment'
                    ])->unique('product_id');
                    $data1 = [];
                    if(!empty($data_products1s)){
                        foreach ($data_products1s as $data_products1) {
                            $data1['check_end_time_flash_sale'] = config('constants.check_endtime_promotion');
                            if($time > $time_end){
                                $data1['check_end_time_flash_sale'] = 0;
                            }
                            $data1['rating'] = isset($data_products1['comment']) ? (float)$data_products1['comment'] : 0;
                            $data1['comment'] = isset($data_products1['rating']) ? (float)$data_products1['rating'] : 0;
                            $data1['status'] = isset($data_products1['status']) ? $data_products1['status'] : '';
                            $data1['hidden_option'] = $data_products1['hidden_option'];
                            $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                            $data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                            $data1['discount_percent'] = 0;
                            if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                            }
                            $data1['check_stock'] = 0; // chay hang
                            if($data_products1['amount'] > $data_products1['nk_check_amount'] && $data_products1['amount'] > $data_products1['nk_check_total_amount']){
                                $data1['check_stock'] = 1;
                            }
                            $data_products['text_promotion'] = [];
                            $data1['images'] = '';
                            $data1['tag_image_top_left'] = '';
                            $data1['tag_image_top_right'] = '';
                            $data1['tag_image_bottom_left'] = '';
                            $data1['tag_image_bottom_right'] = '';
                            $data1['model'] = '';
                            $data1['brand'] = '';
                            if (!empty($data_products1['nk_product_tags'])) {
                                foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                    if ($val['tag_position'] == 'bottom-right') {
                                        $data1['tag_image_bottom_right'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'bottom-left') {
                                        $data1['tag_image_bottom_left'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'top-right') {
                                        $data1['tag_image_top_right'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'top-left') {
                                        $data1['tag_image_top_left'] = $val['tag_image'];
                                    }
                                }
                            }
                            if ($data_products1) {
                                if (!empty($data_products1['product_options'])) {
                                    foreach ($data_products1['product_options'] as $key => $val) {
                                        if (trim($val['nk_uu_dai_len_den']) != '') {
                                            $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                            break;
                                        }
                                    }
                                }

//                                $thumb = $data_products1['main_pair'];
//                                $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
//                                $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                $image = '';
                                $thumb = isset($data_products1['main_pair']) ? $data_products1['main_pair'] : [];
                                if(!empty($thumb['icon'])){
                                    $image = $thumb['icon'];
                                }else{
                                    if(!empty($thumb['detailed']))
                                        $image = $thumb['detailed'];
                                }
                                if($image !=''){
                                    $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                }

                                if (trim($data_products1['tag_image_special']) != '') {
                                    $time = time();
                                    if ($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']) {
                                        $data1['images'] = $data_products1['tag_image_special'];
                                    }
                                }


                                if (!empty($data_products1['product_features'])) {
                                    if (!empty($data_products1['product_features']['591'])) {
                                        if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures']['599'])) {
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                if (!empty($data_variant['variants'])) {
                                                    foreach ($data_variant['variants'] as $item) {
                                                        $data1['brand'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!empty($data_products1['product_features'])) {
                                    if (!empty($data_products1['product_features']['591'])) {
                                        if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures']['596'])) {
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                                if (!empty($data_variant['variants'])) {
                                                    foreach ($data_variant['variants'] as $item) {
                                                        $data1['model'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $data1['product_id'] = $data_products1['product_id'];
                            $data1['description'] = isset($data_products1['mobile_short_description']) ? $data_products1['mobile_short_description'] : "";
                            $data1['product_code'] = $data_products1['product_code'];
                            $data1['amount'] = $data_products1['amount'];
                            $data1['weight'] = $data_products1['weight'];
                            $data1['list_price'] = (int)$data_products1['list_price'];
                            if ($data_products1['nk_show_two_price'] == 'N' && ($data_products1['nk_is_shock_price'] != 'Y' && $data_products1['nk_is_shock_price'] != 'O')) {
                                $data1['list_price'] = (int)$data_products1['price'];
                            }
                            $data1['price'] = (int)$data_products1['price'];
                            $data1['offline_price'] = (int)$data_products1['offline_price'];
                            $data1['product_name'] = $data_products1['product'];
                            $data1['shortname'] = $data_products1['nk_shortname'];
                            $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                            $data1['categories'] = $data_products1['category_ids'];
                            $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                            $data1['length'] = $data_products1['length'];
                            $data1['width'] = $data_products1['width'];
                            $data1['height'] = $data_products1['height'];
                            $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                            $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                            $data1['text_shock'] = $data_products1['text_shock'];
                            $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                            $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                            // fix hiện giá rẻ online khỏi sửa bên app
                            if (isset($data_products1['text_shock']) && !empty($data_products1['text_shock'])) {
                                $data1['nk_special_tag'] = 1;
                            }
                            $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                            $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                            $data1['text_tragop'] = '';
                            //if(($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] =='O'){
                            if($data_products1['nk_tragop_0'] == 1)
                                $data1['text_tragop'] = get_text_tra_gop($data_products1['price']);
                            //}
                            $data['data_promotion'][] = $data1;

                        }
                    }
                }
            }else{
                $data['check_promotion'] = 0;
                $data['data_promotion'] = [];
            }
            $data['check_pay_day'] = 0;
            $data['data_pay_day'] = [];
            $data['color1'] = '#004ebd';
            $data['color2'] = '#004ebd';
            $data['color3'] = '#d01d01';
            $data['bg1'] ='https://cdn.nguyenkimmall.com/images/companies/_1/MKT_ECM/1019/1010/Title_count_down_mobile.gif';
            $data['bg2'] = 'https://cdn.nguyenkimmall.com/images/companies/_1/MKT_ECM/1019/1010/Title_count_down_mobile.gif';
            $data['bg3'] = 'https://i.imgur.com/heHZoxY.png';
            //echo '--thoi gian7---'.time();die;
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function home_v3($request,$data_block){
        try {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            list($first, $host) = explode(".", $_SERVER["SERVER_NAME"]);
            if ($first == 'dev' || $first == 'dev1' || $first == 'dev2' || $first == 'test') {
                $url= 'https://test.nguyenkimonline.com/';
            } elseif ($first == 'beta') {
                $url = 'https://test.nguyenkimonline.com/';
            } else {
                $url = 'https://www.nguyenkim.com/';
            }
            $time = time();
            $key_home = "REDIS_HOME_V3";
            if(RedisClient::existsKey($key_home)){
                $data = RedisClient::getValue($key_home);
            }else {
                $area_id = $request->input('area_id', 18001);
                $data['tab_bigbang'] = array(
                    '0' => array(
                        'name' => 'BigBang 2018',
                        'url' => 'https://www.nguyenkim.com/big-bang.html',
                        'default' => 1,
                        'active' => 'https://www.nguyenkim.com/images/companies/_1/bigbang/BigBang.png',
                        'deactive' => 'https://www.nguyenkim.com/images/companies/_1/bigbang/BigBang-1.png',
                    ),
                    '1' => array(
                        'name' => 'Trang chủ',
                        'url' => 'https://www.nguyenkim.com/khuyen-mai.html',
                        'default' => 0,
                        'active' => 'https://www.nguyenkim.com/images/companies/_1/bigbang/BigBang-3.png',
                        'deactive' => 'https://www.nguyenkim.com/images/companies/_1/bigbang/BigBang-2.png',
                    )
                );
                $data['tab_bigbang'] = [];
                // check text_price_online
                if(config('constants.check_text_price_online') == 1){
                    $data['check_text_price_online'] = 1;
                    $data['text_price_online'] = array(
                        '0' => array(
                            'icon' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T07/icon/hopqua_33x33.gif',
                            'name' => 'Mua online ưu đãi hơn',
                            'alias' => ''
                        ),
                        '1' => array(
                            'name' => 'Xem ngay',
                            //'alias' => 'https://www.nguyenkim.com/mua-online-uu-dai-hon.html'
                            'alias' => 'https://www.nguyenkim.com/mua-online-uu-dai-hon.html'
                        )
                    );
                }else{
                    $data['check_text_price_online'] = 0;
                    $data['text_price_online'] = [];
                }
                if(config('constants.check_text_price_online') == 1){
                    $data['check_slide_menu'] = 1;
                    $data['slide_menu'] = config('constants.slide_menu');
                }else{
                    $data['check_slide_menu'] = 0;
                    $data['slide_menu'] = [];
                }

                $banner_price_online = [];
                $banner_price_online = DB::connection('mongodb')->collection('banners')
                    ->where('type_display', 1)
                    ->where('time_start', '<', (int)$time)
                    ->where('time_finish', '>', (int)$time)
                    ->where('status', 'A')
                    ->whereIn('banner_areas', [(int)$area_id])
                    ->orderBy('position', 'ASC')
                    ->orderBy('timestamp', 'DESC')
                    ->limit(7)
                    ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);

                $banner_online = [];
                $data['banner_price_online'] = [];
                if(!empty($banner_price_online)){
                    foreach($banner_price_online as $key => $value){
                        $banner_online['_id'] = $value['_id'];
                        $banner_online['banner_id'] = $value['banner_id'];
                        $banner_online['banner'] = $value['banner'];
                        $banner_online['url'] = trim($value['url']);
                        $banner_online['nk_banner_ext'] = $value['nk_banner_ext'];
                        $banner_online['time_start'] = $value['time_start'];
                        $banner_online['time_finish'] = $value['time_finish'];
                        $banner_online['position'] = $value['position'];
                        $banner_online['grid_position'] = $value['grid_position'];
                        $banner_online['timestamp'] = $value['timestamp'];
                        $data['banner_price_online'][] = $banner_online;
                    }
                }
                if(config('constants.check_news') == 1){
                    $data['news'] = [];
                    $data['check_news'] = 0;
                    $data_news = DB::connection('mongodb')->collection('blog')->limit(4)->get(['post_id', 'name', 'seo_name','images','author','date']);
                    if(!empty($data_news)){
                        $datas_new = [];
                        foreach($data_news as $key => $value){
                            $datas_news['id'] = $value['post_id'];
                            $datas_news['name'] = $value['name'];
                            $datas_news['alias'] = 'https://www.nguyenkim.com/'.$value['seo_name'].'.html';
                            $datas_news['images'] = $value['images'];
                            $datas_news['author'] = $value['author'];
                            $datas_news['date'] = $value['date'];
                            $datas_news['category'] = 'Kinh nghiệm hay';
                            $data['news'][] = $datas_news;
                        }
                    }
                }else{
                    $data['check_news'] = 0;
                    $data['news'] = [];
                }
                if(config('constants.menu_icon_home_v2') == 1){
                    $data['check_memu_icon_home'] = 1;
                    $data['memu_icon_home'] = config('constants.menu_icon_home_v2');
                }else{
                    $data['check_memu_icon_home'] = 0;
                    $data['memu_icon_home'] = [];
                }
                if(config('constants.menu_icon_home_top') == 1){
                    $data['check_menu_icon_home_top'] = 1;
                    $data['memu_icon_home_top'] = config('constants.menu_icon_home_top');
                }else{
                    $data['check_menu_icon_home_top'] = 0;
                    $data['memu_icon_home_top'] = [];
                }
                $banner_promotion_hots = DB::connection('mongodb')->collection('banners')
                    ->where('type_display', 2)
                    ->where('position_floor_banner', 19)
                    ->where('time_start', '<', $time)
                    ->where('time_finish', '>', $time)
                    ->where('status', 'A')
                    ->whereIn('banner_areas', [(int)$area_id])
                    ->orderBy('position', 'ASC')
                    ->orderBy('timestamp', 'DESC')
                    ->limit(3)
                    ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
                $data['banner_promotion_hot'] = [];
                $banner_promotion_hot = [];
                if (!empty($banner_promotion_hots)) {
                    foreach ($banner_promotion_hots as $key => $value) {
                        $banner_promotion_hot[$value['banner_id']]['_id'] = $value['_id'];
                        $banner_promotion_hot[$value['banner_id']]['banner_id'] = $value['banner_id'];
                        $banner_promotion_hot[$value['banner_id']]['banner'] = $value['banner'];
                        $banner_promotion_hot[$value['banner_id']]['url'] = trim($value['url']);
                        $banner_promotion_hot[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
                        $banner_promotion_hot[$value['banner_id']]['time_start'] = $value['time_start'];
                        $banner_promotion_hot[$value['banner_id']]['time_finish'] = $value['time_finish'];
                        $banner_promotion_hot[$value['banner_id']]['position'] = $value['position'];
                        $banner_promotion_hot[$value['banner_id']]['grid_position'] = $value['grid_position'];
                        $banner_promotion_hot[$value['banner_id']]['timestamp'] = $value['timestamp'];
                    }
                    $data['banner_promotion_hot'] = array_values($banner_promotion_hot);
                }
                $banner_price_online = [];
                $banner_promotion_hot_bottom = DB::connection('mongodb')->collection('pages')->get(['page_id', 'redirect_url', 'list_img_url']);
                $data['banner_promotion_hot_bottom'] = [];
                if(!empty($banner_promotion_hot_bottom)){
                    foreach($banner_promotion_hot_bottom as $key => $value){
                        $banner_promotion_hot_bottoms['banner_id'] = $value['page_id'];
                        $banner_promotion_hot_bottoms['banner'] = 0;
                        $banner_promotion_hot_bottoms['url'] = $value['redirect_url'];
                        $banner_promotion_hot_bottoms['nk_banner_ext'] = $value['list_img_url'];
                        $data['banner_promotion_hot_bottom'][] = $banner_promotion_hot_bottoms;
                    }
                }
                $data['data_blocks'] = [];
                $datas['banner_block_top'] = [];
                $data_home_v2 = config('constants.config_block_home_v2');
                $data['name_top'] = $data_home_v2['name'];
                $data['name_center'] = $data_home_v2['name1'];
                $data['name_bottom'] = $data_home_v2['name2'];
                $data['wc'] = [];
                $data['data_blocks'] = [];
                $datas['banner_block_top'] = [];
                $home_page = DB::connection('mongodb')->collection('home_page')->where('home_page',(int)$data_home_v2['location'])->get(['datas']);
                if(!empty($home_page)){
                    $product_data = json_decode($home_page[0]['datas'],true);

                }
                foreach ($data_block as $key => $value) {
                    if(!empty($product_data)){
                        $product_ids = $product_data[$value['id']];
                        $arr_id = [];
                        if(count($product_ids) > 0){
                            foreach($product_ids as $k => $v){
                                array_push($arr_id,(int)$v);
                            }
                        }
                        $product_ids = [];
                        if(!empty($arr_id)){
                            $product_ids = array_values($arr_id);
                        }
                    }
                    $product_ids_more = $value['products_view_more'];
                    $position_display = $value['position_floor'];
                    if (!empty($product_ids)) {
                        $total = 0;
                        $datas1 = [];
                        $datas2 = [];
                        $i = 0;
                        $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->whereIn('product_id', $product_ids)->where('status','A')->get([
                            'product_id'
                            , 'mobile_short_description'
                            , 'product_code'
                            , 'amount'
                            , 'weight'
                            , 'length'
                            , 'width'
                            , 'height'
                            , 'list_price'
                            , 'price'
                            , 'product'
                            , 'nk_shortname'
                            , 'main_pair'
                            , 'category_ids'
                            , 'nk_tragop_0'
                            , 'product_options'
                            , 'tag_image'
                            , 'nk_is_shock_price'
                            , 'product_features'
                            , 'nk_product_tags'
                            , 'text_shock'
                            , 'shock_online_exp'
                            , 'nk_special_tag'
                            , 'offline_price'
                            , 'model'
                            , 'status'
                            , 'hidden_option'
                            , 'nk_show_two_price'
                            , 'nk_discount_label'
                            , 'tag_image_special'
                            , 'tag_start_time_special'
                            , 'tag_end_time_special'

                        ])->unique('product_id');
                        $data1 = [];
                        if(!empty($data_products1s)){
                            foreach ($data_products1s as $data_products1) {
                                $i++;
                                $total++;
                                $data1['status'] = $data_products1['status'];
                                $data1['hidden_option'] = $data_products1['hidden_option'];
                                $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                                $data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                                $data1['discount_percent'] = 0;
                                if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                    $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                                }

                                $data_products['text_promotion'] = [];
                                $data1['images'] = '';
                                $data1['tag_image_top_left'] = '';
                                $data1['tag_image_top_right'] = '';
                                $data1['tag_image_bottom_left'] = '';
                                $data1['tag_image_bottom_right'] = '';
                                $data1['model'] = '';
                                $data1['brand'] = '';
                                if (!empty($data_products1['nk_product_tags'])) {
                                    foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                        if ($val['tag_position'] == 'bottom-right') {
                                            $data1['tag_image_bottom_right'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'bottom-left') {
                                            $data1['tag_image_bottom_left'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'top-right') {
                                            $data1['tag_image_top_right'] = $val['tag_image'];
                                        } elseif ($val['tag_position'] == 'top-left') {
                                            $data1['tag_image_top_left'] = $val['tag_image'];
                                        }
                                    }
                                }
                                if ($data_products1) {
                                    if (!empty($data_products1['product_options'])) {
                                        foreach ($data_products1['product_options'] as $key => $val) {
                                            if (trim($val['nk_uu_dai_len_den']) != '') {
                                                $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                                break;
                                            }
                                        }
                                    }

                                    $thumb = $data_products1['main_pair'];
                                    $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
                                    $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                    if (trim($data_products1['tag_image_special']) != '') {
                                        $time = time();
                                        if ($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']) {
                                            $data1['images'] = $data_products1['tag_image_special'];
                                        }
                                    }


                                    if (!empty($data_products1['product_features'])) {
                                        if (!empty($data_products1['product_features']['591'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                                if (!empty($data_products1['product_features']['591']['subfeatures']['599'])) {
                                                    $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                    if (!empty($data_variant['variants'])) {
                                                        foreach ($data_variant['variants'] as $item) {
                                                            $data1['brand'] = $item['variant'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!empty($data_products1['product_features'])) {
                                        if (!empty($data_products1['product_features']['591'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                                if (!empty($data_products1['product_features']['591']['subfeatures']['596'])) {
                                                    $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                                    if (!empty($data_variant['variants'])) {
                                                        foreach ($data_variant['variants'] as $item) {
                                                            $data1['model'] = $item['variant'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                $data1['product_id'] = $data_products1['product_id'];
                                $data1['description'] = $data_products1['mobile_short_description'];
                                $data1['product_code'] = $data_products1['product_code'];
                                $data1['amount'] = $data_products1['amount'];
                                $data1['weight'] = $data_products1['weight'];
                                $data1['list_price'] = (int)$data_products1['list_price'];
                                if ($data_products1['nk_show_two_price'] == 'N' && ($data_products1['nk_is_shock_price'] != 'Y' && $data_products1['nk_is_shock_price'] != 'O')) {
                                    $data1['list_price'] = (int)$data_products1['price'];
                                }
                                $data1['price'] = (int)$data_products1['price'];
                                $data1['offline_price'] = (int)$data_products1['offline_price'];
                                $data1['product_name'] = $data_products1['product'];
                                $data1['shortname'] = $data_products1['nk_shortname'];
                                $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                                $data1['categories'] = $data_products1['category_ids'];
                                $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                                $data1['length'] = $data_products1['length'];
                                $data1['width'] = $data_products1['width'];
                                $data1['height'] = $data_products1['height'];
                                $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                                $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                                $data1['text_shock'] = $data_products1['text_shock'];
                                $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                                $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                                // fix hiện giá rẻ online khỏi sửa bên app
                                if (isset($data_products1['text_shock']) && !empty($data_products1['text_shock'])) {
                                    $data1['nk_special_tag'] = 1;
                                }
                                $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                                $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                                $data1['text_tragop'] = '';
                                //if(($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] =='O'){
                                if($data_products1['nk_tragop_0'] == 1)
                                    $data1['text_tragop'] = get_text_tra_gop($data_products1['price']);
                                //}
                                if($i < 5 ){
                                    $datas1[] = $data1;
                                }
                                $datas2[] = $data1;
                            }
                        }
                    }
                    $datas = $value;
                    $datas['product_list_details'] = $datas1;
                    $datas['total'] = $total;
                    $datas['product_list_details_more'] = $datas2;
                    // banner
                    $banners = DB::connection('mongodb')->collection('banners')
                        //->where('type_display', 2)
                        ->whereIn('position_floor_banner', $position_display)
                        ->where('time_start', '<', $time)
                        ->where('time_finish', '>', $time)
                        ->where('status', 'A')
                        ->whereIn('banner_areas', [(int)$area_id])
                        ->orderBy('position', 'ASC')
                        ->orderBy('timestamp', 'DESC')
                        //->limit(3)
                        ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
                    $banner = [];
                    $datas['banner_block'] = [];
                    if (!empty($banners)) {
                        foreach ($banners as $key => $value) {
                            $banner[$value['banner_id']]['_id'] = $value['_id'];
                            $banner[$value['banner_id']]['banner_id'] = $value['banner_id'];
                            $banner[$value['banner_id']]['banner'] = $value['banner'];
                            $banner[$value['banner_id']]['url'] = trim($value['url']);
                            $banner[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
                            $banner[$value['banner_id']]['time_start'] = $value['time_start'];
                            $banner[$value['banner_id']]['time_finish'] = $value['time_finish'];
                            $banner[$value['banner_id']]['position'] = $value['position'];
                            $banner[$value['banner_id']]['grid_position'] = $value['grid_position'];
                            $banner[$value['banner_id']]['timestamp'] = $value['timestamp'];
                        }
                        $datas['banner_block'] = array_values($banner);
                    }
//                    $banner_block_id = [26,27]; //$value['banner_block_id'];
//                    $datas['banner_block_top'] = [];
//                    if (!empty($banner_block_id)) {
//                        $banner_block_tops = DB::connection('mongodb')->collection('banners')
//                            //->where('position_floor', $banner_block_id)
//                            ->whereIn('position_floor_banner', [99999])
//                            //->where('type_display', 2)
//                            ->where('time_start', '<', $time)
//                            ->where('time_finish', '>', $time)
//                            ->where('status', 'A')
//                            ->whereIn('banner_areas', [(int)$area_id])
//                            ->orderBy('position', 'ASC')
//                            ->orderBy('timestamp', 'DESC')
//                            ->limit(3)
//                            ->get(['banner_id', 'nk_banner_ext', 'url', 'banner', 'time_start', 'time_finish', 'position', 'grid_position', 'timestamp']);
//                        $banners_block_top = [];
//                        if (!empty($banner_block_tops)) {
//                            foreach ($banner_block_tops as $key => $value) {
//                                $banners_block_top[$value['banner_id']]['_id'] = $value['_id'];
//                                $banners_block_top[$value['banner_id']]['banner_id'] = $value['banner_id'];
//                                $banners_block_top[$value['banner_id']]['banner'] = $value['banner'];
//                                $banners_block_top[$value['banner_id']]['url'] = trim($value['url']);
//                                $banners_block_top[$value['banner_id']]['nk_banner_ext'] = $value['nk_banner_ext'];
//                                $banners_block_top[$value['banner_id']]['time_start'] = $value['time_start'];
//                                $banners_block_top[$value['banner_id']]['time_finish'] = $value['time_finish'];
//                                $banners_block_top[$value['banner_id']]['position'] = $value['position'];
//                                $banners_block_top[$value['banner_id']]['grid_position'] = $value['grid_position'];
//                                $banners_block_top[$value['banner_id']]['timestamp'] = $value['timestamp'];
//                            }
//                            $datas['banner_block_top'] = array_values($banners_block_top);
//                        }
//                    }
                    $datas['banner_block_top'] = [];
                    $data['data_blocks'][] = $datas;
                }
                RedisClient::setValue($key_home, $data, 720);
            }
            $data_home_v2 = config('constants.config_block_home_v2');
            $home_page = DB::connection('mongodb')->collection('home_page')->where('home_page',(int)$data_home_v2['location'])->get(['datas']);
            if(!empty($home_page)){
                $product_data = json_decode($home_page[0]['datas'],true);
            }
            $data['check_flash_sale'] = 1;
            $data['date_flash_sale'] = config('constants.date_flash_sale');
            $data['check_endtime_flash_sale'] = config('constants.check_endtime_flash_sale');
            $time = time();
            $time_end = strtotime($data['date_flash_sale']);
            if($time > $time_end){
                $data['check_endtime_flash_sale'] = 0;
            }
            if(config('constants.check_flash_sale') == 1){
                $date_flash_sale = $data['date_flash_sale'];
                $data['time_end_flash_sale'] = strtotime($date_flash_sale) - $time;
                $data['data_flash_sale'] = [];
                if(!empty($product_data)){
                    $flash_sale_id =  config('constants.flash_sale_id');
                    $product_ids = $product_data[$flash_sale_id];
//                    $product_ids = array_values($product_ids);
//                    $product_ids = $product_data[$value['id']];
                    $arr_id = [];
                    if(count($product_ids) > 0){
                        foreach($product_ids as $k => $v){
                            array_push($arr_id,(int)$v);
                        }
                    }
                    $product_ids = [];
                    if(!empty($arr_id)){
                        $product_ids = array_values($arr_id);
                    }
                }
                if(!empty($product_ids)){
                    $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->whereIn('product_id',$product_ids)->where('status','A')->get([
                        'product_id'
                        , 'mobile_short_description'
                        , 'product_code'
                        , 'amount'
                        , 'weight'
                        , 'length'
                        , 'width'
                        , 'height'
                        , 'list_price'
                        , 'price'
                        , 'product'
                        , 'nk_shortname'
                        , 'main_pair'
                        , 'category_ids'
                        , 'nk_tragop_0'
                        , 'product_options'
                        , 'tag_image'
                        , 'nk_is_shock_price'
                        , 'product_features'
                        , 'nk_product_tags'
                        , 'text_shock'
                        , 'shock_online_exp'
                        , 'nk_special_tag'
                        , 'offline_price'
                        , 'model'
                        , 'status'
                        , 'hidden_option'
                        , 'nk_show_two_price'
                        , 'nk_discount_label'
                        , 'tag_image_special'
                        , 'tag_start_time_special'
                        , 'tag_end_time_special'
                        ,'nk_check_amount'
                        ,'nk_check_total_amount'
                    ])->unique('product_id');
                    $data1 = [];
                    if(!empty($data_products1s)){
                        foreach ($data_products1s as $data_products1) {
                            $data1['check_end_time_flash_sale'] = config('constants.check_endtime_flash_sale');
                            if($time > $time_end){
                                $data1['check_end_time_flash_sale'] = 0;
                            }
                            $data1['status'] = $data_products1['status'];
                            $data1['hidden_option'] = $data_products1['hidden_option'];
                            $data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                            $data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                            $data1['discount_percent'] = 0;
                            if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                            }
                            $data1['check_stock'] = 0; // chay hang
                            if($data_products1['amount'] > $data_products1['nk_check_amount'] && $data_products1['amount'] > $data_products1['nk_check_total_amount']){
                                $data1['check_stock'] = 1;
                            }
                            $data_products['text_promotion'] = [];
                            $data1['images'] = '';
                            $data1['tag_image_top_left'] = '';
                            $data1['tag_image_top_right'] = '';
                            $data1['tag_image_bottom_left'] = '';
                            $data1['tag_image_bottom_right'] = '';
                            $data1['model'] = '';
                            $data1['brand'] = '';
                            if (!empty($data_products1['nk_product_tags'])) {
                                foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                    if ($val['tag_position'] == 'bottom-right') {
                                        $data1['tag_image_bottom_right'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'bottom-left') {
                                        $data1['tag_image_bottom_left'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'top-right') {
                                        $data1['tag_image_top_right'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'top-left') {
                                        $data1['tag_image_top_left'] = $val['tag_image'];
                                    }
                                }
                            }
                            if ($data_products1) {
                                if (!empty($data_products1['product_options'])) {
                                    foreach ($data_products1['product_options'] as $key => $val) {
                                        if (trim($val['nk_uu_dai_len_den']) != '') {
                                            $data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                            break;
                                        }
                                    }
                                }

                                $thumb = $data_products1['main_pair'];
                                $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
                                $data1['images'] = $url.'images/thumbnails/190/190/' . $image['relative_path'];
                                if (trim($data_products1['tag_image_special']) != '') {
                                    $time = time();
                                    if ($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']) {
                                        $data1['images'] = $data_products1['tag_image_special'];
                                    }
                                }


                                if (!empty($data_products1['product_features'])) {
                                    if (!empty($data_products1['product_features']['591'])) {
                                        if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures']['599'])) {
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                if (!empty($data_variant['variants'])) {
                                                    foreach ($data_variant['variants'] as $item) {
                                                        $data1['brand'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!empty($data_products1['product_features'])) {
                                    if (!empty($data_products1['product_features']['591'])) {
                                        if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures']['596'])) {
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                                if (!empty($data_variant['variants'])) {
                                                    foreach ($data_variant['variants'] as $item) {
                                                        $data1['model'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $data1['product_id'] = $data_products1['product_id'];
                            $data1['description'] = $data_products1['mobile_short_description'];
                            $data1['product_code'] = $data_products1['product_code'];
                            $data1['amount'] = $data_products1['amount'];
                            $data1['weight'] = $data_products1['weight'];
                            $data1['list_price'] = (int)$data_products1['list_price'];
                            if ($data_products1['nk_show_two_price'] == 'N' && ($data_products1['nk_is_shock_price'] != 'Y' && $data_products1['nk_is_shock_price'] != 'O')) {
                                $data1['list_price'] = (int)$data_products1['price'];
                            }
                            $data1['price'] = (int)$data_products1['price'];
                            $data1['offline_price'] = (int)$data_products1['offline_price'];
                            $data1['product_name'] = $data_products1['product'];
                            $data1['shortname'] = $data_products1['nk_shortname'];
                            $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                            $data1['categories'] = $data_products1['category_ids'];
                            $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                            $data1['length'] = $data_products1['length'];
                            $data1['width'] = $data_products1['width'];
                            $data1['height'] = $data_products1['height'];
                            $data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                            $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%20402x.png';
                            $data1['text_shock'] = $data_products1['text_shock'];
                            $data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                            $data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                            // fix hiện giá rẻ online khỏi sửa bên app
                            if (isset($data_products1['text_shock']) && !empty($data_products1['text_shock'])) {
                                $data1['nk_special_tag'] = 1;
                            }
                            $data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                            $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                            $data1['text_tragop'] = '';
                            //if(($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] =='O'){
                            if($data_products1['nk_tragop_0'] == 1)
                                $data1['text_tragop'] = get_text_tra_gop($data_products1['price']);
                            //}
                            $data['data_flash_sale'][] = $data1;

                        }
                    }
                }
            }else{
                $data['check_flash_sale'] = 0;
                $data['data_flash_sale'] = [];
            }
            $data['check_pay_day'] = 0;
            $data['data_pay_day'] = [];
            $data['color1'] = '#004ebd';
            $data['color2'] = '#004ebd';
            $data['bg1'] ='https://cdn.nguyenkimmall.com/images/companies/_1/MKT_ECM/1019/1010/Title_count_down_mobile.gif';
            $data['bg2'] = 'https://cdn.nguyenkimmall.com/images/companies/_1/MKT_ECM/1019/1010/Title_count_down_mobile.gif';
            //echo '--thoi gian7---'.time();die;
            return array(
                'status' => 200,
                'message' => 'Thành công',
                'data' => $data
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function banner_by_category_id($request,$id){
        $data_info = config('constants.category_info');
        $time = time();
        $data = [];
//        $key_home = "REDIS_BBANNER_CATEGORY_".$id;
//        if(RedisClient::existsKey($key_home)){
//            $data = RedisClient::getValue($key_home);
//        }else {
//            foreach ($data_info as $key => $value){
//                if($value['id'] == $id){
//                    $position_display = $value['position_display'];
//                    $banner =  DB::connection('mongodb')->collection('banners')
//                        ->where('type_display',2)
//                        ->where('position_floor_banner',$position_display)
//                        ->where('time_start','<',$time)
//                        ->where('time_finish','>',$time)
//                        ->where('status','A')
//                        ->whereIn('banner_areas',[18001])
//                        ->orderBy('position', 'ASC')
//                        ->get(['banner_id','nk_banner_ext','url','banner']);
//                    $data = $banner;
//                    break;
//                }
//            }
//            RedisClient::setValue($key_home, $data, 86400);
//        }
        $data = [];
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data
        );
    }
    public function banner_by_cate_home($request,$id){
        $data_info = config('constants.category_info');
        $time = time();
        $data = [];
        //       $key_home = "REDIS_BBANNER_CATE_HOME_".$id;
//        if(RedisClient::existsKey($key_home)){
//            $data = RedisClient::getValue($key_home);
//        }else {
        foreach ($data_info as $key => $value){
            if($value['id'] == $id){
                $position_display = $value['position_display'];
                $banner =  DB::connection('mongodb')->collection('banners')
                    ->where('type_display',2)
                    ->where('position_floor_banner',$position_display)
                    ->where('time_start','<',$time)
                    ->where('time_finish','>',$time)
                    ->where('status','A')
                    ->whereIn('banner_areas',[18001])
                    ->orderBy('position', 'ASC')
                    ->get(['banner_id','nk_banner_ext','url','banner']);
                $data = $banner;
                break;
            }
        }
//            RedisClient::setValue($key_home, $data, 86400);
//        }
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data
        );
    }
    public function category_search($request,$id){
        $data_info = config('constants.category_search');
        $time = time();
        $data = [];
        foreach ($data_info as $key => $value){
            if($key == $id){
                $data = $value;
                break;
            }
        }
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data
        );
    }
    public function categories_pretties($request){
        $data_info = config('constants.category_pretties');
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data_info
        );
    }
    public function nk_array_search($item, $array)
    {
        $count = count($array);
        for ($i = 0; $i < $count; $i++) {
            if ($array[$i] == $item) {
                return $i;
            }
        }
        return -1;
    }

    public function process_data_banner($banners){
        $exist_positions = array();
        $exist_banners = array();
        foreach ($banners as $k => $v) {
            $s_index = $this->nk_array_search($v['grid_position'], $exist_positions);
            if ($s_index > -1 && intval($v['timestamp']) > intval($exist_banners[$s_index]['timestamp'])) {
                unset($banners[$exist_banners[$s_index]['index']]);
            }
            elseif ($s_index > -1 ) {
                unset($banners[$k]);
                continue;
            }
            else {
                array_push($exist_positions, $v['grid_position']);
                array_push($exist_banners, array('index' => $k, 'position' => $v['grid_position'], 'timestamp' => $v['timestamp']));
            }
        }


        if(!empty($banners)){
            return array_values($banners);
        }

    }
    public function cat_home($request) {
        $key_home = "REDIS_HOME_FPT";
        $data = [];
        if(RedisClient::existsKey($key_home)){
            $data = RedisClient::getValue($key_home);
        }else {
            $data_block = config('constants.category_floors_desktop');
            $datas['product_list_details'] = [];
            foreach ($data_block as $key => $value) {
                $product_ids = $value['product_list'];
                if (!empty($product_ids)) {
                    $datas1 = [];
                    foreach ($product_ids as $key1 => $value1) {
                        $data_products1s = DB::connection('mongodb')->collection('products_full_mode_new')->where('product_id', (int) $value1)->get([
                            'product_id'
                            , 'mobile_short_description'
                            , 'product_code'
                            , 'amount'
                            , 'weight'
                            , 'length'
                            , 'width'
                            , 'height'
                            , 'list_price'
                            , 'price'
                            , 'product'
                            , 'nk_shortname'
                            , 'main_pair'
                            , 'category_ids'
                            , 'nk_tragop_0'
                            , 'product_options'
                            , 'tag_image'
                            , 'nk_is_shock_price'
                            , 'product_features'
                            , 'nk_product_tags'
                            , 'text_shock'
                            , 'shock_online_exp'
                            , 'nk_special_tag'
                            , 'offline_price'
                            , 'model'
                            , 'status'
                            , 'hidden_option'
                            , 'nk_show_two_price'
                            , 'nk_discount_label'
                            , 'tag_image_special'
                            , 'tag_start_time_special'
                            , 'tag_end_time_special',
                        ]);

                        foreach ($data_products1s as $data_products1) {
                            //$data1['status'] = $data_products1['status'];
                            //$data1['hidden_option'] = $data_products1['hidden_option'];
                            //$data1['nk_show_two_price'] = $data_products1['nk_show_two_price'];
                            //$data1['nk_discount_label'] = $data_products1['nk_discount_label'];
                            $data1['discount_percent'] = 0;
                            if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                if (($data_products1['nk_is_shock_price'] == 'O' && $data_products1['nk_discount_label'] == 'Y') || ($data_products1['nk_show_two_price'] == 'Y' && $data_products1['nk_discount_label'] == 'Y')) {
                                    $data1['discount_percent'] = round((($data_products1['list_price'] - $data_products1['price']) / ($data_products1['list_price']) * 100));
                                }

                            }

                            //$data_products['text_promotion'] = [];
                            $data1['images'] = '';
                            $data1['tag_image_top_left'] = '';
                            $data1['tag_image_top_right'] = '';
                            $data1['tag_image_bottom_left'] = '';
                            $data1['tag_image_bottom_right'] = '';
                            $data1['model'] = '';
                            if (!empty($data_products1['nk_product_tags'])) {
                                foreach ($data_products1['nk_product_tags'] as $key => $val) {
                                    if ($val['tag_position'] == 'bottom-right') {
                                        $data1['tag_image_bottom_right'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'bottom-left') {
                                        $data1['tag_image_bottom_left'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'top-right') {
                                        $data1['tag_image_top_right'] = $val['tag_image'];
                                    } elseif ($val['tag_position'] == 'top-left') {
                                        $data1['tag_image_top_left'] = $val['tag_image'];
                                    }
                                }
                            }
                            if ($data_products1) {
                                // if (!empty($data_products1['product_options'])) {
                                // 	foreach ($data_products1['product_options'] as $key => $val) {
                                // 		$data1['text_promotion'] = $val['nk_uu_dai_len_den'];
                                // 		break;
                                // 	}
                                // }
                                if (trim($data_products1['tag_image_special']) != '') {
                                    $time = time();
                                    if ($time > $data_products1['tag_start_time_special'] && $time < $data_products1['tag_end_time_special']) {
                                        $data1['images'] = $data_products1['tag_image_special'];
                                    }
                                } else {
                                    $thumb = $data_products1['main_pair'];
                                    $image = !empty($thumb['icon']) ? $thumb['icon'] : $thumb['detailed'];
                                    $data1['images'] = 'https://www.nguyenkim.com/images/thumbnails/190/190/' . $image['relative_path'];
                                }

                                if (!empty($data_products1['product_features'])) {
                                    if (!empty($data_products1['product_features']['591'])) {
                                        if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                            if (!empty($data_products1['product_features']['591']['subfeatures']['599'])) {
                                                $data_variant = $data_products1['product_features']['591']['subfeatures']['599'];
                                                if (!empty($data_variant['variants'])) {
                                                    foreach ($data_variant['variants'] as $item) {
                                                        $data1['brand'] = $item['variant'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                // if (!empty($data_products1['product_features'])) {
                                // 	if (!empty($data_products1['product_features']['591'])) {
                                // 		if (!empty($data_products1['product_features']['591']['subfeatures'])) {
                                // 			if (!empty($data_products1['product_features']['591']['subfeatures']['596'])) {
                                // 				$data_variant = $data_products1['product_features']['591']['subfeatures']['596'];
                                // 				if (!empty($data_variant['variants'])) {
                                // 					foreach ($data_variant['variants'] as $item) {
                                // 						$data1['model'] = $item['variant'];
                                // 						break;
                                // 					}
                                // 				}
                                // 			}
                                // 		}
                                // 	}
                                // }
                            }
                            $data1['product_id'] = $data_products1['product_id'];
                            $data1['description'] = $data_products1['mobile_short_description'];
                            //$data1['product_code'] = $data_products1['product_code'];
                            //$data1['amount'] = $data_products1['amount'];
                            //$data1['weight'] = $data_products1['weight'];
                            $data1['sellingprice'] = (int) $data_products1['list_price'];
                            $data1['originprice'] = (int) $data_products1['price'];
                            //$data1['offline_price'] = (int) $data_products1['offline_price'];
                            $data1['product_name'] = $data_products1['product'];
                            //$data1['shortname'] = $data_products1['nk_shortname'];
                            $data1['tag_image'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png';
                            $data1['categories'] = $data_products1['category_ids'];
                            $data1['nk_tragop_0'] = $data_products1['nk_tragop_0'];
                            //$data1['length'] = $data_products1['length'];
                            //$data1['width'] = $data_products1['width'];
                            //$data1['height'] = $data_products1['height'];
                            //$data1['nk_is_shock_price'] = $data_products1['nk_is_shock_price'];
                            $data1['tag_image_online'] = 'https://www.nguyenkim.com/images/companies/_1/img/Sticker_GiaReOnline%402x.png';
                            //$data1['text_shock'] = $data_products1['text_shock'];
                            //$data1['shock_online_exp'] = $data_products1['shock_online_exp'];
                            //$data1['nk_special_tag'] = $data_products1['nk_special_tag'];
                            //$data1['nk_product_tags'] = $data_products1['nk_product_tags'];
                            $data1['tag_image_master'] = 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Pic_Tags/icon-master-small.png';
                            //$data1['text_tragop'] = '';
                            // if (($data_products1['list_price'] > $data_products1['price']) && $data_products1['nk_is_shock_price'] == 'O') {
                            // 	if ($data_products1['nk_tragop_0'] == 1) {
                            // 		$data1['text_tragop'] = get_text_tra_gop($data_products1['price']);
                            // 	}

                            // }
                            $datas1[] = $data1;
                        }

                    }
                }

                $datas = $value;
                $datas['product_list'] = $datas1;

                $data[] = $datas;
            }
            RedisClient::setValue($key_home, $data);

        }

        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $data,
        );
    }
}
