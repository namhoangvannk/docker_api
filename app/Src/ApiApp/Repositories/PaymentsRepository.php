<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 8/24/2018
 * Time: 3:17 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class PaymentsRepository extends RepositoryBase
{
    public function listPayments($request)
    {
        $payments = array();
        $outputs = array();
        $is_mobile = ($request->input('is_mobile')) ? (int)$request->input('is_mobile') : 1;
        $total_price_order = ($request->input('total_price_order')) ? (int)($request->input('total_price_order')) : 0;

        $query = DB::table('cscart_payments AS cp');
        $query->leftJoin('cscart_payment_descriptions AS cpd', 'cp.payment_id', '=', 'cpd.payment_id');
        $query->leftJoin('cscart_payment_processors AS cpp', 'cp.processor_id', '=', 'cpp.processor_id');
        $query->leftJoin('cscart_addons AS ca', 'cpp.addon', '=', 'ca.addon');
        $query->where('cp.status', 'A');
        $query->where('cpd.lang_code', 'vi');
        if ($is_mobile == 1) {
            // remove payments method don't show on mobile base on payment_id
            $query->whereNotIn('cp.payment_id', [77, 91]);
        }
        if ($total_price_order < 3000000) {
            // remove payments method Trả góp 0% qua thẻ tín dụng neu total price order < 3tr
            $query->whereNotIn('cp.payment_id', [59,103]);
        }
        $query->whereNotIn('cp.payment_id', [33, 73, 89,119]);
        $query->whereIn('cp.usergroup_ids', [0, 1]);
        $query->orderBy('cp.position', 'ASC');
        $payments = $query->select('cp.*', 'cpd.*', 'cpp.*')->get();

        if (count($payments) > 0) {
            $this->validatePayment($payments);
        }
        if (!empty($payments)) {
            foreach ($payments as $payment) {
                if($payment->payment_id == 118){
                    $outputs[] = array(
                        'payment_id' => $payment->payment_id,
                        'payment' => $payment->payment,
                        'description' => 'Tặng 150.000đ mua sắm thả ga tại Nguyễn Kim cho khách hàng LẦN ĐẦU liên kết ngân hàng với Ví MoMo. <a  href="https://www.nguyenkim.com/giam-150k-cung-vi-momo.html">xem chi tiết</a>',
                    );
                }else{
                    $outputs[] = array(
                        'payment_id' => $payment->payment_id,
                        'payment' => $payment->payment,
                        'description' => $payment->description,
                    );
                }

            }
        }
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $outputs
        );
    }
    public function validatePayment($payments)
    {
        $now = time();
        foreach ($payments as $key => $payment) {
            // Kiem tra thoi han cua phuong thuc nay con hay khong. Neu khong con thi an phuong thuc thanh toan nay
            if (!empty($payment->nk_from_date)) {
                if ($now < $payment->nk_from_date) {
                    unset($payments[$key]);
                    continue;
                }
            }
            if (!empty($payment->nk_to_date)) {
                if ($payment->nk_to_date < $now) {
                    unset($payments[$key]);
                    continue;
                }
            }
        }
    }
    public function info($request, $payment_id)
    {
        $datas = array();
        $payments = config('constants.payments');
        $payment_image_url = config('constants.payment_image_url');
        $list_payment = isset($payments[$payment_id]) ? $payments[$payment_id] : array();
        if (count($list_payment) > 0) {
            foreach ($list_payment as $payment) {
                $datas[] = array(
                    'title' => $payment['title'],
                    'image' => $payment_image_url . $payment['image'],
                    'value' => $payment['value'],
                );
            }
        }
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $datas
        );
    }

    public function imageCreditCart()
    {
        $image_credit_card = config('constants.image_credit_card');
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'datas' => $image_credit_card
        );
    }
    //danh sách cty thanh toán bằng thẻ tín dụng
    public function getListCreditCart()
    {
        $dir_image_credit_card = config('constants.dir_image_credit_card');
        $data_config = config('constants.payment_credit');
        $datas = array();
        $payment_credit = json_decode($data_config, true);
        if ($payment_credit) {
            foreach ($payment_credit as $bank => $info) {
                $datas[] = array(
                    'value' => $bank,
                    'label' => $info['label'],
                    'image' => $dir_image_credit_card . $info['logo'],
                );
            }
        }
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'datas' => $datas
        );

    }
    //tra gop
    public function partPay($request)
    {
        $data_config = config('constants.payment_credit');
        $payment_credit = json_decode($data_config, true);
        $tra_gop = array();
        $total = (int)$request->input('total');
        $bank = strtoupper($request->input('bank'));
        if (!empty($total) && !empty($bank)) {
            if (isset($payment_credit[$bank]['chuyendoi']) && $payment_credit[$bank]['chuyendoi']) {
                foreach ($payment_credit[$bank]['chuyendoi'] as $thang => $tile) {
                    $tien_phai_tra = (float)$total / $thang;
                    $tra_gop['kieu_tra_gop'][] = array(
                        'month' => $thang,
                        'money' => round($tien_phai_tra),
                        'label' => 'Trả trong ' . $thang . ' tháng, ' . number_format($tien_phai_tra, 0, '.', '.') . 'đ/tháng',
                    );
                }
                if (!empty($bank) && !empty($payment_credit[$bank]['cards_prefix'])) {
                    $tra_gop['lien_he'] = '<span style="color: #f34d54;">*</span> Chương trình áp dụng trả góp 0% ngân hàng <span style="color: #f34d54;">' . $bank . '</span> với các đầu thẻ sau: <br/><span style="color: #f34d54;"> ' . implode(', ', $payment_credit[$bank]['cards_prefix']) . '</span>';
                }
                return array(
                    'status' => 200,
                    'message' => 'Thành công',
                    'data' => $tra_gop
                );
            } else {
                return array(
                    'status' => 400,
                    'message' => 'Ngân hàng không hỗ trợ'
                );
            }
        } else {
            return array(
                'status' => 400,
                'message' => 'Tham số truyền vào không hợp lệ'
            );
        }
    }
    //ti le chuyen doi
    public function conversionRate($request)
    {
        // method get : params : total : total price ,  bank : value ngan hang , month : so thang tra gop
        $data_config = config('constants.payment_credit');
        $payment_credit = json_decode($data_config, true);

        $total = (int)$request->input('total');
        $bank = strtoupper($request->input('bank'));
        $month = (int)$request->input('month');

        $chuyen_doi = isset($payment_credit[$bank]['chuyendoi'][$month]) ? $payment_credit[$bank]['chuyendoi'][$month] : 0;
        if (!empty($chuyen_doi)) {
            if ($chuyen_doi < 100) {
                $chuyen_doi = round((float)$chuyen_doi * $total / 100);
            }
            if (!empty($payment_credit[$bank]['phitoithieu'])) {
                if ($chuyen_doi < (int)$payment_credit[$bank]['phitoithieu']) {
                    $chuyen_doi = (int)$payment_credit[$bank]['phitoithieu'];
                }
            }
        } else {
            $chuyen_doi = 0;
        }
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => number_format($chuyen_doi, 0, '.', '.') . 'đ'
        );
    }

    public function listCredit($request)
    {
        $list_credit = config('constants.list_credit');
        return array(
            'status' => 200,
            'message' => 'Thành công',
            'data' => $list_credit
        );
    }

    public function validateCardNumber($number)
    {
        $card_info = array();
        $cards = array(
            '001' => '/^4([x]|[0-9]){15,18}?$/', //visa
            '002' => '/((^5[1-5]([x]|[0-9]){14})|(^2(2[2-9][1-9][0-9][0-9]|7[0-2][0][0-9][0-9]|[3-6][0-9][0-9][0-9][0-9])([x]|[0-9]){10}))$/', //mastercard
            '007' => '/^([3][5](2[8-9]|[3-7][0-9]|8[0-9])([x]|[0-9]){12,15})$/' //JCB
        );
        foreach ($cards as $key => $card) {
            //  if (preg_match ($card , $number) && number . length >= 16 && number . length <= 19) {

            if (preg_match($card, $number)) {
                if ($key == '001') {
                    $name = 'VISA';
                } elseif ($key == '002') {
                    $name = 'MASTERCARD';
                } else {
                    $name = 'JCB';
                }
                $card_info = array('type' => $key, 'name' => $name);
            }
        }
        return $card_info;
    }

    public function infoCreditcart($request)
    {
        $input = $request->all();
        $card_number = (str_replace(' ', '', $input['card_info']['number_card']));
        $current_month = (int)date('m');
        $current_year = (int)date('y');

        if ($input['card_info']['year'] == $current_year) {
            $min_month = $current_month;
        } else {
            $min_month = 1;
        }
        if ($input['card_info']) {
            $rules = [
                'month' => 'required|numeric|min:' . $min_month . '|max:12',
                'year' => 'required|numeric|min:' . $current_year . '|max:68',
                'cvv' => 'required|numeric|digits:3',
                'name' => 'required|regex:/(^[A-Za-z. ]+$)+/',

            ];
            $messages = [
                'month.required' => 'Tháng không được rỗng.',
                'month.numeric' => 'Tháng phải là số nguyên hai chữ số nhỏ hơn hoặc bằng 12.',
                'month.min' => 'Tháng phải lớn hơn hoặc bằng ' . $min_month . '.',
                'month.max' => 'Tháng phải nhỏ hơn hoặc bằng 12.',
                'month.size' => 'Tháng phải là số nguyên hai chữ số nhỏ hơn hoặc bằng 12.',

                'year.required' => 'Năm không được rỗng.',
                'year.numeric' => 'Năm phải là số nguyên hai chữ số.',
                'year.min' => 'Năm phải lớn hơn hoặc bằng ' . $current_year . '.',
                'year.max' => 'Năm phải nhỏ hơn hoặc bằng 68.',
                'year.size' => 'Năm phải là số nguyên hai chữ số.',
//
                'cvv.required' => 'CVV không được rỗng',
                'cvv.numeric' => 'CVV phải là số nguyên ba chữ số',
                'cvv.size' => 'CVV phải là số nguyên ba chữ số.',

                'name.required' => 'Họ tên chủ thẻ không được rỗng.',
                'name.regex' => 'Họ tên chủ thẻ chỉ chứa được chữ cái và không dấu',

            ];

            $data_errors = array();

            $card_info = $this->validateCardNumber($card_number);
            if (count($card_info) == 0) {
                $data_errors['number_card'] = 'Số thẻ không hợp lệ';
            }

            $validator = Validator::make($input['card_info'], $rules, $messages);

            if (count($validator->errors()) > 0 || count($data_errors) > 0) {
                $errors = $validator->errors();
                !$errors->has('month') ?: $data_errors['month'] = $errors->first('month');
                !$errors->has('year') ?: $data_errors['year'] = $errors->first('year');
                !$errors->has('cvv') ?: $data_errors['cvv'] = $errors->first('cvv');
                !$errors->has('name') ?: $data_errors['name'] = $errors->first('name');

                return (
                array(
                    'status' => 400,
                    'message' => 'Error',
                    'errors' => $data_errors,
                )
                );
            } else {
                return [
                    'status' => 200,
                    'message' => 'Success',
                    'datas' => $card_info
                ];
            }
        }

    }

    public function listCardnumber($request, $user_id)
    {
        $users = DB::table('cscart_users')->where('user_id', $user_id)->get(['user_id'])->first();
        $cards = array();
        $cybersource_cards1 = array();
        $info_data = array() ;
        if (!empty($users->user_id)) {

            $data_cards = DB::table('nk_cybersource_card_customer')
                ->where([
                    ['user_id', '=', $user_id],
                    ['payment_token', '<>', ''],
                ])
                ->select('id', 'card_number', 'card_expiry_date', 'card_name', 'payment_token', 'card_type')
                ->get();
            foreach ($data_cards as $card) {

                $card_expiry_date = explode('-', $card->card_expiry_date);
                $card->card_expiry_month = $card_expiry_date[0];
                $card->card_expiry_year = $card_expiry_date[1];
                $card->card_number = 'XXXX-' . 'XXXX-' . 'XXXX-' . substr($card->card_number, -4);
                switch ($card->card_type) {
                    case '001':
                        $card->card_type_name = 'VISA';
                        break;
                    case '002':
                        $card->card_type_name = 'MASTERCARD';
                        break;
                    case '007':
                        $card->card_type_name = 'JCB';
                        break;
                    default:
                }

                $cards[$card->id] = array(
                    'card_number' => $card->card_number,
                    'card_name' => $card->card_name,
                    'month' => $card->card_expiry_month,
                    'year' => $card->card_expiry_year
                );

                $cybersource_cards = array(
                    'id_card' => $card->id,
                    'label' => 'Số thẻ ' . $card->card_type_name . ':' . $card->card_number,
                    'token' => $card->payment_token
                );

                if (!empty($_REQUEST['id_card'])) {
                    if (isset($cards[$_REQUEST['id_card']]) && count($cards[$_REQUEST['id_card']]) > 0) {
                        $info_data = $cards[$_REQUEST['id_card']];
                        //	$cybersource_cards1['info']=$info;
                    }
                }
                $cybersource_cards1[]=$cybersource_cards;
            }
            return array(
                'status' => 200,
                'message' => 'Success',
                'list' => $cybersource_cards1,
                'info' => $info_data,
            );
        } else {
            return array(
                'status' => 400,
                'message' => 'User không tồn tại'
            );
        }
    }


}