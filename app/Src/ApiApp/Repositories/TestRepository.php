<?php
/**
 * Created by PhpStorm.
 * User: nguyenkim
 * Date: 1/3/19
 * Time: 2:50 PM
 */

namespace App\Src\ApiApp\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use DB;

class TestRepository extends RepositoryBase {
	protected $model;
	protected $helper;
	protected $config;
	public function __construct(Helpers $helper) {
		$this->helper = $helper;
	}
	public function index($request) {
		$params = $request->all();
		try {

			$data = DB::connection('mongodb')->collection('products_full_mode_new')->limit(2000)->get([
				'product_id'
				, 'mobile_short_description'
				, 'product_code'
				, 'amount'
				, 'weight'
				, 'length'
				, 'width'
				, 'height'
				, 'list_price'
				, 'price'
				, 'product'
				, 'nk_shortname'
				, 'main_pair'
				, 'category_ids'
				, 'nk_tragop_0'
				, 'product_options'
				, 'tag_image'
				, 'nk_is_shock_price'
				, 'product_features'
				, 'nk_product_tags'
				, 'text_shock'
				, 'shock_online_exp'
				, 'nk_special_tag'
				, 'offline_price'
				, 'model',
			]);
			return array(
				'status' => 200,
				'message' => 'Thành công',
				'data' => [
					'product' => $data,
				],
			);

		} catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		}
	}

}