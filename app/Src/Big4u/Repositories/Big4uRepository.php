<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 20/03/2018
 * Time: 4:26 PM
 */

namespace App\Src\Big4u\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Mongo\Big4uCoupon;
use App\Models\Mongo\Big4uProduct;
use App\Models\Mongo\ProductHunter;
use App\Models\Orders;
use App\Models\Mongo\CouponHunter;
use App\Models\Mongo\FlagCoupon;
use App\Models\ProductDescriptions;
use App\Models\Products;
use App\Rules\Phone;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Log;
use Mockery\Exception;
use DB;

class Big4uRepository extends RepositoryBase
{
    protected $model;
    protected $helper;
    protected $config;
    public function __construct(Helpers $helper)
    {
        $this->helper = $helper;
        $this->model = new Big4uCoupon();
//        $this->config = Config::get('campaign.big4u');
        $this->config = Config::get('campaign.sancp');
    }

    public function all($request)
    {
        $record =  Big4uCoupon::all()->toArray();
        $total = count($record);
        $hasOrder = [];
        foreach ($record as $k => $coupon){
            if(!empty($coupon['OrderID'])){
                $hasOrder[] = $coupon;
            }
        }
        return array(
            'total' => $total,
            'total_order' => count($hasOrder),
            'data' => $hasOrder
        );
    }

    /**
     * Tạo mã giảm giá cho Samsung Big4U
     * @param $request
     * @return array
     */
    public function create($request)
    {
//        $arr1="ST3VZWXGH8,ST3VPHC749,ST3VG36CUJ,ST3VRSEW68,ST3VJF8K5W,ST3V4WZ5YB,ST3V257WPW,ST3VBS845V,ST3V79PNPQ,ST3VDK8SY2,ST3V3X3MWD,ST3V76RE7K,ST3V2DVC38,ST3VNU53FY,ST3VRXD52Q,ST3V6B2BE7,ST3VS7UF39,ST3VJ8V6Y5,ST3VWE3H8D,ST3V6M386J,ST3V59479C,ST3VBQV54J,ST3VWM28K6,ST3VVQNR9Y,ST3V5H3BEN,ST3V374766,ST3VG296SM,ST3VQV8MMX,ST3V8DURT6,ST3V7J4R73,ST3VE22QEZ,ST3VCMRR7B,ST3V8DESNW,ST3V2SZ8B4,ST3V4RKZRN,ST3VZU5HV4,ST3VUZRD82,ST3VZGRM5U,ST3VQKY7R6,ST3V8CFTUG,ST3VA4E4DW,ST3VRU254Q,ST3VQRC62F,ST3V4HDE7T,ST3VQ9VM22,ST3VF5C69K,ST3VJCT4HC,ST3VAFH9Q8,ST3VM3F8HW,ST3V9AU3TE,ST3VWA88GY,ST3V4AZWVT,ST3VEZXYA7,ST3VDR3AJ6,ST3VADCRU9,ST3V42DZWS,ST3V3ZFJMY,ST3VMP3DKM,ST3V4R49VB,ST3VH3T8W9,ST3VV39QWN,ST3V26NYJ3,ST3VCJEK29,ST3VSZD2JA,ST3V8Z5ZN9,ST3VE8A9W6,ST3VHQ3PSB,ST3VH4YHMW,ST3VG8K52D,ST3VD5BZ35,ST3VKV68AJ,ST3VQW77DK,ST3V48WUNC,ST3VBKD7Q4,ST3VYS27MJ,ST3VFF7NK8,ST3V96S3X4,ST3VX8H3A9,ST3VCER5G5,ST3V9J4E3N,ST3VYG243Z,ST3VVXBX76,ST3V322XDB,ST3VFS9674,ST3V7CNG3S,ST3VZ6NDTC,ST3VV46CJZ,ST3V4V4ZDD,ST3V58V4E3,ST3VM3YT6U,ST3V9XCZ2H,ST3V9KMSPW,ST3VZNXTD7,ST3VGJ6PJB,ST3V2Z9K35,ST3V3UUPDN,ST3V57Y3JQ,ST3VEHV32E,ST3VD96NJG,ST3VEF46HY,ST3VUHJG6A,ST3V678ZBB,ST3V88FPM5,ST3VU46DQP,ST3V7UYXXE,ST3VTKPYD5,ST3V649GPJ,ST3VERWVB4,ST3VZACX63,ST3VZB7Q7F,ST3VJQ3T7V,ST3VQ36748,ST3VYE54V7,ST3V7DW75R,ST3VNYE6JD,ST3VT9P5EC,ST3V66AK7Z,ST3V2VQJAN,ST3VBZ642C,ST3VT56UJQ,ST3V3DYYJE,ST3VPK9M6X,ST3VS6JZ4B,ST3VKA27P5,ST3VMNB7X3,ST3VACUM6D,ST3V6VYA6U,ST3V4XVZ9U,ST3V7H3BE3,ST3VER7GSC,ST3V86H7X2,ST3VA3KU83,ST3VE48QD6,ST3VRJ6MXY,ST3VXD2D85,ST3V6EUNC9,ST3V6U6D4V,ST3V55UYNJ,ST3VWDD3Y8,ST3VEN65DC,ST3V2HB234,ST3VDCU8RC,ST3VVJ4TED,ST3VB93GQ7,ST3VKBGDU2,ST3VBPGF9B,ST3V62CAWT,ST3VZQP24B,ST3VXA9D57,ST3VAZX3MW,ST3VRM2VCX,ST3VYXMEH5,ST3VG24URR,ST3VPXDDT8,ST3V32RH9T,ST3VWPGW2X,ST3V39W5AH,ST3VNB69AV,ST3VN39E5S,ST3V9KCJ32,ST3V5DEVC5,ST3VHUQD3G,ST3VUECBX6,ST3VGATFZ4,ST3VXT7ZEM,ST3VXF3Y6Q,ST3VBWN3Y7,ST3VN2RE9E,ST3VNXVEU4,ST3VDBP8N7,ST3V98ECQN,ST3VS3T3DW,ST3VSPBEJ7,ST3V644VE4,ST3VV435SD,ST3VVU8CN9,ST3VRMRJ5N,ST3VU9D4PV,ST3V7E7J24,ST3VGQB98C,ST3V749JSP,ST3VQVBDP2,ST3V5C8NYD,ST3VJSC3DV,ST3VPRVH37,ST3VFJD9EX,ST3V83R35B,ST3VPVB68T,ST3VTDD7U6,ST3VDD968P,ST3VV4SPU3,ST3V3D3NKP,ST3VXTPF4Z,ST3V6HG59B,ST3VW8JZFG,ST3V3DNCUM,ST3VQ84R7K,ST3V2G7SEU,ST3VKNPHU9,ST3VQM6T3D,ST3VR88WSW,ST3VVG6ZP5,ST3VD695JF,ST3VH397VZ,ST3VZ68RYC,ST3VMB87JQ,ST3VT25HQC,ST3V2HD4P2,ST3VBD4H6V,ST3V9Y7YR3,ST3V7PJ54N,ST3VN49AHK,ST3VKARSD9,ST3VWY4PS3,ST3VKG84PZ,ST3VB4WDZ4,ST3VTTG73J,ST3V6TAMN9,ST3VEFW94D,ST3VZ8P5RW,ST3VRH86B8,ST3VNJFB2D,ST3VT9297K,ST3VSKNH57,ST3V9M9WP7,ST3VMUB868,ST3VJ29FTU,ST3VY65N35,ST3VHPYK8U,ST3VXCK4W5,ST3V8PE78J,ST3VRW8EP3,ST3V4TY9XJ,ST3V5NHSDV,ST3VD7KM89,ST3VYXP893,ST3V84X3FJ,ST3V3Z2M7D,ST3V8H4G8A,ST3V4JFCBH,ST3V8ZDS85,ST3V2PR33B,ST3V3WBN68,ST3V7669GR,ST3VY375F5,ST3V4HN8AZ,ST3VS7GCD7,ST3V7VZ7DE,ST3VXJF3DY,ST3V75DV9Y,ST3VZXG687,ST3V7ZRCV4,ST3VP5ESY5,ST3VU76UB4,ST3VQR4QSC,ST3V6MKURH,ST3V3945Y5,ST3VQD8M89,ST3VA6Y9Z4,ST3VAG86D9,ST3V9MX84P,ST3V8S5WEG,ST3VQZY47J,ST3V2RK7BD,ST3V3KWRS3,ST3VH3P77D,ST3VDJNY4E,ST3VDDWFJ3,ST3VDFR95H,ST3VA8DHGP,ST3V6Z2PQ2,ST3VG9K7AE,ST3VBJ7Z73,ST3V828AQ3,ST3V6ZZBU4,ST3V2FVX5B,ST3V2P7PQ6,ST3VTDJ3A8,ST3VPZ42N8,ST3V65F5T7,ST3V54RW7P,ST3V362DCX,ST3VTZ6395,ST3VKE68BN,ST3VAZ4G5K,ST3V8KYSD9,ST3VC45NVH,ST3V6TB29F,ST3VMN3AWU,ST3VDFB5GH,ST3V59DZ7G,ST3V3TYCU8,ST3VDBZ9RM,ST3V4TVBDV,ST3VGXND45,ST3VAGC36Z,ST3VFMM7NV,ST3V6DJBRG,ST3VS22UAY,ST3V8QJYR4,ST3V2GUNV4,ST3V2AD2C9,ST3V4VUYH9,ST3VZ644TV,ST3V2HQWPJ,ST3V5XJ727,ST3VBRMC58,ST3V4QD3GW,ST3V9B2ARZ,ST3VY9CN3P,ST3VGC4J9C,ST3VNF9SVZ,ST3V3RNJ8Q,ST3VR53GC8,ST3VYQSD3D,ST3V25PD3F,ST3V4NB98A,ST3V8TZG2H,ST3V5H58WE,ST3V2X39E8,ST3V3FVWS3,ST3VC8K3R5,ST3V5K4Q3S,ST3VT26W72,ST3VE7J5GU,ST3VAHP4RK,ST3V7FHD6R,ST3V9PHTXN,ST3V7WF23N,ST3VE96VSU,ST3V96UP3A,ST3VDX8H4M,ST3VKR8SXV,ST3V4N4YSW,ST3VKD79NR,ST3V8TCVZ3,ST3VSFJN7A,ST3VPH5P5B,ST3VEDU9B9,ST3VJ732EZ,ST3VN9XEBS,ST3V5EGU6V,ST3VCF2RBE,ST3V6C9VJF,ST3VQM5F6G,ST3V55MCHS,ST3V46SRD2,ST3VZ76TXV,ST3VWB3PDA,ST3VQTDC6B,ST3VB8S37N,ST3VKVSA7M,ST3VW8F9MD,ST3V7Y3A2X,ST3VGV4NX5,ST3V2SCD36,ST3VF4YSGR,ST3V5ADM2Y,ST3VKSQ9X8,ST3VF5AGE9,ST3VA43E5X,ST3V5M2VT5,ST3VF2W29B,ST3V8S43NM,ST3V7WGDDU,ST3VZSKA27,ST3VV7AZ6Y,ST3V9DAV24,ST3VZ9VB36,ST3VKER7J6,ST3VAK8ZQ2,ST3V56EVW9,ST3V8TVED9,ST3VGZ4ED4,ST3VSY9DBK,ST3V93WHD7,ST3V6Y3S5B,ST3VC2DMAB,ST3VV5S38Q,ST3VA9Z24E,ST3V44BMDD,ST3V3NVYC9,ST3VD8BPJT,ST3VNCX7Q2,ST3V8PZ49P,ST3VD96MZN,ST3VJEYY5C,ST3VCU9SZ4,ST3VH9EJQY,ST3V5P62ZM,ST3VSJUP69,ST3VU2EGDM,ST3VRUK4C3,ST3VRJS8VM,ST3VCA38V8,ST3VCU2V24,ST3VCZHE44,ST3VYVFU45,ST3V7JWKCA,ST3VTK857J,ST3VDH948S,ST3VTX3UWD,ST3VGCFZ4K,ST3VNX5MZ5,ST3VZH7YEP,ST3VM3ZE9B,ST3V34QJ46,ST3VW74ZP5,ST3VS7BE86,ST3V46NMCG,ST3VACT8WG,ST3V4DJGAD,ST3V9Q8WM8,ST3VZKGU2E,ST3V6DBGDA,ST3VJ3D5RC,ST3VTE3M8P,ST3VD3VDC9,ST3V469WE2,ST3VFDH75R,ST3VXU9D54,ST3V36KZPG,ST3VU47RFN,ST3VR8UEDS,ST3V4NBU9G,ST3V6C4VP3,ST3VJ4J9WE,ST3VTCNW5X,ST3VEQU59R,ST3V5BW5ZC,ST3VC9MU7T,ST3VZT6XDE,ST3VCN8EP6,ST3VH9KYC5,ST3VFD52JX,ST3V7BDENU,ST3V33T74W,ST3V4H47B3,ST3V2H8XUS,ST3VHG3R5W,ST3VY286WH,ST3VNWUMC8,ST3VW68QWS,ST3V44T3BH,ST3VN7C6P5,ST3VU6N79F,ST3V6N9DHP,ST3VCNZ89D,ST3V87SSE4,ST3VWC8J73,ST3V6N93PD,ST3V689M5F,ST3VN9TH2W,ST3V5Q7GMM,ST3V52D3AG,ST3V8Y4NZ6,ST3VN5BWNZ,ST3VBVHE9N,ST3VTXK98D,ST3VZB258U,ST3VTM893G,ST3VC86UR2,ST3V6H52D4,ST3VVD7WQ2,ST3V68HDPK,ST3V39MNHT,ST3VJ6639B,ST3VJKG6QS,ST3V89N3TH,ST3VDSCB73,ST3VQRC7MT,ST3VRXWZ49,ST3VED89UT,ST3V57JQDB,ST3VJP7S7Y,ST3V7J5U2Q,ST3VNKE25A,ST3VU46K39,ST3V8BY95M,ST3V9FHG23,ST3VBJNM39,ST3VVJ4S7C,ST3VM9R9YZ,ST3V8M7YHG,ST3VK7E95G,ST3VZATKW9,ST3VD82WG9,ST3V6SX84D,ST3V6ESNXU,ST3V5KU52F,ST3VN3K4JS,ST3VZF4A7G,ST3VT3BZQM,ST3VKDBT9P,ST3V9V4G7P,ST3VFJ98GP,ST3V4VQZD2,ST3VF9B3KJ,ST3VH8RW3D,ST3VXW3EJD,ST3VQ3B7DE,ST3VMQ389S,ST3VGZK5GD,ST3VU3H7FX,ST3V523JTE,ST3VV34DF3,ST3VNH8DSF,ST3V5HTZRG,ST3VSPDR2G,ST3V5CDERW,ST3V5XAC7P,ST3VV7NZFD,ST3VUW6TR2,ST3VEMR3FT,ST3V7H27QS,ST3VAMQ8J5,ST3VKS763G,ST3V4DUHGC,ST3VHBC55Z,ST3VE99UGN,ST3VKAVQ64,ST3VG3HFMK,ST3VCYD7V2,ST3VGPJ43G,ST3VF2HVPX,ST3V5WT833,ST3VYM65CF,ST3VUHD7M8,ST3VREDU53,ST3VV4AHPS,ST3VW9PDEH,ST3V2X8RNZ,ST3V57X9DN,ST3VRZ2MS4,ST3VN4Y62R,ST3VKRY9QN,ST3VXTEHD3,ST3VNPM4TE,ST3VC7Z53A,ST3V75D6VJ,ST3VDC83QV,ST3VDHVXK6,ST3V537P4V,ST3VBGM67V,ST3VQ95NMF,ST3VMDK68J,ST3VQXNYS4,ST3VA6N5DH,ST3V2Q4YX5,ST3VGCD23V,ST3VPW9U45,ST3V2N8ZVG,ST3VMNR797,ST3VVH6BZJ,ST3V6JPUD2,ST3VHPP2JE,ST3VV846A4,ST3VJF5QHD,ST3VTVN3XH,ST3V8U4N89,ST3V66T74N,ST3VAVDSG5,ST3VSY6AJK,ST3VQBY9C7,ST3VFN34ZH,ST3V4PPXCJ,ST3VP7ZM8D,ST3VT4AFJB,ST3VX3T3V8,ST3VDW24BX,ST3VYDE7S5,ST3VQA5W5H,ST3V8MJ9BJ,ST3V5MBR7D,ST3VP3GP8V,ST3V83D2CW,ST3V2RE5KY,ST3V9RGB3W,ST3VNC6T76,ST3VDEVX66,ST3VQ3YUD7,ST3VMY627R,ST3VSW586N,ST3VFQ46CR,ST3VC8BV62,ST3VM4SW6H,ST3V6D7MSV,ST3V7DEQFB,ST3VW98WA9,ST3VC746YZ,ST3V9FCMC5,ST3VM3ERF4,ST3V3GANMY,ST3V5N42VB,ST3VKD6SNF,ST3VMP4R2M,ST3VB96HEW,ST3V2T7MDZ,ST3VMT73D7,ST3VJ4FWZX";
//        $value1=explode(",",$arr1);
//        foreach ($value1 as $vl1){
//            $a= CouponHunter::create(['coupon' => $vl1,'type'=>3]);
//        }
//        $arr2="ST6VZ5C7VS,ST6VR9X6N5,ST6VJ3KNZD,ST6V733GUA,ST6V3C9FHH,ST6V7ZA8G3,ST6VSVQ34T,ST6VEBTDF4,ST6VE4ADVR,ST6V82CP9D,ST6VGW44MR,ST6VQ9UUC6,ST6VE4TFM5,ST6VA24K8M,ST6VZ63QEY,ST6VPUJ5S6,ST6VZRKY5X,ST6VGMX5ZU,ST6VRS6YCQ,ST6VHZNZ57,ST6VT6YM6L,ST6VLBT7CW,ST6VDZM3KF,ST6VP6DLJ3,ST6V3Q2CLD,ST6VUZ4LWT,ST6VRXDQ69,ST6VYJDU8M,ST6VJKY435,ST6VR954G7,ST6V98B7YD,ST6VA4SCT6,ST6V4BX7PE,ST6V9WMD3G,ST6VJZV8SN,ST6VW8B5K2,ST6VJAU2Q6,ST6VE8ZQ39,ST6V46C6FH,ST6VJR67AK,ST6VYDR93U,ST6V9DRY5P,ST6VERT3XU,ST6VZ3BVY4,ST6VMX59F8,ST6VQM5JDC,ST6V57JVBU,ST6VD5K3ND,ST6VHSF9R6,ST6VFZ5YXD,ST6VEJ6A6D,ST6V599DNB,ST6VX69SWJ,ST6V76U2NB,ST6V3PQ7KN,ST6VQWB6ET,ST6VX2VF64,ST6VDMF68C,ST6VKFKZ39,ST6VN3G65Z,ST6VF2H4CN,ST6V55MBD4,ST6VGHKXZ2,ST6VQM6JFC,ST6VNG285T,ST6VX3QXA3,ST6VE7Q8LD,ST6VR6982T,ST6VRAXW56,ST6VY6S5CE,ST6VC9W5QV,ST6V7NN2Q8,ST6VN3DC3M,ST6VCB723D,ST6VPDM6RZ,ST6V8N69BR,ST6VJVXJ4T,ST6VC3M6ZT,ST6V3UVTD7,ST6VVNFD64,ST6V7UTV8D,ST6VD76U5X,ST6VAQ32U6,ST6VZJT7KH,ST6V2DCA27,ST6V2WVRED,ST6V7822GZ,ST6VEN7UKE,ST6VWJPAN5,ST6V2YUTP8,ST6VDWD73S,ST6VQ689C9,ST6V3NT2T2,ST6V4G6J6N,ST6VK7X5U2,ST6VND93DD,ST6V7VDMQ8,ST6V6Z9ANQ,ST6VDXNHV8,ST6VCGP6QY,ST6VZRS2XP,ST6VZDK4X4,ST6VR9GNEU,ST6VZ65BVE,ST6V4G7RZJ,ST6V9KCAW8,ST6VMPDQH9,ST6V4D3YJC,ST6VH35WWY,ST6V9FZ5TJ,ST6VN6RAHN,ST6VN28FMT,ST6V4KFF7D,ST6V6ECL4L,ST6V85VZ24,ST6VPQYM27,ST6VLF9QSP,ST6VZGD97P,ST6VZ9B2PH,ST6VPDA8E6,ST6VWY4KMR,ST6VEL5D6A,ST6V5E96UT,ST6VV2NK87,ST6VLZ6VH3,ST6VDU7LTK,ST6VZ2837D,ST6V2C95DJ,ST6VT7JA97,ST6VVJLG34,ST6VDQD8EM,ST6V6B4A77,ST6VDZ4DXU,ST6VFAPX7H,ST6VW48YC9,ST6VA6ZTDK,ST6V8GD2D5,ST6VDLKWB4,ST6VUD58MC,ST6VM2DXB3,ST6VDNR2BK,ST6V2VNG2E,ST6V59A2QF,ST6V6R6L5J,ST6VZMRSL8,ST6VZ5JX9D,ST6V6XRHZL,ST6V75Q39D,ST6VCS9C3K,ST6V2LNU99,ST6V6YWN7W,ST6VDEY94B,ST6V4WNG8F,ST6VJD828F,ST6V24TXQE,ST6VPTZK5X,ST6V9662BK,ST6VKUP9HE,ST6V44ABNM,ST6VM2W4YQ,ST6VMV9NK7,ST6VY7N3LM,ST6VB2N6Z8,ST6V9A4YJX,ST6VSUW77T,ST6VFDKU74,ST6VDLE7JG,ST6V76MSWP,ST6VX3S7E3,ST6V8D377T,ST6VVP37R4,ST6V7KAD65,ST6VXF3TD5,ST6V8CQD6K,ST6VV78TR6,ST6VYDW7FC,ST6VT7VZK9,ST6VE9ZXN7,ST6V57ZXX2,ST6VCSPL45,ST6VT6LD48,ST6VUMP64D,ST6VXPLJ98,ST6VWMVH4A,ST6V37D4HE,ST6V4N2XCE,ST6V8NT9G3,ST6VE7RK5F,ST6V7LAN36,ST6V5D8D79,ST6V2VJSEP,ST6VZU5P36,ST6V7KAVQH,ST6VJ65DET,ST6V5ZLDM6,ST6VWT5GZL,ST6VT3GV9F,ST6VP4MDRA,ST6VZ8NR2E,ST6V7GRDFS,ST6VDZ69D7,ST6V54HQSE,ST6VQCLW9A,ST6VTXW3P7,ST6VEM9LY6,ST6VSQW2PW,ST6VZD4WDX,ST6VHDU283,ST6V4YE95G,ST6VND5YK6,ST6VN4CTL5,ST6VPDAN2U,ST6VEDK647,ST6VABD9UU,ST6V3DCWZ9,ST6VKPHC33,ST6V3G2RYD,ST6V2HFD6Q,ST6V54CE63,ST6VQ8S4S6,ST6VQN7485,ST6VGC87R8,ST6V4B8DQL,ST6VEUJE5K,ST6VRPHJ76,ST6V7QY4ZB,ST6V2RUQ75,ST6VD33UZK,ST6VT2M3UD,ST6VVU7Z52,ST6VUDK39U,ST6V48P6UV,ST6V872TKX,ST6VDDDT42,ST6V23X8PU,ST6V2UQDZ7,ST6V95SCQU,ST6VC7J659,ST6V59CGM2,ST6VZ9JNVG,ST6VT53QM2,ST6VTRDC9S,ST6V46SDUM,ST6VXS89YG,ST6VB2LUYB,ST6VM3QBMA,ST6VDYTR92,ST6VUQXYE4,ST6V6KHUDE,ST6V6X9ZM7,ST6V3KB6WR,ST6VE3UZ4R,ST6V3Y843Y,ST6VF8PAAG,ST6VB76VFE,ST6V77NMWE,ST6V3F5CMJ,ST6VLZC6D8,ST6VMW9Y9L,ST6VTDMZP7,ST6V4CH4BK,ST6VW2DDC4,ST6VX2TG7N,ST6VVNW44B,ST6VA9BT7D,ST6V5SNFD6,ST6VDV39CW,ST6VD3AJ2T,ST6VXD7B9Z,ST6VRC5V2X,ST6V2AJ6ZC,ST6VMWEMC2,ST6V2A7GTX,ST6VNQU975,ST6V4Z8DNL,ST6VWJRYU7,ST6V67KTUA,ST6VE2HS2T,ST6VMQ7YR7,ST6V8BMPR9,ST6V2Z4U2W,ST6VXN5S6H,ST6VBY66YL,ST6V5FRH3W,ST6V36LKXK,ST6VB7Y2F3,ST6V92ABPF,ST6VD5G78D,ST6V7NVYWE,ST6VF87GM6,ST6VEPP5KR,ST6VF8M7JR,ST6V89EMZK,ST6V4U9DQH,ST6VQA67ZY,ST6VDE2AS7,ST6VTCD38Y,ST6VCYEHL9,ST6VFT6E6M,ST6V27DT9X,ST6VPVSK8Y,ST6V5G6G8S,ST6VG4RZDB,ST6V4NV4UD,ST6VN23YDS,ST6VH5PJ27,ST6VTCA828,ST6VP8A6DL,ST6VEG4L9N,ST6VRA35JV,ST6VV9T4AD,ST6VM54D96,ST6VPE3QHW,ST6VXMN5X3,ST6V94GMRX,ST6VL62C6E,ST6VT78XY6,ST6V239VFD,ST6VQ8J8NN,ST6V9QVYH3,ST6VZ93FBU,ST6VSNSMU3,ST6V9NTKQE,ST6VKTARM3,ST6V8EKCU8,ST6VV8TY2K,ST6V9BWTAU,ST6VHVFY6D,ST6V7JF3FS,ST6VW2AMZB,ST6V37CEG8,ST6V53LABU,ST6V4QE22M,ST6V7NG7UH,ST6VS4ECDA,ST6VP6BX6W,ST6VDDW3X7,ST6VYNX37V,ST6V9LBTW9,ST6V7M9GP6,ST6VH837DG,ST6V4KDQN2,ST6VVPD36Z,ST6VFQTB95,ST6VHDBS6S,ST6VY3HQ9P,ST6VF5H28V,ST6VYX3DAJ,ST6V4SHWUJ,ST6VZ37A4Q,ST6V5TD2QA,ST6VJ6ZNWT,ST6V5DDUF4,ST6VYFTHD2,ST6VSD226N,ST6V92JEUQ,ST6VQ5ATJ9,ST6VEWU963,ST6VG8Y7PM,ST6VJY6DDD,ST6VM3UDSG,ST6VGY8YDB,ST6VA6K6RW,ST6VTSK29U,ST6VLY5VRM,ST6V4FRWV5,ST6VHZ3LTE,ST6V5Q32RA,ST6VPRH53C,ST6V8DATMK,ST6VT5S2ZC,ST6VUBCVF3,ST6VT6GXWQ,ST6V4V3WR5,ST6VUSF22X,ST6VKF3F3X,ST6VLTU6FH,ST6VLD3Y8V,ST6VD86DPT,ST6V3KM7HB,ST6VTWAZ7J,ST6V5KAERK,ST6V94CSRL,ST6V9Y42GP,ST6VMY9JLV,ST6V9VEH44,ST6V5GV5GP,ST6VT38LRE,ST6V8EPMRR,ST6VG7VR5B,ST6VQ8GSQW,ST6V4DGSWL,ST6VG6NR8A,ST6VFFV48E,ST6VWCKQA9,ST6VCY8XKT,ST6V3TQUZH,ST6VKD2EJA,ST6VBSD2EY,ST6VEKC3JS,ST6VTQ68Y5,ST6VT2BWJ2,ST6VK8F846,ST6VGP95K9,ST6V9N2GYJ,ST6V4WCD58,ST6VKYS2UE,ST6V45G75D,ST6VU5WPWX,ST6VYGDN98,ST6VQ8D2CL,ST6VNPY9TQ,ST6VS423GK,ST6V49QZFA,ST6VGSA68V,ST6VK7RGK6,ST6VAJDX8S,ST6VDAQ56N,ST6VDF6JCQ,ST6V2YJVAW,ST6VTPBFQ7,ST6V3DNGG6,ST6VS8YW2Q,ST6VV8BH4J,ST6V6QTABJ,ST6V2P4T3M,ST6VK7JFH9,ST6VK92D75,ST6VD6AWM5,ST6VZRW7QT,ST6VH7QYVN,ST6V2RWDGT,ST6VPH2GC9,ST6VS5MU73,ST6VGHU442,ST6VDPN64B,ST6VD64RUB,ST6V75QMY5,ST6VWUG3AD,ST6VM7HTCY,ST6V6P39WD,ST6VHC3398,ST6VU82E2S,ST6VLZEN9M,ST6V8Z9WX4,ST6VPGPC22,ST6VZ7FHX7,ST6VF6JADE,ST6VB5PZUY,ST6VWV78DT,ST6VBDP98S,ST6VR2ECF8,ST6VFSLG4E,ST6VA4N7TU,ST6VLPD6K3,ST6VLC5852,ST6VD683LJ,ST6VDMF2T5,ST6VRKV8WB,ST6VP54UTB,ST6VESV94W,ST6V6KAPEC,ST6V2D8KVN,ST6VU7T2XE,ST6VD6FZD5,ST6V3TJR88,ST6VSH6U7R,ST6VS5DP9Z,ST6VF9LY5C,ST6VLAS3QV,ST6VSXA53U,ST6VTWDYB6,ST6VXR46YD,ST6VG7W6JD,ST6V2TBAP5,ST6VPAD37Z,ST6VWGEBF2,ST6VRX74BD,ST6VUJFH2E,ST6VJYW9VZ,ST6VCTPL2G,ST6V79XAY8,ST6VLFQPR5,ST6V46XQEK,ST6VF2JW5R,ST6VBAQF2Y,ST6V4M6QHG,ST6VS9D3RU,ST6V634LD5,ST6VV4HXFD,ST6VNF6RJ8,ST6V8DZC5Q,ST6VEX97B7,ST6VHSZ65G,ST6VNT9WFH,ST6VV9BZLD,ST6VF52RZG,ST6VAP3H3T,ST6VET8ZB6,ST6VX2KRHG,ST6VSJC3W8,ST6V9DN7G8,ST6V287LS3,ST6V3LR24P,ST6VVB36EG,ST6VW6BFYH,ST6V3RKVN5,ST6V9GU6YM,ST6VL98PMV,ST6VLDJ7GK,ST6VFDG376,ST6V9YVNX6,ST6VT7BSWQ,ST6VTQ4NC7,ST6VE4ZDV4,ST6V4XBVRM,ST6VY8W26X,ST6VZ8V264,ST6VZ2489W,ST6VXKAQZ5,ST6V3Q62DU,ST6VXDH8C2,ST6V2XDW6B,ST6VV6CC8S,ST6VJ2YGF3,ST6VYG23QU,ST6V7C52VF,ST6VD3Q3LK,ST6VKP4ZJL,ST6VQC7QHT,ST6VDNY2F7,ST6VLXYQ28,ST6V6XVMZ6,ST6VW32FNM,ST6VL6H3KT,ST6VG8ZKZ8,ST6VW3RX8D,ST6VD69EWR,ST6VP2NH9B,ST6VAEHZW3,ST6VUQ2LWH,ST6VZB9PD6,ST6VBVS6MV,ST6V7DFTDJ,ST6V9A26NV,ST6V6KWCK2,ST6VDXV34T,ST6VNT7K2E,ST6V5K78MT,ST6V6LKQXR,ST6V24PGFL,ST6V2B23FC,ST6VBYBX2R,ST6V2AVRM7,ST6VA7Y3QR,ST6VNU3Z6M,ST6V3HDLX2,ST6VR5UCY2,ST6V2SGQHL,ST6VT2ZLXC,ST6VH69R4X,ST6VPM3C2W,ST6VCUSYJ9,ST6VXT992H,ST6V3QPWV5,ST6VN6GWPC,ST6VVC59JF,ST6VXNWW3C,ST6VC5QCE8,ST6VHDWPQ3,ST6VCBXH3Q,ST6VCW9J63,ST6V3D8492,ST6V6YA9WS,ST6VSJ2PKT,ST6VZXA24T,ST6VNW357J,ST6V53FB9J,ST6VV5Q88D,ST6VEH84KT,ST6VPF6SGJ,ST6VG3L4ZG,ST6VBAT6C9,ST6VDE5ME9,ST6V6AH5MD,ST6V4XVRY9,ST6V96BSKC,ST6VZGZ4R5,ST6V5DZPB6,ST6VC5RM48,ST6V3WDDH6,ST6VA4S89T,ST6VLGD33Y,ST6VZ3AHE9,ST6VNY8DZ4,ST6VHQ4R2G,ST6VXZDM48,ST6V4Q6ZD5,ST6VDKV9VH,ST6VJ4EBQ9,ST6V355EQS,ST6V94SYM9,ST6V6GSFJD,ST6VYT9C2X,ST6VU2A5GT,ST6VHC2F3M,ST6VATW4N3,ST6V5U9X3Q,ST6VAY65BF,ST6VTNQ695,ST6V4XD47R,ST6VBDHS3M,ST6VW62B4L,ST6V2WZ79C,ST6VP7ALA3,ST6V5R9DT3,ST6VHMNF38,ST6VJSJDK2,ST6VT3Z6RE,ST6VDGR9NX,ST6V3HXRGK,ST6VDWTP87,ST6V986ZKX,ST6V6FL4CZ,ST6VE9YEB7,ST6VHTDK34,ST6VWAD2ZU,ST6VU4PJSH,ST6VSKAW9T,ST6V5JH7QY,ST6V5SFB68,ST6VJDQPH9,ST6VDAH9GM,ST6VRA9UNF,ST6V3KQSL7,ST6V79GF7D,ST6VB8D5X6,ST6VX83AYN,ST6VEB6XVU,ST6V7GBQQ6,ST6VZS7JU2,ST6VSDFBR8,ST6VUB554A,ST6VY64DSR,ST6V37BV4J,ST6VBXSXU3,ST6VFV8ZR7,ST6VHPB7EU,ST6VDA9JE3,ST6V4DEFLW,ST6VFR8KRB,ST6V4T78LR,ST6VTM2YK7,ST6VL8A96H,ST6VJGF4U6,ST6V26GADX,ST6V7DZSRU,ST6VXHRNT4,ST6VL3NL3Q,ST6VWQBJM6,ST6V24NLP9,ST6VZPVHD5,ST6VQGC9Q4,ST6V62G83K,ST6VT5X8HV,ST6VUPD84F,ST6VWHEGC6,ST6V4XYP6M,ST6VD55PTQ,ST6V4EU27H,ST6V9LMPY9,ST6VXX7VRB,ST6V2Z37LB,ST6VLU7WBG,ST6VSG7BC8,ST6VR8GEG2,ST6VKM8DM5,ST6VXMHM7C,ST6VY43TVG,ST6VQ259D7,ST6VQTEY8N,ST6V7THUD4,ST6V8PN2QV,ST6VRZPD96,ST6VLY6QNH,ST6V86SY8G,ST6V6HBVHU,ST6VVDCAR6,ST6VZ3DEML,ST6V4M7HZQ,ST6VD99KKQ,ST6VUDT729,ST6VPE3Q2T,ST6V2AFH8L,ST6V3BU6EJ,ST6VMWTZ2S,ST6VXJ94MC,ST6V8GA9E4,ST6V2KPSH3,ST6VDHW95B,ST6VXB9L6F,ST6VDJPJ87,ST6VYSDB6Z,ST6VTA9EDV,ST6VZCJYD2,ST6VDH5U4V,ST6VYB8G97,ST6VSNQC2A,ST6VPDB5Z3,ST6VDT4AME,ST6V2UN583,ST6V3VTBP5,ST6VWB5JCC,ST6VNTUA34,ST6VDJ6GMX,ST6VP9X24N,ST6VDX2ME7,ST6VSF2JL5,ST6V6E8VMZ,ST6VPFZ58C,ST6VMQ5EG5,ST6VLS3L6P,ST6VQ6H9EB,ST6V76QRJD,ST6VFRPAX7,ST6VHGR5ES,ST6VZT5U3S,ST6V7WVXEY,ST6VJ4KEP5,ST6VTPDMU2,ST6V3RXE5J,ST6VD2BNH9,ST6VXRP9X2,ST6V646PJG,ST6VRFC9TJ,ST6VTJ3DZW,ST6VGW8FG4,ST6VBNQM49,ST6VQ946T8,ST6V3276WT,ST6VEDM54A,ST6V4EMBLY,ST6VM77CQV,ST6VUF7DYH,ST6VZHY2D7,ST6VG5B4K2,ST6V9K2BV2,ST6VK2P2TN,ST6VD2ABRK,ST6VVD52WX";
//        $value2=explode(",",$arr2);
//        foreach ($value2 as $vl2){
//            $a= CouponHunter::create(['coupon' => $vl2,'type'=>6]);
//        }
//        $arr3="ST1VNDZF99,ST1VP64BB4,ST1VD2AVRH,ST1VNASRJ7,ST1VGWE2VN,ST1VHDVBQ8,ST1VF758LC,ST1VKV82FH,ST1VMYS4K6,ST1V2ELYBL,ST1VPBA48G,ST1V35WE59,ST1VTGEXK3,ST1VG6WPD4,ST1V4FNJZB,ST1VR4UH24,ST1VRQC9NB,ST1V9Z4EDV,ST1VCYT6AF,ST1VD6MX7L,ST1V2C6BER,ST1VBJK264,ST1VJWM8H6,ST1VE2AY7N,ST1VGVQN34,ST1VP272GX,ST1VVA4SZ8,ST1VWWP4UY,ST1VYT789R,ST1VDE5NLT,ST1V4WNNSJ,ST1VDL3TFE,ST1VWQ9NDP,ST1V3ALJ43,ST1V2HVADS,ST1VH4HMNJ,ST1VBUG73A,ST1VEVJ73A,ST1V9HX68W,ST1V2SP3DK,ST1VUVSEB7,ST1VBS86E9,ST1VGR866N,ST1VHEFW73,ST1VLEG9RV,ST1VHES26K,ST1VZ8TNU2,ST1VP5M88G,ST1VZCD87K,ST1V282UVR,ST1VU2UZP9,ST1VP7H8X7,ST1V7GCK5N,ST1VDL7DD8,ST1VHHTK69,ST1V6JT4FM,ST1VMSW5FC,ST1V5VDSYD,ST1VCX687E,ST1VWXAD75,ST1V6U7RH4,ST1VRS73HN,ST1VQ8ELGB,ST1VDLF62K,ST1VTD8MZ4,ST1VM6J2HW,ST1V2HWV66,ST1VLSL545,ST1VP7GCK3,ST1V7KQA34,ST1V23FQR7,ST1V3EBAQF,ST1VBD5ZGP,ST1VU99SXQ,ST1V476ARM,ST1VYAG5C2,ST1VR5WE8S,ST1VS5Y7RJ,ST1VSF4DRZ,ST1VRNB7EU,ST1VN86AG5,ST1VJ5RCGU,ST1VQZ4ED6,ST1VDZLDE7,ST1V4RNZ6S,ST1VGAV5EC,ST1V6FTDK8,ST1V2ZN6JP,ST1VY9P8SC,ST1V7XJ32H,ST1V7TQU9R,ST1VXVD3ZK,ST1V2NADL5,ST1VXWZ7SA,ST1VD52FGT,ST1V5T3DKX,ST1VDGUV34,ST1VKTB325,ST1VS9LMLJ,ST1V2XN98Z,ST1V63G8LH,ST1V9KU89V,ST1V7KUY4R,ST1V623KU2,ST1VNRUX6D,ST1VZPCF53,ST1VE7E945,ST1VY8NT68,ST1VDDTW2D,ST1V9WZKKY,ST1V4C5SAY,ST1VZD8QSK,ST1VPQZW4U,ST1VX3P43A,ST1V8BYPB9,ST1VBLK9G3,ST1VNRU5U5,ST1VRWHP27,ST1V8K3VD7,ST1VJT5HG2,ST1VWM5BKA,ST1VL429Z3,ST1VRB7VF5,ST1VQH38UZ,ST1VJBYQU9,ST1V657CYF,ST1V25P9SD,ST1VSH8DXL,ST1VL4HGN7,ST1VG6D4PC,ST1VKC7TG6,ST1VVCFEN8,ST1VF43CH7,ST1VUKSB3N,ST1V65ZK9Q,ST1VBJ5PPG,ST1VBP2N23,ST1VB373DQ,ST1V3TVEJR,ST1VC82TLD,ST1VRNG3AV,ST1VU78Q4D,ST1VTXDDC6,ST1V2XRQVW,ST1V8BDB3T,ST1V8NP9TP,ST1VEB4LR6,ST1VJ65SFK,ST1VM2DGFE,ST1VM7A3VK";
//        $value3=explode(",",$arr3);
//        foreach ($value3 as $vl3){
//            $a= CouponHunter::create(['coupon' => $vl3,'type'=>10]);
//        }
//        die;
        $params = $request->all();
        $validator = Validator::make($request->all(), [
//            'CustomerName' => ['required','string'],
            'CustomerPhone' => ['required','string',new Phone()],
            'CustomerMail' => ['required','string','email']
        ]);
        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()){
                $data = $validator->getData();
                $number = substr($data['CustomerPhone'], -9);
                if(!empty(Big4uCoupon::where('CustomerPhone', 'LIKE', "%$number%")->first())){
                    $validator->errors()->add('PhoneExists', 'The customer phone has already been registered.');
                }
                if(strtotime($this->config['start_date']) > strtotime("now")){
                    $validator->errors()->add('StartTime', 'The campaign has not started yet.');
                }
                if(strtotime($this->config['end_date']) < strtotime("now")){
                    $validator->errors()->add('EndTime', 'The campaign was end.');
                }
            }

        });
        if($validator->fails()){
            return [
                'status' => false,
                'message' => $validator->errors()
            ];
        }

        try{
            //hàm lấy coupon có sẵn;
            $coupon_code=$this->render_coupon();
            if(empty($coupon_code)) {
                return [
                    'status' => false,
                    'message' => 'het ma giam gia',
                    'noti' => 500
                ];
            }else
            {
                $coupon_code->status = 0;
                $coupon_code->save();
                $coupon_cod=$coupon_code->coupon;
                $model = $this->model;
                $data = array_merge($params, [
                    'CouponCode' => $coupon_cod,
                    'Type' => $coupon_code->type
                ]);
                $result = $model->create($data);
                return [
                    'status' => true,
                    'message' => 'success',
                    'data' => [
                        'code' => $result->CouponCode,
                        'expire_date' => $result->ExpireDate,
                        'phone' => $result->CustomerPhone,
                        'mail' => $result->CustomerMail,
                        'type' => $result->Type
                    ]
                ];
            }
        }catch (\Exception $e){
//            throw new \Exception($e->getMessage());
            return [
                'status' => false,
                'errors' => true,
                'message' => 500,
                'noti' => $e->getMessage()
            ];
        }
    }

    //funtion get random coupon
    public function render_coupon(){
        $flag=FlagCoupon::first();
        $daybefore=new \DateTime($flag->CreateAt['date']);
        $before= $daybefore->format('d');
        $daynow= new \DateTime("now");
        $now=$daynow->format('d');
        $quantity_discount=Config::get('campaign.sancp.quantity_discount');
        if(intval($before)<intval($now)){
            $flag->coupon_3=0;
            $flag->coupon_6=0;
            $flag->coupon_10=0;
            $flag->save();
            $discount=Config::get('campaign.sancp.discount');
            $random=[$discount[1],$discount[0],$discount[1],$discount[0],$discount[2],$discount[0],$discount[1],$discount[1],$discount[0],$discount[1]];
            $ran=rand(0,9);
            $ran=$random[$ran];
            $cp="coupon_".$ran;
            $flag->$cp += 1;
            $flag->save();
            $coupon = CouponHunter::where('type', $ran)->where('status', 1)->first();
            if(!empty($coupon)){
                return $coupon;
            }else{
                $cp=CouponHunter::where('status', 1)->first();
                if(!empty($cp))
                {
                    return CouponHunter::where('status', 1)->first();
                }else
                    return '';
            }
        }else{
            if ($flag->coupon_3 >= $quantity_discount[0] && $flag->coupon_6 >= $quantity_discount[1] && $flag->coupon_10 >= $quantity_discount[2]) {
                return '';
            }
            $discount=Config::get('campaign.sancp.discount');
            $random=[$discount[1],$discount[0],$discount[1],$discount[0],$discount[2],$discount[0],$discount[1],$discount[1],$discount[0],$discount[1]];
            $ran=rand(0,9);
            $ran=$random[$ran];
            while (1){
                $a=false;
                $count=0;
                switch ($ran){
                    case 3:
                        if($flag->coupon_3>=$quantity_discount[0]){
                            $ran=6;
                        }else{
                            $a=true;
                        }
                        break;
                    case  6:
                        if($flag->coupon_6>=$quantity_discount[1]){
                            $ran=10;
                        }else{
                            $a=true;
                        }
                        break;
                    case  10:
                        if($flag->coupon_10>=$quantity_discount[2]){
                            $ran=3;
                        }else{
                            $a=true;
                        }
                        break;
                }
                if($a==true){
                    break;
                }
                $count++;
                if($count==5){
                    return '';
                }
            }
            $cp="coupon_".$ran;
            $flag->$cp += 1;
            $flag->save();
            $coupon = CouponHunter::where('type', $ran)->where('status', 1)->first();
            if(!empty($coupon)){
                return $coupon;
            }else{
                $cp=CouponHunter::where('status', 1)->first();
                if(!empty($cp))
                {
                    return CouponHunter::where('status', 1)->first();
                }else
                    return '';
            }

        }
    }
    /**
     * API ghi nhận đơn hàng cho Mã giảm giá
     * @param $request
     * @param $code
     * @return array
     */
    public function update($request, $code)
    {
        $params = $request->all();
        $params['CouponCode'] = $code;
        $validator = Validator::make($params, ['OrderID' => 'required|exists:cscart_orders,order_id'],['exists' => 'The order is not found.']);
        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()) {
                $data = $validator->getData();
                $coupon = Big4uCoupon::where(['CouponCode' => $data['CouponCode']])->first();
                if (empty($coupon)) {
                    $validator->errors()->add('CouponCode', 'The coupon code is not found.');
                }
                if (!empty($coupon->OrderID)) {
                    $validator->errors()->add('CouponCode', 'The coupon has already been used.');
                }
                if (!empty($coupon) && (strtotime($coupon->ExpireDate) < strtotime("now"))) {
                    $validator->errors()->add('Expired', 'The coupon has already expired.');
                }
            }
        });

        if($validator->fails()){
            return [
                'status' => false,
                'message' =>$validator->errors()
            ];
        }

        try{
            $coupon = Big4uCoupon::where('CouponCode',$code)->first();
            $coupon->fill(['OrderID' => $params['OrderID']]);
            $coupon->save();
        }catch (\Exception $e){
            return [
                'status' => false,
                'errors' => true,
                'message' => 500
            ];
        }

        return array(
            'status' => true,
            "message" => "success"
        );
    }

    /**
     * API kiểm tra tính hợp lệ của mã giảm giá
     * @param $request
     * @return array
     */
    public function trackingStatus($request)
    {
        $params = $request->all();
        $validator = Validator::make($params,[
            'CouponCode' => ['required', 'string'],
            'CustomerPhone' => ['required', 'string', new Phone()]
        ]);
        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()) {
                $data = $validator->getData();
                $number = substr($data['CustomerPhone'], -9);
                $coupon = Big4uCoupon::where('CouponCode', strtoupper($data['CouponCode']))
                    ->where('CustomerPhone', 'like', "%$number%")->first();
                if (empty($coupon) || !empty($coupon->OrderID)) {
                    $validator->errors()->add('CouponCode', 'The coupon is not available.');
                }
                if (!empty($coupon) && (strtotime($coupon->ExpireDate) < strtotime("now"))) {
                    $validator->errors()->add('Expired', 'The coupon has already expired.');
                }
            }
        });
        if($validator->fails()){
            $errors = $validator->errors();
            return [
                'status' => false,
                'message' => $errors
            ];
        }

        return array(
            'status' => true,
            "message" => "success"
        );
    }

    /**
     * API kiểm tra trong bảng big4u_product
     * @param $request
     * @return array
     */
    public function searchProduct($request, $id)
    {
        $now = strtotime('now');
        /*
        Thu - big4u v1
        if ( $now < strtotime($this->config['start_date']) || $now > strtotime($this->config['end_date'])){
            return [
                'status' => false,
                'message' => 'Campaign is over'
            ];
        }
        */
        $id=intval($id);
        $validator = Validator::make(['ProductID' => $id],[
            'ProductID' => 'required'
        ]);

        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()) {
                $data = $validator->getData();
                $product = ProductHunter::where('ProductID', $data['ProductID'])->first();
                if (empty($product)) {
                    $validator->errors()->add('ProductID', 'The product is not found.');
                }

            }
        });
        if($validator->fails()){
            return [
                'status' => false,
                'message' => $validator->errors()
            ];
        } else {
            $product = ProductHunter::where('ProductID', $id)->first();
            return array(
                'status' => true,
                'msg'=>'success',
                'data'=>$product

            );
        }

    }

    public function cronUnsetCoupons($request)
    {
        $coupons = Big4uCoupon::whereBetween('IssuedDate',[$this->config['start_date'], $this->config['end_date']])->whereNotIn('OrderID',[null,''])->get(['OrderID'])->toArray();
        if(!empty($coupons)){
            $ids = array_column($coupons, 'OrderID');
            $orders = Orders::whereIn('order_id',array_map('intval', $ids))->where('status','I')->get(['order_id'])->toArray();
            foreach (array_column($orders, 'order_id') as $id){
                $coupon = Big4uCoupon::where(['OrderID' => (string)$id])->first();
                if(!empty($coupon)){
                    $coupon->fill(['OrderID' => ''])->update();
                }
            }
        }

        echo 'All done!!!';
    }

    public function report($request)
    {
        $data = [];
        try {
            $coupons = Big4uCoupon::all()->toArray();
            $status_list = [
                'C' => 'Hoàn tất',
                'E' => 'Đã xác nhân',
                'I' => 'Hủy',
                'O' => 'Chờ xác nhận',
                'F' => 'Thất bại',
                'V' => 'Chờ lấy hàng',
                'A' => 'Đang vận chuyện',
                'B' => 'Giao hàng thành công',
                'G' => 'Đang chuyển hoàn',
                'H' => 'Đã chuyển hoàn',
                'D' => 'Hoãn'
            ];
            foreach ($coupons as $k => $c) {
                $product_name = '';
                $status = '';
                $order_date = '';
                $discount = 0;
                $order_id = $c['OrderID'];
                if (!empty($order_id)) {
                    $order = Orders::where('order_id',$order_id)->first();
                    $status = array_key_exists($order->status,$status_list)?$status_list[$order->status]:$order->status;
                    $time = $order->timestamp;
                    $discount = $order->discount;
                    $order_date = date('Y-m-d H:i:s', $time);
                    $product = $order->order_details()->first()->product()->first();
                    $product_name = $product->product_description()->first()->product;
                }
                $data[] = [
                    //                'ID' => $k,
                    'Voucher_Code' => $c['CouponCode'],
                    'Full_Name' => $c['CustomerName'],
                    'Phone_Number' => $c['CustomerPhone'],
                    'Register_Date' => $c['IssuedDate'],
                    'Redeem_Date' => $order_date,
                    'SKU' => $product_name,
                    'Discount' => $discount,
                    'Status' => $status,
                    'Update_Time' => $order_date,
                ];
            }
            return [
                'status' => true,
                'data' => $data
            ];
        }catch (\Exception $e){
            return [
                'status' => false
            ];
        }
    }
    // Dan moi them
    public function createBannerPDP($request){
        try {
        $params = $request->all();
        $validator = Validator::make($params,[
            "Campaign"=> ['required'],
            "ProductID" => ['required'],
            "PromotionID" => ['required'],
            "CreateAt"=>['required'],
            "BannerType"=>['required'],
            "StartTime" => ['required'],
            "EndTime" => ['required'],
            "Status" => ['required'],
//            "BannerLink" => ['required'],
//            "LinkToPage" => ['required']
        ]);
//        $validator->after(function ($validator) {
//            if($validator->errors()->isEmpty()) {
//                $data = $validator->getData();
//                $product = Big4uProduct::where([
//                    'ProductID'=>$data['ProductID'],
//                    'StartTime'=>$data['StartTime'],
//                    'EndTime'=>$data['EndTime']
//                ])->first();
//                if (!empty($product)) {
//                    $validator->errors()->add('001', 'The product '. $data['ProductID'] .' exits');
//                }
//            }
//        });
        if($validator->fails()){
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        } else {
            $banner = new Big4uProduct();
            $banner->fill($params);
            $banner->save();
            return array(
                'status' => true,
                'msg'=>'success',
                'data'=>$banner
            );
        }

        } catch (\Exception $e ){
            return array(
                'status' => false,
                'code'=>$e->getCode(),
                'errors' => $e->getMessage(),
            );
        }
    }

    public function getAllBannerPDP($request){
        $response = array(
            'status' => true,
            'msg'=>'success'
        );
        $response['data'] = Big4uProduct::groupBy('PromotionID')->get([
            "Campaign",
            "ProductID",
            "PromotionID",
            "CreateAt",
            "BannerType",
            "StartTime",
            "EndTime",
            "Status",
            "BannerLink",
            "LinkToPage"
        ])->toArray();
        if(empty($response['data'])) {
            $response['msg'] = 'no data';
        }
        return $response;
    }

    public function getBannerByPromotionID($request,$id){
        $response = array(
            'status' => true,
            'msg'=>'success'
        );
        $obj_data = Big4uProduct::where('PromotionID',$id)->orderBy('CreateAt', 'desc')->paginate(50);
        $json = json_encode($obj_data);
        $data = json_decode($json, true);
        foreach ($data['data'] as &$product){
            $tmp = ProductDescriptions::where([
                'product_id' => $product['ProductID'],
                'lang_code' => 'vi'
            ])->get(['product'])->toArray();
            $product['Product'] = !empty($tmp[0]['product'])? $tmp[0]['product']: '';
            $tmp2 = Products::where([
                'product_id' => $product['ProductID']])->get(['product_code'])->toArray();
            $product['ProductCode'] = !empty($tmp2[0]['product_code'])? $tmp2[0]['product_code']: '';
        }
        if(empty($data)) {
            $response['msg'] = 'no data';
        }
        return $data;
    }

    public function updateBannerByPromotionID($request,$promotion_id){
        try {
            $params = $request->all();
            $params_update_all = array();
            if(isset($params['Campaign'])){
                $params_update_all['Campaign'] = $params['Campaign'];
            }
            if(isset($params['StartTime'])){
                $params_update_all['StartTime'] = intval($params['StartTime']);
            }
            if(isset($params['EndTime'])){
                $params_update_all['EndTime'] = intval($params['EndTime']);
            }
            if(isset($params['Status'])){
                $params_update_all['Status'] = $params['Status'];
            }
            if(isset($params['BannerLink'])){
                $params_update_all['BannerLink'] = $params['BannerLink'];
            }
            if(isset($params['LinkToPage'])){
                $params_update_all['LinkToPage'] = $params['LinkToPage'];
            }

            if(!empty($params_update_all)){
                Big4uProduct::where('PromotionID',$params['PromotionID'])->update($params_update_all);
                return array(
                    'status' => true,
                    'msg'=>'success',
                    'data'=>$params
                );
            }


        } catch (\Exception $e ){
            return array(
                'status' => false,
                'code'=>$e->getCode(),
                'errors' => $e->getMessage(),
            );
        }
    }

    public function checkIsShowBannerPDP($product_id){
        $response = array(
            'status' => true,
            'msg'=>'success'
        );
        $now = time();
        $response['data'] = Big4uProduct::where('ProductID',intval($product_id))
            ->where('StartTime','<=',intval($now))
            ->where('EndTime','>',intval($now))
            ->where('Status','A')
            ->get(['ProductID','BannerType','BannerLink','LinkToPage','StartTime','EndTime'])
            ->sortByDesc('StartTime')
            ->first();
        unset($response['data']['_id']);
        if(empty($response['data'])) {
            $response['msg'] = 'no data';
            unset($response['data']);
            $response['status'] = false;
        }
        return $response;
    }

    public function updateSingleBannerPDP($request,$m_id){
        try {
            $params = $request->all();
            $validator = Validator::make($params,[
                "Campaign"=> ['required'],
                "ProductID" => ['required'],
                "PromotionID" => ['required'],
                "CreateAt"=>['required'],
                "BannerType"=>['required'],
                "StartTime" => ['required'],
                "EndTime" => ['required'],
                "Status" => ['required'],
//                "BannerLink" => ['required']
//                "LinkToPage" => ['required']
            ]);
            if($validator->fails()){
                return [
                    'status' => false,
                    'errors' => $validator->errors()
                ];
            } else {
                $params['_id'] = $m_id;
                Big4uProduct::destroy($m_id);
                unset($params['_id']);
                $banner = Big4uProduct::create($params);

                return array(
                    'status' => true,
                    'msg'=>'success',
                    'data'=>$banner
                );
            }

        } catch (\Exception $e ){
            return array(
                'status' => false,
                'code'=>$e->getCode(),
                'errors' => $e->getMessage(),
            );
        }
    }
}