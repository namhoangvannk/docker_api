<?php
namespace App\Src\Promotion\Transformers;

use League\Fractal\TransformerAbstract;

class CouponsAutomaticTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        $conditions_hash = json_decode($obj->conditions_hash, true);

        return [
            'promotion_id' => (int)$obj->promotion_id,
            'coupon_code' => $conditions_hash['coupon_code'],
        ];
    }
}
