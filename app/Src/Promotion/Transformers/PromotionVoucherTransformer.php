<?php

namespace App\Src\Promotion\Transformers;

use League\Fractal\TransformerAbstract;

class PromotionVoucherTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj['id']
        ];
    }
}
