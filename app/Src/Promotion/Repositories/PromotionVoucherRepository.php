<?php
namespace App\Src\Promotion\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\Src\Promotion\Transformers\PromotionVoucherTransformer;
use App\Models\NkPromotionVoucher;

class PromotionVoucherRepository extends RepositoryBase
{
    protected $fractal;
    protected $transformer;
    protected $model;

    public function __construct(
        Manager $fractal,
        PromotionVoucherTransformer $transformer
    )
    {
        $this->fractal = $fractal;
        $this->transformer = $transformer;
        $this->model = new NkPromotionVoucher();
    }

    public function create($request)
    {
        $params = $request->all();
        $model = $this->model;

        try {
            $model->fill($params);
            $model->save();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function update($request, $id)
    {
        $params = $request->all();

        try {
            $model = NkPromotionVoucher::find($id);
            $model->fill($params);
            $model->update();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}
