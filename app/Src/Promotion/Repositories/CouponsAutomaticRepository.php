<?php
namespace App\Src\Promotion\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use App\Mail\WinnerCoupons;
use App\Models\ProductDescriptions;
use App\Models\Users;
use App\Src\Product\Transformers\ProductTransformer;
use Illuminate\Support\Facades\Mail;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;

use App\Src\Promotion\Transformers\CouponsAutomaticTransformer;
use App\Models\CouponsAutomatic;
use App\Models\Promotions;
use App\Models\PromotionDescriptions;
use App\Models\Products;
use App\Helpers\Cache\RedisManager;
use App\Models\Mongo\Products as MProducts;
use Log;

class CouponsAutomaticRepository extends RepositoryBase
{
    protected $fractal;
    protected $transformer;
    protected $model;
    protected $helper;
    protected $redis;
    protected $prTransformer;

    public function __construct(
        Manager $fractal,
        CouponsAutomaticTransformer $transformer,
        Helpers $helper,
        RedisManager $redis,
        ProductTransformer $prTransformer
    )
    {
        $this->fractal = $fractal;
        $this->helper = $helper;
        $this->transformer = $transformer;
        $this->model = new Promotions();
        $this->redis = $redis;
        $this->prTransformer = $prTransformer;
    }

    public function create($request)
    {
        $params = $request->all();
        $model = $this->model;

        $products = Products::find(intval($params['product_id']));

        if($products == null)
            return ['success' => false, 'message' => 'Create failed'];

        try {
            $coupon_code = $this->helper->generateUID('AJC');

            $conditions_hash = [
                'product_code_text' => $products->product_code,
                'coupon_code' => $coupon_code,
                'voucher_use_once' => 'Y',
                'no_installment' => 'Y',
                'subscribed' => 'Y',
                'products_number' => 1,
                'users' => $params['user_id']
            ];

            $conditions = [
                'set' => 'all',
                'set_value' => 1,
                'conditions' => [
                    [
                        'operator' => 'in',
                        'condition' => 'product_code_text',
                        'value' => $products->product_code
                    ],
                    [
                        'operator' => 'in',
                        'condition' => 'coupon_code',
                        'value' => $coupon_code
                    ],
                    [
                        'operator' => 'eq',
                        'condition' => 'voucher_use_once',
                        'value' => 'Y'
                    ],
                    [
                        'operator' => 'eq',
                        'condition' => 'no_installment',
                        'value' => 'Y'
                    ],
                    [
                        'operator' => 'eq',
                        'condition' => 'subscribed',
                        'value' => 'Y'
                    ],
                    [
                        'operator' => 'eq',
                        'condition' => 'products_number',
                        'value' => 1
                    ],
                    [
                        'operator' => 'eq',
                        'condition' => 'users',
                        'value' => $params['user_id']
                    ]
                ]
            ];

            $bonuses[] = [
                'bonus' => 'order_discount',
                'discount_bonus' => 'to_fixed',
                'discount_value' => $params['decrease_value']
            ];

            $users_conditions_hash = json_encode($params['user_id']);
            
            $data = [
                'conditions' => serialize($conditions),
                'bonuses' => serialize($bonuses),
                'status' => 'A',
                'conditions_hash' => json_encode($conditions_hash),
                'users_conditions_hash' => $users_conditions_hash,
                'zone' => 'cart',
                'from_date' => strtotime(date('Y-m-d H:i:s')),
                'to_date' => strtotime(date('Y-m-d 23:59:59', strtotime($params['expire_time']))),
            ];
            Log::info('Promotion Coupon: '.var_export($data, true));
            $model->fill($data);
            $model->save();

            $data_description = [
                'promotion_id' => $model->promotion_id,
                'name' =>  date('Ymd').' - Đấu giá - '.$params['coupon_name'],
                'lang_code' => 'vi'
            ];

            PromotionDescriptions::insert($data_description);

            // Send mail
            $product = $this->getProduct($products->product_code);
            Mail::to([$params['email']])->send(new WinnerCoupons($model, $product['data'][0]));
            return ['success' => true, 'message' => 'Create successfull'];

//            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function check($request)
    {
        $result = ['success' => true, 'message' => 'OK'];
        $params = $request->all();
        $current_date = date('Y-m-d H:i:s');

        if (empty($params))
            return ['success' => false, 'message' => 'Vui lòng truyền tham số'];

        $model = CouponsAutomatic::where('coupon_code', '=', $params['coupon_code'])
            ->where('expire_time', '>=', $current_date)->first();

        if ($model == null)
            return ['success' => false, 'message' => 'Mã khuyến mãi không tồn tại hoặc hết hạn sử dụng'];

        return $result;
    }

    private function getProduct($product_code)
    {
        $transformer = $this->prTransformer;
        $offset = 0;
        $limit = 1;
        $key_redis = 'product_detail_'. $product_code;

        $data = $this->redis->getValue($key_redis, 1);
        if($data)
            return $data;

        $product = MProducts::select(
            'product_id',
            'product_code',
            'product_name',
            'price',
            'list_price',
            'display_name',
            'model',
            'brand',
            'product_categories',
            'product_options',
            'seo_names',
            'product_features',
            'product_descriptions',
            'images'
        );

        $product = $product->where('product_code', '=', $product_code);

        $list = $product->skip($offset)->take($limit)->get();
        $resource = new Collection($list, $transformer);
        $data = $this->fractal->createData($resource)->toArray();
        $this->redis->setValue($key_redis, $data, env('CACHE_PRODUCT_DETAIL', 300), 1);

        return $data;
    }

}
