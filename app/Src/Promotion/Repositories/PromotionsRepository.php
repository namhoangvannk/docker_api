<?php

namespace App\Src\Promotion\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\Models\Promotions;
use App\Models\PromotionsUseToday;
use DB;

class PromotionsRepository extends RepositoryBase
{
    protected $fractal;

    public function __construct(Manager $fractal)
    {
        $this->fractal = $fractal;        
    }

    public function check($request)
    {
        $result = ['success' => true];
        $params = $request->all();

        if(empty($params))
            return ['success' => false, 'message' => 'Vui lòng truyền tham số'];

        $coupon_value = explode('-', $params['coupon_code']);
        
        if(!isset($coupon_value[1]))
            return ['success' => false, 'message' => 'Mã giảm giá không tồn tại'];

        $coupon_value = $coupon_value[1];
        $promotions = Promotions::where('status', '=', 'A')->get();

        if($promotions == null)
            return ['success' => false, 'message' => 'Không có chương trình giảm giá'];

        $arr_coupon = [];
        foreach($promotions as $key => $value) {
            $conditions = unserialize($value['conditions']);
            $conditions = $conditions['conditions'];

            foreach($conditions as $item) {
                if(isset($item['condition']) && $item['condition'] == 'coupon_code') {
                    $arr_value = explode(',', $item['value']);
                    $arr_coupon = array_merge($arr_coupon, $arr_value);
                }
            }
        }

        $is_exist = false;
        $message = 'Mã giảm giá không tồn tại';
        if(in_array($coupon_value, $arr_coupon)){
            $is_exist = true;
            $message = 'Mã giảm giá có trong danh sách';
        }

        $result['success'] = $is_exist;
        $result['message'] = $message;
        // $result['data'] = $arr_coupon;

        return $result;
    }

    public function update($request)
    {
        $params = $request->all();

        try {
            $is_update = $params['is_update'];
            $promotionIds = $params['promotion_ids'];
            $today = date("Y-m-d");

            foreach ($promotionIds as $value) {
                $promotion = Promotions::find($value);
                if($promotion == null)
                    continue;

                    

                if($is_update)
                    $promotion->number_of_usages = $promotion->number_of_usages + 1;
                else
                    $promotion->number_of_usages = $promotion->number_of_usages - 1;

                $promotion->update();

                $promotionusetoday = PromotionsUseToday::where('promotion_id','=', $value)
                    ->where('today', '=', $today)
                    ->first();

                if($promotionusetoday == null) {
                    $promotionusetoday = new PromotionsUseToday();
                    $promotionusetoday->number_of_usages_today = 1;
                    $promotionusetoday->today = $today;
                    $promotionusetoday->promotion_id = $value;
                    $promotionusetoday->update();
                } else {
                    DB::table('cscart_promotions_use_today')
                        ->where('promotion_id', $value)
                        ->where('today', $today)
                        ->update(['today' => $today, 'number_of_usages_today'=> $promotionusetoday->number_of_usages_today + 1]);
                }
            }

            return ['data' => [], 'message'=>'Cập nhật thành công'];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}
