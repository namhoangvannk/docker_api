<?php
namespace App\Src\Promotion\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\SMS\Sms;
use App\Rules\Phone;
use Illuminate\Support\Facades\Validator;

class BigcRepository extends RepositoryBase
{
    protected $model;

    public function __construct(){}

    /**
     * Gửi sms mã coupon đến danh sách khách hàng
     * @param $request
     */
    public function sendCouponSms($request)
    {
        $params = $request->all();
        $validator = Validator::make($params,[
            'receiver' => ['required','array'],
            'template' => ['required','string']
        ]);
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }
        try{
            $result = array();
            foreach ($params['receiver'] as $key => $receiver){
                $receiver_valid = Validator::make($receiver,[
                    'sdt' => ['required','string',new Phone()],
                    'coupon' => ['required','string'],
                    'value' => ['required','string'],
                    'expire' => ['required', 'string']
                ]);
                if($receiver_valid->fails()){
                    $receiver['status'] = 'FAILED';
                    $receiver['errors'] = $receiver_valid->errors();
                }else{
                    $template = $params['template'];
                    $find = ['%COUPON','%VALUE','%EXPIRE'];
                    $replace = [
                        strtoupper($receiver['coupon']),
                        $receiver['value'],
                        $receiver['expire'],
                    ];
                    $text = str_replace($find,$replace,$template);
                    $sms = new Sms();
                    $sms->fill([
                        'phone' => $receiver['sdt'],
                        'text' => $text,
                    ]);
                    $sms->fn_send_sms_async();
                    $receiver['status'] = 'SENT';
                    $receiver['content'] = $text;
                }
                $result[$key] = $receiver;
            }
        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
        return $result;
    }
}