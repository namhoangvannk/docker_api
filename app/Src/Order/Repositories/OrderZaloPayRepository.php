<?php

namespace App\Src\Order\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\QRCode\QRCode;
use App\Helpers\QRCodePartner\QRCodePartner;
use App\Helpers\ZaloPay\ZaloPayOrder;
use App\Helpers\ZaloPay\ZaloPayOrderCallBack;
use App\Helpers\ZaloPay\ZaloPayOrderTracking;
use App\Models\Mongo\OrderZaloPay;
use App\Models\Mongo\OrderZaloPayCallback;
use App\Models\Mongo\OrderZaloPayTracking;
use App\Models\NKOrderPayment;
use App\Models\OrderData;
use App\Models\Orders;
use Illuminate\Support\Facades\Config;
use App\Helpers\Helpers;
use App\Libraries\Crypt\Crypt_Blowfish;
use Log;

class OrderZaloPayRepository extends RepositoryBase
{
    protected $helper;
    protected $zalopay_config;
    protected $qrcodePartner;
    protected $qrcode;
    protected $key1;
    protected $key2;
    protected $host;
    protected $api;

    public function __construct(Helpers $helper, QRCode $qrcode, QRCodePartner $qrcodePartner)
    {
        $this->helper = $helper;
        $this->zalopay_config = Config::get('zalopay');
        $this->qrcode = $qrcode;
        $this->qrcodePartner = $qrcodePartner;
        $this->key1 = Config::get('zalopay.key1');
        $this->key2 = Config::get('zalopay.key2');
        $this->host = Config::get('zalopay.host');
        $this->api = Config::get('zalopay.api');
    }

    public function create($request)
    {
        $result = ['success' => false, 'data' => array(), 'message' => ''];
        $params = $request->all();
        try{
            if(empty($params['item'])){
                return ['success' => false, 'data' => array(), 'message' => 'Vui lòng nhập mà đơn hàng'];
            }
            $order_id = $params['item'];
            $order_data = Orders::find(intval($order_id));
            if(empty($order_data)){
                return ['success' => false, 'data' => array(), 'message' => 'Không tìm thấy thông tin đơn hàng'];
            }
            /*Chỉ cho phép thanh toán 1 lần trên 1 order*/
            if(!empty(OrderZaloPay::where('item', $order_id)->first())){
                return ['success' => false, 'data' => array(), 'message' => 'Lỗi thanh toán'];
            }
            $params['apptransid'] = date('ymd').$params['item']."-".strtotime('now');
            $params['appid'] = intval(Config::get('zalopay.appid'));
            $params['apptime'] = (string)intval(round(microtime(true) * 1000, 0));
            $params['appuser'] = $order_data->b_phone;
            $params['amount'] = intval($order_data->total);
            $params['embeddata'] = "{\"promotioninfo\":\"\",\"merchantinfo\":\"\"}";
            $params['description'] = "Thanh toán đơn hàng #$order_id tại Nguyễn Kim";
            $zp = new ZaloPayOrder($params);
            $params['mac'] = $zp->setMac($this->key1);
            $params['status'] = OrderZaloPay::STATUS_NEW;
            $params['create_date'] = date('Y-m-d H:i:s');
            //Insert Mongo
            $model = new OrderZaloPay;
            $model->insert($params);

            $client = new \GuzzleHttp\Client(['base_uri' => $this->host]);
            //Sending application/x-www-form-urlencoded POST requests
            $response = $client->request('POST', $this->api['CreateOrder'], [
                'form_params' => $zp->getParams()
            ]);
            $body = $response->getBody();
            $data = $body->getContents();
            Log::info(" Zalo Pay Create Order: \n". var_export($zp->toArray(),true));
            $data = json_decode($data,true);
            Log::info(" Zalo Pay Create Order Result: \n". var_export($data,true));
            if($data['returncode'] === 1){
                $result['success'] = true;
                $result['data'] = [
                    'appid' => $params['appid'],
                    'zptranstoken' => $data['zptranstoken']
                ];
                if(!isset($params['is_mobile']) || !filter_var($params['is_mobile'], FILTER_VALIDATE_BOOLEAN)){
                    $qrcode_params = json_encode(['zptranstoken' => $data['zptranstoken'], 'appid' => Config::get('zalopay.appid')]);
                    $qrcode = $this->generateQRCode($qrcode_params);
                    $result['data']['qr_image'] = $qrcode;
                }
                //Update cscart_orders
                $order_data->payment_status = 'A';
                $order_data->update();
                //Create Order Payment (cscart_nk_order_payment)
                $op_params = [
                    'OrderID' => $order_id,
                    'PaymentType' => isset($params['paymenttype'])?$params['paymenttype']:'',
                    'PaymentGateway' => 'Zalo Pay',
                    'PaymentAmount' => $params['amount'],
                    'PaymentStatus' => 'A',
                    'TransactionID' => $model->apptransid,
                    'AdditionalData' => '',
                    'PaymentCategoryID' => '1',
                    'PaymentCategoryName' => 'Online',
                    'CreatedDate' => $params['create_date']
                ];
                $op = new NKOrderPayment();
                $op->fill($op_params);
                $op->save();
            }else{
                $result['message'] = $data['returnmessage'];
            }
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Tạo QRCode
     * Chuỗi JSON tạo QRCode Ex: {"zptranstoken": "4e3dec2f2f740fcf160df6d648a", "appid" : "1"}
     * @param $params
     * @return string
     */
    public function generateQRCode($params){
        try {
            $qrContetnSBT = $params;
            $errorCorrectionLevel = 'L';
            $matrixPointSize = 4;
            $filename = md5($qrContetnSBT . '|' . $errorCorrectionLevel . '|' . $matrixPointSize. '|'.time()) . '.png';
            $filename = storage_path($filename);
            $this->qrcode->toPNG($qrContetnSBT, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
            $image = file_get_contents($filename);
            $filetype = 'png';
            $base64 = 'data:image/' . $filetype . ';base64,' . base64_encode($image);
            unlink($filename);
            return $base64;
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function notify($request){
        $params = $request->all();
        $result = ['returncode' => -1, 'returnmessage' => 'Failure'];
        Log::info("ZaloPay Callback: ".var_export($params,true));
        try{
            $cb = new ZaloPayOrderCallBack($params);
            if($cb->checkMac($this->key2)){
                $result['returncode'] = -1;
                $result['returnmessage'] = 'Authenticate error!';
                OrderZaloPayCallback::insert(array_merge($params,$result));
                return $result;
            }

            //Get Order information
            $data = json_decode($params['data'],true);
            $order_id = $data['item'];
            $order = Orders::find($order_id);
            if(empty($order)){
                $result['returncode'] = -1;
                $result['returnmessage'] = 'Không tìm thấy đơn hàng';
                OrderZaloPayCallback::insert(array_merge($params,$result));
                return $result;
            }

            //Update Order Payment (cscart_nk_order_payment)
            $op_params = [
                'PaymentStatus' => 'P',
                'AdditionalData' => json_encode($params),
                'UpdatedDate' => date('Y-m-d H:i:s')
            ];
            $op = NKOrderPayment::where('OrderID','=',$order_id)->first();
            if(!empty($op)){
                $op->fill($op_params);
                $op->update();
            }

            //Insert into mongo
            $result['returncode'] = 1;
            $result['returnmessage'] = 'Success';
            OrderZaloPayCallback::insert(array_merge($params,$result));

            //Update trạng thái đơn hàng
            $order->payment_status = 'B';
            $order->status = 'O';
            $order->update();

            //Update trạng thái order
            $oz = OrderZaloPay::where('item',$order_id)->first();
            Log::info("ZaloPay Has OrderZaloPay: ".var_export($oz->toArray(),true));
            if(!empty($oz)){
                $oz->status = OrderZaloPay::STATUS_PAID;
                $oz->save();
            }

            $ozTracking = OrderZaloPayTracking::where('item',$order_id)->first();
            Log::info("ZaloPay Has OrderZaloPayTracking: ".$ozTracking);
            if(!empty($ozTracking)){
                $ozTracking->zptransid = $data['zptransid'] ? $data['zptransid'] : '';
                $ozTracking->returncode = 1;
                $ozTracking->returnmessage = 'Giao dịch thành công';
                $ozTracking->save();
            }

            return $result;
        } catch(\Exception $e) {
            Log::info('Zalo Pay Callback Error: '. $e->getMessage());
            return $result;
        }
    }

    public function trackingStatus($request, $order_id)
    {
        try{
            $order = OrderZaloPay::where('item',$order_id)->first();
            if(empty($order)){
                return ['success' => false, 'data' => array(), 'message' => 'Không tìm thấy giao dịch'];
            }
            $order_payment = NKOrderPayment::where('OrderID',$order_id)->first();
            if(empty($order_payment)){
                return ['success' => false, 'data' => array(), 'message' => 'Không tìm thấy thông tin thanh toán'];
            }

            if($order->payment_status == 'A'){
                return ['success' => false, 'data' => array(), 'message' => 'Chưa thanh toán'];
            }

            /*Nếu đã đối soát thì lấy thông tin từ mongo*/
            if($order_payment->PaymentReconcile){
                $ot = OrderZaloPayTracking::where(['item' => $order_id])->orderBy('create_date_ts', 'desc')->take(1)->first();
                if(!empty($ot) && intval($ot['returncode']) !== -117 ){
                    return [
                        'success' => true,
                        'data' => [
                            'returncode' => $ot['returncode'],
                            'returnmessage' => $ot['returnmessage'],
                            'isprocessing' => $ot['isprocessing'],
                            'zptransid' => $ot['zptransid'],
                            'apptransid' => $ot['apptransid']
                        ],
                        'message' => $ot['returnmessage']
                    ];
                }
            }
            /**
             * Giao dịch có mã lỗi -117 (Sai PIN) được ghi nhận khi người dùng nhập sai mật khẩu thanh toán.
             * Zalo Pay cho phép người dùng nhập sai mật khẩu nhiều hơn 1 lần
             */
            //Nếu giao dịch quá 48 tiếng thì không thể thực hiện tracking
            $paymentDate = strtotime($order_payment['CreatedDate']);
            $diff = abs(strtotime("now") - $paymentDate);
            if($diff > 2 * (60 * 60 * 24)){
                return ['success' => false, 'data' => array('apptransid' => $order->apptransid), 'message' => 'Sau 48 tiếng không thể đối soát trạng thái'];
            }

            //Kiểm tra trạng thái giao dịch
            $tracking_data = $this->callTrackingAPI($order_id, $order->apptransid);
            $returncode = intval($tracking_data['returncode']);
            if($returncode !== 1 && !$tracking_data['isprocessing']){
                $op_params = [
                    'PaymentStatus' => 'F',
                    'PaymentReconcile' => 1
                ];
            }
            //Tiếp tục đối soát nếu thành toán do lỗi người dùng nhập sai mật khẩu
            if($returncode === -117){
                $op_params['PaymentReconcile'] = 0;
            }
            $op_params['UpdatedDate'] = date('Y-m-d H:i:s');
            $op_params['TransactionID'] = $tracking_data['zptransid'];
            //Update trạng thái payment
            $order_payment->fill($op_params);
            $order_payment->update();

            $ot_params = [
                'item' => $order_id,
                'apptransid' => strval($order->apptransid),
                'create_date' => date('Y-m-d H:i:s'),
                'create_date_ts' => strtotime(date('Y-m-d H:i:s'))
            ];
            $data = array_merge($ot_params,$tracking_data);
            $data['zptransid'] = strval($data['zptransid']);
            OrderZaloPayTracking::insert($data);
            return [
                'success' => true,
                'data' => [
                    'returncode' => $data['returncode'],
                    'returnmessage' => $data['returnmessage'],
                    'isprocessing' => $data['isprocessing'],
                    'zptransid' => $data['zptransid'],
                    'apptransid' => $order->apptransid
                ],
                'message' => $data['returnmessage']
            ];

        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    private function callTrackingAPI($order_id, $apptransid){
        $tk = new ZaloPayOrderTracking([
            'appid' => Config::get('zalopay.appid'),
            'apptransid' => $apptransid
        ]);
        $tk->setMac($this->key1);
        $client = new \GuzzleHttp\Client(['base_uri' => $this->host]);
        //Sending application/x-www-form-urlencoded POST requests
        $response = $client->request('POST', $this->api['TrackingStatus'], [
            'form_params' => $tk->getParams()
        ]);
        $data = $response->getBody()->getContents();
        $data = json_decode($data,true);
        Log::info("Zalo Pay Order Tracking (OrderID:$order_id) : ".var_export($data,true));
        return $data;
    }

    private function ConvertIntToStringByKeys($data,$keys){
        foreach ($keys as $key) {
            if(array_key_exists($key,$data)){
                $data[$key] = strval($data[$key]);
            }
        }
        return $data;
    }
}