<?php

namespace App\Src\Order\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use App\Models\Payments;
use Illuminate\Support\Facades\Config;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;
use App\Models\Mongo\Wards as MWards;
use App\Src\Order\Transformers\OrderProductSalesTransformer;
use App\Src\Order\Transformers\OrderTransformer;

use DB;
use App\Models\Orders;
use App\Models\OrderDetails;
use App\Models\OrderData;
use App\Models\NKOrderPayment;
use App\Models\CscartNkUserGateway;
use App\Models\ProductPopularity;
use App\Models\Mongo\OrderDetail;
use App\Models\Mongo\Products as MProducts;
use App\Models\OrderCoupons;
use App\Models\Mongo\OrderCoupons as MOrderCoupons;
use App\Models\Mongo\Orders as MOrders;
use App\Models\NkTragopOrders;
use App\Models\ProfileFields;
use App\Models\ProfileFieldsData;
use App\Models\NkTragop;
use App\Models\NkFlashSales;
//use App\Models\EmailSending;

use Log;

class OrderRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;

    public function __construct(Helpers $helper, Manager $fractal, 
                                OrderTransformer $transformer)
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new Orders;
        $this->transformer = $transformer;
        //yap

    }

    public function all($request)
    {
        $time = strtotime(date('2017-11-01 00:00:00'));
        $params = $request->all();
        $data = Orders::select('order_id', 'timestamp', 'utm_campaign')
            ->where('utm_source', '=', $params['utm_source'])
            ->where('timestamp', '>=', $time)
            ->get();

        $rs = [];
        foreach ($data as $value) {
            $rs_order = [
                'transaction_id' => $value->order_id,
                'offer_id' => 'nguyenkim',
                'transaction_time' => date('Y-m-d H:i:s', $value->timestamp),
                'utm_campaign' => $value->utm_campaign,
                'signature' => ''
            ];

            $orderdetail = OrderDetails::select('product_id', 'amount', 'product_code', 'price')->
                where('order_id', $value->order_id)->get();

            $tmp = [];

            foreach ($orderdetail as $item) {
                $product_id = intval($item['product_id']);
                $product = MProducts::select('product_id', 'product_code', 'product_name', 'seo_names')->where('product_id', '=', $product_id)->first();
                $seo_name = json_decode($product->seo_names, true);
                $link = isset($seo_name['name']) ? $seo_name['name'] : '';
                $tmps = [
                    'id' => $item['product_id'],
                    'sku' => $item['product_code'],
                    'price' => $item['price'],
                    'name' => $product->product_name,
                    'quantity' => $item['amount'],
                    'link' => 'https://www.nguyenkim.com/' . $link . '.html'
                ];
                $tmp[] = $tmps;
            }
            $rs_order['products'] = $tmp;
            $rs[] = $rs_order;

        }
        return $rs;
    }

    public function create($request)
    {
        $params = $request->all();
        $model = $this->model;
        $data_mongo = [];
        Log::info('Create Order: ' . var_export($params, true));
        try {
            $order = $params['order'];
            $order_detail = $params['order_details'];
            $order_data = $params['order_data'];

            // if(isset($order['payment_id'])) {
            //     $payment_id = intval($order['payment_id']);
            //     if(in_array($payment_id, [6])) {
            //         $order['status']  = 'O';
            //     }
            // }

            //bo sung search phone
			$order['b_phone'] = preg_replace('/\s+/', '', $order['b_phone']);
			$order['s_phone'] = preg_replace('/\s+/', '', $order['s_phone']);
			$order['phone'] = preg_replace('/\s+/', '', $order['phone']);			
            $order['nk_search_phone']  = $order['b_phone'];

            //flash sales
            //flash sales check
            $save_fs = false;
            if(isset($order['flash_sales'])
                && is_array($order['flash_sales'])
                && !empty($order['flash_sales'])){
                $order['nk_has_flash_sales']='Y';
                $save_fs=true;
            }


            $model->fill($order);
            $model->save();

            $order_id = $model->order_id;
            $order['order_id'] = intval($order_id);
            $data_mongo['order_id'] = intval($order_id);
            $data_mongo['data'] = json_encode($params);
            
            $order_detail_push = [];
            foreach ($order_detail as $value) {
                $value['order_id'] = $order_id;
				$value['item_id'] = (string) $value['item_id'];
                $order_detail_push[] = $value;

                // Update ProductPopularity
                $product_id = $value['product_id'];
                $product_pop = ProductPopularity::find($product_id);
                if($product_pop == null) {
                    $product_pop = new ProductPopularity();
                    $product_pop->product_id = $product_id;
                    $product_pop->save();
                    continue;
                }

                $product_pop->bought = intval($product_pop->bought) + 1;
                $product_pop->total = intval($product_pop->total) + intval($value['amount']);
                $product_pop->update();
            }

            $order_data_push = [];
            foreach ($order_data as $value) {
                $value['order_id'] = $order_id;
                $order_data_push[] = $value;
            }

            //save flash save
            if($save_fs == true){
                foreach ($order['flash_sales'] as &$fs){
                    $fs['order_id']=$order_id;
                    $fs['status']='A';
                }
                NkFlashSales::insert($order['flash_sales']);
            }

            // Insert to detail and data
            OrderDetails::insert($order_detail_push);
            OrderData::insert($order_data_push);

			$this->updateOrderPayment($params['order'], $order_id);
            // Insert order coupne
            $tmp = [];
            if (!empty($order['coupon_code'])) {
                $coupon_code = $order['coupon_code'];

                foreach ($coupon_code as $value) {
                    $order_coupon_model = new OrderCoupons();
                    $order_coupon['order_id'] = $order_id;
                    $order_coupon['coupon_code'] = strtoupper($value['code']);
                    $order_coupon['coupon_value'] = $value['value'];
                    $order_coupon_model->fill($order_coupon);
                    $order_coupon_model->save();

                    // Update to mongo
                    $order_coupon_mongo['order_id'] = intval($order['order_id']);
                    $order_coupon_mongo['user_id'] = intval($order['user_id']);
                    $order_coupon_mongo['subtotal'] = floatval($order['subtotal']);
                    $order_coupon_mongo['total_discount'] = isset($order['subtotal_discount']) ? floatval($order['subtotal_discount']) : 0;
                    $order_coupon_mongo['price_discount'] = isset($order['price_discount']) ? floatval($order['price_discount']) : 0;
                    $order_coupon_mongo['coupon_code'] = strtoupper($value['code']);
                    $order_coupon_mongo['coupon_value'] = floatval($value['value']);
                    $order_coupon_mongo['coupon_percent'] = isset($order['coupon_percent']) ? floatval($order['coupon_percent']) : 0;
                    $order_coupon_mongo['order_status'] = strtoupper($order['status']);
                    $order_coupon_mongo['firstname'] = $order['firstname'];
                    //$order_coupon_mongo['lastname'] = $order['lastname'];
                    $order_coupon_mongo['phone'] = $order['phone'];
                    $order_coupon_mongo['email'] = $order['email'];
                    $order_coupon_mongo['order_date'] = strtotime(date('Y-m-d H:i:s'));
                    $tmp[] = $order_coupon_mongo;
                }
                MOrderCoupons::insert($tmp);
            }

            // Insert order coupn theo ma visa, jcb, master card
            if (!empty($order['card_number'])) {
                // Update to mongo
                $order_coupon_mongo['order_id'] = intval($order['order_id']);
                $order_coupon_mongo['user_id'] = intval($order['user_id']);
                $order_coupon_mongo['subtotal'] = floatval($order['subtotal']);
                $order_coupon_mongo['total_discount'] = isset($order['subtotal_discount']) ? floatval($order['subtotal_discount']) : 0;
                $order_coupon_mongo['price_discount'] = isset($order['price_discount']) ? floatval($order['price_discount']) : 0;
                $order_coupon_mongo['coupon_code'] = strtoupper($order['card_number']);
                $order_coupon_mongo['coupon_value'] = 0;
                $order_coupon_mongo['coupon_percent'] = isset($order['coupon_percent']) ? floatval($order['coupon_percent']) : 0;
                $order_coupon_mongo['order_status'] = strtoupper($order['status']);
                $order_coupon_mongo['firstname'] = $order['firstname'];
                $order_coupon_mongo['phone'] = $order['phone'];
                $order_coupon_mongo['email'] = $order['email'];
                $order_coupon_mongo['order_date'] = strtotime(date('Y-m-d H:i:s'));
                $tmp[] = $order_coupon_mongo;
                MOrderCoupons::insert($tmp);
            }

            // Update don hang tra gop neu co
            // if (!empty($params['order_tragop'])) {
            //     $order_tragop_model = new NkTragopOrders();
            //     $order_tragop = $params['order_tragop'];
            //     $order_tragop['order_id'] = $order_id;
            //     $order_tragop_model->fill($order_tragop);
            //     $order_tragop_model->save();
            // }

            // update user data
            if(!empty($params['user_data'])){
                $user_data = $params['user_data'];
                $object_type = $user_data['object_type'];
                $fields = $user_data['fields'];

                if(!empty($fields)) {
                    $delete = ProfileFieldsData::where('object_id', '=', $order_id)
                        ->where('object_type', '=', $object_type);
                    $delete->delete();

                    $fields_info = ProfileFields::select('field_id', 'field_type', 'section')
                        ->whereIn('field_id', array_keys($fields))->get();

                    $fields_info_tmp = [];
                    foreach ($fields_info as $fields_info_key => $fields_info_value) {
                        $fields_info_tmp[$fields_info_value->field_id] = $fields_info_value->field_type;
                    }

                    foreach ($fields as $field_id => $fitem) {
                        $_data = [];
                        $_data['object_type'] = $object_type;
                        $_data['object_id'] = $order_id;
                        $_data['field_id'] = $field_id;
                        $_data['value'] = $fitem;
                        ProfileFieldsData::updateOrCreate($_data);
                    }
                }
            }
            
            // Write log to mongo
            if(!empty($data_mongo))
                MOrders::insert($data_mongo);

			//Create mail sending
            /*$email = new EmailSending();
            $mail_data['receiver'] = $order['email'];
            $mail_data['object_id'] = $order['order_id'];
            $mail_data['object_type'] = "O";
            $mail_data['content'] = 'Create new Order';
            $mail_data['status'] = 'N';
            $mail_data['send_date'] = strtotime(date("Y-m-d H:i:s"));
            $mail_data['is_delete'] = 'N';
            $mail_data['area'] = 'C';
            $mail_data['retry'] = 0;
            $email->fill($mail_data);
            $email->save();*/

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

	public function updateOrderPayment($data,$order_id)
    {
        try{
            if (!empty($data) && !empty($order_id) )
            {
                Log::info(date('Y-m-d H:i:s'));
                Log::info('Order ID: ' . $order_id);
                Log::info('Create Order: ' . var_export($data, true));
                $payment_method_data = isset($data['payment_method_data']) ? $data['payment_method_data'] : [];
                $final_card_info=  isset($data['final_card_info']) ? $data['final_card_info'] : [];
                $nkOrderPayment                 = new NKOrderPayment();
                $payment['OrderID']             = $order_id;
                if(!empty($payment_method_data))
                {
                    $payment['PaymentType']         = isset($payment_method_data['payment_id']) ? $payment_method_data['payment_id'] : '0';
                    $payment['PaymentGateway']      = isset($payment_method_data['payment']) ? $payment_method_data['payment'] : 'payment';
                    $payment['PaymentCategoryID']   = isset($payment_method_data['payment_category']) ? $payment_method_data['payment_category'] : 'payment_category';
                    $payment['PaymentCategoryName'] = isset($payment_method_data['payment']) ? $payment_method_data['payment'] : 'payment';
                    $payment['PaymentStatus']       = isset($payment_method_data['status']) ? $payment_method_data['status'] : 'A';
                }else{
                    $payment['PaymentType']         = isset($data['payment_id']) ? $data['payment_id'] : '0';
                    $payment['PaymentGateway']      = isset($data['installment']) ? $data['installment'] : 'payment';
                    $payment['PaymentCategoryID']   = '3';
                    $payment['PaymentCategoryName'] = 'Trả góp';
                    $payment['PaymentStatus']       = isset($data['payment_status']) ? $data['payment_status'] : 'A';
                }
                $payment['CardName']       =      isset($final_card_info['card_name']) ? $final_card_info['card_name'] : ''; //tên chủ thẻ
                $payment['PaymentAmount']       = isset($data['subtotal']) ? $data['subtotal'] : 0 ;
                $payment['CreatedDate']         = date("Y-m-d h:m:s");
                $payment['UpdatedDate']         = date("Y-m-d h:m:s");
                Log::info('CreateOrderPayment: ' . var_export($payment, true));
                $nkOrderPayment->fill($payment);
                $nkOrderPayment->save();
            }
        } catch (\Exception $e) {
            //TODO Console log
        }
    }

    public function trackingStatus($request)
    {
        if($request->input('transaction_id') == '')
            return ['success' => false, 'message' => 'Chưa có tham số'];

        $transaction_id = $request->input('transaction_id');

        $headers = $request->header();
        $user_gateway = CscartNkUserGateway::checkValidUser(
            $headers['authorization'][0],
            $headers['password'][0]
        );

        $order = Orders::where('order_id', $transaction_id)
            ->where('utm_source', $user_gateway->partner_name)->first();

        if($order == null)
            return ['success' => false, 'message' => 'Không tìm thấy đơn hàng'];

        $status_id = $this->helper->mappingOrderStatus($order->status);
        $result = [
            'success' => true,
            'status' => $status_id
        ];

        return $result;
    }

    public function update($request, $order_id)
    {
        $params = $request->all();
        try {
            $order = Orders::find($order_id);
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Order ID: ' . $order_id);
            Log::info('Params: ' . var_export($params, true));

            if($order == null)
                abort(400, 'Đơn hàng không tồn tại');

			//hunglm update new order status
			if(!empty($params['status']) && $params['status'] == 'P'){
				$params['status'] = 'O';
				$params['payment_status'] = 'B';
			}
			
            $order->fill($params);
            $order->update();
            $this->updateStatus($params, $order_id);

            // update order data
            if(isset($params['order_data'])) {
                $data =  $params['order_data'];
                foreach ($data as $ckey => $value) {
                    $order_data = OrderData::where('order_id', $order_id)
                        ->where('type', $value['type'])->first();
                    if($value['type'] == 'P'){
                        $this->updateStatusOrderDataParam($value , $params, $order_id);
                    }
                    if($order_data == null) {
                        $order_data = new OrderData();
                        $value['order_id'] = intval($order_id);
                        $value['type'] = $value['type'];
                        $value['data'] = $value['data'];
                        $order_data->fill($value);
                        $order_data->save();
                    } else {
                        DB::table('cscart_order_data')
                        ->where('order_id', $order_id)
                        ->where('type', $value['type'])
                        ->update(['data' => $value['data']]);
                    }
                }
            }

             // Update don hang tra gop neu co
            if (!empty($params['order_tragop'])) {
                $order_tragop = $params['order_tragop'];
                $order_tragop['order_id'] = $order_id;

                $order_tragop_model = NkTragopOrders::where('order_id', $order_id)->first();

                if(empty($order_tragop_model)){
                    $order_tragop_model = new NkTragopOrders();
                    $order_tragop_model->fill($order_tragop);
                    $order_tragop_model->save();
                } else {
                    $this->updateStatusParam($params, $order_tragop, $order_id, 'order_tragop');
                    $model= NkTragopOrders::find($order_tragop_model['id']);
                    $model->fill($order_tragop);
                    $model->update();
                }
            }

			// Update thong tin the
            if (!empty($params['order_credit'])) {
                $data = $params['order_credit'];
                $data['order_id'] = $order_id;

                $tragop_credit= NkTragop::find($order_id);
                if(empty($tragop_credit)){
                    $tragop_credit = new NkTragop();
                    $tragop_credit->fill($data);
                    $tragop_credit->save();
                } else {
                    $this->updateStatusParam($params, $data, $order_id,'order_credit');
                    $tragop_credit->fill($data);
                    $tragop_credit->update();
                }
            }

            // return ['success' => true, 'message' => 'Cập nhật thành công'];
            return $this->fractal->createData(new Item($order, $this->transformer))->toArray();
        } catch (\Exception $e) {
            abort(400, $e->getMessage());
        }
        abort(500, 'Có lỗi xảy ra!');
        return [];

    }

	 public function updateStatus($params, $order_id)
    {
		try{
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Order ID: ' . $order_id);
            Log::info('params updateStatus: ' .  var_export($params, true));
            $orderNKOrderPayment = NKOrderPayment::where('OrderID', $order_id)->first();
			if ( !empty($orderNKOrderPayment)) {

				DB::table('cscart_nk_order_payment')
				  ->where('OrderID', $order_id)
				  ->update(
					  [
						  'PaymentStatus' => isset($params['payment_status']) ? $params['payment_status'] : 'A',
						  'UpdatedDate'   => date("Y-m-d h:m:s"),
					  ]
				  );

			}
		} catch (\Exception $e) {
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Error updateStatus Order ID: ' . $order_id);
        }
    }

    public function updateStatusParam($params, $data, $order_id, $type)
    {
		try{
			$orderNKOrderPayment = NKOrderPayment::where('OrderID', $order_id)->first();
			if ( !empty($orderNKOrderPayment)) {
				$dataUpdate = [];
				if ($type == 'order_credit') {
					$dataUpdate = [
						'PaymentGateway' => isset($data['nganhang']) ? $data['nganhang'] : '',
						'UpdatedDate'    => date("Y-m-d h:m:s"),
					];

				}
				if ($type == 'order_tragop') {
					$dataUpdate = [
						'PaymentGateway' => isset($data['credit_inst']) ? $data['credit_inst'] : '',
						'UpdatedDate'    => date("Y-m-d h:m:s"),
					];
                }
                $dataUpdate['PaymentStatus'] = isset($params['payment_status']) ? $params['payment_status'] : 'A';

				DB::table('cscart_nk_order_payment')
				  ->where('OrderID', $order_id)
				  ->update($dataUpdate);

			}
		} catch (\Exception $e) {
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Error updateStatusParam Order ID: ' . $order_id);
        }
    }

    public function updateStatusOrderDataParam($params, $status, $order_id)
    {
		try{
			$data = isset($params['data_payment_row']) ? $params['data_payment_row'] : [];
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Order ID: ' . $order_id);
            Log::info('data updateStatusOrderDataParam: ' . var_export($params['data_payment_row'], true));
            $orderNKOrderPayment = NKOrderPayment::where('OrderID', $order_id)->first();
			if ( !empty($orderNKOrderPayment)) {
			    $dataUpdate = array();
                $dataUpdate['PaymentGateway'] = isset($data['bank_code']) ? $data['bank_code'] : ' ';
                $dataUpdate['PaymentStatus'] = isset($status['payment_status']) ? $status['payment_status'] : 'A';
                $dataUpdate['CardNumber'] = isset($data['card_number']) ? $data['card_number'] : ' ';
                $dataUpdate['TransactionID'] = isset($data['req_id']) ? $data['req_id'] : ' ';
                $dataUpdate['UpdatedDate'] = date("Y-m-d h:m:s");
                if(!empty($data['additional_data'])){
                    $dataUpdate['AdditionalData'] = isset($data['additional_data']) ? $data['additional_data'] : ' ';
                }
                DB::table('cscart_nk_order_payment')
                    ->where('OrderID', $order_id)
                    ->update($dataUpdate);
			}
		} catch (\Exception $e) {
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Error updateStatusOrderDataParam Order ID: ' . $order_id);
        }
    }

    public function trackingProductSale($request)
    {
        $params = $request->all();
        $data = [];
        foreach($params as $product) {
            $tmp = [
                'product_code' => $product['product_code'],
                'total_sales' => 0
            ];
            $startdate = strtotime($product['startdate']);
            $enddate = strtotime($product['enddate']);
            $query = [
                [
                    '$match' => [
                        'product_code' => $product['product_code'],
                        'timestamp' => ['$gte' => $startdate, '$lte' => $enddate]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$product_code',
                        'total' => [
                            '$sum' => '$amount'
                        ],
                    ],
                ]
            ];
            $list = OrderDetail::raw()->aggregate($query);

            foreach($list as $item) {
                $tmp['total_sales'] = $item->total;
            }
            $data[] = $tmp;
        }

        $result = [
            'success' => true,
            'message' => '',
            'data' => $data
        ];

        return $result;
    }

	public function show($request, $id)
    {
        try {

            $data = Orders::where('order_id','=',$id)->first();
			$pro_data = DB::select("SELECT cscart_order_details.*, cscart_product_descriptions.product, cscart_product_descriptions.display_name, cscart_products.status as product_status FROM cscart_order_details "
                . "LEFT JOIN cscart_product_descriptions ON cscart_order_details.product_id = cscart_product_descriptions.product_id AND cscart_product_descriptions.lang_code = 'vi' "
                . "LEFT JOIN cscart_products ON cscart_order_details.product_id = cscart_products.product_id "
                . "WHERE cscart_order_details.order_id = {$id} ORDER BY cscart_product_descriptions.product");

            $tmp = array();
            foreach ($pro_data as $k => $p){
                $tmp[$p->item_id] = $p;
            }
            $data['products'] = $tmp;

            $ord_data = DB::select("SELECT type, data FROM cscart_order_data WHERE order_id = {$id}");
            $tmp = array();
            foreach ($ord_data as $k => $p){
                $tmp[$p->type] = $p->data;
            }
            $data['extra_data'] = $tmp;
            if (empty($data))
                abort(400, 'Đơn hàng không tồn tại');

            return $data;
        } catch (\Exception $e) {
            abort(400, $e->getMessage());
        }
        abort(500, 'Có lỗi xảy ra!');
        return [];

    }

    /**
     * @author LeMinhHung KHONG DUOC XOA
     * @param $request
     * @return array
     */
    public function updateMall($request)
    {
        $params = $request->all();

        $minuteAgo = 60;
        $orderId = null;
        if(isset($params['minuteAgo'])){
            $minuteAgo = $params['minuteAgo'];
        }

        $orderQuery = Orders::select('order_id', 'target_mall_id', 's_state', 's_county', 's_city')
            ->whereRaw('timestamp > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL ? MINUTE)) and s_state is not null and s_state in (\'001\', \'005\', \'027\') and (target_mall_id is null or target_mall_id = \'\') ', [$minuteAgo]);
        if(isset($params['orderId'])){
            $orderQuery = $orderQuery->whereRaw('order_id = ?', [$params['orderId']]);
        }
        $orders = $orderQuery->get();

        $n_update = 0;
        foreach ($orders as $order) {
            $wards = MWards::select(
                'mall_id',
                'state_code',
                'district_code',
                'code'
            );
            if (!empty($order['s_state'])) {
                $wards = $wards->where('state_code', '=', $order['s_state']);
            }
            if (!empty($order['s_county'])) {
                $wards = $wards->where('district_code', '=', $order['s_county']);
            }
            if (!empty($order['s_city'])) {
                $wards = $wards->where('code', '=', $order['s_city']);
            }
            $wards = $wards->orderBy('state_code', 'DES')->orderBy('district_code', 'DES')->orderBy('code', 'DES');
            $w = $wards->skip(0)->take(1)->get();

            if (count($w) > 0) {
                $order = Orders::find($order['order_id']);
                $order->fill([
                    'target_mall_id' => $w[0]['mall_id']
                ]);
                $order->save();
                $n_update++;
                Log::info('MALL UPDATE:' . $order['order_id'] . '|'. $w[0]['mall_id']);
            } else {
                Log::info('MALL NOT FOUND:' . $order['order_id'] . '|'. $order['s_state'] . '|'. $order['s_county'] . '|' . $order['s_city']);
            }
        }

        return [
            'number_update' => $n_update
        ];
    }

    /**
     * Cập nhật đơn hàng tự động xác nhận
     * Trạng thái đơn hàng được thay đổi thành "Đã xác nhận" (E)
     * @param $request
     * @param $order_id
     * @return array
     */
    function updateStatusAutoOrderConfirm($request, $order_id){
        $app_config = Config::get('app.auto_order_confirm');
        $tra_gop = Payments::active()->category(3)->get()->toArray();
        $tra_gop_ids = array_column($tra_gop,'payment_id');
        if(!$app_config['enable']){
            return $result = [
                'success' => false,
                'message' => 'Vui lòng bật chức năng tự động xác nhận đơn hàng (AutoOrderConfirm)'
            ];
        }
        $oi = Orders::find($order_id);
        if(empty($oi)){
            return $result = [
                'success' => false,
                'message' => 'Không tìm thấy đơn hàng'
            ];
        }

        if(in_array($oi->payment_id,$tra_gop_ids) || !empty($oi->mall_id)){
            return $result = [
                'success' => false,
                'message' => 'Đơn hàng trả góp hoặc nhận tại trung tâm'
            ];
        }
        if($oi->status !== $app_config['required_status']){
            return $result = [
                'success' => false,
                'message' => 'Trạng thái không hợp lệ'
            ];
        }

        //Chỉ nhận đơn hàng nhỏ hơn hoặc bằng 2 triệu
        if($oi->total > $app_config['required_price']){
            return $result = [
                'success' => false,
                'message' => 'Điều kiện giá không hợp lệ'
            ];
        }

        /*
         * Update status from O to E
         */
        $oi->fill(['status' => 'E']);
        $oi->update();

        return $result = [
            'success' => true,
            'message' => 'Đơn hàng đủ điều kiện tự động xác nhận. Nhân viên bán hàng không thực hiện cuộc gọi xác nhận tới khách hàng'
        ];
    }
    /**
     * update card_name cho trả góp
     */
    public function updateCardName($params)
    {
        try{
            $order_id = $params['order_id'];
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Order ID: ' . $order_id);
            Log::info('data updateCardName: ' . var_export($params, true));
            $orderNKOrderPayment = NKOrderPayment::where('OrderID', $order_id)->first();
            if ( !empty($orderNKOrderPayment)) {
                $dataUpdate = array();
                $dataUpdate['CardName'] = $params['card_name'] ? $params['card_name'] : ' ';
                DB::table('cscart_nk_order_payment')
                    ->where('OrderID', $order_id)
                    ->update($dataUpdate);
            }
        } catch (\Exception $e) {
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Error updateCardName Order ID: ' . $order_id);
        }
    }

}
