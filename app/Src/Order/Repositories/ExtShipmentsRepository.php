<?php

namespace App\Src\Order\Repositories;

use DB;
use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use App\Src\Order\Collections\ExtShipmentsCollection;
use App\Src\Order\Transformers\ExtShipmentsTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Models\Mongo\ExtShipmentLogs as MExtShipmentLogs;
use App\Models\Mongo\ExtShipmentRequestLogs as MExtShipmentRequestLogs;
use DateTime;
use App\Models\ExtShipments;
use App\Models\Orders;

use Log;

class ExtShipmentsRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;

    public function __construct(
        Manager $fractal,
        ExtShipmentsCollection $collection,
        ExtShipmentsTransformer $transformer)
    {
        $this->fractal = $fractal;
        $this->collection = $collection;
        $this->transformer = $transformer;
        $this->model = new ExtShipments();
    }

    public function all($request)
    {
        $result = $this->allByMongo($request);
        return $result;
    }

    private function allByMongo($request)
    {
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 100);

        $model = MExtShipmentLogs::with('status')->select(
            'shipment_id',
            'vendor_id',
            'tracking_id',
            'created_date',
            'evt_status',
            'evt_description',
            'evt_datetime',
            'evt_city',
            'evt_postcode',
            'evt_state',
            'evt_country'
        );

        if ($request->input('shipment_id') != null) {
            $model = $model->where('shipment_id', '=', $request->input('shipment_id'));
        }

        if ($request->input('tracking_id') != null) {
            $model = $model->where('tracking_id', '=', intval($request->input('tracking_id')));
        }

        if ($request->input('evt_status') != null) {
            $model = $model->where('evt_status', '=', $request->input('evt_status'));
        }

        $model = $model->orderBy('evt_datetime', 'desc');

        $list = $model->skip($offset)->take($limit)->get();
        $resource = new Collection($list, new ExtShipmentsTransformer());
        $data = $this->fractal->createData($resource)->toArray();

        $paging = new PagingComponent($model->count(), $offset, $limit);
        $data['paging'] = $paging->render();
        return $data;
    }

    public function create($request)
    {
        $params = $request->all();
        $model = $this->model;

        try {
            $data_mysql['shipment_id'] = $params['shipment_id'];
            $data_mysql['tracking_id'] = $params['delivery_confirmation_no'];
            $data_mysql['content'] = $params['content'];
            $data_mysql['evt_status'] = $params['evt_status'];
            $data_mysql['main_code'] = $params['main_code'];
            $data_mysql['vendor_id'] = $params['vendor_id'];
            $data_mysql['evt_datetime'] = (new \DateTime())->format('Y-m-d H:i:s');
            $data_mysql['evt_description'] = 'NK Push Shipping';
            $model->fill($data_mysql);
            $model->save();

            MExtShipmentLogs::insert($data_mysql);

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    public function tracking($request)
    {
        $params = $request->all();

        try {
            $shipment_id = $params['shipment_id'];
            $extShipment = ExtShipments::find($shipment_id);

            if (!empty($extShipment)) {
                $current_timestamp = DateTime::createFromFormat("Y-m-d H:i:s", $extShipment['evt_datetime'])->getTimestamp();
				//When new tracking
                if($extShipment['evt_status'] == 71000)
                    $current_timestamp = 0;
                $max_timestamp = 0;
                $max_event = array();

                // Update to mongo
                $events = $params['events'];
                foreach ($events as $event) {
                    $new_event = array();
                    $new_event['shipment_id'] = $shipment_id;
                    $new_event['tracking_id'] = $params['tracking_id'];
                    $new_event['vendor_id'] = $params['vendor_id'];
                    $new_event = array_merge($new_event, $event);

                    $timestamp = DateTime::createFromFormat("Y-m-d H:i:s", $event['evt_datetime'])->getTimestamp();

                    if ($timestamp > $current_timestamp) {
                        MExtShipmentLogs::insert($new_event);
                        if ($timestamp > $max_timestamp) {
                            $max_timestamp = $timestamp;
                            $max_event = $new_event;
                        }
                    }
                }
                if (!empty($max_event)) {
                    $extShipment->fill($max_event);
                    $extShipment->update();
					
					// 13: DHL
                    if ($extShipment['vendor_id'] == 13) {
                        $statusGoups = DB::select("SELECT
                                                o.order_id,
                                                og.order_status,
                                                og.payment_status,
                                                og.shipping_status,
                                                og.installment_status
                                            FROM
                                                cscart_nk_b2s_order_erp e
                                            JOIN cscart_orders o ON o.order_id = e.order_id
                                            JOIN cscart_payments p ON p.payment_id = o.payment_id
                                            JOIN cscart_nk_order_status_group og 
                                            ON og.ext_portal_id = '{$max_event['evt_status']}'
                                            AND og.process_id = p.payment_category
                                            AND og.ext_shipment_unit = 'DHL'
                                            WHERE
                                                e.MainCode = '{$extShipment['main_code']}'");
												
						
						Log::info('Info search status group: ' . $max_event['evt_status'] . ':' . $extShipment['main_code']);												
                        if ($statusGoups && count($statusGoups) > 0) {							 
                            $statusGoup = reset($statusGoups);
							Log::info('Update for order:' . var_export($statusGoup, true));
                            Log::info('Update for orderId:' . $statusGoup->order_id);
							
                            $order = Orders::find(intval($statusGoup->order_id));
                            // FIXME - unset payment_status when update tracking
                            $params = [
                                'status' => $statusGoup->order_status,
//                                'payment_status' => $statusGoup->payment_status,
                                'shipping_status' => $statusGoup->shipping_status,
                                'installment_status' => $statusGoup->installment_status
                            ];
                            $order->fill($params);
                            $order->update();
                        }
                    }
					
                }
            }
            return $this->fractal->createData(new Item($extShipment, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    public function log($request)
    {
        $params = $request->all();

        try {
            $now = new DateTime();
            $params['timestamp'] = $now->getTimestamp();
            $params['created_at'] = $now->format('Y-m-d H:i:s');
            MExtShipmentRequestLogs::insert($params);

            return array(
                "status_code" => '200',
                "description" => "Sucess"
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

}
