<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 31/05/2018
 * Time: 9:33 AM
 */

namespace App\Src\Order\Repositories;
use App\Base\Repositories\RepositoryBase;
use App\Models\Mongo\GHNShipmentTracking;
use App\Models\ExtShipmentsGHN;
use Illuminate\Support\Facades\Validator;
use App\Models\Orders;
use DB;
use Log;

class GHNTrackingRepository extends RepositoryBase {
    protected $model;
    protected $collection;

    public function __construct(){
        $this->model = new ExtShipmentsGHN();
    }

    public function allById($request,$id){
        /* FIXME
        $trackingExits = GHNShipmentTracking::where('shipment_id',$id)->get(['shipment_id'])->toArray();
        $trackingItem  = ExtShipmentsGHN::where('shipment_id',$id)->get()->first()->toArray();
        if(empty($trackingExits) && !empty($trackingItem) ){
            if((empty($trackingItem['status_code']) || $trackingItem['status_code'] == 'ReadyToPick')){
                GHNShipmentTracking::insert([
                    "shipment_id"       => $trackingItem['tracking_id'],
                    "main_code"         => $trackingItem['main_code'],
                    "vendor_id"         => 14,
                    "tracking_id"       => $trackingItem['tracking_id'],
                    "status_code"       => "ReadyToPick",
                    "description"       => "Chờ xử lý",
                    "tracking_time"     => $trackingItem['tracking_time'],
                    "current_warehouse" => $trackingItem['current_warehouse']
                ]);
            }
        }
        */
        $model = GHNShipmentTracking::with('status')->select(
    'shipment_id',
            'status_code',
            'tracking_time',
            'current_warehouse'
        );

        $model = $model->where('shipment_id', '=', $id);
        $model = $model->orderBy('tracking_time', 'desc');
        $data = $model->skip(0)->take(100)->get()->toArray();
        return [
            'status'=> true,
            'msg'=>'success',
            'data'=>$data
        ];
    }

    /*
     * ## CREATE
     * insert MySQL data
     * @params $request
     * @return $data
     */
    public function create($request){
        $params = $request->all();
        $validator = Validator::make($params,[
            'shipment_id' => ['required','string'],
            'tracking_id' => ['required','string'],
            'main_code'   => ['required','string'],
            'vendor_id'   => ['required','integer']
        ]);
        if($validator->fails()){
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }
        $model = new ExtShipmentsGHN();
        try {
            $data_fill['shipment_id'] = $params['shipment_id'];
            $data_fill['tracking_id'] = $params['tracking_id'];
            $data_fill['status_code'] = $params['status_code'];
            $data_fill['main_code'] = $params['main_code'];
            $data_fill['vendor_id'] = $params['vendor_id'];
            $data_fill['tracking_time'] = date('Y-m-d H:i:s');
            $data_fill['current_warehouse'] = $params['current_warehouse'];
            $data_fill['description'] = 'Chờ xử lý';
            $model->fill($data_fill);
            $model->save();


            return [
                'code'=> 200,
                'status' => true,
                'msg' => 'success',
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'msg' => $e->getMessage()

            ];
        }
    }

    /*
     * ## TRACKING SHIPING STATUS
     * update data to MySQL
     * @params $request
     * @return $data
     */
    public function tracking($request){
        $params = $request->all();
        Log::info('ParamsGhnTracking: '.json_encode($params));
        try {
            $shipment_id = $params['OrderCode'];
            $extShipment = ExtShipmentsGHN::find($shipment_id);
            if (!empty($extShipment)) {
                //When new tracking Update to mysql , log mongo
                $timestamp = date("Y-m-d H:i:s");
                if (strtotime($timestamp) > strtotime($extShipment['tracking_time'])) {
                    switch ($params['CurrentStatus']) {
                        case "ReadyToPick":
                            $description = 'Chờ xử lý';
                            break;
                        case "Picking":
                            $description = 'Đang lấy hàng';
                            break;
                        case "Storing":
                            $description = 'Đã lấy hàng';
                            break;
                        case "Delivering":
                            $description = 'Đang đi giao';
                            break;
                        case "Delivered":
                            $description = 'Giao hàng thành công';
                            break;
                        case "WaitingToFinish":
                            $description = 'Chờ hoàn tất';
                            break;
                        case "Finish":
                            $description = 'Hoàn tất';
                            break;
                        case "Return":
                            $description = 'Đang chuyển hoàn';
                            break;
                        case "Returned":
                            $description = 'Đã chuyển hoàn';
                            break;
                        case "LostOrder":
                            $description = 'Đơn hàng bị thất lạc';
                            break;
                        case "Cancel":
                            $description = 'Đơn hàng bị hủy';
                            break;
                        default:
                            $description = '';
                    }
                    $extShipment->update([
                        'status_code'=> $params['CurrentStatus'],
                        'tracking_time'=> $timestamp,
                        'current_warehouse'=> $params['CurrentWarehouseName'],
                        'description'=> $description
                    ]);

                    // insert to mongo

                    GHNShipmentTracking::insert([
                        "shipment_id" => $extShipment['tracking_id'],
                        "tracking_id" => $extShipment['tracking_id'],
                        "status_code" => $params['CurrentStatus'],
                        "main_code" => $extShipment['main_code'],
                        "vendor_id" => 14,
                        "tracking_time" => $timestamp,
                        "current_warehouse" => $params['CurrentWarehouseName'],
                        "description" => $description
                    ]);

                    if ($extShipment['vendor_id'] == 14) {
                        $statusGoups = DB::select("SELECT
                                                o.order_id,
                                                og.order_status,
                                                og.payment_status,
                                                og.shipping_status,
                                                og.installment_status
                                            FROM
                                                cscart_nk_b2s_order_erp e
                                            JOIN cscart_orders o ON o.order_id = e.order_id
                                            JOIN cscart_payments p ON p.payment_id = o.payment_id
                                            JOIN cscart_map_nk_status_group_ghn_shipping_status og
                                            ON og.ghn_status_code = '{$extShipment['status_code']}'
                                            AND og.payment_cate_id = p.payment_category
                                            AND og.shipment_label = 'GHN'
                                            WHERE
                                                e.MainCode = '{$extShipment['main_code']}'");

                        if ($statusGoups && count($statusGoups) > 0) {
                            $statusGoup = reset($statusGoups);
                            Log::info('UpdateForOrder:' . json_encode($statusGoup));
                            Log::info('UpdateForOrderId:' . $statusGoup->order_id);

                            $order = Orders::find(intval($statusGoup->order_id));
                            // FIXME - unset payment_status when update tracking
                            $params = [
                                'status' => $statusGoup->order_status,
//                                'payment_status' => $statusGoup->payment_status,
                                'shipping_status' => $statusGoup->shipping_status,
                                'installment_status' => $statusGoup->installment_status
                            ];
                            $order->fill($params);
                            $order->update();
                        }
                    }
                    return [
                        'code'=> 200,
                        'status'=>true,
                        'msg'=> 'success'
                    ];
                }
                return [
                    'status'=>false,
                    'msg'=> 'no change status!'
                ];
            }

        } catch (\Exception $e) {
            return [
                'code'=> 500,
                'status' => false,
                'msg' => 'errors'
            ];
        }

    }
}