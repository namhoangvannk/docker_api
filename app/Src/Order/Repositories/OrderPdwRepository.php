<?php
namespace App\Src\Order\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use App\Models\OrderDetails;
use App\Src\ApiApp\Repositories\ProfilesRepository;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;

use App\Src\Order\Transformers\OrderProductSalesTransformer;
use App\Src\Order\Transformers\OrderTransformer;

use DB;
use App\Models\Orders;
use App\Models\NKOrderPayment;
use App\Models\OrderData;
use App\Models\CscartNkUserGateway;
use App\Models\Mongo\Orders as MOrders;
use App\Libraries\Crypt\Crypt_Blowfish;
use App\Models\Users;

use App\Src\ApiApp\Repositories\AddressRepository;

use Log;
use SoapClient;

class OrderPdwRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;
    protected $profile_repo;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        OrderTransformer $transformer,
        ProfilesRepository $profile_repo
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new Orders;
        $this->transformer = $transformer;
        $this->profile_repo = $profile_repo;

    }

    public function trackingStatus($request)
    {
        if ($request->input('transaction_id') == '')
            return ['success' => false, 'message' => 'Chưa có tham số'];

        $transaction_id = $request->input('transaction_id');

        $headers = $request->header();
        $user_gateway = CscartNkUserGateway::checkValidUser(
            $headers['authorization'][0],
            $headers['password'][0]
        );

        $order = Orders::where('order_id', $transaction_id)
            ->where('utm_source', $user_gateway->partner_name)->first();

        if ($order == null)
            return ['success' => false, 'message' => 'Không tìm thấy đơn hàng'];

        $status_id = $this->helper->mappingOrderStatus($order->status);
        $result = [
            'success' => true,
            'status' => $status_id
        ];

        return $result;
    }

    public function update($request, $order_id)
    {
        Log::info('Update_order_from_PDW: ' . $order_id);
        $params = $request->all();
		Log::info('Params: ' . var_export($params, true));

        try {
            $order = Orders::find($order_id);

            if ($order == null)
                abort(400, 'Đơn hàng không tồn tại');

            $order->payment_status = $params['payment_status'];
            $order->status = 'P';
            $order->update();

            // update detail
            $order_detail_model = OrderData::where('order_id', $order_id)
                                ->where('type', 'I')->first();

            $data_bank_response = [
                'mid_number' => isset($params['MIDNumber']) ? $params['MIDNumber'] : 0,
                'terminal_id' => isset($params['TerminalID']) ? $params['TerminalID'] : 0,
                'approve_code' => isset($params['ApproveCode']) ? $params['ApproveCode'] : '',
                'transaction_amount' => isset($params['TransactionAmount']) ? $params['TransactionAmount'] : 0,
                'transaction_fee' => isset($params['TransactionFee']) ? $params['TransactionFee'] : 0,
                'transaction_date' => isset($params['TransactionDate']) ? $params['TransactionDate'] : '',
                'transaction_time' => isset($params['TransactionTime']) ? $params['TransactionTime'] : '',
				'card_number' => isset($params['CardNumber']) ? $params['CardNumber'] : '',
				'SystraceAuditNumber' => isset($params['SystraceAuditNumber']) ? $params['SystraceAuditNumber'] : '',
				'RetrievalReferenceNumber' => isset($params['RetrievalReferenceNumber']) ? $params['RetrievalReferenceNumber'] : ''
            ];

            if($order_detail_model == null) {
                $order_detail_model = new OrderData();
                $order_detail_model->order_id = $order_id;
                $order_detail_model->type = 'I';
                $order_detail_model->data = json_encode($data_bank_response);
                $order_detail_model->save();
            } else {
				 DB::table('cscart_order_data')
                    ->where('order_id', $order_id)
                    ->where('type', 'I')
                    ->update(['data' => json_encode($data_bank_response) ]);
			}
            $this->updateOrderPayment($params, $order_id);

            return ['success' => true, 'message' => 'Update successfull'];
        } catch (\Exception $e) {
            abort(400, $e->getMessage());
        }
        abort(500, 'Có lỗi xảy ra!');
        return ['success' => false, 'message' => 'Update fail'];

    }
	public function updateOrderPayment($data, $order_id)
    {
        Log::info('Params NKOrderPayment Pdw: ' . var_export($data, true));
        try{
        $orderDetail = NKOrderPayment::where('OrderID', $order_id);

            if($orderDetail == null) {
                $orderPayment = new NKOrderPayment();
                $payment['OrderID']             = $order_id;
                $payment['PaymentType']         = 'I';
                $payment['PaymentGateway']      = 'Online';
                $payment['PaymentCategoryID']   = 1;
                $payment['PaymentCategoryName'] = 'Online';
                $payment['PaymentAmount']       =  isset($data['TransactionAmount']) ? $data['TransactionAmount'] : '';
                $payment['PaymentStatus']       =  isset($data['payment_status']) ? $data['payment_status'] : '';
                $payment['TransactionID']       =  isset($data['TransactionCurrencyCode']) ? $data['TransactionCurrencyCode'] : '';
                $payment['ApproveCode']         =  isset($data['SystraceAuditNumber']) ? $data['SystraceAuditNumber'] : '';
                $payment['AdditionalData']      =  isset($data['AdditionalData']) ? $data['AdditionalData'] : '';
                $payment['PaymentReconcile']    =  '';
                $payment['CardNumber']          =  isset($data['ConsumerPan']) ? $data['ConsumerPan'] : '';
                $payment['CommissionReconcile'] =  '';
                $payment['CreatedDate']         = date("Y-m-d h:m:s");
                $payment['UpdatedDate']         = date("Y-m-d h:m:s");
                $orderPayment->fill($payment);
                $orderPayment->save();
            } else {
                DB::table('cscart_nk_order_payment')
                ->where('OrderID', $order_id)
                ->update(
                    [
                        'PaymentAmount' => isset($data['TransactionAmount']) ? $data['TransactionAmount'] : '',
                        'PaymentStatus' => isset($data['payment_status']) ? $data['payment_status'] : '',
                        'TransactionID' => isset($data['TransactionCurrencyCode']) ? $data['TransactionCurrencyCode'] : '',
                        'ApproveCode'   => isset($data['ApproveCode']) ? $data['ApproveCode'] : '',
                        'CardNumber'    => isset($data['ConsumerPan']) ? $data['ConsumerPan'] : '',
                        'UpdatedDate'   => date("Y-m-d h:m:s"),
                    ]
                );
            }
        } catch (\Exception $e) {
            //TODO Console log
        }
    }

    public function fn_get_addr_from_order($order){
        return $order['s_address'].(!empty($order['s_address_2'])?' '.$order['s_address_2']:'').', '.$this->profile_repo->getDistrict($order['s_state'],$order['s_city']) . ', '.$this->profile_repo->getState($order['s_state']);
    }
    public function fn_get_warehouse($order_id, $product_id){
        $data = DB::select("SELECT extra FROM cscart_order_details WHERE order_id = {$order_id} and product_id={$product_id};");
        $data = unserialize(json_decode(json_encode($data), True)[0]['extra']);
        return $data['warehouse_name'];
    }
    public function get($request, $order_id)
    {
        $order = Orders::find($order_id);
        $rs_msg = [];
        if(!empty($order)){
            $user = null;
            if(!empty($order->user_id)){
                $user = Users::find($order->user_id);
            }
            $order_detail = DB::select("select t1.product_id, t1.amount, t2.product, t3.product_code from cscart_order_details as t1
                                        left JOIN cscart_products as t3 on t1.product_id = t3.product_id 
                                        left JOIN cscart_product_descriptions as t2 on t1.product_id = t2.product_id and t2.lang_code='vi'
                                        where t1.order_id={$order_id}");
            $order_detail = json_decode(json_encode($order_detail), True);
            $rs_ecommer = [];
            $rs_ecommer['CustAddr'] = $this->fn_get_addr_from_order($order);
            $rs_ecommer['DlvrPrNm']  = $this->profile_repo->getState($order['s_state']);
            $rs_ecommer['DlvrDsNm']  = $this->profile_repo->getDistrict($order['s_state'],$order['s_city']);
            $rs_ecommer['DlvrCust']  = $order['b_firstname'];
            $rs_ecommer['DlvrPhne']  = $order['b_phone'];
            $rs_ecommer['NoteText']  = !empty($order['details'])?$order['details']:'';
            $rs_ecommer['MainDate']  = date('Y-m-d H:i:s', $order['timestamp']);
            $rs_ecommer['RfrnPage'] = '#'.$order_id;


            // get pdw customer by phone
            $cus_pdw_request = $this->fn_get_sap_cust_code($order["s_phone"]); //"0903352224"
            $rs_msg['pdw'] = $cus_pdw_request['message'];
            if($cus_pdw_request['code'] == 200 && !empty($cus_pdw_request['data'])){
                $cus_pdw_data = $cus_pdw_request['data'];

                $rs_ecommer['CustCode'] = $cus_pdw_data['CustCode'];
                $rs_ecommer['CardSite'] = $cus_pdw_data['SiteCode'];
                $rs_ecommer['CustCard'] = $cus_pdw_data['CardCode'];
                $rs_ecommer['CardType'] = $cus_pdw_data['CardType'];
                $rs_ecommer['CustGend'] = $cus_pdw_data['CustGend'];
                $rs_ecommer['CustName'] = $cus_pdw_data['CustName'];
                $rs_ecommer['CustPrCd'] = $cus_pdw_data['PrvnCode'];
                $rs_ecommer['CustPrNm'] = $cus_pdw_data['PrvnName'];
                $rs_ecommer['CustDsCd'] = $cus_pdw_data['DistCode'];
                $rs_ecommer['CustDsNm'] = $cus_pdw_data['DistName'];
                $rs_ecommer['CustWdCd'] = $cus_pdw_data['WardCode'];
                $rs_ecommer['CustWdNm'] = $cus_pdw_data['WardName'];
                $rs_ecommer['CustNumb'] = $cus_pdw_data['AddrNumb'];
                $rs_ecommer['CustStCd'] = $cus_pdw_data['StrtCode'];
                $rs_ecommer['CustStrt'] = $cus_pdw_data['StrtName'];
                $rs_ecommer['CustAddr'] = $cus_pdw_data['CustAddr'];
                $rs_ecommer['CustPhne'] = $cus_pdw_data['HomePhne'];
                $rs_ecommer['CustMobi'] = $cus_pdw_data['MobiPhne'];
                $rs_ecommer['CustMail'] = $cus_pdw_data['CustMail'];

                $rs_ecommer['VAT_Cust'] = $cus_pdw_data['CustName_VAT'];
                $rs_ecommer['VAT_Code'] = $cus_pdw_data['VAT_Code'];
                $rs_ecommer['VAT_Addr'] = $cus_pdw_data['VAT_Addr'];
            }
            //ext
            $ward = $this->fn_get_ward($order['s_city'],$order['s_county'],$order['s_state']);
            $rs_ecommer['DlvrTime']='';
            $rs_ecommer['DlvrCust']= $order['s_firstname']; // ten khach nhan hang
            $rs_ecommer['DlvrPrCd']= $order['s_state']; //Mã Tỉnh/Thành
            $rs_ecommer['DlvrPrNm']= $this->profile_repo->getState($order['s_state']); //Tỉnh thành GN
            $rs_ecommer['DlvrDsCd']= $order['s_city'];
            $rs_ecommer['DlvrDsNm']= $this->fn_get_district_name($order['s_county'], $order['s_state']);
            $rs_ecommer['DlvrWdCd']=!empty($ward)?$ward['code']:'';
            $rs_ecommer['DlvrWdNm']=!empty($ward)?$ward['name']:'';
            $rs_ecommer['DlvrNumb']=$order['s_address'];
            $rs_ecommer['DlvrStCd']='';
            $rs_ecommer['DlvrStrt']=$order['s_address'];
            $rs_ecommer['DlvrAddr']= $order['s_address'].(!empty($order['s_address_2'])?' '.$order['s_address_2']:'').', '.$rs_ecommer['DlvrWdNm'].', '.$rs_ecommer['DlvrDsNm'].', '.$rs_ecommer['DlvrPrNm'];//$this->fn_get_addr_from_order($order);
            $rs_ecommer['DlvrPhne']=$order['s_phone'];
            $rs_ecommer['VAT_Phne']=$order['b_phone'];
            $rs_ecommer['VAT_Prsn']=$order['b_firstname'];


            ksort($rs_ecommer);
            $rs['master'] = $rs_ecommer;
            $list_items = [];
            $QUOMNames = ['BIN'=>'Bình', 'BO'=>'Bộ', 'CAI'=>'Cái', 'CAY'=>'Cây', 'CHI'=>'Chiếc'];
            foreach ($order_detail as $item){
                $product_code_splits = explode('-',$item['product_code']);
                if(!empty($product_code_splits) && count($product_code_splits)>=2){
                    $QUOMCode = $product_code_splits[1];
                }else{
                    $QUOMCode = '';
                }
                $list_item =  [
                  'PrdcName' => $item['product'],
                    'PrdcCode'=>!empty($product_code_splits)?$product_code_splits[0]:'',
                  'QUOMCode' => $QUOMCode,
                  'PrdcQtty'=>$item['amount'],
                    'QUOMName'=>!empty($QUOMCode) && isset($QUOMNames[$QUOMCode])?$QUOMNames[$QUOMCode]:'',
                ];
                ksort($list_item);
                $list_items[] = $list_item;
            }
            $rs['list_items'] = $list_items;
            $rs_payment['PaymAmnt'] = $order['total'];
            $ins = $this->fn_get_order_installment($order_id);
            $payment = OrderData::where(['order_id'=>$order_id,'type'=>'P'])->get()->toArray();

            $rs_payment['PaymType'] = $order['payment_id']==6?"00":"01";
            if(!empty($payment)){
                $payment = $this->fn_decrypt_text_cscart($payment[0]['data']);
                $rs_payment['PaymCard'] = $this->get_ne_value($payment,'card_number');
                $rs_payment['BankCode'] = $this->get_ne_value($payment,'bank_code');
                $rs_payment['CardNumb'] = $this->get_ne_value($payment,'card_number');
                $rs_payment['PaymNote'] = $this->get_ne_value($payment,'reason_text');
            }
            $rs_payment['ExprCode'] = "";
            if(!empty($ins)){
                $rs_payment['ExprCode'] = $ins['term']; // han vay tra gop
            }
            if(empty($rs_payment['ExprCode']) && !empty($payment) &&  !empty($payment["thang_tra"])){
                preg_match_all('!\d+!', $payment['thang_tra'], $matches);
                if(!empty($matches) && !empty(reset($matches))){
                    $rs_payment['ExprCode'] = reset($matches)[0];
                }else{
                    $rs_payment['ExprCode'] = "";
                }
            }
            ksort($rs_payment);
            $rs['payment'] = $rs_payment;
        }else{
            $rs =[];
        }

        if(!empty($rs)){
            $rs_msg['online'] = 'successful!';
            return [
                'code'=>200,
                'message'=>$rs_msg,
                'data'=>$rs
            ];
        }else{
            return [
                'code'=>404,
                'message'=>'data not found',
                'data'=>[]
            ];
        }
    }
    public function fn_get_order_installment($order_id){
        $ins = DB::select("SELECT order_id, term, interest, mPayEachMonth,mprepay from cscart_nk_tragop_orders where order_id={$order_id} ");
        $ins = json_decode(json_encode($ins), True);
        if(!empty($ins)){
            $ins = reset($ins);
        }else{
            $ins = [];
        }
        return $ins;
    }
    public function fn_get_district_name($district_code, $state_code, $lang_code='vi'){
        $district = DB::select(
            "SELECT bd.name FROM cscart_nk_districts b " .
            " LEFT JOIN cscart_nk_district_descriptions bd ON b.district_id = bd.district_id AND bd.lang_code = ? " .
            " WHERE b.code = ? AND b.state_code = ?",  [$lang_code, $district_code, $state_code]);
        $district = json_decode(json_encode($district), True);
        if(!empty($district)){
            return reset($district)['name'];
        }
        return '';
    }
    public function fn_get_ward($s_city, $s_county, $s_state){
        $ward = DB::select(
            "SELECT b.code, bd.name FROM cscart_nk_wards b " .
            " LEFT JOIN cscart_nk_ward_descriptions bd ON b.ward_id = bd.ward_id AND bd.lang_code = 'vi' " .
            " WHERE b.code = '{$s_city}' and b.district_code = '{$s_county}' and  b.state_code =  '{$s_state}'"
        );
        $rs = json_decode(json_encode($ward), True);
        if(!empty($rs)){
            $rs = reset($rs);
        }else{
            $rs = '';
        }
        return $rs;
    }

    public function get_ne_value($val,$key){
        return !empty($val[$key])?$val[$key]:'';
    }
    public function fn_decrypt_text_cscart($text){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://nguyenkim.com/index.php?dispatch=nk_ajax.fn_decrypt_text",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"text\"\r\n\r\n".$text."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return @json_decode($response,true);
        }
    }
    public function fn_get_sap_cust_code($phone){
        $pdw_url = 'https://wslocal.trade.nguyenkim.com/WS_ECOM/ECOM_Services.svc';
        try{
            $client = new SoapClient($pdw_url . '?wsdl', array("trace" => false,"exceptions" => true));
        $params = [];
        $params["Header"] = [
            "SrchType" => "2",
            "CustInfo" => strval($phone),
            "SiteCode" => "NK01",
            "CustB2B" => "",
        ];
        $params  = ['pData' => json_encode($params)];
            $client->__setLocation($pdw_url);
        $result = $client->EC_SearchCustomer($params);
        $getDocumentCodeResult = $result->EC_SearchCustomerResult;
        $data = json_decode($getDocumentCodeResult);
        if (!empty($data) && $data->Header->Msg_Code == 200) {
            $data = reset($data->Detail);
                return [
                    'code'=>200,
                    'message'=>'successful!',
                    'data'=>(array)$data->Data
                ];
            }
            return [
                'code'=>404,
                'message'=>'successful!',
                'data'=>[]
            ];

        }catch (\SoapFault $sf) {
            return [
                'code'=>500,
                'message'=> 'SoapFault: '.$sf->getMessage(),
                'data'=>[]
            ];
        }
        catch (\Exception $e) {
            return [
                'code'=>500,
                'message'=>'EC_SearchCustomer Exception: '.$e->getMessage(),
                'data'=>[]
            ];
        }
    }
}


