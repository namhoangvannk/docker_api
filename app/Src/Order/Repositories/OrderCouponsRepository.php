<?php
namespace App\Src\Order\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\Src\Order\Collections\OrderCouponsCollection;
use App\Src\Order\Transformers\OrderCouponsTransformer;

use App\Models\OrderCoupons;
use App\Models\Mongo\OrderCoupons as MOrderCoupons;

use Log;

class OrderCouponsRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $Transformer;
    protected $model;

    public function __construct(
        Manager $fractal,
        OrderCouponsCollection $collection,
        OrderCouponsTransformer $Transformer)
    {
        $this->fractal = $fractal;
        $this->collection = $collection;
        $this->transformer = $Transformer;
        $this->model = new OrderCoupons();
    }

    public function all($request)
    {
        $result = $this->allByMongo($request);
        return $result;
    }

    private function allByMongo($request)
    {
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 20);

        $model = MOrderCoupons::select(
            'order_id',
            'user_id',
            'total',
            'subtotal',
            'total_discount',
            'price_discount',
            'coupon_code',
            'coupon_value',
            'coupon_percent',
            'order_status',
            'firstname',
            'lastname',
            'phone',
            'email',
            'order_date'
        );

        if ($request->input('user_id') != null) {
            $model = $model->where('user_id', '=', intval($request->input('user_id')));
        }

        if ($request->input('order_id') != null) {
            $model = $model->where('order_id', '=', intval($request->input('order_id')));
        }

        if ($request->input('coupon_code') != null) {
            $model = $model->where('coupon_code', '=', $request->input('coupon_code'));
        }

        if ($request->input('order_status') != null) {
            $model = $model->where('order_status', '=', $request->input('order_status'));
        }

        $list = $model->skip($offset)->take($limit)->get();
        $resource = new Collection($list, new OrderCouponsTransformer());
        $data = $this->fractal->createData($resource)->toArray();

        $paging = new PagingComponent($model->count(), $offset, $limit);
        $data['paging'] = $paging->render();
        return $data;
    }

    public function create($request) 
    {
        $params = $request->all();
        $model = $this->model;

        try {

            $data_mysql['order_id'] = $params['order_id'];
            $data_mysql['coupon_code'] = strtoupper($params['coupon_code']);
            $data_mysql['coupon_value'] = $params['coupon_value'];
            $model->fill($data_mysql);
            $model->save();

            // Update to mongo
            $params['coupon_code'] = strtoupper($params['coupon_code']);
            MOrderCoupons::insert($params);

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    public function show($request, $coupon_code)
    {
        $model = OrderCoupons::join('cscart_orders', 'cscart_orders.order_id', '=', 'cscart_order_coupons.order_id')
            ->where('cscart_order_coupons.coupon_code', '=', $coupon_code)
            ->whereNotIn('cscart_orders.status', ['U', 'H', 'I'])
            ->first();

        $result = [
            'message' => 'Coupon not found',
            'data' => []
        ];

        if($model != null) {
            $result['message'] = 'OK';
            $result['data'] = ['coupon_code' => $coupon_code, 'created_at' => $model->created_at];
        }

        return $result;

    }

}
