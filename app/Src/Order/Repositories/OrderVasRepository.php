<?php
namespace App\Src\Order\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;

use App\Src\Order\Transformers\OrderVasTransformer;
use App\Models\NkVasOrder;
use App\Models\NkVasOrderPayment;

class OrderVasRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        OrderVasTransformer $transformer
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new NkVasOrder;
        $this->transformer = $transformer;

    }

    public function all($request)
    {
        return [];
    }

    public function create($request)
    {
        $params = $request->all();
        $model = $this->model;
        $data_mongo = [];

        try {
            $params['CreatedDate'] = date('Y-m-d H:i:s');
            $model->fill($params);
            $model->save();

            $order_id = $model->OrderID;
            $payment = new NkVasOrderPayment();
            $payment_data = $params['order_payment'];
            $payment_data['OrderID'] = $order_id;
            $payment_data['CreatedDate'] = date('Y-m-d H:i:s');
            $payment->fill($payment_data);
            $payment->save();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function update($request, $order_code)
    {
        $params = $request->all();
        try {
            $data = NkVasOrder::where('OrderCode','=',$order_code)->first();

            if (empty($data))
                abort(400, 'Đơn hàng không tồn tại');

            $order_id = $data->OrderID;
            $params['UpdatedDate'] = date('Y-m-d H:i:s');
            $data->fill($params);
            $data->update();

            if(!empty($params['order_payment'])){
                $order_payment = $params['order_payment'];
                $order_payment['OrderID'] = $order_id;
                $order_payment['UpdatedDate'] = date('Y-m-d H:i:s');
                $order_payment_model = NkVasOrderPayment::where('OrderID','=',$order_id)->first();
                $order_payment_model->fill($order_payment);
                $order_payment_model->update();
            }

            return $this->fractal->createData(new Item($data, $this->transformer))->toArray();
        } catch (\Exception $e) {
            abort(400, $e->getMessage());
        }
        abort(500, 'Có lỗi xảy ra!');
        return [];

    }
}
