<?php

namespace App\Src\Order\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\NkCybersourceCardCustomer;
use App\Models\NkCybersourceOrder;
use Illuminate\Support\Facades\Validator;
use App\Models\Orders;
use Log;

class CybersourceRepository extends RepositoryBase
{
    protected $model;
    protected $config;

    public function __construct()
    {

    }


    public function callSilentWithCardInfo($request)
    {
        $reason_code = '0';
        $system_message = '';
        $message = '';
        $params = $request->all();
        try {
            $submit_url = $params['url'];
            $data = $params['data'];
            $userId = $params['user_id'];
            $orderId = $params['order_id'];
            $isSaveToken = $params['is_save_token'];
            $cardName = $params['card_name'];

            Log::info('Call cybersource with submit_url: ' . $submit_url);
            Log::info('Call cybersource with order_id: ' . $params['order_id']);

            $array_info = array();
            $array_reponse = array();
            $array_info['order_id'] = $orderId;
            $array_info['user_id'] = $userId;
            $array_info['card_name'] = $cardName;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $submit_url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            curl_close($ch);

            Log::info('Response from cybersource: ' . $result);

            $result = str_replace('<meta charset="utf-8">', "", $result);

            //Parse param
            $xml = simplexml_load_string($result);
            if ($xml->body->form) {
                $formResponse = $xml->body->form;
                /**
                 * Tham khao response: resources/responseCybersource/no-3Ds.txt
                 * Voi loai the ko co 3Ds thi bypass qua buoc yeu cau nhap the
                 */
                if ($formResponse['id'] == 'custom_redirect') {
                    Log::info('Having form cybersource');
                    foreach ($xml->body->form->input as $ele) {
                        try {
                            $name = $ele['name'];
                            $value = $ele['value'];
                            $array_info[strval($name)] = $value;
                        } catch (\Exception $e) {
                            Log::info('Error att param cybersource: ' . $name . '-' . $value);
                            Log::info('Detail:' . $e->getMessage());
                        }
                    }

                    $array_order = array(
                        'req_reference_number' => $array_info['req_reference_number'] ? $array_info['req_reference_number'] : '',
                        'req_card_number' => $array_info['req_card_number'] ? $array_info['req_card_number'] : '',
                        'req_card_expiry_date' => $array_info['req_card_expiry_date'] ? $array_info['req_card_expiry_date'] : '',
                        'reason_code' => $array_info['reason_code'] ? $array_info['reason_code'] : '',
                        'decision' => $array_info['decision'] ? $array_info['decision'] : '',
                        'req_payment_method' => $array_info['req_payment_method'] ? $array_info['req_payment_method'] : '',
                        'req_card_type' => $array_info['req_card_type'] ? $array_info['req_card_type'] : '',
                        'message' => $array_info['message'] ? $array_info['message'] : '',
                        'req_transaction_type' => $array_info['req_transaction_type'] ? $array_info['req_transaction_type'] : '',
                        'signed_date_time' => $array_info['signed_date_time'] ? $array_info['signed_date_time'] : '',
                        'transaction_id' => isset($array_info['transaction_id']) ? $array_info['transaction_id'] : '',
                        'required_fields' => isset($array_info['required_fields']) ? $array_info['required_fields'] : '',
                        'response_data' => $result
                    );

                    $array_reponse = array(
                        'req_card_number' => $array_info['req_card_number'] ? $array_info['req_card_number'] : '',
                        'transaction_id' => isset($array_info['transaction_id']) ? $array_info['transaction_id'] : '',
                        'message' => $array_info['message'] ? $array_info['message'] : ''
                    );

                    NkCybersourceOrder::insert($array_order);

                    if ($isSaveToken && $isSaveToken == 'Y') {

                        $token = $array_info['payment_token'];
                        $userToken = NkCybersourceCardCustomer::select('user_id')
                            ->where('payment_token', '=', $token)
                            ->get();

                        if (empty($userToken) || count($userToken) == 0) {
                            $array_card = array(
                                'user_id' => $array_info['user_id'] ? $array_info['user_id'] : '',
                                'payment_token' => $array_info['payment_token'] ? $array_info['payment_token'] : '',
                                'card_number' => $array_info['req_card_number'] ? $array_info['req_card_number'] : '',
                                'card_expiry_date' => $array_info['req_card_expiry_date'] ? $array_info['req_card_expiry_date'] : '',
                                'payment_method' => $array_info['req_payment_method'] ? $array_info['req_payment_method'] : '',
                                'card_type' => $array_info['req_card_type'] ? $array_info['req_card_type'] : '',
                                'card_name' => $array_info['card_name'] ? $array_info['card_name'] : ''
                            );
                            NkCybersourceCardCustomer::insert($array_card);
                        }
                    }

                    $reason_code = strval($array_info['reason_code']);
                    $decision = strval($array_info['decision']);
                    $system_message = strval($array_info['message']);

                    Log::info("NkCybersourceOrder: [order_id, reason_code,decision,message]");
                    Log::info($orderId . ' ' . $reason_code . ' ' . $decision . ' ' . $system_message);

                    if ($reason_code == '100') {
                        $message = 'Thanh toán thành công.';
                    } else {
                        $message = 'Thanh toán thất bại. Liên hệ nguyenkim.com.';
                    }

                    //IF SUCCESS
                    if ($reason_code == '100') {
                        $order = Orders::find($orderId);
                        $param_order = array();
                        $param_order['status'] = 'O';
                        $param_order['payment_status'] = 'B';
                        $order->fill($param_order);
                        $order->update();
                    }
                } /**
                 * Tham khao response: resources/responseCybersource/authen-3Ds.txt
                 * Voi the co 3Ds thi redirect sang trang nhap OTP
                 */
                else if ($formResponse['id'] == 'payer_authentication_form') {
                    foreach ($xml->body->form->input as $ele) {
                        try {
                            $name = $ele['name'];
                            $value = $ele['value'];
                            $array_info[strval($name)] = $value;
                        } catch (\Exception $e) {
                            Log::info('Error att param cybersource: ' . $name . '-' . $value);
                            Log::info('Detail:' . $e->getMessage());
                        }
                    }
                    return [
                        'Code' => '-2018',
                        'Message' => 'Call payer_authentication_form',
                        'Data' => array(
                            'form-action' => $formResponse['action'],
                            'inputs' => $array_info
                        )
                    ];
                }
            } else {
                Log::info('Error author form cybersource');
                $reason_code = '500';
                $system_message = $xml->body->div->div->h2;
                $message = 'Thanh toán thất bại. Liên hệ nguyenkim.com';
            }

            Log::info('Cybersource description: ' . $system_message);
            return [
                'Code' => strval($reason_code),
                'Message' => strval($message),
                'Data' => $array_reponse
            ];
        } catch (\Exception $e) {
            Log::info('Exception Cybersource: ' . $e->getMessage());
            return [
                'Code' => $reason_code,
                'Message' => 'Lỗi hệ thống! Liên hệ nguyenkim.com',
                'Data' => $array_reponse
            ];
            //abort(400, $e->getMessage());
        }

        abort(500, 'Có lỗi xảy ra!');
    }

    public function updateWithCardInfo($request)
    {
        $reason_code = '0';
        $system_message = '';
        $message = '';
        $params = $request->all();
        try {
            $userId = $params['user_id'];
            $orderId = $params['order_id'];

            if (!empty($params['is_save_token']) && $params['is_save_token'] == 'Y') {
                $cardName = $params['card_name'];
                $array_card = array(
                    'user_id' => $userId ? $userId : '',
                    'card_name' => $cardName ? $cardName : '',
                    'merchant_ref' => $orderId ? $orderId : ''
                );
                if(!empty($userId)) {
                    NkCybersourceCardCustomer::insert($array_card);
                }
                return [
                    'Code' => '100'
                ];
            }
            $array_info = $params['data'];
            $array_info['order_id'] = $orderId;
            $array_info['user_id'] = $userId;

            $array_order = array(
                'req_reference_number' => $array_info['req_reference_number'] ? $array_info['req_reference_number'] : '',
                'req_card_number' => $array_info['req_card_number'] ? $array_info['req_card_number'] : '',
                'req_card_expiry_date' => $array_info['req_card_expiry_date'] ? $array_info['req_card_expiry_date'] : '',
                'reason_code' => $array_info['reason_code'] ? $array_info['reason_code'] : '',
                'decision' => $array_info['decision'] ? $array_info['decision'] : '',
                'req_payment_method' => $array_info['req_payment_method'] ? $array_info['req_payment_method'] : '',
                'req_card_type' => $array_info['req_card_type'] ? $array_info['req_card_type'] : '',
                'message' => $array_info['message'] ? $array_info['message'] : '',
                'req_transaction_type' => $array_info['req_transaction_type'] ? $array_info['req_transaction_type'] : '',
                'signed_date_time' => $array_info['signed_date_time'] ? $array_info['signed_date_time'] : '',
                'transaction_id' => isset($array_info['transaction_id']) ? $array_info['transaction_id'] : '',
                'required_fields' => isset($array_info['required_fields']) ? $array_info['required_fields'] : '',
                'response_data' => $array_info['response_data'] ? $array_info['response_data'] : '',
                'invalid_fields' => isset($array_info['invalid_fields']) ? $array_info['invalid_fields'] : ''
            );

            $array_reponse = array(
                'req_card_number' => $array_info['req_card_number'] ? $array_info['req_card_number'] : '',
                'transaction_id' => isset($array_info['transaction_id']) ? $array_info['transaction_id'] : '',
                'message' => $array_info['message'] ? $array_info['message'] : ''
            );

            NkCybersourceOrder::insert($array_order);

            if (!empty($orderId) && !empty($array_info['payment_token'])) {
                $token = $array_info['payment_token'];
                $userToken = NkCybersourceCardCustomer::select('user_id')
                    ->where('payment_token', '=', $token)
                    ->get();
                if (empty($userToken) || count($userToken) == 0) {
                    $cardCustomer = NkCybersourceCardCustomer::where('merchant_ref', '=', $orderId)->first();
                    if (!empty($cardCustomer)) {
                        $array_card = [
                            'payment_token' => $array_info['payment_token'] ? $array_info['payment_token'] : '',
                            'card_number' => $array_info['req_card_number'] ? $array_info['req_card_number'] : '',
                            'card_expiry_date' => $array_info['req_card_expiry_date'] ? $array_info['req_card_expiry_date'] : '',
                            'payment_method' => $array_info['req_payment_method'] ? $array_info['req_payment_method'] : '',
                            'card_type' => $array_info['req_card_type'] ? $array_info['req_card_type'] : '',
                        ];
                        $cardCustomer->fill($array_card);
                        $cardCustomer->save();
                    }
                }
            }

            $reason_code = strval($array_info['reason_code']);
            $decision = strval($array_info['decision']);
            $system_message = strval($array_info['message']);

            Log::info("NkCybersourceOrder: [order_id, reason_code,decision,message]");
            Log::info($orderId . ' ' . $reason_code . ' ' . $decision . ' ' . $system_message);

            if ($reason_code == '100') {
                $message = 'Thanh toán thành công.';
            } else {
                $message = 'Thanh toán thất bại. Liên hệ nguyenkim.com.';
            }

            //IF SUCCESS
            /*if ($reason_code == '100') {
                $order = Orders::find($orderId);
                $param_order = array();
                $param_order['status'] = 'O';
                $param_order['payment_status'] = 'B';
                $order->fill($param_order);
                $order->update();
            }*/


            Log::info('Cybersource description: ' . $system_message);
            return [
                'Code' => strval($reason_code),
                'Message' => strval($message),
                'Data' => $array_reponse
            ];
        } catch (\Exception $e) {
            Log::info('Exception Cybersource: ' . $e->getMessage());
            return [
                'Code' => $reason_code,
                'Message' => 'Lỗi hệ thống! Liên hệ nguyenkim.com',
                'Data' => $array_reponse
            ];
        }
}

}