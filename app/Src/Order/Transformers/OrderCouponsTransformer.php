<?php

namespace App\Src\Order\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class OrderCouponsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'order_id' => $obj['order_id'],
            'user_id' => $obj['user_id'],
            'total' => $obj['total'],
            'subtotal' => $obj['subtotal'],
            'total_discount' => $obj['total_discount'],
            'price_discount' => $obj['price_discount'],
            'coupon_code' => strtoupper($obj['coupon_code']),
            'coupon_value' => $obj['coupon_value'],
            'coupon_percent' => $obj['coupon_percent'],
            'order_status' => $obj['order_status'],
            'firstname' => $obj['firstname'],
            'lastname' => $obj['lastname'],
            'phone' => $obj['phone'],
            'email' => $obj['email'],
            'order_date' => $obj['order_date']
        ];
    }
}
