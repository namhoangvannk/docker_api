<?php

namespace App\Src\Order\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class ExtShipmentsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {

        return ['id' => $obj['id'],
            'shipment_id' => $obj['shipment_id'],
            'vendor_id' => $obj['vendor_id'],
            'tracking_id' => $obj['tracking_id'],
            'created_date' => $obj['created_date'],
            'evt_status' => $obj['evt_status'],
            'evt_description' => $obj['evt_description'],
            'evt_datetime' => $obj['evt_datetime'],
            'evt_city' => $obj['evt_city'],
            'evt_postcode' => $obj['evt_postcode'],
            'evt_state' => $obj['evt_state'],
            'evt_country' => $obj['evt_country'],
            'status' => $obj['status']];
    }

}
