<?php
namespace App\Src\Order\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class OrderProductSalesTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'product_code' => $obj['_id'],
            'total_sales' => $obj['total'],
        ];
    }

}
