<?php
namespace App\Src\Order\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class OrderVasTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'order_id' => $obj->OrderID,
        ];
    }

}
