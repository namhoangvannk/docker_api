<?php

namespace App\Src\Order\Collections;

use App\Helpers\Database\Collection;

class OrderCollection extends Collection
{
    /**
     * The table associated with the collection.
     *
     * @var string
     */
    protected $table = 'cscart_orders';

}
