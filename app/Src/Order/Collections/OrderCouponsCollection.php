<?php

namespace App\Src\Order\Collections;

use App\Helpers\Database\Collection;

class OrderCouponsCollection extends Collection
{
    /**
     * The table associated with the collection.
     *
     * @var string
     */
    protected $table = 'cscart_order_coupons';

}
