<?php

namespace App\Src\Order\Collections;

use App\Helpers\Database\Collection;

class ExtShipmentsCollection extends Collection
{
    /**
     * The table associated with the collection.
     *
     * @var string
     */
    protected $table = 'nk_ext_shipments';

}