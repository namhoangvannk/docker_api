<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 17/04/2018
 * Time: 11:59 AM
 **/

namespace App\Src\FlashSales\Repositories;
use App\Helpers\Cache\RedisManager;
use App\Base\Repositories\RepositoryBase;
use Illuminate\Support\Facades\Validator;
use App\Models\Mongo\HotDeal;
use App\Models\ProductPrices;
use App\Models\ProductDescriptions;
use App\Models\OrderDetails;
use App\Models\Products;
use App\Models\Mongo\FlashSales;
use App\Models\Orders;
use Log;
use DB;

class FlashSalesRepository extends RepositoryBase {
    protected $model;
    Protected $redis;
    public function __construct(RedisManager $redis){
        $this->model = new FlashSales();
        $this->redis = $redis;
    }
    public function allRedisValue($request){
        $data = $this->redis->getValue('flashsales', 3,false);
        if($data)
            return $data;
        return [];
    }
    public function setRedisValue($request){
        $key_redis = "flashsales";
        $params = $request->all();

        if(empty($params['flashsales_value']) || $params['flashsales_value']=='[]'){
            $this->redis->delKeyDB('flashsales', 3);
        }else{
            $this->redis->setValue('flashsales', $params['flashsales_value'], 100, 3,false);
        }
        return ['code' => 'OK'];
    }

}