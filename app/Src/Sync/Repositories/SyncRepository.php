<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:53 AM
 */

namespace App\Src\Sync\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Mongo\Blog;
use App\Models\Mongo\Pages;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class SyncRepository extends RepositoryBase
{
    protected $model;
    protected $helper;
    protected $config;

    public function __construct(Helpers $helper)
    {
        $this->helper = $helper;
        $this->model = new Blog();
    }
    public function blog($request)
    {
        $data = $request->all();
        try {
            DB::connection('mongodb')->collection('blog')->delete();
            DB::connection('mongodb')->collection('blog')->insert($data);
            return [
                'status' => 200,
                'message' => 'Thêm thành công'
            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function page($request)
    {
        $data = $request->all();
        try {
            DB::connection('mongodb')->collection('pages')->delete();
            DB::connection('mongodb')->collection('pages')->insert($data);
            return [
                'status' => 200,
                'message' => 'Thêm thành công'
            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}