<?php
namespace App\Src\Stock\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Query\Query;
use Illuminate\Support\Facades\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\Models\Mongo\CommonApiLogs;
use App\Models\EmailSending;
use Log;
use DB;
use App\Helpers\Cache\RedisManager;

class StockRepository extends RepositoryBase
{
    protected $manage;
    protected $redis;


    public function __construct(Manager $manage, RedisManager $redis)
    {
        $this->manage = $manage;
        $this->redis = $redis;
    }

    public function all($request)
    {
        $params = $request->all();
        $time_cron = 15*60; //15 phut
        $fields = array(
            "SAP_ArticleDummy.SiteCode",
            "SAP_ArticleDummy.Art_Code",
            "SAP_ArticleDummy.QUOMCode",
            "SAP_ArticleDummy.Quantity",
            "NK_SITE_LOC.Site_loc_id",
            "NK_SITE_LOC.status",
            "NK_PRODUCTS.PRODUCT_ID",
            "NK_PRODUCTS.IS_VIRTUAL_WH",
            "NK_PRODUCTS.STATUS as product_status",
            "NK_PRODUCTS.IS_GARNITURE_WH"
        );
        $sql_server = DB::connection('sqlsrv')->table("SAP_ArticleDummy")->select(implode(",",$fields));
        $sql_server->join('NK_PRODUCTS', 'NK_PRODUCTS.ART_CODE', '=', 'SAP_ArticleDummy.Art_Code');
        $sql_server->join('NK_SITE_LOC', 'NK_SITE_LOC.Site_Code', '=', 'SAP_ArticleDummy.SiteCode');
        $sql_server->where('NK_SITE_LOC.Site_loc_id', '=', 'SAP_ArticleDummy.WrhsCode');
        $sql_server->where('NK_PRODUCTS.IS_SYNC_STOCK', '=', 'Y');
        if(!empty($params["auto"])){
            if(!empty($params["time_cron"])){
                $time_cron = $params["time_cron"];
            }
            $params["time"] = date("Y-m-d H:i:s", time()-$time_cron);
        }
        if(!empty($params["time"])){
            $sql_server->where("InptDate", ">=", $params["time"]);
        }
        if(!empty($params["page"])){
            $limit = !empty($params["limit"])?$params["limit"]:10;
            $params["offset"] = $limit*($params["page"]-1);
        }
        if(!empty($params["offset"])){
            $sql_server->skip($params["offset"]);
        }
        if(!empty($params["limit"])){
            $sql_server->take($params["limit"]);
        }
        $res = $sql_server->get()->toArray();
        $rs = [];
        foreach($res as $row){
            $key = $row->PRODUCT_ID;//$row["Art_Code"]."-".$row["QUOMCode"];
            if(array_key_exists($key, $rs)){
                if(($row->IS_VIRTUAL_WH=="N" && $row->status==2) || ($row->IS_GARNITURE_WH=="N" && $row->status==3)){
                    continue;
                }
                if(array_key_exists($row->SiteCode, $rs[$key]["wh"])){
                    $rs[$key]["wh"][$row->SiteCode] += $row->Quantity;
                }else{
                    $rs[$key]["wh"][$row->SiteCode] = $row->Quantity;
                }
            }else{
                $rs[$key] = [
                    "status" => $row->product_status,
                    "product_code" => $row->SiteCode."-".$row->QUOMCode,
                    "wh" => []
                ];
                if(($row->IS_VIRTUAL_WH=="N" && $row->status==2) || ($row->IS_GARNITURE_WH=="N" && $row->status==3)){
                    $rs[$key]["wh"][$row->SiteCode] = 0;
                }else{
                    $rs[$key]["wh"][$row->SiteCode] = $row->Quantity;
                }
            }
        }
        unset($res);
        //chuyen qua mysql cscart_nk_product_stocks
        $region_warehouse = DB::connection('mysql')->table("nk_map_region_warehouse")->select("region_id,warehouse_code")->get()->toArray();
        $reg_ware = [];
        foreach ($region_warehouse as $rw){
            $reg_ware[$rw->warehouse_code] = $rw->region_id;
        }
        unset($region_warehouse);
        $time = date("Y-m-d H:i:s");
        foreach($rs as $k=>$v){
            foreach($v["wh"] as $kv=>$vv){
                $query = "REPLACE INTO cscart_nk_product_stocks VALUES($k, '$kv','NKONLINE','{$v["product_code"]}',$vv,'".$time."',$vv)";
                DB::connection('mysql')->statement($query);
            }
        }
        //chuyen qua cscart_nk_region_amounts
        $stocks = DB::connection('mysql')->table("cscart_nk_product_stocks")->select("product_id,warehouse_code,amount")->whereIn("product_id", array_keys($rs))->orderBy('product_id', 'desc')->get()->toArray();
        $arr_stock = [];
        foreach ($stocks as $ks=>$vs){
            $arr_stock[$vs->product_id][] = $vs;
        }
        unset($stocks);
        $products = DB::connection('mysql')->table("cscart_products")->select("status,hidden_option,chk_amount,chk_total_amount,is_one_click,is_add_cart,is_tra_gop")->whereIn("product_id", array_keys($rs))->get()->toArray();
        $arr_product = [];
        foreach ($products as $kp=>$vp){
            $arr_product[$vp->product_id] = $vp;
        }
        unset($products);
        $arr_mysql = [];
        foreach($arr_stock as $k=>$v){
            if(!empty($arr_product[$k])){
                $arr_mysql[$k] = [
                    "status" => $arr_product[$k]->status,
                    "hidden_option" => $arr_product[$k]->hidden_option,
                    "chk_amount" => $arr_product[$k]->chk_amount,
                    "chk_total_amount" => $arr_product[$k]->chk_total_amount,
                    "oneclick" => $arr_product[$k]->is_one_click,
                    "addcart" => $arr_product[$k]->is_add_cart,
                    "installment" => $arr_product[$k]->is_tra_gop,
                    "regions" => []
                ];
                foreach ($arr_stock[$k] as $stk=>$stv){
                    //to do
                    if(array_key_exists($reg_ware[$stv["warehouse_code"]], $arr_mysql[$k]["regions"])){
                        $arr_mysql[$k]["regions"][$reg_ware[$stv->warehouse_code]] += $stv->amount;
                    }else{
                        $arr_mysql[$k]["regions"][$reg_ware[$stv->warehouse_code]] = $stv->amount;
                    }
                }
                $arr_region = [];
                foreach ($arr_mysql[$k]["regions"] as $kr => $vr){
                    //update mysql
                    $query = "REPLACE INTO cscart_nk_region_amounts VALUES($kr, $k, $vr,'".$time."',0)";
                    DB::connection('mysql')->statement($query);
                    //tao key up redis
                    array_push($arr_region,[
                        "region_id" => $kr,
                        "amount" => $vr
                    ]);
                    unset($arr_mysql["regions"][$k][$kr]);
                }
                $arr_mysql[$k]["regions"] = $arr_region;
                unset($arr_region);
            }
        }
        //chuyen len redis key stock_pdw_
        foreach($rs as $k=>$v){
            $arr_pwd = [
                "status" => $v["status"]
            ];
            if(!empty($v["wh"])){
                foreach($v["wh"] as $kw => $vw){
                    array_push($arr_pwd["wh"],[
                        "site_code" => $kw,
                        "amount" => $vw,
                        "is_sample" => 0
                    ]);
                }
            }
            $this->redis->setValue("stock_pdw_$k", json_encode($arr_pwd), 0, 2);
        }
        //chuyen len redis key stock_product_
        foreach($arr_mysql as $rk => $rv){
            $this->redis->setValue("stock_product_$rk", json_encode($rv), 0, 2);
        }
        return array(
            'status' => 200,
            'message' => "Sync Successfull !"
        );
    }

    public function syncProduct($req){
        $product = DB::connection('mysql')->table("cscart_products");
        $request = $req->all();
        if(!empty($request["status"])){
            $ex = explode(",", $request["status"]);
            $product->whereIn("status", $ex);
        }
        if(!empty($request["pid"])){
            $ex = explode(",", $request["pid"]);
            $product->whereIn("product_id", $ex);
        }
        $fields = [
            "product_id",
            "product_code",
            "nk_is_sync_stock",
            "nk_is_virtual_warehouse",
            "nk_is_garniture_warehouse",
            "status"
        ];
        $rs = $product->select($fields)->get()->toArray();
        //insert sql server
        $product_id = [];
        foreach ($rs as $r){
            $ex = explode("-", $r->product_code);
            DB::connection('sqlsrv')->table("NK_PRODUCTS")->delete()->where("PRODUCT_ID","=",$r->product_id)->where("ART_CODE","=",$ex[0]);
            DB::connection('sqlsrv')->table("NK_PRODUCTS")->insert([
                "PRODUCT_ID" => $r->product_id,
                "PRODUCT_CODE" => $r->product_code,
                "STATUS" => $r->status,
                "ART_CODE" => $ex[0],
                "IS_SYNC_STOCK" => $r->nk_is_sync_stock,
                "IS_VIRTUAL_WH" => $r->nk_is_virtual_warehouse,
                "IS_GARNITURE_WH" => $r->nk_is_garniture_warehouse
            ]);
            array_push($product_id, $r->product_id);
        }
        return array(
            'status' => 200,
            'message' => "Sync Successfull !",
            'product_id' => json_encode($product_id)
        );
    }

    public function syncWarehouse($req){
        $product = DB::connection('mysql')->table("cscart_nk_site_loc");
        $request = $req->all();
        if(!empty($request["sitecode"])){
            $ex = explode(",", $request["sitecode"]);
            $product->whereIn("siteCode", $ex);
        }
        if(!empty($request["status"])){
            $ex = explode(",", $request["status"]);
            $product->whereIn("status", $ex);
        }
        $fields = [
            "siteCode",
            "siteLocId",
            "siteLocDesc",
            "status"
        ];
        $rs = $product->select($fields)->get()->toArray();
        //insert sql server
        foreach ($rs as $r){
            DB::connection('sqlsrv')->table("NK_SITE_LOC")->delete()->where("Site_Code","=",$r->siteCode)->where("Site_loc_id","=",$r->siteLocId);
            DB::connection('sqlsrv')->table("NK_SITE_LOC")->insert([
                "Site_Code" => $r->siteCode,
                "Site_loc_id" => $r->siteLocId,
                "Site_loc_desc" => $r->siteLocDesc,
                "status" => $r->status
            ]);
        }
        return array(
            'status' => 200,
            'message' => "Sync Successfull !"
        );
    }

    public function syncProductStatus2Redis($request){
        $pids        = array();
        $input       = $request->all();
        $product_ids = !empty($input['pids']) ? $input['pids'] : '';
        $chunk       = !empty($input['chunk']) ? intval($input['chunk']) : 10;
        if (!empty($product_ids)) {
            $pos = strpos($product_ids, ',');
            if ($pos === false) {
                $pids[] = (int)$product_ids;
            } else {
                $prod_ids = explode(',', $product_ids);
                if ($prod_ids) {
                    foreach ($prod_ids as $id) {
                        $pids[] = (int)$id;
                    }
                }
            }
        }
        if(!empty($pids)){
            $list_id = implode(",",$pids);
            $products     = DB::select("SELECT product_id,`status`,hidden_option FROM cscart_products WHERE product_id IN ({$list_id})");
            if(!empty($products)){
                $products = json_decode(json_encode($products),true);
                foreach (array_chunk($products,$chunk) as $chunk_products) {
                    foreach ($chunk_products as $product) {
                        $current = $this->redis->getValue("stock_product_{$product['product_id']}",2,0);
                        $update_data = json_decode($current,true);
                        $update_data['status']        = $product['status'];
                        $update_data['hidden_option'] = $product['hidden_option'];
                        $this->redis->setValue("stock_product_{$product['product_id']}", json_encode($update_data), 0, 2);
                    }
                }
                return [
                    'status'=> 201,
                    'msg'   => 'synced redis',
                    'data'  => [
                        'pids'=> array_column($products,'product_id')
                    ]
                ];
            }
            return [
                'status'=> 304,
                'msg'   => 'Not Modified'
            ];
        }
        return [
            'status'=> 304,
            'msg'   => 'Not Modified'
        ];
    }

}
