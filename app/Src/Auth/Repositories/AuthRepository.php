<?php
namespace App\Src\Auth\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;
use DB;
use App\Src\Auth\Transformers\UsersTransformer;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;
//Use App\Helpers\Cache\RedisManager;

class AuthRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;
    //protected $redis;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        UsersTransformer $transformer
        //RedisManager $redis
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new Users;
        $this->transformer = $transformer;
        //$this->redis = $redis;

    }

    public function sigin($request)
    {
        $params = $request->all();
        $model = $this->model;
        try {
            $valid = Validator::make($request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required',
                ],
                [
                    'email.required' => 'Vui lòng nhập Emai của bạn',
                    'email.email' => 'Email không đúng định dạng',
                    'password.required' => 'Vui lòng nhập mật khẩu của bạn'
                ]
            );

            if ($valid->passes()) {
                $email = $params['email'];
                $password = $params['password'];
                $query = DB::table('cscart_users AS u');
                $query->join ('cscart_user_profiles AS p', 'u.user_id', '=','p.user_id');
                $query->where('u.status','A');
                $query->where('u.email',$email);
                $user = $query->select('u.user_id', 'u.salt' , 'u.password' , 'u.email', 'u.firstname','u.lastname','u.phone','u.avatar','p.gender','u.avatar_socical')->get();

                if ($user->isEmpty()) {
                    $data_error = 'Tên người dùng và mật khẩu bạn đã nhập không hợp lệ. Xin vui lòng thử lại';
                    return [
                        'status' => 400,
                        'message' => $data_error
                    ];
                }else{
                    if (trim($user[0]->password) != $this->helper->generatePass($password, $user[0]->salt)) {
                        $data_error = 'Tên người dùng và mật khẩu bạn đã nhập không hợp lệ. Xin vui lòng thử lại';
                        return [
                            'status' => 400,
                            'message' => $data_error
                        ];
                    }else{
                        DB::table('cscart_users')
                            ->where('user_id', $user[0]->user_id)
                            ->update(['last_login' => strtotime(date('Y-m-d H:i:s'))]);

                        $avatar = $user[0]->avatar ? 'https://beta.nguyenkim.com/images/companies/_1/avatar_app/' . $user[0]->avatar : '';
                        if($avatar == ''){
                            $avatar =  $user[0]->avatar_socical;
                        }
                        $data = [
                            'user_id' => $user[0]->user_id,
                            'login_time' => date('Y-m-d H:i:s'),
                            'email' => $user[0]->email,
                            'firstname' => $user[0]->firstname,
                            'lastname' => $user[0]->lastname,
                            'phone' => $user[0]->phone,
                            'avatar' => $avatar,
                            'gender' => $user[0]->gender
                        ];
                        return [
                            'status' => 200,
                            'message' => 'Thành công',
                            'data' => $data
                        ];
                    }
                }
            } else {
                $errors = $valid->errors();
                $data_error = [];
                !$errors->has('email') ?: $data_error['email'] = $errors->first('email');
                !$errors->has('password') ?: $data_error['password'] = $errors->first('password');
                return [
                    'status' => 400,
                    'message' => $data_error
                ];
            }
        } catch
        (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function issigin($request)
    {
        $params = $request->all();
        $model = $this->model;
        try {
            $email = $params['email'];

            $user = Users::select('user_id', 'salt', 'password')->where('email', '=', $email)
                ->where('status', '=', 'A')->where('confirmed', '=', 'Y')->first();

            if ($user == null)
                return ['success' => false, 'message' => 'Invalid user'];

            $isLogged = $this->redis->getValue('session_user_id_' . $user->user_id, 2);
            if(!$isLogged)
                return ['success' => false, 'message' => 'User not login'];

            return [
                'success' => true,
                'message' => 'User Logged'
            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function check_email($request)
    {
        $params = $request->all();
        $model = $this->model;
        try {
            $email = $params['email'];
            $user = Users::select('user_id', 'salt', 'password')->where('email', '=', $email)
                ->where('status', '=', 'A')->where('confirmed', '=', 'Y')->first();

            if ($user == null){
                return ['success' => true, 'message' => 'Email not registered'];
            }
            else{
                return ['success' => false, 'message' => 'Email was registered'];
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}
