<?php
namespace App\Src\Auth\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;
use DB;
use App\Src\Auth\Transformers\UsersTransformer;
use App\Models\Users;
use Illuminate\Support\Facades\Validator;

class UsersRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        UsersTransformer $transformer
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new Users;
        $this->transformer = $transformer;

    }
    public function create($request)
    {
        $params = $request->all();
        $model = $this->model;
        try {

            $phone = substr($params['phone'], -9);
            $user = DB::select("SELECT RIGHT(REPLACE(phone, ' ', ''), 9) 
                                 FROM cscart_users WHERE RIGHT(REPLACE(phone, ' ', ''), 9)='{$phone}'");
            $valid = Validator::make($request->all(),
                [
                    'email' => 'required|unique:cscart_users|email',
                    'phone' => 'required|unique:cscart_users',
                    'password' => 'required|min:6|confirmed',
                    'password_confirmation' => 'required',
                    'firstname' => 'required|min:3',
                    'gender' => 'required'
                ],
                [
                    'email.required' =>'Email không được rỗng',
                    'email.unique' =>'Email đã tồn tại',
                    'email.email' =>'Email không hợp lệ',
                    'phone.required' =>'Số điện thoại không được rỗng',
                    'phone.unique' =>'Số điện thoại đã tồn tại',
                    'password.required' =>'Mật khẩu không được rỗng',
                    'password.min' =>'Mật khẩu tối thiểu 6 ký tự',
                    'password.confirmed' =>'Mật khẩu nhập lại không đúng',
                    'password_confirmation.required' =>'Mật khẩu nhập lại không được rỗng',
                    'firstname.required' =>'Họ tên không được rỗng',
                    'firstname.min' =>'Họ tên tối thiểu 3 ký tự',
                    'gender.required' =>'Giới tính không được rỗng'
                ]
            );
            if($valid->passes()){
                $salt = $this->helper->generateSalt();
                $default = [
                    'timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'lang_code' => 'vi',
                    'salt' => $salt,
                    'confirmed' => 'Y',
                    'status' => 'A',
                    'firstname' => $params['firstname'],
                    'phone' => $params['phone'],
                    'firstname' => $params['firstname']
                ];
                $data = array_merge($default, $params);
                $data['password'] = $this->helper->generatePass($params['password'], $salt);
                $model->fill($data);
                $model->save();
                $result = $this->fractal->createData(new Item($model, $this->transformer))->toArray();
                $user_id = $result['data']['user_id'];
                $user_data['user_id'] = $result['data']['user_id'];
                $user_data['email'] = $params['email'];
                $user_data['firstname'] = $params['firstname'];
                $user_data['phone'] = $params['phone'];
                $user_data['gender'] = $params['gender'];
                $data_profile = array(
                    'profile_name' => 'Chính',
                    'user_id' => $user_id,
                    'gender' => $params['gender']
                );
                DB::table('cscart_user_profiles')->insert($data_profile);
                if(isset($params['subcribe']) && $params['subcribe'] == true){
                    // kiem tra email co trong db chua
                    $user = DB::table('cscart_subscribers')->where('email', $params['email'])->first();
                    if(count($user) == 0){
                        $ip = $_SERVER['REMOTE_ADDR'];
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://api.zerobounce.net/v2/validate?api_key=5913761577bd42dfa3484ed7a4e76fe6&email=".$params['email']."&ip_address=".$ip."",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "GET",
                            CURLOPT_HTTPHEADER => array(
                                "cache-control: no-cache",
                                "postman-token: 3632420e-1097-d677-0ece-2c64ea7a4837"
                            ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);
                        if ($err) {
                            echo "cURL Error #:" . $err;
                        } else {
                            $fullname = explode(' ', $params['firstname']);
                            if(count($fullname)> 1){
                                $last_name =  $fullname[0];
                                $first_name = str_replace($last_name,"",$params['firstname']);
                            }else{
                                $last_name = '';
                                $first_name = $params['firstname'];
                            }
                            $lastname =
                            $res = json_decode($response,true);
                            if($res['status'] == 'valid'){
                                if($params['gender'] == 1)
                                    $gender = 'M';
                                else
                                    $gender = 'W';
                                DB::table('cscart_subscribers')->insert([
                                    'last_name' => $last_name,
                                    'first_name' => $first_name,
                                    'email' => $params['email'],
                                    'phone' => $params['phone'],
                                    'gender' => $gender,
                                    'register_source' => 'user-registration',
                                    'timestamp' => time()
                                ]);
                            }
                        }
                    }
                }
                return [
                    'success' => true,
                    'message' => 'Registry successfull',
                    'data' => $user_data
                ];
            }else{
                $errors = $valid->errors();
                $data_error = [];
                !$errors->has('email') ?: $data_error['email'] = $errors->first('email');
                !$errors->has('phone') ?: $data_error['phone'] = $errors->first('phone');
                !$errors->has('password') ?: $data_error['password'] = $errors->first('password');
                !$errors->has('password_confirmation') ?: $data_error['password_confirmation'] = $errors->first('password_confirmation');
                !$errors->has('firstname') ?: $data_error['firstname'] = $errors->first('firstname');
                !$errors->has('gender') ?: $data_error['gender'] = $errors->first('gender');
                return [
                    'success' => false,
                    'message' => $data_error,
                    'data' => []
                ];
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function trackingOrder($request)
    {
        if ($request->input('transaction_id') == '')
            return ['success' => false, 'message' => 'Chưa có tham số'];

        $transaction_id = $request->input('transaction_id');

        $headers = $request->header();
        $user_gateway = CscartNkUserGateway::checkValidUser(
            $headers['authorization'][0],
            $headers['password'][0]
        );

        $order = Orders::where('order_id', $transaction_id)
            ->where('utm_source', $user_gateway->partner_name)->first();

        if ($order == null)
            return ['success' => false, 'message' => 'Không tìm thấy đơn hàng'];

        $status_id = $this->helper->mappingOrderStatus($order->status);
        $result = [
            'success' => true,
            'status' => $status_id
        ];

        return $result;
    }

    public function update($request, $order_id)
    {
        $params = $request->all();
        try {
            $order = Orders::find($order_id);
            Log::info(date('Y-m-d H:i:s'));
            Log::info('Order ID: ' . $order_id);
            Log::info('Params: ' . var_export($params, true));

            if ($order == null)
                abort(400, 'Đơn hàng không tồn tại');

            $order->fill($params);
            $order->update();

            // update order data
            if (isset($params['order_data'])) {
                $data = $params['order_data'];

                foreach ($data as $ckey => $value) {
                    $order_data = OrderData::where('order_id', $order_id)
                        ->where('type', $value['type'])->first();

                    if ($order_data == null) {
                        $order_data = new OrderData();
                        $value['order_id'] = intval($order_id);
                        $value['type'] = $value['type'];
                        $value['data'] = $value['data'];
                        $order_data->fill($value);
                        $order_data->save();
                    } else {
                        DB::table('cscart_order_data')
                            ->where('order_id', $order_id)
                            ->where('type', $value['type'])
                            ->update(['data' => $value['data']]);
                    }
                }
            }

            // Update don hang tra gop neu co
            if (!empty($params['order_tragop'])) {
                $order_tragop_model = new NkTragopOrders();
                $order_tragop = $params['order_tragop'];
                $order_tragop['order_id'] = $order_id;
                $order_tragop_model->fill($order_tragop);
                $order_tragop_model->save();
            }

            // return ['success' => true, 'message' => 'Cập nhật thành công'];
            return $this->fractal->createData(new Item($order, $this->transformer))->toArray();
        } catch (\Exception $e) {
            abort(400, $e->getMessage());
        }
        abort(500, 'Có lỗi xảy ra!');
        return [];

    }

    public function trackingProductSale($request)
    {
        $params = $request->all();
        $data = [];
        foreach ($params as $product) {
            $tmp = [
                'product_code' => $product['product_code'],
                'total_sales' => 0
            ];
            $startdate = strtotime($product['startdate']);
            $enddate = strtotime($product['enddate']);
            $query = [
                [
                    '$match' => [
                        'product_code' => $product['product_code'],
                        'timestamp' => ['$gte' => $startdate, '$lte' => $enddate]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$product_code',
                        'total' => [
                            '$sum' => '$amount'
                        ],
                    ],
                ]
            ];
            $list = OrderDetail::raw()->aggregate($query);

            foreach ($list as $item) {
                $tmp['total_sales'] = $item->total;
            }
            $data[] = $tmp;
        }

        $result = [
            'success' => true,
            'message' => '',
            'data' => $data
        ];

        return $result;
    }
    public function register_social($request)
    {
        $params = $request->all();
        $model = $this->model;
        try {
            $phone = isset($params['phone']) ? $params['phone'] : '';
            $email = isset($params['email']) ? $params['email'] : '';



            $query = DB::table('cscart_users AS u');
            $query->leftJoin ('cscart_user_profiles AS p', 'u.user_id', '=','p.user_id');
            if($email != ''){
                $query->where('u.email',$email);
            }elseif($phone != ''){
                $query->where('u.phone',$phone);
            }

            $res = $query->select('u.user_id','u.firstname','u.email','u.phone','u.avatar','u.state_id','u.district_id','u.ward_id','u.address','u.social_id','u.is_update_socical','u.avatar_socical','p.gender')->get()->toArray();


            if(count($res) > 0){
                $data = (array)$res[0];
                $datas = $data;
                $datas['avatar'] = '';
                if($data['avatar_socical'] != ''){
                    $datas['avatar'] = $data['avatar_socical'];
                }else{
                    if($data['avatar'] != ''){
                        $datas['avatar'] = 'https://www.nguyenkim.com/images/companies/_1/avatar_app/'.$data['avatar'];
                    }

                }
                foreach ($datas as $i => $value) {
                    if ($value === NULL)
                        $datas[$i] = "";
                }
                return [
                    'success' => 200,
                    'message' => 'Thành công',
                    'data' => $datas
                ];
            }else{
                if($phone != '' || $email){
                    $salt = $this->helper->generateSalt();
                    $default = [
                        'timestamp' => strtotime(date('Y-m-d H:i:s')),
                        'lang_code' => 'vi',
                        'salt' => $salt,
                        'confirmed' => 'Y',
                        'status' => 'A',
                        'firstname' => $params['firstname'],
                        'phone' => $phone,
                        'social_id' => $params['social_id'],
                        'avatar_socical' => $params['avatar_socical']
                    ];

                    $data = array_merge($default, $params);

                    $data['password'] = $this->helper->generatePass('123456', $salt);

                    $model->fill($data);
                    $model->save();
                    $result = $this->fractal->createData(new Item($model, $this->transformer))->toArray();

                    $user_id = $result['data']['user_id'];
                    $data_profile = array(
                        'profile_name' => 'Chính',
                        'user_id' => $user_id,
                        'gender'  => 1
                    );

                    DB::table('cscart_user_profiles')->insert($data_profile);

                    $query = DB::table('cscart_users as u');
                    $query->leftJoin ('cscart_user_profiles AS p', 'u.user_id', '=','p.user_id');
                    $query->where('u.user_id',$user_id);
                    $res = $query->select('u.user_id','u.firstname','u.email','u.phone','u.avatar','u.state_id','u.district_id','u.ward_id','u.address','u.social_id','u.avatar_socical','p.gender')->get()->toArray();
                    $data = (array)$res[0];
                    $datas = $data;
                    $datas['avatar'] = '';
                    if($data['avatar_socical'] != ''){
                        $datas['avatar'] = $data['avatar_socical'];
                    }else{
                        if($data['avatar'] != '')
                            $datas['avatar'] = 'https://www.nguyenkim.com/images/companies/_1/avatar_app/'.$data['avatar'];
                    }

                    foreach ($datas as $i => $value) {
                        if ($value === NULL)
                            $datas[$i] = "";
                    }
                    return [
                        'success' => 200,
                        'message' => 'Thành công',
                        'data' => $datas
                    ];
                }else{
                    return [
                        'success' => 400,
                        'message' => 'Dữ liệu Socical không hợp lệ',
                        'data' => ''
                    ];
                }

            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}
