<?php
namespace App\Src\Auth\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class UsersTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'user_id' => $obj->user_id,
        ];
    }

}
