<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 02/02/2018
 * Time: 11:28 AM
 */

namespace App\Src\System\Repositories;


use App\Helpers\Helpers;
use App\Models\NkActions;
use App\Src\System\Transformers\ActionsTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class NkActionsRepository
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        ActionsTransformer $transformer
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new NkActions();
        $this->transformer = $transformer;

    }

    public function all($request) {
        $params = $request->all();
        $data = [];
        return $data;
    }

    public function create($request) {
        $model = $this->model;
        try {
            if ($model->validate($request->all())) {
                $params = $request->all();
                $model->fill($params);
                $this->model->save();
            } else {
                $errors = $model->errors();
                return ["errors" => $errors];
            }

            return $this->fractal->createData(new Item($this->model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            return ["errors" => $e->getMessage()];
        }
    }

    public function update($request, $id) {
        $params = $request->all();
        $data = [];
        return $data;
    }

    public function show($request, $id) {
        $params = $request->all();
        $data = [];
        return $data;
    }
}