<?php
namespace App\Src\System\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;

use App\Src\System\Transformers\UsergatewayPermissonTransformer;
use App\Models\NkUserGateways;
use App\Models\NkUsergatewayPermisson;
use App\Models\NkActions;
use App\Models\NkModules;
use App\Models\Mongo\NkUsergatewayPermisson as MNkUsergatewayPermisson;

class UsergatewayPermissonRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;
    protected $secutity;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        UsergatewayPermissonTransformer $transformer
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new NkUsergatewayPermisson;
        $this->transformer = $transformer;
    }

    public function all($request)
    {
        $limit = (int)$request->input('limit', 20);
        $offset = (int)$request->input('offset', 0);

        if ($request->has('page') && $request->get('page') > 1) {
            $offset = ((int)$request->get('page', 1) - 1) * (int)$request->input('limit', 20);
        }

        $this->collection->setLimit($limit)->setOffset($offset);

        if ($request->input('p')) {
            $this->collection->filterByField($request->input('p'));
        }

        $resource = new Collection($this->collection->getItems(), $this->transformer);
        $data = $this->fractal->createData($resource)->toArray();
        $paging = new PagingComponent($this->collection->getTotal(), $offset, $limit);

        $data['paging'] = $paging->render();
        return $data;
    }

    public function create($request)
    {
        $model = $this->model;
        try {
            if ($model->validate($request->all())) {
                $params = $request->all();
                $user_gateway_model = NkUserGateways::find(intval($params['user_gateway_id']));
                $user_gateway_name = $user_gateway_model->user_gateway;

                $action = $params['actions'];
                $data_mongo = [];
                foreach ($action as $value) {
                    $data_mongo_temp = [];
                    $model = new NkUsergatewayPermisson();
                    $action_model = NkActions::find(intval($value));
                    $module = NkModules::find(intval($action_model->module_id));

                    $model->user_gateway()->associate($user_gateway_model);
                    $model->action()->associate($action_model);
                    $model->save();

                    $data_mongo_temp['user_gateway'] = $user_gateway_name;
                    $data_mongo_temp['action_path'] = $module->module_code . '/' . $action_model->action;
                    $data_mongo[] = $data_mongo_temp;
                }

                MNkUsergatewayPermisson::insert($data_mongo);
            } else {
                $errors = $model->errors();
                return ["errors" => $errors];
            }

            return $this->fractal->createData(new Item($this->model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            return ["errors" => $e->getMessage()];
        }
    }

    public function update($request, $id)
    {
        $params = $request->all();
        try {
            $model = Modules::find($id);

            if ($model == null)
                return ["errors" => 'Module not found'];

            $model->fill($params);
            $model->update();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            return ["errors" => $e->getMessage()];
        }
    }


}
