<?php
namespace App\Src\System\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;

use App\Src\System\Transformers\ModulesTransformer;
use App\Src\System\Transformers\ListingModulesTransformer;
use App\Models\NkModules;
use App\Models\NkActions;

class NkModulesRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        ModulesTransformer $transformer
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new NkModules;
        $this->transformer = $transformer;

    }

    public function all($request)
    {
        $limit = (int)$request->input('limit', 20);
        $offset = (int)$request->input('offset', 0);

        if ($request->has('page') && $request->get('page') > 1) {
            $offset = ((int)$request->get('page', 1) - 1) * (int)$request->input('limit', 20);
        }

        $modules = NkModules::with('actions');

        if ($request->input('p')) {
            $search = $request->input('p');
            $modules->whereHas('actions', function ($query) use ($search) {
                $query->where('module_code', 'LIKE', "%$search%");
                $query->orWhere('module_name', 'LIKE', "%$search%");
            });
        }

        $paging = new PagingComponent($modules->count(), $offset, $limit);

        $list = $modules->limit($limit)->offset($offset)->get();
        $resource = new Collection($list, new ListingModulesTransformer());
        $data = $this->fractal->createData($resource)->toArray();
        $data['paging'] = $paging->render();

        return $data;
    }

    public function create($request)
    {
        $model = $this->model;
        try {
            if ($model->validate($request->all())) {
                $params = $request->all();
                $this->model->fill($params);
                $this->model->save();

                // Insert actions
                $actions = $params['actions'];
                foreach ($actions as $value) {
                    $action_model = new NkActions();
                    $action_model->module()->associate($this->model);
                    $action_model->fill($value);
                    $action_model->save();
                }
            } else {
                $errors = $model->errors();
                return ["errors" => $errors];
            }

            return $this->fractal->createData(new Item($this->model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            return ["errors" => $e->getMessage()];
        }
    }

    public function update($request, $id)
    {
        $params = $request->all();
        try {
            $model = Modules::find($id);

            if ($model == null)
                return ["errors" => 'Module not found'];

            $model->fill($params);
            $model->update();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            return ["errors" => $e->getMessage()];
        }
    }


}
