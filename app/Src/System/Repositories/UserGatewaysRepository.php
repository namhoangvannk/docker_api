<?php
namespace App\Src\System\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;

use App\Src\System\Transformers\UserGatewaysTransformer;
use App\Src\System\Transformers\ListingUserGatewaysTransformer;
use App\Models\NkUserGateways;
use App\Models\NkUserGatewayIps;
use App\Models\Mongo\NkUserGateways as MNkUserGateways;

class UserGatewaysRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;

    public function __construct(
        Helpers $helper,
        Manager $fractal,
        UserGatewaysTransformer $transformer
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new NkUserGateways;
        $this->transformer = $transformer;

    }

    public function all($request)
    {
        $limit = (int)$request->input('limit', 20);
        $offset = (int)$request->input('offset', 0);

        if ($request->has('page') && $request->get('page') > 1) {
            $offset = ((int)$request->get('page', 1) - 1) * (int)$request->input('limit', 20);
        }

        $modules = UserGateway::with('user_gateway_ips');

        $paging = new PagingComponent($modules->count(), $offset, $limit);

        $list = $modules->limit($limit)->offset($offset)->get();
        $resource = new Collection($list, new ListingUserGatewaysTransformer());
        $data = $this->fractal->createData($resource)->toArray();
        $data['paging'] = $paging->render();

        return $data;
    }

    public function create($request)
    {
        $model = $this->model;
        try {
            if ($model->validate($request->all())) {
                $params = $request->all();

                $params['token'] = base64_encode(gzcompress(serialize($params['token'])));
                $model->fill($params);
                $model->save();

                // Insert ip address
                $ip_address = $params['ip_address'];
                $ip_address_str = '';
                foreach ($ip_address as $value) {
                    $user_gateway_ip_model = new NkUserGatewayIps();
                    $user_gateway_ip_model->user_gateway()->associate($model);
                    $user_gateway_ip_model->ip_address = $value;
                    $user_gateway_ip_model->save();
                    $ip_address_str .= $value.',';
                }

                // Update mongo
                $params['ip_address'] = substr($ip_address_str, 0, strlen($ip_address_str) - 1);
                MNkUserGateways::insert($params);
            } else {
                $errors = $model->errors();
                return ["errors" => $errors];
            }

            return $this->fractal->createData(new Item($this->model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
            return ["errors" => $e->getMessage()];
        }
    }

    public function update($request, $id)
    {
        $params = $request->all();
        try {
            $model = Modules::find($id);

            if ($model == null)
                return ["errors" => 'User gateway not found'];

            $model->fill($params);
            $model->update();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            return ["errors" => $e->getMessage()];
        }
    }


}
