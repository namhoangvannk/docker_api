<?php
namespace App\Src\Backend\System\Transformers;

use League\Fractal\TransformerAbstract;

class UsergatewayIpsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj->id,
            'user_gateway_id' => (int)$obj->user_gateway_id,
            'ip_address' => $obj->ip_address,
            'created_at' => (string)$obj->created_at,
            'updated_at' => (string)$obj->updated_at
        ];
    }
}