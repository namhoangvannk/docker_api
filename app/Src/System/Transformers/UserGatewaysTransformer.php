<?php
namespace App\Src\System\Transformers;

use League\Fractal\TransformerAbstract;

class UserGatewaysTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj->id,
            'user_gateway' => $obj->user_gateway,
            'token' => $obj->token,
            'is_actived' => $obj->is_actived,
            'description' => $obj->description,
            'created_at' => $obj->created_at,
            'updated_at' => $obj->updated_at
        ];
    }
}
