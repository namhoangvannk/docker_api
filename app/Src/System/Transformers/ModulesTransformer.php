<?php
namespace App\Src\System\Transformers;

use League\Fractal\TransformerAbstract;

class ModulesTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj->id,
            'module_code' => $obj->module_code,
            'module_name' => $obj->module_name,
            'created_at' => $obj->created_at,
            'updated_at' => $obj->updated_at
        ];
    }
}