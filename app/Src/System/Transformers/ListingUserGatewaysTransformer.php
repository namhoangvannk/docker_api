<?php
namespace App\Src\Backend\System\Transformers;

use League\Fractal\TransformerAbstract;

class ListingUserGatewaysTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'user_gateway_ips'
    ];

    public function transform($obj)
    {
        return [
            'id' => (int)$obj->id,
            'user_gateway' => $obj->user_gateway,
            'token' => $obj->token
        ];
    }

    public function includeUsergatewayIps($parent)
    {
        return $this->collection($parent->user_gateway_ips, new UsergatewayIpTransformer());
    }
}