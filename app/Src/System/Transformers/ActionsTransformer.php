<?php
namespace App\Src\System\Transformers;

use League\Fractal\TransformerAbstract;

class ActionsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj->id,
            'module_id' => (int)$obj->module_id,
            'action' => $obj->action,
            'display' => $obj->display,
            'created_at' => (string)$obj->created_at,
            'updated_at' => (string)$obj->updated_at
        ];
    }
}