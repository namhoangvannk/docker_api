<?php
namespace App\Src\System\Transformers;

use League\Fractal\TransformerAbstract;

class UsergatewayPermissonTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id' => (int)$obj->id,
            'user_gateway_id' => $obj->user_gateway_id
        ];
    }
}