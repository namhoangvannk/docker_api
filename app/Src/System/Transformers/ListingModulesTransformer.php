<?php
namespace App\Src\System\Transformers;

use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;

class ListingModulesTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'actions'
    ];

    public function transform($obj)
    {
        return [
            'id' => (int)$obj->id,
            'module_name' => $obj->module_name,
        ];
    }

    public function includeActions($module)
    {
        return $this->collection($module->actions, new ActionsTransformer());
    }
}