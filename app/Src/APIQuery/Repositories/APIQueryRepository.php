<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 18/05/2018
 * Time: 2:38 PM
 */

namespace App\Src\APIQuery\Repositories;
use App\Base\Repositories\RepositoryBase;
use App\Helpers\CryptAES\CryptAES;
use App\Helpers\Helpers;
use DB;
use Log;

class APIQueryRepository extends RepositoryBase
{
    protected $get_permission;

    public function __construct(Helpers $helper){

        $this->crypt = new CryptAES();
        $this->get_permission = [
            'allow_update' => [
                'cscart_user_session_products'
            ],
            'allow_delete' =>[
                'cscart_user_session_products'
            ],
            'allow_replace'=>[
                'cscart_user_session_products'
            ]
        ];
    }
    /**
     * ## API Query
     * @param $request query string
     * @return array status
     *
     **/
    public function db_query($request){
        $params_query = $this->crypt->cryptoJsAesDecrypt($request->params);
        Log::info('ParamsQuery: '. json_encode($params_query));
        switch ($params_query['action']) {
            case "update":{
                if(in_array($params_query['table'],$this->get_permission['allow_update'])){
                    $result = DB::table($params_query['table'])->whereRaw($params_query['condition'])->update($params_query['query_params']);
                    if($result == 1){
                        return [
                            'status'=>true,
                            'message'=>'update query success'
                        ];
                    } else {
                        return [
                            'status'=>false,
                            'message'=>'update query failed'
                        ];
                    }
                }
            }
                break;
            case "delete":{
                if(in_array($params_query['table'],$this->get_permission['allow_delete'])){
                    $result = DB::table($params_query['table'])->whereRaw($params_query['condition'])->delete();
                    if($result == 1){
                        return [
                            'status'=>true,
                            'message'=>'delete query success! :)'
                        ];
                    } else {
                        return [
                            'status'=>false,
                            'message'=>'delete query failed :('
                        ];
                    }
                }
            }
                break;
            case "replace": {
                if(in_array($params_query['table'],$this->get_permission['allow_replace'])){
                    $a= $params_query['table'];
                    $b = $params_query['query_params'];
                    $b2 = $params_query['query_params'];
                    $c = DB::getSchemaBuilder()->getColumnListing($a);
                    foreach($b as $col => $value){
                        if(!(in_array($col,$c))){
                            unset($b[$col]);
                        }
                    }
                    $d = array_keys($b);
                    $e = array_values($b);
                    $query = DB::raw("REPLACE INTO ".$a." (".implode(', ',$d).") VALUES ('".implode("', '",$e)."')");
                    $result =(array)$query;
                    $query_str = '';
                    if(!empty($result)){
                        foreach($result as $key => $row){
                            $query_str .= $row;
                        }
                    }
                    DB::beginTransaction();
                    try {
                        DB::statement("".$query_str."");
                        DB::commit();
                        return [
                            'status'=>true,
                            'message'=>'replace query success! :)'
                        ];
                        //all good
                    } catch (\Exception $e) {
                        DB::rollback();
                        return [
                            'status'=>false,
                            'message'=>'replace query failed :('
                        ];
                        // something went wrong
                    }
                }
            }
                break;
            default: {
                return [
                    'status'=>false,
                    'message'=>'do not support! :)'
                ];
            }
        }
    }

}