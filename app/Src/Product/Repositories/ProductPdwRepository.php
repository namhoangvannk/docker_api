<?php
namespace App\Src\Product\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use App\Helpers\Cache\RedisManager;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use DB;
use Maatwebsite\Excel\Excel;

use App\Src\Product\Collections\ProductsCollection;
use App\Src\Product\Transformers\DetailProductTransformer;
use App\Src\Product\Transformers\ProductFeaturePdwTransformer;
use App\Models\Mongo\Products as MProducts;
use App\Models\Mongo\ProductFeature as MProductFeature;
use App\Models\Products;

use Log;

class ProductPdwRepository extends RepositoryBase
{
    protected $manager;
    protected $model;
    protected $redis;

    public function __construct(
        Manager $manager,
        RedisManager $redis
    )
    {
        $this->manager = $manager;
        $this->redis = $redis;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return json|array
     */
    public function all($request)
    {
        $result = $this->allByMongo($request);
        return $result;
    }

    private function allByMongo($request)
    {
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 1000);

        $product = MProductFeature::select(
            'product_id',
            'product_code',
            'list_price',
            'product_short_code',
            'product_features'
        );

        if ($request->input('shortcodes') != null) {
            $productshortcodes = explode(',', $request->input('shortcodes'));
            $product = $product->whereIn('product_short_code', $productshortcodes);
        }

        $list = $product->skip($offset)->take($limit)->get();
        $resource = new Collection($list, new ProductFeaturePdwTransformer());
        $data = $this->manager->createData($resource)->toArray();

        return $data;
    }

    public function show($request, $product_code)
    {
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 1);

        $product = MProducts::select(
            'product_id',
            'product_code',
            'product_name',
            'price',
            'list_price',
            'display_name',
            'model',
            'brand',
            'product_categories',
            'product_options',
            'seo_names'
        );

        $product = $product->where('product_code_nounit', '=', $product_code);

        $list = $product->skip($offset)->take($limit)->get();
        $resource = new Collection($list, new DetailProductTransformer());
        $data = $this->manager->createData($resource)->toArray();

        return $data;
    }
}

