<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 08/06/2018
 * Time: 10:40 AM
 */

namespace App\Src\Product\Repositories;
use App\Base\Repositories\RepositoryBase;
use Illuminate\Support\Facades\Validator;
use DB;
use Mockery\Exception;

class ProductCateFullModeRepository extends RepositoryBase {

    public function __construct(){

    }

    /*
     * ## PRODUCT FULL MODE
     * Store all data product to Mongo DB
     * @params $request
     * @return json|array
     */
    public function storeProductToMongo($request){
        $params = $request->all();
        $validator = Validator::make($params,[
            'product_id'=>['required'],
            'product_code'=>['required']
        ]);
        $validator->after(function ($validator) {
            if ($validator->errors()->isEmpty()) {
                $data = $validator->getData();
                $check = DB::connection('mongodb')->collection('products_full_mode')->where('product_id',$data['product_id'])->get(['product_id'])->first();
                if($check){
                    $validator->errors()->add('ProductExits', 'Product '.$check['product_id'].' has been created in Mongo');
                }
            }
        });

        if($validator->fails()){
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }
        try {
            $result = DB::connection('mongodb')->collection('products_full_mode')->insert($params);
            return [
                'code'=>200,
                'status'=>true,
                'msg'=>'store product to mongo success'
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => 'errors'
            ];
        }

    }

    /*
     * ## CATE FULL MODE
     * Store all data cate to mongo DB
     * @params $request
     * @return json|array
     */
    public function storeCateToMongo($request){
        $params = $request->all();
        $validator = Validator::make($params,[
            'category_id'=>['required'],
            'parent_id'=>['required']
        ]);
        $validator->after(function ($validator) {
            if ($validator->errors()->isEmpty()) {
                $data = $validator->getData();
                $check = DB::connection('mongodb')->collection('category_full_mode')->where('category_id',$data['category_id'])->get(['category_id'])->first();
                if($check){
                    $validator->errors()->add('CategoryExits', 'Category '.$check['category_id'].' has been created in Mongo');
                }
            }
        });

        if($validator->fails()){
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }
        try {
            $result = DB::connection('mongodb')->collection('category_full_mode')->insert($params);
            return [
                'code'=>200,
                'status'=>true,
                'msg'=>'store cate to mongo success'
            ];
        } catch (\Exception $e) {
            return [
                'code'=>500,
                'status' => false,
                'msg' => 'errors'
            ];
        }
    }

}