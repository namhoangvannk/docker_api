<?php

namespace App\Src\Product\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use App\Helpers\Cache\RedisManager;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use DB;
use Maatwebsite\Excel\Excel;

use App\Src\Product\Collections\ProductsCollection;
use App\Src\Product\Transformers\ProductTransformer;
use App\Src\Product\Transformers\UpdateProductTransformer;
use App\Src\Product\Transformers\ListingProductTransformer;
use App\Src\Product\Transformers\DetailProductTransformer;
use App\Src\Product\Transformers\ProductFeaturePdwTransformer;

use App\Models\Mongo\Products as MProducts;
use App\Models\Mongo\ProductFeature as MProductFeature;

use App\Models\Products;
use App\Models\ProductDescriptions;
use App\Models\ProductsCategories;
use App\Models\ProductPrices;
use App\Models\SeoNames;
use App\Models\Images;
use App\Models\ImagesLinks;
use App\Models\CommonDescriptions;
use App\Models\ProductOptions;
use App\Models\ProductOptionsDescriptions;
use App\Models\ProductOptionVariants;
use App\Models\ProductOptionVariantsDescriptions;
use App\Models\ProductFeaturesValues;
use App\Models\ProductGlobalOptionLinks;
use App\Models\Mongo\NkRegionAmounts as MNkRegionAmounts;

use Log;

class ProductsRepository extends RepositoryBase
{
    protected $manager;
    protected $collection;
    protected $Transformer;
    protected $trandetail;
    protected $model;
    protected $redis;

    public function __construct(
        Manager $manager,
        RedisManager $redis,
        ProductsCollection $collection,
        ProductTransformer $Transformer,
        DetailProductTransformer $trandetail)
    {
        $this->manager = $manager;
        $this->collection = $collection;
        $this->transformer = $Transformer;
        $this->model = new Products();
        $this->redis = $redis;
        $this->trandetail = $trandetail;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return json|array
     */
     public function all($request)
     {
         $key_redis = "productids_";
         $key_redis .= $request->input('productIds');

         $data = $this->redis->getValue($key_redis, 1);
         if ($data)
             return $data;

         $result = $this->allByMongo($request);
         return $result;
     }

     private function allByMongo($request)
     {
         $offset = (int)$request->input('offset', 0);
         $limit = (int)$request->input('limit', 20);
         $params = $request->all();
         $key_redis = "productids_";
         $key_redis .= $request->input('productIds');

         $selectField = [
             'product_id',
             'product_code',
             'price',
             'list_price',
             'product_name',
             'display_name',
             'seo_names',
             'nk_is_shock_price'
         ];

         $fields = [];
         if(!empty($params['fields']))
             $fields = explode(',', $params['fields']);

         $select = array_merge($selectField, $fields);
         $product = MProducts::select($select);

         if($request->input('category') != null) {
             $product = $product->where('categories', 'like', "%" . $request->input('category') . '%');
         }

         if ($request->input('status') != null) {
             $product = $product->whereIn('status', explode(',', $request->input('status')));
         }

         if($request->input('productIds') != null) {
             $productIds = array_map('intval', explode('_', $request->input('productIds')));
             $product = $product->whereIn('product_id', $productIds);
         }

         if ($request->input('productcodes') != null) {
             $productcode = explode(',', $request->input('productcode'));
             $product = $product->whereIn('product_code', $productcode);
         }

         if ($request->input('shortcodes') != null) {
             $productshortcodes = explode(',', $request->input('shortcodes'));
             $product = $product->whereIn('product_short_code', $productshortcodes);
         }

         $paging = new PagingComponent($product->count(), $offset, $limit);

         $list = $product->skip($offset)->take($limit)->get();
         $resource = new Collection($list, new ListingProductTransformer());
         $data = $this->manager->createData($resource)->toArray();
         $data['paging'] = $paging->render();

         $this->redis->setValue($key_redis, $data, env('CACHE_PRODUCT_DETAIL', 300), 1);

         return $data;
     }

     public function allBySql($request)
     {
         $model       = $this->model;
         $columns     = $model::getColumns();
         $ambiguousColumns = ['product_id'];
         $collection  = $this->collection;
         $transformer = $this->transformer;

         $offset = 0;
         $limit  = 1;
         if ($request->input('offset')) {
             $offset = (int) $request->input('offset');
         }
         if ($request->input('limit')) {
             $limit = (int) $request->input('limit');
         }

         call_user_func_array(
             [$collection->getDb(), 'leftJoin'],
             ['cscart_product_descriptions', 'cscart_products.product_id', 'cscart_product_descriptions.product_id']
         );

         call_user_func_array(
             [$collection->getDb(), 'leftJoin'],
             ['cscart_product_prices', 'cscart_products.product_id', 'cscart_product_prices.product_id']
         );

         call_user_func_array(
             [$collection->getDb(), 'leftJoin'],
             ['cscart_seo_names', 'cscart_products.product_id', 'cscart_seo_names.object_id']
         );

         // Select
         $collection->getDb()->addSelect(
             'cscart_products.product_id',
             'cscart_products.product_code',
             'cscart_product_descriptions.product as product_name',
             'cscart_product_descriptions.display_name',
             'cscart_product_prices.price',
             'cscart_seo_names.name as url_name'
         );

         // Limit
         $collection->setLimit($limit)->setOffset($offset);

         // Condition
         call_user_func_array(
             [$collection->getDb(), 'where'],
             ['cscart_products.product_code', '=', $request->input('product_code')]
         );

         call_user_func_array(
             [$collection->getDb(), 'whereIn'],
             ['cscart_products.status', ['H', 'A' ]]
         );

         call_user_func_array(
             [$collection->getDb(), 'where'],
             ['cscart_seo_names.type', '=', 'p']
         );

         call_user_func_array(
             [$collection->getDb(), 'where'],
             ['cscart_seo_names.lang_code', '=', 'vi']
         );

         call_user_func_array(
             [$collection->getDb(), 'where'],
             ['cscart_product_descriptions.lang_code', '=', 'vi']
         );

         $itemsResult = $collection->getItems();
         $resource = new Collection($itemsResult, $transformer);
         $data = $this->manager->createData($resource)->toArray();
         $data['paging'] = $collection->getPaging();

         return $data;
     }

     public function show($request, $product_code)
     {
         $collection = $this->collection;
         $transformer = $this->transformer;
         $offset = (int)$request->input('offset', 0);
         $limit = (int)$request->input('limit', 1);
         $key_redis = 'product_detail_'. $product_code;

         $data = $this->redis->getValue($key_redis, 1);
         if($data)
             return $data;

         $product = MProducts::select(
             'product_id',
             'product_code',
             'product_name',
             'price',
             'list_price',
             'display_name',
             'model',
             'brand',
             'product_categories',
             'product_options',
             'seo_names',
             'product_features',
             'product_descriptions',
             'images'
         );

         $product = $product->where('product_code', '=', $product_code);

         $list = $product->skip($offset)->take($limit)->get();
         $resource = new Collection($list, $transformer);
         $data = $this->manager->createData($resource)->toArray();
         $this->redis->setValue($key_redis, $data, env('CACHE_PRODUCT_DETAIL', 300), 1);

         return $data;
     }

     public function partnerDetail($request, $product_code)
     {
         $collection = $this->collection;
         $transformer = $this->transformer;
         $offset = (int)$request->input('offset', 0);
         $limit = (int)$request->input('limit', 1);

         $product = MProducts::select(
             'product_id',
             'product_code',
             'product_name',
             'price',
             'list_price',
             'display_name',
             'model',
             'brand',
             'product_categories',
             'product_options',
             'seo_names',
             'product_features',
             'product_descriptions',
             'images'
         );

         $product = $product->where('product_code_nounit', '=', $product_code);

         $list = $product->skip($offset)->take($limit)->get();
         $resource = new Collection($list, $this->trandetail);
         $data = $this->manager->createData($resource)->toArray();

         return $data;
     }

     public function create($request)
     {
         $params = $request->all();
         $products = $params['products'];

         try {
             $model = $this->model;
             $model->fill($products);
             $model->save();
             $product_id = $model->product_id;
             $products['product_id'] = $product_id;

             // Insert product description
             $product_descriptions = $params['product_descriptions'];
             $product_descriptions['product_id'] = $product_id;
             ProductDescriptions::insert($product_descriptions);

             // Insert products_categories
             $products_categories = $params['products_categories'];
             $arr_products_categories = [];
             $str_categories = '';
             foreach($products_categories as $key => $value) {
                 $value['product_id'] = $product_id;
                 $str_categories .= $value['category_id'].',';
                 $arr_products_categories[] = $value;
             }
             ProductsCategories::insert($arr_products_categories);

             // Insert product_prices
             $product_prices = $params['product_prices'];
             $product_prices['product_id'] = $product_id;
             ProductPrices::insert($product_prices);

             // Insert seo_names
             if(isset($params['seo_names'])) {
                 $arr_seo_names = $params['seo_names'];
                 $arr_seo_names['object_id'] = $product_id;
                 SeoNames::insert($arr_seo_names);
             }

             // Insert image and link image
             $images = $params['images'];
             $arr_images_link = [];
             $arr_images = [];
             foreach($images as $key => $value) {
                 $image = [];
                 $image['image_path'] = $value['image_path'];
                 $image['image_x'] = $value['image_x'];
                 $image['image_y'] = $value['image_y'];
                 $images_model = Images::create($image);

                 // Insert image link
                 $image_link = [];
                 $image_link['object_id'] = $product_id;
                 $image_link['image_id'] = $images_model->id;
                 $image_link['object_type'] = $value['object_type'];
                 $image_link['detailed_id'] = $images_model->id;;
                 $image_link['type'] = $value['type'];
                 $image_link['position'] = $value['position'];
                 $arr_images_link[] = $image_link;

                 $arr_images[] = array_merge($image, $image_link);
             }
             ImagesLinks::insert($arr_images_link);

             // Insert cscart_common_descriptions
             $common_descriptions = $params['common_descriptions'];
             $common_descriptions['object_id'] = $product_id;
             CommonDescriptions::create($common_descriptions);

             // Save to mongoDB
             $products = array_merge(['price' => $product_prices['price']], $products);
             $products = array_merge(['categories' =>  \substr($str_categories, 0, \strlen($str_categories) - 1)], $products);
             $products = array_merge(['product_id' => $product_id], $products);
             $products['product_descriptions'] = json_encode($params['product_descriptions']);
             $products['products_categories'] = json_encode($params['products_categories']);
             $products['product_prices'] = json_encode($params['product_prices']);
             $products['seo_names'] = json_encode($params['seo_names']);
             $products['images'] = json_encode($arr_images);
             $products['common_descriptions'] = json_encode($params['common_descriptions']);
             DB::connection('mongodb')->collection('products')->insert($products);

             return $this->manager->createData(new Item($model, new UpdateProductTransformer()))->toArray();
         } catch (\Exception $e) {
             throw new \Exception($e->getMessage());
         }
     }

     public function updatePrice($request)
     {
         $params = $request->all();

         try {
             foreach($params as $key => $value) {
                 $product_id = intval($value['product_id']);
                 $product = Products::find($product_id);
                 $data_mongo = [];

                 if($product == null)
                     continue;

                 $product->list_price = $value['price'];
                 $product->update();

                 $data_mongo['price'] = $value['price'];
                 $data_mongo['list_price'] = $value['price'];

                 // Update product price
                 $product_price = ProductPrices::find($product_id);
                 $product_price->price = $value['price'];
                 $product_price->update();

                 // Insert/update to cscart_product_options
                 if(isset($value['product_options'])){
                     $m_product_option = $this->updateProductOptions($product_id,  $value['product_options']);
                     $data_mongo['product_options'] = json_encode($m_product_option);
                 }

                 DB::connection('mongodb')->collection('products')
                     ->where('product_id', $product_id)->update($data_mongo, ['upsert' => true]);
             }

             return ['message'=>'Update successfull'];
         } catch (\Exception $e) {
             abort(400, $e->getMessage());
         }

         abort(500, 'Có lỗi xảy ra!');
         return [];
     }

     public function update($request, $product_id)
     {
         $params = $request->all();
         $product_id = intval($product_id);
         $mproduct = MProducts::find($product_id);

         try {
             $data_mongo = [];
             if (isset($params['products'])) {
                 $data_mongo = $params['products'];
                 $this->updateProduct($product_id, $params['products']);
             }

             if (isset($params['product_descriptions'])) {
                 $data_mongo['product_descriptions'] = json_encode($params['product_descriptions']);
                 $this->updateProductDesc($product_id, $params['product_descriptions']);
             }

             if (isset($params['product_prices'])) {
                 $data_mongo['product_prices'] = json_encode($params['product_prices']);
                 $data_mongo['price'] = $params['product_prices']['price'];
                 $data_mongo['list_price'] = $params['product_prices']['price'];
                 $this->updateProductPrice($product_id, $params['product_prices']);
             }

             if (isset($params['seo_names'])) {
                 $data_mongo['seo_names'] = json_encode($params['seo_names']);
                 $this->updateSeoName($product_id, $params['seo_names']);
             }

             if (isset($params['images'])) {
                 $arr_image_data = $this->updateImages($product_id, $params['images']);
                 $data_mongo['images'] = json_encode($arr_image_data);
             }

             if (isset($params['products_categories'])) {
                 $data_mongo['products_categories'] = json_encode($params['products_categories']);
                 $data_mongo['categories'] = json_encode($params['products_categories']);
                 $categories =  $this->updateProductCate($product_id, $params['products_categories']);
                 $data_mongo['categories'] = $categories;
             }

             if (isset($params['common_descriptions'])) {
                 $data_mongo['common_descriptions'] = json_encode($params['common_descriptions']);
                 $this->updateCommonDesc($product_id, $params['common_descriptions']);
             }

             if (isset($params['product_options'])) {
                 $m_product_option = $this->updateProductOptions($product_id, $params['product_options']);
                 $data_mongo['product_options'] = json_encode($m_product_option);
             }

             if (isset($params['product_features_values'])) {
                 $data_mongo['product_features_values'] = json_encode($params['product_features_values']);
                 $this->updateProductFeature($product_id, $params['product_features_values']);
             }


             if (isset($params['global_option_links'])) {
                 $data_mongo['global_option_links'] = json_encode($params['global_option_links']);
                 $this->updateGlobalOptionLink($product_id, $params['global_option_links']);
             }

             DB::connection('mongodb')->collection('products')
                 ->where('product_id', $product_id)->update($data_mongo, ['upsert' => true]);

             return $this->getProduct($product_id);
         } catch(\Exception $e) {
             abort(400, $e->getMessage());
         }
         abort(500, 'Có lỗi xảy ra!');
         return [];
     }

     public function getProduct($product_id)
     {
         $product = Products::findOrFail($product_id);
         $resource = new Item($product, new UpdateProductTransformer());
         return $this->manager->createData($resource)->toArray();
     }

     private function updateProduct($product_id, $data)
     {
         $product = Products::find($product_id);
         $product->fill($data);
         $product->update();
     }

     private function updateProductDesc($product_id, $data)
     {
         $product_desc = ProductDescriptions::find($product_id);
         $product_desc->fill($data);
         $product_desc->update();
     }

     private function updateProductPrice($product_id, $data)
     {
         $product_prices = ProductPrices::find($product_id);
         $product_prices->fill($data);
         $product_prices->update();
     }

     private function updateSeoName($product_id, $data)
     {
         $seo_names = SeoNames::find($product_id);
         $seo_names->fill($data);
         $seo_names->update();
     }

     private function updateProductCate($product_id, $data)
     {
         // Delete all by product id
         $delete = ProductsCategories::where('product_id', '=', $product_id);
         $delete->delete();

         $str_categories = '';
         foreach ($data as $ckey => $value) {
             $str_categories .= $value['category_id'] . ',';
             $model = new ProductsCategories();
             $value['product_id'] = $product_id;
             $model->fill($value);
             $model->save();
         }
         $categories = substr($str_categories, 0, strlen($str_categories) - 1);
         return $categories;
     }

     private function updateImages($product_id, $images_data)
     {
         $arr_image_data = [];
         foreach ($images_data as $ikey => $ivalue) {
             $image_id = intval($ivalue['image_id']);
             $images_model = Images::find($image_id);
             $image = [];

             if ($images_model == null)
                 $images_model = new Images();

             $image['image_path'] = $ivalue['image_path'];
             $image['image_x'] = $ivalue['image_x'];
             $image['image_y'] = $ivalue['image_y'];
             $images_model->fill($image);
             $images_model->save();

             if($image_id <= 0)
                 $image_id = $images_model->image_id;

             // update image link
             $image_link_model = ImagesLinks::where('object_id', '=', $product_id)
                 ->where('image_id', '=', $image_id)->first();
             if ($image_link_model == null)
                 $image_link_model = new ImagesLinks();

             $image_link = [];
             $image_link['object_id'] = $product_id;
             $image_link['image_id'] = $image_id;
             $image_link['object_type'] = $ivalue['object_type'];
             $image_link['detailed_id'] = $image_id;
             $image_link['type'] = $ivalue['type'];
             $image_link['position'] = $ivalue['position'];
             $image_link_model->fill($image_link);
             $image_link_model->save();

             $arr_image_data[] = array_merge($image, $image_link);
         }
         return $arr_image_data;
     }

     private function updateCommonDesc($product_id, $data)
     {
         // Delete all by product id
         $delete = CommonDescriptions::where('object_id', '=', $product_id);
         $delete->delete();

         $model = new CommonDescriptions();
         $data['object_id'] = $product_id;
         $model->fill($data);
         $model->save();
     }

     private function updateProductOptions($product_id, $product_options)
     {
         $m_product_option = [];
         foreach ($product_options as $okey => $ovalue) {
             $option_id = intval($ovalue['option_id']);

             $product_options_model = ProductOptions::find($option_id);
             if ($product_options_model == null)
                 $product_options_model = new ProductOptions();

             $product_options_data = [];
             $product_options_data = $ovalue;
             unset($product_options_data['options_descriptions']);
             unset($product_options_data['option_variants']);
             $product_options_data['product_id'] = $product_id;
             $product_options_model->fill($product_options_data);
             $product_options_model->save();

             if ($option_id <= 0) {
                 $option_id = $product_options_model->option_id;
                 $product_options_data['option_id'] = $option_id;
             }

             // Insert or update cscart_product_options_description
             $product_options_desc_model = ProductOptionsDescriptions::find($option_id);
             if ($product_options_desc_model == null)
                 $product_options_desc_model = new ProductOptionsDescriptions();

             $product_options_desc_data = $ovalue['options_descriptions'];
             $product_options_desc_data['option_id'] = $option_id;
             $product_options_desc_model->fill($product_options_desc_data);
             $product_options_desc_model->save();

             // Reset
             $product_options_data['options_descriptions'] = $ovalue['options_descriptions'];

             // Insert or update to cscart_product_option_variants
             $option_variants = $ovalue['option_variants'];
             $arr_tmp = [];
             foreach ($option_variants as $vkey => $vvalue) {
                 $variant_id = intval($vvalue['variant_id']);

                 $option_variants_model = ProductOptionVariants::find($variant_id);
                 if ($option_variants_model == null)
                     $option_variants_model = new ProductOptionVariants();

                 $option_variants_data = [];
                 $option_variants_data = $vvalue;
                 unset($option_variants_data['variant_descriptions']);
                 $option_variants_data['option_id'] = $option_id;
                 $option_variants_model->fill($option_variants_data);
                 $option_variants_model->save();

                 if ($variant_id <= 0) {
                     $variant_id = $option_variants_model->variant_id;
                     $option_variants_data['variant_id'] = $variant_id;
                 }

                         // Insert or update description
                 $option_variants_desc_model = ProductOptionVariantsDescriptions::find($variant_id);
                 if ($option_variants_desc_model == null)
                     $option_variants_desc_model = new ProductOptionVariantsDescriptions();

                 $option_variants_desc_data = $vvalue['variant_descriptions'];
                 $option_variants_desc_data['variant_id'] = $variant_id;
                 $option_variants_desc_model->fill($option_variants_desc_data);
                 $option_variants_desc_model->save();

                 $option_variants_data['variant_descriptions'] = $vvalue['variant_descriptions'];
                 $arr_tmp[] = $option_variants_data;
             }

             $product_options_data['option_variants'] = $arr_tmp;
             $m_product_option[] = $product_options_data;
         }
         return $m_product_option;
     }

     private function updateProductFeature($product_id, $data)
     {
         // Delete all by product id
         $delete = ProductFeaturesValues::where('product_id', '=', $product_id);
         $delete->delete();

         foreach ($data as $key => $value) {
             $value['product_id'] = $product_id;
             $model = new ProductFeaturesValues();
             $model->fill($value);
             $model->save();
         }
     }

     private function updateGlobalOptionLink($product_id, $data)
     {
         // Delete all by product id
         $delete = ProductGlobalOptionLinks::where('product_id', '=', $product_id);
         $delete->delete();

         foreach($data as $key => $value) {
             $value['product_id'] = $product_id;
             $model = new ProductGlobalOptionLinks();
             $model->fill($value);
             $model->save();
         }
     }

     public function updateFromExcel($request)
     {
         try {
             $data = $request->all();

             $arr_temp = [];
             foreach ($data as $key => $value) {
                 $product_code = $value['Article'];
                 $product = Products::where('product_short_code', $product_code)->first();
                 if ($product == null)
                     continue;

                 $shipping_params = unserialize($product->shipping_params);
                 $shipping_params['box_length'] = $value['Dai_TT'] * 10;
                 $shipping_params['box_width'] = $value['Rong_TT'] * 10 ;
                 $shipping_params['box_height'] = $value['Cao_TT'] * 10;

                 //var_dump($product_code);die;
                 $product->shipping_params = serialize($shipping_params);
                 $product->weight = (float)$value['TL_TT'];
                 $product->length = (float)$value['Rong_TT'] * 10 ;
                 $product->width = (float)$value['Dai_TT'] * 10;
                 $product->height = (float)$value['Cao_TT'] * 10;
                 $product->updated_timestamp = strtotime(date("Y-m-d H:i:s"));
                 $product->update();
             }

             $result['message'] = 'Import successfull';
             return $result;
         } catch (\Exception $e) {
             return [
                 'success' => false,
                 'message' => $e->getMessage()
             ];
         }
     }

    public function getRegionsByProduct($request, $product_id)
    {
        $mresion_product = MNkRegionAmounts::raw()->findOne(['product_id' => intval($product_id)]);

        if ($mresion_product == null)
            return ['data' => []];

        $region_amount = json_decode($mresion_product->region_amount, true);

        $data = [];
        foreach ($region_amount as $value) {
            if ($value['amount'] > 0) {
                $data[] = $value;
            }
        }
        return ['data' => $data];
    }

    public function getProductFeaturePdw($request)
    {
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 1000);

        $product = MProductFeature::select(
            'product_id',
            'product_code',
            'list_price',
            'product_short_code',
            'product_features'
        );

        if ($request->input('shortcodes') != null) {
            $productshortcodes = explode(',', $request->input('shortcodes'));
            $product = $product->whereIn('product_short_code', $productshortcodes);
        }

        $list = $product->skip($offset)->take($limit)->get();
        $resource = new Collection($list, new ProductFeaturePdwTransformer());
        $data = $this->manager->createData($resource)->toArray();

        return $data;
    }

    public function getRelationProducts($request, $product_id)
    {
        $result = [];

        $products = Products::join(
            'cscart_products_categories',
            'cscart_products_categories.product_id',
            '=',
            'cscart_products.product_id')
            ->where('cscart_products.status', '=', 'A')
            ->select('cscart_products.product_id');
        //
        $pro_category = ProductsCategories::select(
            'category_id', 'link_type')
            ->where('product_id', '=', $product_id)
            ->where('link_type', '=', 'M')
            ->orderBy('link_type', 'DES');
        $list = $pro_category->skip(0)->take(6)->get();


        foreach ($list as $key => $value) {
            $products = $products ->where('cscart_products_categories.category_id', '=', $value['category_id']);
            $itemsResult = $products ->skip(0)->take(6)->get();
            $result = $itemsResult;
        }
        return array(
            "data" => $result
        );

        /*
                $mresion_product = MNkRegionAmounts::raw()->findOne(['product_id' => intval($product_id)]);

                if ($mresion_product == null)
                    return ['data' => []];

                $region_amount = json_decode($mresion_product->region_amount, true);

                $data = [];
                foreach ($region_amount as $value) {
                    if ($value['amount'] > 0) {
                        $data[] = $value;
                    }
                }
                return ['data' => $data];*/
    }
	
	 public function setRedisValue($request)
    {
        $params = $request->all();
		Log::info('redis value: ' . var_export($params, true));
		
        $this->redis->setValue($params['key'], $params['value'], env('CACHE_PRODUCT_DETAIL', 300), 0, false);
        return ['code' => 'OK'];
    }

}

