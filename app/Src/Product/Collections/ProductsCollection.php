<?php

namespace App\Src\Product\Collections;

use App\Helpers\Database\Collection;

class ProductsCollection extends Collection
{
    /**
     * The table associated with the collection.
     *
     * @var string
     */
    protected $table = 'cscart_products';

    /**
     * Fitler by ids.
     *
     * @param  string $ids
     * @return $this
     */
    public function filterByIds($ids)
    {
        $this->db->whereIn('product_id', explode(',', $ids));

        return $this;
    }

    /**
     * Fitler by skus.
     *
     * @param  string $skus
     * @return $this
     */
    public function filterBySkus($skus)
    {
        $this->db->whereIn('sku', explode(',', $skus));

        return $this;
    }

    /**
     * Filter by a keyword.
     *
     * @param  string $keyword
     * @return $this
     */
    public function filterByKeyword($keyword)
    {
        $this->db->where('name', 'like', '%' . $keyword . '%');

        return $this;
    }
}
