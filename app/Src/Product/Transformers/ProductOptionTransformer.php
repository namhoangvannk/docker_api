<?php

namespace App\Src\Product\Transformers;

use League\Fractal\TransformerAbstract;

class ProductOptionTransformer extends TransformerAbstract
{
    public function transform($category)
    {
        return [
            'option_id' => (int) $category['option_id'],
            'option_name' => $category['option_name'],
            'description' => $category['description']
        ];
    }
}
