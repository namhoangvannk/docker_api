<?php
namespace App\Src\Product\Transformers;

use League\Fractal\TransformerAbstract;

class ProductFeaturePdwTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'product_id' => (int)$obj['product_id'],
            'product_code' => $obj['product_short_code'],
            'list_price' => (float)$obj['list_price'],
            'features' => json_decode($obj->product_features, true),
        ];
    }
}
