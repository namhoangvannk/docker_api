<?php

namespace App\Src\Product\Transformers;

use League\Fractal\TransformerAbstract;

class UpdateProductTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'product_id' => (int)$obj['product_id']
        ];
    }
}
