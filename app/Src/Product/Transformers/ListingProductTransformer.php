<?php

namespace App\Src\Product\Transformers;

use League\Fractal\TransformerAbstract;

class ListingProductTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        $data = [
            'product_id' => (int)$obj->product_id,
            'product_code' => $obj->product_code,
            'product' => $obj->product_name,
            'display_name' => $obj->display_name,
            'price' => (float)$obj->price,
            'list_price' => (float)$obj->list_price,
            'seo_names' => $obj->seo_names,
            'nk_is_shock_price' => $obj->nk_is_shock_price,
        ];

        if(!empty($obj->product_short_code))
            $data['product_short_code'] = $obj->product_short_code;

        if (!empty($obj->images))
            $data['images'] = json_decode($obj->images, true);

        if (!empty($obj->product_features))
            $data['product_features'] = json_decode($obj->product_features, true);

        if (!empty($obj->model))
            $data['model'] = $obj->model;

        if (!empty($obj->status))
            $data['status'] = $obj->status;

        if (!empty($obj->brand))
            $data['brand'] = $obj->brand;

        if (!empty($obj->product_categories))
            $data['product_categories'] = json_decode($obj->product_categories, true);

        if (!empty($obj->product_descriptions))
            $data['product_descriptions'] = json_decode($obj->product_descriptions, true);

        if (!empty($obj->common_descriptions))
            $data['common_descriptions'] = json_decode($obj->common_descriptions, true);

        if (!empty($obj->product_tags))
            $data['product_tags'] = json_decode($obj->product_tags, true);

        if (!empty($obj->nk_tragop_0))
            $data['nk_tragop_0'] = $obj->nk_tragop_0;

        if (!empty($obj->nk_special_tag))
            $data['nk_special_tag'] = $obj->nk_special_tag;
        
        if (!empty($obj->product_options))
            $data['product_options'] = json_decode($obj->product_options, true);

        if (!empty($obj->nk_uu_dai))
            $data['nk_uu_dai'] = $obj->nk_uu_dai;

        if (!empty($obj->nk_shortname))
            $data['nk_shortname'] = $obj->nk_shortname;
        
        return $data;
    }
}
