<?php

namespace App\Src\Product\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use League\Fractal\Manager;

use App\Models\ProductsCategories;
use App\Models\Categories;
use App\Models\CategoryDescriptions;
use App\Models\ProductOptions;
use App\Models\ProductOptionsDescriptions;
use App\Models\ProductFeaturesValues;
use App\Models\ProductFeatureVariantDescriptions;

class DetailProductTransformer extends TransformerAbstract
{
    public function transform($product)
    {
        $response = [
            'product_id' => (int)$product->product_id,
            'product_code' => $product->product_code,
            'product_name' => $product->product_name,
            'display_name' => $product->display_name,
            'price' => (float)($product->price),
            'list_price' => (float) ($product->list_price),
            'url_name' => $product->seo_names,
            'categories' => json_decode($product->product_categories, true),
            'model' => $product->model,
            'brand' => $product->brand,
        ];

        $collection = collect($response);
        return $collection->all();
    }

}
