<?php

namespace App\Src\Product\Transformers;

use League\Fractal\TransformerAbstract;

class ProductCategoryTransformer extends TransformerAbstract
{
    public function transform($category)
    {
        return [
            'category_id' => (int) $category['category_id'],
            'category_name' => $category['category'],
            'id_path' => $category['id_path'],
            'level' => $category['level'],
        ];
    }
}
