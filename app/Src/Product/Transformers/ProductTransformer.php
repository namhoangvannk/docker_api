<?php

namespace App\Src\Product\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use League\Fractal\Manager;

use App\Models\ProductsCategories;
use App\Models\Categories;
use App\Models\CategoryDescriptions;
use App\Models\ProductOptions;
use App\Models\ProductOptionsDescriptions;
use App\Models\ProductFeaturesValues;
use App\Models\ProductFeatureVariantDescriptions;

class ProductTransformer extends TransformerAbstract
{
    protected $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function transform($product)
    {
//        $seo_name = json_decode($product->seo_names, true);
        $option_decode = json_decode($product->product_options, true);

        $options = [];
        foreach ($option_decode as $key => $value) {
            if(isset($value['variants']))
                $value['variants'] = json_decode($value['variants'], true);
            
            $options[] = $value;
        }

        $response = [
            'product_id' => (int)$product->product_id,
            'product_code' => $product->product_code,
            'product_name' => $product->product_name,
            'display_name' => $product->display_name,
            'price' => (float)($product->price),
            'list_price' => (float) ($product->list_price),
            'images' => json_decode($product->images, true),
//            'url_name' => isset($seo_name['name']) ? $seo_name['name'] : '' ,
            'url_name' => $product->seo_names ,
            'categories' => json_decode($product->product_categories, true),
            'options' => $options,
            'product_features' => json_decode($product->product_features, true),
            'product_descriptions' => json_decode($product->product_descriptions, true)
        ];

        $collection = collect($response);
        return $collection->all();
    }

    public function includeCategories($product_id)
    {
        $product_categories = ProductsCategories::join(
            'cscart_products', 
            'cscart_products.product_id', 
            '=',
            'cscart_products_categories.product_id')
            ->join(
            'cscart_categories',
            'cscart_categories.category_id',
            '=',
            'cscart_products_categories.category_id')
            ->where('cscart_products.product_id', $product_id)
            ->select('cscart_products_categories.category_id')
            ->get();

        $categoryIds = [];
        foreach ($product_categories as $key => $item) {
            array_push($categoryIds, $item->category_id);
        }

        $category_desc = CategoryDescriptions::join(
            'cscart_categories',
            'cscart_categories.category_id','=',
            'cscart_category_descriptions.category_id'
        )
            ->whereIn('cscart_category_descriptions.category_id', $categoryIds)
            ->where('cscart_category_descriptions.lang_code', '=', 'vi')
            ->select(
                'cscart_category_descriptions.category_id',
                'cscart_category_descriptions.category',
                'cscart_categories.id_path',
                'cscart_categories.level'
            )
            ->get();

        $resource = $this->collection($category_desc, new ProductCategoryTransformer, false);
        $datas = $this->manager->createData($resource)->toArray();
        return $datas['data'];
    }

    public function includeOptions($product_id) 
    {
        $product_options = ProductOptions::join(
            'cscart_product_global_option_links',
            'cscart_product_options.option_id',
            '=',
            'cscart_product_global_option_links.option_id'
            )
            ->where('cscart_product_options.product_id', $product_id)
            ->where('cscart_product_options.status', 'A')
            ->get();

        $optionIds = [];
        foreach ($product_options as $key => $item) {
            array_push($optionIds, $item->option_id);
        }

        $option_desc = ProductOptionsDescriptions::whereIn('option_id', $optionIds)
            ->where('lang_code', '=', 'vi')->get();

        $resource = $this->collection($option_desc, new ProductOptionTransformer, false);
        $datas = $this->manager->createData($resource)->toArray();
        return $datas['data'];
    }

    public function includeModelBrand($product_id, $feature_id)
    {
        $feature_value = ProductFeaturesValues::join(
            'cscart_product_features',
            'cscart_product_features.feature_id','=',
            'cscart_product_features_values.feature_id'
            )
            ->where('cscart_product_features_values.product_id', $product_id)
            ->where('cscart_product_features_values.lang_code', 'vi')
            ->where('cscart_product_features.status', 'A')
            ->where('cscart_product_features_values.feature_id', $feature_id)->limit(1)->offset(0)
            ->first();

        $option_desc = ProductFeatureVariantDescriptions::where('variant_id', $feature_value->variant_id)
            ->first();
        
        return $option_desc->variant;
    }

}
