<?php

namespace App\Src\RefundLog\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Query\Query;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\Helpers\QRCodePartner\QRCodePartner;
use App\Helpers\QRCode\QRCode;

use App\Models\Mongo\RefundLog;

use Log;

class RefundLogRepository extends RepositoryBase
{
    public function __construct()
    {

    }

    public function index($request, $id)
    {
        try {
            $data = [];

            if ( !empty($id)) {
                $actionData = RefundLog::where('refund_id', (string)$id)->orderBy('action_time','desc')->get();

                foreach ($actionData as $val) {
                    $data[$val->_id] = [
                        'refund_id'          => $val->refund_id ? $val->refund_id : '',
                        'action_type'        => $val->action_type ? $val->action_type : '',
                        'user_id'            => $val->user_id ? $val->user_id : '',
                        'action_amount'      => $val->action_amount ? $val->action_amount : '',
                        'action_time'        => $val->action_time ? $val->action_time : '',
                        'action_description' => $val->action_description ? $val->action_description : '',
                    ];
                }
            }

            return [
                'code' => 'OK',
                'data' => json_encode($data),
            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }


    }

    public function create($request)
    {
        try {

            $date              = date('Y-m-d H:i:s');
            $refundId          = $request->input('id', '');
            $actionType        = $request->input('status', '');
            $userID            = $request->input('user_id', '');
            $actionAmount      = $request->input('amount', '');
            $actionTime        = $date;
            $actionDescription = $request->input('description', '');

            $data = [
                'refund_id'          => (string)$refundId,
                'action_type'        => $actionType,
                'user_id'            => $userID,
                'action_amount'      => $actionAmount,
                'action_time'        => $actionTime,
                'action_description' => $actionDescription,
            ];

            RefundLog::insert($data);
            return [
                'code'        => 'OK',
                'description' => 'Insert success',
            ];
        } catch (\Exception $e) {
            $data['error_message'] = $e->getMessage();
            Log::info('RefundLogRepository ' . var_export($data, true));
            //throw new \Exception($e->getMessage());
        }
    }

}
