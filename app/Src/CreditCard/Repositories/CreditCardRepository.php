<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 28/12/2017
 * Time: 10:24 AM
 */

namespace App\Src\CreditCard\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Helpers\Query\Query;
use App\Src\CreditCard\Transformers\CreditCardTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Helpers\Helpers;
use App\Models\NkCreditCardRegis;

class CreditCardRepository extends RepositoryBase
{
    protected $fractal;
    protected $collection;
    protected $transformer;
    protected $model;
    protected $helper;

    public function __construct(Helpers $helper,Manager $fractal,CreditCardTransformer $transformer)
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new NkCreditCardRegis;
        $this->transformer = $transformer;

    }

    public function all($request)
    {
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 20);

        $model = $this->model;
        $resource = new Collection($model->all(),$this->transformer);
        $data = $this->fractal->createData($resource)->toArray();

        $paging = new PagingComponent($model->count(), $offset, $limit);
        $data['paging'] = $paging->render();
        return $data;
    }

    public function create($request)
    {
        $params = $request->all();
        $model = $this->model;
        try {
            $model->fill($params);
            $model->save();
            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function update($request, $register_id)
    {
        $params = $request->all();
        try {
            $cc = NkCreditCardRegis::find($register_id);

            if ($cc == null)
                abort(400, 'Thông tin đăng ký không tồn tại');

            $cc->fill($params);
            $cc->update();

            return $this->fractal->createData(new Item($cc, $this->transformer))->toArray();
        } catch (\Exception $e) {
            abort(400, $e->getMessage());
        }
        abort(500, 'Có lỗi xảy ra!');
        return [];
    }
}