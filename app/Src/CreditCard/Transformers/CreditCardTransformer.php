<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 28/12/2017
 * Time: 10:46 AM
 */

namespace App\Src\CreditCard\Transformers;


use League\Fractal\TransformerAbstract;

class CreditCardTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'RegisterID' => $obj->register_id,
            'CustomerName' => $obj->CustomerName,
            'Email' => $obj->Email,
            'Phone' => $obj->Phone,
            'UserId' => $obj->UserId,
            'PersonalId' => $obj->PersonalId,
            'LocationId' => $obj->LocationId,
            'IncomeLevel' => $obj->IncomeLevel,
            'ReceiveIncomeMethod' => $obj->ReceiveIncomeMethod,
            'Partner' => $obj->Partner,
            'Status' => $obj->status,
            'CreditCardType' => $obj->CreditCardType,
            'NKcardNumber' => $obj->NKcardNumber,
            'NKcustomerType' => $obj->NKcustomerType,
            'CouponRewardType' => $obj->CouponRewardType,
            'CouponRewardStatus' => $obj->CouponRewardStatus,
            'CouponCode' => $obj->CouponCode,
        ];
    }
}