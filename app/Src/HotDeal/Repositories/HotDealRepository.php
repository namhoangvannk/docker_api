<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 17/04/2018
 * Time: 11:59 AM
 **/

namespace App\Src\HotDeal\Repositories;

use App\Base\Repositories\RepositoryBase;
use Illuminate\Support\Facades\Validator;
use App\Models\Mongo\HotDeal;
use App\Models\ProductPrices;
use App\Models\ProductDescriptions;
use App\Models\OrderDetails;
use App\Models\Products;
use App\Models\Orders;
use Log;
use DB;

class HotDealRepository extends RepositoryBase {
    protected $model;
    protected $config;
    protected $status;

    public function __construct(){
        $this->model = new HotDeal();
        $this->status = [
            'W'=> Hotdeal::$Waitting,
            'R'=> Hotdeal::$Running,
            'C'=> Hotdeal::$Cancel,
            'E'=> Hotdeal::$End
        ];
        $this->URL_SYNC_ELASTIC_SEARCH = 'https://adm.nguyenkim.com/index.php?dispatch=cron_elastic.index_product&security_hash=NKequy537165hdgahgKAHDNdklahdjada&product_id=';
        $this->URL_SYNC_MONGO          = 'https://www.nguyenkim.com/index.php?dispatch=cron_sync_product2mongo&action=run_sync&pass=sync2mongo12345&list_products=';
    }

    /**
     * ## API LAY DANH SACH SAN PHAM GIA RE ONLINE ADMIN
     * @param $request
     * @return array
     **/
    public function all($request){
        $params = $request->all();

        $product = HotDeal::groupBy([
            'CampaignID',
            'Status'
        ]);

        if(!empty($params['keywords'])){
            $product = $product->orWhere('CampaignName','like', '%'.$params['keywords'].'%')
                ->orWhere('Product','like', '%'.$params['keywords'].'%');
        }
        if(!empty($params['ProductID'])){
            $product_ids = array_map('intval', explode(",", trim($params['ProductID'])));
            if(count($product_ids) >= 2 ) {
                $product = $product->whereIn('ProductID',$product_ids);
            } else {
                $product = $product->where('ProductID',intval($params['ProductID']));
            }
        }
        if(!empty($params['ProductCode'])){
            $arr_product_sku = explode(",", trim($params['ProductCode']));
            foreach ($arr_product_sku as $sku){
                $product = $product->orWhere('ProductCode','like', '%'.intval($sku).'-%');
            }
        }
        if(!empty($params['StartTime'])
            && !empty($params['EndTime'])
            && is_numeric($params['StartTime'])
            && is_numeric($params['EndTime'])
        ){
            $product = $product->where('StartTime','>=',intval($params['StartTime']))->where('EndTime','<=',intval($params['EndTime']));
        }
        if(!empty($params['Status'])){
            if($params['Status'] == 'all'){
                $product = $product->whereIn('Status',$this->status);
            } else {
                $product = $product->where('Status',$params['Status']);
            }
        }
        $product->orderBy('CampaignID','DESC');
        $records =  $product->get([
            'CampaignID',
            'CampaignName',
            'StartTime',
            'EndTime',
            'Status'
        ])->toArray();

        $records = array_map(function ($record){
            unset($record['_id']);
            $record['Total'] = HotDeal::where('CampaignID',$record['CampaignID'])->where('Status',$record['Status'])
                ->count();
            if($record['Status'] == $this->status['W']){
                $record['StatusDesc'] =  'Chờ Hoạt Động';
            }
            if($record['Status'] == $this->status['R']){
                $record['StatusDesc'] =  'Đang Hoạt Động';
            }
            if($record['Status'] == $this->status['C']){
                $record['StatusDesc'] =  'Hủy';
            }
            if($record['Status'] == $this->status['E']){
                $record['StatusDesc'] =  'Kết Thúc';
            }
            return $record;
        },$records);

        $return_records = [];
        if(!empty($records)){
            foreach ($records as $key=>$record){
                $return_records[$record['CampaignID']]['CampaignID']   = $record['CampaignID'];
                $return_records[$record['CampaignID']]['CampaignName'] = $record['CampaignName'];
                $return_records[$record['CampaignID']]['Groups'][]     = $record;
                end($return_records[$record['CampaignID']]['Groups']);
                $last_id =key($return_records[$record['CampaignID']]['Groups']);
                unset($return_records[$record['CampaignID']]['Groups'][$last_id]['CampaignID']);
                unset($return_records[$record['CampaignID']]['Groups'][$last_id]['CampaignName']);
            }
        }

        $total   = count($return_records);
        return array(
            'total' => $total,
            'data'  => $return_records
        );
    }

    /**
     * ## API TAO SAN PHAM GIA RE ONLINE
     * @param $request
     * @return record
     **/
    public function create($request){
        $model      = $this->model;
        $params     = $request->all();
        Log::info('CreateForHotdeal : ' . json_encode($params));
        $validator  = Validator::make($params,[
            'CampaignID'   => ['required'],
            'ProductID'    => ['required'],
            'CampaignName' => ['required','string'],
            'StartTime'    => ['required','integer'],
            'EndTime'      => ['required','integer'],
            'Status'       => ['required','string'],
            'PromoPrice'   => ['required','integer'],
            'StockLimit'   => ['required','integer']
        ]);
        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()){
                $this->validateMoreForHotdeal($validator);
            }
        });
        if($validator->fails()){
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }
        try{
            $product_code          = Products::where('product_id',$params['ProductID'])->get(['product_code'])->first();
            $product_name          = ProductDescriptions::where('product_id',intval($params['ProductID']))->where('lang_code','vi')->get(['product'])->first();
            $params['Product']     = $product_name['product'];
            $params['ProductCode'] = $product_code['product_code'];
            $result                = $model->create($params);
            Log::info('CreateForHotdeal : ' . json_encode($result));
            return [
                'status'  => true,
                'message' => 'success',
                'data' => [
                    'hotdeal' => $result
                ]
            ];
        } catch (\Exception $e){
            return [
                'status'  => false,
                'errors'  => true,
                'message' => 500
            ];
        }
    }
    /**
     * ## API cập nhật hot deal
     * @param $request
     * @param $product_id
     * @return array
     **/
    public function update($request,$mid){
        $params    = $request->all();
        Log::info('UpdateForHotdeal: ' .json_encode($params));
        $validator = Validator::make($params,[
            'CampaignID'    => ['required'],
            'ProductID'     => ['required'],
            'CampaignName'  => ['required','string'],
            'StartTime'     => ['required','integer'],
            'EndTime'       => ['required','integer'],
            'Status'        => ['required','string'],
            'PromoPrice'    => ['required','integer'],
            'StockLimit'    => ['required','integer']
        ]);
        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()){
                $this->validateMoreForHotdeal($validator);
            }
        });
        if($validator->fails()){
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }
        try{
            $hotdeal = HotDeal::where('_id',$mid)->first();
            /// TODO: neu cap nhat trang thai ket thuc
            /// TODO: revert gia lai
            if($params['Status'] == $this->status['E']){
                HotDeal::where('_id',$hotdeal['_id'])
                    ->where('CampaignID',$hotdeal['CampaignID'])
                    ->where('ProductID', $hotdeal['ProductID'])
                    ->update([
                        'EndTime'       => time(),
                        'Status'        => $this->status['E']
                    ]);

                ProductPrices::where('product_id',$hotdeal['ProductID'])->update([
                    'price'=>$hotdeal['SalePriceBefore']
                ]);
                Products::where('product_id',$hotdeal['ProductID'])->update([
                    'offline_price'     => $hotdeal['OfflinePriceBefore'],
                    'nk_is_shock_price' => $hotdeal['SpecialPriceStatusBefore'],
                    'text_shock'        => '',
                    'shock_online_exp'  => ''
                ]);
            } elseif($params['Status'] == $this->status['C']){
                HotDeal::where('_id',$hotdeal['_id'])
                    ->where('CampaignID',$hotdeal['CampaignID'])
                    ->where('ProductID', $hotdeal['ProductID'])
                    ->update([
                        'EndTime'       => time(),
                        'Status'        => $this->status['E']
                    ]);
            } else {
                $hotdeal->fill([
                    "CampaignID"    => intval($params['CampaignID']),
                    "ProductID"     => intval($params['ProductID']),
                    "ProductCode"   => $params['ProductCode'],
                    "CampaignName"  => $params['CampaignName'],
                    "Product"       => $params['Product'],
                    "StartTime"     => intval($params['StartTime']),
                    "EndTime"       => intval($params['EndTime']),
                    "Status"        => $params['Status'],
                    "PromoPrice"    => intval($params['PromoPrice']),
                    "StockLimit"    => intval($params['StockLimit']),
                    "UpdatedTime"   => $params['UpdatedTime'],
                    "UpdatedBy"     => $params['UpdatedBy']
                ]);
                $hotdeal->save();
                Log::info('UpdateForHotdeal : ' . json_encode($hotdeal));
            }

            return array(
                'status'  => true,
                'message' => "success"
            );

        }catch (\Exception $e){
            return [
                'status'  => false,
                'errors'  => 'lỗi API 500',
                'message' => 500
            ];
        }
    }



    /**
     *  ## CRON CHAY GIA RE ONLINE
     *  Step 1. Kiểm tra các cài đặt giá sốc active và đến giờ bắt đầu,
     *  => chuyển trạng thái running
     *  Step 2. Update giá sốc online vào tất cả các sán phẩm được cài đặt
     *  Step 3. Kiểm tra các cài đặt giá sốc running và hết giờ hoặc hết stock,
     *  => chuyển trạng thái end
     *  Step 4. Update lại giá thông thường vào các sản phẩm vừa hết giờ
     *  Step 5. Dong bo data qua bo search, san pham mongo
     *
     *  @param $request
     *  @return array status
     **/
    public function cronShockPriceOnline($request){
        $todate = time();
        $this->printMessage('CRON GIA RE ONLINE '. date('d-m-Y H:i:s') .' ---- '.$todate);
        $this->printMessage('Starting..');
        Log::info('CRON GIA RE ONLINE - ' . date('d-m-Y H:i:s'));
        Log::info('Starting..');
        /// **********************************************************
        /// TODO - SAN PHAM END => CAP NHAT TRANG THAI KET THUC
        /// **********************************************************
        $hotdeals_e = HotDeal::where('EndTime','<',$todate)
            ->where('Status','!=',$this->status['E'])
            ->take(60)
            ->get()->toArray();
        if(!empty($hotdeals_e)) {
            foreach ($hotdeals_e as $hotdeal) {
                // update hotdeal status
                HotDeal::where('_id', $hotdeal['_id'])
                    ->where('CampaignID', $hotdeal['CampaignID'])
                    ->where('ProductID', $hotdeal['ProductID'])
                    ->where('StartTime', $hotdeal['StartTime'])
                    ->where('EndTime', $hotdeal['EndTime'])
                    ->update([
                        'Status'  => $this->status['E'],
                        'EndTime' => $todate
                    ]);
                // update product price
                if($hotdeal['Status'] == $this->status['R']){
                    $offline_price_revert = Products::where('product_id', $hotdeal['ProductID'])
                        ->where('nk_is_shock_price','=','O')
                        ->get(['offline_price'])->first();
                    $arr_price_update = [];
                    if(!empty($offline_price_revert['offline_price']) && $offline_price_revert['offline_price'] > 0){
                        $arr_price_update['price'] = $offline_price_revert['offline_price'];
                    } else {
                        $arr_price_update['price'] = $hotdeal['SalePriceBefore'];
                    }
                    if(!empty($arr_price_update)){
                        Log::info('GIAREONLINE_CRON_PRODUCT_'.$hotdeal['ProductID'].'_end_revert_update_price_cron_e'.json_encode($arr_price_update));
                        ProductPrices::where('product_id', $hotdeal['ProductID'])->update($arr_price_update);
                    }
                    // create array log
                    $log_end_revert = [
                        'offline_price'     => $hotdeal['OfflinePriceBefore'],
                        'list_price'        => $hotdeal['ListPriceBefore'],
                        'nk_is_shock_price' => $hotdeal['SpecialPriceStatusBefore'],
                        'text_shock'        => '',
                        'shock_online_exp'  => ''
                    ];
                    // log something
                    Log::info('GIAREONLINE_CRON_PRODUCT_'.$hotdeal['ProductID'].' end revert '.json_encode($log_end_revert));
                    // update for product
                    Products::where('product_id', $hotdeal['ProductID'])->update($log_end_revert);
                    // log price
                    $log_end_revert['price'] = $hotdeal['SalePriceBefore'];
                    Log::info('GIAREONLINE_CRON_PRODUCT'.$hotdeal['ProductID'].' end revert '.json_encode($log_end_revert));
                    $this->printMessage('Product ' . $hotdeal['ProductID'] . ' end revert '. json_encode($log_end_revert));
                }
                $this->printMessage('Product ' . $hotdeal['ProductID'].' Status : '. $hotdeal['Status'] . 'update status end');
                // Sync Mongo, ElasticSearch
                $this->printMessage('Synced '. $hotdeal['ProductID'] .' product to Elastic Search');
                $this->syncProduct2ElasticSearch($hotdeal['ProductID']);
                $this->printMessage('Synced '. $hotdeal['ProductID'] .' product to Mongo');
                $this->syncProduct2Mongo($hotdeal['ProductID']);
            }
        }

        /// **********************************************************
        /// TODO - SAN PHAM THOA DIEU KIEN CHAY GIA RE
        /// **********************************************************
        $hotdeals = HotDeal::where('StartTime','<=',$todate)
            ->where('EndTime','>',$todate)
            ->where('Status',$this->status['W'])
            ->take(60)
            ->get()->toArray();
        if(!empty($hotdeals)) {
            foreach ($hotdeals as $hotdeal) {
                /*
                // code cu
                // get data product backup
                //$product_bk = Products::where('product_id', $hotdeal['ProductID'])
                //    ->where('nk_is_shock_price', '!=', 'O')
                //    ->get()->first();
                // get price
                //$price = ProductPrices::where('product_id', $hotdeal['ProductID'])->get()->first();
                */

                /// FIXME : GROL CHAY DUPLICATE - SP DA CHAY GIA RE KHONG QUET LAI NUA
                $pro_data = DB::select("SELECT t1.list_price,t1.offline_price,t1.create_user_name,t1.update_user_name,t1.nk_is_shock_price,t2.price FROM cscart_products t1 JOIN cscart_product_prices t2 ON t1.product_id = t2.product_id WHERE t1.product_id = {$hotdeal['ProductID']} AND t1.nk_is_shock_price != 'O'");
                if(empty($pro_data)){
                    continue;
                }
                $pro_data_bk = json_decode(json_encode($pro_data),true)[0];
                if(empty($pro_data_bk)){
                    continue;
                }

                // log something
                Log::info('GIAREONLINE_CRON_'.'BackupPriceForHotdeal_ProductID_' . $hotdeal['ProductID'] . '_Price_' . $pro_data_bk['price']);
                // update product backup to hotdeal
                $store = HotDeal::where('_id', $hotdeal['_id'])
                    ->where('CampaignID', $hotdeal['CampaignID'])
                    ->where('ProductID', $hotdeal['ProductID'])
                    ->where('StartTime', $hotdeal['StartTime'])
                    ->where('EndTime', $hotdeal['EndTime'])
                    ->update([
                        'Status'                   => $this->status['R'],
                        'ListPriceBefore'          => $pro_data_bk['list_price'],
                        'SalePriceBefore'          => $pro_data_bk['price'],
                        'CreatedBy'                => $pro_data_bk['create_user_name'],
                        'UpdatedTime'              => date('d-m-Y H:i:s'),
                        'UpdatedBy'                => $pro_data_bk['update_user_name'],
                        'SpecialPriceStatusBefore' => $pro_data_bk['nk_is_shock_price'],
                        //  'TextShockBefore'=>$product_bk['text_shock'],
                    ]);
                if (count($store) > 0 && !empty($pro_data_bk['price'])) {
                    // update hotdeal to product
                    DB::table('cscart_products')->where('product_id', $hotdeal['ProductID'])
                        ->where('nk_is_shock_price','!=','O')
                        ->update([
                        'offline_price'     => $pro_data_bk['price'],
                        'nk_is_shock_price' => 'O',
                        'text_shock'        => 'Giá rẻ online',
                        'shock_online_exp'  => date('d/m', $hotdeal['EndTime'])
                    ]);
                    HotDeal::where('_id', $hotdeal['_id'])->update(['OfflinePriceBefore' => $pro_data_bk['price']]);
                    // log something
                    Log::info('GIAREONLINE_CRON_'.'Log_Update_ProductID_' . $hotdeal['ProductID'] . ' : ' . json_encode([
                        'offline_price'     => $pro_data_bk['price'],
                        'nk_is_shock_price' => 'O',
                        'text_shock'        => 'Giá rẻ online',
                        'shock_online_exp'  => date('d/m', $hotdeal['EndTime'])
                    ]));
                    // update price
                    DB::table('cscart_product_prices')->where('product_id', $hotdeal['ProductID'])->update([
                        'price' => $hotdeal['PromoPrice']
                    ]);
                    // log something more
                    Log::info('GIAREONLINE_CRON_'.'Log_Update_ProductID_' . $hotdeal['ProductID']. json_encode(['price' => $hotdeal['PromoPrice']]));
                    Log::info('GIAREONLINE_CRON_'.'Log_Update_ProductID_'.$hotdeal['ProductID'].' waiting backup update');
                    $this->printMessage('Product '.$hotdeal['ProductID'] . ' waiting backup update');
                    // Sync Mongo, ElasticSearch
                    $this->printMessage('Synced '. $hotdeal['ProductID'] .' product to Elastic Search');
                    $this->syncProduct2ElasticSearch($hotdeal['ProductID']);
                    $this->printMessage('Synced '. $hotdeal['ProductID'] .' product to Mongo');
                    $this->syncProduct2Mongo($hotdeal['ProductID']);
                }
            }
        }
        /// *************************************************************
        /// TODO - SAN PHAM CANCEL => CAP NHAT TRANG THAI KET THUC
        /// *************************************************************
        $hotdeals_status_cancel = HotDeal::where('StartTime','<=',$todate)
            ->where('EndTime','>=',$todate)
            ->where('Status',$this->status['C'])
            ->get()->toArray();
        if(!empty($hotdeals_status_cancel)) {
            foreach ($hotdeals_status_cancel as $hotdeal) {
                // update hotdeal status end
                HotDeal::where('_id', $hotdeal['_id'])
                    ->where('CampaignID', $hotdeal['CampaignID'])
                    ->where('ProductID', $hotdeal['ProductID'])
                    ->where('StartTime', $hotdeal['StartTime'])
                    ->where('EndTime', $hotdeal['EndTime'])
                    ->update([
                        'Status'  => $this->status['E'],
                        'EndTime' => time()
                    ]);
                // log something
                Log::info('GIAREONLINE_CRON_'.'RevertForHotdeal'. $hotdeal['ProductID']. ' Status '.$this->status['C'] .'---'. date('d-m-Y H:i:s'));
                $this->printMessage('Product '.$hotdeal['ProductID'].' Status '.$this->status['C'].'end revert');
                // Sync Mongo, ElasticSearch
                $this->printMessage('Synced '. $hotdeal['ProductID'] .' product to Elastic Search');
                $this->syncProduct2ElasticSearch($hotdeal['ProductID']);
                $this->printMessage('Synced '. $hotdeal['ProductID'] .' product to Mongo');
                $this->syncProduct2Mongo($hotdeal['ProductID']);
            }
        }
        Log::info('CRON Done!');
        $this->printMessage('CRON Done!');
        exit();
    }

    /**
     * ## API LAY SAN PHAM GIA RE ONLINE THEO CAMPAIGN_ID ADMIN
     * @param $request
     * @return array
     */
    public function getHotDealById($request,$id){
        try{
            $products = HotDeal::where('CampaignID',intval($id))
                ->orderBy('StartTime','DESC')
                ->orderBy('EndTime','DESC')
                ->get()
                ->toArray();
            foreach ($products as &$product){
                $product_name       = ProductDescriptions::where('product_id',intval($product['ProductID']))->where('lang_code','vi')->get(['product'])->first();
                $product['Product'] = $product_name['product'];
                if(empty($product['ProductCode'])){
                    $product_code = Products::where('product_id',intval($product['ProductID']))->get(['product_code'])->first();
                    if(!empty($product_code)){
                        HotDeal::where('_id', $product['_id'])
                            ->update([
                                'ProductCode' => $product_code['product_code'],
                            ]);
                        $product['ProductCode'] = $product_code['product_code'];
                    } else {
                        $product['ProductCode'] = '';
                    }
                }
                continue;
            }
            return array(
                'status' => true,
                'message' => 'success',
                'data' => $products
            );
        } catch (\Exception $e){
            return [
                'status' => false,
                'errors' => true,
                'message' => 500
            ];
        }
    }

    /*
     * ## KIEM TRA THOI GIAN BAT DAU / KET THUC KHI UPDATE
     * @params Resquest
     * @return array status 0,1,2
     */
    public function checkIsDuplicateTime($product){
        $query = HotDeal::where('ProductID',$product['ProductID']);
        $query->whereIn('Status',[$this->status['W'],$this->status['R']]);
        $query->where('EndTime','>=',$product['StartTime']);
        $query->orderBy('StartTime','DESC');
        $arr_productIsAvtive = $query->get()->toArray();
        $msg = '';
        if(!empty($arr_productIsAvtive)){
            foreach ($arr_productIsAvtive as $hotdeal){
                if(!( $product['StartTime'] > $hotdeal['EndTime']
                    || $product['EndTime'] < $hotdeal['StartTime'])){
                    $msg .= 'Thời gian bị trùng với sản phẩm '.$hotdeal['ProductID'].'- CP : '.$hotdeal['CampaignID'].'| S: ';
                    $msg .= date("d-m-Y H:i:s", $hotdeal['StartTime']).'- E : ';
                    $msg .= date("d-m-Y H:i:s", $hotdeal['EndTime']).'- TT : ';
                    $msg .= $hotdeal['Status'];

                    break;
                }
            }
        }
        return $msg;
    }
    /*
     * ## IMPORT SAN PHAM GIA RE ONLINE
     * @params Resquest
     * @return array
     */
    public function import($request){
        $params = $request->all();
        $arr_product_valid   = [];
        $arr_product_invalid = [];
        $create_time         = date("Y-m-d H:i:s");
        $campaign_id         = time();
        if(!empty($params['products'])){
            foreach ($params['products'] as $product){
                $product['campaign_id'] = $campaign_id;
                $product['create_time'] = $create_time;
                $hotdeal = $this->createArrayHotdealProduct($product);
                if($this->checkProductValidImport($hotdeal) == 'OK'){
                    $product_code           = Products::where('product_id',$hotdeal['ProductID'])->get(['product_code'])->first();
                    $hotdeal['ProductCode'] = $product_code['$product_code'];
                    $product_name           = ProductDescriptions::where('product_id',intval($hotdeal['ProductID']))->where('lang_code','vi')->get(['product'])->first();
                    $hotdeal['Product']     = $product_name['product'];
                    array_push($arr_product_valid,$hotdeal);
                } else {
                    $product['error'] = $this->checkProductValidImport($hotdeal);
                    array_push($arr_product_invalid,$product);
                }
            }
            if(count($arr_product_valid) > 0 && count($arr_product_invalid) == 0){
                HotDeal::insert($arr_product_valid);
            } else {
                $arr_product_valid = [];
            }
            return [
                'status'    => true,
                'message'   => 'success',
                'data'      => [
                    'products'  => $arr_product_valid,
                    'errors'    => $arr_product_invalid,
                    'message'   => []
                ]
            ];
        }
        return [
            'status'    => false,
            'message'   => 'errors',
            'data'      => [
                'products'  => [],
                'errors'    => [],
                'message'   => []
            ]
        ];
    }

    /*
     * IMPORT TRANG THAI
     * @params : array
     * @return : array status
     *
     * */
    public function importStatus($request){
        $params = $request->all();
        $arr_product_valid = [];
        $arr_product_invalid = [];
        if(!empty($params['products'])){
            foreach ($params['products'] as $product){
                // get old data status ready to update
                $hotdeal = HotDeal::where('CampaignID',intval($product['campaign_id']))
                    ->where('ProductID',intval($product['product_id']))
                    ->get()->first();
                // if running can be update status end
                if(!empty($hotdeal) && $hotdeal['Status'] == $this->status['R'] && $product['status'] == $this->status['E']){
                    // update hotdeal end
                    HotDeal::where('_id',$hotdeal['_id'])
                        ->update([
                            'Status'=>$this->status['E']
                        ]);
                    // update revert product price
                    $offline_price_revert = Products::where('product_id', $hotdeal['ProductID'])
                        ->where('nk_is_shock_price','=','O')
                        ->get(['offline_price'])->first();
                    $arr_price_update = [];
                    if(!empty($offline_price_revert['offline_price']) && $offline_price_revert['offline_price'] > 0){
                        $arr_price_update['price'] = $offline_price_revert['offline_price'];
                    } else {
                        $arr_price_update['price'] = $hotdeal['SalePriceBefore'];
                    }
                    if(!empty($arr_price_update)){
                        ProductPrices::where('product_id', $hotdeal['ProductID'])->update($arr_price_update);
                    }

                    Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].'import status end revert price '.json_encode($arr_price_update));
                    // update revert product
                    Products::where('product_id',$hotdeal['ProductID'])->update([
                        'offline_price'     => $hotdeal['OfflinePriceBefore'],
                        'nk_is_shock_price' => $hotdeal['SpecialPriceStatusBefore'],
                        'text_shock'        => '',
                        'shock_online_exp'  => ''
                    ]);
                    Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].'import status end revert product '.json_encode([
                        'offline_price'     => $hotdeal['OfflinePriceBefore'],
                        'nk_is_shock_price' => $hotdeal['SpecialPriceStatusBefore'],
                        'text_shock'        => '',
                        'shock_online_exp'  => ''
                    ]));
                    // sync data product to mongo, elastic search
                    $this->syncProduct2ElasticSearch($hotdeal['ProductID']);
                    $this->syncProduct2Mongo($hotdeal['ProductID']);
                    array_push($arr_product_valid,$product);
                } else {
                    array_push($arr_product_invalid,$product);
                }
            }
            return [
                'status'  => true,
                'message' => 'success',
                'data'    =>[
                    'errors'  => $arr_product_invalid,
                    'product' => $arr_product_valid
                ]
            ];
        }
        return [
            'status'  => false,
            'message' => 'errors',
            'data'    =>[
                'errors'  => [],
                'product' => []
            ]
        ];
    }

    // KIEM TRA REQUEST DATA CUA SAN PHAM KHI IMPORT
    public function checkProductValidImport($product){
        // kiem tra kieu du lieu cac cot
        foreach ($product as $key=>$value){
            if(in_array($key,['ProductID','Product','StartTime','EndTime','PromoPrice']) && (empty($value)|| is_null($value))){
                if($key == 'ProductID'){
                    return '#E012 | Không tồn tại SP '.$product['ProductID'];
                }
                return '#E011 | SP '.$product['ProductID'].' | empty '.$key;
            }
        }
        $checkProductExits = Products::find(intval($product['ProductID']));
        if(empty($checkProductExits)){
            return '#E013 | Không tồn tại SP '.$product['ProductID'];
        }
        if(!is_int($product['StartTime'])
            || strpos(date("H:i:s d-m-Y", $product['StartTime']), '-1970') !== false
        ){
            return '#E010 | SP '.$product['ProductID'].' | Thời gian bắt đầu không đúng định dạng.| '.date("H:i:s d-m-Y",$product['StartTime']);
        }
        if(!is_int($product['EndTime'])
            || strpos(date("H:i:s d-m-Y", $product['EndTime']), '-1970') !== false
        ){
            return '#E011 | SP '.$product['ProductID'].' | Thời gian kết thúc không đúng định dạng.| '.date("H:i:s d-m-Y",$product['EndTime']);
        }
        if($product['StartTime'] > $product['EndTime']){
            return '#E001 | SP '.$product['ProductID'].' | Thời gian bắt đầu > Thời gian kết thúc! | HT '.date("H:i:s d-m-Y", $product['StartTime']) .' > '.date("H:i:s d-m-Y", $product['EndTime']);
        }
        $isDuplicate = $this->checkIsDuplicateTime($product); // validate when Insert
        if(!empty($isDuplicate)){
            return '#E002 | SP '.$product['ProductID'].' '. $isDuplicate;
        }else{
            // validate start_time voi end_time
            if($product['StartTime'] == $product['EndTime']){
                return '#E004 | SP '.$product['ProductID'].' | Thời gian bắt đầu trùng với Thời gian kết thúc!| HT '.date("H:i:s d-m-Y", $product['StartTime']) .' = '.date("H:i:s d-m-Y", $product['EndTime']);
            }
            // validate end_time voi start_time
            if($product['EndTime'] < $product['StartTime']){
                return '#E005 | SP '.$product['ProductID'].' | Thời gian kết thúc < thời gian bắt đầu kiểm tra lại thời gian! | HT '.date("H:i:s d-m-Y", $product['StartTime']) .' < '.date("H:i:s d-m-Y", $product['EndTime']);
            }
            // success validate giá
            // so sanh gia re online voi gia ban da apply gia re
            $checkprice = DB::select("SELECT t1.product_id,t1.list_price,t2.price,t1.nk_is_shock_price FROM cscart_products t1 JOIN cscart_product_prices t2 ON t1.product_id = t2.product_id WHERE t1.product_id = {$product['ProductID']} AND t1.nk_is_shock_price = 'O'");
            if(!empty($checkprice)){
                $checkprice = json_decode(json_encode($checkprice),true)[0];
                // so sanh gia re online voi gia niem yet
                if($product['PromoPrice'] > $checkprice['list_price']){
                    return '#E008 | SP '.$product['ProductID'].' | Giá online > Giá niêm yết - PromoPrice '.$product['PromoPrice'].' > list_price '.$checkprice['list_price'];
                }
                if($product['PromoPrice'] == $checkprice['list_price']){
                    return '#E009 | SP '.$product['ProductID'].' | Giá online = Giá niêm yết -- PromoPrice '.$product['PromoPrice'].' = list_price '.$checkprice['list_price'];
                }
            } else {
                $price = ProductPrices::where('product_id',$product['ProductID'])->get(['price'])->first();
                $list_price = Products::where('product_id',$product['ProductID'])->get(['list_price'])->first();
                if($product['PromoPrice'] > $price['price']){
                    return '#E006 | SP '.$product['ProductID'].' | Giá online > giá bán - PromoPrice '.$product['PromoPrice'].' > price '.$price['price'];
                }
                // so sanh gia re online voi gia niem yet
                if($product['PromoPrice'] > $list_price['list_price']){
                    return '#E008 | SP '.$product['ProductID'].' | Giá online > Giá niêm yết - PromoPrice '.$product['PromoPrice'].' > list_price '.$list_price['list_price'];
                }
                if($product['PromoPrice'] == $price['price']){
                    return '#E007 | SP '.$product['ProductID'].' | Giá online = Giá bán - PromoPrice '.$product['PromoPrice'].' = price '.$price['price'];
                }
                if($product['PromoPrice'] == $list_price['list_price']){
                    return '#E009 | SP '.$product['ProductID'].' | Giá online = Giá niêm yết -- PromoPrice '.$product['PromoPrice'].' = list_price '.$list_price['list_price'];
                }
            }
            return 'OK';
        }
    }

    // DONG BO DATA SAN PHAM QUA BO SEARCH
    public function syncProduct2ElasticSearch($product_ids){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->URL_SYNC_ELASTIC_SEARCH.$product_ids,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
             Log::info('GIAREONLINE_PRODUCT_'.$product_ids.' syncProduct2ElasticSearch Error : '.json_encode($err));
        }
    }

    // DONG BO DATA SAN PHAM DA XEM, DANH RIENG CHO BAN
    public function syncProduct2Mongo($product_ids){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->URL_SYNC_MONGO . $product_ids,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            Log::info('GIAREONLINE_PRODUCT_'.$product_ids.' syncProduct2Mongo Error : '.json_encode($err));
        }
    }

    // CAP NHAT TRANG THAI THEO MID, MIDS
    public function updateStatusEnd($request){
        $params = $request->all();
        // update single mid
        if(!empty($params['mid'])){
            $hotdeal = HotDeal::where('_id',$params['mid'])->first();
            if(!empty($hotdeal)){
                if($params['status'] == $this->status['E'] && $hotdeal['Status'] == $this->status['R']){
                    // update hotdeal status end
                    $time = time();
                    HotDeal::where('_id',$hotdeal['_id'])
                        ->update([
                            'Status'    => $this->status['E'],
                            'EndTime'   => $time
                        ]);
                    // update revert product price
                    ProductPrices::where('product_id',$hotdeal['ProductID'])->update([
                        'price'=>$hotdeal['SalePriceBefore']
                    ]);
                    // log update status end revert price
                    Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status end revert price '.json_encode([
                        'price' => $hotdeal['SalePriceBefore']
                    ]));
//                  // update revert product
                    Products::where('product_id',$hotdeal['ProductID'])->update([
                        'offline_price'     => $hotdeal['OfflinePriceBefore'],
                        'nk_is_shock_price' =>$hotdeal['SpecialPriceStatusBefore'],
                        'text_shock'        =>'',
                        'shock_online_exp'  =>''
                    ]);
                    // log update status end revert product
                    Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status end revert product '.json_encode([
                        'offline_price'     => $hotdeal['OfflinePriceBefore'],
                        'nk_is_shock_price' =>$hotdeal['SpecialPriceStatusBefore'],
                        'text_shock'        =>'',
                        'shock_online_exp'  =>''
                    ]));
                    // sync product data to mongo, elastic search
                    $this->syncProduct2ElasticSearch($hotdeal['ProductID']);
                    $this->syncProduct2Mongo($hotdeal['ProductID']);
                    return array(
                        'status'    => true,
                        'message'   => "success",
                        'data'      => ['status_txt'=> 'Kết thúc']
                    );
                }
                if($params['status'] == $this->status['C'] && $hotdeal['Status'] == $this->status['W']){
                    // if status cancel can be status end
                    HotDeal::where('_id',$hotdeal['_id'])
                        ->update([
                            'Status'    => $this->status['E'],
                            'EndTime'   => time()
                        ]);
                    Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status cancel ');
                    return array(
                        'status'    => true,
                        'message'   => "success",
                        'data'      => ['status_txt'=> 'Hủy']
                    );
                }
            }
            return array(
                'status'    => false,
                'message'   => "error update status"
            );
        }
        // update multiple
        if(!empty($params['mids'])){
            // lấy mảng mid
            $mids = explode(',',$params['mids']);
            $hotdeals = HotDeal::whereIn('_id',$mids)->get([
                '_id',
                'ProductID',
                'Status',
                'SalePriceBefore',
                'OfflinePriceBefore',
                'SpecialPriceStatusBefore'
            ]);
            // tắt multiple
            if(!empty($hotdeals)){
                $data_return = array(
                    'status'    => true,
                    'message'   => "success",
                );
                foreach ($hotdeals as $hotdeal){
                    // compare old status and new status if true update status
                    if($params['status'] == $this->status['E'] && $hotdeal['Status'] == $this->status['R']){
                        HotDeal::where('_id',$hotdeal['_id'])->update([
                            'Status'  => $this->status['E'],
                            'EndTime' => time()
                        ]);
                        $offline_price_revert = Products::where('product_id', $hotdeal['ProductID'])
                            ->where('nk_is_shock_price','=','O')
                            ->get(['offline_price'])->first();
                        $arr_price_update = [];
                        if(!empty($offline_price_revert['offline_price']) && $offline_price_revert['offline_price'] > 0){
                            $arr_price_update['price'] = $offline_price_revert['offline_price'];
                        } else {
                            $arr_price_update['price'] = $hotdeal['SalePriceBefore'];
                        }
                        if(!empty($arr_price_update)){
                            ProductPrices::where('product_id', $hotdeal['ProductID'])->update($arr_price_update);
                        }

                        Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status end revert price '.json_encode($arr_price_update));
                        Products::where('product_id',$hotdeal['ProductID'])->update([
                            'offline_price'     => $hotdeal['OfflinePriceBefore'],
                            'nk_is_shock_price' => $hotdeal['SpecialPriceStatusBefore'],
                            'text_shock'        => '',
                            'shock_online_exp'  => ''
                        ]);
                        Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status end revert product '.json_encode([
                            'offline_price'     => $hotdeal['OfflinePriceBefore'],
                            'nk_is_shock_price' =>$hotdeal['SpecialPriceStatusBefore'],
                            'text_shock'        =>'',
                            'shock_online_exp'  =>''
                        ]));
                        $this->syncProduct2ElasticSearch($hotdeal['ProductID']);
                        $this->syncProduct2Mongo($hotdeal['ProductID']);
                        $data_return['data']['status_txt'] = 'Kết thúc';
                        continue;
                    } elseif ($params['status'] == $this->status['C'] && $hotdeal['Status'] == $this->status['W']){
                        HotDeal::where('_id',$hotdeal['_id'])->update([
                            'Status'    => $this->status['E'],
                            'EndTime'   => time()
                        ]);
                        Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status cancel ');
                        $data_return['data']['status_txt'] = 'Hủy';
                        continue;
                    }
                }
                return $data_return;
            }
            return array(
                'status'    => false,
                'message'   => "error"
            );
        }
        return array(
            'status'    => false,
            'message'   => "error"
        );
    }

    // CAP NHAT TRANG THAI THEO CAMPAIGN
    public function updateStatusEndCampaign($request){
        $params = $request->all();
        $time = time();
        if(!empty($params['cpid'])){
            $params['cpid'] = intval($params['cpid']);
            $hotdeals_sync = HotDeal::where('CampaignID',$params['cpid'])
                ->where('Status','!=',$this->status['E'])
                ->get([
                    '_id',
                    'ProductID',
                    'Status',
                    'SalePriceBefore',
                    'OfflinePriceBefore',
                    'SpecialPriceStatusBefore'
                ])->toArray();
            if(!empty($hotdeals_sync)) {
                $data_return = array(
                    'status'    => true,
                    'message'   => "success",
                );
                foreach ($hotdeals_sync as $hotdeal){
                    // compare old status and new status if true update status
                    if($params['status'] == $this->status['E'] && $hotdeal['Status'] == $this->status['R']){
                        HotDeal::where('_id',$hotdeal['_id'])->update([
                            'Status'  => $this->status['E'],
                            'EndTime' => $time
                        ]);
                        $offline_price_revert = Products::where('product_id', $hotdeal['ProductID'])
                            ->where('nk_is_shock_price','=','O')
                            ->get(['offline_price'])->first();
                        $arr_price_update = [];
                        if(!empty($offline_price_revert['offline_price']) && $offline_price_revert['offline_price'] > 0){
                            $arr_price_update['price'] = $offline_price_revert['offline_price'];
                        } else {
                            $arr_price_update['price'] = $hotdeal['SalePriceBefore'];
                        }
                        if(!empty($arr_price_update)){
                            ProductPrices::where('product_id', $hotdeal['ProductID'])->update($arr_price_update);
                        }

                        Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status end revert price '.json_encode($arr_price_update));
                        Products::where('product_id',$hotdeal['ProductID'])->update([
                            'offline_price'     => $hotdeal['OfflinePriceBefore'],
                            'nk_is_shock_price' => $hotdeal['SpecialPriceStatusBefore'],
                            'text_shock'        => '',
                            'shock_online_exp'  => ''
                        ]);
                        Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status end revert product '.json_encode([
                            'offline_price'     => $hotdeal['OfflinePriceBefore'],
                            'nk_is_shock_price' =>$hotdeal['SpecialPriceStatusBefore'],
                            'text_shock'        =>'',
                            'shock_online_exp'  =>''
                        ]));
                        $this->syncProduct2ElasticSearch($hotdeal['ProductID']);
                        $this->syncProduct2Mongo($hotdeal['ProductID']);
                        $data_return['data']['status_txt'] = 'Kết thúc';
                        continue;
                    } elseif ($params['status'] == $this->status['C'] && $hotdeal['Status'] == $this->status['W']){
                        HotDeal::where('_id',$hotdeal['_id'])->update([
                            'Status'    => $this->status['E'],
                            'EndTime'   => $time
                        ]);
                        Log::info('GIAREONLINE_PRODUCT_'.$hotdeal['ProductID'].' update status cancel ');
                        $data_return['data']['status_txt'] = 'Hủy';
                        continue;
                    }
                }
                return $data_return;
            }
            return array(
                'status'  => false,
                'message' => "error"
            );
        }
        return array(
            'status'  => false,
            'message' => "error"
        );
    }

    // XUAT DU LIEU DANH SACH CAC SAN PHAM GIA RE ONLINE DANG HOAT DONG
    public function exportHotdealRunning($request){
        $products = HotDeal::where('Status',$this->status['R'])->get()->toArray();
        if(empty($products)){
            return array(
                'status'  => true,
                'message' => "success",
                'data'    => []
            );
        }
        foreach ($products as &$product){
            $product_name           = ProductDescriptions::where('product_id',intval($product['ProductID']))->where('lang_code','vi')->get(['product'])->first();
            $product['Product']     = $product_name['product'];
        }
        return array(
            'status'  => true,
            'message' => "success",
            'data'    => $products
        );
    }

    // CREATE ARRAY PRODUCT HOTDEAL
    public function createArrayHotdealProduct($product){
        return array(
            "CampaignID"                => intval($product['campaign_id']),
            "ProductID"                 => intval($product['product_id']),
            "CampaignName"              => $product['campaign_name'],
            "Product"                   => $product['product_name'],
            "StartTime"                 => intval($product['start_time']),
            "EndTime"                   => intval($product['end_time']),
            "Status"                    => $this->status['W'],
            "PromoPrice"                => intval($product['price_online']),
            "StockLimit"                => intval($product['amount']),
            "ListPriceBefore"           => 0,
            "SalePriceBefore"           => 0,
            "OfflinePriceBefore"        => 0,
            "SpecialPriceStatusBefore"  => "",
            // "TextShockBefore" => "",
            "CreatedTime"               => $product['create_time'],
            "CreatedBy"                 => 'Import',
            "UpdatedTime"               => "",
            "UpdatedBy"                 => ""
        );
    }
    // VALIDATE MORE
    public function validateMoreForHotdeal($validator){
        $product = $validator->getData();
        if($product['StartTime'] > $product['EndTime']){
            $validator->errors()->add('E001', 'CampaignID='.$product['CampaignID'].' | ProductID='.$product['ProductID'].' | Thời gian bắt đầu không được lớn hơn Thời gian kết thúc!');
        }
        $isDuplicate = $this->checkIsDuplicateTime($product); // validate when Insert

        if(!empty($isDuplicate)){
            //loi dau start
            $validator->errors()->add('E002', $isDuplicate);
        }else{
            // validate start_time voi end_time
            if($product['StartTime'] == $product['EndTime']){
                $validator->errors()->add('E004', 'CampaignID='.$product['CampaignID'].' | ProductID='.$product['ProductID'].' | Thời gian bắt đầu trùng với Thời gian kết thúc!');
            }
            // validate end_time voi start_time
            if($product['EndTime'] < $product['StartTime']){
                $validator->errors()->add('E007', 'CampaignID='.$product['CampaignID'].' | ProductID='.$product['ProductID'].' | EndTime phải lớn hơn StartTime kiểm tra lại thời gian!');
            }
            // so sanh gia re online cua hotdeal moi so voi san pham dang chay gia re
            $checkprice = DB::select("SELECT t1.product_id,t1.list_price,t2.price,t1.nk_is_shock_price FROM cscart_products t1 JOIN cscart_product_prices t2 ON t1.product_id = t2.product_id WHERE t1.product_id = {$product['ProductID']} AND t1.nk_is_shock_price = 'O'");
            if(!empty($checkprice)){
                $checkprice = json_decode(json_encode($checkprice),true)[0];
                // so sanh gia re online voi gia niem yet
                if($product['PromoPrice'] > $checkprice['list_price']){
                    $validator->errors()->add('E005', '#E008 | SP '.$product['ProductID'].' | Giá online > Giá niêm yết - PromoPrice '.$product['PromoPrice'].' > list_price '.$checkprice['list_price']);
                }
                if($product['PromoPrice'] == $checkprice['list_price']){
                    $validator->errors()->add('E005', '#E009 | SP '.$product['ProductID'].' | Giá online = Giá niêm yết -- PromoPrice '.$product['PromoPrice'].' = list_price '.$checkprice['list_price']);
                }
            } else {
                $price      = ProductPrices::where('product_id',$product['ProductID'])->get(['price'])->first();
                $list_price = Products::where('product_id',$product['ProductID'])->get(['list_price'])->first();
                // so sanh gia re voi gia ban
                if($product['PromoPrice'] >= $price['price']){
                    $validator->errors()->add('E005', 'CampaignID='.$product['CampaignID'].' | ProductID='.$product['ProductID'].' | Giá online phải nhỏ hơn giá bán - PromoPrice '.$product['PromoPrice'].' -price '.$price['price']);
                }
                // so sanh gia re voi gia niem yet
                if($product['PromoPrice'] >= $list_price['list_price']){
                    $validator->errors()->add('E006', 'CampaignID='.$product['CampaignID'].' | ProductID='.$product['ProductID'].' | Giá online phải nhỏ hơn giá niêm yết - PromoPrice '.$product['PromoPrice'].' - list_price '.$list_price['list_price']);
                }
            }
        }
    }
    // PRETTY PRINT MESSAGE
    public function printMessage($message){
        echo '<pre>'.$message.'</pre>';
    }
    // LAY SAN PHAM GIA RE THEO MIDS
    public function getHotdealByMids($request){
        $params = $request->all();
        $mids = explode(',',$params['mids']);
        if(empty($params['mids']) || empty($mids)){
            return array(
                'status'  => false,
                'message' => "errors"
            );
        }
        $products = HotDeal::whereIn('_id',$mids)->get()->toArray();
        if(empty($products)){
            return array(
                'status'  => true,
                'message' => "success",
                'data'    => []
            );
        }
        foreach ($products as &$product){
            $product_name           = ProductDescriptions::where('product_id',intval($product['ProductID']))->where('lang_code','vi')->get(['product'])->first();
            $product['Product']     = $product_name['product'];
        }
        return array(
            'status'  => true,
            'message' => "success",
            'data'    => $products
        );
    }
    // UPDATE SP GIA RE v2
    public function updateV2($request){
        $params = $request->all();
        $product_invalid = array();
        $product_valid   = array();
        $errors          = array();
        foreach ($params['products'] as $product){
            if(isset($product['_id'])){
                // update it
                $hotdeal = HotDeal::where('_id',$product['_id'])->get()->first();
                if($hotdeal['Status'] == $this->status['W']){
                    $arrCheck = $this->checkProductValidUpdate($product,$hotdeal->toArray());
                    if($arrCheck[0]== 'OK' && !empty($arrCheck[1])){
                        HotDeal::where('_id',$product['_id'])->update($arrCheck[1]);
                        array_push($product_valid,$arrCheck[1]);
                    } else {
                        if(!isset($arrCheck[1])){
                            array_push($errors,$arrCheck);
                        }
                        array_push($product_invalid,$product);
                    }
                }
            } else {
                // create it
                if($this->checkProductValidImport($product) == 'OK'){
                    $hotdeal = $product;
                    $product_code           = Products::where('product_id',$product['ProductID'])->get(['product_code'])->first();
                    $hotdeal['ProductCode'] = (!empty($product_code['$product_code']))? $product_code['$product_code']: '';
                    $product_name           = ProductDescriptions::where('product_id',intval($product['ProductID']))->where('lang_code','vi')->get(['product'])->first();
                    $hotdeal['Product']     = $product_name['product'];
                    $hotdeal['SalePriceBefore'] = 0;
                    array_push($product_valid,$hotdeal);
                    HotDeal::create($hotdeal);
                } else {
                    $product['error'] = $this->checkProductValidImport($product);
                    $error            = $this->checkProductValidImport($product);
                    array_push($errors,$error);
                    array_push($product_invalid,$product);
                }
            }
        }

        return array(
            'status'  => true,
            'message' => "success",
            'data'    => array(
                'products'          => $product_valid,
                'products_invalid'  => $product_invalid,
                'errors'            => $errors
            ),

        );
    }
    // VALIDATE KHI UPDATE
    public function checkProductValidUpdate($product,$hotdeal){
        $diff = array_diff($product,$hotdeal);
        if(isset($diff['StartTime'])){
            // kiem tra thoi gian
            $product['StartTime'] = $diff['StartTime'];
            $query = HotDeal::where('ProductID',$product['ProductID']);
            $query->whereIn('Status',[$this->status['W'],$this->status['R']]);
            $query->where('EndTime','>=',$product['StartTime']);
            $query->where('_id','!=',$product['_id']);
            $query->orderBy('StartTime','DESC');
            $arr_productIsAvtive = $query->get()->toArray();
            $msg = '';
            if(!empty($arr_productIsAvtive)){
                foreach ($arr_productIsAvtive as $hotdeal){
                    if(!( (isset($product['StartTime']) && $product['StartTime'] > $hotdeal['EndTime'])
                        || (isset($product['EndTime']) && $product['EndTime'] < $hotdeal['StartTime']) )){
                        $msg .= 'Thời gian bị trùng với sản phẩm '.$hotdeal['ProductID'].'- CP : '.$hotdeal['CampaignID'].'| S: ';
                        $msg .= date("d-m-Y H:i:s", $hotdeal['StartTime']).'- E : ';
                        $msg .= date("d-m-Y H:i:s", $hotdeal['EndTime']);
                        break;
                    }
                }
            }
            $isDuplicate = $msg;
        }

        if(!empty($isDuplicate)){
            return '#E002 | SP '.$product['ProductID'].' '. $isDuplicate;
        }
        if(isset($diff['PromoPrice'])){
            // neu gia sp hien tai la gia re khong check voi gia ban & nguoc lai
            $checkprice = DB::select("SELECT t1.product_id,t1.list_price,t2.price,t1.nk_is_shock_price FROM cscart_products t1 JOIN cscart_product_prices t2 ON t1.product_id = t2.product_id WHERE t1.product_id = {$product['ProductID']} AND t1.nk_is_shock_price = 'O'");
            if(!empty($checkprice)){
                $checkprice = json_decode(json_encode($checkprice),true)[0];
                // so sanh gia re online voi gia niem yet
                if($product['PromoPrice'] > $checkprice['list_price']){
                    return '#E008 | SP '.$product['ProductID'].' | Giá online > Giá niêm yết - PromoPrice '.$product['PromoPrice'].' > list_price '.$checkprice['list_price'];
                }
                if($product['PromoPrice'] == $checkprice['list_price']){
                    return '#E009 | SP '.$product['ProductID'].' | Giá online = Giá niêm yết -- PromoPrice '.$product['PromoPrice'].' = list_price '.$checkprice['list_price'];
                }
            } else {
                $price = ProductPrices::where('product_id',$product['ProductID'])->get(['price'])->first();
                $list_price = Products::where('product_id',$product['ProductID'])->get(['list_price'])->first();
                // kiem tra
                if($product['PromoPrice'] > $price['price']){
                    return '#E006 | SP '.$product['ProductID'].' | Giá online > giá bán - PromoPrice '.$product['PromoPrice'].' > price '.$price['price'];
                }
                if($product['PromoPrice'] == $price['price']){
                    return '#E007 | SP '.$product['ProductID'].' | Giá online = Giá bán - PromoPrice '.$product['PromoPrice'].' = price '.$price['price'];
                }
                // so sanh gia re online voi gia niem yet
                if($product['PromoPrice'] > $list_price['list_price']){
                    return '#E008 | SP '.$product['ProductID'].' | Giá online > Giá niêm yết - PromoPrice '.$product['PromoPrice'].' > list_price '.$list_price['list_price'];
                }
                if($product['PromoPrice'] == $list_price['list_price']){
                    return '#E009 | SP '.$product['ProductID'].' | Giá online = Giá niêm yết -- PromoPrice '.$product['PromoPrice'].' = list_price '.$list_price['list_price'];
                }
            }
        }
        return array('OK',$diff);
    }
}