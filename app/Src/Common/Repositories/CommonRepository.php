<?php
namespace App\Src\Common\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Query\Query;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\Helpers\QRCodePartner\QRCodePartner;
use App\Helpers\QRCode\QRCode;

use App\Models\Mongo\OrderQrCode;
use App\Models\Mongo\CommonApiLogs;


class CommonRepository extends RepositoryBase
{
    protected $fractal;
    protected $qrcodePartner; 

    public function __construct(Manager $fractal, QRCode $qrcode, QRCodePartner $qrcodePartner)
    {
        $this->fractal = $fractal;
        $this->qrcode = $qrcode;
        $this->qrcodePartner = $qrcodePartner;
    }

    public function generateQRCode($request)
    {
        $result = ['success' => true];
        $params = $request->all();
        
        try {
            $qrContetnSBT = $this->qrcodePartner->getQRContentSTB($params);

            $errorCorrectionLevel = 'L';
            $matrixPointSize = 4;

            $filename = md5($qrContetnSBT . '|' . $errorCorrectionLevel . '|' . $matrixPointSize. '|'.time()) . '.png';
            // $filename =  'Test.png';

            $filename = '../temp/'. $filename;
            //$filename = '/home/apink/public_html/storage/logs/'.$filename;

            $this->qrcode->toPNG($qrContetnSBT, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

            $image = file_get_contents($filename);
			$filetype = 'png';
            $base64 = 'data:image/' . $filetype . ';base64,' . base64_encode($image);
            
            // save to order mogno qrcode
            $data['order_id'] = intval($params['order_id']);
            $data['qrcode'] = $base64;
            OrderQrCode::insert($data);
            unlink($filename);
            
            return [
                'order_id'=> $params['order_id'],
                'data_image' => $base64
            ];

        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        
    }
	
	public function createApiLog($request)
    {
        try {
            $params = $request->all();
            if (empty($params['source'])) {
                $params['source'] = 'OTHER';
            }
            $params['created_date'] = date("Y-m-d H:i:s");
            $params['timestamp'] = strtotime(date("Y-m-d H:i:s"));

            CommonApiLogs::insert($params);
            return [
                'code' => 'OK',
                'description' => 'Insert success'
            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function genQrCodeImage($QRData){

        $errorCorrectionLevel = 'L';
        $matrixPointSize      = 4;

        $filename = md5($QRData . '|' . $errorCorrectionLevel . '|' . $matrixPointSize . '|' . time()) . '.png';

        $filename = '../temp/' . $filename;

        $this->qrcode->toPNG($QRData, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

        $image    = file_get_contents($filename);
        $filetype = 'png';
        $base64   = 'data:image/' . $filetype . ';base64,' . base64_encode($image);

        unlink($filename);
        return $base64;
    }

    public function generateQRCodePartner($request){
        $params = $request->all();
        $QRData = isset($params['qr_data'])? $params['qr_data'] : '';
        $base64 = '';
        try {
            if (!empty($QRData))
            {
                $base64 = $this->genQrCodeImage($QRData);

                return [
                    'status'     =>201,
                    'data_image' => $base64
                ];
            } else {
                return [
                    'status'     =>304,
                    'data_image' => $base64
                ];
            }
        } catch (\Exception $e) {
            return [
                'status'     =>304,
                'data_image' => $base64
            ];
        }
    }

}
