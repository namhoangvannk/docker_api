<?php
/**
 * NGUYEN KIM
 * Created by Dan.SonThanh
 * Date: 14/06/2019
 * Time: 9:20 AM
 */

namespace App\Src\Common\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Cache\RedisManager;
use Log;


class RedisRepository extends RepositoryBase {

    protected $redis;

    public function __construct(RedisManager $redis){
        $this->redis    = $redis;
    }

    public function getRedisByKey($request,$key){
        $params = $request->all();
        if(empty($key)){
            return array(
                'status'=> 204,
                'data'=> []
            );
        }
        $database = isset($params['rdb'])? intval($params['rdb']): 0;
        if($this->redis->existsKey($key,$database)) {
            $data = $this->redis->getValue($key, $database,false);
            return array(
                'status'=> 200,
                'data'  => $data
            );
        }
        return array(
            'status'  => 204,
            'data'    => []
        );
    }

    public function setRedisByKey($request){
        $params = $request->all();
        $database = isset($params['rdb'])? intval($params['rdb']): 0;
        $expire   = isset($params['expire'])? intval($params['expire']): env('CACHE_PRODUCT_DETAIL', 300);
        if(empty($params['key'])){
            return array(
                'status' => 304,
                'msg'    => 'Not Modified'
            );
        }
        $this->redis->setValue($params['key'], $params['value'], $expire, $database, false);
        return array(
            'status'=> 200,
            'msg'   => 'Success set value key : '.$params['key']
        );
    }

    public function setRedisTime2LiveByKey($request){
        $params = $request->all();
        $database = isset($params['rdb'])? intval($params['rdb']): 0;
        if(empty($params['key']) || empty($params['ttl'])){
            return array(
                'status' => 304,
                'msg'    => 'Not Modified'
            );
        }
        if($this->redis->existsKey($params['key'],$database)) {
            //set the expiration with the TTL value
           $this->redis->setExpire($params['key'],$database,$params['ttl']);
           return array(
               'status' => 200,
               'msg'    =>'updated success key : '.$params['key']
           );
        }
        return array(
            'status' => 304,
            'msg'    => 'Not Modified'
        );
    }

}