<?php
/**
 * Created by PhpStorm.
 * User: Nam.HoangVan
 * Date: 6/4/2018
 * Time: 11:53 AM
 */

namespace App\Src\Banner\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Mongo\Banner;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;
use App\Models\Banners;

class BannerRepository extends RepositoryBase
{
    protected $model;
    protected $helper;
    protected $config;

    public function __construct(Helpers $helper)
    {
        $this->helper = $helper;
        $this->model = new Banner();
    }
    public function all($request)
    {
        $record =  Banner::all()->toArray();
        $total = count($record);
        return array(
            'total' => $total,
            'data' => $record
        );
    }
    public function show($request,$id)
    {
        $banner_id = intval($id);
        try {
            $datas =  DB::connection('mongodb')->collection('banners')->where('banner_id', $banner_id)->first();
            $total = count($datas);
            return array(
                'data' => $datas
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function create($request)
    {
        $data = $request->all();
        try {
            if (isset($data['timestamp'])) {
                $data['timestamp'] = $this->fn_date_to_timestamp($data['timestamp']);
            }
            if (isset($data['time_start'])) {
                $data['time_start'] = $this->fn_date_to_timestamp($data['time_start']);
            }
            if (isset($data['time_finish'])) {
                $data['time_finish'] = $this->fn_date_to_timestamp($data['time_finish']);
            }
            if (empty($data['background_color'])) {
                $data['background_color'] = '#ffffff';
            }
            $data['localization'] = empty($data['localization']) ? '' : fn_implode_localizations($data['localization']);
            if (!empty($data['background_color'])){
                $background_color = $data['background_color'];
            }
            $banner_id = intval($data['banner_id']);
            $lang_code = $data['lang_code'];
            $data_banner_images = array(
                'banner_id' => $banner_id,
                'lang_code' => $lang_code,
                'background_color' => $background_color
            );
            $languages = DB::connection('mongodb')->collection('languages')->get();
            $data_banner_descriptions = array();
            if(!empty($languages)){
                foreach ($languages as $item){
                    $descriptions['banner_id'] = $banner_id ;
                    $descriptions['banner'] = $data['banner'] ;
                    $descriptions['url'] = $data['url'] ;
                    $descriptions['bg_image'] = $data['bg_image'] ;
                    $descriptions['description'] = $data['description'] ;
                    $descriptions['lang_code'] = $item['lang_code'] ;
                    $descriptions['short_description'] = $data['description'] ;
                    $data_banner_descriptions[] = $descriptions;
                }
            }
            $data_nk_banner_ext = array();
            $nk_banner_ext_id = 2167;
            if(!empty($data['bg_image'])){
                    $nk_banner_ext['nk_banner_ext_id'] = $nk_banner_ext_id ;
                    $nk_banner_ext['banner_id'] = $banner_id ;
                    $nk_banner_ext['bg_image'] = $data['bg_image'] ;
                    $nk_banner_ext['left_image'] = '' ;
                    $nk_banner_ext['right_image'] = '' ;
                    $nk_banner_ext['type_effect'] = '' ;
                    $nk_banner_ext['bpm_is_fullscreen'] = 0 ;
                    $nk_banner_ext['bpm_phone_landscape_image_url'] = '' ;
                    $nk_banner_ext['bpm_phone_portrait_image_url'] = '' ;
                    $nk_banner_ext['bpm_tablet_landscape_image_url'] = '' ;
                    $nk_banner_ext['bpm_tablet_portrait_image_url'] = '' ;
                    $nk_banner_ext['grid_position'] = $data['grid_position'] ;
                    $nk_banner_ext['lossy_desktop_image'] = '' ;
                    $nk_banner_ext['lossy_mobile_image'] = '' ;
                    $nk_banner_ext['bpm_is_firework'] = $data['bpm_is_firework'] ;
                    $data_nk_banner_ext[] = $nk_banner_ext;
            }
            $data_main =array();
            $data_main['status'] = $data['status'];
            $data_main['type'] = $data['type'];
            $data_main['target'] = $data['target'];
            $data_main['timestamp'] = $data['timestamp'];
            $data_main['position'] = $data['position'];
            $data_main['position_floor_banner'] = $data['position_floor_banner'];
            $data_main['time_start'] = $data['time_start'];
            $data_main['time_finish'] = $data['time_finish'];
            $data_main['type_display'] = $data['type_display'];
            $data_main['category_ids'] = $data['category_ids'];
            $data_main['subcat_ids'] = $data['subcat_ids'];
            $data_main['is_recursive'] = $data['brand_ids'];
            $data_main['brand_ids'] = $data['background_color'];
            $data_main['area_ids'] = $data['area_ids'];
            $data_main['banner_images'] = json_encode($data_banner_images);
            $data_main['banner_descriptions'] = json_encode($data_banner_descriptions);
            $data_main['nk_banner_ext'] = json_encode($data_nk_banner_ext);

            DB::connection('mongodb')->collection('banners')->insert($data_main);
            return [
                'status' => 200,
                'message' => 'Thêm thành công'
            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function update($request)
    {
        $data = $request->all();
        $banner_id = 3631;
        $nk_banner_ext_id = 2167;
        $lang_code = $data['lang_code'];
        if (isset($data['timestamp'])) {
            $data['timestamp'] = $this->fn_date_to_timestamp($data['timestamp']);
        }
        if (isset($data['time_start'])) {
            $data['time_start'] = $this->fn_date_to_timestamp($data['time_start']);
        }
        if (isset($data['time_finish'])) {
            $data['time_finish'] = $this->fn_date_to_timestamp($data['time_finish']);
        }
        if (empty($data['background_color'])) {
            $data['background_color'] = '#ffffff';
        }
        $data['localization'] = empty($data['localization']) ? '' : fn_implode_localizations($data['localization']);
        if (!empty($data['background_color'])){
            $background_color = $data['background_color'];
        }
        // lấy data theo banner_id
        $banner_ext = DB::connection('mongodb')->collection('banners')->where('banner_id',$banner_id)->select('nk_banner_ext')->get();
        // xử lý dữ liệu nk_banner_ext
        $data_nk_banner_ext = array();
        $nk_banner_ext = $banner_ext[0]['nk_banner_ext'];
        if(!empty($nk_banner_ext)){
            $nk_banner_ext = json_decode($nk_banner_ext,true);
            if(!empty($nk_banner_ext)){
                if(!empty($data['bg_image'])){
                    foreach ($nk_banner_ext as $row){
                        $nk_banner_ext['nk_banner_ext_id'] = $row['nk_banner_ext_id'] ;
                        $nk_banner_ext['banner_id'] = $banner_id ;
                        $nk_banner_ext['bg_image'] = $data['bg_image'] ;
                        $nk_banner_ext['left_image'] = '' ;
                        $nk_banner_ext['right_image'] = '' ;
                        $nk_banner_ext['type_effect'] = '' ;
                        $nk_banner_ext['bpm_is_fullscreen'] = 0 ;
                        $nk_banner_ext['bpm_phone_landscape_image_url'] = '' ;
                        $nk_banner_ext['bpm_phone_portrait_image_url'] = '' ;
                        $nk_banner_ext['bpm_tablet_landscape_image_url'] = '' ;
                        $nk_banner_ext['bpm_tablet_portrait_image_url'] = '' ;
                        $nk_banner_ext['grid_position'] = $data['grid_position'] ;
                        $nk_banner_ext['lossy_desktop_image'] = '' ;
                        $nk_banner_ext['lossy_mobile_image'] = '' ;
                        $nk_banner_ext['bpm_is_firework'] = $data['bpm_is_firework'] ;
                        $data_nk_banner_ext[] = $nk_banner_ext;
                    }
                }
            }
        }
        // dữ liệu description
        $languages = DB::connection('mongodb')->collection('languages')->get();
        $data_banner_descriptions = array();
        if(!empty($languages)){
            foreach ($languages as $item){
                $descriptions['banner_id'] = $banner_id ;
                $descriptions['banner'] = $data['banner'] ;
                $descriptions['url'] = $data['url'] ;
                $descriptions['description'] = $data['description'] ;
                $descriptions['lang_code'] = $item['lang_code'] ;
                $descriptions['short_description'] = $data['description'] ;
                $data_banner_descriptions[] = $descriptions;
            }
        }
        // dữ liệu
        $data_banner_images = array();
        if(!empty($nk_banner_ext['banner_images'])){
            foreach ($nk_banner_ext['banner_images'] as $item){
                $banner_images = array(
                    'banner_image_id' => $item['banner_image_id'],
                    'banner_id' => $banner_id,
                    'lang_code' => $lang_code,
                    'background_color' => $background_color
                );
                $data_banner_images[] = $banner_images;
            }
        }else{
            $banner_image_id = 234567;
            $banner_images = array(
                'banner_image_id' => $banner_image_id,
                'banner_id' => $banner_id,
                'lang_code' => $lang_code,
                'background_color' => $background_color
            );
            $data_banner_images = $banner_images;
        }
        $data_main =array();
        $data_main['status'] = $data['status'];
        $data_main['type'] = $data['type'];
        $data_main['target'] = $data['target'];
        $data_main['timestamp'] = $data['timestamp'];
        $data_main['position'] = $data['position'];
        $data_main['position_floor_banner'] = $data['position_floor_banner'];
        $data_main['time_start'] = $data['time_start'];
        $data_main['time_finish'] = $data['time_finish'];
        $data_main['type_display'] = $data['type_display'];
        $data_main['category_ids'] = $data['category_ids'];
        $data_main['subcat_ids'] = $data['subcat_ids'];
        $data_main['is_recursive'] = $data['brand_ids'];
        $data_main['brand_ids'] = $data['background_color'];
        $data_main['area_ids'] = $data['area_ids'];
        $data_main['banner_images'] = json_encode($data_banner_images);
        $data_main['banner_descriptions'] = json_encode($data_banner_descriptions);
        DB::connection('mongodb')->collection('banners')->insert($data_main);

        $banner_image_id = $this->fn_get_banner_image_id($banner_id, $lang_code);
        $banner_image_exist = !empty($banner_image_id);
        $banner_is_multilang =$data['banner_multilang']; // Registry::get('addons.banners.banner_multilang') == 'Y';
        $image_is_update = $this->fn_banners_need_image_update();
        if ($banner_is_multilang == 'Y') {
            if ($banner_image_exist > 0  && $image_is_update) {
                //fn_delete_image_pairs($banner_image_id, 'promo');
                $banner_image_exist = 0;
            }
        } else {
            if(isset($data['url'])) {
                $data_banner_descriptions = DB::connection('mongodb')->collection('banners')->where('banner_id',$banner_id)->select('banner_descriptions')->get();
                if(!empty($data_banner_descriptions)){
                    $banner_descriptions = $data_banner_descriptions[0]['banner_descriptions'];
                    if(!empty($banner_descriptions)){
                        $banner_descriptions = json_decode($banner_descriptions,true);
                        $data_banner_descriptions = array();
                        foreach($banner_descriptions as $row){
                            $descriptions['banner_id'] = $row['banner_id'] ;
                            $descriptions['banner'] = $row['banner'] ;
                            $descriptions['url'] = $data['url'] ;
                            $descriptions['description'] = $row['description'] ;
                            $descriptions['lang_code'] = $row['lang_code'] ;
                            $descriptions['short_description'] = $row['description'] ;
                            $data_banner_descriptions[] = $descriptions;
                        }
                        DB::connection('mongodb')->collection('banners')->where('banner_id', $banner_id)
                            ->update(['banner_descriptions' => json_decode($data_banner_descriptions,true)]);
                    }
                }
                //db_query("UPDATE ?:banner_descriptions SET url = ?s WHERE banner_id = ?i", $data['url'], $banner_id);
            }
        }
        if ($image_is_update && $banner_image_exist == 0) {
            //db_query("delete from  ?:banner_images where banner_id=?i and lang_code=?s ", $banner_id, $lang_code);
            $banner_image_id = db_get_field("SELECT banner_image_id FROM ?:banner_images WHERE banner_id=?i AND lang_code=?s", $banner_id, $lang_code);
            if(empty($banner_image_id)){
                $banner_image_id = db_query("INSERT INTO ?:banner_images (banner_id, lang_code, background_color) VALUE(?i, ?s, ?s)", $banner_id, $lang_code, $background_color);
            }else{
                db_query("UPDATE ?:banner_images SET background_color=?s WHERE banner_id=?i AND lang_code=?s ", $background_color,$banner_id, $lang_code);
            }
        }



    }
    public function delete($request,$id)
    {
        $banner_id = intval($id);
        try {
            DB::connection('mongodb')->collection('banners')->where('banner_id',$banner_id)->delete();
            return [
                'status' => 200,
                'message' => 'Xóa thành công banner_id '.$banner_id

            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function sync($request)
    {
        $datas = $request->all();
        try {
            if(!empty($datas)){
                foreach ($datas as $key => $data){
                    $data_main['banner_id'] = (int)$data['banner_id'];
                    $data_main['banner'] = (int)$data['banner'];
                    $data_main['url'] = $data['url'];
                    $data_main['status'] = $data['status'];
                    $data_main['type'] = $data['type'];
                    $data_main['target'] = $data['target'];
                    $data_main['localization'] = $data['localization'];
                    $data_main['timestamp'] = (int)$data['timestamp'];
                    $data_main['position'] = (int)$data['position'];
                    $data_main['position_floor_banner'] =(int) $data['position_floor_banner'];
                    $data_main['company_id'] = (int)$data['company_id'];
                    $data_main['time_start'] = (int)$data['time_start'];
                    $data_main['time_finish'] = (int)$data['time_finish'];
                    $data_main['type_display'] = (int)$data['type_display'];
                    $data_main['category_ids'] = $data['category_ids'];
                    $data_main['subcat_ids'] = $data['subcat_ids'];
                    $data_main['is_recursive'] = $data['brand_ids'];
                    $data_main['is_check'] = (int)$data['is_check'];
                    $data_main['flexible_banner_position'] = $data['flexible_banner_position'];
                    $data_main['display_type'] = $data['display_type'];
                    $data_main['floor_position'] = $data['floor_position'];
                    $data_main['start_time'] = (int)$data['start_time'];
                    $data_main['end_time'] = (int)$data['end_time'];
                    $data_main['background_color'] = $data['background_color'];
                    $data_main['category_menu_position'] = (int)$data['category_menu_position'];
                    $data_main['category_menu_position_inner'] = (int)$data['category_menu_position_inner'];
                    $data_main['display_on'] = (int)$data['display_on'];
                    $data_main['nk_mp_vendor'] = $data['nk_mp_vendor'];
                    $data_main['carousel_grid_location'] = (int)$data['carousel_grid_location'];
                    $data_main['brand_ids'] = $data['background_color'];
                    $data_main['area_ids'] = $data['area_ids'];
                    $data_main['banner_images'] = $data['banner_image'];
                    $data_main['banner_descriptions'] = $data['banner_descriptions'];
                    $data_main['bg_image'] = $data['bg_image'];
                    $data_main['nk_banner_ext'] = $data['nk_banner_ext'];
                    $data_main['banner_areas'] = $data['banner_areas'];
                    $data_main['grid_position'] = $data['grid_position'];
                    $check = DB::connection('mongodb')->collection('banners')->where('banner_id',(int)$data['banner_id'])->get(['banner_id'])->first();
                    if($check){
                        $result = DB::connection('mongodb')->collection('banners')->where('banner_id',(int)$data['banner_id'])
                            ->update($data_main);
                        Banners::where('banner_id', (int)$data['banner_id'])
                            ->update(['sync_mongo' => 1]);
                    }else{
                        $result = DB::connection('mongodb')->collection('banners')->insert($data_main);
                        Banners::where('banner_id', (int)$data['banner_id'])
                            ->update(['sync_mongo' => 1]);
                    }
                }
                return [
                    'status' => 200,
                    'message' => 'Sync thành công'
                ];
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function fn_date_to_timestamp($date)
    {
        return strtotime(str_replace('/', '-', $date));
    }
    public function fn_implode_localizations($localizations)
    {
        return empty($localizations) ? '' : implode($localizations, ',');
    }
    public function fn_get_banner_image_id($banner_id, $lang_code)
    {
        $data_banner_images = DB::connection('mongodb')->collection('banners')->where('banner_id',$banner_id)->where('lang_code',$lang_code)->select('banner_images')->get();
        $banner_image_id = 0;
        if(!empty($data_banner_images)){
            $banner_images = $data_banner_images[0]['banner_images'];
            if(!empty($banner_images)){
                $banner_images = json_decode($banner_images,true);
                foreach($banner_images as $row){
                    if($row['banner_id'] == $banner_id && $lang_code == $row['lang_code']){
                        $banner_image_id = $row['banner_image_id'];
                        break;
                    }
                }

            }
        }
        return $banner_image_id;
       // return db_get_field("SELECT banner_image_id FROM ?:banner_images WHERE banner_id = ?i AND lang_code = ?s", $banner_id, $lang_code);
    }
    public function fn_banners_need_image_update($request)
    {
        if (!empty($request['file_banners_main_image_icon']) && array($request['file_banners_main_image_icon'])) {
            $image_banner = $this->reset ($request['file_banners_main_image_icon']);
            if ($image_banner == 'banners_main') {
                return false;
            }
        }
        return true;
    }
    public function reset (array &$array) {}
    public function sync2($request)
    {
        $datas = $request->all();
        try {
            if(!empty($datas)){
                foreach ($datas as $key => $data){
                    $data_main['banner_id'] = (int)$data['banner_id'];
                    $data_main['banner'] = (int)$data['banner'];
                    $data_main['url'] = $data['url'];
                    $data_main['status'] = $data['status'];
                    $data_main['type'] = $data['type'];
                    $data_main['target'] = $data['target'];
                    $data_main['localization'] = $data['localization'];
                    $data_main['timestamp'] = (int)$data['timestamp'];
                    $data_main['position'] = (int)$data['position'];
                    $data_main['position_floor_banner'] =(int) $data['position_floor_banner'];
                    $data_main['company_id'] = (int)$data['company_id'];
                    $data_main['time_start'] = (int)$data['time_start'];
                    $data_main['time_finish'] = (int)$data['time_finish'];
                    $data_main['type_display'] = (int)$data['type_display'];
                    $data_main['category_ids'] = $data['category_ids'];
                    $data_main['subcat_ids'] = $data['subcat_ids'];
                    $data_main['is_recursive'] = $data['brand_ids'];
                    $data_main['is_check'] = (int)$data['is_check'];
                    $data_main['flexible_banner_position'] = $data['flexible_banner_position'];
                    $data_main['display_type'] = $data['display_type'];
                    $data_main['floor_position'] = $data['floor_position'];
                    $data_main['start_time'] = (int)$data['start_time'];
                    $data_main['end_time'] = (int)$data['end_time'];
                    $data_main['background_color'] = $data['background_color'];
                    $data_main['category_menu_position'] = (int)$data['category_menu_position'];
                    $data_main['category_menu_position_inner'] = (int)$data['category_menu_position_inner'];
                    $data_main['display_on'] = (int)$data['display_on'];
                    $data_main['nk_mp_vendor'] = $data['nk_mp_vendor'];
                    $data_main['carousel_grid_location'] = (int)$data['carousel_grid_location'];
                    $data_main['brand_ids'] = $data['background_color'];
                    $data_main['area_ids'] = $data['area_ids'];
                    $data_main['banner_images'] = $data['banner_image'];
                    $data_main['banner_descriptions'] = $data['banner_descriptions'];
                    $data_main['nk_banner_ext'] = $data['nk_banner_ext'];
                    $data_main['nk_banner_ext'] = $data['nk_banner_ext'];
                    $data_main['banner_areas'] = $data['banner_areas'];
                    $data_main['grid_position'] = $data['grid_position'];
                    $check = DB::connection('mongodb')->collection('banners2')->where('banner_id',(int)$data['banner_id'])->get(['banner_id'])->first();
                    if($check){
                        $result = DB::connection('mongodb')->collection('banners2')->where('banner_id',(int)$data['banner_id'])
                            ->update($data_main);
                        Banners::where('banner_id', (int)$data['banner_id'])
                            ->update(['sync_mongo' => 1]);
                    }else{
                        $result = DB::connection('mongodb')->collection('banners2')->insert($data_main);
                        Banners::where('banner_id', (int)$data['banner_id'])
                            ->update(['sync_mongo' => 1]);
                    }
                }
                return [
                    'status' => 200,
                    'message' => 'Sync thành công'
                ];
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}