<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 10:03 AM
 */

namespace App\Src\WorldCup\Transformers;

use App\Models\Mongo\WorldCupMatches;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class WorldCupMatchesTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'guess',
        'guesses'
    ];

    protected $defaultIncludes = [
        'fst_team',
        'sec_team'
    ];

    public function transform($obj)
    {
        $MatchStartTime = '';
        if(!empty($obj->MatchStartTime)){
            /** @var Carbon $date */
            $date = $obj->MatchStartTime;
            $MatchStartTime = $date->toDateTimeString();
        }
        return [
            'MatchID' => $obj->MatchID,
            'RoundCode' => $obj->RoundCode,
            'RoundName' => $obj->RoundName,
            'FstTeam' => $obj->FstTeam,
            'FstTeamScoreNinety' => $obj->FstTeamScoreNinety,
            'FstTeamScoreTotal' => $obj->FstTeamScoreTotal,
            'FstTeamPoint' => $obj->FstTeamPoint,
            'SecTeam' => $obj->SecTeam,
            'SecTeamScoreNinety' => $obj->SecTeamScoreNinety,
            'SecTeamScoreTotal' => $obj->SecTeamScoreTotal,
            'SecTeamPoint' => $obj->SecTeamPoint,
            'MatchResFinal' => $obj->MatchResFinal,
            'MatchStartTime' => $MatchStartTime
        ];
    }
    /**
     * Include First Team
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeFstTeam(WorldCupMatches $matches)
    {
        if (!$matches->fst_team) {
            return null;
        }
        return $this->item($matches->fst_team, new WorldCupTeamsTransformer());
    }

    /**
     * Include Second Team
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeSecTeam(WorldCupMatches $matches)
    {
        if (!$matches->sec_team) {
            return null;
        }
        return $this->item($matches->sec_team, new WorldCupTeamsTransformer());
    }

    /**
     * Include the guess from one user
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeGuess(WorldCupMatches $matches)
    {
        if (!$matches->guess) {
            return null;
        }
        return $this->item($matches->guess, new WorldCupPlayerMatchesTransformer());
    }

    /**
     * Include the guesses from all user
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeGuesses(WorldCupMatches $matches)
    {
        if (!$matches->guesses) {
            return null;
        }
        return $this->collection($matches->guesses, new WorldCupPlayerMatchesTransformer());
    }
}