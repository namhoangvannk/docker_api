<?php
namespace App\Src\WorldCup\Transformers;

use App\Models\Mongo\WorldCupPlayerMatches;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class WorldCupPlayerMatchesTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'player'
    ];

    public function transform($obj)
    {
        $PredictTime = '';
        if(!empty($obj->PredictTime)){
            /** @var Carbon $date */
            $date = $obj->PredictTime;
            $PredictTime = $date->toDateTimeString();
        }
        return [
            'PlayerID' => $obj->PlayerID,
            'MatchID' => $obj->MatchID,
            'FstTeamScoreNinety' => $obj->FstTeamScoreNinety,
            'SecTeamScoreNinety' => $obj->SecTeamScoreNinety,
            'MatchResFinal' => $obj->MatchResFinal,
            'PlayerPredictRes' => $obj->PlayerPredictRes,
            'PlayerScoreReward' => $obj->PlayerScoreReward,
            'PredictTime' => $PredictTime
        ];
    }

    /**
     * Include player
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePlayer(WorldCupPlayerMatches $player)
    {
        if (!$player->player) {
            return null;
        }
        return $this->collection($player->player, new WorldCupPlayersTransformer());
    }
}