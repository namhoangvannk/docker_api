<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 10:51 AM
 */

namespace App\Src\WorldCup\Transformers;

use App\Models\Mongo\WorldCupPlayerMatches;
use App\Models\Mongo\WorldCupPlayers;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class WorldCupPlayersTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'matches'
    ];

    /*protected $defaultIncludes = [
        'matches'
    ];*/

    public function transform($obj)
    {
        $RegisteredDate = '';
        if(!empty($obj->RegisteredDate)){
            /** @var Carbon $date */
            $date = $obj->RegisteredDate;
            $RegisteredDate = $date->toDateTimeString();
        }
        return [
            'PlayerID' => $obj->PlayerID,         // ID tham chiếu đến user NK
            'PersonalID' => $obj->PersonalID,       // Số CMND
            'RegisteredDate' => $RegisteredDate,   // Ngày đăng ký tham gia
            'Status' => $obj->Status,           // Active/Inactive
            'TotalScore' => $obj->TotalScore,       // Tổng số điểm đạt được
            'Group' => $obj->Group,            // Y: được tham gia / N: không được tham gia
            'GroupScore' => $obj->GroupScore,       // Tổng điểm đạt được ở vòng loại
            'TimeGroup'  => $obj->TimeGroup,
            'RankGroup'=>$obj->RankGroup,
            'Sixteen' => $obj->Sixteen,          // Y: được tham gia / N: không được tham gia
            'SixteenScore' => $obj->SixteenScore,     // Tổng điểm đạt được ở vòng 1 / 16
            'TimeSixteen'  => $obj->TimeSixteen,
            'RankSixteen'=>$obj->RankSixteen,
            'Quarter' => $obj->Quarter,          // Y: được tham gia / N: không được tham gia
            'QuarterScore' => $obj->QuarterScore,     // Tổng điểm đạt được ở vòng tứ kết
            'TimeQuarter'  => $obj->TimeQuarter,
            'RankQuarter'=>$obj->RankQuarter,
            'Semi' => $obj->Semi,             // Y: được tham gia / N: không được tham gia
            'SemiScore' => $obj->SemiScore,        // Tổng điểm đạt được ở vòng bán kết
            'TimeSemi'  => $obj->TimeSemi,
            'RankSemi'=>$obj->RankSemi,
            'Playoff' => $obj->Playoff,          // Y: được tham gia / N: không được tham gia
            'PlayoffScore' => $obj->PlayoffScore,     // Tổng điểm đạt được ở vòng play off
            'TimePlayoff'  => $obj->TimePlayoff,
            'RankPlayoff'=>$obj->RankPlayoff,
            'Final' => $obj->Final,            // Y: được tham gia / N: không được tham gia
            'FinalScore'  => $obj->FinalScore,       // Tổng điểm đạt được ở vòng chung kết
            'TimeFinal'  => $obj->TimeFinal,
            'RankFinal'=>$obj->RankFinal,
            'FullName'  => $obj->FullName,
            'Gender'  => $obj->Gender,
            'Phone'  => $obj->Phone,
            'Email'  => $obj->Email,
        ];
    }
    /**
     * Include Matches
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeMatches(WorldCupPlayers $player)
    {
        if (!$player->matches) {
            return null;
        }
        return $this->collection($player->matches, new WorldCupPlayerMatchesTransformer());
    }
}