<?php

namespace App\Src\WorldCup\Transformers;

use League\Fractal\TransformerAbstract;

class WorldCupTeamsTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'TeamID'        => $obj->TeamID,       //Mã đội bóng (Ví dụ: VN)
            'Name'          => $obj->Name,         //Tên đội (Ví dụ: vietnam)
            'Description'   => $obj->Description,  //Tên đội (Ví dụ: Việt Nam)
            'Group'         => $obj->Group,         //Tên bảng đấu (A,B,C,D,E)
            'Class'         => $obj->Class,         //Tên bảng đấu (A,B,C,D,E)
            'Matches'       => $obj->Matches,
            'Win'           => $obj->Win,
            'Draw'          => $obj->Draw,
            'Loss'          => $obj->Loss,
            'Points'        => $obj->Points,
        ];
    }
}