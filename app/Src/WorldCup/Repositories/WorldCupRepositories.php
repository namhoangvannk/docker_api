<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 9:43 AM
 */

namespace App\Src\WorldCup\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Fractal\ArraySerializer;
use App\Helpers\Helpers;
use App\Models\Mongo\WorldCupMatches;
use App\Models\Mongo\WorldCupPlayers;
use App\Models\Mongo\WorldCupPlayerMatches;
use App\Rules\Uppercase;
use App\Src\WorldCup\Transformers\WorldCupMatchesTransformer;
use App\Src\WorldCup\Transformers\WorldCupPlayerMatchesTransformer;
use App\Src\WorldCup\Transformers\WorldCupPlayersTransformer;
use League\Fractal\Manager;
use App\Component\PagingComponent;
use League\Fractal\Resource\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Resource\Item;
use Log;

class WorldCupRepositories extends RepositoryBase
{
    protected $helper;
    protected $fractal;
    protected $model;
    protected $transformer;

    public function __construct(
        Helpers $helper,
        Manager $fractal
    )
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new WorldCupMatches();
    }

    public function ranks($request){
        try{
            $players = null;
            if($request->input('PlayerID')){
                $player_id = $request->input('PlayerID');
            }
            $rounds = [
                'GROUP' => ['Group','GroupScore','TimeGroup'],
                'R16' => ['Sixteen','SixteenScore','TimeSixteen'],
                'QUATER' => ['Quarter','QuarterScore','TimeQuarter'],
                'SEMI' => ['Semi','SemiScore','TimeSemi'],
                'PLAYOFF' => ['Playoff','PlayoffScore','TimePlayoff'],
                'FINAL' => ['Final','FinalScore','TimeFinal']
            ];
            $data = array();
            foreach ($rounds as $round => $value){
                $players = WorldCupPlayers::where('Status','Y')->where($value[0],'Y')->take(8)->orderBy($value[1],'desc')->orderBy($value[2],'asc')->get();
                if(isset($player_id) && !$players->contains('PlayerID', $player_id)){
                    $player = WorldCupPlayers::where('Status','Y')->where($value[0],'Y')->where('PlayerID',(int)$player_id)->first();
                    if(!empty($player)){
                        $players = $players->push($player);
                    }
                }
                $data[$round] = !empty($players->toArray())?$this->fractal->setSerializer(new ArraySerializer())->createData(new Collection($players,new WorldCupPlayersTransformer()))->toArray():array();
            }

            return $data;
        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }

    }

    public function updateScore($request)
    {
        try{
            return array();
        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }


    public function guesses($request, $id)
    {
        if(empty(WorldCupPlayers::where('PlayerID', (int)$id)->first())){
            return ['errors' => ['PlayerID' => 'The player is not found.']];
        }
        $manager = $this->fractal;
        $model = $this->model;

        $matches = $model->with(['fst_team','sec_team']);
        $matches->player($id);
        $matches->with(['guess' => function ($query) use ($id) {
            $query->where('PlayerID', (int)$id);
        }]);
        $manager->parseIncludes('guess');
        $matches->orderby('created_at','ASC')->orderby('MatchStartTime','ASC');
        $resource = new Collection($matches->get(),new WorldCupMatchesTransformer());
        $data = $manager->createData($resource)->toArray();
        return $data;
    }

}