<?php

namespace App\Src\WorldCup\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Component\PagingComponent;
use App\Models\Mongo\WorldCupMatches;
use App\Models\Mongo\WorldCupPlayerMatches;
use App\Models\Mongo\WorldCupPlayers;
use App\Src\WorldCup\Transformers\WorldCupPlayerMatchesTransformer;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class WorldCupPlayerMatchesRepositories extends RepositoryBase
{
    protected $model;
    protected $transformer;
    protected $fractal;

    public function __construct(Manager $fractal, WorldCupPlayerMatchesTransformer $transformer)
    {
        $this->model = new WorldCupPlayerMatches();
        $this->fractal = $fractal;
        $this->transformer = $transformer;
    }

    public function all($request){
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 20);

        $model = $this->model;
        $resource = new Collection($model->all(),$this->transformer);
        $data = $this->fractal->createData($resource)->toArray();

        $paging = new PagingComponent($model->count(), $offset, $limit);
        $data['paging'] = $paging->render();
        return $data;
    }

    public function create($request){
        $model = $this->model;
        $params = $request->all();
        $validator = Validator::make($params,[
            'PlayerID' => ['required',"exists:mongodb.worldcup_players,PlayerID"],
            'MatchID' => ['required',"exists:mongodb.worldcup_matches,MatchID"],
            'FstTeamScoreNinety' => ['integer'],
            'SecTeamScoreNinety' => ['integer'],
            'MatchResFinal' => ['required','in:FstWin,SecWin,Draw'],
            'PlayerPredictRes' => ['boolean'],
            'PlayerScoreReward' => ['integer']
        ]);
        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()){
                $data = $validator->getData();
                $guess = WorldCupPlayerMatches::where('PlayerID',$data['PlayerID'])->where('MatchID',$data['MatchID'])->get()->toArray();
                if (!empty($guess)){
                    $validator->errors()->add('Predicted', 'The player has already predicted.');
                }
                $match = WorldCupMatches::where('MatchID',$data['MatchID'])->first();
                $timegroup= date('Y-m-d H:i:s');
                $player = WorldCupPlayers::where('PlayerID',$data['PlayerID'])->first();
                if ($player['TimeGroup']==null && strpos($data['MatchID'],'GROUP')) $player['TimeGroup']=$timegroup;
                if ($player['TimeSixteen']==null && strpos($data['MatchID'],'R16')) $player['TimeSixteen']=$timegroup;
                if ($player['TimeQuarter']==null && strpos($data['MatchID'],'QUATER')) $player['TimeQuarter']=$timegroup;
                if ($player['TimeSemi']==null && strpos($data['MatchID'],'SEMI')) $player['TimeSemi']=$timegroup;
                if ($player['TimePlayoff']==null && strpos($data['MatchID'],'PLAYOFF')) $player['TimePlayoff']=$timegroup;
                if ($player['TimeFinal']==null && strpos($data['MatchID'],'FINAL')) $player['TimeFinal']=$timegroup;
                $player->update();
                if(
                    $player['Status'] == 'N' ||
                    ($match['RoundCode'] == 'GROUP' && $player['Group'] !== 'Y') ||
                    ($match['RoundCode'] == 'R16' && $player['Sixteen'] !== 'Y') ||
                    ($match['RoundCode'] == 'QUATER' && $player['Quarter'] !== 'Y') ||
                    ($match['RoundCode'] == 'SEMI' && $player['Semi'] !== 'Y') ||
                    ($match['RoundCode'] == 'PLAYOFF' && $player['Playoff'] !== 'Y') ||
                    ($match['RoundCode'] == 'FINAL' && $player['Final'] !== 'Y')
                ){
                    $validator->errors()->add('NotEligible', 'The player are not eligible to participate in the prediction.');
                }
                if(!empty($match['MatchResFinal']) || $match['MatchStartTime']->timestamp < strtotime('now')){
                    $validator->errors()->add('NotEligible', 'The player are not eligible to participate in the prediction.');
                }
            }

        });

        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }
        try{
            $model->fill($params);
            $model->save();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();

        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }
}