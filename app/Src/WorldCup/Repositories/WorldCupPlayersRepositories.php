<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 10:49 AM
 */

namespace App\Src\WorldCup\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Mongo\WorldCupPlayerMatches;
use App\Models\Mongo\WorldCupPlayers;
use App\Rules\Uppercase;
use League\Fractal\Manager;
use App\Component\PagingComponent;
use App\Src\WorldCup\Transformers\WorldCupPlayersTransformer;
use League\Fractal\Resource\Collection;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use Log;

class WorldCupPlayersRepositories extends RepositoryBase
{
    protected $helper;
    protected $fractal;
    protected $model;
    protected $transformer;

    public function __construct(Helpers $helper, Manager $fractal, WorldCupPlayersTransformer $transformer)
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new WorldCupPlayers();
        $this->transformer = $transformer;
    }

    public function all($request){
        $offset = (int)$request->input('offset', 0);
        $limit = (int)$request->input('limit', 20);

        $model = $this->model;
        $players = $model->all();
        $validator = Validator::make($request->all(),[
            'RoundCode' => ['string', new Uppercase() ,'in:GROUP,R16,QUATER,SEMI,PLAYOFF,FINAL']
        ]);
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }
        //get user of round
        if(!empty($request->get('RoundCode'))){
            $rounds = [
                'GROUP' => 'Group',
                'R16' => 'Sixteen',
                'QUATER' => 'Quarter',
                'SEMI' => 'Semi',
                'PLAYOFF' => 'Playoff',
                'FINAL' => 'Final'
            ];
            $players = $players->where($rounds[$request->get('RoundCode')], 'Y');
        }
        $resource = new Collection($players,$this->transformer);
        $data = $this->fractal->createData($resource)->toArray();

        $paging = new PagingComponent($players->count(), $offset, $limit);
        $data['paging'] = $paging->render();
        return $data;
    }

    public function create($request){
        $model = $this->model;
        $params = $request->all();
        $collection = $model->getTable();
        $validator = Validator::make($params,[
            'PlayerID' => [
                'required',
                'integer',
                "unique:mongodb.$collection,PlayerID,". $model->PlayerID . ",_id"
            ],
//            'PersonalID' => [
//                'required',
//                'string',
//                "unique:mongodb.$collection,PersonalID,". $model->PersonalID . ",_id"
//            ],
            'Status'        => ['in:Y,N'],
            'TotalScore'    => ['integer'],
            'Group'         => ['in:Y,N'],
            'GroupScore'    => ['integer'],
            'Sixteen'       => ['in:Y,N'],
            'SixteenScore'  => ['integer'],
            'Quarter'       => ['in:Y,N'],
            'QuarterScore'  => ['integer'],
            'Semi'          => ['in:Y,N'],
            'SemiScore'     => ['integer'],
            'Playoff'       => ['in:Y,N'],
            'PlayoffScore'  => ['integer'],
            'Final'         => ['in:Y,N'],
            'FinalScore'    => ['integer'],
            'FullName'      => ['required','string'],
            'Gender'        => ['required','string'],
            'Phone'         => ['required','string'],
            'Email'         => ['required','string']
        ]);
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }

        try{
            $params['PlayerID'] = (int)$params['PlayerID'];
            $model->fill($params);
            $model->save();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();

        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }


    public function show($request, $player_id){
        $player = $this->model->where('PlayerID',(int)$player_id)->with(['matches'])->first();
        if(empty($player)){
            return ['errors' => ['PlayerID' => 'The Player is not found']];
        }
        $manager = $this->fractal;
        if($request->input('includes')){
            $manager->parseIncludes('matches');
        }
        $manager->setSerializer(new ArraySerializer());
        return $manager->createData(new Item($player, $this->transformer))->toArray();
    }
    public function updateuser(){
        $data=array();
        $player= WorldCupPlayers::all();
        foreach ($player as $key => $value){
            $data['RankGroup']=0;
            $data['RankSixteen']=0;
            $data['RankQuarter']=0;
            $data['RankSemi']=0;
            $data['RankPlayoff']=0;
            $data['RankFinal']=0;
            $value->update($data);
        }
        return 1;
    }
    public function update($request, $player_id){
        $model = $this->model->where('PlayerID',$player_id)->first();
        if(!$model){
            return ['errors' => ['PlayerID' => 'The player has already exists']];
        }
        $params = $request->all();
        $validator = Validator::make($params,[
            'PersonalID'    => ['string'],
            'Status'        => ['in:Y,N'],
            'TotalScore'    => ['integer'],
            'Group'         => ['in:Y,N'],
            'GroupScore'    => ['integer'],
            'Sixteen'       => ['in:Y,N'],
            'SixteenScore'  => ['integer'],
            'Quarter'       => ['in:Y,N'],
            'QuarterScore'  => ['integer'],
            'Semi'          => ['in:Y,N'],
            'SemiScore'     => ['integer'],
            'Playoff'       => ['in:Y,N'],
            'PlayoffScore'  => ['integer'],
            'Final'         => ['in:Y,N'],
            'FinalScore'    => ['integer'],
        ]);
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }
        try{
            $model->fill($params);
            $model->save();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();

        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }

    public function scoring($request)
    {
        try{
            $players = WorldCupPlayers::all();
            foreach ($players->values() as $player){
                $data = array();
                $guesses = WorldCupPlayerMatches::where('PlayerID', $player->PlayerID);
                $TotalScore = $guesses->where("PlayerPredictRes",true)->get()->count();
                $GroupScore = $guesses->with(['match' => function($query){
                    $query->where('RoundCode',"GROUP");
                }])->get()->count();

                $data['TotalScore'] = $TotalScore;
                $data['GroupScore'] = $GroupScore;
                $data['Sixteen'] = ($GroupScore >= 8)?'Y':'N';

                if(isset($data['Sixteen']) && $data['Sixteen'] == 'Y'){
                    $R16Score = $guesses->with(['match' => function($query){
                        $query->where('RoundCode',"R16");
                    }])->get()->count();
                    $data['SixteenScore'] = $R16Score;
                    $data['Quarter'] = ($R16Score >= 4)?'Y':'N';
                }
                if(isset($data['Quarter']) && $data['Quarter'] == 'Y'){
                    $QuarterScore = $guesses->with(['match' => function($query){
                        $query->where('RoundCode',"QUARTER");
                    }])->get()->count();
                    $data['QuarterScore'] = $QuarterScore;
                    $data['Semi'] = ($QuarterScore >= 2)?'Y':'N';
                }
                if(isset($data['Semi']) && $data['Semi'] == 'Y'){
                    $SemiScore = $guesses->with(['match' => function($query){
                        $query->where('RoundCode',"SEMI");
                    }])->get()->count();
                    $data['SemiScore'] = $SemiScore;
                    $data['Playoff'] = ($SemiScore >= 1)?'Y':'N';
                    $data['Final'] = ($SemiScore >= 1)?'Y':'N';
                }
                if(isset($data['Playoff']) && $data['Playoff'] == 'Y'){
                    $PlayoffScore = $guesses->with(['match' => function($query){
                        $query->where('RoundCode',"PLAYOFF");
                    }])->get()->count();
                    $data['PlayoffScore'] = $PlayoffScore;
                }
                if(isset($data['Final']) && $data['Final'] == 'Y'){
                    $FinalScore = $guesses->with(['match' => function($query){
                        $query->where('RoundCode',"FINAL");
                    }])->get()->count();
                    $data['FinalScore'] = $FinalScore;
                }

                $player->update($data);
            }
            return ['status' => true];
        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }
}