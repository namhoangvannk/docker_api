<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 9:43 AM
 */

namespace App\Src\WorldCup\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Helpers;
use App\Models\Mongo\WorldCupMatches;
use App\Models\Mongo\WorldCupPlayerMatches;
use App\Models\Mongo\WorldCupPlayers;
use App\Models\Mongo\WorldCupTeams;
use App\Rules\Uppercase;
use App\Src\WorldCup\Transformers\WorldCupPlayersTransformer;
use League\Fractal\Manager;
use App\Component\PagingComponent;
use App\Src\WorldCup\Transformers\WorldCupMatchesTransformer;
use League\Fractal\Resource\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use Log;

class WorldCupMatchesRepositories extends RepositoryBase
{
    protected $helper;
    protected $fractal;
    protected $model;
    protected $transformer;

    public function __construct(Helpers $helper, Manager $fractal, WorldCupMatchesTransformer $transformer)
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new WorldCupMatches();
        $this->transformer = $transformer;
    }

    public function all($request){
        $validator = Validator::make($request->all(),[
            'RoundCode' => ['string', new Uppercase(),'in:GROUP,R16,QUATER,SEMI,PLAYOFF,FINAL'],
            'TeamID' => ['string',new Uppercase,'exists:mongodb.worldcup_teams,TeamID']
        ]);
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }
        $player = $request->input('PlayerID');
        $round_code = $request->input('RoundCode');
        $team = $request->input('TeamID');
        $manager = $this->fractal;
        $model = $this->model;

        $matches = $model->with(['fst_team','sec_team']);
        if(!empty($round_code)){
            $matches->where('RoundCode',$round_code);
        }
        if(!empty($team)){
            $matches->where('FstTeam',$team)->orWhere('SecTeam',$team);
        }
        if(!empty($player)){
            $matches->with(['guess' => function ($query) use ($player) {
                $query->where('PlayerID', (int)$player);
            }]);
            $manager->parseIncludes('guess');
        }
        $matches->orderby('MatchStartTime','ASC')->orderby('created_at','ASC');
        $resource = new Collection($matches->get(),$this->transformer);
        $data = $manager->createData($resource)->toArray();
        if(!empty($player)){
            $player_info = WorldCupPlayers::where('PlayerID',(int)$player)->first();
            if(!empty($player_info)){
            $data['PlayerData'] = $manager->setSerializer(new ArraySerializer())->createData(new Item($player_info, new WorldCupPlayersTransformer()))->toArray();
            }
        }
        return $data;
    }

    public function show($request, $id){
        $model = $this->model;
        $manager = $this->fractal;
        $params = $request->all();
        $params['MatchID'] = $id;
        $validator = Validator::make($params,[
            'MatchID' => ['required','string', new Uppercase(),'exists:mongodb.worldcup_matches,MatchID']
        ]);
        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()){
                $data = $validator->getData();
                if(isset($data['PlayerID']) && empty(WorldCupPlayers::where('PlayerID', (int)$data['PlayerID'])->first())){
                    $validator->errors()->add('PlayerID', 'The player is not found.');
                }
            }
        });
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }
        $relationships = ['guesses'];
        $includes = [];
        if(!empty($params['PlayerID'])){
            $player = $params['PlayerID'];
            $relationships['guess'] = function ($query) use ($player) {
                $query->where('PlayerID', (int)$player);
            };
            array_push($includes,'guess');
        }
        $matches = $model->where('MatchID',$id)->with($relationships)->first();
        $fist_win = $matches->guesses->where('MatchResFinal','FstWin')->count();
        $sec_win = $matches->guesses->where('MatchResFinal','SecWin')->count();
        $raw = $matches->guesses->where('MatchResFinal','Draw')->count();
        $total = $matches->guesses->count();
        if($request->input('includes') == 'guesses'){
            array_push($includes,'guesses');
        }
        $manager->parseIncludes($includes);
        $manager->setSerializer(new ArraySerializer());
        $data =  $manager->createData(new Item($matches, $this->transformer))->toArray();
        $data['aggregate'] = [
            'Total' => $total,
            'FstWin' => $fist_win,
            'SecWin' => $sec_win,
            'Raw' => $raw
        ];
        return $data;
    }

    public function create($request){
        $params = $request->all();
        $validator = Validator::make($params,[
            'RoundCode' => ['required','string',new Uppercase,'in:GROUP,R16,QUATER,SEMI,PLAYOFF,FINAL'],
            'FstTeam' => ['required','string',new Uppercase,'exists:mongodb.worldcup_teams,TeamID'],
            'SecTeam' => ['required','string',new Uppercase,'exists:mongodb.worldcup_teams,TeamID','different:FstTeam'],
            'RoundName' => ['required','string'],
            'MatchResFinal' => ['string','in:FstWin,SecWin,Draw'],
            'MatchStartTime' => ['required','string','date_format:Y-m-d H:i:s']
        ]);
        $validator->after(function ($validator) {
            if($validator->errors()->isEmpty()){
                $data = $validator->getData();
                $MatchID = strtoupper(implode("_",[$data['FstTeam'],$data['SecTeam'],$data['RoundCode']]));
                if(WorldCupMatches::where('MatchID', $MatchID)->first()){
                    $validator->errors()->add('MatchID', 'The match has already created.');
                }
            }
        });
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }
        try{
            $model = $this->model;
            $model->fill($params);
            $model->save();
            return $this->fractal->setSerializer(new ArraySerializer())->createData(new Item($model, $this->transformer))->toArray();

        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }

    public function update($request, $id){
        $model = $this->model;
        $match = $model->where('MatchID',strtoupper($id))->first();
        if(empty($match)){
            return ['errors' => 'The match is not found'];
        }
        $params = $request->all();
        $valid = Validator::make($params,
            [
                "RoundCode" => ['string',new Uppercase,'in:GROUP,R16,QUATER,SEMI,PLAYOFF,FINAL'],
                "RoundName" => ['string'],
                "FstTeam" => ['string',new Uppercase,'exists:mongodb.worldcup_teams,TeamID'],
                "FstTeamScoreNinety" => ['required','integer'],
                "FstTeamScoreTotal" => ['required','integer'],
                "FstTeamPoint" => ['integer'],
                "SecTeam" => ['string',new Uppercase,'exists:mongodb.worldcup_teams,TeamID'],
                "SecTeamScoreNinety" => ['required','integer'],
                "SecTeamScoreTotal" => ['required','integer'],
                "SecTeamPoint" => ['integer'],
                "MatchResFinal" => ['string','in:FstWin,SecWin,Draw'],
                'MatchStartTime' => ['required','string','date_format:Y-m-d H:i:s']
            ]
        );

        if($valid->fails()){
            return ['errors' => $valid->errors()];
        }

        try{
            $match->fill($params);
            if($match->RoundCode === 'GROUP'){
                if($match->FstTeamScoreTotal > $match->SecTeamScoreTotal){
                    $match->FstTeamPoint = 3;
                    $match->SecTeamPoint = 0;
                }elseif ($match->FstTeamScoreTotal < $match->SecTeamScoreTotal){
                    $match->FstTeamPoint = 0;
                    $match->SecTeamPoint = 3;
                }else{
                    $match->FstTeamPoint = 1;
                    $match->SecTeamPoint = 1;
                }
            }
            $match->update();
            if(!empty($match->MatchResFinal)){
                $this->updateTeamScore($match->FstTeam);
                $this->updateTeamScore($match->SecTeam);

                $guesses = WorldCupPlayerMatches::where('MatchID',$match->MatchID)->get();
                $guesses->each(function ($guess, $key) use ($match) {
                    if($match->RoundCode !== 'FINAL' && $match->RoundCode !== 'PLAYOFF' ){
                        if($guess->MatchResFinal == $match->MatchResFinal){
                            $guess->update([
                                'PlayerScoreReward' => 1,
                                'PlayerPredictRes' => true
                            ]);
                        }else{
                            $guess->update([
                                'PlayerScoreReward' => 0,
                                'PlayerPredictRes' => false
                            ]);
                        }
                    }else{
                        if($guess->FstTeamScoreNinety == $match->FstTeamScoreNinety && $guess['SecTeamScoreNinety'] == $match->SecTeamScoreNinety){
                            $guess->update([
                                'PlayerScoreReward' => 1,
                                'PlayerPredictRes' => true
                            ]);
                        }else{
                            $guess->update([
                                'PlayerScoreReward' => 0,
                                'PlayerPredictRes' => false
                            ]);
                        }
                    }
                    $this->updatePlayerScore($guess->PlayerID);
                });
            }

            return $this->fractal->setSerializer(new ArraySerializer())->createData(new Item($match, $this->transformer))->toArray();

        }catch (\Exception $e){
            Log::info('worldcupupdateguesses: ' . $e->getMessage());
            return ['errors' => $e->getMessage()];
        }
    }
    public function updatefinal(){
        $data=array();
        $updatefinal= WorldCupPlayerMatches::where('MatchID','FRA_CRO_FINAL')->get();
        foreach ($updatefinal as $key => $value){
            $FstTeamScoreNinety=  $value['FstTeamScoreNinety'];
            $SecTeamScoreNinety=  $value['SecTeamScoreNinety'];
            if($FstTeamScoreNinety < $SecTeamScoreNinety)
            {
                $data['MatchResFinal']='SecWin';
            }elseif($FstTeamScoreNinety == $SecTeamScoreNinety) {
                $data['MatchResFinal'] = 'Draw';
            }else
            {
                $data['MatchResFinal'] = 'FstWin';
            }
              $value->update($data);
        }
        return $updatefinal;
    }

    public function updateTeamScore($team_id)
    {
        $team = WorldCupTeams::where('TeamID', $team_id)->first();
        $matches = WorldCupMatches::where(function($query) use ($team_id){
            $query->where('FstTeam', $team_id);
            $query->orWhere('SecTeam', $team_id);
        })->where('RoundCode','GROUP')->whereIn('MatchResFinal',['FstWin','SecWin','Draw'])->get();

        $wins = WorldCupMatches::where(function($query) use ($team_id){
            $query->where('FstTeam', $team_id)->where('MatchResFinal','FstWin');
        })->orWhere(function($query) use ($team_id){
            $query->where('SecTeam', $team_id)->where('MatchResFinal','SecWin');
        })->where('RoundCode','GROUP')->get()->count();
        $draws = $matches->where('MatchResFinal','Draw')->count();
        $losses = $matches->count() - $wins - $draws;
        $points = ($wins*3) + $draws;
        $team->Matches = $matches->count();
        $team->Win = $wins;
        $team->Loss = $losses;
        $team->Draw = $draws;
        $team->Points = $points;
        $team->update();
    }

    public function updatePlayerScore($player_id){
        $data = array();
        $player = WorldCupPlayers::where('PlayerID',$player_id)->first();
        $guesses = WorldCupPlayerMatches::where('PlayerID', $player_id);
        $TotalScore = WorldCupPlayerMatches::where('PlayerID', $player_id)->where("PlayerPredictRes",true)->get()->count();
        $GroupScore = $guesses->where('MatchID','like',"%GROUP")->where("PlayerPredictRes",true)->get()->count();
        $data['TotalScore'] = $TotalScore;
        $data['GroupScore'] = $GroupScore;
//        $data['Sixteen'] = ($GroupScore >= 28)?'Y':'N';
        $data['Sixteen'] = ($GroupScore >= 8)?'Y':'N';
        if(isset($data['Sixteen']) && $data['Sixteen'] == 'Y'){
            $R16Score = WorldCupPlayerMatches::where('PlayerID', $player_id)->where('MatchID','like',"%R16")->where("PlayerPredictRes",true)->get()->count();
            $data['SixteenScore'] = $R16Score;
//            $data['Quarter'] = ($R16Score >= 5)?'Y':'N';
            $data['Quarter'] = ($R16Score >= 4)?'Y':'N';
        }
        if(isset($data['Quarter']) && $data['Quarter'] == 'Y'){
            $QuarterScore = WorldCupPlayerMatches::where('PlayerID', $player_id)
                ->where(function($q) {
                    $q->where('MatchID','=',"URU_FRA_GROUP")
                        ->orWhere('MatchID','=',"BRA_BEL_GROUP")
                        ->orWhere('MatchID','=',"SWE_ENG_GROUP")
                        ->orWhere('MatchID','=',"RUS_CRO_GROUP");
                })
                ->where("PlayerPredictRes",true)->get()->count();
            $data['QuarterScore'] = $QuarterScore;
            $data['Semi'] = ($QuarterScore >= 2)?'Y':'N';
        }
        if(isset($data['Semi']) && $data['Semi'] == 'Y'){
            $SemiScore = WorldCupPlayerMatches::where('PlayerID', $player_id)->where('MatchID','like',"%SEMI")->where("PlayerPredictRes",true)->get()->count();
            $data['SemiScore'] = $SemiScore;
            $data['Playoff'] = ($SemiScore >= 1)?'Y':'N';
            $data['Final'] = ($SemiScore >= 1)?'Y':'N';
        }
        if(isset($data['Playoff']) && $data['Playoff'] == 'Y'){
            $PlayoffScore = WorldCupPlayerMatches::where('PlayerID', $player_id)->where('MatchID','like',"%PLAYOFF")->where("PlayerPredictRes",true)->get()->count();
            $data['PlayoffScore'] = $PlayoffScore;
        }
        if(isset($data['Final']) && $data['Final'] == 'Y'){
            $FinalScore = WorldCupPlayerMatches::where('PlayerID', $player_id)->where('MatchID','like',"%FINAL")->where("PlayerPredictRes",true)->get()->count();
            $data['FinalScore'] = $FinalScore;

        }
        $rounds = [
            'GROUP' => ['Group','GroupScore','TimeGroup','RankGroup'],
            'R16' => ['Sixteen','SixteenScore','TimeSixteen','RankSixteen'],
            'QUATER' => ['Quarter','QuarterScore','TimeQuarter','RankQuarter'],
            'SEMI' => ['Semi','SemiScore','TimeSemi','RankSemi'],
            'PLAYOFF' => ['Playoff','PlayoffScore','TimePlayoff','RankPlayoff'],
            'FINAL' => ['Final','FinalScore','TimeFinal','RankFinal']
        ];
        foreach ($rounds as $round => $value) {
            $playlists = WorldCupPlayers::where($value[0],'Y')->orderBy($value[1],'desc')->orderBy($value[2],'asc')->get();
            foreach ($playlists as $key => $vl) {
                if ($vl->PlayerID == (int)$player_id) {
                    $data[$value[3]] = $key+1;
                    break;
                }
            }
        }
        if(!empty($data)){
            $dtlimit=array();
            $total=count($data);
            $zing=$total/300;
            $page=0;
            $vna=ceil($zing);
            for($i=1; $i<=$vna; $i++){
                $dtlimit=array_slice($data,$page,300,true);
                if(!empty($dtlimit)){
                $player->update($dtlimit);
                }
                $page+=300;
            }
        }
    }
}