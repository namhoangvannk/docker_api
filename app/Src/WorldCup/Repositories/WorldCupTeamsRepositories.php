<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 24/04/2018
 * Time: 10:49 AM
 */

namespace App\Src\WorldCup\Repositories;

use App\Base\Repositories\RepositoryBase;
use App\Helpers\Fractal\ArraySerializer;
use App\Helpers\Helpers;
use App\Models\Mongo\WorldCupMatches;
use App\Models\Mongo\WorldCupPlayerMatches;
use App\Models\Mongo\WorldCupTeams;
use League\Fractal\Manager;
use App\Component\PagingComponent;
use App\Src\WorldCup\Transformers\WorldCupTeamsTransformer;
use League\Fractal\Resource\Collection;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Resource\Item;
use Log;

class WorldCupTeamsRepositories extends RepositoryBase
{
    protected $helper;
    protected $fractal;
    protected $model;
    protected $transformer;

    public function __construct(Helpers $helper, Manager $fractal, WorldCupTeamsTransformer $transformer)
    {
        $this->helper = $helper;
        $this->fractal = $fractal;
        $this->model = new WorldCupTeams();
        $this->transformer = $transformer;
    }

    public function all($request){
        $model = $this->model;
        $model=WorldCupTeams::orderBy('Group', 'ASC')->get();
        $resource = new Collection($model,$this->transformer);
        return $this->fractal->createData($resource)->toArray();
    }

    public function create($request){
        $model = $this->model;
        $params = $request->all();
        $collection = $model->getTable();
        $validator = Validator::make($params,[
            'TeamID'        => ['required','string',"unique:mongodb.$collection,TeamID,". $model->TeamID . ",_id"],
            'Name'          => ['required','string'],
            'Description'   => ['required','string'],
            'Group'         => ['required','string'],
            'Class'         => ['string']
        ]);
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }

        try{
            $model->fill($params);
            $model->save();

            return $this->fractal->createData(new Item($model, $this->transformer))->toArray();

        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }


    public function show($request, $id){
        $model = $this->model->where('TeamID',$id)->first();
        if(!$model){
            return ['errors' => ['TeamID' => 'The team is not found']];
        }
        return $this->fractal->setSerializer(new ArraySerializer())->createData(new Item($model, $this->transformer))->toArray();
    }

    public function update($request, $id){
        $model = $this->model->where('TeamID',$id)->first();
        if(!$model){
            return ['errors' => ['TeamID' => 'The team is not found']];
        }
        $params = $request->all();
        $validator = Validator::make($params,[
            'Name'          => ['string'],
            'Description'   => ['string'],
            'Group'         => ['string'],
            'Class'         => ['string']
        ]);
        if($validator->fails()){
            return ['errors' => $validator->errors()];
        }
        try{
            $model->fill($params);
            $model->update();

            return $this->fractal->setSerializer(new ArraySerializer())->createData(new Item($model, $this->transformer))->toArray();

        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }

    /**
     * Tính điểm của đội bóng
     * @param array $match
     */
    public function scoring($request, $team_id){
        $model = $this->model;
        $team = $model->where('TeamID', $team_id)->first();
        if(empty($team)){
            return ['errors' => 'The team is not found'];
        }
        try{
            $team = WorldCupTeams::where('TeamID', $team_id)->first();
            $matches = WorldCupMatches::where(function($query) use ($team_id){
                $query->where('FstTeam', $team_id);
                $query->orWhere('SecTeam', $team_id);
            })->where('RoundCode','GROUP')->whereIn('MatchResFinal',['FstWin','SecWin','Draw'])->get();

            $wins = WorldCupMatches::where(function($query) use ($team_id){
                $query->where('FstTeam', $team_id)->where('MatchResFinal','FstWin');
            })->orWhere(function($query) use ($team_id){
                $query->where('SecTeam', $team_id)->where('MatchResFinal','SecWin');
            })->where('RoundCode','GROUP')->get()->count();
            $draws = $matches->where('MatchResFinal','Draw')->count();
            $losses = $matches->count() - $wins - $draws;
            $points = ($wins*3) + $draws;
            $team->Matches = $matches->count();
            $team->Win = $wins;
            $team->Loss = $losses;
            $team->Draw = $draws;
            $team->Points = $points;
            $team->update();

            return $this->fractal->setSerializer(new ArraySerializer())->createData(new Item($team, $this->transformer))->toArray();

        }catch (\Exception $e){
            return ['errors' => $e->getMessage()];
        }
    }
}