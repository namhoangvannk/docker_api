<?php

namespace App\Src\Payoo\Repositories;

use App\Base\Repositories\RepositoryBase;
use SoapClient;


class PayooRepository extends RepositoryBase
{
    public function execute2($request)
    {

        try {
            $params  = $request->all();
            $options = [
                "stream_context" => stream_context_create(
                    [
                        'ssl' => [
                            'verify_peer'       => false,
                            'verify_peer_name'  => false,
                            'allow_self_signed' => true,
                        ],
                    ]
                ),
            ];

            $client   = new SoapClient($params['url'], $options);
            $response = $client->__soapCall($params['function_name'], [['request' => $params['params']]]);

            return [
                'code' => '200',
                'data' => $response,
            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }


}
