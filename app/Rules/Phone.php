<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 22/03/2018
 * Time: 11:17 AM
 */
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Phone implements Rule
{
    protected $phone_regex = '/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/';
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value){
        $number = preg_replace('/[\s\.\-\(\)]+/', '', strval($value));
        return (preg_match($this->phone_regex, $number));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(){
        return 'The :attribute must be a valid phone number.';
    }
}