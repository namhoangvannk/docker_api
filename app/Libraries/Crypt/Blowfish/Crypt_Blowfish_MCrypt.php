<?php
namespace App\Libraries\Crypt\Blowfish;

use App\Libraries\Crypt\Crypt_Blowfish;


class Crypt_Blowfish_MCrypt extends Crypt_Blowfish
{
    /**
     * Mcrypt td resource
     *
     * @var resource
     * @access private
     */
    protected $_td = null;

    /**
     * Crypt_Blowfish Constructor
     * Initializes the Crypt_Blowfish object, and sets the secret key
     *
     * @param string $key
     * @param string $mode operating mode 'ecb', 'cbc'...
     * @param string $iv initialization vector
     * @access public
     */
    public function __construct($key = null, $mode = 'ecb', $iv = null)
    {
        $this->_iv = $iv . ((strlen($iv) < 8)
                            ? str_repeat(chr(0), 8 - strlen($iv)) : '');

        $this->_td = mcrypt_module_open(MCRYPT_BLOWFISH, '', $mode, '');
        if (is_null($iv)) {
            $this->_iv = mcrypt_create_iv(8, MCRYPT_RAND);
        }

        switch (strtolower($mode)) {
            case 'ecb':
                $this->_iv_required = false;
                break;

            case 'cbc':
            default:
                $this->_iv_required = true;
                break;
        }

        $this->setKey($key, $this->_iv);
    }

    /**
     * Encrypts a string
     *
     * Value is padded with NUL characters prior to encryption. You may
     * need to trim or cast the type when you decrypt.
     *
     * @param string $plainText string of characters/bytes to encrypt
     * @return string|PEAR_Error Returns cipher text on success,
     *                           or PEAR_Error on failure
     * @access public
     */
    function encrypt($plainText)
    {
        if (!is_string($plainText)) {
            throw new \Exception('Input must be a string');
        }

        return mcrypt_generic($this->_td, $plainText);
    }


    /**
     * Decrypts an encrypted string
     *
     * The value was padded with NUL characters when encrypted. You may
     * need to trim the result or cast its type.
     *
     * @param string $cipherText
     * @return string|PEAR_Error Returns plain text on success,
     *                           or PEAR_Error on failure
     * @access public
     */
    function decrypt($cipherText)
    {
        if (!is_string($cipherText)) {
            throw new \Exception('Cipher text must be a string');
        }

        return mdecrypt_generic($this->_td, $cipherText);
    }

    /**
     * Sets the secret key
     * The key must be non-zero, and less than or equal to
     * 56 characters (bytes) in length.
     *
     * If you are making use of the PHP mcrypt extension, you must call this
     * method before each encrypt() and decrypt() call.
     *
     * @param string $key
     * @param string $iv 8-char initialization vector (required for CBC mode)
     * @return boolean|PEAR_Error  Returns TRUE on success, PEAR_Error on failure
     * @access public
     */
    function setKey($key, $iv = null)
    {
        static $keyHash = null;

        if (!is_string($key)) {
            throw new \Exception('Key must be a string');
        }

        $len = strlen($key);

        if ($len > 56 || $len == 0) {
            throw new \Exception('Key must be less than 56 characters (bytes) and non-zero. Supplied key length: ' . $len);
        }

        if ($this->_iv_required) {
            if (strlen($iv) != 8) {
                throw new \Exception('IV must be 8-character (byte) long. Supplied IV length: ' . strlen($iv));
            }
            $this->_iv = $iv;
        }

        $return = mcrypt_generic_init($this->_td, $key, $this->_iv);
        if ($return < 0) {
            throw new \Exception('Unknown PHP MCrypt library error');
        }
        return true;
    }
}

?>