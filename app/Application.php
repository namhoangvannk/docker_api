<?php

namespace App;

use Laravel\Lumen\Application as LumenApplication;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Application extends LumenApplication
{
    protected function registerLogBindings()
    {
        $this->singleton('Psr\Log\LoggerInterface', function () {
            return new Logger('lumen', $this->getMonologHandler());
        });
    }
    
    protected function getMonologHandler()
    {
        $handlers = [];
        $info_log = 'logs/info_' . date('Y_m_d') . '.log';
        $handlers []= (new StreamHandler(storage_path($info_log), Logger::INFO))
            ->setFormatter(new LineFormatter(null, null, true, true));

        $warning_log = 'logs/warning_' . date('Y_m_d') . '.log';
        $handlers[] = (new StreamHandler(storage_path($warning_log), Logger::WARNING))
            ->setFormatter(new LineFormatter(null, null, true, true));

        $error_log = 'logs/error_' . date('Y_m_d') . '.log';
        $handlers[] = (new StreamHandler(storage_path($error_log), Logger::ERROR))
            ->setFormatter(new LineFormatter(null, null, true, true));
        
        return $handlers;
    }
}