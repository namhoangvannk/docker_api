<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 04/01/2018
 * Time: 3:00 PM
 */

namespace App\Mail;

use App\Models\Mongo\OrderCoupons;
use App\Models\ProductDescriptions;
use App\Models\Products;
use App\Models\Promotions;
use App\Models\Users;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use League\Fractal\Resource\Collection;

class WinnerCoupons extends Mailable
{
    use Queueable, SerializesModels;
    public $promotion;
    public $product;
    public $redis;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Promotions $promotion, $product)
    {
        $this->promotion = $promotion;
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $bonuses = unserialize($this->promotion->bonuses);
        $discount_value = $bonuses[0]['discount_value'];
        $coupon_hash = json_decode($this->promotion->conditions_hash, true);
        $coupon_code = $coupon_hash['coupon_code'];
        $user = Users::find(intval($coupon_hash['users']));
        $images = $this->product['images'];
        $image_url = $this->getProductImage($images);
        $host = 'https://www.nguyenkim.com/';
        return $this->view('coupons')
            ->subject('Chúc mừng đấu sĩ '.$user->firstname.' đã chiến thắng "ĐẤU TRƯỜNG NGƯỢC GIÁ"')
            ->with([
                'UserName'=> $user->firstname,
                'CouponCode' => $coupon_code,
                'WinnerDate' => date('d/m/Y'),
                'Value' => number_format(intval($discount_value),0,'.',','),
                'ProductName' => $this->product['product_name'],
                'product_descriptions' => $this->product['product_descriptions']['short_description'],
                'product_id' => $this->product['product_id'],
                'image_url' => $image_url,
                'url' => $host.$this->product['url_name'].'.html'
            ]);
    }
    private function getProductImage($images)
    {
        $imgURL = '';
        foreach ($images as $img){
            $file_name = $img['image_path'];
            $detail = floor(intval($img['image_id'])/1000);
            $imgURL = 'https://cdn.nguyenkimmall.com/images/thumbnails/300/300/detailed/'.$detail.'/'.$file_name;

            if(@getimagesize($imgURL)){
                break;
            }
        }
        return $imgURL;
    }
}