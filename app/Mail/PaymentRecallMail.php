<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 23/03/2018
 * Time: 3:29 PM
 */

namespace App\Mail;

use App\Models\Mongo\PaymentRecall;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class PaymentRecallMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $urlRecall;

    /**
     * Create a new message instance.
     *
     * @param PaymentRecall $payment
     * @param string        $urlRecall
     */
    public function __construct(PaymentRecall $payment, $urlRecall)
    {
        $this->data      = $payment;
        $this->urlRecall = $urlRecall;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $images = $this->fn_get_option_images($this->data->ProductID);

        return $this->view('payment_recall')
                    ->subject('Chúc mừng đã đăng ký cọc ' . $this->data->Product)
                    ->with(
                        [
                            'token'         => $this->data->_id,
                            'title'         => $this->data->Product,
                            'urlRecall'     => $this->urlRecall,
                            'total'         => number_format($this->data->Total, 0, ',', '.'),
                            'option_images' => $images,
                        ]
                    );
    }

    protected function fn_get_option_images($product_id){
        /* Get options */
        $options = DB::connection('mysql')->table('cscart_product_options as a')
            ->join("cscart_product_options_descriptions AS b", function ($join) {
                $join->on('b.option_id', '=', 'a.option_id')->where("b.lang_code","=","vi");
            })
            ->join("cscart_product_global_option_links AS c", 'c.option_id', '=', 'a.option_id')
            ->select('c.product_id AS cur_product_id', 'a.*', 'b.option_name', 'b.option_text', 'b.description', 'b.inner_hint', 'b.incorrect_message', 'b.comment', 'b.nk_popup_title', 'b.nk_popup_content')
            ->where('c.product_id',$product_id) ->where('a.product_id',0)->where('status','A')
            ->get()->keyBy('option_id')->toArray()/*->transform(function($i) {
                return (array)$i;
            })*/;

        /* Get variants */
        $variants = DB::connection('mysql')->table('cscart_product_option_variants as a')
            ->join("cscart_product_option_variants_descriptions AS b", function ($join) {
                $join->on('b.variant_id', '=', 'a.variant_id')->where("b.lang_code","=","vi");
            })
            ->select('a.variant_id','a.option_id','a.position','a.modifier','a.modifier_type','a.weight_modifier','a.weight_modifier_type','b.variant_name')
            ->whereIn('a.option_id',array_keys($options))
            ->orderBy('a.position','asc')
            ->orderBy('a.variant_id','asc')
            ->get()->keyBy('variant_id')->toArray();
        /* icons */
        $object_type = 'variant_image_desktop';
        $icons = DB::connection('mysql')->table('cscart_images_links as a')
            ->join("cscart_images AS b", 'a.image_id', '=', 'b.image_id')
            ->join("cscart_common_descriptions AS c", function ($join) {
                $join->on('c.object_id', '=', 'b.image_id')->where("c.object_holder","=",'images')->where('c.lang_code','=','vi');
            })
            ->select('a.*', 'b.image_path', 'c.description AS alt', 'b.image_x', 'b.image_y', 'b.image_id AS images_image_id')
            ->where('a.object_type',$object_type)->where('a.type','V')
            ->whereIn('a.object_id',array_keys($variants))
            ->orderBy('a.position','asc')
            ->orderBy('a.pair_id','asc')
            ->get()->toArray();

        foreach ($icons as &$icon) {
            $image_id = !empty($icon->images_image_id) ? $icon->images_image_id : $icon->image_id;
            $path = $object_type . '/' . floor($image_id / 1000);
            $icon->image_path = 'https://cdn.nguyenkimmall.com/images/'.$path.'/'.$icon->image_path;
            //$icon->image_path = 'https://test.nguyenkimonline.com/images/'.$path.'/'.$icon->image_path;
        }


        return $icons;
    }
}