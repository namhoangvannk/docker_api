<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 23/03/2018
 * Time: 3:29 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use App\Models\Mongo\Big4uCoupon as MBig4uCoupon;

class Big4uCoupon extends Mailable
{
    use Queueable, SerializesModels;
    public $coupon;
    protected $config;

    /**
     * Create a new message instance.
     *
     * @param MBig4uCoupon $coupon
     */
    public function __construct(MBig4uCoupon $coupon)
    {
        $this->coupon = $coupon;
        $this->config = Config::get('campaign.big4u');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $new = substr($this->coupon->CustomerPhone, 0, -4) . 'XXXX';
        $utm_source = $this->config['utm_source'];
        $utm_medium = $this->config['utm_medium'];
        $utm_campaign = $this->config['utm_campaign'];
        return $this->view('sancoupon')
            ->subject('Chúc mừng quý khách '.$this->coupon->CustomerName.' đã nhận coupon giảm giá của Nguyễn Kim')
            ->with([
                'CustomerName' => $this->coupon->CustomerName,
                'CouponCode' => $this->coupon->CouponCode,
                'ExpireDate' => date('d/m/Y', strtotime($this->coupon->ExpireDate)),
                'CustomerPhone' => $new,
                'utm' => "utm_source=$utm_source,utm_medium=$utm_medium,utm_campaign=$utm_campaign"
            ]);
    }
}