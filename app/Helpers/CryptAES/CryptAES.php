<?php

namespace App\Helpers\CryptAES;

class CryptAES
{
    private $key = "QlHCwjNzch34zmATK6KgdLu0MzQ970T2";
    private $iv = "T5nvPg0UHn2GH5ER";
    private $type = "aes-256-cbc";
    public function __construct()
    {

    }

    /*
     * Decrypt value to a cryptojs compatiable json encoding string
     *
     * @param mixed $string
     * @return string
     * **/
    function cryptoJsAesDecrypt($string){
        $data = openssl_decrypt(base64_decode($string), $this->type, $this->key, true, $this->iv);
        return json_decode($data, true);
    }

    /**
     * Encrypt value to a cryptojs compatiable json encoding string
     * @param mixed $value
     * @return string
     */
    function cryptoJsAesEncrypt($value){
        $encrypted_data = openssl_encrypt(json_encode($value),$this->type, $this->key, true, $this->iv);
        return base64_encode($encrypted_data);
    }

}
