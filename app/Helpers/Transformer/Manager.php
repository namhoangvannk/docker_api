<?php

namespace App\Helpers\Transformer;

use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\Item as FractalItem;
use League\Fractal\Resource\Collection as FractalCollection;
use App\Helpers\Fractal\ArraySerializer;

class Manager
{
    private $fractalManager;
    
    public function __construct()
    {
        $this->fractalManager = new \League\Fractal\Manager();
        $this->fractalManager->setSerializer(new ArraySerializer());

        if (isset($_GET['include'])) {
            $this->fractalManager->parseIncludes($_GET['include']);
        }
    }

    public function transform($item, $transformer)
    {
        return $this->fractalManager
            ->createData(new FractalItem($item, $transformer))
            ->toArray();
    }

    public function transformCollection($collection, $transformer, $page = null)
    {
        $data = $this->fractalManager
            ->createData(new FractalCollection($collection, $transformer))
            ->toArray();

        return $data;
    }
}
