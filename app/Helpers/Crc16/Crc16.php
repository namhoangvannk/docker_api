<?php

namespace App\Helpers\Crc16;

class Crc16
{
    public function __construct()
    {

    }

    public function stringToBytes($input)
    {
        $length = 0;
        $data = [];

        if (strlen($input) < 2)
            return null;

        str_replace(" ", "", $input);
        str_replace("-", "", $input);
        str_replace("$", "", $input);
        str_replace("h", "", $input);
        str_replace("0x", "", $input);
        str_replace("{", "", $input);
        str_replace("}", "", $input);
        str_replace("<", "", $input);
        str_replace(">", "", $input);

        $length = strlen($input) / 2;

        for ($i = 0; $i < $length; $i++) {
            $int = intval(substr($input, $i * 2, 2), 16);
            $data[$i] = $int;
        }

        return $data;
    }

    public function CrcFFFF($data)
    {
        $crc = 0xFFFF;
        for ($i = 0; $i < count($data); $i++) {
            $crc ^= ($data[$i] << 8);
            for ($j = 0; $j < 8; $j++) {
                if ( ($crc & 0x8000) != 0)
                    $crc = ($crc << 1) ^ 0x1021;
                else
                    $crc = $crc << 1;
            }
        }
        return $crc & 0xFFFF;
    }

    public function convertAsciiToHex($ascii)
    {
        $hex = '';
        for ($i = 0; $i < strlen($ascii); $i++) {
            $byte = strtoupper(dechex(ord($ascii {
                $i})));
            $byte = str_repeat('0', 2 - strlen($byte)) . $byte;
            $hex .= $byte . "";
        }
        return $hex;
    }
}
