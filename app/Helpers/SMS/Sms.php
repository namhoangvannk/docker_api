<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 23/03/2018
 * Time: 11:39 AM
 */
namespace App\Helpers\SMS;

use App\Helpers\SMS\Sender as Sender;

class Sms
{
    protected $SMS_ENABLE = true;
    protected $fillable = [
        'phone',
        'text',
        'external_user',
        'external_identifier',
        'complete_cb'
    ];

    protected $attributes = [
        'external_user' => null,
        'external_identifier' => null,
        'complete_cb' => null
    ];

    protected function getFillable(){
        return $this->fillable;
    }

    protected function getAttributes(){
        return $this->attributes;
    }

    protected function fillableFromArray(array $attributes)
    {
        if (count($this->getAttributes()) > 0) {
            return array_intersect_key($attributes, array_flip($this->getFillable()));
        }

        return $attributes;
    }

    function fill($attributes = array()){

        foreach ($this->fillableFromArray($attributes) as $key => $value) {
            if(in_array($key,$this->getFillable())){
                $this->attributes[$key] = $value;
            }
        }

        return $this;
    }

    function fn_send_sms() {
        extract($this->getAttributes());
        if (empty($phone)||empty($text)||$this->SMS_ENABLE!=true) {
            return false;
        }
        $obj = Sender::getInstance();
        $result = $sender->send($phone,$text,$external_user,$external_identifier,$complete_cb);
        return $result;
    }

    function fn_send_sms_async() {
        extract($this->getAttributes());
        if (empty($phone)||empty($text)||$this->SMS_ENABLE!=true) {
            return false;
        }
        $obj = Sender::getInstance();
        $result = $obj->send_async($phone,$text,$external_user,$external_identifier,$complete_cb);
        return $result;
    }

}