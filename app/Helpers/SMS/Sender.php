<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 23/03/2018
 * Time: 8:49 AM
 */
namespace App\Helpers\SMS;

class Sender
{
    protected $SMS_API_ENDPOINT = 'http://event.nguyenkim.com/mobile/notificationgw/public/api/v2/sms';
    protected $SMS_API_TOKEN = 'Gj6k$L92%szM3t7a@K';

    private static $_instance = null;

    private function __construct() { // private constructor to prevent creating new object

    }

    public static function getInstance() {
        if (empty(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private $_sms_queue = array(); // Hang doi cac sms gui theo dang bat dong bo (async)

    /**
     * Send an SMS immediately
     *
     * @param $phone
     * @param $text
     * @param null $external_user
     * @param null $external_identifier
     * @return bool|mixed
     */
    public function send($phone,$text,$external_user=null,$external_identifier=null) {
        $result = $this->_send($phone,$text,$external_user=null,$external_identifier=null);
        return $result;
    }

    /**
     * Send an SMS async
     *
     * @param $phone
     * @param $text
     * @param null $external_user
     * @param null $external_identifier
     * @param null $complete_cb
     * @return string
     */
    public function send_async($phone,$text,$external_user=null,$external_identifier=null,$complete_cb=null) {
        $sms = func_get_args(); // build sms object from input
        $key = md5(implode($sms)); // key for sms in sms_queue
        $this->_sms_queue[$key] = $sms; // push sms to sms_queue
        return $key; // return index of sms in sms_queue
    }

    public function cancel($sms_key) {
        if (isset($this->_sms_queue[$sms_key])) {
            unset($this->_sms_queue[$sms_key]);
            return true;
        } else {
            return false;
        }
    }

    private function _send($phone,$text,$external_user=null,$external_identifier=null) {
        try {
            $data = array(
                'phone'=>$phone,
                'text'=>$text
            );
            if (isset($external_user)) {
                $data['external_user'] = $external_user;
            }
            if (isset($external_identifier)) {
                $data['external_identifier'] = $external_identifier;
            }
            $client = new \GuzzleHttp\Client();
            //Sending application/x-www-form-urlencoded POST requests
            $response = $client->request('POST', $this->SMS_API_ENDPOINT, [
                'form_params' => $data,
                'headers' => [
                    'User-Agent'    => 'NK API',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer '.$this->SMS_API_TOKEN
                ]
            ]);
            $body = $response->getBody();
            $data = $body->getContents();
            $sms = json_decode($data,true);
            return $sms;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function __destruct() {
        /// Send sms in sms_queue. This method will be call after (PHP) script execution finishes
        while (count($this->_sms_queue)>0) {
            $sms = array_shift($this->_sms_queue); // pop sms from queue
            list($phone,$text,$external_user,$external_identifier,$complete_cb) = $sms; // extract sms information
            $result = $this->_send($phone,$text,$external_user,$external_identifier); // send sms to sms gateway
            if (is_callable($complete_cb)) {
                call_user_func($complete_cb,$result);
            }
        }
    }
}