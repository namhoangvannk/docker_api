<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 29/01/2018
 * Time: 1:18 AM
 */

namespace App\Helpers\ZaloPay;


class ZaloPayBase
{
    protected $params;

    public function __construct($params = array())
    {
        $this->setParams($params);
    }

    public function __toString()
    {
        return json_encode(get_object_vars ( $this ));
    }

    public function toArray()
    {
        return get_object_vars ( $this );
    }

    public function toString()
    {
        return (string) ( $this );
    }


    public function setParam($key, $value){
        if(array_key_exists($key,$this->params)){
            $this->params[$key] = $value;
        }
    }

    public function setParams($params) {
        foreach ($params as $key => $value){
            $this->setParam($key,$value);
        }
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($key){
        if(array_key_exists($key,$this->params)){
            return $this->params[$key];
        }
        return false;
    }
}