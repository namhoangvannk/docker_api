<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 26/01/2018
 * Time: 9:50 AM
 */

namespace App\Helpers\ZaloPay;

class ZaloPayOrder extends ZaloPayBase
{
    protected $params = array(
        'appid' => '', //appid của đơn hàng
        'appuser' => '', //appuser của đơn hàng
        'apptime' => '', //apptime của đơn hàng  (unix timestamp)
        'amount' => '', //số tiền ứng dụng nhận được
        'apptransid' => '', //apptransid của đơn hàng ('yymmdd' + order_id)
        'embeddata' => '', //embeddata của đơn hàng
        'item' => '', //item của đơn hàng
        'description' => '',
        'mac' => '', //chuỗi hmac xác thực cho đơn hàng
    );

    public function setMac($key)
    {
        $mac = $this->generateHMAC($key);
        $this->setParam('mac',$mac);
        return $mac;
    }

    /**
     * Tạo dữ liệu xác thực cho đơn hàng
     * phuongthucxacthucdulieu: HmacSHA256
     * hmacinput: appid +”|”+ apptransid +”|”+ appuser +”|”+ amount +"|" + apptime +”|”+ embeddata +"|" +item
     * mac = HMAC(phuongthucxacthucdulieu, key1, hmacinput)
     */
    public function generateHMAC($key) {
        $arr_content = ['appid','apptransid','appuser','amount','apptime','embeddata','item'];
        $content = array();
        foreach ( $arr_content as $item){
            $content[] = $this->getParam($item);
        }
        $hmac_input = implode("|",$content);
        return hash_hmac("sha256", $hmac_input, $key, false);
    }
}