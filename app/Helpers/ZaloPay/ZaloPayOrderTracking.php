<?php

namespace App\Helpers\ZaloPay;


class ZaloPayOrderTracking extends ZaloPayBase
{
    protected $params = array(
        'appid' => '', //appid của đơn hàng
        'apptransid' => '', //apptransid của đơn hàng
        'mac' => '', //chuỗi hmac xác thực cho đơn hàng
    );

    public function setMac($key)
    {
        $mac = $this->generateHMAC($key);
        $this->setParam('mac',$mac);
        return $mac;
    }

    /**
     * mac = HMAC(phuongthucxacthucdulieu, key1, appid+"|"+apptransid+"|"+key1)
     * @param $key
     */
    public function generateHMAC($key) {
        $appid = $this->getParam('appid');
        $apptransid = $this->getParam('apptransid');
        $hmac_input = implode("|",[$appid,$apptransid,$key]);
        return hash_hmac("sha256", $hmac_input, $key, false);
    }
}