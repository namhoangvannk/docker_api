<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 29/01/2018
 * Time: 1:15 AM
 */

namespace App\Helpers\ZaloPay;


class ZaloPayOrderCallBack extends ZaloPayBase
{
    protected $params = array(
        'data' => '', //dữ liệu giao dịch ZaloPay gọi về cho ứng dụng
        'mac' => '' //HMAC(phuongthucxacthucdulieu, key2, data)
    );

    public function checkMac($key)
    {
        $mac = $this->generateHMAC($key);
        return $mac == $this->getParam('mac')?true:false;
    }

    public function generateHMAC($key) {
        return hash_hmac("sha256", json_encode($this->getParam('data')), $key, false);
    }

}