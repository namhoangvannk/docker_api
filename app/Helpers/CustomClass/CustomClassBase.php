<?php
/**
 * Created by PhpStorm.
 * User: Thu.VuDinh
 * Date: 29/01/2018
 * Time: 1:18 AM
 */

namespace App\Helpers\CustomClass;


Abstract class CustomClassBase
{
    protected $required;
    protected $params;
    protected $errors = [];

    public function __construct($params = array())
    {
        $this->setParams($params);
    }

    public function __toString()
    {
        return json_encode(get_object_vars ( $this ));
    }

    public function toArray()
    {
        return get_object_vars ( $this );
    }

    public function toString()
    {
        return (string) ( $this );
    }

    public function setErrors($error){
        array_push($this->errors,$error);
    }

    public function getErrors()
    {
        $errors = [];
        foreach ($this->errors as $error){
            $errors[] = $error[0];
        }
        if(count($errors) == 1){
            return $errors[0];
        }
        return $errors;
    }

    public function validate(){
        if(!empty($this->required)){
            foreach ($this->required as $key){
                if(empty($this->getParam($key))){
                    $this->setErrors([$key => "$key is required"]);
                }
            }
        }
    }

    public function isValidate()
    {
        $this->validate();
        if(!empty($this->errors)){
            return false;
        }
        return true;
    }

    public function setParam($key, $value){
        if(array_key_exists($key,$this->params)){
            $this->params[$key] = $value;
        }
    }

    public function setParams($params) {
        foreach ($params as $key => $value){
            $this->setParam($key,$value);
        }
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($key){
        if(array_key_exists($key,$this->params)){
            return $this->params[$key];
        }
        return false;
    }
}