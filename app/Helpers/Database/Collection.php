<?php

namespace App\Helpers\Database;

use App\Component\PagingComponent;
use DB;

class Collection extends \Illuminate\Support\Collection
{
    /**
     * The table associated with the collection.
     *
     * @var string
     */
    protected $table;
    /**
     * Db instance.
     */
    protected $db;
    /**
     * @var int
     */
    protected $limit = 20;
    /**
     * @var int
     */
    protected $offset = 0;
    /**
     * @var array
     */
    protected $filters = [];
    /**
     * @var array
     */
    protected $sorts = [];

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->db = DB::table($this->table);
    }

    /**
     * Limit setter.
     *
     * @param int
     */
    public function setLimit($limit)
    {
        $limit = intval($limit);
        if ($limit > 0) {
            $this->limit = $limit;
        }

        return $this;
    }

    /**
     * Offset setter.
     *
     * @param int
     */
    public function setOffset($offset)
    {
        $offset = intval($offset);
        if ($offset >= 0) {
            $this->offset = $offset;
        }

        return $this;
    }

    /**
     * Filter by a attribute.
     *
     * @param varchar
     * @param mixed
     */
    public function filterBy($attribute, $value)
    {
        $this->filters[$attribute] = $value;

        return $this;
    }

    /**
     * Sort by a attribute.
     *
     * @param varchar
     * @param mixed
     */
    public function orderBy($attribute, $direction)
    {
        $this->sorts[$attribute] = $direction;

        return $this;
    }

    /**
     * Load data from db query.
     *
     * @return array
     */
    protected function load()
    {
        // Filtering
        if (count($this->filters)) {
            foreach ($this->filters as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        // Ordering
        if (count($this->sorts)) {
            foreach ($this->sorts as $field => $direction) {
                $this->db->orderBy($field, $direction);
            }
        }

        return $this;
    }

    /**
     * Retrieve items from db.
     *
     * @return array
     */
    public function getItems()
    {
        $this->load();
        $db = $this->db;

        return $db
            ->limit($this->limit)
            ->offset($this->offset)
            ->get();
    }

    /**
     * Retrieve items from db.
     *
     * @return array
     */
    public function getTotal()
    {
        return $this->db->limit(PHP_INT_MAX)->offset(0)->count();
    }

    /**
     * Retrieve items from db.
     *
     * @return array
     */
    public function getPaging()
    {
        $limit = intval($this->limit) > 0 ? intval($this->limit) : 20;
        $offset = intval($this->offset);
        $paging = new PagingComponent($this->getTotal(), $offset, $limit);

        return $paging->render();
    }

    /**
     * Retunr Database.
     * @return DB
     */
    public function getDb()
    {
        return $this->db;
    }
}
