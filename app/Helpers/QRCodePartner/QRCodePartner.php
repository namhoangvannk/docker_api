<?php

namespace App\Helpers\QRCodePartner;

use App\Helpers\Crc16\Crc16;
use Monolog\Handler\NullHandler;

class QRCodePartner
{
    protected $crc16;
    protected $mcc;
    protected $visa_id;
    protected $master_qr_id;
    protected $bank_qr_id;
    protected $merchant_name;
    protected $merchant_city;
    protected $bill_number;
    protected $store_id;
    protected $fee_indicator;
    protected $fee_fixed;
    protected $fee_percentage;

    public function __construct(Crc16 $crc16)
    {
        $this->crc16 = $crc16;
        $this->mcc = "5732";

        $this->visa_id = "4110611026916687";
        $this->master_qr_id = "5263391026916688";
        $this->bank_qr_id = "2112991026916685";
        $this->merchant_name = "NGUYEN KIM ONLINE QR";
        $this->merchant_city = "HO CHI MINH";
        $this->bill_number = null;
        $this->store_id = "ONLINE";
        $this->fee_indicator = 0;
        $this->fee_fixed = 0;
        $this->fee_percentage = 0;
    }

    /**
     * 
     */
    public function getQRContentSTB($params)
    {
        $qrContent = "";
	
	    // 00: Payload Format Indicator 
        $qrContent = sprintf("00%s%s", "02", "01"); 
	
        // 01: Point of initiation method  
        $transactionAmount = intval($params['transaction_amount']);
        if ($transactionAmount > 0) { // QR dynamic
            $qrContent .= sprintf("01%s%s", "02", "12");
        } else {// QR Static
            $qrContent .= sprintf("01%s%s", "02", "11");
        }
	
        // 02: Merchant identifier (as defined by network) 
        // $visaId =  $params['visa_id'];
        $visaId = $this->visa_id;
        $qrContent .= sprintf("02%02s%s", strlen($visaId), $visaId);
	
	    // 03: future

        // 04 - 07: Other network definitions
        // $massterQrId = $params['master_qr_id'];
        $massterQrId = $this->master_qr_id;
        $qrContent .= sprintf("04%02s%s", strlen($massterQrId) - 1, substr($massterQrId, 0, strlen($massterQrId) - 1));
	
	    // 08-51: future

        // 50: 
        // $bankQrId = $params['bank_qr_id'];
        $bankQrId = $this->bank_qr_id;
        if (!empty($bankQrId)) {
            $subTag00 = "SACOMBANKQR";
            $crcText = $subTag00 . trim($bankQrId);

            $sacombankQRSTag = sprintf("00%02s%s", strlen($subTag00), $subTag00);
            $sacombankQRSTag .= sprintf("01%02s%s", strlen($bankQrId), $bankQrId);
		
		    // Calc CRC for tag 50
            $crcTextHex = $this->crc16->convertAsciiToHex($crcText);
            $tag50CRC = $this->crc16->stringToBytes($crcTextHex);
            $sacombank_CCITT_0XFFFF = $this->crc16->CrcFFFF($tag50CRC);

            $sacombankQRSTag .= sprintf("9904%s", sprintf('%04X', $sacombank_CCITT_0XFFFF));

            $qrContent .= sprintf("50%02s%s", strlen($sacombankQRSTag), $sacombankQRSTag);
        }
	
	    // 52 : MCC (Merchant Category Code) default: mcc
        $qrContent .= sprintf("52%s%s", "04", $this->mcc);
	
	    // 53: Transaction Currency Code 
        $qrContent .= sprintf("53%s%s", "03", "704");
	
        // 54: Transaction Amount
        if ($transactionAmount > 0) {
            $qrContent .= sprintf("54%02s%s", strlen($transactionAmount), $transactionAmount);
        }

        // 55,56,57 : options
        // $feePercentage = $params['fee_percentage'];
        $feePercentage = $this->fee_percentage;
        $qrContent .= sprintf("57%02s%s", strlen($feePercentage), $feePercentage);
	
	    // 58: Country Code 
        $qrContent .= sprintf("58%02s%s", "02", "VN"); // default
	
        // 59: Merchant Name – Doing Business As Name
        // $merchantName = $params['merchant_name'];
        $merchantName = $this->merchant_name;
        if (strlen($merchantName) > 25) {
            $qrContent .= sprintf("59%02s%s", "25", substr($merchantName, 0, 25));
        } else {
            $qrContent .= sprintf("59%02s%s", strlen($merchantName), trim($merchantName));
        }
	
        // 60: Merchant city – Doing Business As Name
        // $merchantCity = $params['merchant_city'];
        $merchantCity = $this->merchant_city;
        if (strlen($merchantCity) > 15) {
            $qrContent .= sprintf("60%02s%s", "15", substr($merchantCity, 0, 15));
        } else {
            $qrContent .= sprintf("60%02s%s", strlen($merchantCity), trim($merchantCity));
        }
	
	    // 61: option
	
        // 62: Additional Data Field
        $billNumber = $params['order_id'];
        // $billNumber = $this->bill_number;
        // $storeID = $params['store_id'];
        $storeID = $this->store_id;
        $additionalData = $this->additionalDataSBT($billNumber, $storeID);
        if (!empty($additionalData) && strlen($additionalData) < 100) {
            $qrContent .= sprintf("62%02s%s", strlen($additionalData), trim($additionalData));
        }
	
	    // 63: CRC
        $input = $qrContent . "6304";
        $qrContentHex = $this->crc16->convertAsciiToHex($input);
        $qrContentByte = $this->crc16->stringToBytes($qrContentHex);
        $qrContentCrc16 = $this->crc16->CrcFFFF($qrContentByte);
        $qrContent .= sprintf("6304%s", sprintf('%04X', $qrContentCrc16));

        return $qrContent;
    }

    private function additionalDataSBT($billNumber, $storeID)
    {
        $additionalData = "";

        if (!empty($billNumber))
            $additionalData .= sprintf("01%02s%s", strlen($billNumber), trim($billNumber));

        if (!empty($storeID))
            $additionalData .= sprintf("03%02s%s", strlen($storeID), trim($storeID));

        return $additionalData;
    }

}
