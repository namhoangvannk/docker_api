<?php
namespace App\Helpers\QRCodePartner;

use App\Helpers\QRCode\QRCode;
use App\Models\Mongo\OrderQrCode;

require_once __DIR__ . '/../ExTwigmo/Core/rsa/rsa.php';

//test
define('SACOMBANK_USER_NAME', '1a58a26b-d200-47ce-bfa2-397ca15cf0f7');
define('SACOMBANK_PASSWORD', '9c42e6c069b3de62');
define('SACOMBANK_URL_API', 'https://cardtest.sacombank.com.vn:9443/epay/stb');
define('SACOMBANK_MID', '000000060103298');
define('SACOMBANK_TID', '60006744');
define('SACOMBANK_USE', 'test002');
define('SACOMBANK_Return_Format', 'BASE64');

/*//PRO
define('SACOMBANK_USER_NAME', '1a58a26b-d200-47ce-bfa2-397ca15cf0f7');
define('SACOMBANK_PASSWORD', '9c42e6c069b3de62');
define('SACOMBANK_URL_API', 'https://cardtest.sacombank.com.vn:9443/epay/stb');
define('SACOMBANK_MID', '000000060103298');
define('SACOMBANK_TID', '60006744');
define('SACOMBANK_USE', 'test002');
define('SACOMBANK_Return_Format', 'BASE64');*/


class QRCodeSacombank
{

    public function GUID()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf(
            '%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535)
        );
    }

    public function GetDatetime()
    {
       return gmdate('Y-m-d\TH:i:s\Z');
    }

    public function createSignature($dataSign)
    {
        $md5_string = strtoupper(md5($dataSign));
        $pkeyid     = openssl_pkey_get_private(OrderQrCode::PRIVATE_KEY_SACOMBANK);
        openssl_sign($md5_string, $signature, $pkeyid);

        return base64_encode($signature);
    }

    public function encrypt($data, $key)
    {
        $key         = $key . substr($key, 0, 8);
        $tripleKey   = substr($key, 0, mcrypt_get_key_size('tripledes', 'ecb'));
        $blocksize   = mcrypt_get_block_size('tripledes', 'ecb');
        $paddingSize = $blocksize - (strlen($data) % $blocksize);
        $data        .= str_repeat(chr($paddingSize), $paddingSize);
        $encodedText = mcrypt_encrypt('tripledes', $tripleKey, $data, 'ecb');

        return base64_encode($encodedText);
    }

    public function decrypt($data, $key)
    {
        //Generate a key from a hash
        $key         = $key . substr($key, 0, 8);

        $data = base64_decode($data);

        $data = mcrypt_decrypt('tripledes', $key, $data, 'ecb');

        $len = strlen($data);
        $pad = ord($data[$len-1]);

        return substr($data, 0, strlen($data) - $pad);
    }

    public function CreateRefNo()
    {
        for ($randomNumber = mt_rand(1, 9), $i = 1; $i < 10; $i++) {
            $randomNumber .= mt_rand(0, 9);
        }

        return "NK$randomNumber";
    }

    public function fn_api_gen_qrCode($setting, $json = [], $headers_ext = null)
    {
        $arrRequest['Data']            = $this->encrypt(json_encode($json, JSON_UNESCAPED_SLASHES), SACOMBANK_PASSWORD);
        $arrRequest['FunctionName']    = 'PartnerQRGeneration';
        $arrRequest['RequestDateTime'] = $this->GetDatetime();
        $arrRequest['RequestID']       = $this->GUID();
        $data_post                     = json_encode($arrRequest, JSON_UNESCAPED_SLASHES);
        $signature = $this->createSignature($data_post);

        $headers   = [
            'Content-Type: application/json',
            'Signature: ' . $signature,
            'Accept: application/json',
            'Content-Length: ' . strlen($data_post),
            'Authorization: Basic MWE1OGEyNmItZDIwMC00N2NlLWJmYTItMzk3Y2ExNWNmMGY3OjljNDJlNmMwNjliM2RlNjI=',
        ];

        if (null != $headers_ext) {
            $headers = $headers_ext;
        }

        $ch = curl_init();

        $apiUrl = $setting["url"];
        if (isset($setting['method']) && $setting['method'] == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, isset($setting["ssl"]) ? $setting["ssl"] : false);
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        //if($result===false) echo curl_error($ch);
        return json_decode($result);
    }
}
