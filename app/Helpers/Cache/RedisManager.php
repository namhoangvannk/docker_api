<?php

namespace App\Helpers\Cache;

use Redis;
// use Illuminate\Support\Facades\Redis;

class RedisManager
{
	protected $client;

	public function __construct()
    {
		$this->client = new \Redis();
		$host = env('REDIS_HOST', 'localhost');
		$port = env('REDIS_PORT', 6379);
		$password = env('REDIS_PASSWORD', null);
		$this->client->connect($host, $port);
		$this->client->auth($password);
	}
	
	 public function setValue($key, $value, $time = 0, $database = 0, $is_encode = true)
    {
        $time = intval($time);
        if ($is_encode)
            $data = base64_encode(gzcompress(serialize($value)));
        else
            $data = $value;
        $this->client->select($database);

        if ($time > 0) {
            $v_seted = $this->client->set($key, $data, $time);
        } else {
            $v_seted = $this->client->set($key, $data);
        }

        return $v_seted;
    }
	
	 public function getValue($key, $database = 0, $is_encode = true)
    {
        $this->client->select($database);
        $v_value = $this->client->get($key);
        if($is_encode)
            $data = unserialize(@gzuncompress(base64_decode($v_value)));
        else
            $data = $v_value;
        return $data;
    }
	
	public function delKey($key) 
	{
		return $this->client->del($key);
	}
    public function delKeyDB($key, $database = 0)
    {
        $this->client->select($database);
        return $this->client->del($key);
    }

    //
    public function existsKey($key, $database = 0){
        $this->client->select($database);
        return ($this->client->exists($key))? true : false;
    }

    //
    public function setExpire($key, $database = 0, $ttl){
        if($this->existsKey($key,$database)){
            $this->client->expire($key, $ttl);
            return true;
        }
        return false;
    }
}
