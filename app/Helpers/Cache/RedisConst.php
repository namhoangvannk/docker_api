<?php

namespace App\Helpers\Cache;

class RedisConst
{
    const DATA_TF_CATEGORIES = 'data_tf_categories';
    const DATA_TF_COUNTRIES = 'data_tf_countries';
    const DATA_TF_PROVINCES = 'data_tf_provinces';
    const DATA_TF_DISTRICTS = 'data_tf_districts';
    const DATA_TF_WARDS = 'data_tf_wards';
    const DATA_TF_MODULES = 'data_tf_modules';

}
