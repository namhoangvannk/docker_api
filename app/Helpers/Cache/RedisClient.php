<?php

namespace App\Helpers\Cache;

use Illuminate\Support\Facades\Redis;

class RedisClient
{
    protected $redis;
    protected $host;
    protected $pass;

    public function __construct()
    {
    }

    public static function setValue($key, $value, $time = 0, $database = 0, $is_encode = true)
    {
        $redis = new \Predis\Client('103.9.76.104:6379');
        $redis->auth('NKr3D1SnK5o01z');
        $time = intval($time);
        if ($is_encode)
            $data = base64_encode(gzcompress(serialize($value)));
        else
            $data = $value;
        if ($time > 0) {
            $redis->set($key,$data);
            $redis->expire($key, $time);
        } else {
            $redis->set($key,$data);
        }
    }
    public static function getValue($key, $database = 0, $is_encode = true)
    {
        $redis = new \Predis\Client('103.9.76.104:6379');
        $redis->auth('NKr3D1SnK5o01z');
        $v_value = $redis->get($key);
        if($is_encode)
            $data = unserialize(@gzuncompress(base64_decode($v_value)));
        else
            $data = $v_value;
        return $data;
    }
    public static function delKey($key)
    {
        $redis = new \Predis\Client('103.9.76.104:6379');
        $redis->auth('NKr3D1SnK5o01z');
        $redis->del($key);
    }
    public static function existsKey($key)
    {
        $redis = new \Predis\Client('103.9.76.104:6379');
        $redis->auth('NKr3D1SnK5o01z');
        if($redis->exists($key)){
            return true;
        }
        return false;
    }
}
