<?php

namespace App\Helpers\Query;

class Query
{
    const OPERATORS = [
        'eq'      => '=',
        'neq'     => '!=',
        'gt'      => '>',
        'gte'     => '>=',
        'lt'      => '<',
        'lte'     => '<=',
        'like'    => 'LIKE',
        'nlike'   => 'NOT LIKE',
        'in'      => 'IN',
        'nin'     => 'NOT IN',
        'is'      => 'IS',
        'notnull' => 'IS NOT NULL',
        'null'    => 'IS NULL',
        'from'    => '>=',
        'to'      => '<=',
    ];

    /**
     * Check if operator is valid.
     *
     * @param string $operator
     *
     * @return bool
     */
    public static function isValidOperator($operator)
    {
        return in_array($operator, array_keys(self::OPERATORS));
    }

    /**
     * Retrieve method for query builder.
     *
     * @param $operator
     *
     * @return bool|string
     */
    public static function getOperator($operator)
    {
        $operators = self::OPERATORS;
        if (self::isValidOperator($operator)) {
            return $operators[$operator];
        }

        return false;
    }

    /**
     * Retrieve query condition from single query string.
     *
     * @param $column
     * @param $conditionFromQueryString
     *
     * @return array|bool
     */
    public static function parseQueryCondition($column, $conditionFromQueryString)
    {
        if (in_array($conditionFromQueryString, ['null', 'notnull'])) {
            $operator = $conditionFromQueryString;
            $conditionValue = null;
        } else {
            $queries = explode('|', $conditionFromQueryString);
            if (count($queries) === 1) {
                $conditionValue = $conditionFromQueryString;
                $operator = 'eq';
            } else {
                $operator = $queries[0];
                $conditionValue = $queries[1];
            }
        }
        if (self::getOperator($operator)) {
            return [
                $column,
                self::getOperator($operator),
                $conditionValue,
            ];
        } else {
            return false;
        }
    }

    /**
     * Retrieve array of query conditions can be parsed for column.
     *
     * @param string       $column
     * @param string|array $value
     *
     * @return array
     */
    public static function getQueryConditions($column, $value)
    {
        $queryConditions = [];
        if (is_array($value)) {
            foreach ($value as $condition) {
                $queryConditions[] = self::parseQueryCondition($column, $condition);
            }
        } else {
            $queryConditions[] = self::parseQueryCondition($column, $value);
        }

        return $queryConditions;
    }

    /**
     * Convert from camelCase string to under_score.
     *
     * @param string $string
     *
     * @return string
     * @codeCoverageIgnore
     */
    public static function convertCamelToUnderscore($string)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $string));
    }
}
