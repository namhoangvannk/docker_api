<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;

class Helpers
{
    public function get_browser() 
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Internet_Explorer';
            $ub = "MSIE";
        }
        elseif(preg_match('/Firefox/i',$u_agent))
        {
            $bname = 'Mozilla_Firefox';
            $ub = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$u_agent))
        {
            $bname = 'Google_Chrome';
            $ub = "Chrome";
        }
        elseif(preg_match('/Safari/i',$u_agent))
        {
            $bname = 'Apple_Safari';
            $ub = "Safari";
        }
        elseif(preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Opera';
            $ub = "Opera";
        }
        elseif(preg_match('/Netscape/i',$u_agent))
        {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }

        // check if we have a number
        if ($version==null || $version=="") {$version="?";}

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    }

    public function _url_text($string, $ext = '.html')
    {
	    // remove all characters that aren"t a-z, 0-9, dash, underscore or space
        $string = strip_tags(str_replace('&nbsp;', ' ', $string));
        $string = str_replace('&quot;', '', $string);

        //$string = $func($string);
        //$string = $this->_utf8_to_ascii($string);

        $NOT_acceptable_characters_regex = '#[^-a-zA-Z0-9_ ]#';
        $string = preg_replace($NOT_acceptable_characters_regex, '', $string);
	    // remove all leading and trailing spaces
        $string = trim($string); 
        
        // change all dashes, underscores and spaces to dashes
        $string = preg_replace('#[-_]+#', '-', $string);
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('#[-]+#', '-', $string);

        return strtolower($string . $ext);
    }

    public function _utf8_to_ascii($str)
    {
        $chars = array(
            'a' => array('ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ă', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Â', 'Ă'),
            'e' => array('ế', 'ề', 'ể', 'ễ', 'ệ', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê'),
            'i' => array('í', 'ì', 'ỉ', 'ĩ', 'ị', 'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'),
            'o' => array('ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'Ố', 'Ồ', 'Ổ', 'Ô', 'Ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ơ', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ơ'),
            'u' => array('ứ', 'ừ', 'ử', 'ử', 'ữ', 'ự', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư'),
            'y' => array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'),
            'd' => array('đ', 'Đ'),
        );
        foreach ($chars as $key => $arr) {
            $str = str_replace($arr, $key, $str);
        }
        return $str;
    }

    public function createImageFromBase64($base64encode, $path)
    {
        $data = $base64encode;
        $name = 'img_' . time() . '.png';
        $orgpath = env('UPLOAD_FOLDER') . $path;
        self::_mk_dirs($orgpath);
        $file_name = $orgpath . '/' . $name;
        $data = str_replace('data:image/png;base64,', '', $data);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        $file = $file_name;
        $success = file_put_contents($file, $data);
        return $path . '/' . $name;

        // $file_name = 'img_' . time() . '.png';
        // $orgpath = self::_mk_dirs(env('UPLOAD_FOLDER') . $path);
        // $desctination = $orgpath .'/'. $file_name;

        // @list($type, $file_data) = explode(';', $base64encode);
        // @list(, $file_data) = explode(',', $base64encode);
        // if ($file_data != "") {
        //     Storage::disk('public')->put($desctination, base64_decode($file_data));
        // }
        // return $path. '/' . $file_name;
    }

    public function is_base64_encoded($string)
    {
        $string = explode(',', $string);
        
        if (isset($string[1]) &&  base64_decode($string[1], true)) {
            return true;
        } else {
            return false;
        }
    }

    public static function _mk_dirs($strPath)
    {
        if (is_dir($strPath)) {
            return true;
        }

        $pStrPath = dirname($strPath);
        if (!self::_mk_dirs($pStrPath)) {
            return false;
        }

        $return = mkdir($strPath);
        @chmod($strPath, 0777);
        return $return;
    }

    public static function uploadCV($file)
    {
        $extension = array("pdf", "doc", "docx");
        $file_extension = $file->getClientOriginalExtension();
        if (!in_array(mb_strtolower($file_extension), $extension)) {
            return null;
        }
        $destination = 'uploads/cv/' . Date('Ymd') . '/' . Date('H');
        $path = $file->storePubliclyAs($destination, $file->getClientOriginalName());
        return [
            'public_url' => env('BASE_CDN_URL') . $path,
            'path' => $path
        ];
    }

    public static function calTotalTime($start_date, $end_date)
    {
        $start = strtotime($start_date);
        $end = strtotime($end_date);
        $totaltime = ($end - $start);

        $hours = intval($totaltime / 3600);
        $seconds_remain = ($totaltime - ($hours * 3600));

        $minutes = intval($seconds_remain / 60);
        $seconds = ($seconds_remain - ($minutes * 60)); 
        return sprintf("%02dH : %02dM : %02dS", $hours, $minutes, $seconds);
    }

    public function mappingOrderStatus($status_code)
    {
        $status_id = 0;
        switch($status_code) {
            case 'I':
                $status_id = 2;
                break;
            case 'C':
                $status_id = 1;
                break;
        }

        return $status_id;
    }

    public function generateUID($prefix,$length = 8)
    {
        $uuid = "";
        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        return $prefix.substr($charid, 0, $length);
        // $uuid = substr($charid, 0, 8) . $hyphen
        //     . substr($charid, 8, 4) . $hyphen
        //     . substr($charid, 12, 4) . $hyphen
        //     . substr($charid, 16, 4) . $hyphen
        //     . substr($charid, 20, 12);
        // return $uuid;
    }

    public function isValidEmail($email) {
        $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
        $rs = preg_match($pattern, $email);

        if($rs)
            return true;

        return false;
    }

    public function generateSalt($length = 10)
    {
        $length = $length > 10 ? 10 : $length;

        $salt = '';

        for ($i = 0; $i < $length; $i++) {
            $salt .= chr(rand(33, 126));
        }

        return $salt;
    }

    public function generatePass($password, $salt)
    {
        $_pass = '';

        if (empty($salt)) {
            $_pass = md5($password);
        } else {
            $_pass = md5(md5($password) . md5($salt));
        }

        return $_pass;
    }

    public function isValidPhone($phone = ''){
        $phone = preg_replace('/[\s\.\-\(\)]+/', '', strval($phone));
        if (preg_match("/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/", $phone)){
            return true;
        }
        return false;
    }

    function findBetween($string, $start, $end, $trim = true, $greedy = false) {
        $pattern = '/' . preg_quote($start) . '(.*';
        if (!$greedy) $pattern .= '?';
        $pattern .= ')' . preg_quote($end) . '/';
        preg_match($pattern, $string, $matches);
        $string = $matches[0];
        if ($trim) {
            $string = substr($string, strlen($start));
            $string = substr($string, 0, -strlen($start) + 1);
        }
        return $string;
    }
}
