<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorldcupTeams extends Migration
{
    /**
     * The name of the database connection to use.
     *
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('worldcup_teams', function (Blueprint $collection) {
            $collection->unique(array( 'TeamID'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('worldcup_teams', function (Blueprint $collection)
        {
            $collection->drop();
        });
    }
}
