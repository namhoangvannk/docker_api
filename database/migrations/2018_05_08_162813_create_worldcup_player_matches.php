<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorldcupPlayerMatches extends Migration
{
    /**
     * The name of the database connection to use.
     *
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('worldcup_player_matches', function (Blueprint $collection) {
            $collection->index(array( 'PlayerID', 'MatchID' ));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('worldcup_player_matches', function (Blueprint $collection)
        {
            $collection->drop();
        });
    }
}
