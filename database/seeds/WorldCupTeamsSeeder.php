<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorldCupTeamsSeeder extends Seeder
{
    /**
     * The name of the database connection to use.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = array(
            ['TeamID' => 'RUS','Name' => 'russia','Description' => 'Nga','Group' => 'A','Class' => 'sen','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'ARA','Name' => 'saudi_arabia','Description' => 'Saudi Arabia','Group' => 'A','Class' => 'ksa','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'URU','Name' => 'uruquay','Description' => 'Uruguay','Group' => 'A','Class' => 'tun','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'EGY','Name' => 'egypt','Description' => 'Ai Cập','Group' => 'A','Class' => 'egy','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'MOR','Name' => 'morocco','Description' => 'Morocco','Group' => 'B','Class' => 'mar','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'IRA','Name' => 'iran','Description' => 'Iran','Group' => 'B','Class' => 'irn','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'POR','Name' => 'portugal','Description' => 'Bồ Đào Nha','Group' => 'B','Class' => 'por','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'SPA','Name' => 'spain','Description' => 'Tây Ban Nha','Group' => 'B','Class' => 'fra','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'FRA','Name' => 'franch','Description' => 'Pháp','Group' => 'C','Class' => 'esp','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'PER','Name' => 'peru','Description' => 'Peru','Group' => 'C','Class' => 'per','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'DEN','Name' => 'denmark','Description' => 'Đan Mạch','Group' => 'C','Class' => 'den','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'AUS','Name' => 'australia','Description' => 'Australia','Group' => 'C','Class' => 'aus','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'ARG','Name' => 'argentina','Description' => 'Argentina','Group' => 'D','Class' => 'arg','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'CRO','Name' => 'croatia','Description' => 'Croatia','Group' => 'D','Class' => 'cro','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'ICE','Name' => 'iceland','Description' => 'Iceland','Group' => 'D','Class' => 'isl','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'NIG','Name' => 'nigeria','Description' => 'Nigeria','Group' => 'D','Class' => 'pan','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'BRA','Name' => 'brazil','Description' => 'Brazil','Group' => 'E','Class' => 'bra','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'SWI','Name' => 'switzerland','Description' => 'Thụy Sỹ','Group' => 'E','Class' => 'sui','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'COS','Name' => 'costa_rica','Description' => 'Costa Rica','Group' => 'E','Class' => 'crc','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'SER','Name' => 'serbia','Description' => 'Serbia','Group' => 'E','Class' => 'srb','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'GER','Name' => 'germany','Description' => 'Đức','Group' => 'F','Class' => 'ger','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'MEX','Name' => 'mexico','Description' => 'Mexico','Group' => 'F','Class' => 'mex','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'SWE','Name' => 'sweden','Description' => 'Thụy Điển','Group' => 'F','Class' => 'swe','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'KOR','Name' => 'korea_republic','Description' => 'Hàn Quốc','Group' => 'F','Class' => 'jpn','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'BEL','Name' => 'belgium','Description' => 'Bỉ','Group' => 'G','Class' => 'rus','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'PAN','Name' => 'panama','Description' => 'Panama','Group' => 'G','Class' => 'nga','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'TUN','Name' => 'tunisia','Description' => 'Tunisia','Group' => 'G','Class' => 'uru','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'ENG','Name' => 'england','Description' => 'Anh','Group' => 'G','Class' => 'eng','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'POL','Name' => 'poland','Description' => 'Ba Lan','Group' => 'H','Class' => 'pol','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'COL','Name' => 'colombia','Description' => 'Colombia','Group' => 'H','Class' => 'col','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'SEN','Name' => 'senegal','Description' => 'Senegal','Group' => 'H','Class' => 'rus','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,],
            ['TeamID' => 'JAP','Name' => 'japan','Description' => 'Nhật Bản','Group' => 'H','Class' => 'kor','Matches' => 0,'Win' => 0,'Draw' => 0,'Loss' => 0,'Points' => 0,]
        );
        foreach ($teams as $team) {
            \App\Models\Mongo\WorldCupTeams::create($team);
        }
    }
}
