<?php
return [
    'list_credit' => array(
        0 => array(
            'img' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T01/tragop/FE2x.png',
            'alt' => 'Trả góp FE credit',
        ),
        1 => array(
            'img' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T01/tragop/Home2x.png',
            'alt' => 'Trả góp Home Credit',
        ),
        2 => array(
            'img' => 'https://cdn.nguyenkimmall.com/images/companies/_1/2020/01/aeon_4x.png',
            'alt' => 'Trả góp ACS Credit',
        ),
        3 => array(
            'img' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T01/tragop/hd2x.png',
            'alt' => 'Trả góp HDSaiSon',
        ),
    ),
    'payment_image_url' => 'https://www.nguyenkim.com/design/themes/responsive/media/images/',
    'partner_code_momo' => 'MOMOIQA420180417',
    'public_key_rsa' => "-----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkpa+qMXS6O11x7jBGo9W3yxeHEsAdyDE40UoXhoQf9K6attSIclTZMEGfq6gmJm2BogVJtPkjvri5/j9mBntA8qKMzzanSQaBEbr8FyByHnf226dsLt1RbJSMLjCd3UC1n0Yq8KKvfHhvmvVbGcWfpgfo7iQTVmL0r1eQxzgnSq31EL1yYNMuaZjpHmQuT24Hmxl9W9enRtJyVTUhwKhtjOSOsR03sMnsckpFT9pn1/V9BE2Kf3rFGqc6JukXkqK6ZW9mtmGLSq3K+JRRq2w8PVmcbcvTr/adW4EL2yc1qk9Ec4HtiDhtSYd6/ov8xLVkKAQjLVt7Ex3/agRPfPrNwIDAQAB
-----END PUBLIC KEY-----",
    'payments' => array(
        89 => array(),
        59 => array
        (
            0 => array
            (
                'title' => 'SacomBank',
                'image' => 'bank/tragop/sacombank.png',
                'value' => 'SACOMBANK',
            ),

            1 => array
            (
                'title' => 'TechcomBank',
                'image' => 'bank/tragop/techcombank.png',
                'value' => 'TECHCOMBANK',
            ),

            2 => array
            (
                'title' => 'VIB',
                'image' => 'bank/tragop/vib.png',
                'value' => 'VIB',
            ),

            3 => array
            (
                'title' => 'CitiBank',
                'image' => 'bank/tragop/citibank.png',
                'value' => 'CITIBANK',
            ),

            4 => array
            (
                'title' => 'HSBC',
                'image' => 'bank/tragop/hsbc.png',
                'value' => 'HSBC',
            ),

            5 => array
            (
                'title' => 'ShinhanBank',
                'image' => 'bank/tragop/shinhabank.png',
                'value' => 'SHINHABANK',
            ),

            6 => array
            (
                'title' => 'MaritimeBank',
                'image' => 'bank/tragop/maritimebank.png',
                'value' => 'MARITIMEBANK',
            ),

            7 => array
            (
                'title' => 'Standard Chartered',
                'image' => 'bank/tragop/standard.png',
                'value' => 'STANDARD CHARTERED',
            ),

            8 => array
            (
                'title' => 'VPBank',
                'image' => 'bank/tragop/vpbank.png',
                'value' => 'VPBANK',
            ),

            9 => array
            (
                'title' => 'TMCP',
                'image' => 'bank/tragop/tmcp.jpg',
                'value' => 'TMCP',
            ),

            10 => array
            (
                'title' => 'TPBANK',
                'image' => 'bank/tragop/tpbank.jpg',
                'value' => 'TPBANK',
            ),
            11 => array
            (
                'title' => 'EXIMBANK',
                'image' => 'bank/tragop/eximbank.jpg',
                'value' => 'EXIMBANK',
            ),
        ),
        81 => array(),
        6 => array(),
        18 => array(
            1 => array(
                'title' => 'Ngoại thương Việt Nam',
                'image' => 'bank/banknews/vcb.png',
                'id' => '123PVCB',
                'value' => '123PVCB',
            ),
            2 => array(
                'title' => 'TMCP Kỹ thương Việt Nam',
                'image' => 'bank/banknews/tcb.png',
                'id' => '123PTCB',
                'value' => '123PTCB',
            ),
            3 => array(
                'title' => 'TMCP Công thương Việt Nam',
                'image' => 'bank/banknews/vtb.png',
                'id' => '123PVTB',
                'value' => '123PVTB',
            ),
            4 => array(
                'title' => 'TMCP Xuất nhập khẩu',
                'image' => 'bank/banknews/exb.png',
                'id' => '123PEIB',
                'value' => '123PEIB',
            ),
            5 => array(
                'title' => 'TMCP Đông Á',
                'image' => 'bank/banknews/dab.png',
                'id' => '123PDAB',
                'value' => '123PDAB',
            ),
            6 => array(
                'title' => 'TMCP Quốc tế',
                'image' => 'bank/banknews/vib.png',
                'id' => '123PVIB',
                'value' => '123PVIB',
            ),
            7 => array(
                'title' => 'TMCP Quân Đội',
                'image' => 'bank/banknews/mb.png',
                'id' => '123PMB',
                'value' => '123PMB',
            ),
            8 => array(
                'title' => 'TMCP Á Châu',
                'image' => 'bank/banknews/acb.png',
                'id' => '123PACB',
                'value' => '123PACB',
            ),
            9 => array(
                'title' => 'Phát triển nhà tp Hồ Chí Minh',
                'image' => 'bank/banknews/hdb.png',
                'id' => '123PHDB',
                'value' => '123PHDB',
            ),
            10 => array(
                'title' => 'TMCP Sài Gòn Thương tín',
                'image' => 'bank/banknews/scb.png',
                'id' => '123PSCB',
                'value' => '123PSCB',
            ),
            11 => array(
                'title' => 'Quốc Dân',
                'image' => 'bank/banknews/nvb.png',
                'id' => '123PNVB',
                'value' => '123PNVB',
            ),
            12 => array(
                'title' => 'TMCP Hàng hải Việt nam',
                'image' => 'bank/banknews/mtb.png',
                'id' => '123PMRTB',
                'value' => '123PMRTB',
            ),
            13 => array(
                'title' => 'TMCP Việt Á',
                'image' => 'bank/banknews/vab.png',
                'id' => '123PVAB',
                'value' => '123PVAB',
            ),
            14 => array(
                'title' => 'TMCP Việt Nam Thịnh Vượng',
                'image' => 'bank/banknews/vpb.png',
                'id' => '123PVPB',
                'value' => '123PVPB',
            ),

            15 => array(
                'title' => 'Thương mại TNHH Một Thành Viên Dầu khí Toàn cầu',
                'image' => 'bank/banknews/gpb.png',
                'id' => '123PGPB',
                'value' => '123PGPB',
            ),

            16 => array(
                'title' => 'TMCP một thành viên Đại Dương',
                'image' => 'bank/banknews/oceanb.png',
                'id' => '123POCEB',
                'value' => '123POCEB',
            ),
            17 => array(
                'title' => 'TMCP Phương Đông',
                'image' => 'bank/banknews/ocb.png',
                'id' => '123POCB',
                'value' => '123POCB',
            ),
            18 => array(
                'title' => 'Đầu tư và Phát triển Việt Nam',
                'image' => 'bank/banknews/bidv.png',
                'id' => '123PBIDV',
                'value' => '123PBIDV',
            ),
            19 => array(
                'title' => 'Nông nghiệp và Phát triển Nông thôn',
                'image' => 'bank/banknews/agb.png',
                'id' => '123PAGB',
                'value' => '123PAGB',
            ),
            20 => array(
                'title' => 'TMCP Nam Á',
                'image' => 'bank/banknews/nab.png',
                'id' => '123PNAB',
                'value' => '123PNAB',
            ),
            21 => array(
                'title' => 'TMCP Sài Gòn Công thương',
                'image' => 'bank/banknews/sgb.png',
                'id' => '123PSGB',
                'value' => '123PSGB',
            ),
            22 => array(
                'title' => 'TMCP Xăng dầu Petrolimex',
                'image' => 'bank/banknews/pgb.png',
                'id' => '123PPGB',
                'value' => '123PPGB',
            ),
            23 => array(
                'title' => 'TMCP An Bình',
                'image' => 'bank/banknews/abb.png',
                'id' => '123PABB',
                'value' => '123PABB',
            ),
            24 => array(
                'title' => 'TMCP Bưu  Điện Liên Việt',
                'image' => 'bank/banknews/lienvietbank.png',
                'id' => '123PLPB',
                'value' => '123PLPB',
            ),
            25 => array(
                'title' => 'TMCP Đông Nam Á',
                'image' => 'bank/banknews/seabank.png',
                'id' => '123PSEAB',
                'value' => '123PSEAB',
            ),
            26 => array(
                'title' => 'TMCP Bảo Việt',
                'image' => 'bank/banknews/baovietbank.png',
                'id' => '123PBVB',
                'value' => '123PBVB',
            ),
            27 => array(
                'title' => 'TMCP Sài Gòn Hà Nội',
                'image' => 'bank/banknews/shb.png',
                'id' => '123PSHB',
                'value' => '123PSHB',
            ),
            28 => array(
                'title' => 'TMCP Bắc Á',
                'image' => 'bank/banknews/bacab.png',
                'id' => '123PBAB',
                'value' => '123PBAB',
            ),
            29 => array(
                'title' => 'TMCP Tiên Phong',
                'image' => 'bank/banknews/tienphongb.png',
                'id' => '123PTPB',
                'value' => '123PTPB',
            ),
        ),
    ),
    'dir_image_credit_card' => 'https://www.nguyenkim.com/design/themes/responsive/media/images/bank/tragop/',
    'image_credit_card' => array(
        0 => array(
            'img' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T01/tragop/Master.png',
            'alt' => 'Thanh toán mastercard',
        ),
        1 => array(
            'img' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T01/tragop/Visa.png',
            'alt' => 'Thanh toán Visa',
        ),
        2 => array(
            'img' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T01/tragop/JCB.png',
            'alt' => 'Thanh toán JCB',
        ),
    ),

    'payment_credit' => json_encode([
        "SACOMBANK" => [
            "chuyendoi" => [
                6 => 1,
                12 => 2.6,
            ],
            //"hotline" => "1900 5555 88",
            "label" => "Ngân hàng Sacombank",
            "payment_txt" => 'Sacombank sẽ thu phí chuyển đổi trả góp 1% /giao dịch cho kỳ hạn 6 tháng, hoặc 2,6%/giao dịch cho kỳ hạn 12 tháng',
            "logo" => "sacombank.png",
            "cards_prefix" => [
                472074, 472075, 486265, 436438, 455376, 466243, 512341, 526830, 552332,
                461137, 461140, 356480, 625002, 356062, 356481,
            ],
        ],
        "TECHCOMBANK" => [
            "chuyendoi" => [
                3 => 1.1,
                6 => 1.1,
            ],
            "hotline" => "1800 588 822",
            "phitoithieu" => "150000",
            "label" => "Ngân hàng TechcomBank",
            "payment_txt" => 'Techcombank sẽ thu phí chuyển đổi trả góp 1.1%/giao dịch, tối thiểu 150.000đ/giao dịch Nhân viên Techcombank sẽ liên hệ với khách hàng để xác nhận thông tin chuyển đổi trả góp',
            "logo" => "techcombank.png",
            "cards_prefix" => [
                457353, 422076, 422071, 422075, 356394, 488971, 457453,
            ],
        ],
        "VIB" => [
            "chuyendoi" => [
                3 => 0,
                6 => 0,
                9 => 0,
                12 => 0,
            ],
            "hotline" => "1800 8180",
            "ngaychuyendoi" => 5,
            "label" => "Ngân hàng VIB",
            "payment_txt" => 'Khách hàng gọi tổng đài 1800 8180 để đăng ký chuyển đổi trả góp trong vòng 5 ngày kể từ ngày thực hiện giao dịch',
            "logo" => "vib.png",
            "cards_prefix" => [
                512824, 526887, 513892, 513094,
            ],
        ],
        "CITIBANK" => [
            "chuyendoi" => [
                6 => 0,
            ],
            "hotline" => "08 35211111",
            "label" => "Ngân hàng CITIBANK",
            "payment_txt" => 'Khách hàng soạn tin nhắn "0" gửi 6058 hoặc  truy cập <a target="_blank"  class="bank_link" href="https://www.citibank.com.vn/Landing_Pages/vietnamese/PayLite/application-form.htm?lid=VNVNCBLPECATLApplicationFormVLIT">https://www.citibank.com.vn/Landing_Pages/vietnamese/PayLite/application-form.htm?lid=VNVNCBLPECATLApplicationFormVLIT</a> để đăng ký chuyển đổi trả góp',
            "logo" => "citibank.png",
            "cards_prefix" => [
                533948, 437374,
            ],
        ],
        "HSBC" => [
            "chuyendoi" => [
                6 => 0,
            ],
            //"hotline" => "(028) 37 247 247 (miền Nam) <font color='#898989'>và</font> (024) 62 707 707 (miền Bắc)",
            "ngaychuyendoi" => 5,
            "label" => "Ngân hàng HSBC",
            "logo" => "hsbc.png",
            "cards_prefix" => [
                546022, 437841, 445094, 445093,
            ],
        ],
        "SHINHABANK" => [
            "chuyendoi" => [
                6 => 0,
                12 => 0,
            ],
            //"hotline" => "1800 1560 <font color='#898989'>hoặc</font> (028) 3829 1566",
            "ngaychuyendoi" => 5,
            "label" => "Ngân hàng SHINHABANK",
            "logo" => "shinhabank.png",
            "cards_prefix" => [
                469673, 450255, 469672, 403013,
            ],
        ],
        "MARITIMEBANK" => [
            "chuyendoi" => [
                6 => 1,
            ],
            "hotline" => "1800 59 9999 <font color='#898989'>hoặc</font> 024 39 44 55 66",
            "label" => "Ngân hàng MARITIMEBank",
            "payment_txt" => 'MaritimeBank sẽ thu phí chuyển đổi trả góp 1%/giao dịch. Khách hàng gọi tổng đài 1800 59 9999 hoặc (024) 39 44 55 66 để đăng ký chuyển đổi trong vòng 7 ngày kể từ ngày thực hiện giao dịch',
            "logo" => "maritimebank.png",
            "cards_prefix" => [
                532451, 516294, 430389,
            ],
        ],
        "STANDARD CHARTERED" => [
            "chuyendoi" => [
                6 => 0,
            ],
            //"hotline" => "(028) 3911 0000 <font color='#898989'>hoặc</font> (024) 3696 0000",
            "ngaychuyendoi" => 5,
            "label" => "Ngân hàng STANDARD CHARTERED",
            "logo" => "standard.png",
            "cards_prefix" => [
                516101, 526181,
            ],
        ],
        "VPBANK" => [
            "chuyendoi" => [
                6 => 1.99,
            ],
            //"hotline" => "1900545415",
            "ngaychuyendoi" => 4,
            "label" => "Ngân hàng VPBank",
            "payment_txt" => 'VPBank sẽ thu phí chuyển đổi trả góp 1.99%/giao dịch. Khách hàng gọi tổng đài 1900 54 54 15 để đăng ký chuyển đổi trả góp trong vòng 4 ngày kể từ ngày thực hiện giao dịch',
            "logo" => "vpbank.png",
            "cards_prefix" => [
                52039930, 52439430, 52439468, 52397579, 52397568, 52439466, 52439469, 52039967, 52439499, 52397599, 52039999, 51896601, 51896602,
            ],
        ],
        "TMCP" => [
            "chuyendoi" => [
                6 => 0,
                9 => 0,
                12 => 0,
            ],
//        "hotline" => "1900 6538",
            "label" => "Ngân hàng TMCP",
            "logo" => "tmcp.jpg",
            "cards_prefix" => [
                554627, 545579, 510235, 489518, 489517, 489516,
            ],
        ],
        "TPBANK" => [
            "chuyendoi" => [
                6 => 0,
            ],
            //"hotline" => "1900 58 58 85",
            "ngaychuyendoi" => 5,
            "label" => "Ngân hàng TPBank",
            "logo" => "tpbank.jpg",
            "cards_prefix" => [
                466582, 466583, 470970, 536287,
            ],
        ],
        "EXIMBANK" => [
            "chuyendoi" => [
                6 => 0,
            ],
            //"hotline" => "1800 1199",
            "ngaychuyendoi" => 5,
            "label" => "Ngân hàng Eximbank",
            "logo" => "eximbank.jpg",
            "cards_prefix" => [
                423960, 404152, 469655, 418159, 436308, 548370, 542853, 536302, 527559, 356680, 356681, 356513,
            ],
        ],
        "FECREDIT" => [
            "chuyendoi" => [
                3 => 0,
                6 => 0,
                9 => 0,
                12 => 0,
            ],
            //"hotline" => "1800 1199",
            "ngaychuyendoi" => 5,
            "label" => "Thẻ tín dụng FE CREDIT",
            "logo" => "fecredit.png",
            "cards_prefix" => [
                539146, 539597,
            ],
        ],
        "SEABANK" => [
            "chuyendoi" => [
                3 => 0,
                6 => 0,
            ],
            // "hotline" => "1900 555 587",
            //"hotline" => "",
            //        "ngaychuyendoi" => 5,
            "label" => "Ngân hàng Đông Nam Á",
            "logo" => "seabank.png",
            "cards_prefix" => [
                523611, 436545, 436546, 476636,
            ],
        ],
    ])
    ,
    'menu_config' => array(
        '0' => array(
            'icon' => '\E91A',
            'data' => array(
                '0' => array(
                    'id' => 480,
                    'name' => 'Điện thoại',
                    'alias' => 'dien-thoai-di-dong',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 528,
                    'name' => 'Tablet',
                    'alias' => 'may-tinh-bang',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '2' => array(
                    'id' => 483,
                    'name' => 'Phụ kiện',
                    'alias' => 'phu-kien-dien-thoai',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '1' => array(
            'icon' => '\E900',
            'data' => array(
                '0' => array(
                    'id' => 350,
                    'name' => 'Tivi',
                    'alias' => 'tivi-man-hinh-lcd',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 2148,
                    'name' => 'Loa',
                    'alias' => 'amply-va-loa',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '2' => array(
                    'id' => 352,
                    'name' => 'Âm thanh',
                    'alias' => 'dan-may-nghe-nhac',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '2' => array(
            'icon' => '\E901',
            'data' => array(
                '0' => array(
                    'id' => 423,
                    'name' => 'Tủ lạnh',
                    'alias' => 'tu-lanh',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 424,
                    'name' => 'Tủ đông',
                    'alias' => 'tu-dong',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '3' => array(
            'icon' => '\E905',
            'data' => array(
                '0' => array(
                    'id' => 420,
                    'name' => 'Máy lạnh',
                    'alias' => 'may-lanh',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 427,
                    'name' => 'Máy lọc không khí',
                    'alias' => 'may-loc-khong-khi',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '4' => array(
            'icon' => '\E903',
            'data' => array(
                '0' => array(
                    'id' => 421,
                    'name' => 'Máy giặt',
                    'alias' => 'may-giat',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 422,
                    'name' => 'Máy sấy',
                    'alias' => 'may-say-quan-ao',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '5' => array(
            'icon' => '\E913',
            'data' => array(
                '0' => array(
                    'id' => 344,
                    'name' => 'Đồ gia dụng',
                    'alias' => 'gia-dung',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '6' => array(
            'icon' => '\E90B',
            'data' => array(
                '0' => array(
                    'id' => 527,
                    'name' => 'Laptop',
                    'alias' => 'may-tinh-xach-tay',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 529,
                    'name' => 'PC',
                    'alias' => 'may-tinh-de-ban',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '2' => array(
                    'id' => 2896,
                    'name' => 'Phụ kiện',
                    'alias' => 'phu-kien-tin-hoc',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '7' => array(
            'icon' => '\E910',
            'data' => array(
                '0' => array(
                    'id' => 343,
                    'name' => 'Máy ảnh',
                    'alias' => 'may-anh-mirrorless',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 641,
                    'name' => 'Máy Quay',
                    'alias' => 'may-quay-phim',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '2' => array(
                    'id' => 2894,
                    'name' => 'Phụ kiện',
                    'alias' => 'phu-kien-ky-thuat-so',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '8' => array(
            'icon' => '\E949',
            'data' => array(
                '0' => array(
                    'id' => 531,
                    'name' => 'Máy in',
                    'alias' => 'may-in-van-phong',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 2434,
                    'name' => 'Thiết bị văn phòng',
                    'alias' => 'thiet-bi-van-phong',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '9' => array(
            'icon' => '\E908',
            'data' => array(
                '0' => array(
                    'id' => 1080,
                    'name' => 'Công cụ',
                    'alias' => 'dien-co',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '1' => array(
                    'id' => 1168,
                    'name' => 'Bách hóa',
                    'alias' => 'bach-hoa',
                    'subcats' => '',
                    'features_hash' => '',
                ),
                '2' => array(
                    'id' => 347,
                    'name' => 'Làm đẹp',
                    'alias' => 'suc-khoe-va-lam-dep',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
        '10' => array(
            'icon' => '\E94A',
            'data' => array(
                '0' => array(
                    'id' => 3049,
                    'name' => 'Sim số - Thẻ cào - Thu hộ',
                    'alias' => 'sim-so-dep.html',
                    'subcats' => '',
                    'features_hash' => '',
                ),
            ),
        ),
    ),
    'category_floors_v2' => array(

        '1' => array(
            'id' => 2700,
            'color' => '#F00',
            'header' => [
                'name' => 'Bếp sạch nhà sang - khang trang năm mới',
                'alias' => 'dien-thoai-di-dong',
            ],
            'position_display' => 6,
            'position_floor' => [1000000],
            'banner_block_id' => [],
            'products' => [83256,81289,70876,85790,76228,76268,70286,86470,81480,20017,61483,64664,63971,81138,48776,63099,75952,49182,14989,63659],
            'products_view_more' => [83256,81289,70876,85790,76228,76268,70286,86470,81480,20017,61483,64664,63971,81138,48776,63099,75952,49182,14989,63659],
            'footer' => [
                '0' => [
                    'id' => 527,
                    'name' => 'Xem tất cả Laptop',
                    'alias' => 'may-tinh-xach-tay',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 537,
                    'name' => 'DELL',
                    'alias' => 'laptop-dell',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '2' => [
                    'id' => 541,
                    'name' => 'ASUS',
                    'alias' => 'laptop-asus',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 545,
                    'name' => 'Macbook',
                    'alias' => 'laptop-apple-macbook',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 530,
                    'name' => 'LCD',
                    'alias' => 'man-hinh-lcd-vi-tinh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '5' => [
                    'id' => 529,
                    'name' => 'Máy tính bàn',
                    'alias' => 'may-tinh-de-ban',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '6' => [
                    'id' => 2896,
                    'name' => 'Phụ kiện tin học',
                    'alias' => 'phu-kien-may-tinh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
            ]
        ),
        '2' => array(
            'id' => 2706,
            'color' => '#F00',
            'header' => [
                'name' => 'Lên đời đón Tết, kết lộc ngày xuân ',
                'alias' => 'dien-thoai-di-dong',
            ],
            'position_display' => 6,
            'position_floor' => [5,31],
            'banner_block_id' => [],
            'products' => [83256,81289,70876,85790,76228,76268,70286,86470,81480,20017,61483,64664,63971,81138,48776,63099,75952,49182,14989,63659],
            'products_view_more' => [83256,81289,70876,85790,76228,76268,70286,86470,81480,20017,61483,64664,63971,81138,48776,63099,75952,49182,14989,63659],
            'footer' => [
                '0' => [
                    'id' => 527,
                    'name' => 'Xem tất cả Laptop',
                    'alias' => 'may-tinh-xach-tay',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 537,
                    'name' => 'DELL',
                    'alias' => 'laptop-dell',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '2' => [
                    'id' => 541,
                    'name' => 'ASUS',
                    'alias' => 'laptop-asus',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 545,
                    'name' => 'Macbook',
                    'alias' => 'laptop-apple-macbook',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 530,
                    'name' => 'LCD',
                    'alias' => 'man-hinh-lcd-vi-tinh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '5' => [
                    'id' => 529,
                    'name' => 'Máy tính bàn',
                    'alias' => 'may-tinh-de-ban',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '6' => [
                    'id' => 2896,
                    'name' => 'Phụ kiện tin học',
                    'alias' => 'phu-kien-may-tinh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
            ]
        ),
        '3' => array(
            'id' => 2748,
            'color' => '#F00',
            'header' => [
                'name' => 'Vui đón xuân, khuân hàng xịn',
                'alias' => 'dien-thoai-di-dong',
            ],
            'position_display' => 6,
            'position_floor' => [1],
            'banner_block_id' => [],
            'products' => [83256,81289,70876,85790,76228,76268,70286,86470,81480,20017,61483,64664,63971,81138,48776,63099,75952,49182,14989,63659],
            'products_view_more' => [83256,81289,70876,85790,76228,76268,70286,86470,81480,20017,61483,64664,63971,81138,48776,63099,75952,49182,14989,63659],
            'footer' => [
                '0' => [
                    'id' => 527,
                    'name' => 'Xem tất cả Laptop',
                    'alias' => 'may-tinh-xach-tay',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 537,
                    'name' => 'DELL',
                    'alias' => 'laptop-dell',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '2' => [
                    'id' => 541,
                    'name' => 'ASUS',
                    'alias' => 'laptop-asus',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 545,
                    'name' => 'Macbook',
                    'alias' => 'laptop-apple-macbook',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 530,
                    'name' => 'LCD',
                    'alias' => 'man-hinh-lcd-vi-tinh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '5' => [
                    'id' => 529,
                    'name' => 'Máy tính bàn',
                    'alias' => 'may-tinh-de-ban',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '6' => [
                    'id' => 2896,
                    'name' => 'Phụ kiện tin học',
                    'alias' => 'phu-kien-may-tinh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
            ]
        )
    ),
    'category_floors' => array(
        
        '1' => array(
            'id' => 527,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'LAPTOP - PC - PHỤ KIỆN',
                'alias' => 'may-tinh-xach-tay',
            ],
            'position_display' => 18,
            'banner_block_id' => [],
            'products' => [53704, 60347, 65672, 56052],
            
            'footer' => [
                '0' => [
                    'id' => 527,
                    'name' => 'Xem tất cả Laptop',
                    'alias' => 'may-tinh-xach-tay',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 537,
                    'name' => 'DELL',
                    'alias' => 'laptop-dell',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '2' => [
                    'id' => 541,
                    'name' => 'ASUS',
                    'alias' => 'laptop-asus',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 545,
                    'name' => 'Macbook',
                    'alias' => 'laptop-apple-macbook',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 530,
                    'name' => 'LCD',
                    'alias' => 'man-hinh-lcd-vi-tinh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '5' => [
                    'id' => 529,
                    'name' => 'Máy tính bàn',
                    'alias' => 'may-tinh-de-ban',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '6' => [
                    'id' => 2896,
                    'name' => 'Phụ kiện tin học',
                    'alias' => 'phu-kien-may-tinh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
            ]
        ),
        '2' => array(
            'id' => 350,
            'color' => '#f57650',
            'header' => [
                'name' => 'TIVI - LOA - ÂM THANH',
                'alias' => 'tivi-man-hinh-lcd',
            ],
            'position_display' => 2,
            'banner_block_id' => [],
            'products' => [55276, 67451, 53252, 55932],
            'footer' => [
                '0' => [
                    'id' => 350,
                    'name' => 'Xem tất cả Tivi',
                    'alias' => 'tivi-man-hinh-lcd',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 365,
                    'name' => 'Samsung',
                    'alias' => 'tivi-lcd-samsung',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '2' => [
                    'id' => 359,
                    'name' => 'Sony',
                    'alias' => 'tivi-lcd-sony',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 364,
                    'name' => 'LG',
                    'alias' => 'tivi-lcd-lg',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 350,
                    'name' => 'Smart Tivi',
                    'alias' => 'tivi-man-hinh-lcd',
                    'subcats' => 'Y',
                    'features_hash' => '36-86629',
                ],
                '5' => [
                    'id' => 350,
                    'name' => 'Tivi 4K',
                    'alias' => 'tivi-man-hinh-lcd',
                    'subcats' => 'Y',
                    'features_hash' => '77-50422-84042-84752-66082-49350-80652-82834-43821-22015-85666-81884-69824-83210',
                ],
                '6' => [
                    'id' => 352,
                    'name' => 'Dàn máy nghe nhạc',
                    'alias' => 'dan-may-nghe-nhac',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '7' => [
                    'id' => 2148,
                    'name' => 'Amply & Loa',
                    'alias' => 'amply-va-loa',
                    'subcats' => '',
                    'features_hash' => '',
                ],
            ]
        ),
        '4' => array(
            'id' => 350,
            'color' => '#00a1c0',
            'header' => [
                'name' => 'MÁY LẠNH',
                'alias' => 'may-lanh',
            ],
            'position_display' => 16,
            'banner_block_id' => [],
            'products' => [53952, 63925, 63609, 63549],
            'footer' => [
                '0' => [
                    'id' => 350,
                    'name' => 'Xem tất cả Máy lạnh',
                    'alias' => 'may-lanh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 430,
                    'name' => 'Daikin',
                    'alias' => 'may-lanh-daikin',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '2' => [
                    'id' => 433,
                    'name' => 'Panasonic',
                    'alias' => 'may-lanh-panasonic',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 436,
                    'name' => 'Toshiba',
                    'alias' => 'may-lanh-toshiba',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 420,
                    'name' => 'Inverter',
                    'alias' => 'may-lanh',
                    'subcats' => 'Y',
                    'features_hash' => '71-5820',
                ],
            ]
        ),

        '5' => array(
            'id' => 423,
            'color' => '#70c4fd',
            'header' => [
                'name' => 'TỦ LẠNH - TỦ ĐÔNG',
                'alias' => 'tu-lanh',
            ],
            'position_display' => 17,
            'banner_block_id' => [],
            'products' => [71188, 43472, 66492, 47056],
            'footer' => [
                '0' => [
                    'id' => 423,
                    'name' => 'Xem tất cả Tủ lạnh',
                    'alias' => 'tu-lanh',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 423,
                    'name' => 'Tủ lạnh mini',
                    'alias' => 'tu-lanh',
                    'subcats' => 'Y',
                    'features_hash' => '82-35046',
                ],
                '2' => [
                    'id' => 451,
                    'name' => 'Hitachi',
                    'alias' => 'tu-lanh-hitachi',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 456,
                    'name' => 'Toshiba',
                    'alias' => 'tu-lanh-toshiba',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 2463,
                    'name' => 'Panasonic',
                    'alias' => 'tu-lanh-panasonic',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '5' => [
                    'id' => 453,
                    'name' => 'Samsung',
                    'alias' => 'tu-lanh-samsung',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '6' => [
                    'id' => 423,
                    'name' => 'Side by side',
                    'alias' => 'tu-lanh',
                    'subcats' => 'Y',
                    'features_hash' => '82-5872',
                ],
            ]
        ),
        '6' => array(
            'id' => 421,
            'color' => '#57b6b6',
            'header' => [
                'name' => 'MÁY GIẶT - MÁY SẤY',
                'alias' => 'may-giat',
            ],
            'position_display' => 15,
            'banner_block_id' => [],
            'products' => [32509, 32413, 2884, 58197],
            'footer' => [
                '0' => [
                    'id' => 421,
                    'name' => 'Xem tất cả Máy giặt',
                    'alias' => 'may-giat',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 437,
                    'name' => 'Electrolux',
                    'alias' => 'may-giat-electrolux',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '2' => [
                    'id' => 453,
                    'name' => 'LG',
                    'alias' => 'may-giat-lg',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 443,
                    'name' => 'Toshiba',
                    'alias' => 'may-giat-toshiba',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 2461,
                    'name' => 'Panasonic',
                    'alias' => 'may-giat-panasonic',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '5' => [
                    'id' => 441,
                    'name' => 'Samsung',
                    'alias' => 'may-giat-samsung',
                    'subcats' => '',
                    'features_hash' => '',
                ],
            ]
        ),
        '7' => array(
            'id' => 3011,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'MÁY ẢNH - PHỤ KIỆN',
                'alias' => 'may-anh-mirrorless',
            ],
            'position_display' => 21,
            'banner_block_id' => [],
            'products' => [54968, 60853, 52860, 30150],
            'footer' => [
                '0' => [
                    'id' => 3011,
                    'name' => 'Xem tất cả Máy ảnh',
                    'alias' => 'may-anh-mirrorless',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 3041,
                    'name' => 'Canon',
                    'alias' => 'may-anh-mirrorless-canon',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '2' => [
                    'id' => 653,
                    'name' => 'Nikon',
                    'alias' => 'may-anh-chuyen-nghiep-nikon',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 3037,
                    'name' => 'Sony',
                    'alias' => 'may-anh-mirrorless-sony',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 639,
                    'name' => 'Máy ảnh du lịch',
                    'alias' => 'may-anh-ky-thuat-so',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '5' => [
                    'id' => 640,
                    'name' => 'Máy ảnh DSLR',
                    'alias' => 'may-anh-chuyen-nghiep',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '6' => [
                    'id' => 662,
                    'name' => 'Phụ kiện máy ảnh',
                    'alias' => 'phu-kien-ky-thuat-so',
                    'subcats' => '',
                    'features_hash' => '',
                ],
            ]
        ),

        '8' => array(
            'id' => 344,
            'color' => '#b8d733',
            'header' => [
                'name' => 'GIA DỤNG',
                'alias' => 'gia-dung',
            ],
            'position_display' => 3,
            'banner_block_id' => [],
            'products' => [19544, 62549, 58115, 60105],
            'footer' => [
                '0' => [
                    'id' => 344,
                    'name' => 'Xem tất cả Gia dụng',
                    'alias' => 'gia-dung',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '1' => [
                    'id' => 820,
                    'name' => 'Bếp từ',
                    'alias' => 'bep-dien',
                    'subcats' => 'Y',
                    'features_hash' => '168-35069',
                ],
                '2' => [
                    'id' => 719,
                    'name' => 'Máy xay sinh tố',
                    'alias' => 'may-xay-sinh-to',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '3' => [
                    'id' => 2360,
                    'name' => 'Bếp gas',
                    'alias' => 'bep-gas',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '4' => [
                    'id' => 720,
                    'name' => 'Máy ép trái cây',
                    'alias' => 'may-ep-trai-cay',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '5' => [
                    'id' => 818,
                    'name' => 'Nồi cơm',
                    'alias' => 'noi-com-dien',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '6' => [
                    'id' => 717,
                    'name' => 'Máy hút bụi',
                    'alias' => 'may-hut-bui',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '7' => [
                    'id' => 715,
                    'name' => 'Bàn ủi',
                    'alias' => 'ban-ui-ban-la',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '8' => [
                    'id' => 714,
                    'name' => 'Quạt',
                    'alias' => 'quat-dien-quat-may',
                    'subcats' => '',
                    'features_hash' => '',
                ],
                '9' => [
                    'id' => 821,
                    'name' => 'Lò vi sóng',
                    'alias' => 'lo-vi-song-microwave',
                    'subcats' => '',
                    'features_hash' => '',
                ],
            ]
        ),

    ),
    'menu_icon_home' => array(
        '0' => array(
            'id' => 350,
            'name' => 'Tivi',
            'alias' => 'tivi-man-hinh-lcd',
            'icon' => '\E900',
            'subcats' => '',
            'features_hash' => '',
        ),
        '1' => array(
            'id' => 423,
            'name' => 'Tủ lạnh',
            'alias' => 'tu-lanh',
            'icon' => "\E901",
            'subcats' => '',
            'features_hash' => '',
        ),
        '2' => array(
            'id' => 420,
            'name' => 'Máy lạnh',
            'alias' => 'may-lanh',
            'icon' => "\E905",
            'subcats' => '',
            'features_hash' => '',
        ),
        '3' => array(
            'id' => 421,
            'name' => 'Máy giặt',
            'alias' => 'may-giat',
            'icon' => "\E903",
            'subcats' => '',
            'features_hash' => '',
        ),
        '4' => array(
            'id' => 480,
            'name' => 'Điện thoại',
            'alias' => 'dien-thoai-di-dong',
            'icon' => "\E91A",
            'subcats' => '',
            'features_hash' => '',
        ),
        '5' => array(
            'id' => 527,
            'name' => 'Máy Tính',
            'alias' => 'may-tinh-xach-tay',
            'icon' => "\E90B",
            'subcats' => '',
            'features_hash' => '',
        ),
        '6' => array(
            'id' => 344,
            'name' => 'Gia dụng',
            'alias' => 'gia-dung',
            'icon' => "\E913",
            'subcats' => '',
            'features_hash' => '',
        ),
        '7' => array(
            'id' => 531,
            'name' => 'Máy in',
            'alias' => 'may-in-van-phong',
            'icon' => "\E949",
            'subcats' => '',
            'features_hash' => '',
        ),
        '8' => array(
            'id' => 3049,
            'name' => 'Sim số',
            'alias' => 'sim-so-dep.html',
            'icon' => "\E94A",
            'subcats' => '',
            'features_hash' => '',
        ),
    ),
    'menu_icon_home_top' => array(
        '0' => array(
            'id' => 1,
            'name' => 'Tất cả danh mục',
            'alias' => '',
            'icon' => "\EA3D",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#fb6322'
        ),
        '1' => array(
            'id' => 2,
            'name' => 'Khuyến mãi hot',
            'alias' => 'https://www.nguyenkim.com/khuyen-mai.html',
            'icon' => "\E96D",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#26aae3'
        ),
        '2' => array(
            'id' => 3,
            'name' => 'Trả góp 0%',
            'alias' => 'https://www.nguyenkim.com/khuyen-mai-tra-gop-tai-nguyen-kim.html',
            'icon' => "\E953",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#fb5050'
        ),
        '3' => array(
            'id' => 4,
            'name' => 'Tin tức mẹo vặt',
            'alias' => 'https://www.nguyenkim.com/blog.html',
            'icon' => "\E952",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#fcc065'
        ),
        '4' => array(
            'id' => 5,
            'name' => 'Tải App',
            'alias' => 'https://play.google.com/store/apps/details?id=com.nguyenkimapp',
            'icon' => "\EA3C",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#4dd3cb'
        ),

    ),
    'menu_icon_home_v2' => array(
        '0' => array(
            'id' => 350,
            'name' => 'Tivi',
            'alias' => 'tivi-man-hinh-lcd',
            'icon' => '\EA08',
            'subcats' => '',
            'features_hash' => '',
            'color' => '#7c50fb',
            'image' => ''
        ),
        '1' => array(
            'id' => 421,
            'name' => 'Máy giặt',
            'alias' => 'may-giat',
            'icon' => "\E94F",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#89c053',
            'image' => ''
        ),
        '2' => array(
            'id' => 423,
            'name' => 'Tủ lạnh',
            'alias' => 'tu-lanh',
            'icon' => "\Ea0B",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#4a88da',
            'image' => ''
        ),
        '3' => array(
            'id' => 420,
            'name' => 'Máy lạnh',
            'alias' => 'may-lanh',
            'icon' => "\E9E2",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#35ba9b',
            'image' => ''
        ),
        '4' => array(
            'id' => 480,
            'name' => 'Điện thoại',
            'alias' => 'dien-thoai-di-dong',
            'icon' => "\EA10",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#fa6c51',
            'image' => ''
        ),
        '5' => array(
            'id' => 527,
            'name' => 'Máy Tính',
            'alias' => 'may-tinh-xach-tay',
            'icon' => "\E90B",
            'subcats' => '',
            'features_hash' => 'https://chungcuhn24h.net/wp-content/uploads/2017/12/icon-hot.png',
            'color' => '#fa6c51',
            'image' => 'HOT'
        ),
        '6' => array(
            'id' => 344,
            'name' => 'Gia dụng',
            'alias' => 'gia-dung',
            'icon' => "\E913",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#fa6c51',
            'image' => 'HOT'
        ),
        '7' => array(
            'id' => 531,
            'name' => 'Máy in',
            'alias' => 'may-in-van-phong',
            'icon' => "\E949",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#fa6c51',
            'image' => ''
        ),
        '8' => array(
            'id' => 1080,
            'name' => 'Công cụ',
            'alias' => 'dien-',
            'icon' => "\E94A",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#fa6c51',
            'image' => ''
        ),
        '9' => array(
            'id' => 3011,
            'name' => 'Máy ảnh',
            'alias' => 'may-anh',
            'icon' => "\E94A",
            'subcats' => '',
            'features_hash' => '',
            'color' => '#fa6c51',
            'image' => ''
        ),

    ),
    'header_app' => array(
        'background-image' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T5/welcome_wc/bg_wc.svg',
        'logo' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T5/welcome_wc/logo_Wc_mobile%402x_1.png',
        'background-color' => '#d02',
    ),
    'category_info' => array(
        //ok
        '0' => array(
            'id' => 420,
            'color' => '#00a1c0',
            'header' => [
                'name' => 'MÁY LẠNH',
                'alias' => 'may-lanh',
            ],
            'position_display' => 16,
            'premium' => [
                'title' => 'Trải nghiệm các dòng Máy lạnh đời mới 2018',
                'status' => 1,
                'background' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/Bg-Block-Maylanh.jpg',
                'data' => [
                    '0' => [
                        'id' => '67987',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10036934_MAYLANH_REETECH-1-5-HP_RTV12-DE-A.png',
                    ],
                    '1' => [
                        'id' => '64754',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10036064_MAYLANH_REETECH_2-HP_RT18-DE-A_01.png',
                    ],
                    '2' => [
                        'id' => '66951',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10036723_MAYLANH_DAIKIN_INVERTER-1-5-HP_FTKQ35SAVMV_01.png',
                    ],
                    '3' => [
                        'id' => '66963',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10036725_MAYLANH_DAIKIN_ATKC35TAVMV-ARKC35TAVMV_01.png',
                    ],
                    '4' => [
                        'id' => '66879',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10036867_MAYLANH_SHARP_2-HP_AH-X18VEW.png',
                    ],
                ],
            ],
        ),
        // ok
        '1' => array(
            'id' => 480,
            'color' => '#ffc300',
            'header' => [
                'name' => 'ĐIỆN THOẠI - TABLET - PHỤ KIỆN',
                'alias' => 'dien-thoai-di-dong',
            ],
            'position_display' => 6,
            'premium' => [
                'title' => 'Trải nghiệm các dòng điện thoại cao cấp',
                'status' => 1,
                'background' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/47%20laptop/Premium_BgZone_DT.jpg',
                'data' => [
                    '0' => [
                        'id' => '59287',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/dt/01.png',
                    ],
                    '1' => [
                        'id' => '65264',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/dt/2_2.png',
                    ],
                    '2' => [
                        'id' => '67335',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/dt/03.png',
                    ],
                    '3' => [
                        'id' => '58779',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/dt/04.png',
                    ],
                    '4' => [
                        'id' => '53642',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/dt/05.png',
                    ],
                ],
            ],
        ),
        // ok
        '2' => array(
            'id' => 350,
            'color' => '#f57650',
            'header' => [
                'name' => 'TIVI - LOA - ÂM THANH',
                'alias' => 'tivi-man-hinh-lcd',
            ],
            'position_display' => 2,
            'premium' => [
                'title' => 'Trải nghiệm các dòng tivi cao cấp',
                'status' => 1,
                'background' => 'https://www.nguyenkim.com/images/companies/_1/PremiumZoneBG_Tivi.jpg',
                'data' => [
                    '0' => [
                        'id' => '58259',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Banner%20Premium%20zone/0905/1_1.png',
                    ],
                    '1' => [
                        'id' => '55790',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Banner%20Premium%20zone/0905/2_1.png',
                    ],
                    '2' => [
                        'id' => '54876',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Banner%20Premium%20zone/0905/3.png',
                    ],
                    '3' => [
                        'id' => '54154',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Banner%20Premium%20zone/0905/4.png',
                    ],
                    '4' => [
                        'id' => '55282',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Banner%20Premium%20zone/0905/5.png',
                    ],
                ],
            ],
        ),
        //ok
        '3' => array(
            'id' => 423,
            'color' => '#70c4fd',
            'header' => [
                'name' => 'TỦ LẠNH - TỦ ĐÔNG',
                'alias' => 'tu-lanh',
            ],
            'position_display' => 17,
            'premium' => [
                'title' => 'TRẢI NGHIỆM CÁC DÒNG TỦ LẠNH CAO CẤP',
                'status' => 1,
                'background' => 'https://cdn.nguyenkimmall.com/images/companies/_1/PremiumZoneBG_TuLanh.jpg',
                'data' => [
                    '0' => [
                        'id' => '54096',
                        'images' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap5_%402x-17h-16032018.png',
                    ],
                    '1' => [
                        'id' => '47064',
                        'images' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap4_%402x.png',
                    ],
                    '2' => [
                        'id' => '49738',
                        'images' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap2_%402x.png',
                    ],
                    '3' => [
                        'id' => '39944',
                        'images' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap3_%402x.png',
                    ],
                    '4' => [
                        'id' => '31053',
                        'images' => 'https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T4/PremiumZone/TLcaocap1_%402x-16032018.png',
                    ],
                ],
            ],
        ),
        //ok
        '4' => array(
            'id' => 421,
            'color' => '#57b6b6',
            'header' => [
                'name' => 'MÁY GIẶT - MÁY SẤY',
                'alias' => 'may-giat',
            ],
            'position_display' => 15,
            'premium' => [
                'title' => 'Trải nghiệm các dòng Máy Giặt cao cấp',
                'status' => 1,
                'background' => 'https://www.nguyenkim.com/images/companies/_1/PremiumZoneBG_MayGiat.jpg',
                'data' => [
                    '0' => [
                        'id' => '62401',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T4/PremiumZone/MG-Toshiba.png',
                    ],
                    '1' => [
                        'id' => '54739',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T4/PremiumZone/MGcaocap4_%402.png',
                    ],
                    '2' => [
                        'id' => '56553',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T4/PremiumZone/MGcaocap5_%402.png',
                    ],
                    '3' => [
                        'id' => '61873',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T4/PremiumZone/MGcaocap1_%402.png',
                    ],
                    '4' => [
                        'id' => '60551',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T4/PremiumZone/MGcaocap2_%402.png',
                    ],
                ],
            ],
        ),
        '5' => array(
            'id' => 3011,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'MÁY ẢNH - PHỤ KIỆN',
                'alias' => 'may-anh-mirrorless',
            ],
            'position_display' => 21,
            'premium' => [

            ]
        ),
        //ok
        '6' => array(
            'id' => 527,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'LAPTOP - PC - PHỤ KIỆN',
                'alias' => 'may-tinh-xach-tay',
            ],
            'position_display' => 18,
            'premium' => [
                'title' => 'TRẢI NGHIỆM CÁC DÒNG LAPTOP CAO CẤP',
                'status' => 1,
                'background' => 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Banner%20Premium%20zone/Laptop%2027/Premium_BgZone_Laptop_1920x380.jpg',
                'data' => [
                    '0' => [
                        'id' => '62181',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/47%20laptop/01.png',
                    ],
                    '1' => [
                        'id' => '66016',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/47%20laptop/02.png',
                    ],
                    '2' => [
                        'id' => '67115',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/47%20laptop/03.png',
                    ],
                    '3' => [
                        'id' => '55304',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Data_Price/Banner%20Premium%20zone/0905/MB_Pro_13_2017.png',
                    ],
                    '4' => [
                        'id' => '65437',
                        'images' => 'https://adm.nguyenkim.com/images/companies/_1/Content/HT2018/zone/47%20laptop/05.png',
                    ],
                ],
            ],
        ),
        '7' => array(
            'id' => 344,
            'color' => '#b8d733',
            'header' => [
                'name' => 'GIA DỤNG',
                'alias' => 'gia-dung',
            ],
            'position_display' => 3,
            'premium' => [

            ]
        ),
        // ok
        '8' => array(
            'id' => 531,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'MÁY IN',
                'alias' => 'may-in-van-phong',
            ],
            'position_display' => 0,
            'premium' => [
                'title' => 'TRẢI NGHIỆM CÁC DÒNG MÁY IN ĐỜI MỚI 2018',
                'status' => 1,
                'background' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/Bg-Block-Mayin.jpg',
                'data' => [
                    '0' => [
                        'id' => '69471',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10037179_MAYINDACHUCNANG_BROTHER_MFC-L8690CDW_01.png',
                    ],
                    '1' => [
                        'id' => '68551',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10037089.png',
                    ],
                    '2' => [
                        'id' => '66755',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10036667_MAYIN_HP_LJ-MFP_M436DN.png',
                    ],
                    '3' => [
                        'id' => '68099',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10036964_MAYCHIEU_CANON_LV-WX300USTI.png',
                    ],
                    '4' => [
                        'id' => '51826',
                        'images' => 'https://www.nguyenkim.com/images/companies/_1/Content/HT2018/zone/2018/10031281_MAYPHOTOCOPY%C2%A0_CANON_IR2530.png',
                    ],
                ],
            ],
        ),
    ),
    'category_search' => array(
        '350' => array(
            '0' => [
                'id' => 350,
                'name' => 'Tivi LCD',
                'alias' => 'tivi-man-hinh-lcd',
                'subcats' => '',
                'features_hash' => '',
            ],
            '1' => [
                'id' => 365,
                'name' => 'Tivi Samsung',
                'alias' => 'tivi-lcd-samsung',
                'subcats' => '',
                'features_hash' => '',
            ],
            '2' => [
                'id' => 359,
                'name' => 'Tivi Sony',
                'alias' => 'tivi-lcd-sony',
                'subcats' => '',
                'features_hash' => '',
            ],
            '3' => [
                'id' => 364,
                'name' => 'Tivi LG',
                'alias' => 'tivi-lcd-lg',
                'subcats' => '',
                'features_hash' => '',
            ],
            '4' => [
                'id' => 363,
                'name' => 'Tivi Toshiba',
                'alias' => 'tivi-lcd-toshiba',
                'subcats' => '',
                'features_hash' => '',
            ],
            '5' => [
                'id' => 362,
                'name' => 'Tivi Panasonic',
                'alias' => 'tivi-lcd-panasonic',
                'subcats' => '',
                'features_hash' => '',
            ],

        ),
        '480' => array(
            '0' => [
                'id' => 488,
                'name' => 'Samsung',
                'alias' => 'dien-thoai-di-dong-samsung',
                'subcats' => '',
                'features_hash' => '',
            ],
            '1' => [
                'id' => 480,
                'name' => 'Smartphone',
                'alias' => 'dien-thoai-di-dong',
                'subcats' => '',
                'features_hash' => '',
            ],
            '2' => [
                'id' => 2092,
                'name' => 'Oppo',
                'alias' => 'dien-thoai-di-dong-oppo',
                'subcats' => '',
                'features_hash' => '',
            ],
            '3' => [
                'id' => 485,
                'name' => 'iPhone',
                'alias' => 'dien-thoai-di-dong-apple-iphone',
                'subcats' => '',
                'features_hash' => '',
            ],
            '4' => [
                'id' => 487,
                'name' => 'Sony',
                'alias' => 'dien-thoai-di-dong-sony',
                'subcats' => '',
                'features_hash' => '',
            ],
            '5' => [
                'id' => 2852,
                'name' => 'Xiaomi',
                'alias' => 'dien-thoai-xiaomi',
                'subcats' => '',
                'features_hash' => '',
            ],
            '6' => [
                'id' => 3069,
                'name' => 'iPhone X',
                'alias' => 'iphone-x',
                'subcats' => '',
                'features_hash' => '',
            ],
            '7' => [
                'id' => 486,
                'name' => 'Nokia',
                'alias' => 'dien-thoai-di-dong-nokia',
                'subcats' => '',
                'features_hash' => '',
            ],
            '8' => [
                'id' => 2660,
                'name' => 'Vivo',
                'alias' => 'dien-thoai-vivo',
                'subcats' => '',
                'features_hash' => '',
            ],

        ),
    ),
    'category_pretties' => array(
        'id' => 344,
        'title' => 'Gia dụng',
        'alisa' => 'gia-dung',
        'background' => 'https://cdn.nguyenkimmall.com/images/companies/_1/NKv40/bg-giadung-mobile.png',
        'menu' => array(
            '0' => array(
                'id' => 1,
                'name' => 'Gia dụng nhà bếp',
                'alias' => 'gia-dung-nha-bep',
                'sub_category' => array(
                    '0' => array(
                        'id' => 2360,
                        'name' => 'Bếp gas',
                        'alias' => 'bep-gas',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 820,
                        'name' => 'Bếp từ',
                        'alias' => 'bep-dien',
                        'subcats' => 'Y',
                        'features_hash' => '168-35069',
                    ),
                    '2' => array(
                        'id' => 820,
                        'name' => 'Bếp hồng ngoại',
                        'alias' => 'bep-dien',
                        'subcats' => 'Y',
                        'features_hash' => '168-34904',
                    ),
                    '3' => array(
                        'id' => 822,
                        'name' => 'Lò nướng',
                        'alias' => 'lo-nuong-vi-nuong',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 821,
                        'name' => 'Lò vi sóng',
                        'alias' => 'lo-vi-song-microwave',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 823,
                        'name' => 'Lẩu điện',
                        'alias' => 'lau-dien-da-nang',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 819,
                        'name' => 'Nồi áp suất',
                        'alias' => 'noi-ap-suat',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 818,
                        'name' => 'Nồi cơm điện',
                        'alias' => 'noi-com-dien',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '8' => array(
                        'id' => 1033,
                        'name' => 'Nồi đa năng',
                        'alias' => 'noi-da-nang',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '9' => array(
                        'id' => 2859,
                        'name' => 'Bình thủy điện',
                        'alias' => 'binh-thuy-dien',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '10' => array(
                        'id' => 826,
                        'name' => 'Bình đun siêu tốc',
                        'alias' => 'am-nuoc-binh-nuoc',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '11' => array(
                        'id' => 829,
                        'name' => 'Máy sấy chén',
                        'alias' => 'may-say-chen',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '12' => array(
                        'id' => 827,
                        'name' => 'Máy hút khói, Khử mùi',
                        'alias' => 'may-hut-khoi-khu-mui',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),

            ),
            '1' => array(
                'id' => 2,
                'name' => 'Dụng cụ nhà bếp',
                'alias' => 'gia-dung-nha-bep',
                'sub_category' => array(
                    '0' => array(
                        'id' => 824,
                        'name' => 'Xoong, Nồi',
                        'alias' => 'bo-noi-nau-an',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 825,
                        'name' => 'Chảo chống dính',
                        'alias' => 'chao-chong-dinh',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 1645,
                        'name' => 'Tô - Chén - Dĩa',
                        'alias' => 'to-chen-dia',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 1643,
                        'name' => 'Dụng cụ ăn',
                        'alias' => 'dung-cu-nha-bep',
                        'subcats' => '',
                        'features_hash' => '',
                    ),

                ),

            ),
            '2' => array(
                'id' => 3,
                'name' => 'Đồ dùng gia đình',
                'alias' => 'do-dung-gia-dinh',
                'sub_category' => array(
                    '0' => array(
                        'id' => 2818,
                        'name' => 'Balo, Vali',
                        'alias' => 'balo-va-vali',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 1759,
                        'name' => 'Nệm',
                        'alias' => 'nem',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 1437,
                        'name' => 'Mền',
                        'alias' => 'men',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 1184,
                        'name' => 'Bộ drap',
                        'alias' => 'bo-dra-trai-giuong-va-ao-goi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 0,
                        'name' => 'Gối và áo gối',
                        'alias' => '',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 1239,
                        'name' => 'Ổ cắm điện',
                        'alias' => 'o-cam-dien',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 2752,
                        'name' => 'Bộ lau nhà - Thùng rác',
                        'alias' => 'thung-rac',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 2870,
                        'name' => 'Đèn bắt muỗi',
                        'alias' => 'den-bat-muoi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),
            ),
            '3' => array(
                'id' => 4,
                'name' => 'thiết bị gia đình',
                'alias' => 'thiet-bi-gia-dinh',
                'sub_category' => array(
                    '0' => array(
                        'id' => 426,
                        'name' => 'Máy nước nóng',
                        'alias' => 'may-nuoc-nong',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 717,
                        'name' => 'Máy hút bụi',
                        'alias' => 'may-hut-bui',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 428,
                        'name' => 'Máy lọc nước nóng',
                        'alias' => 'may-loc-nuoc-nong-lanh',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 715,
                        'name' => 'Bàn ủi',
                        'alias' => 'ban-ui-ban-la',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 717,
                        'name' => 'Máy lọc nước',
                        'alias' => 'may-hut-bui',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 427,
                        'name' => 'Máy nước nóng lạnh',
                        'alias' => 'may-loc-khong-khi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 427,
                        'name' => 'Máy lọc không khí',
                        'alias' => 'may-loc-khong-khi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 714,
                        'name' => 'Quạt điện - Quạt máy',
                        'alias' => 'quat-dien-quat-may',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '8' => array(
                        'id' => 2400,
                        'name' => 'Quạt phun sương',
                        'alias' => 'quat-phun-suong-quat-hoi-nuoc',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '9' => array(
                        'id' => 2549,
                        'name' => 'Máy hút ẩm - tạo ẩm',
                        'alias' => 'may-hut-am',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '10' => array(
                        'id' => 716,
                        'name' => 'Đèn điện - đèn sạc',
                        'alias' => 'den-dien-den-sac',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '11' => array(
                        'id' => 1331,
                        'name' => 'Ổn áp',
                        'alias' => 'on-ap',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '12' => array(
                        'id' => 726,
                        'name' => 'Các thiết bị gia đình khác',
                        'alias' => 'san-pham-gia-dung-khac',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),
            ),
            '4' => array(
                'id' => 5,
                'name' => 'Máy xay,vắt,ép',
                'alias' => 'may-xay-vat-ep',
                'sub_category' => array(
                    '0' => array(
                        'id' => 720,
                        'name' => 'Máy ép trái cây',
                        'alias' => 'may-ep-trai-cay',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 723,
                        'name' => 'Máy vắt cam',
                        'alias' => 'may-vat-cam',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 719,
                        'name' => 'Máy xay sinh tố',
                        'alias' => 'may-xay-sinh-to',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 722,
                        'name' => 'Máy pha cà phê',
                        'alias' => 'may-pha-ca-phe',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 725,
                        'name' => 'Máy làm sữa chua',
                        'alias' => 'may-lam-sua-chua',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 721,
                        'name' => 'Máy Xay đậu nành',
                        'alias' => 'may-xay-dau-nanh',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 724,
                        'name' => 'Máy đánh trứng',
                        'alias' => 'may-danh-trung',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 1182,
                        'name' => 'Máy xay thịt',
                        'alias' => 'may-xay-thit',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 3015,
                        'name' => 'Máy làm bánh mì',
                        'alias' => 'may-lam-banh-mi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),
            ),
            '4' => array(
                'id' => 6,
                'name' => 'Sức khỏe làm đẹp',
                'alias' => 'suc-khoe-lam-dep',
                'sub_category' => array(
                    '0' => array(
                        'id' => 2526,
                        'name' => 'Máy sấy tóc - tạo kiểu tóc',
                        'alias' => 'may-say-toc',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 2864,
                        'name' => 'Thiết bị làm đẹp',
                        'alias' => 'thiet-bi-sac-dep',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 2528,
                        'name' => 'Máy cạo râu',
                        'alias' => 'may-cao-rau',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 2530,
                        'name' => 'Cân sức khỏe',
                        'alias' => 'can-suc-khoe',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 2521,
                        'name' => 'Thiết bị y tế',
                        'alias' => 'thiet-bi-y-te',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 2523,
                        'name' => 'Ghế Massage - Máy Massage',
                        'alias' => 'may-massage',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 955,
                        'name' => 'Máy tập thể dục đa năng',
                        'alias' => 'may-tap-da-nang',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),
            ),

        ),
        'item_menu' => array(
            '0' => array(
                'id' => 1,
                'name' => 'Cơm ngon mỗi ngày',
                'alias' => 'gia-dung-nha-bep',
                'background' => 'https://www.nguyenkim.com/images/companies/_1/NKv40/giadung-img-1.jpg',
                'sub_category' => array(
                    '0' => array(
                        'id' => 2360,
                        'name' => 'Bếp gas',
                        'alias' => 'bep-gas',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 820,
                        'name' => 'Bếp từ',
                        'alias' => 'bep-dien',
                        'subcats' => 'Y',
                        'features_hash' => '168-35069',
                    ),
                    '2' => array(
                        'id' => 820,
                        'name' => 'Bếp hồng ngoại',
                        'alias' => 'bep-dien',
                        'subcats' => 'Y',
                        'features_hash' => '168-34904',
                    ),
                    '3' => array(
                        'id' => 822,
                        'name' => 'Lò nướng',
                        'alias' => 'lo-nuong-vi-nuong',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 821,
                        'name' => 'Lò vi sóng',
                        'alias' => 'lo-vi-song-microwave',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 823,
                        'name' => 'Lẩu điện',
                        'alias' => 'lau-dien-da-nang',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 819,
                        'name' => 'Nồi áp suất',
                        'alias' => 'noi-ap-suat',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 818,
                        'name' => 'Nồi cơm điện',
                        'alias' => 'noi-com-dien',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '8' => array(
                        'id' => 1033,
                        'name' => 'Nồi đa năng',
                        'alias' => 'noi-da-nang',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '9' => array(
                        'id' => 2859,
                        'name' => 'Bình thủy điện',
                        'alias' => 'binh-thuy-dien',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '10' => array(
                        'id' => 826,
                        'name' => 'Bình đun siêu tốc',
                        'alias' => 'am-nuoc-binh-nuoc',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '11' => array(
                        'id' => 829,
                        'name' => 'Máy sấy chén',
                        'alias' => 'may-say-chen',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '12' => array(
                        'id' => 827,
                        'name' => 'Máy hút khói, Khử mùi',
                        'alias' => 'may-hut-khoi-khu-mui',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),

            ),
            '1' => array(
                'id' => 2,
                'name' => 'Nữ công gia chánh',
                'alias' => 'gia-dung-nha-bep',
                'background' => 'https://www.nguyenkim.com/images/companies/_1/NKv40/giadung-img-2.jpg',
                'sub_category' => array(
                    '0' => array(
                        'id' => 824,
                        'name' => 'Xoong, Nồi',
                        'alias' => 'bo-noi-nau-an',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 825,
                        'name' => 'Chảo chống dính',
                        'alias' => 'chao-chong-dinh',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 1645,
                        'name' => 'Tô - Chén - Dĩa',
                        'alias' => 'to-chen-dia',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 1643,
                        'name' => 'Dụng cụ ăn',
                        'alias' => 'dung-cu-nha-bep',
                        'subcats' => '',
                        'features_hash' => '',
                    ),

                ),

            ),
            '2' => array(
                'id' => 3,
                'name' => 'Giải khát thanh nhiệt',
                'alias' => 'may-xay-vat-ep',
                'background' => 'https://www.nguyenkim.com/images/companies/_1/NKv40/giadung-img-3.jpg',
                'sub_category' => array(
                    '0' => array(
                        'id' => 720,
                        'name' => 'Máy ép trái cây',
                        'alias' => 'may-ep-trai-cay',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 723,
                        'name' => 'Máy vắt cam',
                        'alias' => 'may-vat-cam',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 719,
                        'name' => 'Máy xay sinh tố',
                        'alias' => 'may-xay-sinh-to',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 722,
                        'name' => 'Máy pha cà phê',
                        'alias' => 'may-pha-ca-phe',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 725,
                        'name' => 'Máy làm sữa chua',
                        'alias' => 'may-lam-sua-chua',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 721,
                        'name' => 'Máy Xay đậu nành',
                        'alias' => 'may-xay-dau-nanh',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 724,
                        'name' => 'Máy đánh trứng',
                        'alias' => 'may-danh-trung',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 1182,
                        'name' => 'Máy xay thịt',
                        'alias' => 'may-xay-thit',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 3015,
                        'name' => 'Máy làm bánh mì',
                        'alias' => 'may-lam-banh-mi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),
            ),
            '3' => array(
                'id' => 4,
                'name' => 'Nhà sạch thì mát',
                'alias' => 'thiet-bi-gia-dinh',
                'background' => 'https://www.nguyenkim.com/images/companies/_1/NKv40/giadung-img-4.jpg',
                'sub_category' => array(
                    '0' => array(
                        'id' => 426,
                        'name' => 'Máy nước nóng',
                        'alias' => 'may-nuoc-nong',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 717,
                        'name' => 'Máy hút bụi',
                        'alias' => 'may-hut-bui',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 428,
                        'name' => 'Máy lọc nước nóng',
                        'alias' => 'may-loc-nuoc-nong-lanh',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 715,
                        'name' => 'Bàn ủi',
                        'alias' => 'ban-ui-ban-la',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 717,
                        'name' => 'Máy lọc nước',
                        'alias' => 'may-hut-bui',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 427,
                        'name' => 'Máy nước nóng lạnh',
                        'alias' => 'may-loc-khong-khi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 427,
                        'name' => 'Máy lọc không khí',
                        'alias' => 'may-loc-khong-khi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 714,
                        'name' => 'Quạt điện - Quạt máy',
                        'alias' => 'quat-dien-quat-may',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '8' => array(
                        'id' => 2400,
                        'name' => 'Quạt phun sương',
                        'alias' => 'quat-phun-suong-quat-hoi-nuoc',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '9' => array(
                        'id' => 2549,
                        'name' => 'Máy hút ẩm - tạo ẩm',
                        'alias' => 'may-hut-am',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '10' => array(
                        'id' => 716,
                        'name' => 'Đèn điện - đèn sạc',
                        'alias' => 'den-dien-den-sac',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '11' => array(
                        'id' => 1331,
                        'name' => 'Ổn áp',
                        'alias' => 'on-ap',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '12' => array(
                        'id' => 726,
                        'name' => 'Các thiết bị gia đình khác',
                        'alias' => 'san-pham-gia-dung-khac',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),
            ),
            '4' => array(
                'id' => 5,
                'name' => 'Tươi trẻ, khỏe đẹp',
                'alias' => 'suc-khoe-lam-dep',
                'background' => 'https://www.nguyenkim.com/images/companies/_1/NKv40/giadung-img-5.jpg',
                'sub_category' => array(
                    '0' => array(
                        'id' => 2526,
                        'name' => 'Máy sấy tóc - tạo kiểu tóc',
                        'alias' => 'may-say-toc',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 2864,
                        'name' => 'Thiết bị làm đẹp',
                        'alias' => 'thiet-bi-sac-dep',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 2528,
                        'name' => 'Máy cạo râu',
                        'alias' => 'may-cao-rau',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 2530,
                        'name' => 'Cân sức khỏe',
                        'alias' => 'can-suc-khoe',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 2521,
                        'name' => 'Thiết bị y tế',
                        'alias' => 'thiet-bi-y-te',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 2523,
                        'name' => 'Ghế Massage - Máy Massage',
                        'alias' => 'may-massage',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 955,
                        'name' => 'Máy tập thể dục đa năng',
                        'alias' => 'may-tap-da-nang',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),
            ),
            '5' => array(
                'id' => 6,
                'name' => 'Chăn ấm nệm êm',
                'alias' => 'do-dung-gia-dinh',
                'background' => 'https://www.nguyenkim.com/images/companies/_1/NKv40/giadung-img-6.jpg',
                'sub_category' => array(
                    '0' => array(
                        'id' => 2818,
                        'name' => 'Balo, Vali',
                        'alias' => 'balo-va-vali',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '1' => array(
                        'id' => 1759,
                        'name' => 'Nệm',
                        'alias' => 'nem',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '2' => array(
                        'id' => 1437,
                        'name' => 'Mền',
                        'alias' => 'men',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '3' => array(
                        'id' => 1184,
                        'name' => 'Bộ drap',
                        'alias' => 'bo-dra-trai-giuong-va-ao-goi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '4' => array(
                        'id' => 0,
                        'name' => 'Gối và áo gối',
                        'alias' => '',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '5' => array(
                        'id' => 1239,
                        'name' => 'Ổ cắm điện',
                        'alias' => 'o-cam-dien',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '6' => array(
                        'id' => 2752,
                        'name' => 'Bộ lau nhà - Thùng rác',
                        'alias' => 'thung-rac',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                    '7' => array(
                        'id' => 2870,
                        'name' => 'Đèn bắt muỗi',
                        'alias' => 'den-bat-muoi',
                        'subcats' => '',
                        'features_hash' => '',
                    ),
                ),
            ),
        ),
        'blog' => array(
            '0' => array(
                'id' => 1,
                'name' => 'Chọn quạt điện cho từng không gian phòng đơn giản thế thôi',
                'alias' => 'https://www.nguyenkim.com/chon-quat-dien-cho-tung-khong-gian-phong-don-gian-the-thoi.html',
                'images' => 'https://cdn.nguyenkimmall.com/images/detailed/523/quat.jpg',
                'category' => 'Kinh nghiệm hay',
            ),
            '1' => array(
                'id' => 2,
                'name' => 'Nồi cơm điện đã trở nên hiện đại với những tính năng này rồi sao?',
                'alias' => 'https://www.nguyenkim.com/noi-com-dien-da-tro-nen-hien-dai-voi-nhung-tinh-nang-nay-roi-sao.html',
                'images' => 'https://cdn.nguyenkimmall.com/images/detailed/513/noi-com-dien-da-tro-nen-hien-dai-voi-nhung-tinh-nang-nay-roi-sao-00.jpg',
                'category' => 'Kinh nghiệm hay',
            ),
            '2' => array(
                'id' => 3,
                'name' => 'Ngon ngây ngất món tóp mỡ lắc tỏi ớt cho mùa World Cup',
                'alias' => 'https://www.nguyenkim.com/ngon-ngay-ngat-mon-top-mo-lac-toi-ot-cho-mua-world-cup.html',
                'images' => 'https://cdn.nguyenkimmall.com/images/detailed/526/top-mo-lac-toi-ot-3.jpg',
                'category' => 'Kinh nghiệm hay',
            ),
            '3' => array(
                'id' => 4,
                'name' => '6 mẹo khử vị đắng của khổ qua cực dễ bạn đã thử chưa?',
                'alias' => 'https://www.nguyenkim.com/6-meo-khu-vi-dang-cua-kho-qua-cuc-de-ban-da-thu-chua.html',
                'images' => 'https://cdn.nguyenkimmall.com/images/detailed/520/khu-vi-dang-trong-kho-qua-00.jpg',
                'category' => 'Kinh nghiệm hay',
            ),
            '4' => array(
                'id' => 5,
                'name' => 'Lạ mà quen 10 món ngon tuyệt cú mèo từ quả bơ',
                'alias' => 'https://www.nguyenkim.com/la-ma-quen-10-mon-ngon-tuyet-cu-meo-tu-qua-bo.html',
                'images' => 'https://cdn.nguyenkimmall.com/images/detailed/522/la-ma-quen-10-mon-ngon-tuyet-cu-meo-tu-qua-bo-00.jpg',
                'category' => 'Kinh nghiệm hay',
            ),
            '5' => array(
                'id' => 6,
                'name' => 'Việc nhà dễ dàng hơn với 12 mẹo làm sạch đồ dùng đơn giản này',
                'alias' => 'https://www.nguyenkim.com/viec-nha-de-dang-hon-voi-12-meo-lam-sach-do-dung-don-gian-nay.html',
                'images' => 'https://cdn.nguyenkimmall.com/images/detailed/519/viec-nha-de-dang-hon-voi-12-meo-lam-sach-do-dung-don-gian-nay-00.jpg',
                'category' => 'Kinh nghiệm hay',
            ),
        ),
    ),
    'site_root' => 'https://nguyenkim.com/',
    'promotion_code' => array(
        'banner' => 'https://www.nguyenkim.com/images/companies/_1/img/banner.png',
        'banner_text' => 'Hiện có 2 mã giảm giá chưa sử dụng',
        'data' => array(
            '0' => array(
                'code' => 'PRMS0004',
                'value' => 800000,
                'url' => '',
                'status' => 'Chưa sử dụng',
                'expiration_date' => '30/12/2018',
                'expiration_date_status' => 1,
                'expiration_date_text' => 'Sao chép',
            ),
            '1' => array(
                'code' => 'PRMS0002',
                'value' => 600000,
                'url' => '',
                'status' => 'Chưa sử dụng',
                'expiration_date' => '30/12/2018',
                'expiration_date_status' => 1,
                'expiration_date_text' => 'Sao chép',
            ),
            '2' => array(
                'code' => 'PRMS0003',
                'value' => 700000,
                'url' => '',
                'status' => 'Chưa sử dụng',
                'expiration_date' => '30/12/2017',
                'expiration_date_status' => 0,
                'expiration_date_text' => 'Hết hạn',
            ),
            '3' => array(
                'code' => 'PRMS0001',
                'value' => 500000,
                'url' => '',
                'status' => 'Chưa sử dụng',
                'expiration_date' => '30/12/2017',
                'expiration_date_status' => 0,
                'expiration_date_text' => 'Hết hạn',
            ),

            '4' => array(
                'code' => 'PRMS0005',
                'value' => 900000,
                'url' => '',
                'status' => 'Đã sử dụng',
                'expiration_date' => '30/12/2017',
                'expiration_date_status' => 0,
                'expiration_date_text' => 'Hết hạn',
            ),
            '5' => array(
                'code' => 'PRMS0006',
                'value' => 1000000,
                'url' => '',
                'status' => 'Chưa sử dụng',
                'expiration_date' => '30/12/2017',
                'expiration_date_status' => 0,
                'expiration_date_text' => 'Hết hạn',
            ),
            '6' => array(
                'code' => 'PRMS0007',
                'value' => 1100000,
                'url' => '',
                'status' => 'Chưa sử dụng',
                'expiration_date' => '30/12/2017',
                'expiration_date_status' => 0,
                'expiration_date_text' => 'Hết hạn',
            ),
        ),
    ),
    'packe_data_simso' => array(
        '0' => array(
            'id' => 1,
            'name' => 'Tài Phát Lộc',
            'code' => 'Mobifone',
            'show' => 0,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '1,5GB/Tháng',
            'data' => array(
                'title' => 'Tài Phát Lộc',
                'des' => array(
                    ['name' => 'Thời gian khuyến mãi', 'summ' => '12 tháng'],
                    ['name' => 'Phí duy trì', 'summ' => '30.000đ/tháng'],
                    ['name' => 'Tặng 368 phút thoại nội mạng/tháng', 'summ' => ''],
                    ['name' => 'Tặng dung lượng data', 'summ' => '1,5GB/tháng'],
                    ['name' => 'Miễn phí truy cập Facebook (FB30)', 'summ' => ''],
                    ['name' => 'Tổng ưu đãi trong 12 tháng', 'summ' => '8.537.280đ'],
                    ['name' => 'Độc quyền tại Nguyễn Kim', 'summ' => ''],
                ),
            ),
        ),
        '1' => array(
            'id' => 2,
            'name' => 'Mobifone C90',
            'code' => 'Mobifone',
            'show' => 0,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '60GB/Tháng',
            'data' => array(
                'title' => 'Mobifone C90',
                'des' => array(
                    ['name' => 'Gói cước bao gồm', 'summ' => '1 Sim mobile + thẻ cào 100.000đ'],
                    ['name' => 'Giá bán', 'summ' => '119.000đ'],
                    ['name' => 'Giá ưu đãi khi mua kèm Smartphone', 'summ' => '69.000đ'],
                    ['name' => 'Miễn phí cuộc gọi nội mạng dưới 20 phút', 'summ' => ''],
                    ['name' => '50 phút liên mạng trong nước', 'summ' => ''],
                    ['name' => '2GB tốc độ cao mỗi ngày (60GB/30 ngày)', 'summ' => ''],
                ),

            ),
        ),
        '2' => array(
            'id' => 3,
            'name' => 'Mobifone F500',
            'code' => 'Mobifone',
            'show' => 0,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '5GB/Tháng',
            'data' => array(
                'title' => 'Mobifone F500',
                'des' => array(
                    ['name' => 'Thời gian khuyến mãi:', 'summ' => '12 tháng'],
                    ['name' => 'Phí duy trì:', 'summ' => '500.000đ/360 ngày'],
                    ['name' => 'Tặng dung lượng tháng đầu 8GB/tháng', 'summ' => ''],
                    ['name' => 'Tặng dung lượng tháng 2 - tháng 12 5GB/tháng', 'summ' => ''],
                ),

            ),
        ),
        '3' => array(
            'id' => 4,
            'name' => 'Mobifone F90',
            'code' => 'Mobifone',
            'show' => 0,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '9GB/Tháng',
            'data' => array(
                'title' => 'Mobifone F90',
                'des' => array(
                    ['name' => 'Thời gian khuyến mãi:', 'summ' => '12 tháng'],
                    ['name' => 'Phí duy trì:', 'summ' => '90.000đ/tháng'],
                    ['name' => 'Tặng dung lượng 4 tháng đầu 18GB/tháng', 'summ' => ''],
                    ['name' => 'Tặng dung lượng từ tháng thứ 5 trở đi 9GB/tháng', 'summ' => ''],
                    ['name' => 'Cước ngoài gói:', 'summ' => '200đ/MB'],
                ),

            ),
        ),
        '4' => array(
            'id' => 5,
            'name' => 'Vinaphone VD89',
            'code' => 'Vinaphone',
            'show' => 0,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_vinaphone%402x.png',
            'volume' => '60GB/Tháng',
            'data' => array(
                'title' => 'Vinaphone VD89',
                'des' => array(
                    ['name' => 'Thời gian khuyến mãi:', 'summ' => '12 tháng'],
                    ['name' => 'Phí duy trì:', 'summ' => '89.000đ'],
                    ['name' => 'Miễn phí cuộc gọi nội mạng dưới 20 phút', 'summ' => ''],
                    ['name' => 'Tặng 50 phút gọi ngoại mạng/tháng', 'summ' => ''],
                    ['name' => 'Tặng dụng lượng data:', 'summ' => '60GB/tháng'],
                    ['name' => 'Tổng ưu đãi trong 12 tháng:', 'summ' => '146.376.000đ'],
                    ['name' => 'Độc quyền tại Nguyễn Kim', 'summ' => ''],
                ),

            ),
        ),
        '5' => array(
            'id' => 5,
            'name' => 'F90N',
            'code' => 'Mobifone',
            'show' => 1,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '9GB/Tháng',
            'data' => array(
                'title' => 'Mobifone F90N',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '90.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '9GB'],
                    ['name' => 'Loại thuê bao', 'summ' => 'MobiQ'],
                    ['name' => 'Loại hoà mạng:', 'summ' => 'MSC'],
                    ['name' => 'Giá gói cước được trừ vào Tài khoản chính thức của thuê bao trả trước', 'summ' => ''],
                    ['name' => 'Nhắn tin đăng ký theo cú pháp:', 'summ' => 'DK F90N gởi 999'],
                ),

            ),
        ),
        '6' => array(
            'id' => 6,
            'name' => 'NK50',
            'code' => 'Mobifone',
            'show' => 1,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '9GB/Tháng',
            'data' => array(
                'title' => 'Mobifone NK50',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '50.000đ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '2GB'],
                    ['name' => 'Miễn phí cuộc gọi nội mạng dưới 10 phút', 'summ' => ''],
                    ['name' => 'Giá gói cước được trừ vào Tài khoản chính thức của thuê bao trả trước', 'summ' => ''],
                    ['name' => 'Giá gói cước được trừ vào Tài khoản chính thức của thuê bao trả trước', 'summ' => ''],
                    ['name' => 'Nếu thuê bao sử dụng hết dung lượng của gói cước, được đăng kí các gói MAX hiện hành của Mobifone', 'summ' => ''],
                    ['name' => 'Nhắn tin đăng ký theo cú pháp: ', 'summ' => 'DK NK50 gởi 789'],
                ),

            ),
        ),
        '7' => array(
            'id' => 7,
            'name' => 'HDP70',
            'code' => 'Mobifone',
            'show' => 1,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '2GB/Tháng',
            'data' => array(
                'title' => 'Mobifone HDP70',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '70.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '2GB'],
                    ['name' => 'Ưu đãi gói cước:', 'summ' => '70 phút gọi nội mạng.'],
                    ['name' => 'Loại thuê bao:', 'summ' => 'MOBI Q'],
                    ['name' => 'Đăng ký gói cước bằng ứng dụng NKM – Phần đăng ký gói Vas Mobifone', 'summ' => ''],

                ),

            ),
        ),
        '8' => array(
            'id' => 8,
            'name' => 'HDP100',
            'code' => 'Mobifone',
            'show' => 1,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '3GB/Tháng',
            'data' => array(
                'title' => 'Mobifone HDP100',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '100.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '3GB'],
                    ['name' => 'Ưu đãi gói cước:', 'summ' => '100 phút gọi nội mạng.'],
                    ['name' => 'Loại thuê bao:', 'summ' => 'MOBI Q'],
                    ['name' => 'Đăng ký gói cước bằng ứng dụng NKM – Phần đăng ký gói Vas Mobifone', 'summ' => ''],

                ),

            ),
        ),
        '9' => array(
            'id' => 9,
            'name' => 'HDP120',
            'code' => 'Mobifone',
            'show' => 1,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '4GB/Tháng',
            'data' => array(
                'title' => 'Mobifone HDP120',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '120.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '4GB'],
                    ['name' => 'Ưu đãi gói cước:', 'summ' => '120 phút gọi nội mạng.'],
                    ['name' => 'Loại thuê bao:', 'summ' => 'MOBI Q'],
                    ['name' => 'Đăng ký gói cước bằng ứng dụng NKM – Phần đăng ký gói Vas Mobifone', 'summ' => ''],

                ),

            ),
        ),
        '10' => array(
            'id' => 10,
            'name' => 'HDP200',
            'code' => 'Mobifone',
            'show' => 1,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '6.5GB/Tháng',
            'data' => array(
                'title' => 'Mobifone HDP200',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '200.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '6.5GB'],
                    ['name' => 'Ưu đãi gói cước:', 'summ' => '200 phút gọi nội mạng.'],
                    ['name' => 'Loại thuê bao:', 'summ' => 'MOBI Q'],
                    ['name' => 'Đăng ký gói cước bằng ứng dụng NKM – Phần đăng ký gói Vas Mobifone', 'summ' => ''],

                ),

            ),
        ),
        '11' => array(
            'id' => 11,
            'name' => 'HDP300',
            'code' => 'Mobifone',
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '10GB/Tháng',
            'show' => 1,
            'data' => array(
                'title' => 'Mobifone HDP300',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '300.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '10GB'],
                    ['name' => 'Ưu đãi gói cước:', 'summ' => '300 phút gọi nội mạng.'],
                    ['name' => 'Loại thuê bao:', 'summ' => 'MOBI Q'],
                    ['name' => 'Đăng ký gói cước bằng ứng dụng NKM – Phần đăng ký gói Vas Mobifone', 'summ' => ''],
                ),

            ),
        ),
        '12' => array(
            'id' => 12,
            'name' => 'HDP600',
            'code' => 'Mobifone',
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png',
            'volume' => '20GB/Tháng',
            'show' => 1,
            'data' => array(
                'title' => 'Mobifone HDP600',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '600.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '20GB'],
                    ['name' => 'Ưu đãi gói cước:', 'summ' => '600 phút gọi nội mạng.'],
                    ['name' => 'Loại thuê bao:', 'summ' => 'MOBI Q'],
                    ['name' => 'Đăng ký gói cước bằng ứng dụng NKM – Phần đăng ký gói Vas Mobifone', 'summ' => ''],

                ),

            ),
        ),
        '14' => array(
            'id' => 14,
            'name' => 'VINA7GB',
            'code' => 'Vinaphone',
            'show' => 1,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_vinaphone%402x.png',
            'volume' => '7GB/Tháng',
            'data' => array(
                'title' => 'Vinaphone VINA7GB',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '70.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '7GB'],
                    ['name' => 'Giá cước:', 'summ' => '690đ/phút (12 tháng); thoại nội mạng = thoại ngoại mạng.'],
                    ['name' => 'Tháng thứ 13 trở đi trở về lại với mức giá chuẩn của nhà mạng.', 'summ' => ''],
                    ['name' => 'Cách đăng ký gói cước:', 'summ' => 'Soạn tin nhắn [DK_TENGOICUOC] gửi 888'],

                ),

            ),
        ),
        '15' => array(
            'id' => 15,
            'name' => 'HEY90',
            'code' => 'Vinaphone',
            'show' => 1,
            'icon' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_vinaphone%402x.png',
            'volume' => '5GB/Tháng',
            'data' => array(
                'title' => 'Vinaphone HEY90',
                'des' => array(
                    ['name' => 'Giá bán:', 'summ' => '90.000đ/chu kỳ/30 ngày'],
                    ['name' => 'Dung lượng data:', 'summ' => '5GB'],
                    ['name' => 'Ưu đãi gói cước:', 'summ' => 'Thoại nội mạng 1000phút; Thoại ngoại mạng 20phút; Xem gói dịch vụ truyền hình MyTV'],
                    ['name' => 'Cách đăng ký gói cước:', 'summ' => 'Soạn tin nhắn [DK_TENGOICUOC] gửi 888'],

                ),

            ),
        ),

    ),
    'category_simso' => array(
        ['value' => '', 'name' => 'Tất cả'],
        ['value' => 'Sim Bình dân', 'name' => 'Bình dân'],
        ['value' => 'Sim Thần tài', 'name' => 'Thần tài'],
        ['value' => 'Sim Tam hoa', 'name' => 'Tam hoa'],
        ['value' => 'Sim Tứ quý', 'name' => 'Tứ quý'],
    ),
    'nhamang' => array(
        ['value' => '', 'name' => 'Tất cả', 'image' => ''],
        ['value' => 'vinaphone', 'name' => 'Vinaphone', 'image' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_vinaphone%402x.png'],
        ['value' => 'mobifone', 'name' => 'Mobifone', 'image' => 'https://www.nguyenkim.com/images/companies/_1/html/2018/T07/bannerTLV/logo_napcard/logo_mobifone%402x.png'],
    ),
    'dauso' => array(
        'value' => 10,
        'name' => '10 số',
    ),
    'price_table' => array(
        ['menh_gia' => 50000, 'gia_nk' => 50000],
        ['menh_gia' => 100000, 'gia_nk' => 98000],
        ['menh_gia' => 200000, 'gia_nk' => 195000],
        ['menh_gia' => 300000, 'gia_nk' => 294000],
        ['menh_gia' => 500000, 'gia_nk' => 490000],
    ),
    'text_search' => array(
        "Nhập 999 tìm sim có chứa 999",
        "Nhập 098* tìm sim có 3 số đầu 098",
        "Nhập *999 tìm sim có 3 số cuối 999",
        "Nhập 098*999 tìm sim có đầu số 098 & 3 số cuối 999",
    ),
    'home_page' => array(
        1925 => array(
            'id' => 480,
            'color' => '#ffc300',
            'header' => [
                'name' => 'ĐIỆN THOẠI - TABLET - PHỤ KIỆN',
                'alias' => 'dien-thoai-di-dong',
            ],
        ),
        2111 => array(
            'id' => 480,
            'color' => '#ffc300',
            'header' => [
                'name' => 'ĐIỆN THOẠI - TABLET - PHỤ KIỆN',
                'alias' => 'dien-thoai-di-dong',
            ],
        ),
        2001 => array(
            'id' => 527,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'LAPTOP - PC - PHỤ KIỆN',
                'alias' => 'may-tinh-xach-tay',
            ],
        ),
        2139 => array(
            'id' => 527,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'LAPTOP - PC - PHỤ KIỆN',
                'alias' => 'may-tinh-xach-tay',
            ],
        ),
        1971 => array(
            'id' => 350,
            'color' => '#f57650',
            'header' => [
                'name' => 'TIVI - LOA - ÂM THANH',
                'alias' => 'tivi-man-hinh-lcd',
            ],

        ),
        2079 => array(
            'id' => 350,
            'color' => '#f57650',
            'header' => [
                'name' => 'TIVI - LOA - ÂM THANH',
                'alias' => 'tivi-man-hinh-lcd',
            ],

        ),
        1951 => array(
            'id' => 350,
            'color' => '#00a1c0',
            'header' => [
                'name' => 'MÁY LẠNH',
                'alias' => 'may-lanh',
            ],

        ),
        2095 => array(
            'id' => 350,
            'color' => '#00a1c0',
            'header' => [
                'name' => 'MÁY LẠNH',
                'alias' => 'may-lanh',
            ],

        ),
        1963 => array(
            'id' => 423,
            'color' => '#70c4fd',
            'header' => [
                'name' => 'TỦ LẠNH - TỦ ĐÔNG',
                'alias' => 'tu-lanh',
            ],

        ),
        2087 => array(
            'id' => 423,
            'color' => '#70c4fd',
            'header' => [
                'name' => 'TỦ LẠNH - TỦ ĐÔNG',
                'alias' => 'tu-lanh',
            ],

        ),
        1943 => array(
            'id' => 421,
            'color' => '#57b6b6',
            'header' => [
                'name' => 'MÁY GIẶT - MÁY SẤY',
                'alias' => 'may-giat',
            ],

        ),
        2103 => array(
            'id' => 421,
            'color' => '#57b6b6',
            'header' => [
                'name' => 'MÁY GIẶT - MÁY SẤY',
                'alias' => 'may-giat',
            ],

        ),
        2045 => array(
            'id' => 3011,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'MÁY ẢNH - PHỤ KIỆN',
                'alias' => 'may-anh-mirrorless',
            ],

        ),
        2121 => array(
            'id' => 3011,
            'color' => '#00c0c6',
            'header' => [
                'name' => 'MÁY ẢNH - PHỤ KIỆN',
                'alias' => 'may-anh-mirrorless',
            ],

        ),
        1904 => array(
            'id' => 344,
            'color' => '#b8d733',
            'header' => [
                'name' => 'GIA DỤNG',
                'alias' => 'gia-dung',
            ],
        ),
        2129 => array(
            'id' => 344,
            'color' => '#b8d733',
            'header' => [
                'name' => 'GIA DỤNG',
                'alias' => 'gia-dung',
            ],
        ),
    ),
    'category_floors_desktop' => array(
        '0' => array(
            'id' => 420,
            'name' => 'MÁY LẠNH',
            'order' => 1,
            'product_list' => [75228, 66871, 63551, 63609, 63975, 66951],
        ),
        '1' => array(
            'id' => 420,
            'name' => 'QUẠT LÀM MÁT',
            'order' => 1,
            'product_list' => [53194, 49238, 64169, 53296, 53341, 53990],
        ),
        '2' => array(
            'id' => 350,
            'name' => 'TIVI - LOA - ÂM THANH',
            'order' => 2,
            'product_list' => [76204, 68203, 59859, 49790, 53540, 66719, 63979, 63971, 64746, 51366, 67539, 64521],
        ),
       '2' => array(
        			'id' => 480,
        			'name' => 'ĐIỆN THOẠI - TABLET - PHỤ KIỆN',
        			'order' => 3,
                   'product_list' => '70827,78664,77856,80190,77540,77544,77744,74828,74800,72423'
               ),
        '3' => array(
            'id' => 423,
            'name' => 'TỦ LẠNH',
            'order' => 4,
            'product_list' => [61969, 75944, 60845, 62083, 58755, 74908, 64790],
        ),
        '4' => array(
            'id' => 421,
            'name' => 'MÁY GIẶT - MÁY SẤY',
            'order' => 5,
            'product_list' => [20964, 49640, 62539, 36306, 67687, 67695, 75476, 73476, 54741, 54739],
        ),
        '5' => array(
            'id' => 343,
            'name' => 'MÁY ẢNH - PHỤ KIỆN',
            'order' => 6,
            'product_list' => [53700, 19714, 25119, 54968, 60853, 52860, 40472, 58535, 80577, 80581],
        ),
//		,
        //        '6' => array(
        //			'id' => 527,
        //			'name' => 'LAPTOP - PC - PHỤ KIỆN',
        //			'order' => 7,
        //            'product_list' => '65948,73596,75004,79478,56052,72204,70876,66524,65430,55326,69699'
        //        )

        ),
    'config_block_home_v2' => array(
        'location' => 394,
        'block_id' => 2628,
        'name' => 'Quà ngon giá tốt',
        'name1' => 'Chương trình nổi bật',
        'name2' => 'Tin tức & mẹo vặt'
    ),
    'slide_menu' => array(
        0 => array(
            array(
                "icon" =>"EA10",
                "title" => "Giao nhận tiện lợi",
                "color" => ""
            ),
            array(
                "icon" => "E9E2",
                "title" => "Lắp đặt chuyên nghiệp",
                "color" => ""
            )
        ),
        1 => array(
            array(
                "icon" => "EA08",
                "title" => "Thanh toán linh hoạt",
                "color" => ""
            ),
            array(
                "icon" => "Ea0B",
                "title" => "Đổi trả dễ dàng",
                "color" => ""
            )
        ),
        2 => array(
            array(
                "icon" => "E94F",
                "title" => "Hậu mãi chu đáo",
                "color" => ""
            ),
            array(
                "icon" =>"EA10",
                "title" => "Giao nhận tiện lợi",
                "color" => ""
            )
        )
    ),
    'check_prouct_view' => 0,
    'check_news' => 0,
    'check_menu_icon_home_v2' => 1,
    'check_menu_icon_home_top' => 0,
    'check_flash_sale' => 1, // 1: hien thi, 0: tat
    'check_off_promotion' => 1,
    'check_endtime_flash_sale' => 1, // 1: con thoi gian, 0: het thoi gian
    'check_endtime_promotion' => 1, // 1: con thoi gian, 0: het thoi gian
    'check_pay_day' => 1,
    'check_pay_day' => 1,
    'check_promotion' => 1,
    'check_text_price_online' => 0,
    'check_slide_menu' => 0,
    'flash_sale_id' => 382, // flash sale
    'pay_day' => 2810, // pay_day
    'promotion_id' => 2812, // promotion
    'date_flash_sale' => '2020-02-10 00:00:00',
    'date_pay_day' => '2020-09-30 23:59:59',
    'date_promotion' => '2020-09-30 23:59:59',
    /* config gia dung */
    'config_block_giadung' => array(
        'location' => 511,
        'block_id' => 2628,
        'name' => 'Quà ngon giá tốt',
        'name1' => 'Chương trình nổi bật',
        'name2' => 'Tin tức & mẹo vặt'
    ),
    'category_giadung' => array(
        '1' => array(
            'id' => 2896,
            'color' => '#F00',
            'name' => 'Nổi bật',
            'alias' => '',
            'icon' => 'HOT',
            'position_display' => 1,
        ),
        '2' => array(
            'id' => 2902,
            'color' => '#F00',
            'name' => 'Lò vi sóng, lò nướng',
            'alias' => '',
            'icon' => '',
            'position_display' => 2,
        ),
        '3' => array(
            'id' => 2906,
            'color' => '#F00',
            'name' => 'Máy xay sinh tố',
            'alias' => '',
            'icon' => '',
            'position_display' => 3,
        ),
        '4' => array(
            'id' => 2900,
            'color' => '#F00',
            'name' => 'Nồi cơm điện',
            'alias' => '',
            'icon' => '',
            'position_display' => 4,
        ),
        '5' => array(
            'id' => 2910,
            'color' => '#F00',
            'name' => 'Quạt điều hòa',
            'alias' => '',
            'icon' => '',
            'position_display' => 5,
        ),
        '6' => array(
            'id' => 2904,
            'color' => '#F00',
            'name' => 'Bình đun, máy sấy tóc',
            'alias' => '',
            'icon' => '',
            'position_display' => 6,
        ),
        '7' => array(
            'id' => 2908,
            'color' => '#F00',
            'name' => 'Máy lọc nước',
            'alias' => '',
            'icon' => '',
            'position_display' => 7,
        ),
        '8' => array(
            'id' => 2912,
            'color' => '#F00',
            'name' => 'Điện gia dụng khác',
            'alias' => '',
            'icon' => '',
            'position_display' => 8,
        ),
    ),
    'category_menu_giadung' => array(
        '1' => array(
            'id' => 34,
            'borderColor' => '#F00',
            'name' => 'Gia dụng nhà bếp',
            'color' => '#000',
            'position_display' => 1,
            'row' => 2,
        ),
        '2' => array(
            'id' => 36,
            'borderColor' => '#F00',
            'name' => 'Thiết bị gia đình',
            'color' => '#000',
            'position_display' => 2,
            'row' => 1
        ),
        '3' => array(
            'id' => 38,
            'borderColor' => '#F00',
            'name' => 'Sức khoẻ gia đình',
            'color' => '#000',
            'position_display' => 3,
            'row' => 1
        ),
    ),

]


?>
