<?php

use Monolog\Logger;

return array(
    'server'          => "103.9.76.104:27017",
    'database'        => 'nkm_db',
    'collection'      => 'logs',
    'log_level'       => Logger::DEBUG,
    'time_zone'       => 'UTC',
    'datetime_format' => 'Y-m-d H:i:s',
);