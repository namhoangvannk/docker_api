<?php
/**
 * Created by NGUYEN KIM.
 * User: Thu.VuDinh@trade.nguyenkim.com
 * Date: 23/03/2018
 * Time: 1:09 AM
 */

return [
    'big4u' => [
        'prefix' => 'NKB4U',
        'start_date' => '2019-01-01 00:00:00',
        'end_date' => '2019-01-31 23:59:59',
        'duration' => 15,
        'notify' => [
            'sms' => false,
            'email' => true
        ],
        'utm_source' => 'san_coupon',
        'utm_medium' => 'email',
        'utm_campaign' => 'san_coupon'
    ],
    'sancp' => [
        'prefix' => 'NKCP',
        'start_date' => '2019-01-18 00:00:00',
        'end_date' => '2019-01-31 23:59:59',
        'duration' => 15,
        'notify' => [
            'sms' => false,
            'email' => true
        ],
        'utm_source' => 'san_coupon',
        'utm_medium' => 'email',
        'utm_campaign' => 'san_cp',
        'discount'=>[3,6,10],
        'quantity_discount'=>[40,50,10]
    ]
];