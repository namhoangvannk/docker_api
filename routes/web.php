<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () use ($router) {
    return response()->json([
        'name' => 'Welcome NK Online Service',
        'version' => '1.0.0'
    ]);
});

if (!function_exists('restfulRoutes')) {
    function restfulRoutes($path, $controller, $router)
    {
        $router->get($path . '/', $controller . '@index');
        $router->get($path . '/{id}', $controller . '@show');
        $router->post($path, $controller . '@create');
        $router->post($path.'/import', $controller . '@import');
        $router->put($path . '/{id}', $controller . '@update');
        $router->delete($path . '/{id}', $controller . '@delete');
    }
}

/*--------------------------------------------------------------------------*/
/*                              PRODUCT                                     */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Product'], function () use ($router) {
    restfulRoutes('products', 'ProductsController', $router);
    $router->post('products-cron-update', 'ProductsController@updatePrice');
    $router->post('products-update-excel', 'ProductsController@updateFromExcel');
    $router->get('products-regions-amount/{product_id}', 'ProductsController@getRegionsByProduct');
    $router->get('products-relation/{product_id}', 'ProductsController@getRelationProducts');
	 $router->post('redis-value', 'ProductsController@setRedisValue');
});

$router->group(['prefix' => 'v1', 'namespace' => 'Product', 'middleware' => 'public_auth'], function () use ($router) {
    restfulRoutes('products-pdw', 'ProductPdwController', $router);
});

/*--------------------------------------------------------------------------*/
/*                              ORDER                                       */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Order', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('orders', 'OrderController', $router);
    $router->get('order-mall/update', 'OrderController@updateMall');
});

$router->group(['prefix' => 'v1', 'namespace' => 'Order', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('orders-vas', 'OrderVasController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Order', 'middleware' => 'auth'], function () use ($router) {
    $router->post('order/count-product-sale', 'OrderController@trackingProductSale');
    $router->post('order/update-card-name', 'OrderController@updateCardName');

});

$router->group(['prefix' => 'v1', 'namespace' => 'Order', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('order-coupons', 'OrderCouponsController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Order', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('extshipments', 'ExtShipmentsController', $router);
    $router->post('extshipments/tracking', 'ExtShipmentsController@tracking');
    $router->post('extshipments/log', 'ExtShipmentsController@log');
});

$router->group(['prefix' => 'v1', 'namespace' => 'Order'], function () use ($router) {
    $router->post('orders-pdw/tracking-status', 'OrderPdwController@trackingStatus');
});
$router->group(['prefix' => 'v1', 'namespace' => 'Order', 'middleware' => 'public_auth'], function () use ($router) {
    restfulRoutes('orders-pdw', 'OrderPdwController', $router);
    restfulRoutes('orders-pdw/get/{order_id}', 'OrderPdwController@get', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Order'], function () use ($router) {
    $router->post('cybersource/call-silent', 'CybersourceController@callSilentWithCardInfo');
});

$router->group(['prefix' => 'v1', 'namespace' => 'Order'], function () use ($router) {
    $router->post('cybersource/update-card', 'CybersourceController@updateWithCardInfo');
});
/*--------------------------------------------------------------------------*/
/*                              PAYMENT                                     */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Payment', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('payment-recall', 'PaymentRecallController', $router);
    $router->post('payment_recall_ant', 'PaymentRecallAntController@create');
});

/*--------------------------------------------------------------------------*/
/*                              SHIPPING                                    */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Shipping', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('shipping-vendors', 'NkShippingVendorsController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Shipping', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('shipping-regions-cities', 'NkShippingRegionsCitiesController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Shipping', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('shipping-price-packages', 'NkShippingPricePackagesController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Shipping', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('shipping-range-weights', 'NkShippingRangeWeightsController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Shipping', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('shipping-routes', 'NkShippingRoutesController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Shipping', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('shipping-vrp-configs', 'NkShippingVrpConfigsController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Shipping', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('shipping-vrp-price-configs', 'NkShippingVrpPriceConfigsController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Shipping', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('shippings', 'ShippingsController', $router);
    $router->post('shippings/checkout', 'ShippingsController@checkout', $router);
});


/*--------------------------------------------------------------------------*/
/*                              PROMOTION                                   */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Promotion', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('promotions', 'PromotionsController', $router);
    $router->post('promotions/check', 'PromotionsController@check', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Promotion', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('promotions-today', 'PromotionsUseTodayController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Promotion', 'middleware' => 'auth'], function ($router) {
    restfulRoutes('promotions-voucher', 'PromotionVoucherController', $router);
});

//$router->group(['prefix' => 'v1', 'namespace' => 'Promotion', 'middleware' => 'public_auth'], function ($router) {
//    restfulRoutes('coupons-auto', 'CouponsAutomaticController', $router);
//});

$router->group(['prefix' => 'v1', 'namespace' => 'Promotion', 'middleware' => 'auth'], function ($router) {
    $router->post('coupons-auto/check', 'CouponsAutomaticController@check', $router);
    $router->post('bigc/send-coupon-sms', 'BigcController@sendCouponSms', $router);
});

/*--------------------------------------------------------------------------*/
/*                              COMMON                                      */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Common', 'middleware' => 'auth'], function () use ($router) {
    $router->post('commons/generate-qrcode', 'CommonController@generateQRCode', $router);
	$router->post('commons/api-log', 'CommonController@createApiLog', $router);

    // *******  NK Redis : get value, set value, set time to live
    $router->get('nkredis/getbykey/{key}','RedisController@getRedisByKey');
    $router->post('nkredis/setbyKey','RedisController@setRedisByKey');
    $router->post('nkredis/settime2live','RedisController@setRedisTime2LiveByKey');
    $router->post('commons/generate-qrcode-partner', 'CommonController@generateQRCodePartner');
});

/*--------------------------------------------------------------------------*/
/*                              SYSTEM                                      */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'System', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('user-gateways', 'UserGatewaysController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'System', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('modules', 'NkModulesController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'System', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('module-actions', 'NkActionsController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'System', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('usergateway-permission', 'UsergatewayPermissonController', $router);
});

$router->group(['prefix' => 'v1', 'namespace' => 'Auth'], function () use ($router) {
    restfulRoutes('users', 'UsersController', $router);
});

/*--------------------------------------------------------------------------*/
/*                              COMMENT                                     */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Discussion', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('discussions', 'DiscussionsController', $router);
});


/*--------------------------------------------------------------------------*/
/*                              AUTH                                      */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Auth'], function () use ($router) {
    $router->post('auth/sigin', 'AuthController@sigin', $router);
    $router->post('auth/issigin', 'AuthController@issigin', $router);
    $router->post('auth/signup', 'UsersController@create', $router);
    $router->post('auth/check_email', 'AuthController@check_email', $router);
    $router->post('user/trackingOrder', 'UsersController@trackingOrder', $router);
    $router->post('register_social', 'UsersController@register_social');
    $router->post('get_info_social', 'UsersController@get_info_social');
});

/*--------------------------------------------------------------------------*/
/*                              CREDIT CARD                                 */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'CreditCard', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('credit-card', 'CreditCardController', $router);
});

/*--------------------------------------------------------------------------*/
/*                              ZALO PAY                                 */
/*--------------------------------------------------------------------------*/

$router->group(['prefix' => 'v1', 'namespace' => 'Order'], function () use ($router) {
    $router->post('order-zalopay/notify', 'OrderZaloPayController@notify', $router);
});
$router->group(['prefix' => 'v1', 'namespace' => 'Order', 'middleware' => 'auth'], function () use ($router) {
    restfulRoutes('order-zalopay', 'OrderZaloPayController', $router);
    $router->get('order-zalopay/tracking-status/{id}', 'OrderZaloPayController@trackingStatus');
});

/*--------------------------------------------------------------------------*/
/*                              BIG4U                                 */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'Big4u', 'middleware' => 'public_auth'], function () use ($router) {
    $router->post('big4u/tracking', 'Big4uController@trackingStatus');
    $router->get('big4u/product/{id}', 'Big4uController@searchProduct');
    $router->get('big4u/report', 'Big4uController@report');
    restfulRoutes('big4u', 'Big4uController', $router);
});
$router->group(['prefix' => 'v1', 'namespace' => 'Big4u', 'middleware' => 'auth'], function () use ($router) {
    $router->post('nkbig4u/tracking', 'Big4uController@trackingStatus');
    $router->get('nkbig4u/cron-unset-coupons', 'Big4uController@cronUnsetCoupons');
    $router->get('nkbig4u/product/{id}', 'Big4uController@searchProduct');
    $router->get('nkbig4u/report', 'Big4uController@report');
    restfulRoutes('nkbig4u', 'Big4uController', $router);
    // dan moi them
    $router->post('banner-pdp','Big4uController@createBannerPDP');
    $router->get('banner-pdp','Big4uController@getAllBannerPDP');
    $router->get('banner-pdp/promotion/{id}','Big4uController@getBannerByPromotionID');
    $router->post('banner-pdp/promotion/{promotion_id}','Big4uController@updateBannerByPromotionID');
    $router->get('banner-pdp/search/{product_id}','Big4uController@checkIsShowBannerPDP');
    $router->post('banner-pdp/update-single/{m_id}','Big4uController@updateSingleBannerPDP');
});

/*--------------------------------------------------------------------------*/
/*                          HOTDEAL GIA RE ONLINE                           */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'HotDeal','middleware' => 'auth'], function () use ($router) {
    $router->get('hotdeal/product/{id}', 'HotDealController@getById');
    $router->get('hotdeal-export-running', 'HotDealController@exportHotdealRunning');
    $router->post('hotdeal-import', 'HotDealController@import');
    $router->post('hotdeal-import-status', 'HotDealController@importStatus');
    $router->post('hotdeal-off-campaign', 'HotDealController@updateStatusEndCampaign');
    $router->post('hotdeal-off-product', 'HotDealController@updateStatusEnd');
    $router->post('hotdeal-export-checked', 'HotDealController@getHotdealByMids');
    // FIXME update v2
    $router->post('hotdeal-update-products', 'HotDealController@updateV2');
    restfulRoutes('hotdeal', 'HotDealController', $router);
});
$router->group(['prefix' => 'v1', 'namespace' => 'HotDeal'], function () use ($router) {
      $router->get('hotdeal-cron', 'HotDealController@cronHotDeal');
});

/*--------------------------------------------------------------------------*/
/*                              WORLDCUP                                    */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'WorldCup', 'middleware' => 'auth'], function () use ($router) {
    /* Đội bóng */
    restfulRoutes('world-cup/teams', 'WorldCupTeamsController', $router);
    $router->put('world-cup/teams/{id}/scoring', 'WorldCupTeamsController@scoring');
    /* Người dự đoán */
    $router->get('world-cup/players', 'WorldCupPlayersController@index');
    $router->get('world-cup/players/{id}', 'WorldCupPlayersController@show');
    $router->post('world-cup/players', 'WorldCupPlayersController@create');
    $router->put('world-cup/players/{id}', 'WorldCupPlayersController@update');
    $router->post('world-cup/players/scoring', 'WorldCupPlayersController@scoring');
    $router->get('world-cup/updateuser', 'WorldCupPlayersController@updateuser');
    /* Trận đấu */
    $router->get('world-cup/matches', 'WorldCupMatchesController@index');
    $router->put('world-cup/matches/{id}', 'WorldCupMatchesController@update');
    $router->post('world-cup/matches', 'WorldCupMatchesController@create');
    $router->get('world-cup/matches/{id}', 'WorldCupMatchesController@show');
    /* Dự đoán */
    $router->get('world-cup/player-matches', 'WorldCupPlayerMatchesController@index');
    $router->post('world-cup/player-matches', 'WorldCupPlayerMatchesController@create');
    $router->get('world-cup/updatefinal', 'WorldCupMatchesController@updatefinal');
    /* Tính điểm người chơi*/
    $router->get('world-cup/ranks', 'WorldCupController@ranks');
    $router->get('world-cup/history/{id}', 'WorldCupController@guesses');
});

/*--------------------------------------------------------------------------*/
/*                              API func                                    */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'APIQuery', 'middleware' => 'auth'], function () use ($router) {
    $router->post('dbquery', 'APIQueryController@APIquery');
});

/*--------------------------------------------------------------------------
 *                              GHN TRACKING
 *--------------------------------------------------------------------------
 */
$router->group(['prefix' => 'v1', 'namespace' => 'Order','middleware' => 'auth'], function () use ($router) {
    $router->get('extshipments/ghn-tracking/{id}', 'GHNTrackingController@getTrackingData');
    $router->post('extshipments/ghn-tracking', 'GHNTrackingController@createTracking');
});
$router->group(['prefix' => 'v1', 'namespace' => 'Order'], function () use ($router) {
    $router->post('extshipments/ghn-callback', 'GHNTrackingController@receiveCallbackDataGHN');
});

$router->group(['prefix' => 'v1', 'namespace' => 'Banner'], function () use ($router) {
    $router->get('banner', 'BannerController@index');
    $router->post('banner', 'BannerController@create');
    $router->put('banner/{id}', 'BannerController@update');
    $router->delete('banner/{id}', 'BannerController@delete');
    $router->get('banner/{id}', 'BannerController@show');
    $router->post('banner/sync', 'BannerController@sync');
    $router->post('banner/sync2', 'BannerController@sync2');

});
$router->group(['prefix' => 'v1', 'namespace' => 'Sync'], function () use ($router) {

    $router->post('sync_blog', 'SyncController@blog');
    $router->post('sync_page', 'SyncController@page');
});
/*--------------------------------------------------------------------------
 *  API APP NGUYENKIM
 *--------------------------------------------------------------------------
 *
 *
 */
$router->group(['prefix' => 'v1', 'namespace' => 'ApiApp','middleware' => 'auth'], function () use ($router) {
    $router->get('menu', 'MenuController@index');
    $router->get('state', 'StateController@index');
    $router->get('BlockById/{id}', 'BlockController@index');
    $router->get('categories/{id}', 'ProductController@index');
    $router->get('categories-premium/{id}', 'ProductController@premium');
    $router->get('product-details/{id}/{user_id}', 'ProductController@show');
    $router->get('CateFloor','BlockController@getAll');
    $router->get('CateFloor/getById/{id}','BlockController@getById');
    $router->get('CateHome','BlockController@home');
    $router->get('CateHomeV2','BlockController@home_v2');
    $router->get('CateHomeV3','BlockController@home_v3');
    $router->get('giadung','ProductController@giadung');
    $router->post('categories_fillter', 'ProductController@categories_fillter');
    $router->get('BannerByCategoryID/{id}','BlockController@banner_by_category_id');
    /* sync from cscart */
    $router->post('SyncProductFullMode', 'ProductController@storeProductFullMode');
    $router->post('SyncProductFullModeNew', 'ProductController@storeProductFullModeNew');
    $router->get('SyncBrandProductFullMode', 'ProductController@syncBrandProductFullMode');

    $router->post('SyncProductFullMode3', 'ProductController@storeProductFullMode3');
    $router->post('SyncProductFullMode3Feature', 'ProductController@storeProductFullMode3Feature');
    $router->get('ProductFullMode/{id}', 'ProductController@getProductFullMode');
    $router->post('SyncCateFullMode', 'ProductController@storeCateFullMode');
    $router->get('HeaderApp', 'BlockController@getHeaderApp');
    $router->post('GetListProduct','ProductController@GetListProduct');
    $router->get('list_review','CommentsController@index');
    $router->post('storeLike','LikesController@storeLike');
    $router->get('likes','LikesController@index');
    $router->post('storeReview','CommentsController@storeReview');
    $router->get('rating','CommentsController@rating');
    $router->get('review','CommentsController@review');
    $router->get('categories-keyword/{id}','BlockController@category_search');
    $router->get('categories-pretties','BlockController@categories_pretties');
    $router->get('get-states', 'AddressController@getStates');
    $router->get('get-wards', 'AddressController@getWards');
    $router->get('get-districts', 'AddressController@getDistricts');
    $router->get('get-districts-by-state/{state_code}', 'AddressController@getDistrictsByState');
    $router->get('get-wards-by-district/{district_id}/{state_id}', 'AddressController@getWardsByDistrict');
    $router->get('get_option/{id}', 'PartialController@get_option');
    $router->get('get_video/{id}', 'PartialController@get_video');
    $router->get('get_image/{id}', 'PartialController@get_image');
    $router->get('barcode/{id}', 'ProductController@barcode');
    $router->get('checkqrcode', 'ProductController@checkqrcode');
    $router->get('get-payments', 'PaymentsController@index');
    $router->post('searchproduct', 'ProductController@search');
    $router->get('list-product', 'ProductForSaleController@listProduct');
    $router->get('sysn-homepage', 'LayoutController@index');
    $router->post('sysn-menu-cate-giadung', 'LayoutController@giadung');
    $router->post('sysn-blog-giadung', 'LayoutController@blog');

    $router->post('create-ticket', 'CrmController@createTicket');
    $router->post('resolved-ticket/{ticket_id}', 'CrmController@resolveTicket');
    $router->get('list-bank', 'InstallmentCreditController@listBank');
    $router->get('rate-conversion', 'InstallmentCreditController@conversionRate');
    $router->get('details-bank/{bank_id}', 'InstallmentCreditController@detailsBank');
    $router->post('validate-card/{bank_id}', 'InstallmentCreditController@validateCard');
    $router->post('card-type', 'InstallmentCreditController@cardType');
    $router->get('export-csv-feed','ProductForSaleController@exportCSV');
    $router->get('list-mall', 'PayController@getListmall');
    $router->post('package-optimize', 'PayController@packageOptimize');
    $router->get('list-hobby', 'ProfilesController@listHobby');
    $router->post('change-password/{user_id}', 'ProfilesController@changePassword');
    $router->post('update-user/{user_id}', 'ProfilesController@updateUser');
    $router->post('update-address', 'ProfilesController@updateAddress');
    $router->get('delete-address', 'ProfilesController@deleteAddress');
    $router->post('add-address', 'ProfilesController@addAddress');
    $router->get('list-address/{user_id}', 'ProfilesController@getListAddress');
    $router->get('address-default/{user_id}', 'ProfilesController@getAddressdefault');
    $router->get('get-profile/{user_id}', 'ProfilesController@getUser');
    $router->get('instruction-credit', 'InstructioncreditController@credit');
    $router->get('instruction-atm', 'InstructioncreditController@atm');
    $router->get('info-payment/{payment_id}', 'PaymentsController@info');
    $router->get('image-credit-cart', 'PaymentsController@imageCreditCart');
    $router->get('list-credit-cart', 'PaymentsController@getListCreditCart');
    $router->get('part-pay', 'PaymentsController@partPay');
    $router->get('conversion-rate', 'PaymentsController@conversionRate');
    $router->get('sysn-homepage', 'LayoutController@index');
    $router->post('info-credit-cart/', 'PaymentsController@infoCreditcart');
    $router->get('list-card-number/{user_id}', 'PaymentsController@listCardnumber');
    $router->get('list-credit', 'PaymentsController@listCredit');
    $router->post('cart-info', 'CartController@index');
    $router->post('forgot-password', 'ForgotpasswordController@index');
    $router->get('list_comment','CommentsController@list_comment');
    $router->post('storeReply','CommentsController@storeReply');
    $router->post('likePost','CommentsController@likePost');
    $router->get('mall', 'PartialController@get_mall');
    $router->get('count_duplication', 'ProductController@count_duplication');
    $router->get('review_by_user','CommentsController@list_review_by_user_id');
    $router->get('comment_by_user','CommentsController@list_comment_by_user_id');
    $router->get('promotions_by_user','PromotionsController@index');
    $router->post('upload-avatar', 'ProfilesController@uploadAvatar');
    $router->get('get_total_profile/{user_id}', 'PartialController@get_total_profile');
    $router->post('get_info_package', 'PayController@getInfoPackageByPriceMonth');
    $router->get('deeplink', 'DeeplinkController@index');
    $router->post('compare', 'ProductController@compare');
    $router->post('compare', 'ProductController@compare');
    $router->get('menu-simso', 'SimsoController@menu_simso');
    $router->get('get_price_by_provider', 'SimsoController@get_price_by_provider');
    $router->get('recharge_card', 'SimsoController@recharge_card');
    $router->get('recharge_phone', 'SimsoController@recharge_phone');
    $router->get('recharge_game', 'SimsoController@recharge_game');
    $router->get('get_price_by_provider', 'SimsoController@get_price_by_provider');
    $router->get('simso', 'SimsoController@simso');
    $router->get('search_simso', 'SimsoController@search_simso');

    $router->get('search_simso', 'SimsoController@search_simso');
    $router->get('banner-simso', 'SimsoController@banner');
    $router->get('vas_provider_value', 'SimsoController@vas_provider_value');
    $router->post('subscribe', 'PartialController@subscribe');
    $router->post('compare1', 'ProductController@compare1');
    $router->post('update-user-socical/{user_id}', 'ProfilesController@updateUserSocical');
    $router->post('get_area_id', 'AddressController@get_area_id');
    $router->get('get_order', 'OrdersController@index');
    $router->get('get_ton_kho', 'OrdersController@get_products_by_code');
    $router->get('get_redis', 'TestController@get_redis');
    $router->get('update_status_card', 'OrdersController@update_status_card');
    $router->get('getbrand', 'ProductController@getbrand');
    $router->post('create_log', 'LogsController@create');
    $router->post('momo_log', 'MomoController@create');
    $router->get('data-cate-home','BlockController@cat_home');
    $router->get('test', 'TestController@index');

});

$router->group(['prefix' => 'v1', 'namespace' => 'ApiApp'], function () use ($router) {
    $router->get('SyncAdditionDataFeed', 'ProductController@syncAdditionDataFeed');
    $router->get('cronSyncStocks2MongoAll', 'ProductController@cronSyncStocks2MongoAll');
});

$router->group(['prefix' => 'v1', 'namespace' => 'SyncMongo'], function () use ($router) {
    $router->post('productcategories/sync', 'ProductCategoriesController@sync');
});

/*--------------------------------------------------------------------------
 *                              CALL PAYOO
 *--------------------------------------------------------------------------
 */
$router->group(['prefix' => 'v1', 'namespace' => 'Payoo', 'middleware' => 'auth'], function () use ($router) {
    $router->post('payoo/execute2', 'PayooController@execute2', $router);
    $router->post('payoo/api-log', 'PayooController@payoo', $router);
});

/*--------------------------------------------------------------------------
 *                              SYNC STOCK
 *--------------------------------------------------------------------------
 */
$router->group(['prefix' => 'v1', 'namespace' => 'Stock'], function () use ($router) {
    $router->get('stock', 'StockController@index', $router);
    $router->get('sync_product', 'StockController@syncProduct', $router);
    $router->get('sync_warehouse', 'StockController@syncWarehouseRepositories/ProductsRepository.php', $router);
    $router->get('sync_product_redis', 'StockController@syncProductStatus2Redis', $router);
});

/*--------------------------------------------------------------------------
 *                            refund api log
 *--------------------------------------------------------------------------
 */

$router->group(['prefix' => 'v1', 'namespace' => 'RefundLog', 'middleware' => 'auth'], function () use ($router) {
    $router->get('refund_log/{id}', 'RefundLogController@index', $router);
    $router->post('refund_log/inset', 'RefundLogController@create', $router);
    $router->post('refund_log/update', 'RefundLogController@update', $router);
});

/*--------------------------------------------------------------------------
 *                            settle api log
 *--------------------------------------------------------------------------
 */

$router->group(['prefix' => 'v1', 'namespace' => 'SettleLog', 'middleware' => 'auth'], function () use ($router) {
    $router->get('settle_log/{id}', 'SettleLogController@index', $router);
    $router->post('settle_log/inset', 'SettleLogController@create', $router);
});
/*--------------------------------------------------------------------------*/
/*                              FLASH SALES                                 */
/*--------------------------------------------------------------------------*/
$router->group(['prefix' => 'v1', 'namespace' => 'FlashSales', 'middleware' => 'auth'], function () use ($router) {
    $router->get('flash-sales-all', 'FlashSalesController@all',$router);
    $router->post('flash-sales-sync', 'FlashSalesController@sync');
});
