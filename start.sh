# run docker
docker-compose up -d --build

#install vendors
docker-compose run composer install

#create .env
docker-compose exec php cp .env.example .env

#clear cache
docker-compose exec php php artisan cache:clear


#migrate db
docker-compose exec php php artisan migrate

docker-compose exec php chmod -R 777 storage/logs

echo "Setup successfully"

read